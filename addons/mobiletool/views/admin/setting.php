<div class="page-title">
    <div>
        <h1><?= $heading_title ?></h1>
        <p><?= $heading_subtitle ?></p>
    </div>
    <div>
        <a href="<?= admin_url('system/addons') ?>" class="btn btn-default"><i class="fa fa-lg fa-reply"></i>
            Kembali</a>
        <button id="submit" class="btn btn-success"><i class="fa fa-lg fa-check"></i> Save</button>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?= form_open($action, ['id' => 'form', 'class' => 'form-horizontal']) ?>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab" id="default">Build Configuration</a></li>
                    <!--<li><a href="#tab-2" data-toggle="tab">Interface</a></li>-->
                </ul>
                <div class="tab-content" style="padding-top:20px;">
                    <div class="tab-pane active" id="tab-1">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Enable</label>
                            <div class="toggle lg col-sm-4">
                                <label style="margin-top:5px;">
                                    <?php if ($active) { ?>
                                        <input type="checkbox" name="active" value="1" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="active" value="1">
                                    <?php } ?>
                                    <span class="button-indecator"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">First Name and Last Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="mobiletool_first_and_last_name"
                                       value="<?= isset($mobiletool_first_and_last_name) ? $mobiletool_first_and_last_name : ''; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Organization Unit</label>
                            <div class="col-sm-9">
                                <input type="text" name="mobiletool_organization_unit"
                                       value="<?= isset($mobiletool_organization_unit) ? $mobiletool_organization_unit : ''; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Organization Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="mobiletool_organization_name"
                                       value="<?= isset($mobiletool_organization_name) ? $mobiletool_organization_name : ''; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">City</label>
                            <div class="col-sm-9">
                                <input type="text" name="mobiletool_city"
                                       value="<?= isset($mobiletool_city) ? $mobiletool_city : ''; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Province</label>
                            <div class="col-sm-9">
                                <input type="text" name="mobiletool_province"
                                       value="<?= isset($mobiletool_province) ? $mobiletool_province : ''; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Two-letter Country Code</label>
                            <div class="col-sm-9">
                                <input type="text" name="mobiletool_country_code"
                                       value="<?= isset($mobiletool_country_code) ? $mobiletool_country_code : ''; ?>"
                                       max="2" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $this = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                $($this).attr('disabled', true);
                $($this).append('<span class="wait"> <i class="fa fa-refresh fa-spin"></i></span>');
            },
            complete: function () {
                $($this).attr('disabled', false);
                $('.wait').remove();
            },
            success: function (json) {
                $('.error').remove();
                $('.form-group').removeClass('has-error');
                if (json['redirect']) {
                    window.location = json['redirect'];
                } else if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').after('<span class="help-block error">' + json['errors'][i] + '</span>');
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                    }
                }
            }
        });
    });
</script>