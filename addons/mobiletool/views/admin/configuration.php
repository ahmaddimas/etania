<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <button id="submit" class="btn btn-success"><i class="fa fa-lg fa-check"></i> Save</button>
        <a href="<?= admin_url('mobiletool/configuration/build') ?>" class="btn btn-info"><i
                    class="fa fa-lg fa-android"></i> Build App</a>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?= form_open($action, ['id' => 'form', 'class' => 'form-horizontal']) ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">App Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="mobile_app_name"
                               value="<?= isset($mobile_app_name) ? $mobile_app_name : ''; ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Icon</label>
                    <div class="col-sm-9">
                        <input type="text" name="mobile_app_icon"
                               value="<?= isset($mobile_app_icon) ? $mobile_app_icon : ''; ?>" class="form-control">
                    </div>
                </div>
                <!--<div class="form-group">
                    <label class="control-label col-sm-3">Organization Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="mobiletool_organization_name" value="<? /*=isset($mobiletool_organization_name) ? $mobiletool_organization_name : ''; */ ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">City</label>
                    <div class="col-sm-9">
                        <input type="text" name="mobiletool_city" value="<? /*=isset($mobiletool_city) ? $mobiletool_city : ''; */ ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Province</label>
                    <div class="col-sm-9">
                        <input type="text" name="mobiletool_province" value="<? /*=isset($mobiletool_province) ? $mobiletool_province : ''; */ ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Two-letter Country Code</label>
                    <div class="col-sm-9">
                        <input type="text" name="mobiletool_country_code" value="<? /*=isset($mobiletool_country_code) ? $mobiletool_country_code : ''; */ ?>" max="2" class="form-control">
                    </div>
                </div>-->
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });
</script>