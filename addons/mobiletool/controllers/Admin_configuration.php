<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_configuration extends Admin_Controller
{
    private $setting = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_mobiletool');

        if (empty($this->setting['active'])) {
            show_404();
        }


        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('format');
    }

    public function index()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->model('mobiletool_configuration_model');

        $data['action'] = admin_url('mobiletool/configuration/validate');
        $data['mobile_app_id'] = null;
        $data['mobile_app_name'] = '';
        $data['mobile_app_icon'] = '';
        /*if ($account = $this->mobiletool_configuration_model->get($mobile_app_id)) {
            $data['mobile_app_id'] = (int)$account['mobile_app_id'];
            $data['mobile_app_name'] = $account['mobile_app_name'];
            $data['mobile_app_icon'] = $account['mobile_app_icon'];
        }*/

        $data['success'] = $this->session->userdata('success');
        $data['error'] = $this->session->userdata('error');
        $data['message'] = $this->session->userdata('message');

        $this->load
            ->title('Mobile App Configuration')
            ->js('/assets/js/bootstrap-typeahead.min.js')
            ->view('admin/configuration', $data);
    }

    /**
     * Load product form
     *
     * @access private
     * @param int $account_id
     * @return void
     */
    private function form($whatsapp_account_id = null)
    {
        $this->load->model('Mobiletool_configuration_model');

        $data['action'] = admin_url('whatsapp/account/validate');
        $data['whatsapp_account_id'] = null;
        $data['whatsapp_account_name'] = '';
        $data['whatsapp_account_number'] = null;
        $data['whatsapp_account_title'] = '';
        $data['whatsapp_account_status'] = 0;

        $this->load->library('image');

        $data['whatsapp_account_picture'] = $this->image->resize('no_image.jpg', 150, 150);
        $data['whatsapp_account_picture_src'] = $this->image->resize('no_image.jpg', 150, 150);

        if ($account = $this->whatsapp_account_model->get($whatsapp_account_id)) {
            $data['whatsapp_account_id'] = (int)$account['whatsapp_account_id'];
            $data['whatsapp_account_name'] = $account['whatsapp_account_name'];
            $data['whatsapp_account_number'] = (int)$account['whatsapp_account_number'];
            $data['whatsapp_account_title'] = $account['whatsapp_account_title'];
            $data['whatsapp_account_status'] = $account['whatsapp_account_status'];
            $data['whatsapp_account_picture'] = $account['whatsapp_account_picture'];
            $data['whatsapp_account_picture_src'] = $this->image->resize($account['whatsapp_account_picture'], 150, 150);
        }


        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/account_form', $data, true, true)
        )));
    }

    /**
     * Validate product form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $post = $this->input->post();

        $mobile_app_id = $post['mobile_app_id'];

        $this->form_validation
            ->set_rules('mobile_app_name', 'Nama App', 'trim|required')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $this->load->model('mobiletool_configuration_model');
            $account = $this->mobiletool_configuration_model->cek_akun($post['mobile_app_id']);

            if ($mobile_app_id) {
                $this->mobile_app_model->update($mobile_app_id, $post);

                $json['success'] = lang('success_update');
            } else {
                if (count($account) == 0) {
                    if ($this->whatsapp_account_model->insert($post)) {
                        $json['success'] = lang('success_create');
                    }
                } else {
                    $json['error']['product'] = "Product exist";
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Upload picture account
     *
     * @access public
     * @return json
     */
    public function upload()
    {
        $json = array();

        $upload_path = DIR_IMAGE . 'whatsapp';

        if (!file_exists($upload_path)) {
            @mkdir($upload_path, 0777);
        }

        $this->load->library('upload', array(
            'upload_path' => $upload_path,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size' => '3000',
            'encrypt_name' => true
        ));

        if (!$this->upload->do_upload('accountpicture')) {
            $json['error'] = $this->upload->display_errors('', '');
        } else {
            $this->load->library('image');
            $this->load->model('Mobiletool_configuration_model');

            $upload_data = $this->upload->data();
            $image = substr($upload_data['full_path'], strlen(FCPATH . DIR_IMAGE));

            $thumb = $this->image->resize($image, 150, 150);
            $this->whatsapp_account_model->update($this->input->get('id'), array('whatsapp_account_picture' => $image));

            $json['image'] = $thumb;
            $json['imageUrl'] = $image;
            $json['success'] = 'File berhasil diupload!';
        }

        $this->output->set_output(json_encode($json));
    }

    public function build()
    {
        $settings = $this->setting_model->get_settings();
        $json = array();

        if (isset($settings['addon_mobiletool'])) {
            foreach ($settings['addon_mobiletool'] as $key => $value) {
                $data[$key] = $value;
            }
        } else {
            $json['error']['build_configuration'] = "Please complete Build Configuration data";
        }

        $keys = array(
            "mobiletool_first_and_last_name" => "First and Last Name is empty",
            "mobiletool_organization_unit" => "Organization Unit is empty",
            "mobiletool_organization_name" => "Organization Name is empty",
            "mobiletool_city" => "City is empty",
            "mobiletool_province" => "Province is empty",
            "mobiletool_country_code" => "Country Code is empty"
        );
        foreach ($keys as $key => $value) {
            if (empty($data[$key])) {
                $json['error']['build_configuration'] = $value;
            }
        }

        if (!empty($json['error'])) {
            $this->output->set_output(json_encode($json));
        } else {
            $serverUrl = "172.25.139.10:8080";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $serverUrl . "/g");
            curl_setopt($ch, CURLOPT_POST, count($data));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $output = curl_exec($ch);
            curl_close($ch);
            echo $output;
//            redirect("http://".$serverUrl.$output);
        }
    }

}
	
