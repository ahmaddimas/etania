<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller
{
    /**
     * Construction
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_mobiletool');


        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->config('mobiletool/manifest');
    }

    /**
     * Module setting
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $data['active'] = false;
        $data['title'] = '';
        $data['subtitle'] = '';

        $settings = $this->setting_model->get_settings();

        if (isset($settings['addon_mobiletool'])) {
            foreach ($settings['addon_mobiletool'] as $key => $value) {
                $data[$key] = $value;
            }
        }

        $data['heading_title'] = $this->config->item('name');
        $data['heading_subtitle'] = 'Pengaturan modul ' . $this->config->item('name') . ' versi ' . $this->config->item('version');

        $data['action'] = admin_url('mobiletool/validate');

        $this->load
            ->js('/assets/js/bootstrap-typeahead.min.js')
            ->view('admin/setting', $data);
    }

    /**
     * Validate configuration
     *
     * @access public
     * @return void
     */
    public function validate()
    {
        check_ajax();

        $json = array();


        $post = $this->input->post();


        if ($this->input->post('active') == null) {
            $post['active'] = 0;
        }

        $this->setting_model->edit_setting('addon_mobiletool', $post);
        $this->session->set_flashdata('success', 'Pengaturan modul ' . $this->config->item('name') . ' berhasil disimpan');
        $json['redirect'] = admin_url('system/addons');


        $this->output->set_output(json_encode($json));
    }
}