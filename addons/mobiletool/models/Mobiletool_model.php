<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobiletool_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add database
     *
     * @access public
     * @return void
     */
    public function install()
    {
        // TODO: fix field in database
        $sql[] = "CREATE TABLE `mobiletool_configuration` (
				  `mobile_app_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				  `mobile_app_name` varchar(100) NOT NULL,
				  `mobile_app_icon` varchar(13) NOT NULL,
				  `mobile_app_title` varchar(255) NOT NULL,
				  `mobile_app_picture` text NOT NULL,
				  `mobile_app_status` int(1) NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

        $this->db->simple_query("SET SQL_MODE = 'NO_ZERO_IN_DATE'");

        foreach ($sql as $query) {
            $this->db->query($query);
        }
    }

    /**
     * Remove database
     *
     * @access public
     * @return void
     */
    public function uninstall()
    {
        $this->load->dbforge();

        $this->dbforge->drop_table('mobiletool_configuration', true);
    }

    /**
     * Get menus
     *
     * @access public
     * @return array
     */
    public function get_menus()
    {
        return array(
            array(
                'title' => 'Configuration',
                'url' => admin_url('mobiletool/configuration'),
            )
        );
    }
}
