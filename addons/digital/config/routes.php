<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['digital/admin/product(:any)?'] = 'admin_product$1';
$route['digital/admin/order(:any)?'] = 'admin_order$1';
