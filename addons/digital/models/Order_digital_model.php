<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_digital_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function create($order_id)
    {
        $data = array('order_id' => $order_id);
        $this->db
            ->set($this->set_data($data))
            ->insert('order_digital');
    }

    public function cekorder($order_id)
    {
        $data = $this->db
            ->from('order_digital')
            ->where('order_id', (int)$order_id)
            ->get()
            ->row_array();

        return count($data);
    }


    public function getorder($order_id)
    {
        $data = $this->db
            ->from('order_digital')
            ->where('order_id', (int)$order_id)
            ->get()
            ->row_array();

        return $data;
    }


    public function get_order_products($order_id)
    {
        $results = array();

        $order = $this->db
            ->from('order')
            ->where('order_id', (int)$order_id)
            ->get()
            ->row_array();

        $orderdigital = $this->db
            ->from('order_digital')
            ->where('order_id', (int)$order_id)
            ->get()
            ->row_array();


        $order_products = $this->db
            ->where('order_id', (int)$order_id)
            ->get('order_product')
            ->result_array();

        foreach ($order_products as $order_product) {
            $order_product['link'] = $this->db
                ->select('file')
                ->where('product_id', (int)$order_product['product_id'])
                ->where('customer_id', $order['customer_id'])
                ->where('order_digital_id', $orderdigital['order_digital_id'])
                ->get('product_digital')
                ->result_array();

            $order_product['option'] = $this->db
                ->select('oo.*, pov.subtract')
                ->join('product_option_value pov', 'pov.product_option_value_id = oo.product_option_value_id', 'left')
                ->where('oo.order_id', (int)$order_id)
                ->where('oo.order_product_id', (int)$order_product['order_product_id'])
                ->get('order_option oo')
                ->result_array();

            $results[] = $order_product;
        }

        return $results;
    }


}