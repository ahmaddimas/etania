<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_digital_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function set_product_digitals($product_id, $digitals = array(), $type)
    {
        if ($type == 1) {
            $qty = count($digitals);
            $this->db
                ->set(array(
                    'quantity' => $qty,
                ))->where('product_id', $product_id)
                ->update('product');
        }
    }


    public function getproducts($product_id)
    {
        return $this->db
            ->where('product_id', (int)$product_id)
            ->where('customer_id', 0)
            ->where('order_digital_id', 0)
            ->get('product_digital')
            ->result_array();
    }


    public function sendfile($p, $order_digital_id, $customer_id)
    {

        $ress = 1;
        foreach ($p as $ps) {
            $product_id = $ps['product_id'];
            $qty = $ps['quantity'];

            $data = $this->db
                ->where('product_id', (int)$product_id)
                ->where('customer_id', 0)
                ->where('order_digital_id', 0)
                ->limit($qty)
                ->order_by('product_id', 'asc')
                ->get('product_digital')
                ->result_array();

            if (count($data) < $qty) {
                $ress = 0;
            }
        }

        if ($ress == 0) {
            return $ress;
        } else {
            foreach ($p as $ps) {
                $product_id = $ps['product_id'];
                $qty = $ps['quantity'];

                $data = $this->db
                    ->where('product_id', (int)$product_id)
                    ->where('customer_id', 0)
                    ->where('order_digital_id', 0)
                    ->limit($qty)
                    ->order_by('product_id', 'asc')
                    ->get('product_digital')
                    ->result_array();


                foreach ($data as $dt) {
                    $this->db
                        ->set(array(
                            'order_digital_id' => $order_digital_id,
                            'customer_id' => $customer_id,
                        ))->where('product_digital_id', $dt['product_digital_id'])
                        ->update('product_digital');
                }

                $qtynow = $this->db
                    ->where('product_id', (int)$product_id)
                    ->where('order_digital_id', 0)
                    ->where('customer_id', 0)
                    ->get('product_digital')
                    ->result_array();

                $this->db
                    ->set(array(
                        'quantity' => count($qtynow),
                    ))->where('product_id', $product_id)
                    ->update('product');


                $this->db
                    ->set(array(
                        'status' => 1,
                    ))->where('order_digital_id', $order_digital_id)
                    ->update('order_digital');

            }
            return $ress;
        }
    }


    public function get_product_digital($data)
    {
        $this->db
            ->select('product_id, product_digital_id, file')
            ->where('product_digital_id', $data)
            ->order_by('customer_id')
            ->from('product_digital');

        $product = $this->db
            ->get()
            ->row_array();

        return $product;
    }


    public function get_product_digitals($data)
    {
        $this->db
            ->select('product_digital_id, file, customer_id')
            ->where('product_id', $data)
            ->order_by('customer_id')
            ->from('product_digital');

        return $this->db->get()->result_array();
    }

    public function get_product_digital_solds($data)
    {
        $this->db
            ->select('p.product_digital_id, p.file, c.name')
            ->join('customer c', 'c.customer_id = p.customer_id', 'left')
            ->where('p.product_id', $data)
            ->where('p.customer_id <>', '0')
            ->order_by('p.customer_id')
            ->from('product_digital p');

        return $this->db->get()->result_array();
    }


    public function delete_product_digital($product_digital_id)
    {
        $pd = $this->get_product_digital($product_digital_id);

        $this->db
            ->where('product_digital_id', $product_digital_id)
            ->delete('product_digital');

        $digitals = $this->getproducts($pd['product_id']);
        $qty = count($digitals);
        $this->db
            ->set(array(
                'quantity' => $qty,
            ))->where('product_id', $pd['product_id'])
            ->update('product');
    }


}