<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Digital_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add database
     *
     * @access public
     * @return void
     */
    public function install()
    {
        $sql[] = "CREATE TABLE `product_digital` (
				  `product_digital_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				  `order_digital_id` int(11) NOT NULL DEFAULT '0',
				  `product_id` int(11) NOT NULL,
				  `file` varchar(255) NOT NULL,
				  `datetime` datetime NOT NULL,
				  `customer_id` int(11) NOT NULL DEFAULT '0'
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

        $sql[] = "CREATE TABLE `order_digital` ( 
					`order_digital_id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY , 
					`order_id` INT(11) NOT NULL , 
					`status` INT(11) NOT NULL COMMENT '0 pending, 1 done' , 
					`link` TEXT NOT NULL
					) ENGINE = InnoDB;";

        $this->db->simple_query("SET SQL_MODE = 'NO_ZERO_IN_DATE'");

        foreach ($sql as $query) {
            $this->db->query($query);
        }
    }

    /**
     * Remove database
     *
     * @access public
     * @return void
     */
    public function uninstall()
    {
        $this->load->dbforge();

        $this->dbforge->drop_table('product_digital', true);
        $this->dbforge->drop_table('order_digital', true);
    }

    /**
     * Get menus
     *
     * @access public
     * @return array
     */
    public function get_menus()
    {
        return array(
            array(
                'title' => 'Product',
                'url' => admin_url('digital/product'),
            ),
            array(
                'title' => 'Pesanan',
                'url' => admin_url('digital/order'),
            )
        );
    }
}