<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Digital extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('customer/order');
        $this->load->model('order/order_model');
        $this->load->model('digital/order_digital_model');
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('o.order_id, o.total, os.name as status, o.date_added,  od.status as filestatus, CONCAT(invoice_prefix, invoice_no) as invoice', false)
                ->join('order o', 'o.order_id = od.order_id', 'left')
                ->join('order_status os', 'os.order_status_id = o.order_status_id', 'left')
                ->from('order_digital od')
                ->where('o.customer_id', $this->customer->customer_id())
                ->edit_column('total', '$1', 'format_money(total)')
                ->edit_column('date_added', '$1', 'format_date(date_added)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            $this->load
                ->title(lang('heading_title'))
                ->breadcrumb(lang('text_home'), site_url())
                ->breadcrumb(lang('text_account'), site_url('customer'))
                ->breadcrumb(lang('heading_title'), site_url('customer/order'))
                ->css('/assets/plugins/datatables/dataTables.bootstrap.css')
                ->js('/assets/plugins/datatables/jquery.dataTables.min.js')
                ->js('/assets/plugins/datatables/dataTables.bootstrap.min.js')
                ->view('orderdigital');
        }
    }

    /**
     * Detail
     *
     * @access public
     * @param int $order_id
     * @return void
     */
    public function detail($order_id = null)
    {
        $this->load->helper('form');
        $this->load->helper('format');
        $this->load->library('currency');

        $order = $this->order_model->get_order($order_id);

        if ($order) {
            $data = $order;

            $data['date_added'] = format_date($order['date_added']);

            $address = array(
                'name' => '',
                'telephone' => '',
                'address' => $order['address'],
                'postcode' => $order['postcode'],
                'province' => $order['province'],
                'city' => $order['city'],
                'subdistrict' => $order['subdistrict']
            );

            $data['address'] = format_address($address);
            $data['products'] = array();

            $products = $this->order_digital_model->get_order_products($order['order_id']);

            foreach ($products as $product) {
                $data['products'][] = array(
                    'name' => $product['name'],
                    'sku' => $product['sku'],
                    'quantity' => $product['quantity'],
                    'price' => $this->currency->format($product['price'], '', '', true),
                    'total' => $this->currency->format($product['total'], '', '', true),
                    'option' => $product['option'],
                    'link' => $product['link']
                );
            }

            $data['totals'] = $this->order_model->get_order_totals($order['order_id']);
            $data['comment'] = nl2br($order['comment']);
            $data['action'] = site_url('customer/order/accept');

            $this->load
                ->title('Pesanan #' . $order['order_id'])
                ->breadcrumb(lang('text_home'), site_url())
                ->breadcrumb(lang('text_account'), site_url('customer'))
                ->breadcrumb(lang('heading_title'), site_url('customer/order'))
                ->breadcrumb('#' . $order['order_id'], site_url('customer/order/detail/' . $order['order_id']))
                ->css('/assets/plugins/datatables/dataTables.bootstrap.css')
                ->js('/assets/plugins/datatables/jquery.dataTables.min.js')
                ->js('/assets/plugins/datatables/dataTables.bootstrap.min.js')
                ->view('orderdigital_detail', $data);
        } else {
            show_404();
        }
    }

    /**
     * History
     *
     * @access public
     * @return void
     */
    public function history()
    {
        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('oh.*, oh.date_added as date_added, os.name as status', false)
                ->join('order_status os', 'os.order_status_id = oh.order_status_id', 'left')
                ->where('(oh.order_id = ' . $this->input->post('order_id') . ')', null, false)
                ->or_where('(oh.order_id = ' . $this->input->post('order_id') . ' AND oh.order_id = 0)', null, false)
                ->where('oh.notify', 1)
                ->from('order_history oh')
                ->edit_column('date_added', '$1', 'format_date(date_added, true)')
                ->edit_column('total', '$1', 'format_money(total)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            show_404();
        }
    }

    /**
     * Accept
     *
     * @access public
     * @return void
     */
    public function accept()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('text', 'lang:entry_review', 'trim|required|min_length[16]|max_length[255]');
        $this->form_validation->set_rules('rating', 'lang:entry_rating', 'trim|required|numeric');

        $json = array();

        if ($this->form_validation->run() == true) {
            $this->load->model('catalog/review_model');
            $this->load->model('order/order_model');
            $this->load->model('order/order_history_model');

            $order = $this->order_model->get($this->input->post('order_id'));
            $products = $this->order_model->get_order_products($order['order_id']);

            foreach ($products as $product) {
                $review = array(
                    'text' => htmlentities($this->input->post('text'), ENT_QUOTES, 'UTF-8'),
                    'rating' => $this->input->post('rating'),
                    'product_id' => $product['product_id'],
                    'customer_id' => $this->customer->customer_id(),
                    'author' => $this->customer->name(),
                    'active' => 0,
                    'date_added' => date('Y-m-d H:i:s', time())
                );

                $this->review_model->insert($review);
            }

            $order_history = array(
                'order_id' => (int)$order['order_id'],
                'order_status_id' => (int)$this->config->item('order_complete_status_id'),
                'notify' => 1,
                'comment' => htmlentities($this->input->post('text'), ENT_QUOTES, 'UTF-8')
            );

            $this->order_history_model->insert($order_history);

            $this->order_model->update((int)$this->input->post('order_id'), array(
                'order_status_id' => (int)$this->config->item('order_complete_status_id')
            ));

            $json['redirect'] = site_url('customer/order');
        } else {
            if (form_error('text') != '') {
                $json['error']['text'] = form_error('text');
            }
        }

        $this->output->set_output(json_encode($json));
    }
}