<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_order extends Admin_Controller
{
    private $setting = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_digital');

        if (empty($this->setting['active'])) {
            show_404();
        }
        $this->load->library('currency');

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('order/Order_model');
        $this->load->model('Order_digital_model');
        $this->load->model('Product_digital_model');
        $this->load->model('system/order_status_model');


    }


    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('o.order_id, o.date_added, o.name, o.total, od.status as filestatus, os.name as status, o.order_status_id', false)
                ->join('order o', 'o.order_id = od.order_id', 'left')
                ->join('order_status os', 'os.order_status_id = o.order_status_id', 'left')
                ->from('order_digital od')
                ->where('o.order_id <>', 'null')
                ->edit_column('date_added', '$1 WIB', 'format_date(date_added, true)')
                ->edit_column('total', '$1', 'format_money(total)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');

            $this->load
                ->title('Data Order')
                ->view('admin/digital_order', $data);
        }
    }

    public function detail($order_id = null)
    {
        $this->load->helper('format');


        if ($order = $this->Order_model->get_order($order_id)) {
            foreach ($order as $key => $value) {
                $data[$key] = $value;
            }

            $order_status = $this->order_status_model->get($data['order_status_id']);

            $data['order_statuses'] = $this->order_status_model->get_all();
            $data['total'] = $this->currency->format($data['total']);
            $data['status'] = $order_status ? $order_status['name'] : '';
            $data['date_added'] = date('d/m/Y', strtotime($order['date_added']));
            $data['products'] = array();

            $key = 1;

            foreach ($this->Order_digital_model->get_order_products($order_id) as $product) {
                $product['price'] = format_money($product['price']);
                $product['total'] = format_money($product['total']);

                $data['products'][$key] = $product;

                $key++;
            }

            $data['totals'] = $this->Order_model->get_order_totals($order_id);

            if ($this->input->get('ref')) {
                $data['back'] = admin_url($this->input->get('ref'));
            } else {
                $data['back'] = admin_url('sale/shipping');
            }

            $address = array(
                'name' => $data['name'],
                'address' => $data['address'],
                'telephone' => $data['telephone'],
                'postcode' => $data['postcode'],
                'subdistrict' => $data['subdistrict'],
                'city' => $data['city'],
                'province' => $data['province']
            );

            $data['shipping_address'] = format_address($address);
            $data['action'] = admin_url('order/add_history');

            $this->load
                ->title('Detil Order #' . $order_id)
                ->view('admin/digital_order_detail', $data);
        } else {
            show_404();
        }
    }


    public function sendfile($order_id)
    {
        $orders = $this->Order_model->get_order($order_id);
        $orderproducts = $this->Order_model->get_order_products($order_id);
        $orderdigitals = $this->Order_digital_model->getorder($order_id);
        $p = array();

        foreach ($orderproducts as $ops) {
            $ps = array();
            $ps['product_id'] = $ops['product_id'];
            $ps['quantity'] = $ops['quantity'];
            $p[] = $ps;
        }

        $ress = $this->Product_digital_model->sendfile($p, $orderdigitals['order_digital_id'], $orders['customer_id']);

        $this->output->set_output(json_encode($ress));
    }
} 