<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_product extends Admin_Controller
{
    private $setting = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_digital');

        if (empty($this->setting['active'])) {
            show_404();
        }

        $this->load->model('product_digital_model');
        $this->load->model('catalog/product_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('format');
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('p.type, p.product_id, p.name, p.active, p.model, p.sku, c.name as category, p.quantity')
                ->join('category c', 'c.category_id = p.category_id', 'left')
                ->where('p.type', 1)
                ->from('product p')
                ->edit_column('price', '$1', 'format_money(price)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');
            $data['message'] = $this->session->userdata('message');

            $this->load
                ->title('Produk Digital')
                ->view('admin/digital_product', $data);
        }
    }


    public function deleteproductdigital()
    {
        $product_digital_id = $this->input->post('product_digital_id');
        $p = $this->product_digital_model->get_product_digital($product_digital_id);
        unlink($p['file']);

        $this->product_digital_model->delete_product_digital($product_digital_id);
        $json['success'] = 'File deleted';
        $this->output->set_output(json_encode($json));
    }


    public function editupload($product_id)
    {
        $json = array();

        $upload_path = DIR_FILE . 'digital/' . $product_id;

        if (!file_exists($upload_path)) {
            mkdir($upload_path, 0777, true);
        }

        $this->load->library('upload', array(
            'upload_path' => $upload_path,
            // belum fixed, kenapa json masih ditolak?
            'allowed_types' => 'txt|jpg|jpeg|png',
            'max_size' => '4000',
            'overwrite' => true,
        ));

        if (!$this->upload->do_upload()) {
            $json['error'] = $this->upload->display_errors('', '');
        } else {
            $upload_data = $this->upload->data();

            $this->db
                ->set(array(
                    'product_id' => (int)$product_id,
                    'file' => 'storage/files/digital/' . $product_id . '/' . $upload_data['file_name'],
                ))->insert('product_digital');

            $p = $this->product_model->get_product($product_id);
            $digitals = $this->product_digital_model->getproducts($product_id);
            if ($p['type'] == 1) {
                $qty = count($digitals);
                $this->db
                    ->set(array(
                        'quantity' => $qty,
                    ))->where('product_id', $product_id)
                    ->update('product');
            }


            $json['file'] = substr($upload_data['full_path'], strlen($upload_path));
            $json['success'] = 'File berhasil diupload!';
        }

        $this->output->set_output(json_encode($json));
    }
} 