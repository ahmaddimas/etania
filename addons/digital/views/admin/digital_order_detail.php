<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
        <p><?= $date_added ?></p>
    </div>
    <div class="btn-group">
        <button onclick="window.history.back();" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</button>
    </div>
</div>
<section class="content">
    <div class="card">
        <div class="card-body">
            <div id="notification"></div>
            <div class="row">
                <div class="col-sm-6">
                    <dl class="dl-horizontal">
                        <dt>No. Invoice :</dt>
                        <dd><?= $invoice ?></dd>
                        <dt>Tanggal :</dt>
                        <dd><?= $date_added ?></dd>
                        <dt>Status :</dt>
                        <dd id="order-status"><?= $status ?></dd>
                        <dt>Nama :</dt>
                        <dd><?= $name ?></dd>
                        <dt>Email :</dt>
                        <dd><?= $email ?></dd>
                        <dt>No. Telepon :</dt>
                        <dd><?= $telephone ?></dd>
                        <dt>Total :</dt>
                        <dd><?= $total ?></dd>
                    </dl>
                </div>
                <div class="col-sm-6">
                    <dl class="dl-horizontal">
                        <dt>Metode Pembayaran :</dt>
                        <dd><?= $payment_method ?></dd>
                        <dt>Metode Pengiriman :</dt>
                        <dd><?= $shipping_method ?></dd>
                        <dt>Catatan :</dt>
                        <dd><?= $comment ?></dd>
                    </dl>
                </div>
            </div>
            <legend><h4>Item Produk</h4></legend>
            <div class="table-responsive">
                <table class="table table-bordered" width="100%">
                    <thead>
                    <tr>
                        <th>Produk</th>
                        <th class="text-right">Qty</th>
                        <th class="text-right">Harga</th>
                        <th class="text-right">Total</th>
                        <th class="text-right">File(s)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($products as $product) { ?>
                        <tr>
                            <td>
                                <?= $product['name'] ?>
                                <?php if ($product['option']) { ?>
                                    <p>
                                        <?php foreach ($product['option'] as $option) { ?>
                                            <small><i><?= $option['name'] ?>: <?= $option['value'] ?></i></small><br>
                                        <?php } ?>
                                    </p>
                                <?php } ?>
                            </td>
                            <td class="text-right"><?= $product['quantity'] ?></td>
                            <td class="text-right"><?= $product['price'] ?></td>
                            <td class="text-right"><?= $product['total'] ?></td>
                            <td>
                                <?php
                                if (count($product['link']) == 0) {
                                    echo "Waiting for confirm";
                                } else {

                                    $nod = 1;
                                    foreach ($product['link'] as $link) {
                                        ?>
                                        <?= $nod ?>. <a target="_blank"
                                                        href="<?= site_url($link['file']) ?>">Download</a><br>
                                        <?php
                                        $nod++;
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <?php foreach ($totals as $total) { ?>
                        <tr>
                            <th colspan="3" class="text-right"><?= $total['title'] ?></th>
                            <th class="text-right"><?= $total['text'] ?></th>
                        </tr>
                    <?php } ?>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<div id="modal"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('order/history')?>",
                "type": "POST",
                "data": function (d) {
                    d.order_id = "<?=$order_id?>";
                }
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "date_added"},
                {"orderable": false, "searchable": false, "data": "status"},
                {"orderable": false, "searchable": false, "data": "updater"},
                {"orderable": false, "searchable": false, "data": "comment"},
                {"orderable": false, "searchable": false, "data": "notify"},
            ],
            "createdRow": function (row, data, index) {
                if (data.notify == '1') {
                    $('td', row).eq(4).html('<i class="fa fa-check" style="color:green;"></i>');
                } else {
                    $('td', row).eq(4).html('');
                }
            },
            "sDom": '<"table-responsive" t>p',
            "order": [[0, 'desc']],
        });
    });

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }

</script>