<div class="page-title">
    <div>
        <h1><?= $heading_title ?></h1>
        <p><?= $heading_subtitle ?></p>
    </div>
    <div>
        <a href="<?= admin_url('system/addons') ?>" class="btn btn-default"><i class="fa fa-lg fa-reply"></i>
            Kembali</a>
        <button id="submit" class="btn btn-success"><i class="fa fa-lg fa-check"></i> Save</button>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?= form_open($action, ['id' => 'form', 'class' => 'form-horizontal']) ?>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab" id="default">Umum</a></li>
                    <li><a href="#tab-2" data-toggle="tab">Interface</a></li>
                </ul>
                <div class="tab-content" style="padding-top:20px;">
                    <div class="tab-pane active" id="tab-1">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Enable</label>
                            <div class="toggle lg col-sm-4">
                                <label style="margin-top:5px;">
                                    <?php if ($active) { ?>
                                        <input type="checkbox" name="active" value="1" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="active" value="1">
                                    <?php } ?>
                                    <span class="button-indecator"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Auto Text</label>
                            <div class="col-sm-9">
                                <input type="text" name="whatsapp_auto_text"
                                       value="<?= isset($whatsapp_auto_text) ? $whatsapp_auto_text : ''; ?>"
                                       class="form-control">
                                <span class="text-muted">This text will pre-populate the chat textfield.</span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-2">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Toggle Text Color</label>
                            <div class="col-sm-9">
                                <input type="text" name="whatsapp_toggle_text_color"
                                       value="<?= isset($whatsapp_toggle_text_color) ? $whatsapp_toggle_text_color : 'white'; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Toggle Background Color</label>
                            <div class="col-sm-9">
                                <input type="text" name="whatsapp_toggle_background_color"
                                       value="<?= isset($whatsapp_toggle_background_color) ? $whatsapp_toggle_background_color : 'rgb(37,211,102)'; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Toggle Title</label>
                            <div class="col-sm-9">
                                <input type="text" name="whatsapp_toggle_title"
                                       value="<?= isset($whatsapp_toggle_title) ? $whatsapp_toggle_title : 'Chat with us on WhatsApp'; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Container Text Color</label>
                            <div class="col-sm-9">
                                <input type="text" name="whatsapp_container_text_color"
                                       value="<?= isset($whatsapp_container_text_color) ? $whatsapp_container_text_color : 'rgb(85,85,85)'; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Container Background Color</label>
                            <div class="col-sm-9">
                                <input type="text" name="whatsapp_container_background_color"
                                       value="<?= isset($whatsapp_container_background_color) ? $whatsapp_container_background_color : 'white'; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Account Item Background Color on Hover</label>
                            <div class="col-sm-9">
                                <input type="text" name="whatsapp_account_background_hover"
                                       value="<?= isset($whatsapp_account_background_hover) ? $whatsapp_account_background_hover : 'rgb(240,240,240)'; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Toggle Position</label>
                            <div class="col-sm-9">
                                <label for="right-bottom">
                                    <input type="radio" id="right-bottom" name="whatsapp_toggle_position"
                                           value="right-bottom" <?= !isset($whatsapp_toggle_position) || $whatsapp_toggle_position == 'right-bottom' ? 'checked' : ''; ?>>
                                    Right Bottom
                                </label>
                                <br>
                                <label for="left-bottom">
                                    <input type="radio" id="left-bottom" name="whatsapp_toggle_position"
                                           value="left-bottom" <?= isset($whatsapp_toggle_position) && $whatsapp_toggle_position == 'left-bottom' ? 'checked' : ''; ?>>
                                    Left Bottom
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Toggle Icon Size</label>
                            <div class="col-sm-9">
                                <label for="toggle-icon-lg">
                                    <input type="radio" id="toggle-icon-lg" name="whatsapp_toggle_icon_size"
                                           value="toggle-icon-lg" <?= !isset($whatsapp_toggle_icon_size) || $whatsapp_toggle_icon_size == 'toggle-icon-lg' ? 'checked' : ''; ?>>
                                    Large
                                </label>
                                <br>
                                <label for="toggle-icon-md">
                                    <input type="radio" id="toggle-icon-md" name="whatsapp_toggle_icon_size"
                                           value="toggle-icon-md" <?= isset($whatsapp_toggle_icon_size) && $whatsapp_toggle_icon_size == 'toggle-icon-md' ? 'checked' : ''; ?>>
                                    Medium
                                </label>
                                <br>
                                <label for="toggle-icon-sm">
                                    <input type="radio" id="toggle-icon-sm" name="whatsapp_toggle_icon_size"
                                           value="toggle-icon-sm" <?= isset($whatsapp_toggle_icon_size) && $whatsapp_toggle_icon_size == 'toggle-icon-sm' ? 'checked' : ''; ?>>
                                    Small
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Toggle Size</label>
                            <div class="col-sm-9">
                                <label for="toggle-lg">
                                    <input type="radio" id="toggle-lg" name="whatsapp_toggle_size"
                                           value="toggle-lg" <?= !isset($whatsapp_toggle_size) || $whatsapp_toggle_size == 'toggle-lg' ? 'checked' : ''; ?>>
                                    Large
                                </label>
                                <br>
                                <label for="toggle-md">
                                    <input type="radio" id="toggle-md" name="whatsapp_toggle_size"
                                           value="toggle-md" <?= isset($whatsapp_toggle_size) && $whatsapp_toggle_size == 'toggle-md' ? 'checked' : ''; ?>>
                                    Medium
                                </label>
                                <br>
                                <label for="toggle-sm">
                                    <input type="radio" id="toggle-sm" name="whatsapp_toggle_size"
                                           value="toggle-sm" <?= isset($whatsapp_toggle_size) && $whatsapp_toggle_size == 'toggle-sm' ? 'checked' : ''; ?>>
                                    Small
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $this = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                $($this).attr('disabled', true);
                $($this).append('<span class="wait"> <i class="fa fa-refresh fa-spin"></i></span>');
            },
            complete: function () {
                $($this).attr('disabled', false);
                $('.wait').remove();
            },
            success: function (json) {
                $('.error').remove();
                $('.form-group').removeClass('has-error');
                if (json['redirect']) {
                    window.location = json['redirect'];
                } else if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').after('<span class="help-block error">' + json['errors'][i] + '</span>');
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                    }
                }
            }
        });
    });
</script>