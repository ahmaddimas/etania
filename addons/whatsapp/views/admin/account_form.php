<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $template['title'] ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="whatsapp_account_id" value="<?= $whatsapp_account_id ?>">
            <div class="form-group">
                <label class="control-label col-sm-3">Active</label>
                <div class="toggle lg col-sm-4">
                    <label style="margin-top:5px;">
                        <?php if ($whatsapp_account_status) { ?>
                            <input type="checkbox" name="whatsapp_account_status" value="1" checked="checked">
                        <?php } else { ?>
                            <input type="checkbox" name="whatsapp_account_status" value="1">
                        <?php } ?>
                        <span class="button-indecator"></span>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Foto Profil</label>
                <div class="col-md-3">
                    <img id="image" src="<?= $whatsapp_account_picture_src ?>" class="img-thumbnail">
                    <div class="caption" style="margin-top:10px;">
                        <button type="button" class="btn btn-default" id="upload">Upload</button>
                    </div>
                    <input type="hidden" name="whatsapp_account_picture" id="picture"
                           value="<?= $whatsapp_account_picture ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nama Akun <sup class="text-danger">*</sup></label>
                <div class="col-sm-9">
                    <input type="text" name="whatsapp_account_name" value="<?= $whatsapp_account_name ?>"
                           class="form-control" autocomplete="off" placeholder="Nama Akun" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Nomor Akun <sup class="text-danger">*</sup></label>
                <div class="col-sm-9">
                    <input type="text" name="whatsapp_account_number" value="<?= $whatsapp_account_number ?>"
                           class="form-control" maxlength="13" required>
                    <span class="text-muted">Full phone number in international format. Refer to <a
                                href="https://faq.whatsapp.com/en/general/21016748">https://faq.whatsapp.com/en/general/21016748</a> for a detailed information</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Status</label>
                <div class="col-sm-9">
                    <input type="text" name="whatsapp_account_title" value="<?= $whatsapp_account_title ?>"
                           class="form-control" maxlength="100">
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
            <button type="button" class="btn btn-success" id="submit"><i class="fa fa-check"></i> Simpan</button>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/js/ajaxupload.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    new AjaxUpload('#upload', {
        action: "<?=admin_url('whatsapp/account/upload?id=' . $whatsapp_account_id);?>",
        name: 'accountpicture',
        autoSubmit: false,
        responseType: 'json',
        onChange: function (file, extension) {
            this.submit();
        },
        onSubmit: function (file, extension) {
            $('#upload').append('<span class="wait"><i class="fa fa-refresh"></i></span>');
        },
        onComplete: function (file, json) {
            if (json.success) {
                $('.wait').remove();
                $('#image').attr('src', json.image);
                $('#picture').val(json.imageUrl);
            }
            if (json.error) {
                alert(json.error);
            }
            $('.loading').remove();
        }
    });

    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });
</script>