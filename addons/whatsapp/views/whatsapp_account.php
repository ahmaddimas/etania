<div class="account-item" data-target="$sendUrl">
    <div class="account-picture">
        <img src="$whatsapp_account_picture" alt="$whatsapp_account_name">
    </div>
    <div class="account-description">
        <span class="account-title">$whatsapp_account_title</span>
        <span class="account-name">$whatsapp_account_name</span>
        <span class="account-number">$whatsapp_account_number</span>
    </div>
</div>