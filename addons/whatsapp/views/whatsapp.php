<div class="whatsapp-toggle">
    <div class="toggle-icon $whatsapp_toggle_icon_size $whatsapp_toggle_position"
         style="background-color: $whatsapp_toggle_background_color; color: $whatsapp_toggle_text_color;">
        <i class="fa fa-whatsapp"></i>
    </div>
    <div class="toggle-content $whatsapp_toggle_size $whatsapp_toggle_position"
         style="background-color: $whatsapp_toggle_background_color; color: $whatsapp_toggle_text_color;">
        <div class="toggle-header">
            $whatsapp_toggle_title
        </div>
        <div class="toggle-body"
             style="background-color: $whatsapp_container_background_color; color: $whatsapp_container_text_color; border: 1px solid $whatsapp_toggle_background_color;"
             hover-color="$whatsapp_account_background_hover">
            $whatsapp_account_list
        </div>
        <div class="toggle-footer">
            <span data-toggle="close">Close&nbsp;<i class="fa fa-close"></i></span>
        </div>
    </div>
</div>