<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_account extends Admin_Controller
{
    private $setting = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_whatsapp');

        if (empty($this->setting['active'])) {
            show_404();
        }


        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('format');
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('*')
                ->from('whatsapp_account');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');
            $data['message'] = $this->session->userdata('message');

            $this->load
                ->title('Akun WhatsApp')
                ->js('/assets/js/bootstrap-typeahead.min.js')
                ->view('admin/account', $data);
        }
    }


    /**
     * Create new product
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Akun WhatsApp');

        $this->form();
    }

    /**
     * Edit existing product
     *
     * @access public
     * @param int $account_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Akun WhatsApp');

        $this->form($this->input->get('whatsapp_account_id'));
    }

    /**
     * Load product form
     *
     * @access private
     * @param int $account_id
     * @return void
     */
    private function form($whatsapp_account_id = null)
    {
        $this->load->model('whatsapp_account_model');

        $data['action'] = admin_url('whatsapp/account/validate');
        $data['whatsapp_account_id'] = null;
        $data['whatsapp_account_name'] = '';
        $data['whatsapp_account_number'] = null;
        $data['whatsapp_account_title'] = '';
        $data['whatsapp_account_status'] = 0;

        $this->load->library('image');

        $data['whatsapp_account_picture'] = $this->image->resize('no_image.jpg', 150, 150);
        $data['whatsapp_account_picture_src'] = $this->image->resize('no_image.jpg', 150, 150);

        if ($account = $this->whatsapp_account_model->get($whatsapp_account_id)) {
            $data['whatsapp_account_id'] = (int)$account['whatsapp_account_id'];
            $data['whatsapp_account_name'] = $account['whatsapp_account_name'];
            $data['whatsapp_account_number'] = (int)$account['whatsapp_account_number'];
            $data['whatsapp_account_title'] = $account['whatsapp_account_title'];
            $data['whatsapp_account_status'] = $account['whatsapp_account_status'];
            $data['whatsapp_account_picture'] = $account['whatsapp_account_picture'];
            $data['whatsapp_account_picture_src'] = $this->image->resize($account['whatsapp_account_picture'], 150, 150);
        }


        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/account_form', $data, true, true)
        )));
    }

    /**
     * Validate product form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $post = $this->input->post();

        $whatsapp_account_id = $post['whatsapp_account_id'];

        $this->form_validation
            ->set_rules('whatsapp_account_name', 'Nama Akun', 'trim|required')
            ->set_rules('whatsapp_account_number', 'Nomor Akun', 'trim|required|max_length[13]')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else if (!preg_match('/(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,13}$/', $post['whatsapp_account_number'])) {
            $json['error']['whatsapp_account_number'] = 'Phone number format is not international format';
        } else {

            if ($this->input->post('whatsapp_account_status') == null) {
                $post['whatsapp_account_status'] = 0;
            }

            $this->load->model('whatsapp_account_model');
            $account = $this->whatsapp_account_model->cek_akun($post['whatsapp_account_id']);

            if ($whatsapp_account_id) {
                $this->whatsapp_account_model->update($whatsapp_account_id, $post);

                $json['success'] = lang('success_update');
            } else {
                if (count($account) == 0) {
                    if ($this->whatsapp_account_model->insert($post)) {
                        $json['success'] = lang('success_create');
                    }
                } else {
                    $json['error']['product'] = "Product exist";
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Upload picture account
     *
     * @access public
     * @return json
     */
    public function upload()
    {
        $json = array();

        $upload_path = DIR_IMAGE . 'whatsapp';

        if (!file_exists($upload_path)) {
            @mkdir($upload_path, 0777);
        }

        $this->load->library('upload', array(
            'upload_path' => $upload_path,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size' => '3000',
            'encrypt_name' => true
        ));

        if (!$this->upload->do_upload('accountpicture')) {
            $json['error'] = $this->upload->display_errors('', '');
        } else {
            $this->load->library('image');
            $this->load->model('whatsapp_account_model');

            $upload_data = $this->upload->data();
            $image = substr($upload_data['full_path'], strlen(FCPATH . DIR_IMAGE));

            $thumb = $this->image->resize($image, 150, 150);
            $this->whatsapp_account_model->update($this->input->get('id'), array('whatsapp_account_picture' => $image));

            $json['image'] = $thumb;
            $json['imageUrl'] = $image;
            $json['success'] = 'File berhasil diupload!';
        }

        $this->output->set_output(json_encode($json));
    }


    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $whatsapp_account_id = $this->input->post('whatsapp_account_id');

        if (!$whatsapp_account_id) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('whatsapp_account_model');
            $this->whatsapp_account_model->delete($whatsapp_account_id);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }

}
	
