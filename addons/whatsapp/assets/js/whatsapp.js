$(document).ready(function () {
    $(document).on('click', '.whatsapp-toggle [data-toggle="close"]', function (e) {
        $(this).parents('.whatsapp-toggle').find('.toggle-content').addClass('content-hide').removeClass('content-show');
    });
    $(document).on('click', '.whatsapp-toggle .toggle-icon', function (e) {
        $(this).parents('.whatsapp-toggle').find('.toggle-content').addClass('content-show').removeClass('content-hide');
    });
    $(document).on('hover', '.whatsapp-toggle .account-item', function (e) {
        var color = $(this).parents('.toggle-body').attr('hover-color');
        $(this).css('background-color', color);
    }).on('mouseleave', '.whatsapp-toggle .account-item', function (e) {
        var color = $(this).parents('.toggle-body').css('background-color');
        $(this).css('background-color', color);
    }).on('click', '.whatsapp-toggle .account-item', function (e) {
        var url = $(this).attr('data-target');
        var win = window.open(url, '_blank');
        if (win) {
            win.focus();
        } else {
            alert('Please allow popups for this website');
        }
    });
});