<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Whatsapp
{
    private $CI;
    private $setting = array();

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('whatsapp/whatsapp_account_model', 'wam');
        $this->setting = $this->CI->config->item('addon_whatsapp');
    }

    public function install($class)
    {
        $whatsapp = $this->setting;

        $whatsapp['content'] = $this->createView();
        $assets_path = 'addons/whatsapp/assets/';

        $class->load->css($assets_path . 'css/style.css')->js($assets_path . 'js/whatsapp.js')->vars('whatsapp', $whatsapp);
    }

    public function createView()
    {
        $view = file_get_contents(__DIR__ . '/../views/whatsapp.php');
        $list_account = $this->generateAccountList();
        if (!empty($list_account))
            $this->setting['whatsapp_account_list'] = $list_account;
        else
            return '';

        foreach ($this->setting as $key => $value) {
            $view = str_replace('$' . $key, $value, $view);
        }

        return $view;
    }

    private function generateAccountList()
    {
        $this->CI->load->library('image');
        $accounts = $this->CI->wam->get_all();
        $views = '';

        foreach ($accounts as $account) {
            if ($account['whatsapp_account_status']) {
                $view = file_get_contents(__DIR__ . '/../views/whatsapp_account.php');
                foreach ($account as $key => $value) {
                    if ($key == 'whatsapp_account_picture') {
                        $value = $this->CI->image->resize($value, 150, 150);
                    }
                    $view = str_replace('$' . $key, $value, $view);
                }
                $view = str_replace('$sendUrl', $this->createUrl($account['whatsapp_account_number']), $view);
                $views .= $view;
            }
        }

        return $views;
    }

    private function createUrl($phone_number)
    {
        $text = isset($this->setting['whatsapp_auto_text']) ? $this->setting['whatsapp_auto_text'] : '';
        $url = 'https://web.whatsapp.com/send?phone=' . $phone_number;
        $url = empty($text) ? $url : $url . '&text=' . $text;

        if ($this->CI->agent->is_mobile()) {
            $url = 'whatsapp://send?phone=' . $phone_number;
            $url = empty($text) ? $url : $url . '&text=' . $text;
        }

        return $url;
    }
}
