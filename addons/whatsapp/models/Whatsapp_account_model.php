<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Whatsapp_account_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function cek_akun($whatsapp_account_id)
    {
        return $this->db
            ->where('whatsapp_account_id', $whatsapp_account_id)
            ->where('whatsapp_account_id <>', 0)
            ->get('whatsapp_account')
            ->result_array();
    }
}