<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Whatsapp_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add database
     *
     * @access public
     * @return void
     */
    public function install()
    {
        $sql[] = "CREATE TABLE `whatsapp_account` (
				  `whatsapp_account_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				  `whatsapp_account_name` varchar(100) NOT NULL,
				  `whatsapp_account_number` varchar(13) NOT NULL,
				  `whatsapp_account_title` varchar(255) NOT NULL,
				  `whatsapp_account_picture` text NOT NULL,
				  `whatsapp_account_status` int(1) NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

        $this->db->simple_query("SET SQL_MODE = 'NO_ZERO_IN_DATE'");

        foreach ($sql as $query) {
            $this->db->query($query);
        }
    }

    /**
     * Remove database
     *
     * @access public
     * @return void
     */
    public function uninstall()
    {
        $this->load->dbforge();

        $this->dbforge->drop_table('whatsapp_account', true);
    }

    /**
     * Get menus
     *
     * @access public
     * @return array
     */
    public function get_menus()
    {
        return array(
            array(
                'title' => 'Account',
                'url' => admin_url('whatsapp/account'),
            )
        );
    }
}
