<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_free_shipping_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function cek_category($category_id)
    {
        return $this->db
            ->where('category_id', $category_id)
            ->where('category_id <>', 0)
            ->get('product_free_shipping')
            ->result_array();
    }

    public function cek_product($product_id)
    {
        return $this->db
            ->where('product_id', $product_id)
            ->where('product_id <>', 0)
            ->get('product_free_shipping')
            ->result_array();
    }

}