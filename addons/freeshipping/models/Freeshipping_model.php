<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Freeshipping_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add database
     *
     * @access public
     * @return void
     */
    public function install()
    {
        $sql[] = "CREATE TABLE `product_free_shipping` ( 
			`product_free_shipping_id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
			`product_id` INT(11) NOT NULL , 
			`category_id` INT(11) NOT NULL DEFAULT '0'
			) ENGINE = InnoDB;";

        $this->db->simple_query("SET SQL_MODE = 'NO_ZERO_IN_DATE'");

        foreach ($sql as $query) {
            $this->db->query($query);
        }
    }

    /**
     * Remove database
     *
     * @access public
     * @return void
     */
    public function uninstall()
    {
        $this->load->dbforge();

        $this->dbforge->drop_table('product_free_shipping', true);
    }

    /**
     * Get menus
     *
     * @access public
     * @return array
     */
    public function get_menus()
    {
        return array(
            array(
                'title' => 'Product',
                'url' => admin_url('freeshipping/product'),
            ),
            array(
                'title' => 'Category',
                'url' => admin_url('freeshipping/category'),
            )
        );
    }
}