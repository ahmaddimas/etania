<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['freeshipping/admin/product(:any)?'] = 'admin_product$1';
$route['freeshipping/admin/category(:any)?'] = 'admin_category$1';
