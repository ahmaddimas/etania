<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $template['title'] ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="product_free_shipping_id" value="<?= $product_free_shipping_id ?>">
            <div class="form-group">
                <label class="col-sm-3 control-label">Kategori</label>
                <div class="col-sm-9">
                    <input type="text" name="category" value="<?= $category ?>" class="form-control" autocomplete="off"
                           placeholder="Auto complete...">
                    <input type="hidden" name="category_id" value="<?= $category_id ?>">
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
            <button type="button" class="btn btn-success" id="submit"><i class="fa fa-check"></i> Simpan</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('input[name=\'category\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('catalog/category/auto_complete')?>",
                data: 'filter_name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    categories = [];
                    map = {};
                    $.each(json, function (i, category) {
                        map[category.name] = category;
                        categories.push(category.name);
                    });
                    process(categories);
                }
            });
        },
        updater: function (item) {
            $('input[name=\'category_id\']').val(map[item].category_id);
            return item;
        },
        minLength: 1
    });

    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });
</script>