<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $template['title'] ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="product_free_shipping_id" value="<?= $product_free_shipping_id ?>">
            <div class="form-group">
                <label class="col-sm-3 control-label">Produk</label>
                <div class="col-sm-9">
                    <input type="text" name="product" value="<?= $product ?>" class="form-control" autocomplete="off"
                           placeholder="Auto complete...">
                    <input type="hidden" name="product_id" value="<?= $product_id ?>">
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
            <button type="button" class="btn btn-success" id="submit"><i class="fa fa-check"></i> Simpan</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('input[name=\'product\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('catalog/product/auto_complete')?>",
                data: 'filter_name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    categories = [];
                    map = {};
                    $.each(json, function (i, discount) {
                        map[discount.name] = discount;
                        categories.push(discount.name);
                    });
                    process(categories);
                }
            });
        },
        updater: function (item) {
            $('input[name=\'product_id\']').val(map[item].product_id);
            return item;
        },
        minLength: 1
    });

    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });
</script>