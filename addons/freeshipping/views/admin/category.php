<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a onclick="getForm(null, 'freeshipping/category/create');" class="btn btn-success"><i
                    class="fa fa-plus-circle"></i> <?= lang('button_add') ?></a>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> <?= lang('button_refresh') ?>
        </a>
        <a onclick="delRecord();" class="btn btn-danger dropdown-toggle"><i
                    class="fa fa-trash"></i> <?= lang('button_delete') ?></a>
    </div>
</div>
<section class="content">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="message"></div>
                    <?php if ($success) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="icon fa fa-check"></i> <?= $success ?></div>
                    <?php } ?>
                    <?php if ($message) { ?>
                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button><?= $message ?></div>
                    <?php } ?>
                    <?php if ($error) { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="icon fa fa-ban"></i> <?= $error ?></div>
                    <?php } ?>
                    <?= form_open(admin_url('freeshipping/category/delete'), 'id="bulk-action"') ?>
                    <table class="table table-hover table-bordered" id="datatable" width="100%">
                        <thead>
                        <tr>
                            <th style="width:5%"><input type="checkbox" id="select-all"></th>
                            <th style="width:15%">Category ID</th>
                            <th>Category Name</th>
                            <th style="width:15%" class="text-right"></th>
                        </tr>
                        </thead>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="modal"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('freeshipping/category')?>",
                "type": "POST",
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "product_free_shipping_id"},
                {"data": "category_id"},
                {"data": "category"},
                {"orderable": false, "searchable": false, "data": "product_free_shipping_id"},
            ],
            "sDom": 'lfr<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<input type="checkbox" class="checkbox" name="product_free_shipping_id[]" value="' + data.product_free_shipping_id + '"/>');
                $('td', row).eq(3).html('<a style="cursor:pointer;" onclick="getForm(' + data.product_free_shipping_id + ', \'freeshipping/category/edit\');"><i class="fa fa-cog"></i> <?=lang('button_edit')?></a>').addClass('text-right');
            },
            "order": [[1, 'asc']],
        });

        $('#select-all').click(function () {
            var checkBoxes = $('.checkbox');
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });
    });

    function delRecord() {
        swal({
            title: '<?=lang('text_warning')?>',
            text: '<?=lang('text_confirm_delete')?>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('freeshipping/category/delete')?>',
                    type: 'post',
                    data: $('#bulk-action').serialize(),
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
        $('#select-all').prop('checked', false);
    }

    function getForm(product_free_shipping_id, path) {
        $.ajax({
            url: $('base').attr('href') + path,
            data: 'product_free_shipping_id=' + product_free_shipping_id,
            dataType: 'json',
            success: function (json) {
                if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
                } else if (json['content']) {
                    $('#modal').html('<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['content'] + '</div>');
                    $('#form-modal').modal('show');
                    $('#form-modal').on('hidden.bs.modal', function (e) {
                        refreshTable();
                    });
                }
            }
        });
    }
</script>