<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_category extends Admin_Controller
{
    private $setting = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_freeshipping');

        if (empty($this->setting['active'])) {
            show_404();
        }
        $this->load->library('currency');

        $this->load->library('form_validation');
        $this->load->helper('form');


    }


    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('f.product_free_shipping_id, c.category_id, c.name as category')
                ->join('category c', 'c.category_id = f.category_id', 'left')
                ->where('f.product_id', 0)
                ->from('product_free_shipping f');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');
            $data['message'] = $this->session->userdata('message');

            $this->load
                ->title('Category Free Shipping')
                ->js('/assets/js/bootstrap-typeahead.min.js')
                ->view('admin/category', $data);
        }
    }

    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Kategori Free Shipping');

        $this->form();
    }


    /**
     * Edit existing category
     *
     * @access public
     * @param int $offers_category_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Kategori Free Shipping');

        $this->form($this->input->get('product_free_shipping_id'));
    }

    /**
     * Load category form
     *
     * @access private
     * @param int $offers_category_id
     * @return void
     */
    private function form($product_free_shipping_id = null)
    {
        $this->load->model('product_free_shipping_model');

        $data['action'] = admin_url('freeshipping/category/validate');
        $data['product_free_shipping_id'] = null;
        $data['category_id'] = null;
        $data['category'] = '';

        if ($freeshipping = $this->product_free_shipping_model->get($product_free_shipping_id)) {
            $data['product_free_shipping_id'] = (int)$freeshipping['product_free_shipping_id'];
            $data['category_id'] = (int)$freeshipping['category_id'];

            $this->load->model('catalog/category_model');

            if ($category = $this->category_model->get($freeshipping['category_id'])) {
                $data['category'] = $category['name'];
            } else {
                $data['category'] = '';
            }
        }


        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/category_form', $data, true, true)
        )));
    }

    /**
     * Validate category form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $product_free_shipping_id = $this->input->post('product_free_shipping_id');

        $this->form_validation
            ->set_rules('category', 'Kategori', 'required')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $post = $this->input->post();

            $this->load->model('product_free_shipping_model');
            $category = $this->product_free_shipping_model->cek_category($post['category_id']);

            if ($product_free_shipping_id) {
                $this->product_free_shipping_model->update($product_free_shipping_id, $post);

                $json['success'] = lang('success_update');
            } else {
                if (count($category) == 0) {
                    if ($this->product_free_shipping_model->insert($post)) {
                        $json['success'] = lang('success_create');
                    }
                } else {
                    $json['error']['category'] = "Kategori exist";
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $product_free_shipping_id = $this->input->post('product_free_shipping_id');

        if (!$product_free_shipping_id) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('product_free_shipping_model');
            $this->product_free_shipping_model->delete($product_free_shipping_id);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
} 