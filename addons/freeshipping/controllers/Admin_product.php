<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_product extends Admin_Controller
{
    private $setting = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_freeshipping');

        if (empty($this->setting['active'])) {
            show_404();
        }


        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('format');
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('f.product_free_shipping_id, p.type, p.product_id, p.name, p.active, p.model, p.sku, c.name as category, p.quantity')
                ->join('product p', 'p.product_id = f.product_id', 'left')
                ->join('category c', 'c.category_id = p.category_id', 'left')
                ->where('f.category_id', 0)
                ->from('product_free_shipping f')
                ->edit_column('price', '$1', 'format_money(price)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');
            $data['message'] = $this->session->userdata('message');

            $this->load
                ->title('Produk Free Shipping')
                ->js('/assets/js/bootstrap-typeahead.min.js')
                ->view('admin/product', $data);
        }
    }


    /**
     * Create new product
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Product Free Shipping');

        $this->form();
    }

    /**
     * Edit existing product
     *
     * @access public
     * @param int $product_free_shipping_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Product Free Shipping');

        $this->form($this->input->get('product_free_shipping_id'));
    }

    /**
     * Load product form
     *
     * @access private
     * @param int $product_free_shipping_id
     * @return void
     */
    private function form($product_free_shipping_id = null)
    {
        $this->load->model('product_free_shipping_model');

        $data['action'] = admin_url('freeshipping/product/validate');
        $data['product_free_shipping_id'] = null;
        $data['product'] = '';
        $data['product_id'] = null;

        if ($freeshipping = $this->product_free_shipping_model->get($product_free_shipping_id)) {
            $data['product_free_shipping_id'] = (int)$freeshipping['product_free_shipping_id'];
            $data['product_id'] = (int)$freeshipping['product_id'];

            $this->load->model('catalog/product_model');

            if ($product = $this->product_model->get($freeshipping['product_id'])) {
                $data['product'] = $product['name'];
            } else {
                $data['product'] = '';
            }

        }


        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/product_form', $data, true, true)
        )));
    }

    /**
     * Validate product form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $product_free_shipping_id = $this->input->post('product_free_shipping_id');

        $this->form_validation
            ->set_rules('product', 'Produk', 'trim|required|min_length[3]')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $post = $this->input->post();

            $this->load->model('product_free_shipping_model');
            $product = $this->product_free_shipping_model->cek_product($post['product_id']);

            if ($product_free_shipping_id) {
                $this->product_free_shipping_model->update($product_free_shipping_id, $post);

                $json['success'] = lang('success_update');
            } else {
                if (count($product) == 0) {
                    if ($this->product_free_shipping_model->insert($post)) {
                        $json['success'] = lang('success_create');
                    }
                } else {
                    $json['error']['product'] = "Product exist";
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }


    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $product_free_shipping_ids = $this->input->post('product_free_shipping_id');

        if (!$product_free_shipping_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('product_free_shipping_model');
            $this->product_free_shipping_model->delete($product_free_shipping_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }

} 