<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Freeshipping extends Front_Controller
{
    private $setting = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_freeshipping');
    }


}