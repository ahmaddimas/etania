<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['custom/admin'] = 'Pengaturan Rakit PC';
$lang['custom/admin_category'] = 'Kategori Part';
$lang['custom/admin_part'] = 'Part Rakitan';
$lang['custom/admin_order'] = 'Pesanan Rakitan';