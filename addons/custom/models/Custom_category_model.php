<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom_category_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function get_categories()
    {
        return $this->db
            ->order_by('sort_order')
            ->get('custom_category')
            ->result_array();
    }
}