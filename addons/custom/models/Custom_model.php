<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add database
     *
     * @access public
     * @return void
     */
    public function install()
    {
        $sql[] = "CREATE TABLE IF NOT EXISTS `custom_category` (
			`custom_category_id` int(11) NOT NULL AUTO_INCREMENT,
			`name` varchar(255) NOT NULL DEFAULT '',
			`sort_order` int(11) NOT NULL DEFAULT '0',
			`required` tinyint(1) NOT NULL DEFAULT '0',
			PRIMARY KEY (`custom_category_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";

        $sql[] = "CREATE TABLE IF NOT EXISTS `custom_part` (
			`custom_part_id` int(11) NOT NULL AUTO_INCREMENT,
			`custom_category_id` int(11) NOT NULL DEFAULT '0',
			`product_id` int(11) NOT NULL DEFAULT '0',
			`price` decimal(15,2) NOT NULL DEFAULT '0.00',
			`active` tinyint(1) NOT NULL DEFAULT '0',
			PRIMARY KEY (`custom_part_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";

        $sql[] = "CREATE TABLE IF NOT EXISTS `custom_part_pair` (
			`product_id` int(11) NOT NULL,
			`pair_id` int(11) NOT NULL,
			PRIMARY KEY (`product_id`,`pair_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        foreach ($sql as $query) {
            $this->db->query($query);
        }
    }

    /**
     * Remove database
     *
     * @access public
     * @return void
     */
    public function uninstall()
    {
        $this->load->dbforge();

        $this->dbforge->drop_table('custom_category', true);
        $this->dbforge->drop_table('custom_part', true);
        $this->dbforge->drop_table('custom_part_pair', true);
    }

    /**
     * Get menus
     *
     * @access public
     * @return array
     */
    public function get_menus()
    {
        return array(
            array(
                'title' => 'Kategori Part',
                'url' => admin_url('custom/category'),
            ),
            array(
                'title' => 'Part',
                'url' => admin_url('custom/part'),
            )
        );
    }
}