<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom_part_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get price
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_price($product_id = null)
    {
        return $this->db
            ->select('price')
            ->where('product_id', (int)$product_id)
            ->where('active', 1)
            ->limit(1)
            ->get('custom_part')
            ->row_array();
    }

    /**
     * Insert new part
     *
     * @access public
     * @param array $data
     * @return int
     */
    public function insert($data = array())
    {
        $custom_part_id = parent::insert($data);

        if (isset($data['part_pair'])) $this->set_part_pairs($data['product_id'], $data['part_pair']);

        return $custom_part_id;
    }

    /**
     * Update part
     *
     * @access public
     * @param int $custom_part_id
     * @param array $data
     * @return void
     */
    public function update($custom_part_id = null, $data = array())
    {
        $custom_part_id = parent::update($custom_part_id, $data);

        $this->db
            ->where('product_id', (int)$data['product_id'])
            ->delete('custom_part_pair');

        if (isset($data['part_pair'])) $this->set_part_pairs($data['product_id'], $data['part_pair']);
    }

    /**
     * Get part
     *
     * @access public
     * @param int $custom_part_id
     * @return array
     */
    public function get($custom_part_id = null)
    {
        return $this->db
            ->select('cp.*, p.name as product')
            ->from('custom_part cp')
            ->join('product p', 'p.product_id = cp.product_id', 'left')
            ->where('custom_part_id', (int)$custom_part_id)
            ->get()
            ->row_array();
    }

    /**
     * Get products
     *
     * @access public
     * @param array $data
     * @return array
     */
    public function get_products($data = array())
    {
        $product_ids = array(0);

        $results = $this->db
            ->select('product_id')
            ->group_by('product_id')
            ->get('custom_part')
            ->result_array();

        foreach ($results as $row) {
            $product_ids[] = $row['product_id'];
        }

        return $this->db
            ->select('product_id, name')
            ->where_not_in('product_id', $product_ids)
            ->get('product')
            ->result_array();
    }

    /**
     * Get parts
     *
     * @access public
     * @param array $data
     * @return array
     */
    public function get_parts($data = array())
    {
        $this->db->select('p.product_id, p.name, cp.price');
        $this->db->from('custom_part cp');
        $this->db->join('product p', 'p.product_id = cp.product_id', 'left');

        if (isset($data['exclude_id'])) {
            $this->db->where('cp.product_id !=', (int)$data['exclude_id']);
        }

        if (isset($data['custom_category_id'])) {
            $this->db->where('cp.custom_category_id', (int)$data['custom_category_id']);
        }

        if (isset($data['filter_name'])) {
            $implode = array();

            $words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

            foreach ($words as $word) {
                $this->db->like('p.name', $word, 'both');
            }
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        return $this->db->get()->result_array();
    }

    /**
     * Get part pairs
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_part_pairs($product_id = null)
    {
        $pairs = array();

        foreach ($this->db
                     ->select('pair_id')
                     ->where('product_id', (int)$product_id)
                     ->get('custom_part_pair')
                     ->result_array() as $result) {
            $pairs[] = $result['pair_id'];
        }

        return $pairs;
    }

    /**
     * Set part pairs
     *
     * @access public
     * @param int $product_id
     * @param array $pairs
     * @return void
     */
    public function set_part_pairs($product_id, $pairs = array())
    {
        foreach ($pairs as $pair_id) {
            $this->db
                ->set(array(
                    'product_id' => (int)$product_id,
                    'pair_id' => (int)$pair_id
                ))->insert('custom_part_pair');
        }
    }

    /**
     * Get pairs
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_pairs($product_id)
    {
        return $this->db
            ->select('p.product_id, p.name')
            ->from('custom_part_pair cpp')
            ->join('product p', 'p.product_id = cpp.pair_id', 'left')
            ->where('cpp.product_id', $product_id)
            ->get()
            ->result_array();
    }

    /**
     * Delete
     *
     * @access public
     * @param mixed $custom_part_id
     * @return void
     */
    public function delete($custom_part_id = null)
    {
        if (is_array($custom_part_id)) {
            foreach ($custom_part_id as $id) {
                $this->delete($id);
            }
        } else {
            if ($custom_part = $this->get($custom_part_id)) {
                $this->db
                    ->where('product_id', (int)$custom_part['product_id'])
                    ->delete(array('custom_part', 'custom_part_pair'));

                $this->db
                    ->where('pair_id', (int)$custom_part['product_id'])
                    ->delete('custom_part_pair');
            }
        }
    }
}