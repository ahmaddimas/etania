<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $template['title'] ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="custom_part_id" value="<?= $custom_part_id ?>">
            <div class="form-group">
                <label class="col-sm-3 control-label">Nama</label>
                <div class="col-sm-9">
                    <input type="hidden" name="product_id" value="<?= $product_id ?>">
                    <input type="text" name="product" value="<?= $product ?>" class="form-control" autocomplete="off"
                           placeholder="Ketikkan nama produk (autocomplete)...">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Kategori Part</label>
                <div class="col-sm-9">
                    <select class="form-control" name="custom_category_id">
                        <?php foreach ($custom_categories as $custom_category) { ?>
                            <?php if ($custom_category_id == $custom_category['custom_category_id']) { ?>
                                <option value="<?= $custom_category['custom_category_id'] ?>"
                                        selected="selected"><?= $custom_category['name'] ?></option>
                            <?php } else { ?>
                                <option value="<?= $custom_category['custom_category_id'] ?>"><?= $custom_category['name'] ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Harga Part</label>
                <div class="col-sm-9">
                    <input type="text" name="price" value="<?= $price ?>" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Part Pasangan</label>
                <div class="col-sm-9">
                    <input type="text" name="part" value="" class="form-control" autocomplete="off"
                           placeholder="Ketikkan nama produk...">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="controls"
                         style="border: 1px solid #ddd; height: 150px; background: #fff; overflow-y: scroll;">
                        <table class="table table-striped" width="100%">
                            <tbody id="part-pair">
                            <?php foreach ($custom_pairs as $pair) { ?>
                                <tr id="part-pair<?= $pair['product_id'] ?>">
                                    <td><?= $pair['name'] ?><input type="hidden" name="part_pair[]"
                                                                   value="<?= $pair['product_id'] ?>"></td>
                                    <td class="text-right"><a class="btn btn-danger btn-xs"
                                                              onclick="$('#part-pair<?= $pair['product_id'] ?>').remove();"><i
                                                    class="fa fa-remove"></i></a></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Status</label>
                <div class="toggle lg col-sm-4">
                    <label style="margin-top:5px;">
                        <?php if ($active) { ?>
                            <input type="checkbox" name="active" value="1" checked="checked">
                        <?php } else { ?>
                            <input type="checkbox" name="active" value="1">
                        <?php } ?>
                        <span class="button-indecator"></span>
                    </label>
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
            <button type="button" class="btn btn-success" id="submit"><i class="fa fa-check"></i> Simpan</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });

    $('input[name=\'part\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('custom/part/get_parts')?>",
                data: 'filter_name=' + query + '&product_id=' + $('input[name=\'product_id\']').val(),
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    products = [];
                    map = {};
                    $.each(json, function (i, product) {
                        map[product.name] = product;
                        products.push(product.name);
                    });
                    process(products);
                }
            });
        },
        updater: function (item) {
            $('#part-pair' + map[item].product_id).remove();
            $('#part-pair').append('<tr id="part-pair' + map[item].product_id + '"><td>' + item + '<input type="hidden" name="part_pair[]" value="' + map[item].product_id + '" /></td><td class="text-right"><a class="btn btn-danger btn-xs btn-flat" onclick="$(\'#part-pair' + map[item].product_id + '\').remove();"><i class="fa fa-remove "></i></a></td></tr>');
        },
        minLength: 1
    });

    $('input[name=\'product\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('custom/part/get_products')?>",
                data: 'filter_name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    products = [];
                    map = {};
                    $.each(json, function (i, product) {
                        map[product.name] = product;
                        products.push(product.name);
                    });
                    process(products);
                }
            });
        },
        updater: function (item) {
            $('input[name=\'product_id\']').val(map[item].product_id);
            return item;
        },
        minLength: 1
    });
</script>