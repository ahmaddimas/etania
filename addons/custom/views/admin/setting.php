<div class="page-title">
    <div>
        <h1><?= $heading_title ?></h1>
        <p><?= $heading_subtitle ?></p>
    </div>
    <div>
        <a href="<?= admin_url('system/addons') ?>" class="btn btn-default"><i class="fa fa-lg fa-reply"></i>
            Kembali</a>
        <button id="submit" class="btn btn-success"><i class="fa fa-lg fa-check"></i> Simpan</button>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?= form_open($action, 'id="form" class="form-horizontal"') ?>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab" id="default">Umum</a></li>
                    <li><a href="#tab-2" data-toggle="tab">Tingkat Lanjut</a></li>
                </ul>
                <div class="tab-content" style="padding-top:20px;">
                    <div class="tab-pane active" id="tab-1">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Judul</label>
                            <div class="col-sm-9">
                                <input type="text" name="title" value="<?= $title ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Sub Judul</label>
                            <div class="col-sm-9">
                                <input type="text" name="subtitle" value="<?= $subtitle ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Enable</label>
                            <div class="toggle lg col-sm-4">
                                <label style="margin-top:5px;">
                                    <?php if ($active) { ?>
                                        <input type="checkbox" name="active" value="1" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="active" value="1">
                                    <?php } ?>
                                    <span class="button-indecator"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-2">
                        <div class="alert alert-info">Pengaturan tingkat lanjut tidak tersedia untuk versi ini</div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $this = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                $($this).attr('disabled', true);
                $($this).append('<span class="wait"> <i class="fa fa-refresh fa-spin"></i></span>');
            },
            complete: function () {
                $($this).attr('disabled', false);
                $('.wait').remove();
            },
            success: function (json) {
                $('.error').remove();
                $('.form-group').removeClass('has-error');
                if (json['redirect']) {
                    window.location = json['redirect'];
                } else if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').after('<span class="help-block error">' + json['errors'][i] + '</span>');
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                    }
                }
            }
        });
    });
</script>