<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['custom/admin/category(:any)?'] = 'admin_category$1';
$route['custom/admin/part(:any)?'] = 'admin_part$1';