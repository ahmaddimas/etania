<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom extends Front_Controller
{
    private $setting = array();

    /**
     * Construction
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_custom');

        if (empty($this->setting['active'])) {
            show_404();
        }

        $this->load->helper('form');
    }

    /**
     * Module setting
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $data['heading_title'] = $this->setting['title'];
        $data['heading_subtitle'] = $this->setting['subtitle'];

        $this->load->model('custom_category_model');
        $this->load->model('custom_part_model');
        $data['categories'] = array();

        foreach ($this->custom_category_model->get_categories() as $category) {
            $parts = $this->custom_part_model->get_parts(array('custom_category_id' => $category['custom_category_id']));
            $data['categories'][] = array(
                'custom_category_id' => $category['custom_category_id'],
                'name' => $category['name'],
                'parts' => $parts
            );
        }

        $this->load
            ->title($this->setting['title'])
            ->breadcrumb(lang('text_home'), site_url())
            ->breadcrumb($this->setting['title'], site_url('custom'))
            ->view('custom', $data);
    }

    public function validate()
    {
        $json = array();

        $this->load->model('custom_category_model');
        $this->load->model('custom_part_model');

        $parts = $this->input->post('parts') ? $this->input->post('parts') : array();

        foreach ($parts as $key => $value) {
            if ($custom_category = $this->custom_category_model->get($key)) {
                if ($custom_category['required'] && $value == 0) {
                    $json['errors'][$key] = 'Silakan pilih part!';
                } else {
                    if ($pairs = $this->custom_part_model->get_part_pairs($value)) {
                        foreach ($pairs as $pair) {
                            if (!in_array($pair, $parts)) {
                                $json['errors'][$key] = 'Item ini tidak kompatible!';
                            }
                        }
                    }
                }
            } else {
                $json['errors'][$key] = 'Kategori ini tidak valid!';
            }
        }

        if (empty($json)) {
            if ($this->input->post('submit')) {
                $this->load->library('shopping_cart');
                $this->shopping_cart->clear();

                $parts = $this->input->post('parts');

                foreach ($parts as $product_id) {
                    $this->shopping_cart->add($product_id, 1);
                }

                $this->session->set_userdata('comment', 'Pembelian PC Rakitan');

                $json['redirect'] = site_url('checkout');
            } else {
                $json['success'] = true;
            }
        }

        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($json));
    }
}