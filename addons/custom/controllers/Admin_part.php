<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_part extends Admin_Controller
{
    private $setting = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_custom');

        if (empty($this->setting['active'])) {
            show_404();
        }

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('format');

        $this->load->model('custom_part_model');
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('cp.custom_part_id, p.name, cc.name as category, cp.price, cp.active')
                ->join('product p', 'p.product_id = cp.product_id', 'left')
                ->join('custom_category cc', 'cp.custom_category_id = cc.custom_category_id', 'left')
                ->from('custom_part cp')
                ->edit_column('price', '$1', 'format_money(price)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load
                ->js('/assets/js/bootstrap-typeahead.min.js')
                ->title('Part')
                ->view('admin/custom_part');
        }
    }

    /**
     * Create new stock status
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Part');

        $this->form();
    }

    /**
     * Edit existing stock status
     *
     * @access public
     * @param int $custom_part_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Part');

        $this->form($this->input->get('custom_part_id'));
    }

    /**
     * Load stock status form
     *
     * @access private
     * @param int $custom_part_id
     * @return void
     */
    private function form($custom_part_id = null)
    {
        $data['custom_part_id'] = null;
        $data['custom_category_id'] = null;
        $data['product_id'] = null;
        $data['active'] = null;
        $data['product'] = '';
        $data['price'] = '';
        $data['custom_pairs'] = array();

        if ($custom_part = $this->custom_part_model->get($custom_part_id)) {
            $data = $custom_part;
            $data['custom_pairs'] = $this->custom_part_model->get_pairs($custom_part['product_id']);
        }

        $data['action'] = admin_url('custom/part/validate');

        $this->load->model('custom_category_model');
        $data['custom_categories'] = $this->custom_category_model->get_all();

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/custom_part_form', $data, true, true)
        )));
    }

    /**
     * Validate stock status form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $custom_part_id = $this->input->post('custom_part_id');

        $this->form_validation
            ->set_rules('product_id', 'Nama Produk', 'trim|required')
            ->set_rules('price', 'Harga', 'trim|required|numeric')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $data = $this->input->post(null, true);
            $data['active'] = (int)$this->input->post('active');

            $this->load->model('custom_part_model');

            if ($custom_part_id) {
                $this->custom_part_model->update($custom_part_id, $data);

                $json['success'] = lang('success_update');
            } else {
                if ($this->custom_part_model->insert($data)) {
                    $json['success'] = lang('success_create');
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $custom_part_ids = $this->input->post('custom_part_id');

        if (!$custom_part_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('custom_part_model');
            $this->custom_part_model->delete($custom_part_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Get products
     *
     * @access public
     * @return json
     */
    public function get_products()
    {
        $json = array();

        if ($this->input->get('filter_name')) {
            $params = array(
                'filter_name' => $this->input->get('filter_name'),
                'start' => 0,
                'limit' => 20
            );

            foreach ($this->custom_part_model->get_products($params) as $product) {
                $json[] = array(
                    'product_id' => $product['product_id'],
                    'name' => strip_tags(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->output->set_output(json_encode($json));
    }

    /**
     * Get parts
     *
     * @access public
     * @return json
     */
    public function get_parts()
    {
        $json = array();

        if ($this->input->get('filter_name')) {
            $params = array(
                'filter_name' => $this->input->get('filter_name'),
                'start' => 0,
                'limit' => 20,
                'exclude_id' => $this->input->get('product_id')
            );

            foreach ($this->custom_part_model->get_parts($params) as $product) {
                $json[] = array(
                    'product_id' => $product['product_id'],
                    'name' => strip_tags(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->output->set_output(json_encode($json));
    }
} 