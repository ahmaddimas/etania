<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_category extends Admin_Controller
{
    private $setting = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setting = $this->config->item('addon_custom');

        if (empty($this->setting['active'])) {
            show_404();
        }

        $this->load->library('form_validation');
        $this->load->helper('form');
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('custom_category_id, name, sort_order')
                ->from('custom_category');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load
                ->title('Kategori Part')
                ->view('admin/custom_category');
        }
    }

    /**
     * Create new category
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Kategori Part');

        $this->form();
    }

    /**
     * Edit existing category
     *
     * @access public
     * @param int $custom_category_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Kategori Part');

        $this->form($this->input->get('custom_category_id'));
    }

    /**
     * Load category form
     *
     * @access private
     * @param int $custom_category_id
     * @return void
     */
    private function form($custom_category_id = null)
    {
        $this->load->model('custom_category_model');

        $data['action'] = admin_url('custom/category/validate');
        $data['custom_category_id'] = null;
        $data['name'] = '';
        $data['required'] = 0;
        $data['sort_order'] = 0;

        if ($custom_category = $this->custom_category_model->get($custom_category_id)) {
            $data['custom_category_id'] = (int)$custom_category['custom_category_id'];
            $data['name'] = $custom_category['name'];
            $data['required'] = $custom_category['required'];
            $data['sort_order'] = $custom_category['sort_order'];
        }

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/custom_category_form', $data, true, true)
        )));
    }

    /**
     * Validate category form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $custom_category_id = $this->input->post('custom_category_id');

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required|min_length[3]|max_length[32]')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $data['name'] = $this->input->post('name');
            $data['required'] = (bool)$this->input->post('required');
            $data['sort_order'] = $this->input->post('sort_order');

            $this->load->model('custom_category_model');

            if ($custom_category_id) {
                $this->custom_category_model->update($custom_category_id, $data);

                $json['success'] = lang('success_update');
            } else {
                if ($this->custom_category_model->insert($data)) {
                    $json['success'] = lang('success_create');
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $custom_category_ids = $this->input->post('custom_category_id');

        if (!$custom_category_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('custom_category_model');
            $this->custom_category_model->delete($custom_category_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
} 