<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller
{
    /**
     * Construction
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->config('custom/manifest');
    }

    /**
     * Module setting
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401();
        }

        $data['active'] = false;
        $data['title'] = '';
        $data['subtitle'] = '';

        $settings = $this->setting_model->get_settings();

        if (isset($settings['addon_custom'])) {
            foreach ($settings['addon_custom'] as $key => $value) {
                $data[$key] = $value;
            }
        }

        $data['heading_title'] = $this->config->item('name');
        $data['heading_subtitle'] = 'Pengaturan modul ' . $this->config->item('name') . ' versi ' . $this->config->item('version');

        $data['action'] = admin_url('custom/validate');

        $this->load
            ->view('admin/setting', $data);
    }

    /**
     * Validate configuration
     *
     * @access public
     * @return void
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $this->form_validation->set_rules('title', 'Judul', 'trim|required');
        $this->form_validation->set_rules('subtitle', 'Sub Judul', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['errors'][$field] = $error;
            }
        } else {
            $post = $this->input->post();

            if ($this->input->post('active') == null) {
                $post['active'] = 0;
            }

            $this->setting_model->edit_setting('addon_custom', $post);
            $this->session->set_flashdata('success', 'Pengaturan modul ' . $this->config->item('name') . ' berhasil disimpan');
            $json['redirect'] = admin_url('system/addons');
        }

        $this->output->set_output(json_encode($json));
    }
}