$(function () {
    "use strict";

    $("[data-toggle='tooltip']").tooltip();

    $("[data-toggle='utility-menu']").click(function () {
        $(this).next().slideToggle(300);
        $(this).toggleClass('open');
        return false;
    });

    if ($('body').hasClass('fixed') || $('body').hasClass('only-sidebar')) {
        $('.sidebar').slimScroll({
            height: ($(window).height() - $(".main-header").height()) + "px",
            color: "rgba(0,0,0,0.8)",
            size: "3px"
        });
    } else {
        var docHeight = $(document).height();
        $('.main-sidebar').height(docHeight);
    }
});

$.pushMenu = {
    activate: function (toggleBtn) {

        $(toggleBtn).on('click', function (e) {
            e.preventDefault();

            if ($(window).width() > (767)) {
                if ($("body").hasClass('sidebar-collapse')) {
                    $("body").removeClass('sidebar-collapse').trigger('expanded.pushMenu');
                } else {
                    $("body").addClass('sidebar-collapse').trigger('collapsed.pushMenu');
                }
            } else {
                if ($("body").hasClass('sidebar-open')) {
                    $("body").removeClass('sidebar-open').removeClass('sidebar-collapse').trigger('collapsed.pushMenu');
                } else {
                    $("body").addClass('sidebar-open').trigger('expanded.pushMenu');
                }
            }
            if ($('body').hasClass('fixed') && $('body').hasClass('sidebar-mini') && $('body').hasClass('sidebar-collapse')) {
                $('.sidebar').css("overflow", "visible");
                $('.main-sidebar').find(".slimScrollDiv").css("overflow", "visible");
            }
            if ($('body').hasClass('only-sidebar')) {
                $('.sidebar').css("overflow", "visible");
                $('.main-sidebar').find(".slimScrollDiv").css("overflow", "visible");
            }
        });

        $(".content-wrapper").click(function () {
            if ($(window).width() <= (767) && $("body").hasClass("sidebar-open")) {
                $("body").removeClass('sidebar-open');
            }
        });
    }
};

$.tree = function (menu) {
    var _this = this;
    var animationSpeed = 200;

    $(document).on('click', menu + ' li a', function (e) {
        var $this = $(this);
        var checkElement = $this.next();

        if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible'))) {
            checkElement.slideUp(animationSpeed, function () {
                checkElement.removeClass('menu-open');
            });
            checkElement.parent("li").removeClass("active");
        } else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
            var parent = $this.parents('ul').first();
            var ul = parent.find('ul:visible').slideUp(animationSpeed);
            ul.removeClass('menu-open');
            var parent_li = $this.parent("li");
            checkElement.slideDown(animationSpeed, function () {
                checkElement.addClass('menu-open');
                parent.find('li.active').removeClass('active');
                parent_li.addClass('active');
            });
        }

        if (checkElement.is('.treeview-menu')) {
            e.preventDefault();
        }
    });
};

$.tree('.sidebar');
$.pushMenu.activate("[data-toggle='offcanvas']");

function loginFlip() {
    $('.login-box').toggleClass('flipped');
}

$.fn.loadingBtn = function (options) {
    var settings = $.extend({
        text: "Loading"
    }, options);
    this.html('<span class="btn-spinner"></span> ' + settings.text + '');
    this.addClass("disabled");
};

$.fn.loadingBtnComplete = function (options) {
    var settings = $.extend({
        html: "submit"
    }, options);
    this.html(settings.html);
    this.removeClass("disabled");
};

var adminUrl = $('base').attr('href');

$(document).ready(function () {
    var query = String(document.location).split('?');
    var arrPath = query[0].split('/').splice(0, 6);
    $('.sidebar-menu li a').each(function (i, val) {
        if (arrPath.join('/') === this.href) {
            $(this).closest('ul').parent().addClass('active');
            $(this).closest('ul').parent().closest('ul').parent().addClass('active');
            $(this).closest('ul').parent().closest('ul').parent().closest('ul').parent().addClass('active');
            $(this).closest('li').addClass('active');
        }
    });

    if (typeof $.summernote !== 'undefined') {
        $('.summernote').summernote({
            height: 300,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'image', 'video']],
                ['view', ['fullscreen', 'codeview']]
            ],
            buttons: {
                image: function () {
                    var ui = $.summernote.ui;
                    var button = ui.button({
                        contents: '<i class="fa fa-image" />',
                        tooltip: "Image Manager",
                        click: function () {
                            $(this).parents('.note-editor').find('.note-editable').focus();
                            $('#modal-image').remove();
                            $.ajax({
                                url: adminUrl + 'tool/image_manager',
                                dataType: 'html',
                                success: function (html) {
                                    $('body').append('<div id="modal-image" class="modal">' + html + '</div>');
                                    $('#modal-image').modal('show');
                                }
                            });
                        }
                    });
                    return button.render();
                }
            }
        });
    }

    $(document).delegate('a[data-toggle=\'image\']', 'click', function (e) {
        e.preventDefault();

        $('.popover').popover('hide', function () {
            $('.popover').remove();
        });

        var element = this;

        $(element).popover({
            html: true,
            placement: 'right',
            trigger: 'manual',
            content: function () {
                return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
            }
        });

        $(element).popover('show');

        $('#button-image').on('click', function () {
            $('#modal-image').remove();

            $.ajax({
                url: adminUrl + 'tool/image_manager',
                data: '&target=' + $(element).parent().find('input').attr('id') + '&thumb=' + $(element).attr('id'),
                dataType: 'html',
                beforeSend: function () {
                    $('#button-image i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                    $('#button-image').prop('disabled', true);
                },
                complete: function () {
                    $('#button-image i').replaceWith('<i class="fa fa-pencil"></i>');
                    $('#button-image').prop('disabled', false);
                },
                success: function (html) {
                    $('body').append('<div id="modal-image" class="modal">' + html + '</div>');
                    $('#modal-image').modal('show');
                }
            });

            $(element).popover('hide', function () {
                $('.popover').remove();
            });
        });

        $('#button-clear').on('click', function () {
            $(element).find('img').attr('src', $(element).find('img').attr('data-placeholder'));
            $(element).parent().find('input').attr('value', '');
            $(element).popover('hide', function () {
                $('.popover').remove();
            });

            parentElement = $(element).parent().parent();

            if (parentElement.hasClass('additional-image')) {
                parentElement.remove();
            }
        });
    });

    $(document).delegate('[data-toggle=\'tooltip\']', 'click', function (e) {
        $('body > .tooltip').remove();
    });
});