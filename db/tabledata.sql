SET
SQL_MODE
=
"NO_AUTO_VALUE_ON_ZERO";
SET
time_zone
=
"+00:00";

--
-- Truncate table before insert `addon`
--

TRUNCATE
TABLE
`addon`;
--
-- Truncate table before insert `address`
--

TRUNCATE
TABLE
`address`;
--
-- Truncate table before insert `admin`
--

TRUNCATE
TABLE
`admin`;
--
-- Dumping data for table `admin`
--

--
-- Truncate table before insert `admin_group`
--

TRUNCATE
TABLE
`admin_group`;
--
-- Truncate table before insert `attribute`
--

TRUNCATE
TABLE
`attribute`;
--
-- Truncate table before insert `attribute_group`
--

TRUNCATE
TABLE
`attribute_group`;
--
-- Truncate table before insert `banner`
--

TRUNCATE
TABLE
`banner`;
--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `name`, `description`, `active`)
VALUES
  (1, 'Top Banner', 'Top Banner', 1),
  (2, 'Bottom Banner', 'Baner Bawah', 1),
  (3, 'Slideshow', 'Slideshow', 1),
  (4, 'Header Banner', 'Header Banner', 1);

--
-- Truncate table before insert `banner_image`
--

TRUNCATE
TABLE
`banner_image`;
--
-- Truncate table before insert `category`
--

TRUNCATE
TABLE
`category`;
--
-- Truncate table before insert `category_path`
--

TRUNCATE
TABLE
`category_path`;
--
-- Truncate table before insert `currency`
--

TRUNCATE
TABLE
`currency`;
--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`currency_id`, `title`, `code`, `symbol`, `value`, `active`, `date_modified`)
VALUES
  (8, 'Rupiah', 'IDR', 'Rp', 1.00000000, 1, '2017-04-02 00:00:00');

--
-- Truncate table before insert `customer`
--

TRUNCATE
TABLE
`customer`;
--
-- Truncate table before insert `customer_group`
--

TRUNCATE
TABLE
`customer_group`;
--
-- Dumping data for table `customer_group`
--

INSERT INTO `customer_group` (`customer_group_id`, `name`, `description`, `approval`)
VALUES
  (3, 'Default', NULL, 1),
  (4, 'Reseller', NULL, 0);

--
-- Truncate table before insert `customer_history`
--

TRUNCATE
TABLE
`customer_history`;
--
-- Truncate table before insert `customer_transaction`
--

TRUNCATE
TABLE
`customer_transaction`;
--
-- Truncate table before insert `custom_category`
--

TRUNCATE
TABLE
`custom_category`;
--
-- Truncate table before insert `custom_part`
--

TRUNCATE
TABLE
`custom_part`;
--
-- Truncate table before insert `custom_part_pair`
--

TRUNCATE
TABLE
`custom_part_pair`;
--
-- Truncate table before insert `filter`
--

TRUNCATE
TABLE
`filter`;
--
-- Truncate table before insert `filter_group`
--

TRUNCATE
TABLE
`filter_group`;
--
-- Truncate table before insert `length_class`
--

TRUNCATE
TABLE
`length_class`;
--
-- Dumping data for table `length_class`
--

INSERT INTO `length_class` (`length_class_id`, `title`, `unit`, `value`)
VALUES
  (1, 'Milimeter', 'mm', '1.0000'),
  (2, 'Centimeter', 'cm', '0.1000');

--
-- Truncate table before insert `location`
--

TRUNCATE
TABLE
`location`;
--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `province_id`, `city_id`, `subdistrict_id`, `name`, `type`, `postcode`)
VALUES
  (1, 1, 0, 0, 'Bali', 'Provinsi', ''),
  (2, 2, 0, 0, 'Bangka Belitung', 'Provinsi', ''),
  (3, 3, 0, 0, 'Banten', 'Provinsi', ''),
  (4, 4, 0, 0, 'Bengkulu', 'Provinsi', ''),
  (5, 5, 0, 0, 'DI Yogyakarta', 'Provinsi', ''),
  (6, 6, 0, 0, 'DKI Jakarta', 'Provinsi', ''),
  (7, 7, 0, 0, 'Gorontalo', 'Provinsi', ''),
  (8, 8, 0, 0, 'Jambi', 'Provinsi', ''),
  (9, 9, 0, 0, 'Jawa Barat', 'Provinsi', ''),
  (10, 10, 0, 0, 'Jawa Tengah', 'Provinsi', ''),
  (11, 11, 0, 0, 'Jawa Timur', 'Provinsi', ''),
  (12, 12, 0, 0, 'Kalimantan Barat', 'Provinsi', ''),
  (13, 13, 0, 0, 'Kalimantan Selatan', 'Provinsi', ''),
  (14, 14, 0, 0, 'Kalimantan Tengah', 'Provinsi', ''),
  (15, 15, 0, 0, 'Kalimantan Timur', 'Provinsi', ''),
  (16, 16, 0, 0, 'Kalimantan Utara', 'Provinsi', ''),
  (17, 17, 0, 0, 'Kepulauan Riau', 'Provinsi', ''),
  (18, 18, 0, 0, 'Lampung', 'Provinsi', ''),
  (19, 19, 0, 0, 'Maluku', 'Provinsi', ''),
  (20, 20, 0, 0, 'Maluku Utara', 'Provinsi', ''),
  (21, 21, 0, 0, 'Nanggroe Aceh Darussalam (NAD)', 'Provinsi', ''),
  (22, 22, 0, 0, 'Nusa Tenggara Barat (NTB)', 'Provinsi', ''),
  (23, 23, 0, 0, 'Nusa Tenggara Timur (NTT)', 'Provinsi', ''),
  (24, 24, 0, 0, 'Papua', 'Provinsi', ''),
  (25, 25, 0, 0, 'Papua Barat', 'Provinsi', ''),
  (26, 26, 0, 0, 'Riau', 'Provinsi', ''),
  (27, 27, 0, 0, 'Sulawesi Barat', 'Provinsi', ''),
  (28, 28, 0, 0, 'Sulawesi Selatan', 'Provinsi', ''),
  (29, 29, 0, 0, 'Sulawesi Tengah', 'Provinsi', ''),
  (30, 30, 0, 0, 'Sulawesi Tenggara', 'Provinsi', ''),
  (31, 31, 0, 0, 'Sulawesi Utara', 'Provinsi', ''),
  (32, 32, 0, 0, 'Sumatera Barat', 'Provinsi', ''),
  (33, 33, 0, 0, 'Sumatera Selatan', 'Provinsi', ''),
  (34, 34, 0, 0, 'Sumatera Utara', 'Provinsi', ''),
  (35, 6, 151, 0, 'Jakarta Barat', 'Kota', '11220'),
  (36, 6, 152, 0, 'Jakarta Pusat', 'Kota', '10540'),
  (37, 6, 153, 0, 'Jakarta Selatan', 'Kota', '12230'),
  (38, 6, 154, 0, 'Jakarta Timur', 'Kota', '13330'),
  (39, 6, 155, 0, 'Jakarta Utara', 'Kota', '14140'),
  (40, 6, 189, 0, 'Kepulauan Seribu', 'Kabupaten', '14550'),
  (41, 6, 152, 2095, 'Cempaka Putih', 'Kecamatan', ''),
  (42, 6, 152, 2096, 'Gambir', 'Kecamatan', ''),
  (43, 6, 152, 2097, 'Johar Baru', 'Kecamatan', ''),
  (44, 6, 152, 2098, 'Kemayoran', 'Kecamatan', ''),
  (45, 6, 152, 2099, 'Menteng', 'Kecamatan', ''),
  (46, 6, 152, 2100, 'Sawah Besar', 'Kecamatan', ''),
  (47, 6, 152, 2101, 'Senen', 'Kecamatan', ''),
  (48, 6, 152, 2102, 'Tanah Abang', 'Kecamatan', ''),
  (49, 11, 31, 0, 'Bangkalan', 'Kabupaten', '69118'),
  (50, 11, 42, 0, 'Banyuwangi', 'Kabupaten', '68416'),
  (51, 11, 51, 0, 'Batu', 'Kota', '65311'),
  (52, 11, 74, 0, 'Blitar', 'Kabupaten', '66171'),
  (53, 11, 75, 0, 'Blitar', 'Kota', '66124'),
  (54, 11, 80, 0, 'Bojonegoro', 'Kabupaten', '62119'),
  (55, 11, 86, 0, 'Bondowoso', 'Kabupaten', '68219'),
  (56, 11, 133, 0, 'Gresik', 'Kabupaten', '61115'),
  (57, 11, 160, 0, 'Jember', 'Kabupaten', '68113'),
  (58, 11, 164, 0, 'Jombang', 'Kabupaten', '61415'),
  (59, 11, 178, 0, 'Kediri', 'Kabupaten', '64184'),
  (60, 11, 179, 0, 'Kediri', 'Kota', '64125'),
  (61, 11, 222, 0, 'Lamongan', 'Kabupaten', '64125'),
  (62, 11, 243, 0, 'Lumajang', 'Kabupaten', '67319'),
  (63, 11, 247, 0, 'Madiun', 'Kabupaten', '63153'),
  (64, 11, 248, 0, 'Madiun', 'Kota', '63122'),
  (65, 11, 251, 0, 'Magetan', 'Kabupaten', '63314'),
  (66, 11, 255, 0, 'Malang', 'Kabupaten', '65163'),
  (67, 11, 256, 0, 'Malang', 'Kota', '65112'),
  (68, 11, 289, 0, 'Mojokerto', 'Kabupaten', '61382'),
  (69, 11, 290, 0, 'Mojokerto', 'Kota', '61316'),
  (70, 11, 305, 0, 'Nganjuk', 'Kabupaten', '64414'),
  (71, 11, 306, 0, 'Ngawi', 'Kabupaten', '63219'),
  (72, 11, 317, 0, 'Pacitan', 'Kabupaten', '63512'),
  (73, 11, 330, 0, 'Pamekasan', 'Kabupaten', '69319'),
  (74, 11, 342, 0, 'Pasuruan', 'Kabupaten', '67153'),
  (75, 11, 343, 0, 'Pasuruan', 'Kota', '67118'),
  (76, 11, 363, 0, 'Ponorogo', 'Kabupaten', '63411'),
  (77, 11, 369, 0, 'Probolinggo', 'Kabupaten', '67282'),
  (78, 11, 370, 0, 'Probolinggo', 'Kota', '67215'),
  (79, 11, 390, 0, 'Sampang', 'Kabupaten', '69219'),
  (80, 11, 409, 0, 'Sidoarjo', 'Kabupaten', '61219'),
  (81, 11, 418, 0, 'Situbondo', 'Kabupaten', '68316'),
  (82, 11, 441, 0, 'Sumenep', 'Kabupaten', '69413'),
  (83, 11, 444, 0, 'Surabaya', 'Kota', '60119'),
  (84, 11, 487, 0, 'Trenggalek', 'Kabupaten', '66312'),
  (85, 11, 489, 0, 'Tuban', 'Kabupaten', '62319'),
  (86, 11, 492, 0, 'Tulungagung', 'Kabupaten', '66212'),
  (87, 11, 51, 708, 'Batu', 'Kecamatan', ''),
  (88, 11, 51, 709, 'Bumiaji', 'Kecamatan', ''),
  (89, 11, 51, 710, 'Junrejo', 'Kecamatan', ''),
  (90, 2, 27, 0, 'Bangka', 'Kabupaten', '33212'),
  (91, 2, 28, 0, 'Bangka Barat', 'Kabupaten', '33315'),
  (92, 2, 29, 0, 'Bangka Selatan', 'Kabupaten', '33719'),
  (93, 2, 30, 0, 'Bangka Tengah', 'Kabupaten', '33613'),
  (94, 2, 56, 0, 'Belitung', 'Kabupaten', '33419'),
  (95, 2, 57, 0, 'Belitung Timur', 'Kabupaten', '33519'),
  (96, 2, 334, 0, 'Pangkal Pinang', 'Kota', '33115'),
  (97, 2, 27, 426, 'Bakam', 'Kecamatan', ''),
  (98, 2, 27, 427, 'Belinyu', 'Kecamatan', ''),
  (99, 2, 27, 428, 'Mendo Barat', 'Kecamatan', ''),
  (100, 2, 27, 429, 'Merawang', 'Kecamatan', ''),
  (101, 2, 27, 430, 'Pemali', 'Kecamatan', ''),
  (102, 2, 27, 431, 'Puding Besar', 'Kecamatan', ''),
  (103, 2, 27, 432, 'Riau Silip', 'Kecamatan', ''),
  (104, 2, 27, 433, 'Sungai Liat', 'Kecamatan', ''),
  (105, 33, 40, 0, 'Banyuasin', 'Kabupaten', '30911'),
  (106, 33, 121, 0, 'Empat Lawang', 'Kabupaten', '31811'),
  (107, 33, 220, 0, 'Lahat', 'Kabupaten', '31419'),
  (108, 33, 242, 0, 'Lubuk Linggau', 'Kota', '31614'),
  (109, 33, 292, 0, 'Muara Enim', 'Kabupaten', '31315'),
  (110, 33, 297, 0, 'Musi Banyuasin', 'Kabupaten', '30719'),
  (111, 33, 298, 0, 'Musi Rawas', 'Kabupaten', '31661'),
  (112, 33, 312, 0, 'Ogan Ilir', 'Kabupaten', '30811'),
  (113, 33, 313, 0, 'Ogan Komering Ilir', 'Kabupaten', '30618'),
  (114, 33, 314, 0, 'Ogan Komering Ulu', 'Kabupaten', '32112'),
  (115, 33, 315, 0, 'Ogan Komering Ulu Selatan', 'Kabupaten', '32211'),
  (116, 33, 316, 0, 'Ogan Komering Ulu Timur', 'Kabupaten', '32312'),
  (117, 33, 324, 0, 'Pagar Alam', 'Kota', '31512'),
  (118, 33, 327, 0, 'Palembang', 'Kota', '31512'),
  (119, 33, 367, 0, 'Prabumulih', 'Kota', '31121'),
  (120, 32, 12, 0, 'Agam', 'Kabupaten', '26411'),
  (121, 32, 93, 0, 'Bukittinggi', 'Kota', '26115'),
  (122, 32, 116, 0, 'Dharmasraya', 'Kabupaten', '27612'),
  (123, 32, 186, 0, 'Kepulauan Mentawai', 'Kabupaten', '25771'),
  (124, 32, 236, 0, 'Lima Puluh Koto/Kota', 'Kabupaten', '26671'),
  (125, 32, 318, 0, 'Padang', 'Kota', '25112'),
  (126, 32, 321, 0, 'Padang Panjang', 'Kota', '27122'),
  (127, 32, 322, 0, 'Padang Pariaman', 'Kabupaten', '25583'),
  (128, 32, 337, 0, 'Pariaman', 'Kota', '25511'),
  (129, 32, 339, 0, 'Pasaman', 'Kabupaten', '26318'),
  (130, 32, 340, 0, 'Pasaman Barat', 'Kabupaten', '26511'),
  (131, 32, 345, 0, 'Payakumbuh', 'Kota', '26213'),
  (132, 32, 357, 0, 'Pesisir Selatan', 'Kabupaten', '25611'),
  (133, 32, 394, 0, 'Sawah Lunto', 'Kota', '27416'),
  (134, 32, 411, 0, 'Sijunjung (Sawah Lunto Sijunjung)', 'Kabupaten', '27511'),
  (135, 32, 420, 0, 'Solok', 'Kabupaten', '27365'),
  (136, 32, 421, 0, 'Solok', 'Kota', '27315'),
  (137, 32, 422, 0, 'Solok Selatan', 'Kabupaten', '27779'),
  (138, 32, 453, 0, 'Tanah Datar', 'Kabupaten', '27211'),
  (139, 34, 15, 0, 'Asahan', 'Kabupaten', '21214'),
  (140, 34, 52, 0, 'Batu Bara', 'Kabupaten', '21655'),
  (141, 34, 70, 0, 'Binjai', 'Kota', '20712'),
  (142, 34, 110, 0, 'Dairi', 'Kabupaten', '22211'),
  (143, 34, 112, 0, 'Deli Serdang', 'Kabupaten', '20511'),
  (144, 34, 137, 0, 'Gunungsitoli', 'Kota', '22813'),
  (145, 34, 146, 0, 'Humbang Hasundutan', 'Kabupaten', '22457'),
  (146, 34, 173, 0, 'Karo', 'Kabupaten', '22119'),
  (147, 34, 217, 0, 'Labuhan Batu', 'Kabupaten', '21412'),
  (148, 34, 218, 0, 'Labuhan Batu Selatan', 'Kabupaten', '21511'),
  (149, 34, 219, 0, 'Labuhan Batu Utara', 'Kabupaten', '21711'),
  (150, 34, 229, 0, 'Langkat', 'Kabupaten', '20811'),
  (151, 34, 268, 0, 'Mandailing Natal', 'Kabupaten', '22916'),
  (152, 34, 278, 0, 'Medan', 'Kota', '20228'),
  (153, 34, 307, 0, 'Nias', 'Kabupaten', '22876'),
  (154, 34, 308, 0, 'Nias Barat', 'Kabupaten', '22895'),
  (155, 34, 309, 0, 'Nias Selatan', 'Kabupaten', '22865'),
  (156, 34, 310, 0, 'Nias Utara', 'Kabupaten', '22856'),
  (157, 34, 319, 0, 'Padang Lawas', 'Kabupaten', '22763'),
  (158, 34, 320, 0, 'Padang Lawas Utara', 'Kabupaten', '22753'),
  (159, 34, 323, 0, 'Padang Sidempuan', 'Kota', '22727'),
  (160, 34, 325, 0, 'Pakpak Bharat', 'Kabupaten', '22272'),
  (161, 34, 353, 0, 'Pematang Siantar', 'Kota', '21126'),
  (162, 34, 389, 0, 'Samosir', 'Kabupaten', '22392'),
  (163, 34, 404, 0, 'Serdang Bedagai', 'Kabupaten', '20915'),
  (164, 34, 407, 0, 'Sibolga', 'Kota', '22522'),
  (165, 34, 413, 0, 'Simalungun', 'Kabupaten', '21162'),
  (166, 34, 459, 0, 'Tanjung Balai', 'Kota', '21321'),
  (167, 34, 463, 0, 'Tapanuli Selatan', 'Kabupaten', '22742'),
  (168, 34, 464, 0, 'Tapanuli Tengah', 'Kabupaten', '22611'),
  (169, 34, 465, 0, 'Tapanuli Utara', 'Kabupaten', '22414'),
  (170, 34, 470, 0, 'Tebing Tinggi', 'Kota', '20632'),
  (171, 34, 481, 0, 'Toba Samosir', 'Kabupaten', '22316'),
  (172, 34, 278, 3906, 'Medan Amplas', 'Kecamatan', ''),
  (173, 34, 278, 3907, 'Medan Area', 'Kecamatan', ''),
  (174, 34, 278, 3908, 'Medan Barat', 'Kecamatan', ''),
  (175, 34, 278, 3909, 'Medan Baru', 'Kecamatan', ''),
  (176, 34, 278, 3910, 'Medan Belawan Kota', 'Kecamatan', ''),
  (177, 34, 278, 3911, 'Medan Deli', 'Kecamatan', ''),
  (178, 34, 278, 3912, 'Medan Denai', 'Kecamatan', ''),
  (179, 34, 278, 3913, 'Medan Helvetia', 'Kecamatan', ''),
  (180, 34, 278, 3914, 'Medan Johor', 'Kecamatan', ''),
  (181, 34, 278, 3915, 'Medan Kota', 'Kecamatan', ''),
  (182, 34, 278, 3916, 'Medan Labuhan', 'Kecamatan', ''),
  (183, 34, 278, 3917, 'Medan Maimun', 'Kecamatan', ''),
  (184, 34, 278, 3918, 'Medan Marelan', 'Kecamatan', ''),
  (185, 34, 278, 3919, 'Medan Perjuangan', 'Kecamatan', ''),
  (186, 34, 278, 3920, 'Medan Petisah', 'Kecamatan', ''),
  (187, 34, 278, 3921, 'Medan Polonia', 'Kecamatan', ''),
  (188, 34, 278, 3922, 'Medan Selayang', 'Kecamatan', ''),
  (189, 34, 278, 3923, 'Medan Sunggal', 'Kecamatan', ''),
  (190, 34, 278, 3924, 'Medan Tembung', 'Kecamatan', ''),
  (191, 34, 278, 3925, 'Medan Timur', 'Kecamatan', ''),
  (192, 34, 278, 3926, 'Medan Tuntungan', 'Kecamatan', ''),
  (193, 1, 17, 0, 'Badung', 'Kabupaten', '80351'),
  (194, 1, 32, 0, 'Bangli', 'Kabupaten', '80619'),
  (195, 1, 94, 0, 'Buleleng', 'Kabupaten', '81111'),
  (196, 1, 114, 0, 'Denpasar', 'Kota', '80227'),
  (197, 1, 128, 0, 'Gianyar', 'Kabupaten', '80519'),
  (198, 1, 161, 0, 'Jembrana', 'Kabupaten', '82251'),
  (199, 1, 170, 0, 'Karangasem', 'Kabupaten', '80819'),
  (200, 1, 197, 0, 'Klungkung', 'Kabupaten', '80719'),
  (201, 1, 447, 0, 'Tabanan', 'Kabupaten', '82119'),
  (202, 1, 32, 472, 'Bangli', 'Kecamatan', ''),
  (203, 1, 32, 473, 'Kintamani', 'Kecamatan', ''),
  (204, 1, 32, 474, 'Susut', 'Kecamatan', ''),
  (205, 1, 32, 475, 'Tembuku', 'Kecamatan', ''),
  (206, 11, 178, 2497, 'Badas', 'Kecamatan', ''),
  (207, 11, 178, 2498, 'Banyakan', 'Kecamatan', ''),
  (208, 11, 178, 2499, 'Gampengrejo', 'Kecamatan', ''),
  (209, 11, 178, 2500, 'Grogol', 'Kecamatan', ''),
  (210, 11, 178, 2501, 'Gurah', 'Kecamatan', ''),
  (211, 11, 178, 2502, 'Kandangan', 'Kecamatan', ''),
  (212, 11, 178, 2503, 'Kandat', 'Kecamatan', ''),
  (213, 11, 178, 2504, 'Kayen Kidul', 'Kecamatan', ''),
  (214, 11, 178, 2505, 'Kepung', 'Kecamatan', ''),
  (215, 11, 178, 2506, 'Kras', 'Kecamatan', ''),
  (216, 11, 178, 2507, 'Kunjang', 'Kecamatan', ''),
  (217, 11, 178, 2508, 'Mojo', 'Kecamatan', ''),
  (218, 11, 178, 2509, 'Ngadiluwih', 'Kecamatan', ''),
  (219, 11, 178, 2510, 'Ngancar', 'Kecamatan', ''),
  (220, 11, 178, 2511, 'Ngasem', 'Kecamatan', ''),
  (221, 11, 178, 2512, 'Pagu', 'Kecamatan', ''),
  (222, 11, 178, 2513, 'Papar', 'Kecamatan', ''),
  (223, 11, 178, 2514, 'Pare', 'Kecamatan', ''),
  (224, 11, 178, 2515, 'Plemahan', 'Kecamatan', ''),
  (225, 11, 178, 2516, 'Plosoklaten', 'Kecamatan', ''),
  (226, 11, 178, 2517, 'Puncu', 'Kecamatan', ''),
  (227, 11, 178, 2518, 'Purwoasri', 'Kecamatan', ''),
  (228, 11, 178, 2519, 'Ringinrejo', 'Kecamatan', ''),
  (229, 11, 178, 2520, 'Semen', 'Kecamatan', ''),
  (230, 11, 178, 2521, 'Tarokan', 'Kecamatan', ''),
  (231, 11, 178, 2522, 'Wates', 'Kecamatan', ''),
  (232, 1, 94, 1279, 'Banjar', 'Kecamatan', ''),
  (233, 1, 94, 1280, 'Buleleng', 'Kecamatan', ''),
  (234, 1, 94, 1281, 'Busungbiu', 'Kecamatan', ''),
  (235, 1, 94, 1282, 'Gerokgak', 'Kecamatan', ''),
  (236, 1, 94, 1283, 'Kubutambahan', 'Kecamatan', ''),
  (237, 1, 94, 1284, 'Sawan', 'Kecamatan', ''),
  (238, 1, 94, 1285, 'Seririt', 'Kecamatan', ''),
  (239, 1, 94, 1286, 'Sukasada', 'Kecamatan', ''),
  (240, 1, 94, 1287, 'Tejakula', 'Kecamatan', ''),
  (241, 11, 256, 3634, 'Blimbing', 'Kecamatan', ''),
  (242, 11, 256, 3635, 'Kedungkandang', 'Kecamatan', ''),
  (243, 11, 256, 3636, 'Klojen', 'Kecamatan', ''),
  (244, 11, 256, 3637, 'Lowokwaru', 'Kecamatan', ''),
  (245, 11, 256, 3638, 'Sukun', 'Kecamatan', ''),
  (246, 30, 53, 0, 'Bau-Bau', 'Kota', '93719'),
  (247, 30, 85, 0, 'Bombana', 'Kabupaten', '93771'),
  (248, 30, 101, 0, 'Buton', 'Kabupaten', '93754'),
  (249, 30, 102, 0, 'Buton Utara', 'Kabupaten', '93745'),
  (250, 30, 182, 0, 'Kendari', 'Kota', '93126'),
  (251, 30, 198, 0, 'Kolaka', 'Kabupaten', '93511'),
  (252, 30, 199, 0, 'Kolaka Utara', 'Kabupaten', '93911'),
  (253, 30, 200, 0, 'Konawe', 'Kabupaten', '93411'),
  (254, 30, 201, 0, 'Konawe Selatan', 'Kabupaten', '93811'),
  (255, 30, 202, 0, 'Konawe Utara', 'Kabupaten', '93311'),
  (256, 30, 295, 0, 'Muna', 'Kabupaten', '93611'),
  (257, 30, 494, 0, 'Wakatobi', 'Kabupaten', '93791'),
  (258, 30, 295, 4157, 'Barangka', 'Kecamatan', ''),
  (259, 30, 295, 4158, 'Batalaiwaru (Batalaiworu)', 'Kecamatan', ''),
  (260, 30, 295, 4159, 'Batukara', 'Kecamatan', ''),
  (261, 30, 295, 4160, 'Bone (Bone Tondo)', 'Kecamatan', ''),
  (262, 30, 295, 4161, 'Duruka', 'Kecamatan', ''),
  (263, 30, 295, 4162, 'Kabangka', 'Kecamatan', ''),
  (264, 30, 295, 4163, 'Kabawo', 'Kecamatan', ''),
  (265, 30, 295, 4164, 'Katobu', 'Kecamatan', ''),
  (266, 30, 295, 4165, 'Kontu Kowuna', 'Kecamatan', ''),
  (267, 30, 295, 4166, 'Kontunaga', 'Kecamatan', ''),
  (268, 30, 295, 4167, 'Kusambi', 'Kecamatan', ''),
  (269, 30, 295, 4168, 'Lasalepa', 'Kecamatan', ''),
  (270, 30, 295, 4169, 'Lawa', 'Kecamatan', ''),
  (271, 30, 295, 4170, 'Lohia', 'Kecamatan', ''),
  (272, 30, 295, 4171, 'Maginti', 'Kecamatan', ''),
  (273, 30, 295, 4172, 'Maligano', 'Kecamatan', ''),
  (274, 30, 295, 4173, 'Marobo', 'Kecamatan', ''),
  (275, 30, 295, 4174, 'Napabalano', 'Kecamatan', ''),
  (276, 30, 295, 4175, 'Napano Kusambi', 'Kecamatan', ''),
  (277, 30, 295, 4176, 'Parigi', 'Kecamatan', ''),
  (278, 30, 295, 4177, 'Pasi Kolaga', 'Kecamatan', ''),
  (279, 30, 295, 4178, 'Pasir Putih', 'Kecamatan', ''),
  (280, 30, 295, 4179, 'Sawerigadi (Sawerigading/Sewergadi)', 'Kecamatan', ''),
  (281, 30, 295, 4180, 'Tiworo Kepulauan', 'Kecamatan', ''),
  (282, 30, 295, 4181, 'Tiworo Selatan', 'Kecamatan', ''),
  (283, 30, 295, 4182, 'Tiworo Tengah', 'Kecamatan', ''),
  (284, 30, 295, 4183, 'Tiworo Utara', 'Kecamatan', ''),
  (285, 30, 295, 4184, 'Tongkuno', 'Kecamatan', ''),
  (286, 30, 295, 4185, 'Tongkuno Selatan', 'Kecamatan', ''),
  (287, 30, 295, 4186, 'Towea', 'Kecamatan', ''),
  (288, 30, 295, 4187, 'Wa Daga', 'Kecamatan', ''),
  (289, 30, 295, 4188, 'Wakorumba Selatan', 'Kecamatan', ''),
  (290, 30, 295, 4189, 'Watopute', 'Kecamatan', ''),
  (291, 5, 39, 0, 'Bantul', 'Kabupaten', '55715'),
  (292, 5, 135, 0, 'Gunung Kidul', 'Kabupaten', '55812'),
  (293, 5, 210, 0, 'Kulon Progo', 'Kabupaten', '55611'),
  (294, 5, 419, 0, 'Sleman', 'Kabupaten', '55513'),
  (295, 5, 501, 0, 'Yogyakarta', 'Kota', '55222'),
  (296, 5, 39, 537, 'Bambang Lipuro', 'Kecamatan', ''),
  (297, 5, 39, 538, 'Banguntapan', 'Kecamatan', ''),
  (298, 5, 39, 539, 'Bantul', 'Kecamatan', ''),
  (299, 5, 39, 540, 'Dlingo', 'Kecamatan', ''),
  (300, 5, 39, 541, 'Imogiri', 'Kecamatan', ''),
  (301, 5, 39, 542, 'Jetis', 'Kecamatan', ''),
  (302, 5, 39, 543, 'Kasihan', 'Kecamatan', ''),
  (303, 5, 39, 544, 'Kretek', 'Kecamatan', ''),
  (304, 5, 39, 545, 'Pajangan', 'Kecamatan', ''),
  (305, 5, 39, 546, 'Pandak', 'Kecamatan', ''),
  (306, 5, 39, 547, 'Piyungan', 'Kecamatan', ''),
  (307, 5, 39, 548, 'Pleret', 'Kecamatan', ''),
  (308, 5, 39, 549, 'Pundong', 'Kecamatan', ''),
  (309, 5, 39, 550, 'Sanden', 'Kecamatan', ''),
  (310, 5, 39, 551, 'Sedayu', 'Kecamatan', ''),
  (311, 5, 39, 552, 'Sewon', 'Kecamatan', ''),
  (312, 5, 39, 553, 'Srandakan', 'Kecamatan', ''),
  (313, 22, 68, 0, 'Bima', 'Kabupaten', '84171'),
  (314, 22, 69, 0, 'Bima', 'Kota', '84139'),
  (315, 22, 118, 0, 'Dompu', 'Kabupaten', '84217'),
  (316, 22, 238, 0, 'Lombok Barat', 'Kabupaten', '83311'),
  (317, 22, 239, 0, 'Lombok Tengah', 'Kabupaten', '83511'),
  (318, 22, 240, 0, 'Lombok Timur', 'Kabupaten', '83612'),
  (319, 22, 241, 0, 'Lombok Utara', 'Kabupaten', '83711'),
  (320, 22, 276, 0, 'Mataram', 'Kota', '83131'),
  (321, 22, 438, 0, 'Sumbawa', 'Kabupaten', '84315'),
  (322, 22, 439, 0, 'Sumbawa Barat', 'Kabupaten', '84419'),
  (323, 1, 17, 258, 'Abiansemal', 'Kecamatan', ''),
  (324, 1, 17, 259, 'Kuta', 'Kecamatan', ''),
  (325, 1, 17, 260, 'Kuta Selatan', 'Kecamatan', ''),
  (326, 1, 17, 261, 'Kuta Utara', 'Kecamatan', ''),
  (327, 1, 17, 262, 'Mengwi', 'Kecamatan', ''),
  (328, 1, 17, 263, 'Petang', 'Kecamatan', ''),
  (329, 1, 128, 1764, 'Belah Batuh (Blahbatuh)', 'Kecamatan', ''),
  (330, 1, 128, 1765, 'Gianyar', 'Kecamatan', ''),
  (331, 1, 128, 1766, 'Payangan', 'Kecamatan', ''),
  (332, 1, 128, 1767, 'Sukawati', 'Kecamatan', ''),
  (333, 1, 128, 1768, 'Tampak Siring', 'Kecamatan', ''),
  (334, 1, 128, 1769, 'Tegallalang', 'Kecamatan', ''),
  (335, 1, 128, 1770, 'Ubud', 'Kecamatan', ''),
  (336, 1, 161, 2232, 'Jembrana', 'Kecamatan', ''),
  (337, 1, 161, 2233, 'Melaya', 'Kecamatan', ''),
  (338, 1, 161, 2234, 'Mendoyo', 'Kecamatan', ''),
  (339, 1, 161, 2235, 'Negara', 'Kecamatan', ''),
  (340, 1, 161, 2236, 'Pekutatan', 'Kecamatan', ''),
  (341, 1, 170, 2370, 'Abang', 'Kecamatan', ''),
  (342, 1, 170, 2371, 'Bebandem', 'Kecamatan', ''),
  (343, 1, 170, 2372, 'Karang Asem', 'Kecamatan', ''),
  (344, 1, 170, 2373, 'Kubu', 'Kecamatan', ''),
  (345, 1, 170, 2374, 'Manggis', 'Kecamatan', ''),
  (346, 1, 170, 2375, 'Rendang', 'Kecamatan', ''),
  (347, 1, 170, 2376, 'Selat', 'Kecamatan', ''),
  (348, 1, 170, 2377, 'Sidemen', 'Kecamatan', ''),
  (349, 1, 197, 2748, 'Banjarangkan', 'Kecamatan', ''),
  (350, 1, 197, 2749, 'Dawan', 'Kecamatan', ''),
  (351, 1, 197, 2750, 'Klungkung', 'Kecamatan', ''),
  (352, 1, 197, 2751, 'Nusapenida', 'Kecamatan', ''),
  (353, 1, 447, 6179, 'Baturiti', 'Kecamatan', ''),
  (354, 1, 447, 6180, 'Kediri', 'Kecamatan', ''),
  (355, 1, 447, 6181, 'Kerambitan', 'Kecamatan', ''),
  (356, 1, 447, 6182, 'Marga', 'Kecamatan', ''),
  (357, 1, 447, 6183, 'Penebel', 'Kecamatan', ''),
  (358, 1, 447, 6184, 'Pupuan', 'Kecamatan', ''),
  (359, 1, 447, 6185, 'Selemadeg', 'Kecamatan', ''),
  (360, 1, 447, 6186, 'Selemadeg / Salamadeg Timur', 'Kecamatan', ''),
  (361, 1, 447, 6187, 'Selemadeg / Salemadeg Barat', 'Kecamatan', ''),
  (362, 1, 447, 6188, 'Tabanan', 'Kecamatan', ''),
  (363, 1, 114, 1573, 'Denpasar Barat', 'Kecamatan', ''),
  (364, 1, 114, 1574, 'Denpasar Selatan', 'Kecamatan', ''),
  (365, 1, 114, 1575, 'Denpasar Timur', 'Kecamatan', ''),
  (366, 1, 114, 1576, 'Denpasar Utara', 'Kecamatan', ''),
  (367, 2, 28, 434, 'Jebus', 'Kecamatan', ''),
  (368, 2, 28, 435, 'Kelapa', 'Kecamatan', ''),
  (369, 2, 28, 436, 'Mentok (Muntok)', 'Kecamatan', ''),
  (370, 2, 28, 437, 'Parittiga', 'Kecamatan', ''),
  (371, 2, 28, 438, 'Simpang Teritip', 'Kecamatan', ''),
  (372, 2, 28, 439, 'Tempilang', 'Kecamatan', ''),
  (373, 2, 29, 440, 'Air Gegas', 'Kecamatan', ''),
  (374, 2, 29, 441, 'Kepulauan Pongok', 'Kecamatan', ''),
  (375, 2, 29, 442, 'Lepar Pongok', 'Kecamatan', ''),
  (376, 2, 29, 443, 'Payung', 'Kecamatan', ''),
  (377, 2, 29, 444, 'Pulau Besar', 'Kecamatan', ''),
  (378, 2, 29, 445, 'Simpang Rimba', 'Kecamatan', ''),
  (379, 2, 29, 446, 'Toboali', 'Kecamatan', ''),
  (380, 2, 29, 447, 'Tukak Sadai', 'Kecamatan', ''),
  (381, 2, 30, 448, 'Koba', 'Kecamatan', ''),
  (382, 2, 30, 449, 'Lubuk Besar', 'Kecamatan', ''),
  (383, 2, 30, 450, 'Namang', 'Kecamatan', ''),
  (384, 2, 30, 451, 'Pangkalan Baru', 'Kecamatan', ''),
  (385, 2, 30, 452, 'Simpang Katis', 'Kecamatan', ''),
  (386, 2, 30, 453, 'Sungai Selan', 'Kecamatan', ''),
  (387, 2, 56, 761, 'Badau', 'Kecamatan', ''),
  (388, 2, 56, 762, 'Membalong', 'Kecamatan', ''),
  (389, 2, 56, 763, 'Selat Nasik', 'Kecamatan', ''),
  (390, 2, 56, 764, 'Sijuk', 'Kecamatan', ''),
  (391, 2, 56, 765, 'Tanjung Pandan', 'Kecamatan', ''),
  (392, 2, 57, 766, 'Damar', 'Kecamatan', ''),
  (393, 2, 57, 767, 'Dendang', 'Kecamatan', ''),
  (394, 2, 57, 768, 'Gantung', 'Kecamatan', ''),
  (395, 2, 57, 769, 'Kelapa Kampit', 'Kecamatan', ''),
  (396, 2, 57, 770, 'Manggar', 'Kecamatan', ''),
  (397, 2, 57, 771, 'Simpang Pesak', 'Kecamatan', ''),
  (398, 2, 57, 772, 'Simpang Renggiang', 'Kecamatan', ''),
  (399, 2, 334, 4713, 'Bukit Intan', 'Kecamatan', ''),
  (400, 2, 334, 4714, 'Gabek', 'Kecamatan', ''),
  (401, 2, 334, 4715, 'Gerunggang', 'Kecamatan', ''),
  (402, 2, 334, 4716, 'Girimaya', 'Kecamatan', ''),
  (403, 2, 334, 4717, 'Pangkal Balam', 'Kecamatan', ''),
  (404, 2, 334, 4718, 'Rangkui', 'Kecamatan', ''),
  (405, 2, 334, 4719, 'Taman Sari', 'Kecamatan', ''),
  (406, 3, 106, 0, 'Cilegon', 'Kota', '42417'),
  (407, 3, 232, 0, 'Lebak', 'Kabupaten', '42319'),
  (408, 3, 331, 0, 'Pandeglang', 'Kabupaten', '42212'),
  (409, 3, 402, 0, 'Serang', 'Kabupaten', '42182'),
  (410, 3, 403, 0, 'Serang', 'Kota', '42111'),
  (411, 3, 455, 0, 'Tangerang', 'Kabupaten', '15914'),
  (412, 3, 456, 0, 'Tangerang', 'Kota', '15111'),
  (413, 3, 457, 0, 'Tangerang Selatan', 'Kota', '15332'),
  (414, 3, 106, 1461, 'Cibeber', 'Kecamatan', ''),
  (415, 3, 106, 1462, 'Cilegon', 'Kecamatan', ''),
  (416, 3, 106, 1463, 'Citangkil', 'Kecamatan', ''),
  (417, 3, 106, 1464, 'Ciwandan', 'Kecamatan', ''),
  (418, 3, 106, 1465, 'Gerogol', 'Kecamatan', ''),
  (419, 3, 106, 1466, 'Jombang', 'Kecamatan', ''),
  (420, 3, 106, 1467, 'Pulomerak', 'Kecamatan', ''),
  (421, 3, 106, 1468, 'Purwakarta', 'Kecamatan', ''),
  (422, 3, 232, 3298, 'Banjarsari', 'Kecamatan', ''),
  (423, 3, 232, 3299, 'Bayah', 'Kecamatan', ''),
  (424, 3, 232, 3300, 'Bojongmanik', 'Kecamatan', ''),
  (425, 3, 232, 3301, 'Cibadak', 'Kecamatan', ''),
  (426, 3, 232, 3302, 'Cibeber', 'Kecamatan', ''),
  (427, 3, 232, 3303, 'Cigemblong', 'Kecamatan', ''),
  (428, 3, 232, 3304, 'Cihara', 'Kecamatan', ''),
  (429, 3, 232, 3305, 'Cijaku', 'Kecamatan', ''),
  (430, 3, 232, 3306, 'Cikulur', 'Kecamatan', ''),
  (431, 3, 232, 3307, 'Cileles', 'Kecamatan', ''),
  (432, 3, 232, 3308, 'Cilograng', 'Kecamatan', ''),
  (433, 3, 232, 3309, 'Cimarga', 'Kecamatan', ''),
  (434, 3, 232, 3310, 'Cipanas', 'Kecamatan', ''),
  (435, 3, 232, 3311, 'Cirinten', 'Kecamatan', ''),
  (436, 3, 232, 3312, 'Curugbitung', 'Kecamatan', ''),
  (437, 3, 232, 3313, 'Gunung Kencana', 'Kecamatan', ''),
  (438, 3, 232, 3314, 'Kalanganyar', 'Kecamatan', ''),
  (439, 3, 232, 3315, 'Lebakgedong', 'Kecamatan', ''),
  (440, 3, 232, 3316, 'Leuwidamar', 'Kecamatan', ''),
  (441, 3, 232, 3317, 'Maja', 'Kecamatan', ''),
  (442, 3, 232, 3318, 'Malingping', 'Kecamatan', ''),
  (443, 3, 232, 3319, 'Muncang', 'Kecamatan', ''),
  (444, 3, 232, 3320, 'Panggarangan', 'Kecamatan', ''),
  (445, 3, 232, 3321, 'Rangkasbitung', 'Kecamatan', ''),
  (446, 3, 232, 3322, 'Sajira', 'Kecamatan', ''),
  (447, 3, 232, 3323, 'Sobang', 'Kecamatan', ''),
  (448, 3, 232, 3324, 'Wanasalam', 'Kecamatan', ''),
  (449, 3, 232, 3325, 'Warunggunung', 'Kecamatan', ''),
  (450, 3, 331, 4655, 'Angsana', 'Kecamatan', ''),
  (451, 3, 331, 4656, 'Banjar', 'Kecamatan', ''),
  (452, 3, 331, 4657, 'Bojong', 'Kecamatan', ''),
  (453, 3, 331, 4658, 'Cadasari', 'Kecamatan', ''),
  (454, 3, 331, 4659, 'Carita', 'Kecamatan', ''),
  (455, 3, 331, 4660, 'Cibaliung', 'Kecamatan', ''),
  (456, 3, 331, 4661, 'Cibitung', 'Kecamatan', ''),
  (457, 3, 331, 4662, 'Cigeulis', 'Kecamatan', ''),
  (458, 3, 331, 4663, 'Cikeudal (Cikedal)', 'Kecamatan', ''),
  (459, 3, 331, 4664, 'Cikeusik', 'Kecamatan', ''),
  (460, 3, 331, 4665, 'Cimanggu', 'Kecamatan', ''),
  (461, 3, 331, 4666, 'Cimanuk', 'Kecamatan', ''),
  (462, 3, 331, 4667, 'Cipeucang', 'Kecamatan', ''),
  (463, 3, 331, 4668, 'Cisata', 'Kecamatan', ''),
  (464, 3, 331, 4669, 'Jiput', 'Kecamatan', ''),
  (465, 3, 331, 4670, 'Kaduhejo', 'Kecamatan', ''),
  (466, 3, 331, 4671, 'Karang Tanjung', 'Kecamatan', ''),
  (467, 3, 331, 4672, 'Koroncong', 'Kecamatan', ''),
  (468, 3, 331, 4673, 'Labuan', 'Kecamatan', ''),
  (469, 3, 331, 4674, 'Majasari', 'Kecamatan', ''),
  (470, 3, 331, 4675, 'Mandalawangi', 'Kecamatan', ''),
  (471, 3, 331, 4676, 'Mekarjaya', 'Kecamatan', ''),
  (472, 3, 331, 4677, 'Menes', 'Kecamatan', ''),
  (473, 3, 331, 4678, 'Munjul', 'Kecamatan', ''),
  (474, 3, 331, 4679, 'Pagelaran', 'Kecamatan', ''),
  (475, 3, 331, 4680, 'Pandeglang', 'Kecamatan', ''),
  (476, 3, 331, 4681, 'Panimbang', 'Kecamatan', ''),
  (477, 3, 331, 4682, 'Patia', 'Kecamatan', ''),
  (478, 3, 331, 4683, 'Picung', 'Kecamatan', ''),
  (479, 3, 331, 4684, 'Pulosari', 'Kecamatan', ''),
  (480, 3, 331, 4685, 'Saketi', 'Kecamatan', ''),
  (481, 3, 331, 4686, 'Sindangresmi', 'Kecamatan', ''),
  (482, 3, 331, 4687, 'Sobang', 'Kecamatan', ''),
  (483, 3, 331, 4688, 'Sukaresmi', 'Kecamatan', ''),
  (484, 3, 331, 4689, 'Sumur', 'Kecamatan', ''),
  (485, 3, 402, 5540, 'Anyar', 'Kecamatan', ''),
  (486, 3, 402, 5541, 'Bandung', 'Kecamatan', ''),
  (487, 3, 402, 5542, 'Baros', 'Kecamatan', ''),
  (488, 3, 402, 5543, 'Binuang', 'Kecamatan', ''),
  (489, 3, 402, 5544, 'Bojonegara', 'Kecamatan', ''),
  (490, 3, 402, 5545, 'Carenang (Cerenang)', 'Kecamatan', ''),
  (491, 3, 402, 5546, 'Cikande', 'Kecamatan', ''),
  (492, 3, 402, 5547, 'Cikeusal', 'Kecamatan', ''),
  (493, 3, 402, 5548, 'Cinangka', 'Kecamatan', ''),
  (494, 3, 402, 5549, 'Ciomas', 'Kecamatan', ''),
  (495, 3, 402, 5550, 'Ciruas', 'Kecamatan', ''),
  (496, 3, 402, 5551, 'Gunungsari', 'Kecamatan', ''),
  (497, 3, 402, 5552, 'Jawilan', 'Kecamatan', ''),
  (498, 3, 402, 5553, 'Kibin', 'Kecamatan', ''),
  (499, 3, 402, 5554, 'Kopo', 'Kecamatan', ''),
  (500, 3, 402, 5555, 'Kragilan', 'Kecamatan', ''),
  (501, 3, 402, 5556, 'Kramatwatu', 'Kecamatan', ''),
  (502, 3, 402, 5557, 'Lebak Wangi', 'Kecamatan', ''),
  (503, 3, 402, 5558, 'Mancak', 'Kecamatan', ''),
  (504, 3, 402, 5559, 'Pabuaran', 'Kecamatan', ''),
  (505, 3, 402, 5560, 'Padarincang', 'Kecamatan', ''),
  (506, 3, 402, 5561, 'Pamarayan', 'Kecamatan', ''),
  (507, 3, 402, 5562, 'Petir', 'Kecamatan', ''),
  (508, 3, 402, 5563, 'Pontang', 'Kecamatan', ''),
  (509, 3, 402, 5564, 'Pulo Ampel', 'Kecamatan', ''),
  (510, 3, 402, 5565, 'Tanara', 'Kecamatan', ''),
  (511, 3, 402, 5566, 'Tirtayasa', 'Kecamatan', ''),
  (512, 3, 402, 5567, 'Tunjung Teja', 'Kecamatan', ''),
  (513, 3, 402, 5568, 'Waringin Kurung', 'Kecamatan', ''),
  (514, 3, 403, 5569, 'Cipocok Jaya', 'Kecamatan', ''),
  (515, 3, 403, 5570, 'Curug', 'Kecamatan', ''),
  (516, 3, 403, 5571, 'Kasemen', 'Kecamatan', ''),
  (517, 3, 403, 5572, 'Serang', 'Kecamatan', ''),
  (518, 3, 403, 5573, 'Taktakan', 'Kecamatan', ''),
  (519, 3, 403, 5574, 'Walantaka', 'Kecamatan', ''),
  (520, 3, 455, 6268, 'Balaraja', 'Kecamatan', ''),
  (521, 3, 455, 6269, 'Cikupa', 'Kecamatan', ''),
  (522, 3, 455, 6270, 'Cisauk', 'Kecamatan', ''),
  (523, 3, 455, 6271, 'Cisoka', 'Kecamatan', ''),
  (524, 3, 455, 6272, 'Curug', 'Kecamatan', ''),
  (525, 3, 455, 6273, 'Gunung Kaler', 'Kecamatan', ''),
  (526, 3, 455, 6274, 'Jambe', 'Kecamatan', ''),
  (527, 3, 455, 6275, 'Jayanti', 'Kecamatan', ''),
  (528, 3, 455, 6276, 'Kelapa Dua', 'Kecamatan', ''),
  (529, 3, 455, 6277, 'Kemiri', 'Kecamatan', ''),
  (530, 3, 455, 6278, 'Kosambi', 'Kecamatan', ''),
  (531, 3, 455, 6279, 'Kresek', 'Kecamatan', ''),
  (532, 3, 455, 6280, 'Kronjo', 'Kecamatan', ''),
  (533, 3, 455, 6281, 'Legok', 'Kecamatan', ''),
  (534, 3, 455, 6282, 'Mauk', 'Kecamatan', ''),
  (535, 3, 455, 6283, 'Mekar Baru', 'Kecamatan', ''),
  (536, 3, 455, 6284, 'Pagedangan', 'Kecamatan', ''),
  (537, 3, 455, 6285, 'Pakuhaji', 'Kecamatan', ''),
  (538, 3, 455, 6286, 'Panongan', 'Kecamatan', ''),
  (539, 3, 455, 6287, 'Pasar Kemis', 'Kecamatan', ''),
  (540, 3, 455, 6288, 'Rajeg', 'Kecamatan', ''),
  (541, 3, 455, 6289, 'Sepatan', 'Kecamatan', ''),
  (542, 3, 455, 6290, 'Sepatan Timur', 'Kecamatan', ''),
  (543, 3, 455, 6291, 'Sindang Jaya', 'Kecamatan', ''),
  (544, 3, 455, 6292, 'Solear', 'Kecamatan', ''),
  (545, 3, 455, 6293, 'Sukadiri', 'Kecamatan', ''),
  (546, 3, 455, 6294, 'Sukamulya', 'Kecamatan', ''),
  (547, 3, 455, 6295, 'Teluknaga', 'Kecamatan', ''),
  (548, 3, 455, 6296, 'Tigaraksa', 'Kecamatan', ''),
  (549, 3, 456, 6297, 'Batuceper', 'Kecamatan', ''),
  (550, 3, 456, 6298, 'Benda', 'Kecamatan', ''),
  (551, 3, 456, 6299, 'Cibodas', 'Kecamatan', ''),
  (552, 3, 456, 6300, 'Ciledug', 'Kecamatan', ''),
  (553, 3, 456, 6301, 'Cipondoh', 'Kecamatan', ''),
  (554, 3, 456, 6302, 'Jatiuwung', 'Kecamatan', ''),
  (555, 3, 456, 6303, 'Karang Tengah', 'Kecamatan', ''),
  (556, 3, 456, 6304, 'Karawaci', 'Kecamatan', ''),
  (557, 3, 456, 6305, 'Larangan', 'Kecamatan', ''),
  (558, 3, 456, 6306, 'Neglasari', 'Kecamatan', ''),
  (559, 3, 456, 6307, 'Periuk', 'Kecamatan', ''),
  (560, 3, 456, 6308, 'Pinang (Penang)', 'Kecamatan', ''),
  (561, 3, 456, 6309, 'Tangerang', 'Kecamatan', ''),
  (562, 3, 457, 6310, 'Ciputat', 'Kecamatan', ''),
  (563, 3, 457, 6311, 'Ciputat Timur', 'Kecamatan', ''),
  (564, 3, 457, 6312, 'Pamulang', 'Kecamatan', ''),
  (565, 3, 457, 6313, 'Pondok Aren', 'Kecamatan', ''),
  (566, 3, 457, 6314, 'Serpong', 'Kecamatan', ''),
  (567, 3, 457, 6315, 'Serpong Utara', 'Kecamatan', ''),
  (568, 3, 457, 6316, 'Setu', 'Kecamatan', ''),
  (569, 4, 62, 0, 'Bengkulu', 'Kota', '38229'),
  (570, 4, 63, 0, 'Bengkulu Selatan', 'Kabupaten', '38519'),
  (571, 4, 64, 0, 'Bengkulu Tengah', 'Kabupaten', '38319'),
  (572, 4, 65, 0, 'Bengkulu Utara', 'Kabupaten', '38619'),
  (573, 4, 175, 0, 'Kaur', 'Kabupaten', '38911'),
  (574, 4, 183, 0, 'Kepahiang', 'Kabupaten', '39319'),
  (575, 4, 233, 0, 'Lebong', 'Kabupaten', '39264'),
  (576, 4, 294, 0, 'Muko Muko', 'Kabupaten', '38715'),
  (577, 4, 379, 0, 'Rejang Lebong', 'Kabupaten', '39112'),
  (578, 4, 397, 0, 'Seluma', 'Kabupaten', '38811'),
  (579, 4, 62, 832, 'Gading Cempaka', 'Kecamatan', ''),
  (580, 4, 62, 833, 'Kampung Melayu', 'Kecamatan', ''),
  (581, 4, 62, 834, 'Muara Bangka Hulu', 'Kecamatan', ''),
  (582, 4, 62, 835, 'Ratu Agung', 'Kecamatan', ''),
  (583, 4, 62, 836, 'Ratu Samban', 'Kecamatan', ''),
  (584, 4, 62, 837, 'Selebar', 'Kecamatan', ''),
  (585, 4, 62, 838, 'Singaran Pati', 'Kecamatan', ''),
  (586, 4, 62, 839, 'Sungai Serut', 'Kecamatan', ''),
  (587, 4, 62, 840, 'Teluk Segara', 'Kecamatan', ''),
  (588, 4, 63, 841, 'Air Nipis', 'Kecamatan', ''),
  (589, 4, 63, 842, 'Bunga Mas', 'Kecamatan', ''),
  (590, 4, 63, 843, 'Kedurang', 'Kecamatan', ''),
  (591, 4, 63, 844, 'Kedurang Ilir', 'Kecamatan', ''),
  (592, 4, 63, 845, 'Kota Manna', 'Kecamatan', ''),
  (593, 4, 63, 846, 'Manna', 'Kecamatan', ''),
  (594, 4, 63, 847, 'Pasar Manna', 'Kecamatan', ''),
  (595, 4, 63, 848, 'Pino', 'Kecamatan', ''),
  (596, 4, 63, 849, 'Pinoraya', 'Kecamatan', ''),
  (597, 4, 63, 850, 'Seginim', 'Kecamatan', ''),
  (598, 4, 63, 851, 'Ulu Manna', 'Kecamatan', ''),
  (599, 4, 64, 852, 'Bang Haji', 'Kecamatan', ''),
  (600, 4, 64, 853, 'Karang Tinggi', 'Kecamatan', ''),
  (601, 4, 64, 854, 'Merigi Kelindang', 'Kecamatan', ''),
  (602, 4, 64, 855, 'Merigi Sakti', 'Kecamatan', ''),
  (603, 4, 64, 856, 'Pagar Jati', 'Kecamatan', ''),
  (604, 4, 64, 857, 'Pematang Tiga', 'Kecamatan', ''),
  (605, 4, 64, 858, 'Pondok Kelapa', 'Kecamatan', ''),
  (606, 4, 64, 859, 'Pondok Kubang', 'Kecamatan', ''),
  (607, 4, 64, 860, 'Taba Penanjung', 'Kecamatan', ''),
  (608, 4, 64, 861, 'Talang Empat', 'Kecamatan', ''),
  (609, 4, 65, 862, 'Air Besi', 'Kecamatan', ''),
  (610, 4, 65, 863, 'Air Napal', 'Kecamatan', ''),
  (611, 4, 65, 864, 'Air Padang', 'Kecamatan', ''),
  (612, 4, 65, 865, 'Arga Makmur', 'Kecamatan', ''),
  (613, 4, 65, 866, 'Arma Jaya', 'Kecamatan', ''),
  (614, 4, 65, 867, 'Batik Nau', 'Kecamatan', ''),
  (615, 4, 65, 868, 'Enggano', 'Kecamatan', ''),
  (616, 4, 65, 869, 'Giri Mulia', 'Kecamatan', ''),
  (617, 4, 65, 870, 'Hulu Palik', 'Kecamatan', ''),
  (618, 4, 65, 871, 'Kerkap', 'Kecamatan', ''),
  (619, 4, 65, 872, 'Ketahun', 'Kecamatan', ''),
  (620, 4, 65, 873, 'Lais', 'Kecamatan', ''),
  (621, 4, 65, 874, 'Napal Putih', 'Kecamatan', ''),
  (622, 4, 65, 875, 'Padang Jaya', 'Kecamatan', ''),
  (623, 4, 65, 876, 'Putri Hijau', 'Kecamatan', ''),
  (624, 4, 65, 877, 'Tanjung Agung Palik', 'Kecamatan', ''),
  (625, 4, 65, 878, 'Ulok Kupai', 'Kecamatan', ''),
  (626, 4, 175, 2450, 'Kaur Selatan', 'Kecamatan', ''),
  (627, 4, 175, 2451, 'Kaur Tengah', 'Kecamatan', ''),
  (628, 4, 175, 2452, 'Kaur Utara', 'Kecamatan', ''),
  (629, 4, 175, 2453, 'Kelam Tengah', 'Kecamatan', ''),
  (630, 4, 175, 2454, 'Kinal', 'Kecamatan', ''),
  (631, 4, 175, 2455, 'Luas', 'Kecamatan', ''),
  (632, 4, 175, 2456, 'Lungkang Kule', 'Kecamatan', ''),
  (633, 4, 175, 2457, 'Maje', 'Kecamatan', ''),
  (634, 4, 175, 2458, 'Muara Sahung', 'Kecamatan', ''),
  (635, 4, 175, 2459, 'Nasal', 'Kecamatan', ''),
  (636, 4, 175, 2460, 'Padang Guci Hilir', 'Kecamatan', ''),
  (637, 4, 175, 2461, 'Padang Guci Hulu', 'Kecamatan', ''),
  (638, 4, 175, 2462, 'Semidang Gumai (Gumay)', 'Kecamatan', ''),
  (639, 4, 175, 2463, 'Tanjung Kemuning', 'Kecamatan', ''),
  (640, 4, 175, 2464, 'Tetap (Muara Tetap)', 'Kecamatan', ''),
  (641, 4, 183, 2563, 'Bermani Ilir', 'Kecamatan', ''),
  (642, 4, 183, 2564, 'Kebawetan (Kabawetan)', 'Kecamatan', ''),
  (643, 4, 183, 2565, 'Kepahiang', 'Kecamatan', ''),
  (644, 4, 183, 2566, 'Merigi', 'Kecamatan', ''),
  (645, 4, 183, 2567, 'Muara Kemumu', 'Kecamatan', ''),
  (646, 4, 183, 2568, 'Seberang Musi', 'Kecamatan', ''),
  (647, 4, 183, 2569, 'Tebat Karai', 'Kecamatan', ''),
  (648, 4, 183, 2570, 'Ujan Mas', 'Kecamatan', ''),
  (649, 4, 233, 3326, 'Amen', 'Kecamatan', ''),
  (650, 4, 233, 3327, 'Bingin Kuning', 'Kecamatan', ''),
  (651, 4, 233, 3328, 'Lebong Atas', 'Kecamatan', ''),
  (652, 4, 233, 3329, 'Lebong Sakti', 'Kecamatan', ''),
  (653, 4, 233, 3330, 'Lebong Selatan', 'Kecamatan', ''),
  (654, 4, 233, 3331, 'Lebong Tengah', 'Kecamatan', ''),
  (655, 4, 233, 3332, 'Lebong Utara', 'Kecamatan', ''),
  (656, 4, 233, 3333, 'Pelabai', 'Kecamatan', ''),
  (657, 4, 233, 3334, 'Pinang Belapis', 'Kecamatan', ''),
  (658, 4, 233, 3335, 'Rimbo Pengadang', 'Kecamatan', ''),
  (659, 4, 233, 3336, 'Topos', 'Kecamatan', ''),
  (660, 4, 233, 3337, 'Uram Jaya', 'Kecamatan', ''),
  (661, 4, 294, 4142, 'Air Dikit', 'Kecamatan', ''),
  (662, 4, 294, 4143, 'Air Majunto', 'Kecamatan', ''),
  (663, 4, 294, 4144, 'Air Rami', 'Kecamatan', ''),
  (664, 4, 294, 4145, 'Ipuh (Muko-Muko Selatan)', 'Kecamatan', ''),
  (665, 4, 294, 4146, 'Kota Mukomuko (Mukomuko Utara)', 'Kecamatan', ''),
  (666, 4, 294, 4147, 'Lubuk Pinang', 'Kecamatan', ''),
  (667, 4, 294, 4148, 'Malin Deman', 'Kecamatan', ''),
  (668, 4, 294, 4149, 'Penarik', 'Kecamatan', ''),
  (669, 4, 294, 4150, 'Pondok Suguh', 'Kecamatan', ''),
  (670, 4, 294, 4151, 'Selagan Raya', 'Kecamatan', ''),
  (671, 4, 294, 4152, 'Sungai Rumbai', 'Kecamatan', ''),
  (672, 4, 294, 4153, 'Teramang Jaya', 'Kecamatan', ''),
  (673, 4, 294, 4154, 'Teras Terunjam', 'Kecamatan', ''),
  (674, 4, 294, 4155, 'V Koto', 'Kecamatan', ''),
  (675, 4, 294, 4156, 'XIV Koto', 'Kecamatan', ''),
  (676, 4, 379, 5274, 'Bermani Ulu', 'Kecamatan', ''),
  (677, 4, 379, 5275, 'Bermani Ulu Raya', 'Kecamatan', ''),
  (678, 4, 379, 5276, 'Binduriang', 'Kecamatan', ''),
  (679, 4, 379, 5277, 'Curup', 'Kecamatan', ''),
  (680, 4, 379, 5278, 'Curup Selatan', 'Kecamatan', ''),
  (681, 4, 379, 5279, 'Curup Tengah', 'Kecamatan', ''),
  (682, 4, 379, 5280, 'Curup Timur', 'Kecamatan', ''),
  (683, 4, 379, 5281, 'Curup Utara', 'Kecamatan', ''),
  (684, 4, 379, 5282, 'Kota Padang', 'Kecamatan', ''),
  (685, 4, 379, 5283, 'Padang Ulak Tanding', 'Kecamatan', ''),
  (686, 4, 379, 5284, 'Selupu Rejang', 'Kecamatan', ''),
  (687, 4, 379, 5285, 'Sindang Beliti Ilir', 'Kecamatan', ''),
  (688, 4, 379, 5286, 'Sindang Beliti Ulu', 'Kecamatan', ''),
  (689, 4, 379, 5287, 'Sindang Daratan', 'Kecamatan', ''),
  (690, 4, 379, 5288, 'Sindang Kelingi', 'Kecamatan', ''),
  (691, 4, 397, 5465, 'Air Periukan', 'Kecamatan', ''),
  (692, 4, 397, 5466, 'Ilir Talo', 'Kecamatan', ''),
  (693, 4, 397, 5467, 'Lubuk Sandi', 'Kecamatan', ''),
  (694, 4, 397, 5468, 'Seluma', 'Kecamatan', ''),
  (695, 4, 397, 5469, 'Seluma Barat', 'Kecamatan', ''),
  (696, 4, 397, 5470, 'Seluma Selatan', 'Kecamatan', ''),
  (697, 4, 397, 5471, 'Seluma Timur', 'Kecamatan', ''),
  (698, 4, 397, 5472, 'Seluma Utara', 'Kecamatan', ''),
  (699, 4, 397, 5473, 'Semidang Alas', 'Kecamatan', ''),
  (700, 4, 397, 5474, 'Semidang Alas Maras', 'Kecamatan', ''),
  (701, 4, 397, 5475, 'Sukaraja', 'Kecamatan', ''),
  (702, 4, 397, 5476, 'Talo', 'Kecamatan', ''),
  (703, 4, 397, 5477, 'Talo Kecil', 'Kecamatan', ''),
  (704, 4, 397, 5478, 'Ulu Talo', 'Kecamatan', ''),
  (705, 5, 135, 1865, 'Gedang Sari', 'Kecamatan', ''),
  (706, 5, 135, 1866, 'Girisubo', 'Kecamatan', ''),
  (707, 5, 135, 1867, 'Karangmojo', 'Kecamatan', ''),
  (708, 5, 135, 1868, 'Ngawen', 'Kecamatan', ''),
  (709, 5, 135, 1869, 'Nglipar', 'Kecamatan', ''),
  (710, 5, 135, 1870, 'Paliyan', 'Kecamatan', ''),
  (711, 5, 135, 1871, 'Panggang', 'Kecamatan', ''),
  (712, 5, 135, 1872, 'Patuk', 'Kecamatan', ''),
  (713, 5, 135, 1873, 'Playen', 'Kecamatan', ''),
  (714, 5, 135, 1874, 'Ponjong', 'Kecamatan', ''),
  (715, 5, 135, 1875, 'Purwosari', 'Kecamatan', ''),
  (716, 5, 135, 1876, 'Rongkop', 'Kecamatan', ''),
  (717, 5, 135, 1877, 'Sapto Sari', 'Kecamatan', ''),
  (718, 5, 135, 1878, 'Semanu', 'Kecamatan', ''),
  (719, 5, 135, 1879, 'Semin', 'Kecamatan', ''),
  (720, 5, 135, 1880, 'Tanjungsari', 'Kecamatan', ''),
  (721, 5, 135, 1881, 'Tepus', 'Kecamatan', ''),
  (722, 5, 135, 1882, 'Wonosari', 'Kecamatan', ''),
  (723, 5, 210, 2930, 'Galur', 'Kecamatan', ''),
  (724, 5, 210, 2931, 'Girimulyo', 'Kecamatan', ''),
  (725, 5, 210, 2932, 'Kalibawang', 'Kecamatan', ''),
  (726, 5, 210, 2933, 'Kokap', 'Kecamatan', ''),
  (727, 5, 210, 2934, 'Lendah', 'Kecamatan', ''),
  (728, 5, 210, 2935, 'Nanggulan', 'Kecamatan', ''),
  (729, 5, 210, 2936, 'Panjatan', 'Kecamatan', ''),
  (730, 5, 210, 2937, 'Pengasih', 'Kecamatan', ''),
  (731, 5, 210, 2938, 'Samigaluh', 'Kecamatan', ''),
  (732, 5, 210, 2939, 'Sentolo', 'Kecamatan', ''),
  (733, 5, 210, 2940, 'Temon', 'Kecamatan', ''),
  (734, 5, 210, 2941, 'Wates', 'Kecamatan', ''),
  (735, 5, 419, 5779, 'Berbah', 'Kecamatan', ''),
  (736, 5, 419, 5780, 'Cangkringan', 'Kecamatan', ''),
  (737, 5, 419, 5781, 'Depok', 'Kecamatan', ''),
  (738, 5, 419, 5782, 'Gamping', 'Kecamatan', ''),
  (739, 5, 419, 5783, 'Godean', 'Kecamatan', ''),
  (740, 5, 419, 5784, 'Kalasan', 'Kecamatan', ''),
  (741, 5, 419, 5785, 'Minggir', 'Kecamatan', ''),
  (742, 5, 419, 5786, 'Mlati', 'Kecamatan', ''),
  (743, 5, 419, 5787, 'Moyudan', 'Kecamatan', ''),
  (744, 5, 419, 5788, 'Ngaglik', 'Kecamatan', ''),
  (745, 5, 419, 5789, 'Ngemplak', 'Kecamatan', ''),
  (746, 5, 419, 5790, 'Pakem', 'Kecamatan', ''),
  (747, 5, 419, 5791, 'Prambanan', 'Kecamatan', ''),
  (748, 5, 419, 5792, 'Seyegan', 'Kecamatan', ''),
  (749, 5, 419, 5793, 'Sleman', 'Kecamatan', ''),
  (750, 5, 419, 5794, 'Tempel', 'Kecamatan', ''),
  (751, 5, 419, 5795, 'Turi', 'Kecamatan', ''),
  (752, 5, 501, 6981, 'Danurejan', 'Kecamatan', ''),
  (753, 5, 501, 6982, 'Gedong Tengen', 'Kecamatan', ''),
  (754, 5, 501, 6983, 'Gondokusuman', 'Kecamatan', ''),
  (755, 5, 501, 6984, 'Gondomanan', 'Kecamatan', ''),
  (756, 5, 501, 6985, 'Jetis', 'Kecamatan', ''),
  (757, 5, 501, 6986, 'Kotagede', 'Kecamatan', ''),
  (758, 5, 501, 6987, 'Kraton', 'Kecamatan', ''),
  (759, 5, 501, 6988, 'Mantrijeron', 'Kecamatan', ''),
  (760, 5, 501, 6989, 'Mergangsan', 'Kecamatan', ''),
  (761, 5, 501, 6990, 'Ngampilan', 'Kecamatan', ''),
  (762, 5, 501, 6991, 'Pakualaman', 'Kecamatan', ''),
  (763, 5, 501, 6992, 'Tegalrejo', 'Kecamatan', ''),
  (764, 5, 501, 6993, 'Umbulharjo', 'Kecamatan', ''),
  (765, 5, 501, 6994, 'Wirobrajan', 'Kecamatan', ''),
  (766, 6, 189, 2622, 'Kepulauan Seribu Selatan', 'Kecamatan', ''),
  (767, 6, 189, 2623, 'Kepulauan Seribu Utara', 'Kecamatan', ''),
  (768, 6, 151, 2087, 'Cengkareng', 'Kecamatan', ''),
  (769, 6, 151, 2088, 'Grogol Petamburan', 'Kecamatan', ''),
  (770, 6, 151, 2089, 'Kalideres', 'Kecamatan', ''),
  (771, 6, 151, 2090, 'Kebon Jeruk', 'Kecamatan', ''),
  (772, 6, 151, 2091, 'Kembangan', 'Kecamatan', ''),
  (773, 6, 151, 2092, 'Palmerah', 'Kecamatan', ''),
  (774, 6, 151, 2093, 'Taman Sari', 'Kecamatan', ''),
  (775, 6, 151, 2094, 'Tambora', 'Kecamatan', ''),
  (776, 6, 153, 2103, 'Cilandak', 'Kecamatan', ''),
  (777, 6, 153, 2104, 'Jagakarsa', 'Kecamatan', ''),
  (778, 6, 153, 2105, 'Kebayoran Baru', 'Kecamatan', ''),
  (779, 6, 153, 2106, 'Kebayoran Lama', 'Kecamatan', ''),
  (780, 6, 153, 2107, 'Mampang Prapatan', 'Kecamatan', ''),
  (781, 6, 153, 2108, 'Pancoran', 'Kecamatan', ''),
  (782, 6, 153, 2109, 'Pasar Minggu', 'Kecamatan', ''),
  (783, 6, 153, 2110, 'Pesanggrahan', 'Kecamatan', ''),
  (784, 6, 153, 2111, 'Setia Budi', 'Kecamatan', ''),
  (785, 6, 153, 2112, 'Tebet', 'Kecamatan', ''),
  (786, 6, 154, 2113, 'Cakung', 'Kecamatan', ''),
  (787, 6, 154, 2114, 'Cipayung', 'Kecamatan', ''),
  (788, 6, 154, 2115, 'Ciracas', 'Kecamatan', ''),
  (789, 6, 154, 2116, 'Duren Sawit', 'Kecamatan', ''),
  (790, 6, 154, 2117, 'Jatinegara', 'Kecamatan', ''),
  (791, 6, 154, 2118, 'Kramat Jati', 'Kecamatan', ''),
  (792, 6, 154, 2119, 'Makasar', 'Kecamatan', ''),
  (793, 6, 154, 2120, 'Matraman', 'Kecamatan', ''),
  (794, 6, 154, 2121, 'Pasar Rebo', 'Kecamatan', ''),
  (795, 6, 154, 2122, 'Pulo Gadung', 'Kecamatan', ''),
  (796, 6, 155, 2123, 'Cilincing', 'Kecamatan', ''),
  (797, 6, 155, 2124, 'Kelapa Gading', 'Kecamatan', ''),
  (798, 6, 155, 2125, 'Koja', 'Kecamatan', ''),
  (799, 6, 155, 2126, 'Pademangan', 'Kecamatan', ''),
  (800, 6, 155, 2127, 'Penjaringan', 'Kecamatan', ''),
  (801, 6, 155, 2128, 'Tanjung Priok', 'Kecamatan', ''),
  (802, 7, 77, 0, 'Boalemo', 'Kabupaten', '96319'),
  (803, 7, 88, 0, 'Bone Bolango', 'Kabupaten', '96511'),
  (804, 7, 129, 0, 'Gorontalo', 'Kabupaten', '96218'),
  (805, 7, 130, 0, 'Gorontalo', 'Kota', '96115'),
  (806, 7, 131, 0, 'Gorontalo Utara', 'Kabupaten', '96611'),
  (807, 7, 361, 0, 'Pohuwato', 'Kabupaten', '96419'),
  (808, 7, 77, 1015, 'Botumoita (Botumoito)', 'Kecamatan', ''),
  (809, 7, 77, 1016, 'Dulupi', 'Kecamatan', ''),
  (810, 7, 77, 1017, 'Mananggu', 'Kecamatan', ''),
  (811, 7, 77, 1018, 'Paguyaman', 'Kecamatan', ''),
  (812, 7, 77, 1019, 'Paguyaman Pantai', 'Kecamatan', ''),
  (813, 7, 77, 1020, 'Tilamuta', 'Kecamatan', ''),
  (814, 7, 77, 1021, 'Wonosari', 'Kecamatan', ''),
  (815, 7, 88, 1199, 'Bone', 'Kecamatan', ''),
  (816, 7, 88, 1200, 'Bone Raya', 'Kecamatan', ''),
  (817, 7, 88, 1201, 'Bonepantai', 'Kecamatan', ''),
  (818, 7, 88, 1202, 'Botu Pingge', 'Kecamatan', ''),
  (819, 7, 88, 1203, 'Bulango Selatan', 'Kecamatan', ''),
  (820, 7, 88, 1204, 'Bulango Timur', 'Kecamatan', ''),
  (821, 7, 88, 1205, 'Bulango Ulu', 'Kecamatan', ''),
  (822, 7, 88, 1206, 'Bulango Utara', 'Kecamatan', ''),
  (823, 7, 88, 1207, 'Bulawa', 'Kecamatan', ''),
  (824, 7, 88, 1208, 'Kabila', 'Kecamatan', ''),
  (825, 7, 88, 1209, 'Kabila Bone', 'Kecamatan', ''),
  (826, 7, 88, 1210, 'Pinogu', 'Kecamatan', ''),
  (827, 7, 88, 1211, 'Suwawa', 'Kecamatan', ''),
  (828, 7, 88, 1212, 'Suwawa Selatan', 'Kecamatan', ''),
  (829, 7, 88, 1213, 'Suwawa Tengah', 'Kecamatan', ''),
  (830, 7, 88, 1214, 'Suwawa Timur', 'Kecamatan', ''),
  (831, 7, 88, 1215, 'Tapa', 'Kecamatan', ''),
  (832, 7, 88, 1216, 'Tilongkabila', 'Kecamatan', ''),
  (833, 7, 129, 1771, 'Asparaga', 'Kecamatan', ''),
  (834, 7, 129, 1772, 'Batudaa', 'Kecamatan', ''),
  (835, 7, 129, 1773, 'Batudaa Pantai', 'Kecamatan', ''),
  (836, 7, 129, 1774, 'Bilato', 'Kecamatan', ''),
  (837, 7, 129, 1775, 'Biluhu', 'Kecamatan', ''),
  (838, 7, 129, 1776, 'Boliohuto (Boliyohuto)', 'Kecamatan', ''),
  (839, 7, 129, 1777, 'Bongomeme', 'Kecamatan', ''),
  (840, 7, 129, 1778, 'Dungaliyo', 'Kecamatan', ''),
  (841, 7, 129, 1779, 'Limboto', 'Kecamatan', ''),
  (842, 7, 129, 1780, 'Limboto Barat', 'Kecamatan', ''),
  (843, 7, 129, 1781, 'Mootilango', 'Kecamatan', ''),
  (844, 7, 129, 1782, 'Pulubala', 'Kecamatan', ''),
  (845, 7, 129, 1783, 'Tabongo', 'Kecamatan', ''),
  (846, 7, 129, 1784, 'Telaga', 'Kecamatan', ''),
  (847, 7, 129, 1785, 'Telaga Biru', 'Kecamatan', ''),
  (848, 7, 129, 1786, 'Telaga Jaya', 'Kecamatan', ''),
  (849, 7, 129, 1787, 'Tibawa', 'Kecamatan', ''),
  (850, 7, 129, 1788, 'Tilango', 'Kecamatan', ''),
  (851, 7, 129, 1789, 'Tolangohula', 'Kecamatan', ''),
  (852, 7, 130, 1790, 'Dumbo Raya', 'Kecamatan', ''),
  (853, 7, 130, 1791, 'Dungingi', 'Kecamatan', ''),
  (854, 7, 130, 1792, 'Hulonthalangi', 'Kecamatan', ''),
  (855, 7, 130, 1793, 'Kota Barat', 'Kecamatan', ''),
  (856, 7, 130, 1794, 'Kota Selatan', 'Kecamatan', ''),
  (857, 7, 130, 1795, 'Kota Tengah', 'Kecamatan', ''),
  (858, 7, 130, 1796, 'Kota Timur', 'Kecamatan', ''),
  (859, 7, 130, 1797, 'Kota Utara', 'Kecamatan', ''),
  (860, 7, 130, 1798, 'Sipatana', 'Kecamatan', ''),
  (861, 7, 131, 1799, 'Anggrek', 'Kecamatan', ''),
  (862, 7, 131, 1800, 'Atinggola', 'Kecamatan', ''),
  (863, 7, 131, 1801, 'Biau', 'Kecamatan', ''),
  (864, 7, 131, 1802, 'Gentuma Raya', 'Kecamatan', ''),
  (865, 7, 131, 1803, 'Kwandang', 'Kecamatan', ''),
  (866, 7, 131, 1804, 'Monano', 'Kecamatan', ''),
  (867, 7, 131, 1805, 'Ponelo Kepulauan', 'Kecamatan', ''),
  (868, 7, 131, 1806, 'Sumalata', 'Kecamatan', ''),
  (869, 7, 131, 1807, 'Sumalata Timur', 'Kecamatan', ''),
  (870, 7, 131, 1808, 'Tolinggula', 'Kecamatan', ''),
  (871, 7, 131, 1809, 'Tomolito', 'Kecamatan', ''),
  (872, 7, 361, 5042, 'Buntulia', 'Kecamatan', ''),
  (873, 7, 361, 5043, 'Dengilo', 'Kecamatan', ''),
  (874, 7, 361, 5044, 'Duhiadaa', 'Kecamatan', ''),
  (875, 7, 361, 5045, 'Lemito', 'Kecamatan', ''),
  (876, 7, 361, 5046, 'Marisa', 'Kecamatan', ''),
  (877, 7, 361, 5047, 'Paguat', 'Kecamatan', ''),
  (878, 7, 361, 5048, 'Patilanggio', 'Kecamatan', ''),
  (879, 7, 361, 5049, 'Popayato', 'Kecamatan', ''),
  (880, 7, 361, 5050, 'Popayato Barat', 'Kecamatan', ''),
  (881, 7, 361, 5051, 'Popayato Timur', 'Kecamatan', ''),
  (882, 7, 361, 5052, 'Randangan', 'Kecamatan', ''),
  (883, 7, 361, 5053, 'Taluditi (Taluduti)', 'Kecamatan', ''),
  (884, 7, 361, 5054, 'Wanggarasi', 'Kecamatan', ''),
  (885, 8, 50, 0, 'Batang Hari', 'Kabupaten', '36613'),
  (886, 8, 97, 0, 'Bungo', 'Kabupaten', '37216'),
  (887, 8, 156, 0, 'Jambi', 'Kota', '36111'),
  (888, 8, 194, 0, 'Kerinci', 'Kabupaten', '37167'),
  (889, 8, 280, 0, 'Merangin', 'Kabupaten', '37319'),
  (890, 8, 293, 0, 'Muaro Jambi', 'Kabupaten', '36311'),
  (891, 8, 393, 0, 'Sarolangun', 'Kabupaten', '37419'),
  (892, 8, 442, 0, 'Sungaipenuh', 'Kota', '37113'),
  (893, 8, 460, 0, 'Tanjung Jabung Barat', 'Kabupaten', '36513'),
  (894, 8, 461, 0, 'Tanjung Jabung Timur', 'Kabupaten', '36719'),
  (895, 8, 471, 0, 'Tebo', 'Kabupaten', '37519'),
  (896, 8, 50, 700, 'Bajubang', 'Kecamatan', ''),
  (897, 8, 50, 701, 'Batin XXIV', 'Kecamatan', ''),
  (898, 8, 50, 702, 'Maro Sebo Ilir', 'Kecamatan', ''),
  (899, 8, 50, 703, 'Maro Sebo Ulu', 'Kecamatan', ''),
  (900, 8, 50, 704, 'Mersam', 'Kecamatan', ''),
  (901, 8, 50, 705, 'Muara Bulian', 'Kecamatan', ''),
  (902, 8, 50, 706, 'Muara Tembesi', 'Kecamatan', ''),
  (903, 8, 50, 707, 'Pemayung', 'Kecamatan', ''),
  (904, 8, 97, 1308, 'Bathin II Babeko', 'Kecamatan', ''),
  (905, 8, 97, 1309, 'Bathin II Pelayang', 'Kecamatan', ''),
  (906, 8, 97, 1310, 'Bathin III', 'Kecamatan', ''),
  (907, 8, 97, 1311, 'Bathin III Ulu', 'Kecamatan', ''),
  (908, 8, 97, 1312, 'Bungo Dani', 'Kecamatan', ''),
  (909, 8, 97, 1313, 'Jujuhan', 'Kecamatan', ''),
  (910, 8, 97, 1314, 'Jujuhan Ilir', 'Kecamatan', ''),
  (911, 8, 97, 1315, 'Limbur Lubuk Mengkuang', 'Kecamatan', ''),
  (912, 8, 97, 1316, 'Muko-Muko Batin VII', 'Kecamatan', ''),
  (913, 8, 97, 1317, 'Pasar Muara Bungo', 'Kecamatan', ''),
  (914, 8, 97, 1318, 'Pelepat', 'Kecamatan', ''),
  (915, 8, 97, 1319, 'Pelepat Ilir', 'Kecamatan', ''),
  (916, 8, 97, 1320, 'Rantau Pandan', 'Kecamatan', ''),
  (917, 8, 97, 1321, 'Rimbo Tengah', 'Kecamatan', ''),
  (918, 8, 97, 1322, 'Tanah Sepenggal', 'Kecamatan', ''),
  (919, 8, 97, 1323, 'Tanah Sepenggal Lintas', 'Kecamatan', ''),
  (920, 8, 97, 1324, 'Tanah Tumbuh', 'Kecamatan', ''),
  (921, 8, 156, 2129, 'Danau Teluk', 'Kecamatan', ''),
  (922, 8, 156, 2130, 'Jambi Selatan', 'Kecamatan', ''),
  (923, 8, 156, 2131, 'Jambi Timur', 'Kecamatan', ''),
  (924, 8, 156, 2132, 'Jelutung', 'Kecamatan', ''),
  (925, 8, 156, 2133, 'Kota Baru', 'Kecamatan', ''),
  (926, 8, 156, 2134, 'Pasar Jambi', 'Kecamatan', ''),
  (927, 8, 156, 2135, 'Pelayangan', 'Kecamatan', ''),
  (928, 8, 156, 2136, 'Telanaipura', 'Kecamatan', ''),
  (929, 8, 194, 2686, 'Air Hangat', 'Kecamatan', ''),
  (930, 8, 194, 2687, 'Air Hangat Barat', 'Kecamatan', ''),
  (931, 8, 194, 2688, 'Air Hangat Timur', 'Kecamatan', ''),
  (932, 8, 194, 2689, 'Batang Merangin', 'Kecamatan', ''),
  (933, 8, 194, 2690, 'Bukitkerman', 'Kecamatan', ''),
  (934, 8, 194, 2691, 'Danau Kerinci', 'Kecamatan', ''),
  (935, 8, 194, 2692, 'Depati Tujuh', 'Kecamatan', ''),
  (936, 8, 194, 2693, 'Gunung Kerinci', 'Kecamatan', ''),
  (937, 8, 194, 2694, 'Gunung Raya', 'Kecamatan', ''),
  (938, 8, 194, 2695, 'Gunung Tujuh', 'Kecamatan', ''),
  (939, 8, 194, 2696, 'Kayu Aro', 'Kecamatan', ''),
  (940, 8, 194, 2697, 'Kayu Aro Barat', 'Kecamatan', ''),
  (941, 8, 194, 2698, 'Keliling Danau', 'Kecamatan', ''),
  (942, 8, 194, 2699, 'Sitinjau Laut', 'Kecamatan', ''),
  (943, 8, 194, 2700, 'Siulak', 'Kecamatan', ''),
  (944, 8, 194, 2701, 'Siulak Mukai', 'Kecamatan', ''),
  (945, 8, 280, 3938, 'Bangko', 'Kecamatan', ''),
  (946, 8, 280, 3939, 'Bangko Barat', 'Kecamatan', ''),
  (947, 8, 280, 3940, 'Batang Masumai', 'Kecamatan', ''),
  (948, 8, 280, 3941, 'Jangkat', 'Kecamatan', ''),
  (949, 8, 280, 3942, 'Lembah Masurai', 'Kecamatan', ''),
  (950, 8, 280, 3943, 'Margo Tabir', 'Kecamatan', ''),
  (951, 8, 280, 3944, 'Muara Siau', 'Kecamatan', ''),
  (952, 8, 280, 3945, 'Nalo Tantan', 'Kecamatan', ''),
  (953, 8, 280, 3946, 'Pamenang', 'Kecamatan', ''),
  (954, 8, 280, 3947, 'Pamenang Barat', 'Kecamatan', ''),
  (955, 8, 280, 3948, 'Pamenang Selatan', 'Kecamatan', ''),
  (956, 8, 280, 3949, 'Pangkalan Jambu', 'Kecamatan', ''),
  (957, 8, 280, 3950, 'Renah Pembarap', 'Kecamatan', ''),
  (958, 8, 280, 3951, 'Renah Pemenang', 'Kecamatan', ''),
  (959, 8, 280, 3952, 'Sungai Manau', 'Kecamatan', ''),
  (960, 8, 280, 3953, 'Sungai Tenang', 'Kecamatan', ''),
  (961, 8, 280, 3954, 'Tabir', 'Kecamatan', ''),
  (962, 8, 280, 3955, 'Tabir Barat', 'Kecamatan', ''),
  (963, 8, 280, 3956, 'Tabir Ilir', 'Kecamatan', ''),
  (964, 8, 280, 3957, 'Tabir Lintas', 'Kecamatan', ''),
  (965, 8, 280, 3958, 'Tabir Selatan', 'Kecamatan', ''),
  (966, 8, 280, 3959, 'Tabir Timur', 'Kecamatan', ''),
  (967, 8, 280, 3960, 'Tabir Ulu', 'Kecamatan', ''),
  (968, 8, 280, 3961, 'Tiang Pumpung', 'Kecamatan', ''),
  (969, 8, 293, 4131, 'Bahar Selatan', 'Kecamatan', ''),
  (970, 8, 293, 4132, 'Bahar Utara', 'Kecamatan', ''),
  (971, 8, 293, 4133, 'Jambi Luar Kota', 'Kecamatan', ''),
  (972, 8, 293, 4134, 'Kumpeh', 'Kecamatan', ''),
  (973, 8, 293, 4135, 'Kumpeh Ulu', 'Kecamatan', ''),
  (974, 8, 293, 4136, 'Maro Sebo', 'Kecamatan', ''),
  (975, 8, 293, 4137, 'Mestong', 'Kecamatan', ''),
  (976, 8, 293, 4138, 'Sekernan', 'Kecamatan', ''),
  (977, 8, 293, 4139, 'Sungai Bahar', 'Kecamatan', ''),
  (978, 8, 293, 4140, 'Sungai Gelam', 'Kecamatan', ''),
  (979, 8, 293, 4141, 'Taman Rajo / Rejo', 'Kecamatan', ''),
  (980, 8, 393, 5433, 'Air Hitam', 'Kecamatan', ''),
  (981, 8, 393, 5434, 'Batang Asai', 'Kecamatan', ''),
  (982, 8, 393, 5435, 'Bathin VIII (Batin VIII)', 'Kecamatan', ''),
  (983, 8, 393, 5436, 'Cermin Nan Gadang', 'Kecamatan', ''),
  (984, 8, 393, 5437, 'Limun', 'Kecamatan', ''),
  (985, 8, 393, 5438, 'Mandiangin', 'Kecamatan', ''),
  (986, 8, 393, 5439, 'Pauh', 'Kecamatan', ''),
  (987, 8, 393, 5440, 'Pelawan', 'Kecamatan', ''),
  (988, 8, 393, 5441, 'Sarolangun', 'Kecamatan', ''),
  (989, 8, 393, 5442, 'Singkut', 'Kecamatan', ''),
  (990, 8, 442, 6118, 'Hamparan Rawang', 'Kecamatan', ''),
  (991, 8, 442, 6119, 'Koto Baru', 'Kecamatan', ''),
  (992, 8, 442, 6120, 'Kumun Debai', 'Kecamatan', ''),
  (993, 8, 442, 6121, 'Pesisir Bukit', 'Kecamatan', ''),
  (994, 8, 442, 6122, 'Pondok Tinggi', 'Kecamatan', ''),
  (995, 8, 442, 6123, 'Sungai Bungkal', 'Kecamatan', ''),
  (996, 8, 442, 6124, 'Sungai Penuh', 'Kecamatan', ''),
  (997, 8, 442, 6125, 'Tanah Kampung', 'Kecamatan', ''),
  (998, 8, 460, 6343, 'Batang Asam', 'Kecamatan', ''),
  (999, 8, 460, 6344, 'Betara', 'Kecamatan', ''),
  (1000, 8, 460, 6345, 'Bram Itam', 'Kecamatan', ''),
  (1001, 8, 460, 6346, 'Kuala Betara', 'Kecamatan', ''),
  (1002, 8, 460, 6347, 'Merlung', 'Kecamatan', ''),
  (1003, 8, 460, 6348, 'Muara Papalik', 'Kecamatan', ''),
  (1004, 8, 460, 6349, 'Pengabuan', 'Kecamatan', '');
INSERT INTO `location` (`location_id`, `province_id`, `city_id`, `subdistrict_id`, `name`, `type`, `postcode`)
VALUES
  (1005, 8, 460, 6350, 'Renah Mendaluh', 'Kecamatan', ''),
  (1006, 8, 460, 6351, 'Seberang Kota', 'Kecamatan', ''),
  (1007, 8, 460, 6352, 'Senyerang', 'Kecamatan', ''),
  (1008, 8, 460, 6353, 'Tebing Tinggi', 'Kecamatan', ''),
  (1009, 8, 460, 6354, 'Tungkal Ilir', 'Kecamatan', ''),
  (1010, 8, 460, 6355, 'Tungkal Ulu', 'Kecamatan', ''),
  (1011, 8, 461, 6356, 'Berbak', 'Kecamatan', ''),
  (1012, 8, 461, 6357, 'Dendang', 'Kecamatan', ''),
  (1013, 8, 461, 6358, 'Geragai', 'Kecamatan', ''),
  (1014, 8, 461, 6359, 'Kuala Jambi', 'Kecamatan', ''),
  (1015, 8, 461, 6360, 'Mendahara', 'Kecamatan', ''),
  (1016, 8, 461, 6361, 'Mendahara Ulu', 'Kecamatan', ''),
  (1017, 8, 461, 6362, 'Muara Sabak Barat', 'Kecamatan', ''),
  (1018, 8, 461, 6363, 'Muara Sabak Timur', 'Kecamatan', ''),
  (1019, 8, 461, 6364, 'Nipah Panjang', 'Kecamatan', ''),
  (1020, 8, 461, 6365, 'Rantau Rasau', 'Kecamatan', ''),
  (1021, 8, 461, 6366, 'Sadu', 'Kecamatan', ''),
  (1022, 8, 471, 6490, 'Muara Tabir', 'Kecamatan', ''),
  (1023, 8, 471, 6491, 'Rimbo Bujang', 'Kecamatan', ''),
  (1024, 8, 471, 6492, 'Rimbo Ilir', 'Kecamatan', ''),
  (1025, 8, 471, 6493, 'Rimbo Ulu', 'Kecamatan', ''),
  (1026, 8, 471, 6494, 'Serai Serumpun', 'Kecamatan', ''),
  (1027, 8, 471, 6495, 'Sumay', 'Kecamatan', ''),
  (1028, 8, 471, 6496, 'Tebo Ilir', 'Kecamatan', ''),
  (1029, 8, 471, 6497, 'Tebo Tengah', 'Kecamatan', ''),
  (1030, 8, 471, 6498, 'Tebo Ulu', 'Kecamatan', ''),
  (1031, 8, 471, 6499, 'Tengah Ilir', 'Kecamatan', ''),
  (1032, 8, 471, 6500, 'VII Koto', 'Kecamatan', ''),
  (1033, 8, 471, 6501, 'VII Koto Ilir', 'Kecamatan', ''),
  (1034, 9, 22, 0, 'Bandung', 'Kabupaten', '40311'),
  (1035, 9, 23, 0, 'Bandung', 'Kota', '40111'),
  (1036, 9, 24, 0, 'Bandung Barat', 'Kabupaten', '40721'),
  (1037, 9, 34, 0, 'Banjar', 'Kota', '46311'),
  (1038, 9, 54, 0, 'Bekasi', 'Kabupaten', '17837'),
  (1039, 9, 55, 0, 'Bekasi', 'Kota', '17121'),
  (1040, 9, 78, 0, 'Bogor', 'Kabupaten', '16911'),
  (1041, 9, 79, 0, 'Bogor', 'Kota', '16119'),
  (1042, 9, 103, 0, 'Ciamis', 'Kabupaten', '46211'),
  (1043, 9, 104, 0, 'Cianjur', 'Kabupaten', '43217'),
  (1044, 9, 107, 0, 'Cimahi', 'Kota', '40512'),
  (1045, 9, 108, 0, 'Cirebon', 'Kabupaten', '45611'),
  (1046, 9, 109, 0, 'Cirebon', 'Kota', '45116'),
  (1047, 9, 115, 0, 'Depok', 'Kota', '16416'),
  (1048, 9, 126, 0, 'Garut', 'Kabupaten', '44126'),
  (1049, 9, 149, 0, 'Indramayu', 'Kabupaten', '45214'),
  (1050, 9, 171, 0, 'Karawang', 'Kabupaten', '41311'),
  (1051, 9, 211, 0, 'Kuningan', 'Kabupaten', '45511'),
  (1052, 9, 252, 0, 'Majalengka', 'Kabupaten', '45412'),
  (1053, 9, 332, 0, 'Pangandaran', 'Kabupaten', '46511'),
  (1054, 9, 376, 0, 'Purwakarta', 'Kabupaten', '41119'),
  (1055, 9, 428, 0, 'Subang', 'Kabupaten', '41215'),
  (1056, 9, 430, 0, 'Sukabumi', 'Kabupaten', '43311'),
  (1057, 9, 431, 0, 'Sukabumi', 'Kota', '43114'),
  (1058, 9, 440, 0, 'Sumedang', 'Kabupaten', '45326'),
  (1059, 9, 468, 0, 'Tasikmalaya', 'Kabupaten', '46411'),
  (1060, 9, 469, 0, 'Tasikmalaya', 'Kota', '46116'),
  (1061, 9, 22, 307, 'Arjasari', 'Kecamatan', ''),
  (1062, 9, 22, 308, 'Baleendah', 'Kecamatan', ''),
  (1063, 9, 22, 309, 'Banjaran', 'Kecamatan', ''),
  (1064, 9, 22, 310, 'Bojongsoang', 'Kecamatan', ''),
  (1065, 9, 22, 311, 'Cangkuang', 'Kecamatan', ''),
  (1066, 9, 22, 312, 'Cicalengka', 'Kecamatan', ''),
  (1067, 9, 22, 313, 'Cikancung', 'Kecamatan', ''),
  (1068, 9, 22, 314, 'Cilengkrang', 'Kecamatan', ''),
  (1069, 9, 22, 315, 'Cileunyi', 'Kecamatan', ''),
  (1070, 9, 22, 316, 'Cimaung', 'Kecamatan', ''),
  (1071, 9, 22, 317, 'Cimeunyan', 'Kecamatan', ''),
  (1072, 9, 22, 318, 'Ciparay', 'Kecamatan', ''),
  (1073, 9, 22, 319, 'Ciwidey', 'Kecamatan', ''),
  (1074, 9, 22, 320, 'Dayeuhkolot', 'Kecamatan', ''),
  (1075, 9, 22, 321, 'Ibun', 'Kecamatan', ''),
  (1076, 9, 22, 322, 'Katapang', 'Kecamatan', ''),
  (1077, 9, 22, 323, 'Kertasari', 'Kecamatan', ''),
  (1078, 9, 22, 324, 'Kutawaringin', 'Kecamatan', ''),
  (1079, 9, 22, 325, 'Majalaya', 'Kecamatan', ''),
  (1080, 9, 22, 326, 'Margaasih', 'Kecamatan', ''),
  (1081, 9, 22, 327, 'Margahayu', 'Kecamatan', ''),
  (1082, 9, 22, 328, 'Nagreg', 'Kecamatan', ''),
  (1083, 9, 22, 329, 'Pacet', 'Kecamatan', ''),
  (1084, 9, 22, 330, 'Pameungpeuk', 'Kecamatan', ''),
  (1085, 9, 22, 331, 'Pangalengan', 'Kecamatan', ''),
  (1086, 9, 22, 332, 'Paseh', 'Kecamatan', ''),
  (1087, 9, 22, 333, 'Pasirjambu', 'Kecamatan', ''),
  (1088, 9, 22, 334, 'Ranca Bali', 'Kecamatan', ''),
  (1089, 9, 22, 335, 'Rancaekek', 'Kecamatan', ''),
  (1090, 9, 22, 336, 'Solokan Jeruk', 'Kecamatan', ''),
  (1091, 9, 22, 337, 'Soreang', 'Kecamatan', ''),
  (1092, 9, 23, 338, 'Andir', 'Kecamatan', ''),
  (1093, 9, 23, 339, 'Antapani (Cicadas)', 'Kecamatan', ''),
  (1094, 9, 23, 340, 'Arcamanik', 'Kecamatan', ''),
  (1095, 9, 23, 341, 'Astana Anyar', 'Kecamatan', ''),
  (1096, 9, 23, 342, 'Babakan Ciparay', 'Kecamatan', ''),
  (1097, 9, 23, 343, 'Bandung Kidul', 'Kecamatan', ''),
  (1098, 9, 23, 344, 'Bandung Kulon', 'Kecamatan', ''),
  (1099, 9, 23, 345, 'Bandung Wetan', 'Kecamatan', ''),
  (1100, 9, 23, 346, 'Batununggal', 'Kecamatan', ''),
  (1101, 9, 23, 347, 'Bojongloa Kaler', 'Kecamatan', ''),
  (1102, 9, 23, 348, 'Bojongloa Kidul', 'Kecamatan', ''),
  (1103, 9, 23, 349, 'Buahbatu (Margacinta)', 'Kecamatan', ''),
  (1104, 9, 23, 350, 'Cibeunying Kaler', 'Kecamatan', ''),
  (1105, 9, 23, 351, 'Cibeunying Kidul', 'Kecamatan', ''),
  (1106, 9, 23, 352, 'Cibiru', 'Kecamatan', ''),
  (1107, 9, 23, 353, 'Cicendo', 'Kecamatan', ''),
  (1108, 9, 23, 354, 'Cidadap', 'Kecamatan', ''),
  (1109, 9, 23, 355, 'Cinambo', 'Kecamatan', ''),
  (1110, 9, 23, 356, 'Coblong', 'Kecamatan', ''),
  (1111, 9, 23, 357, 'Gedebage', 'Kecamatan', ''),
  (1112, 9, 23, 358, 'Kiaracondong', 'Kecamatan', ''),
  (1113, 9, 23, 359, 'Lengkong', 'Kecamatan', ''),
  (1114, 9, 23, 360, 'Mandalajati', 'Kecamatan', ''),
  (1115, 9, 23, 361, 'Panyileukan', 'Kecamatan', ''),
  (1116, 9, 23, 362, 'Rancasari', 'Kecamatan', ''),
  (1117, 9, 23, 363, 'Regol', 'Kecamatan', ''),
  (1118, 9, 23, 364, 'Sukajadi', 'Kecamatan', ''),
  (1119, 9, 23, 365, 'Sukasari', 'Kecamatan', ''),
  (1120, 9, 23, 366, 'Sumur Bandung', 'Kecamatan', ''),
  (1121, 9, 23, 367, 'Ujung Berung', 'Kecamatan', ''),
  (1122, 9, 24, 368, 'Batujajar', 'Kecamatan', ''),
  (1123, 9, 24, 369, 'Cihampelas', 'Kecamatan', ''),
  (1124, 9, 24, 370, 'Cikalong Wetan', 'Kecamatan', ''),
  (1125, 9, 24, 371, 'Cililin', 'Kecamatan', ''),
  (1126, 9, 24, 372, 'Cipatat', 'Kecamatan', ''),
  (1127, 9, 24, 373, 'Cipeundeuy', 'Kecamatan', ''),
  (1128, 9, 24, 374, 'Cipongkor', 'Kecamatan', ''),
  (1129, 9, 24, 375, 'Cisarua', 'Kecamatan', ''),
  (1130, 9, 24, 376, 'Gununghalu', 'Kecamatan', ''),
  (1131, 9, 24, 377, 'Lembang', 'Kecamatan', ''),
  (1132, 9, 24, 378, 'Ngamprah', 'Kecamatan', ''),
  (1133, 9, 24, 379, 'Padalarang', 'Kecamatan', ''),
  (1134, 9, 24, 380, 'Parongpong', 'Kecamatan', ''),
  (1135, 9, 24, 381, 'Rongga', 'Kecamatan', ''),
  (1136, 9, 24, 382, 'Saguling', 'Kecamatan', ''),
  (1137, 9, 24, 383, 'Sindangkerta', 'Kecamatan', ''),
  (1138, 9, 34, 495, 'Banjar', 'Kecamatan', ''),
  (1139, 9, 34, 496, 'Langensari', 'Kecamatan', ''),
  (1140, 9, 34, 497, 'Pataruman', 'Kecamatan', ''),
  (1141, 9, 34, 498, 'Purwaharja', 'Kecamatan', ''),
  (1142, 9, 54, 726, 'Babelan', 'Kecamatan', ''),
  (1143, 9, 54, 727, 'Bojongmangu', 'Kecamatan', ''),
  (1144, 9, 54, 728, 'Cabangbungin', 'Kecamatan', ''),
  (1145, 9, 54, 729, 'Cibarusah', 'Kecamatan', ''),
  (1146, 9, 54, 730, 'Cibitung', 'Kecamatan', ''),
  (1147, 9, 54, 731, 'Cikarang Barat', 'Kecamatan', ''),
  (1148, 9, 54, 732, 'Cikarang Pusat', 'Kecamatan', ''),
  (1149, 9, 54, 733, 'Cikarang Selatan', 'Kecamatan', ''),
  (1150, 9, 54, 734, 'Cikarang Timur', 'Kecamatan', ''),
  (1151, 9, 54, 735, 'Cikarang Utara', 'Kecamatan', ''),
  (1152, 9, 54, 736, 'Karangbahagia', 'Kecamatan', ''),
  (1153, 9, 54, 737, 'Kedung Waringin', 'Kecamatan', ''),
  (1154, 9, 54, 738, 'Muara Gembong', 'Kecamatan', ''),
  (1155, 9, 54, 739, 'Pebayuran', 'Kecamatan', ''),
  (1156, 9, 54, 740, 'Serang Baru', 'Kecamatan', ''),
  (1157, 9, 54, 741, 'Setu', 'Kecamatan', ''),
  (1158, 9, 54, 742, 'Sukakarya', 'Kecamatan', ''),
  (1159, 9, 54, 743, 'Sukatani', 'Kecamatan', ''),
  (1160, 9, 54, 744, 'Sukawangi', 'Kecamatan', ''),
  (1161, 9, 54, 745, 'Tambelang', 'Kecamatan', ''),
  (1162, 9, 54, 746, 'Tambun Selatan', 'Kecamatan', ''),
  (1163, 9, 54, 747, 'Tambun Utara', 'Kecamatan', ''),
  (1164, 9, 54, 748, 'Tarumajaya', 'Kecamatan', ''),
  (1165, 9, 55, 749, 'Bantar Gebang', 'Kecamatan', ''),
  (1166, 9, 55, 750, 'Bekasi Barat', 'Kecamatan', ''),
  (1167, 9, 55, 751, 'Bekasi Selatan', 'Kecamatan', ''),
  (1168, 9, 55, 752, 'Bekasi Timur', 'Kecamatan', ''),
  (1169, 9, 55, 753, 'Bekasi Utara', 'Kecamatan', ''),
  (1170, 9, 55, 754, 'Jati Sampurna', 'Kecamatan', ''),
  (1171, 9, 55, 755, 'Jatiasih', 'Kecamatan', ''),
  (1172, 9, 55, 756, 'Medan Satria', 'Kecamatan', ''),
  (1173, 9, 55, 757, 'Mustika Jaya', 'Kecamatan', ''),
  (1174, 9, 55, 758, 'Pondok Gede', 'Kecamatan', ''),
  (1175, 9, 55, 759, 'Pondok Melati', 'Kecamatan', ''),
  (1176, 9, 55, 760, 'Rawalumbu', 'Kecamatan', ''),
  (1177, 9, 78, 1022, 'Babakan Madang', 'Kecamatan', ''),
  (1178, 9, 78, 1023, 'Bojonggede', 'Kecamatan', ''),
  (1179, 9, 78, 1024, 'Caringin', 'Kecamatan', ''),
  (1180, 9, 78, 1025, 'Cariu', 'Kecamatan', ''),
  (1181, 9, 78, 1026, 'Ciampea', 'Kecamatan', ''),
  (1182, 9, 78, 1027, 'Ciawi', 'Kecamatan', ''),
  (1183, 9, 78, 1028, 'Cibinong', 'Kecamatan', ''),
  (1184, 9, 78, 1029, 'Cibungbulang', 'Kecamatan', ''),
  (1185, 9, 78, 1030, 'Cigombong', 'Kecamatan', ''),
  (1186, 9, 78, 1031, 'Cigudeg', 'Kecamatan', ''),
  (1187, 9, 78, 1032, 'Cijeruk', 'Kecamatan', ''),
  (1188, 9, 78, 1033, 'Cileungsi', 'Kecamatan', ''),
  (1189, 9, 78, 1034, 'Ciomas', 'Kecamatan', ''),
  (1190, 9, 78, 1035, 'Cisarua', 'Kecamatan', ''),
  (1191, 9, 78, 1036, 'Ciseeng', 'Kecamatan', ''),
  (1192, 9, 78, 1037, 'Citeureup', 'Kecamatan', ''),
  (1193, 9, 78, 1038, 'Dramaga', 'Kecamatan', ''),
  (1194, 9, 78, 1039, 'Gunung Putri', 'Kecamatan', ''),
  (1195, 9, 78, 1040, 'Gunung Sindur', 'Kecamatan', ''),
  (1196, 9, 78, 1041, 'Jasinga', 'Kecamatan', ''),
  (1197, 9, 78, 1042, 'Jonggol', 'Kecamatan', ''),
  (1198, 9, 78, 1043, 'Kemang', 'Kecamatan', ''),
  (1199, 9, 78, 1044, 'Klapa Nunggal (Kelapa Nunggal)', 'Kecamatan', ''),
  (1200, 9, 78, 1045, 'Leuwiliang', 'Kecamatan', ''),
  (1201, 9, 78, 1046, 'Leuwisadeng', 'Kecamatan', ''),
  (1202, 9, 78, 1047, 'Megamendung', 'Kecamatan', ''),
  (1203, 9, 78, 1048, 'Nanggung', 'Kecamatan', ''),
  (1204, 9, 78, 1049, 'Pamijahan', 'Kecamatan', ''),
  (1205, 9, 78, 1050, 'Parung', 'Kecamatan', ''),
  (1206, 9, 78, 1051, 'Parung Panjang', 'Kecamatan', ''),
  (1207, 9, 78, 1052, 'Ranca Bungur', 'Kecamatan', ''),
  (1208, 9, 78, 1053, 'Rumpin', 'Kecamatan', ''),
  (1209, 9, 78, 1054, 'Sukajaya', 'Kecamatan', ''),
  (1210, 9, 78, 1055, 'Sukamakmur', 'Kecamatan', ''),
  (1211, 9, 78, 1056, 'Sukaraja', 'Kecamatan', ''),
  (1212, 9, 78, 1057, 'Tajurhalang', 'Kecamatan', ''),
  (1213, 9, 78, 1058, 'Tamansari', 'Kecamatan', ''),
  (1214, 9, 78, 1059, 'Tanjungsari', 'Kecamatan', ''),
  (1215, 9, 78, 1060, 'Tenjo', 'Kecamatan', ''),
  (1216, 9, 78, 1061, 'Tenjolaya', 'Kecamatan', ''),
  (1217, 9, 79, 1062, 'Bogor Barat - Kota', 'Kecamatan', ''),
  (1218, 9, 79, 1063, 'Bogor Selatan - Kota', 'Kecamatan', ''),
  (1219, 9, 79, 1064, 'Bogor Tengah - Kota', 'Kecamatan', ''),
  (1220, 9, 79, 1065, 'Bogor Timur - Kota', 'Kecamatan', ''),
  (1221, 9, 79, 1066, 'Bogor Utara - Kota', 'Kecamatan', ''),
  (1222, 9, 79, 1067, 'Tanah Sereal', 'Kecamatan', ''),
  (1223, 9, 103, 1379, 'Banjarsari', 'Kecamatan', ''),
  (1224, 9, 103, 1380, 'Baregbeg', 'Kecamatan', ''),
  (1225, 9, 103, 1381, 'Ciamis', 'Kecamatan', ''),
  (1226, 9, 103, 1382, 'Cidolog', 'Kecamatan', ''),
  (1227, 9, 103, 1383, 'Cihaurbeuti', 'Kecamatan', ''),
  (1228, 9, 103, 1384, 'Cijeungjing', 'Kecamatan', ''),
  (1229, 9, 103, 1385, 'Cikoneng', 'Kecamatan', ''),
  (1230, 9, 103, 1386, 'Cimaragas', 'Kecamatan', ''),
  (1231, 9, 103, 1387, 'Cipaku', 'Kecamatan', ''),
  (1232, 9, 103, 1388, 'Cisaga', 'Kecamatan', ''),
  (1233, 9, 103, 1389, 'Jatinagara', 'Kecamatan', ''),
  (1234, 9, 103, 1390, 'Kawali', 'Kecamatan', ''),
  (1235, 9, 103, 1391, 'Lakbok', 'Kecamatan', ''),
  (1236, 9, 103, 1392, 'Lumbung', 'Kecamatan', ''),
  (1237, 9, 103, 1393, 'Pamarican', 'Kecamatan', ''),
  (1238, 9, 103, 1394, 'Panawangan', 'Kecamatan', ''),
  (1239, 9, 103, 1395, 'Panjalu', 'Kecamatan', ''),
  (1240, 9, 103, 1396, 'Panumbangan', 'Kecamatan', ''),
  (1241, 9, 103, 1397, 'Purwadadi', 'Kecamatan', ''),
  (1242, 9, 103, 1398, 'Rajadesa', 'Kecamatan', ''),
  (1243, 9, 103, 1399, 'Rancah', 'Kecamatan', ''),
  (1244, 9, 103, 1400, 'Sadananya', 'Kecamatan', ''),
  (1245, 9, 103, 1401, 'Sindangkasih', 'Kecamatan', ''),
  (1246, 9, 103, 1402, 'Sukadana', 'Kecamatan', ''),
  (1247, 9, 103, 1403, 'Sukamantri', 'Kecamatan', ''),
  (1248, 9, 103, 1404, 'Tambaksari', 'Kecamatan', ''),
  (1249, 9, 104, 1405, 'Agrabinta', 'Kecamatan', ''),
  (1250, 9, 104, 1406, 'Bojongpicung', 'Kecamatan', ''),
  (1251, 9, 104, 1407, 'Campaka', 'Kecamatan', ''),
  (1252, 9, 104, 1408, 'Campaka Mulya', 'Kecamatan', ''),
  (1253, 9, 104, 1409, 'Cianjur', 'Kecamatan', ''),
  (1254, 9, 104, 1410, 'Cibeber', 'Kecamatan', ''),
  (1255, 9, 104, 1411, 'Cibinong', 'Kecamatan', ''),
  (1256, 9, 104, 1412, 'Cidaun', 'Kecamatan', ''),
  (1257, 9, 104, 1413, 'Cijati', 'Kecamatan', ''),
  (1258, 9, 104, 1414, 'Cikadu', 'Kecamatan', ''),
  (1259, 9, 104, 1415, 'Cikalongkulon', 'Kecamatan', ''),
  (1260, 9, 104, 1416, 'Cilaku', 'Kecamatan', ''),
  (1261, 9, 104, 1417, 'Cipanas', 'Kecamatan', ''),
  (1262, 9, 104, 1418, 'Ciranjang', 'Kecamatan', ''),
  (1263, 9, 104, 1419, 'Cugenang', 'Kecamatan', ''),
  (1264, 9, 104, 1420, 'Gekbrong', 'Kecamatan', ''),
  (1265, 9, 104, 1421, 'Haurwangi', 'Kecamatan', ''),
  (1266, 9, 104, 1422, 'Kadupandak', 'Kecamatan', ''),
  (1267, 9, 104, 1423, 'Karangtengah', 'Kecamatan', ''),
  (1268, 9, 104, 1424, 'Leles', 'Kecamatan', ''),
  (1269, 9, 104, 1425, 'Mande', 'Kecamatan', ''),
  (1270, 9, 104, 1426, 'Naringgul', 'Kecamatan', ''),
  (1271, 9, 104, 1427, 'Pacet', 'Kecamatan', ''),
  (1272, 9, 104, 1428, 'Pagelaran', 'Kecamatan', ''),
  (1273, 9, 104, 1429, 'Pasirkuda', 'Kecamatan', ''),
  (1274, 9, 104, 1430, 'Sindangbarang', 'Kecamatan', ''),
  (1275, 9, 104, 1431, 'Sukaluyu', 'Kecamatan', ''),
  (1276, 9, 104, 1432, 'Sukanagara', 'Kecamatan', ''),
  (1277, 9, 104, 1433, 'Sukaresmi', 'Kecamatan', ''),
  (1278, 9, 104, 1434, 'Takokak', 'Kecamatan', ''),
  (1279, 9, 104, 1435, 'Tanggeung', 'Kecamatan', ''),
  (1280, 9, 104, 1436, 'Warungkondang', 'Kecamatan', ''),
  (1281, 9, 107, 1469, 'Cimahi Selatan', 'Kecamatan', ''),
  (1282, 9, 107, 1470, 'Cimahi Tengah', 'Kecamatan', ''),
  (1283, 9, 107, 1471, 'Cimahi Utara', 'Kecamatan', ''),
  (1284, 9, 108, 1472, 'Arjawinangun', 'Kecamatan', ''),
  (1285, 9, 108, 1473, 'Astanajapura', 'Kecamatan', ''),
  (1286, 9, 108, 1474, 'Babakan', 'Kecamatan', ''),
  (1287, 9, 108, 1475, 'Beber', 'Kecamatan', ''),
  (1288, 9, 108, 1476, 'Ciledug', 'Kecamatan', ''),
  (1289, 9, 108, 1477, 'Ciwaringin', 'Kecamatan', ''),
  (1290, 9, 108, 1478, 'Depok', 'Kecamatan', ''),
  (1291, 9, 108, 1479, 'Dukupuntang', 'Kecamatan', ''),
  (1292, 9, 108, 1480, 'Gebang', 'Kecamatan', ''),
  (1293, 9, 108, 1481, 'Gegesik', 'Kecamatan', ''),
  (1294, 9, 108, 1482, 'Gempol', 'Kecamatan', ''),
  (1295, 9, 108, 1483, 'Greged (Greget)', 'Kecamatan', ''),
  (1296, 9, 108, 1484, 'Gunung Jati (Cirebon Utara)', 'Kecamatan', ''),
  (1297, 9, 108, 1485, 'Jamblang', 'Kecamatan', ''),
  (1298, 9, 108, 1486, 'Kaliwedi', 'Kecamatan', ''),
  (1299, 9, 108, 1487, 'Kapetakan', 'Kecamatan', ''),
  (1300, 9, 108, 1488, 'Karangsembung', 'Kecamatan', ''),
  (1301, 9, 108, 1489, 'Karangwareng', 'Kecamatan', ''),
  (1302, 9, 108, 1490, 'Kedawung', 'Kecamatan', ''),
  (1303, 9, 108, 1491, 'Klangenan', 'Kecamatan', ''),
  (1304, 9, 108, 1492, 'Lemahabang', 'Kecamatan', ''),
  (1305, 9, 108, 1493, 'Losari', 'Kecamatan', ''),
  (1306, 9, 108, 1494, 'Mundu', 'Kecamatan', ''),
  (1307, 9, 108, 1495, 'Pabedilan', 'Kecamatan', ''),
  (1308, 9, 108, 1496, 'Pabuaran', 'Kecamatan', ''),
  (1309, 9, 108, 1497, 'Palimanan', 'Kecamatan', ''),
  (1310, 9, 108, 1498, 'Pangenan', 'Kecamatan', ''),
  (1311, 9, 108, 1499, 'Panguragan', 'Kecamatan', ''),
  (1312, 9, 108, 1500, 'Pasaleman', 'Kecamatan', ''),
  (1313, 9, 108, 1501, 'Plered', 'Kecamatan', ''),
  (1314, 9, 108, 1502, 'Plumbon', 'Kecamatan', ''),
  (1315, 9, 108, 1503, 'Sedong', 'Kecamatan', ''),
  (1316, 9, 108, 1504, 'Sumber', 'Kecamatan', ''),
  (1317, 9, 108, 1505, 'Suranenggala', 'Kecamatan', ''),
  (1318, 9, 108, 1506, 'Susukan', 'Kecamatan', ''),
  (1319, 9, 108, 1507, 'Susukan Lebak', 'Kecamatan', ''),
  (1320, 9, 108, 1508, 'Talun (Cirebon Selatan)', 'Kecamatan', ''),
  (1321, 9, 108, 1509, 'Tengah Tani', 'Kecamatan', ''),
  (1322, 9, 108, 1510, 'Waled', 'Kecamatan', ''),
  (1323, 9, 108, 1511, 'Weru', 'Kecamatan', ''),
  (1324, 9, 109, 1512, 'Harjamukti', 'Kecamatan', ''),
  (1325, 9, 109, 1513, 'Kejaksan', 'Kecamatan', ''),
  (1326, 9, 109, 1514, 'Kesambi', 'Kecamatan', ''),
  (1327, 9, 109, 1515, 'Lemahwungkuk', 'Kecamatan', ''),
  (1328, 9, 109, 1516, 'Pekalipan', 'Kecamatan', ''),
  (1329, 9, 115, 1577, 'Beji', 'Kecamatan', ''),
  (1330, 9, 115, 1578, 'Bojongsari', 'Kecamatan', ''),
  (1331, 9, 115, 1579, 'Cilodong', 'Kecamatan', ''),
  (1332, 9, 115, 1580, 'Cimanggis', 'Kecamatan', ''),
  (1333, 9, 115, 1581, 'Cinere', 'Kecamatan', ''),
  (1334, 9, 115, 1582, 'Cipayung', 'Kecamatan', ''),
  (1335, 9, 115, 1583, 'Limo', 'Kecamatan', ''),
  (1336, 9, 115, 1584, 'Pancoran Mas', 'Kecamatan', ''),
  (1337, 9, 115, 1585, 'Sawangan', 'Kecamatan', ''),
  (1338, 9, 115, 1586, 'Sukmajaya', 'Kecamatan', ''),
  (1339, 9, 115, 1587, 'Tapos', 'Kecamatan', ''),
  (1340, 9, 126, 1711, 'Banjarwangi', 'Kecamatan', ''),
  (1341, 9, 126, 1712, 'Banyuresmi', 'Kecamatan', ''),
  (1342, 9, 126, 1713, 'Bayongbong', 'Kecamatan', ''),
  (1343, 9, 126, 1714, 'Blubur Limbangan', 'Kecamatan', ''),
  (1344, 9, 126, 1715, 'Bungbulang', 'Kecamatan', ''),
  (1345, 9, 126, 1716, 'Caringin', 'Kecamatan', ''),
  (1346, 9, 126, 1717, 'Cibalong', 'Kecamatan', ''),
  (1347, 9, 126, 1718, 'Cibatu', 'Kecamatan', ''),
  (1348, 9, 126, 1719, 'Cibiuk', 'Kecamatan', ''),
  (1349, 9, 126, 1720, 'Cigedug', 'Kecamatan', ''),
  (1350, 9, 126, 1721, 'Cihurip', 'Kecamatan', ''),
  (1351, 9, 126, 1722, 'Cikajang', 'Kecamatan', ''),
  (1352, 9, 126, 1723, 'Cikelet', 'Kecamatan', ''),
  (1353, 9, 126, 1724, 'Cilawu', 'Kecamatan', ''),
  (1354, 9, 126, 1725, 'Cisewu', 'Kecamatan', ''),
  (1355, 9, 126, 1726, 'Cisompet', 'Kecamatan', ''),
  (1356, 9, 126, 1727, 'Cisurupan', 'Kecamatan', ''),
  (1357, 9, 126, 1728, 'Garut Kota', 'Kecamatan', ''),
  (1358, 9, 126, 1729, 'Kadungora', 'Kecamatan', ''),
  (1359, 9, 126, 1730, 'Karangpawitan', 'Kecamatan', ''),
  (1360, 9, 126, 1731, 'Karangtengah', 'Kecamatan', ''),
  (1361, 9, 126, 1732, 'Kersamanah', 'Kecamatan', ''),
  (1362, 9, 126, 1733, 'Leles', 'Kecamatan', ''),
  (1363, 9, 126, 1734, 'Leuwigoong', 'Kecamatan', ''),
  (1364, 9, 126, 1735, 'Malangbong', 'Kecamatan', ''),
  (1365, 9, 126, 1736, 'Mekarmukti', 'Kecamatan', ''),
  (1366, 9, 126, 1737, 'Pakenjeng', 'Kecamatan', ''),
  (1367, 9, 126, 1738, 'Pameungpeuk', 'Kecamatan', ''),
  (1368, 9, 126, 1739, 'Pamulihan', 'Kecamatan', ''),
  (1369, 9, 126, 1740, 'Pangatikan', 'Kecamatan', ''),
  (1370, 9, 126, 1741, 'Pasirwangi', 'Kecamatan', ''),
  (1371, 9, 126, 1742, 'Peundeuy', 'Kecamatan', ''),
  (1372, 9, 126, 1743, 'Samarang', 'Kecamatan', ''),
  (1373, 9, 126, 1744, 'Selaawi', 'Kecamatan', ''),
  (1374, 9, 126, 1745, 'Singajaya', 'Kecamatan', ''),
  (1375, 9, 126, 1746, 'Sucinaraja', 'Kecamatan', ''),
  (1376, 9, 126, 1747, 'Sukaresmi', 'Kecamatan', ''),
  (1377, 9, 126, 1748, 'Sukawening', 'Kecamatan', ''),
  (1378, 9, 126, 1749, 'Talegong', 'Kecamatan', ''),
  (1379, 9, 126, 1750, 'Tarogong Kaler', 'Kecamatan', ''),
  (1380, 9, 126, 1751, 'Tarogong Kidul', 'Kecamatan', ''),
  (1381, 9, 126, 1752, 'Wanaraja', 'Kecamatan', ''),
  (1382, 9, 149, 2050, 'Anjatan', 'Kecamatan', ''),
  (1383, 9, 149, 2051, 'Arahan', 'Kecamatan', ''),
  (1384, 9, 149, 2052, 'Balongan', 'Kecamatan', ''),
  (1385, 9, 149, 2053, 'Bangodua', 'Kecamatan', ''),
  (1386, 9, 149, 2054, 'Bongas', 'Kecamatan', ''),
  (1387, 9, 149, 2055, 'Cantigi', 'Kecamatan', ''),
  (1388, 9, 149, 2056, 'Cikedung', 'Kecamatan', ''),
  (1389, 9, 149, 2057, 'Gabuswetan', 'Kecamatan', ''),
  (1390, 9, 149, 2058, 'Gantar', 'Kecamatan', ''),
  (1391, 9, 149, 2059, 'Haurgeulis', 'Kecamatan', ''),
  (1392, 9, 149, 2060, 'Indramayu', 'Kecamatan', ''),
  (1393, 9, 149, 2061, 'Jatibarang', 'Kecamatan', ''),
  (1394, 9, 149, 2062, 'Juntinyuat', 'Kecamatan', ''),
  (1395, 9, 149, 2063, 'Kandanghaur', 'Kecamatan', ''),
  (1396, 9, 149, 2064, 'Karangampel', 'Kecamatan', ''),
  (1397, 9, 149, 2065, 'Kedokan Bunder', 'Kecamatan', ''),
  (1398, 9, 149, 2066, 'Kertasemaya', 'Kecamatan', ''),
  (1399, 9, 149, 2067, 'Krangkeng', 'Kecamatan', ''),
  (1400, 9, 149, 2068, 'Kroya', 'Kecamatan', ''),
  (1401, 9, 149, 2069, 'Lelea', 'Kecamatan', ''),
  (1402, 9, 149, 2070, 'Lohbener', 'Kecamatan', ''),
  (1403, 9, 149, 2071, 'Losarang', 'Kecamatan', ''),
  (1404, 9, 149, 2072, 'Pasekan', 'Kecamatan', ''),
  (1405, 9, 149, 2073, 'Patrol', 'Kecamatan', ''),
  (1406, 9, 149, 2074, 'Sindang', 'Kecamatan', ''),
  (1407, 9, 149, 2075, 'Sliyeg', 'Kecamatan', ''),
  (1408, 9, 149, 2076, 'Sukagumiwang', 'Kecamatan', ''),
  (1409, 9, 149, 2077, 'Sukra', 'Kecamatan', ''),
  (1410, 9, 149, 2078, 'Trisi/Terisi', 'Kecamatan', ''),
  (1411, 9, 149, 2079, 'Tukdana', 'Kecamatan', ''),
  (1412, 9, 149, 2080, 'Widasari', 'Kecamatan', ''),
  (1413, 9, 171, 2378, 'Banyusari', 'Kecamatan', ''),
  (1414, 9, 171, 2379, 'Batujaya', 'Kecamatan', ''),
  (1415, 9, 171, 2380, 'Ciampel', 'Kecamatan', ''),
  (1416, 9, 171, 2381, 'Cibuaya', 'Kecamatan', ''),
  (1417, 9, 171, 2382, 'Cikampek', 'Kecamatan', ''),
  (1418, 9, 171, 2383, 'Cilamaya Kulon', 'Kecamatan', ''),
  (1419, 9, 171, 2384, 'Cilamaya Wetan', 'Kecamatan', ''),
  (1420, 9, 171, 2385, 'Cilebar', 'Kecamatan', ''),
  (1421, 9, 171, 2386, 'Jatisari', 'Kecamatan', ''),
  (1422, 9, 171, 2387, 'Jayakerta', 'Kecamatan', ''),
  (1423, 9, 171, 2388, 'Karawang Barat', 'Kecamatan', ''),
  (1424, 9, 171, 2389, 'Karawang Timur', 'Kecamatan', ''),
  (1425, 9, 171, 2390, 'Klari', 'Kecamatan', ''),
  (1426, 9, 171, 2391, 'Kotabaru', 'Kecamatan', ''),
  (1427, 9, 171, 2392, 'Kutawaluya', 'Kecamatan', ''),
  (1428, 9, 171, 2393, 'Lemahabang', 'Kecamatan', ''),
  (1429, 9, 171, 2394, 'Majalaya', 'Kecamatan', ''),
  (1430, 9, 171, 2395, 'Pakisjaya', 'Kecamatan', ''),
  (1431, 9, 171, 2396, 'Pangkalan', 'Kecamatan', ''),
  (1432, 9, 171, 2397, 'Pedes', 'Kecamatan', ''),
  (1433, 9, 171, 2398, 'Purwasari', 'Kecamatan', ''),
  (1434, 9, 171, 2399, 'Rawamerta', 'Kecamatan', ''),
  (1435, 9, 171, 2400, 'Rengasdengklok', 'Kecamatan', ''),
  (1436, 9, 171, 2401, 'Talagasari', 'Kecamatan', ''),
  (1437, 9, 171, 2402, 'Tegalwaru', 'Kecamatan', ''),
  (1438, 9, 171, 2403, 'Telukjambe Barat', 'Kecamatan', ''),
  (1439, 9, 171, 2404, 'Telukjambe Timur', 'Kecamatan', ''),
  (1440, 9, 171, 2405, 'Tempuran', 'Kecamatan', ''),
  (1441, 9, 171, 2406, 'Tirtajaya', 'Kecamatan', ''),
  (1442, 9, 171, 2407, 'Tirtamulya', 'Kecamatan', ''),
  (1443, 9, 211, 2942, 'Ciawigebang', 'Kecamatan', ''),
  (1444, 9, 211, 2943, 'Cibeureum', 'Kecamatan', ''),
  (1445, 9, 211, 2944, 'Cibingbin', 'Kecamatan', ''),
  (1446, 9, 211, 2945, 'Cidahu', 'Kecamatan', ''),
  (1447, 9, 211, 2946, 'Cigandamekar', 'Kecamatan', ''),
  (1448, 9, 211, 2947, 'Cigugur', 'Kecamatan', ''),
  (1449, 9, 211, 2948, 'Cilebak', 'Kecamatan', ''),
  (1450, 9, 211, 2949, 'Cilimus', 'Kecamatan', ''),
  (1451, 9, 211, 2950, 'Cimahi', 'Kecamatan', ''),
  (1452, 9, 211, 2951, 'Ciniru', 'Kecamatan', ''),
  (1453, 9, 211, 2952, 'Cipicung', 'Kecamatan', ''),
  (1454, 9, 211, 2953, 'Ciwaru', 'Kecamatan', ''),
  (1455, 9, 211, 2954, 'Darma', 'Kecamatan', ''),
  (1456, 9, 211, 2955, 'Garawangi', 'Kecamatan', ''),
  (1457, 9, 211, 2956, 'Hantara', 'Kecamatan', ''),
  (1458, 9, 211, 2957, 'Jalaksana', 'Kecamatan', ''),
  (1459, 9, 211, 2958, 'Japara', 'Kecamatan', ''),
  (1460, 9, 211, 2959, 'Kadugede', 'Kecamatan', ''),
  (1461, 9, 211, 2960, 'Kalimanggis', 'Kecamatan', ''),
  (1462, 9, 211, 2961, 'Karangkancana', 'Kecamatan', ''),
  (1463, 9, 211, 2962, 'Kramat Mulya', 'Kecamatan', ''),
  (1464, 9, 211, 2963, 'Kuningan', 'Kecamatan', ''),
  (1465, 9, 211, 2964, 'Lebakwangi', 'Kecamatan', ''),
  (1466, 9, 211, 2965, 'Luragung', 'Kecamatan', ''),
  (1467, 9, 211, 2966, 'Maleber', 'Kecamatan', ''),
  (1468, 9, 211, 2967, 'Mandirancan', 'Kecamatan', ''),
  (1469, 9, 211, 2968, 'Nusaherang', 'Kecamatan', ''),
  (1470, 9, 211, 2969, 'Pancalang', 'Kecamatan', ''),
  (1471, 9, 211, 2970, 'Pasawahan', 'Kecamatan', ''),
  (1472, 9, 211, 2971, 'Selajambe', 'Kecamatan', ''),
  (1473, 9, 211, 2972, 'Sindangagung', 'Kecamatan', ''),
  (1474, 9, 211, 2973, 'Subang', 'Kecamatan', ''),
  (1475, 9, 252, 3553, 'Argapura', 'Kecamatan', ''),
  (1476, 9, 252, 3554, 'Banjaran', 'Kecamatan', ''),
  (1477, 9, 252, 3555, 'Bantarujeg', 'Kecamatan', ''),
  (1478, 9, 252, 3556, 'Cigasong', 'Kecamatan', ''),
  (1479, 9, 252, 3557, 'Cikijing', 'Kecamatan', ''),
  (1480, 9, 252, 3558, 'Cingambul', 'Kecamatan', ''),
  (1481, 9, 252, 3559, 'Dawuan', 'Kecamatan', ''),
  (1482, 9, 252, 3560, 'Jatitujuh', 'Kecamatan', ''),
  (1483, 9, 252, 3561, 'Jatiwangi', 'Kecamatan', ''),
  (1484, 9, 252, 3562, 'Kadipaten', 'Kecamatan', ''),
  (1485, 9, 252, 3563, 'Kasokandel', 'Kecamatan', ''),
  (1486, 9, 252, 3564, 'Kertajati', 'Kecamatan', ''),
  (1487, 9, 252, 3565, 'Lemahsugih', 'Kecamatan', ''),
  (1488, 9, 252, 3566, 'Leuwimunding', 'Kecamatan', ''),
  (1489, 9, 252, 3567, 'Ligung', 'Kecamatan', ''),
  (1490, 9, 252, 3568, 'Maja', 'Kecamatan', ''),
  (1491, 9, 252, 3569, 'Majalengka', 'Kecamatan', ''),
  (1492, 9, 252, 3570, 'Malausma', 'Kecamatan', ''),
  (1493, 9, 252, 3571, 'Palasah', 'Kecamatan', ''),
  (1494, 9, 252, 3572, 'Panyingkiran', 'Kecamatan', ''),
  (1495, 9, 252, 3573, 'Rajagaluh', 'Kecamatan', ''),
  (1496, 9, 252, 3574, 'Sindang', 'Kecamatan', ''),
  (1497, 9, 252, 3575, 'Sindangwangi', 'Kecamatan', ''),
  (1498, 9, 252, 3576, 'Sukahaji', 'Kecamatan', ''),
  (1499, 9, 252, 3577, 'Sumberjaya', 'Kecamatan', ''),
  (1500, 9, 252, 3578, 'Talaga', 'Kecamatan', ''),
  (1501, 9, 332, 4690, 'Cigugur', 'Kecamatan', ''),
  (1502, 9, 332, 4691, 'Cijulang', 'Kecamatan', ''),
  (1503, 9, 332, 4692, 'Cimerak', 'Kecamatan', ''),
  (1504, 9, 332, 4693, 'Kalipucang', 'Kecamatan', ''),
  (1505, 9, 332, 4694, 'Langkaplancar', 'Kecamatan', ''),
  (1506, 9, 332, 4695, 'Mangunjaya', 'Kecamatan', ''),
  (1507, 9, 332, 4696, 'Padaherang', 'Kecamatan', ''),
  (1508, 9, 332, 4697, 'Pangandaran', 'Kecamatan', ''),
  (1509, 9, 332, 4698, 'Parigi', 'Kecamatan', ''),
  (1510, 9, 332, 4699, 'Sidamulih', 'Kecamatan', ''),
  (1511, 9, 376, 5217, 'Babakancikao', 'Kecamatan', ''),
  (1512, 9, 376, 5218, 'Bojong', 'Kecamatan', ''),
  (1513, 9, 376, 5219, 'Bungursari', 'Kecamatan', ''),
  (1514, 9, 376, 5220, 'Campaka', 'Kecamatan', ''),
  (1515, 9, 376, 5221, 'Cibatu', 'Kecamatan', ''),
  (1516, 9, 376, 5222, 'Darangdan', 'Kecamatan', ''),
  (1517, 9, 376, 5223, 'Jatiluhur', 'Kecamatan', ''),
  (1518, 9, 376, 5224, 'Kiarapedes', 'Kecamatan', ''),
  (1519, 9, 376, 5225, 'Maniis', 'Kecamatan', ''),
  (1520, 9, 376, 5226, 'Pasawahan', 'Kecamatan', ''),
  (1521, 9, 376, 5227, 'Plered', 'Kecamatan', ''),
  (1522, 9, 376, 5228, 'Pondoksalam', 'Kecamatan', ''),
  (1523, 9, 376, 5229, 'Purwakarta', 'Kecamatan', ''),
  (1524, 9, 376, 5230, 'Sukasari', 'Kecamatan', ''),
  (1525, 9, 376, 5231, 'Sukatani', 'Kecamatan', ''),
  (1526, 9, 376, 5232, 'Tegal Waru', 'Kecamatan', ''),
  (1527, 9, 376, 5233, 'Wanayasa', 'Kecamatan', ''),
  (1528, 9, 428, 5883, 'Binong', 'Kecamatan', ''),
  (1529, 9, 428, 5884, 'Blanakan', 'Kecamatan', ''),
  (1530, 9, 428, 5885, 'Ciasem', 'Kecamatan', ''),
  (1531, 9, 428, 5886, 'Ciater', 'Kecamatan', ''),
  (1532, 9, 428, 5887, 'Cibogo', 'Kecamatan', ''),
  (1533, 9, 428, 5888, 'Cijambe', 'Kecamatan', ''),
  (1534, 9, 428, 5889, 'Cikaum', 'Kecamatan', ''),
  (1535, 9, 428, 5890, 'Cipeundeuy', 'Kecamatan', ''),
  (1536, 9, 428, 5891, 'Cipunagara', 'Kecamatan', ''),
  (1537, 9, 428, 5892, 'Cisalak', 'Kecamatan', ''),
  (1538, 9, 428, 5893, 'Compreng', 'Kecamatan', ''),
  (1539, 9, 428, 5894, 'Dawuan', 'Kecamatan', ''),
  (1540, 9, 428, 5895, 'Jalancagak', 'Kecamatan', ''),
  (1541, 9, 428, 5896, 'Kalijati', 'Kecamatan', ''),
  (1542, 9, 428, 5897, 'Kasomalang', 'Kecamatan', ''),
  (1543, 9, 428, 5898, 'Legonkulon', 'Kecamatan', ''),
  (1544, 9, 428, 5899, 'Pabuaran', 'Kecamatan', ''),
  (1545, 9, 428, 5900, 'Pagaden', 'Kecamatan', ''),
  (1546, 9, 428, 5901, 'Pagaden Barat', 'Kecamatan', ''),
  (1547, 9, 428, 5902, 'Pamanukan', 'Kecamatan', ''),
  (1548, 9, 428, 5903, 'Patokbeusi', 'Kecamatan', ''),
  (1549, 9, 428, 5904, 'Purwadadi', 'Kecamatan', ''),
  (1550, 9, 428, 5905, 'Pusakajaya', 'Kecamatan', ''),
  (1551, 9, 428, 5906, 'Pusakanagara', 'Kecamatan', ''),
  (1552, 9, 428, 5907, 'Sagalaherang', 'Kecamatan', ''),
  (1553, 9, 428, 5908, 'Serangpanjang', 'Kecamatan', ''),
  (1554, 9, 428, 5909, 'Subang', 'Kecamatan', ''),
  (1555, 9, 428, 5910, 'Sukasari', 'Kecamatan', ''),
  (1556, 9, 428, 5911, 'Tambakdahan', 'Kecamatan', ''),
  (1557, 9, 428, 5912, 'Tanjungsiang', 'Kecamatan', ''),
  (1558, 9, 430, 5918, 'Bantargadung', 'Kecamatan', ''),
  (1559, 9, 430, 5919, 'Bojong Genteng', 'Kecamatan', ''),
  (1560, 9, 430, 5920, 'Caringin', 'Kecamatan', ''),
  (1561, 9, 430, 5921, 'Ciambar', 'Kecamatan', ''),
  (1562, 9, 430, 5922, 'Cibadak', 'Kecamatan', ''),
  (1563, 9, 430, 5923, 'Cibitung', 'Kecamatan', ''),
  (1564, 9, 430, 5924, 'Cicantayan', 'Kecamatan', ''),
  (1565, 9, 430, 5925, 'Cicurug', 'Kecamatan', ''),
  (1566, 9, 430, 5926, 'Cidadap', 'Kecamatan', ''),
  (1567, 9, 430, 5927, 'Cidahu', 'Kecamatan', ''),
  (1568, 9, 430, 5928, 'Cidolog', 'Kecamatan', ''),
  (1569, 9, 430, 5929, 'Ciemas', 'Kecamatan', ''),
  (1570, 9, 430, 5930, 'Cikakak', 'Kecamatan', ''),
  (1571, 9, 430, 5931, 'Cikembar', 'Kecamatan', ''),
  (1572, 9, 430, 5932, 'Cikidang', 'Kecamatan', ''),
  (1573, 9, 430, 5933, 'Cimanggu', 'Kecamatan', ''),
  (1574, 9, 430, 5934, 'Ciracap', 'Kecamatan', ''),
  (1575, 9, 430, 5935, 'Cireunghas', 'Kecamatan', ''),
  (1576, 9, 430, 5936, 'Cisaat', 'Kecamatan', ''),
  (1577, 9, 430, 5937, 'Cisolok', 'Kecamatan', ''),
  (1578, 9, 430, 5938, 'Curugkembar', 'Kecamatan', ''),
  (1579, 9, 430, 5939, 'Geger Bitung', 'Kecamatan', ''),
  (1580, 9, 430, 5940, 'Gunung Guruh', 'Kecamatan', ''),
  (1581, 9, 430, 5941, 'Jampang Kulon', 'Kecamatan', ''),
  (1582, 9, 430, 5942, 'Jampang Tengah', 'Kecamatan', ''),
  (1583, 9, 430, 5943, 'Kabandungan', 'Kecamatan', ''),
  (1584, 9, 430, 5944, 'Kadudampit', 'Kecamatan', ''),
  (1585, 9, 430, 5945, 'Kalapa Nunggal', 'Kecamatan', ''),
  (1586, 9, 430, 5946, 'Kali Bunder', 'Kecamatan', ''),
  (1587, 9, 430, 5947, 'Kebonpedes', 'Kecamatan', ''),
  (1588, 9, 430, 5948, 'Lengkong', 'Kecamatan', ''),
  (1589, 9, 430, 5949, 'Nagrak', 'Kecamatan', ''),
  (1590, 9, 430, 5950, 'Nyalindung', 'Kecamatan', ''),
  (1591, 9, 430, 5951, 'Pabuaran', 'Kecamatan', ''),
  (1592, 9, 430, 5952, 'Parakan Salak', 'Kecamatan', ''),
  (1593, 9, 430, 5953, 'Parung Kuda', 'Kecamatan', ''),
  (1594, 9, 430, 5954, 'Pelabuhan/Palabuhan Ratu', 'Kecamatan', ''),
  (1595, 9, 430, 5955, 'Purabaya', 'Kecamatan', ''),
  (1596, 9, 430, 5956, 'Sagaranten', 'Kecamatan', ''),
  (1597, 9, 430, 5957, 'Simpenan', 'Kecamatan', ''),
  (1598, 9, 430, 5958, 'Sukabumi', 'Kecamatan', ''),
  (1599, 9, 430, 5959, 'Sukalarang', 'Kecamatan', ''),
  (1600, 9, 430, 5960, 'Sukaraja', 'Kecamatan', ''),
  (1601, 9, 430, 5961, 'Surade', 'Kecamatan', ''),
  (1602, 9, 430, 5962, 'Tegal Buleud', 'Kecamatan', ''),
  (1603, 9, 430, 5963, 'Waluran', 'Kecamatan', ''),
  (1604, 9, 430, 5964, 'Warung Kiara', 'Kecamatan', ''),
  (1605, 9, 431, 5965, 'Baros', 'Kecamatan', ''),
  (1606, 9, 431, 5966, 'Cibeureum', 'Kecamatan', ''),
  (1607, 9, 431, 5967, 'Cikole', 'Kecamatan', ''),
  (1608, 9, 431, 5968, 'Citamiang', 'Kecamatan', ''),
  (1609, 9, 431, 5969, 'Gunung Puyuh', 'Kecamatan', ''),
  (1610, 9, 431, 5970, 'Lembursitu', 'Kecamatan', ''),
  (1611, 9, 431, 5971, 'Warudoyong', 'Kecamatan', ''),
  (1612, 9, 440, 6065, 'Buahdua', 'Kecamatan', ''),
  (1613, 9, 440, 6066, 'Cibugel', 'Kecamatan', ''),
  (1614, 9, 440, 6067, 'Cimalaka', 'Kecamatan', ''),
  (1615, 9, 440, 6068, 'Cimanggung', 'Kecamatan', ''),
  (1616, 9, 440, 6069, 'Cisarua', 'Kecamatan', ''),
  (1617, 9, 440, 6070, 'Cisitu', 'Kecamatan', ''),
  (1618, 9, 440, 6071, 'Conggeang', 'Kecamatan', ''),
  (1619, 9, 440, 6072, 'Darmaraja', 'Kecamatan', ''),
  (1620, 9, 440, 6073, 'Ganeas', 'Kecamatan', ''),
  (1621, 9, 440, 6074, 'Jatigede', 'Kecamatan', ''),
  (1622, 9, 440, 6075, 'Jatinangor', 'Kecamatan', ''),
  (1623, 9, 440, 6076, 'Jatinunggal', 'Kecamatan', ''),
  (1624, 9, 440, 6077, 'Pamulihan', 'Kecamatan', ''),
  (1625, 9, 440, 6078, 'Paseh', 'Kecamatan', ''),
  (1626, 9, 440, 6079, 'Rancakalong', 'Kecamatan', ''),
  (1627, 9, 440, 6080, 'Situraja', 'Kecamatan', ''),
  (1628, 9, 440, 6081, 'Sukasari', 'Kecamatan', ''),
  (1629, 9, 440, 6082, 'Sumedang Selatan', 'Kecamatan', ''),
  (1630, 9, 440, 6083, 'Sumedang Utara', 'Kecamatan', ''),
  (1631, 9, 440, 6084, 'Surian', 'Kecamatan', ''),
  (1632, 9, 440, 6085, 'Tanjungkerta', 'Kecamatan', ''),
  (1633, 9, 440, 6086, 'Tanjungmedar', 'Kecamatan', ''),
  (1634, 9, 440, 6087, 'Tanjungsari', 'Kecamatan', ''),
  (1635, 9, 440, 6088, 'Tomo', 'Kecamatan', ''),
  (1636, 9, 440, 6089, 'Ujungjaya', 'Kecamatan', ''),
  (1637, 9, 440, 6090, 'Wado', 'Kecamatan', ''),
  (1638, 9, 468, 6436, 'Bantarkalong', 'Kecamatan', ''),
  (1639, 9, 468, 6437, 'Bojongasih', 'Kecamatan', ''),
  (1640, 9, 468, 6438, 'Bojonggambir', 'Kecamatan', ''),
  (1641, 9, 468, 6439, 'Ciawi', 'Kecamatan', ''),
  (1642, 9, 468, 6440, 'Cibalong', 'Kecamatan', ''),
  (1643, 9, 468, 6441, 'Cigalontang', 'Kecamatan', ''),
  (1644, 9, 468, 6442, 'Cikalong', 'Kecamatan', ''),
  (1645, 9, 468, 6443, 'Cikatomas', 'Kecamatan', ''),
  (1646, 9, 468, 6444, 'Cineam', 'Kecamatan', ''),
  (1647, 9, 468, 6445, 'Cipatujah', 'Kecamatan', ''),
  (1648, 9, 468, 6446, 'Cisayong', 'Kecamatan', ''),
  (1649, 9, 468, 6447, 'Culamega', 'Kecamatan', ''),
  (1650, 9, 468, 6448, 'Gunung Tanjung', 'Kecamatan', ''),
  (1651, 9, 468, 6449, 'Jamanis', 'Kecamatan', ''),
  (1652, 9, 468, 6450, 'Jatiwaras', 'Kecamatan', ''),
  (1653, 9, 468, 6451, 'Kadipaten', 'Kecamatan', ''),
  (1654, 9, 468, 6452, 'Karang Jaya', 'Kecamatan', ''),
  (1655, 9, 468, 6453, 'Karangnunggal', 'Kecamatan', ''),
  (1656, 9, 468, 6454, 'Leuwisari', 'Kecamatan', ''),
  (1657, 9, 468, 6455, 'Mangunreja', 'Kecamatan', ''),
  (1658, 9, 468, 6456, 'Manonjaya', 'Kecamatan', ''),
  (1659, 9, 468, 6457, 'Padakembang', 'Kecamatan', ''),
  (1660, 9, 468, 6458, 'Pagerageung', 'Kecamatan', ''),
  (1661, 9, 468, 6459, 'Pancatengah', 'Kecamatan', ''),
  (1662, 9, 468, 6460, 'Parungponteng', 'Kecamatan', ''),
  (1663, 9, 468, 6461, 'Puspahiang', 'Kecamatan', ''),
  (1664, 9, 468, 6462, 'Rajapolah', 'Kecamatan', ''),
  (1665, 9, 468, 6463, 'Salawu', 'Kecamatan', ''),
  (1666, 9, 468, 6464, 'Salopa', 'Kecamatan', ''),
  (1667, 9, 468, 6465, 'Sariwangi', 'Kecamatan', ''),
  (1668, 9, 468, 6466, 'Singaparna', 'Kecamatan', ''),
  (1669, 9, 468, 6467, 'Sodonghilir', 'Kecamatan', ''),
  (1670, 9, 468, 6468, 'Sukahening', 'Kecamatan', ''),
  (1671, 9, 468, 6469, 'Sukaraja', 'Kecamatan', ''),
  (1672, 9, 468, 6470, 'Sukarame', 'Kecamatan', ''),
  (1673, 9, 468, 6471, 'Sukaratu', 'Kecamatan', ''),
  (1674, 9, 468, 6472, 'Sukaresik', 'Kecamatan', ''),
  (1675, 9, 468, 6473, 'Tanjungjaya', 'Kecamatan', ''),
  (1676, 9, 468, 6474, 'Taraju', 'Kecamatan', ''),
  (1677, 9, 469, 6475, 'Bungursari', 'Kecamatan', ''),
  (1678, 9, 469, 6476, 'Cibeureum', 'Kecamatan', ''),
  (1679, 9, 469, 6477, 'Cihideung', 'Kecamatan', ''),
  (1680, 9, 469, 6478, 'Cipedes', 'Kecamatan', ''),
  (1681, 9, 469, 6479, 'Indihiang', 'Kecamatan', ''),
  (1682, 9, 469, 6480, 'Kawalu', 'Kecamatan', ''),
  (1683, 9, 469, 6481, 'Mangkubumi', 'Kecamatan', ''),
  (1684, 9, 469, 6482, 'Purbaratu', 'Kecamatan', ''),
  (1685, 9, 469, 6483, 'Tamansari', 'Kecamatan', ''),
  (1686, 9, 469, 6484, 'Tawang', 'Kecamatan', ''),
  (1687, 10, 37, 0, 'Banjarnegara', 'Kabupaten', '53419'),
  (1688, 10, 41, 0, 'Banyumas', 'Kabupaten', '53114'),
  (1689, 10, 49, 0, 'Batang', 'Kabupaten', '51211'),
  (1690, 10, 76, 0, 'Blora', 'Kabupaten', '58219'),
  (1691, 10, 91, 0, 'Boyolali', 'Kabupaten', '57312'),
  (1692, 10, 92, 0, 'Brebes', 'Kabupaten', '52212'),
  (1693, 10, 105, 0, 'Cilacap', 'Kabupaten', '53211'),
  (1694, 10, 113, 0, 'Demak', 'Kabupaten', '59519'),
  (1695, 10, 134, 0, 'Grobogan', 'Kabupaten', '58111'),
  (1696, 10, 163, 0, 'Jepara', 'Kabupaten', '59419'),
  (1697, 10, 169, 0, 'Karanganyar', 'Kabupaten', '57718'),
  (1698, 10, 177, 0, 'Kebumen', 'Kabupaten', '54319'),
  (1699, 10, 181, 0, 'Kendal', 'Kabupaten', '51314'),
  (1700, 10, 196, 0, 'Klaten', 'Kabupaten', '57411'),
  (1701, 10, 209, 0, 'Kudus', 'Kabupaten', '59311'),
  (1702, 10, 249, 0, 'Magelang', 'Kabupaten', '56519'),
  (1703, 10, 250, 0, 'Magelang', 'Kota', '56133'),
  (1704, 10, 344, 0, 'Pati', 'Kabupaten', '59114'),
  (1705, 10, 348, 0, 'Pekalongan', 'Kabupaten', '51161'),
  (1706, 10, 349, 0, 'Pekalongan', 'Kota', '51122'),
  (1707, 10, 352, 0, 'Pemalang', 'Kabupaten', '52319'),
  (1708, 10, 375, 0, 'Purbalingga', 'Kabupaten', '53312'),
  (1709, 10, 377, 0, 'Purworejo', 'Kabupaten', '54111'),
  (1710, 10, 380, 0, 'Rembang', 'Kabupaten', '59219'),
  (1711, 10, 386, 0, 'Salatiga', 'Kota', '50711'),
  (1712, 10, 398, 0, 'Semarang', 'Kabupaten', '50511'),
  (1713, 10, 399, 0, 'Semarang', 'Kota', '50135'),
  (1714, 10, 427, 0, 'Sragen', 'Kabupaten', '57211'),
  (1715, 10, 433, 0, 'Sukoharjo', 'Kabupaten', '57514'),
  (1716, 10, 445, 0, 'Surakarta (Solo)', 'Kota', '57113'),
  (1717, 10, 472, 0, 'Tegal', 'Kabupaten', '52419'),
  (1718, 10, 473, 0, 'Tegal', 'Kota', '52114'),
  (1719, 10, 476, 0, 'Temanggung', 'Kabupaten', '56212'),
  (1720, 10, 497, 0, 'Wonogiri', 'Kabupaten', '57619'),
  (1721, 10, 498, 0, 'Wonosobo', 'Kabupaten', '56311'),
  (1722, 10, 37, 509, 'Banjarmangu', 'Kecamatan', ''),
  (1723, 10, 37, 510, 'Banjarnegara', 'Kecamatan', ''),
  (1724, 10, 37, 511, 'Batur', 'Kecamatan', ''),
  (1725, 10, 37, 512, 'Bawang', 'Kecamatan', ''),
  (1726, 10, 37, 513, 'Kalibening', 'Kecamatan', ''),
  (1727, 10, 37, 514, 'Karangkobar', 'Kecamatan', ''),
  (1728, 10, 37, 515, 'Madukara', 'Kecamatan', ''),
  (1729, 10, 37, 516, 'Mandiraja', 'Kecamatan', ''),
  (1730, 10, 37, 517, 'Pagedongan', 'Kecamatan', ''),
  (1731, 10, 37, 518, 'Pagentan', 'Kecamatan', ''),
  (1732, 10, 37, 519, 'Pandanarum', 'Kecamatan', ''),
  (1733, 10, 37, 520, 'Pejawaran', 'Kecamatan', ''),
  (1734, 10, 37, 521, 'Punggelan', 'Kecamatan', ''),
  (1735, 10, 37, 522, 'Purwonegoro', 'Kecamatan', ''),
  (1736, 10, 37, 523, 'Purworejo Klampok', 'Kecamatan', ''),
  (1737, 10, 37, 524, 'Rakit', 'Kecamatan', ''),
  (1738, 10, 37, 525, 'Sigaluh', 'Kecamatan', ''),
  (1739, 10, 37, 526, 'Susukan', 'Kecamatan', ''),
  (1740, 10, 37, 527, 'Wanadadi (Wonodadi)', 'Kecamatan', ''),
  (1741, 10, 37, 528, 'Wanayasa', 'Kecamatan', ''),
  (1742, 10, 49, 685, 'Bandar', 'Kecamatan', ''),
  (1743, 10, 49, 686, 'Banyuputih', 'Kecamatan', ''),
  (1744, 10, 49, 687, 'Batang', 'Kecamatan', ''),
  (1745, 10, 49, 688, 'Bawang', 'Kecamatan', ''),
  (1746, 10, 49, 689, 'Blado', 'Kecamatan', ''),
  (1747, 10, 49, 690, 'Gringsing', 'Kecamatan', ''),
  (1748, 10, 49, 691, 'Kandeman', 'Kecamatan', ''),
  (1749, 10, 49, 692, 'Limpung', 'Kecamatan', ''),
  (1750, 10, 49, 693, 'Pecalungan', 'Kecamatan', ''),
  (1751, 10, 49, 694, 'Reban', 'Kecamatan', ''),
  (1752, 10, 49, 695, 'Subah', 'Kecamatan', ''),
  (1753, 10, 49, 696, 'Tersono', 'Kecamatan', ''),
  (1754, 10, 49, 697, 'Tulis', 'Kecamatan', ''),
  (1755, 10, 49, 698, 'Warungasem', 'Kecamatan', ''),
  (1756, 10, 49, 699, 'Wonotunggal', 'Kecamatan', ''),
  (1757, 10, 41, 573, 'Ajibarang', 'Kecamatan', ''),
  (1758, 10, 41, 574, 'Banyumas', 'Kecamatan', ''),
  (1759, 10, 41, 575, 'Baturaden', 'Kecamatan', ''),
  (1760, 10, 41, 576, 'Cilongok', 'Kecamatan', ''),
  (1761, 10, 41, 577, 'Gumelar', 'Kecamatan', ''),
  (1762, 10, 41, 578, 'Jatilawang', 'Kecamatan', ''),
  (1763, 10, 41, 579, 'Kalibagor', 'Kecamatan', ''),
  (1764, 10, 41, 580, 'Karanglewas', 'Kecamatan', ''),
  (1765, 10, 41, 581, 'Kebasen', 'Kecamatan', ''),
  (1766, 10, 41, 582, 'Kedung Banteng', 'Kecamatan', ''),
  (1767, 10, 41, 583, 'Kembaran', 'Kecamatan', ''),
  (1768, 10, 41, 584, 'Kemranjen', 'Kecamatan', ''),
  (1769, 10, 41, 585, 'Lumbir', 'Kecamatan', ''),
  (1770, 10, 41, 586, 'Patikraja', 'Kecamatan', ''),
  (1771, 10, 41, 587, 'Pekuncen', 'Kecamatan', ''),
  (1772, 10, 41, 588, 'Purwojati', 'Kecamatan', ''),
  (1773, 10, 41, 589, 'Purwokerto Barat', 'Kecamatan', ''),
  (1774, 10, 41, 590, 'Purwokerto Selatan', 'Kecamatan', ''),
  (1775, 10, 41, 591, 'Purwokerto Timur', 'Kecamatan', ''),
  (1776, 10, 41, 592, 'Purwokerto Utara', 'Kecamatan', ''),
  (1777, 10, 41, 593, 'Rawalo', 'Kecamatan', ''),
  (1778, 10, 41, 594, 'Sokaraja', 'Kecamatan', ''),
  (1779, 10, 41, 595, 'Somagede', 'Kecamatan', ''),
  (1780, 10, 41, 596, 'Sumbang', 'Kecamatan', ''),
  (1781, 10, 41, 597, 'Sumpiuh', 'Kecamatan', ''),
  (1782, 10, 41, 598, 'Tambak', 'Kecamatan', ''),
  (1783, 10, 41, 599, 'Wangon', 'Kecamatan', ''),
  (1784, 10, 76, 999, 'Banjarejo', 'Kecamatan', ''),
  (1785, 10, 76, 1000, 'Blora kota', 'Kecamatan', ''),
  (1786, 10, 76, 1001, 'Bogorejo', 'Kecamatan', ''),
  (1787, 10, 76, 1002, 'Cepu', 'Kecamatan', ''),
  (1788, 10, 76, 1003, 'Japah', 'Kecamatan', ''),
  (1789, 10, 76, 1004, 'Jati', 'Kecamatan', ''),
  (1790, 10, 76, 1005, 'Jepon', 'Kecamatan', ''),
  (1791, 10, 76, 1006, 'Jiken', 'Kecamatan', ''),
  (1792, 10, 76, 1007, 'Kedungtuban', 'Kecamatan', ''),
  (1793, 10, 76, 1008, 'Kradenan', 'Kecamatan', ''),
  (1794, 10, 76, 1009, 'Kunduran', 'Kecamatan', ''),
  (1795, 10, 76, 1010, 'Ngawen', 'Kecamatan', ''),
  (1796, 10, 76, 1011, 'Randublatung', 'Kecamatan', ''),
  (1797, 10, 76, 1012, 'Sambong', 'Kecamatan', ''),
  (1798, 10, 76, 1013, 'Todanan', 'Kecamatan', ''),
  (1799, 10, 76, 1014, 'Tunjungan', 'Kecamatan', ''),
  (1800, 10, 91, 1240, 'Ampel', 'Kecamatan', ''),
  (1801, 10, 91, 1241, 'Andong', 'Kecamatan', ''),
  (1802, 10, 91, 1242, 'Banyudono', 'Kecamatan', ''),
  (1803, 10, 91, 1243, 'Boyolali', 'Kecamatan', ''),
  (1804, 10, 91, 1244, 'Cepogo', 'Kecamatan', ''),
  (1805, 10, 91, 1245, 'Juwangi', 'Kecamatan', ''),
  (1806, 10, 91, 1246, 'Karanggede', 'Kecamatan', ''),
  (1807, 10, 91, 1247, 'Kemusu', 'Kecamatan', ''),
  (1808, 10, 91, 1248, 'Klego', 'Kecamatan', ''),
  (1809, 10, 91, 1249, 'Mojosongo', 'Kecamatan', ''),
  (1810, 10, 91, 1250, 'Musuk', 'Kecamatan', ''),
  (1811, 10, 91, 1251, 'Ngemplak', 'Kecamatan', ''),
  (1812, 10, 91, 1252, 'Nogosari', 'Kecamatan', ''),
  (1813, 10, 91, 1253, 'Sambi', 'Kecamatan', ''),
  (1814, 10, 91, 1254, 'Sawit', 'Kecamatan', ''),
  (1815, 10, 91, 1255, 'Selo', 'Kecamatan', ''),
  (1816, 10, 91, 1256, 'Simo', 'Kecamatan', ''),
  (1817, 10, 91, 1257, 'Teras', 'Kecamatan', ''),
  (1818, 10, 91, 1258, 'Wonosegoro', 'Kecamatan', ''),
  (1819, 10, 92, 1259, 'Banjarharjo', 'Kecamatan', ''),
  (1820, 10, 92, 1260, 'Bantarkawung', 'Kecamatan', ''),
  (1821, 10, 92, 1261, 'Brebes', 'Kecamatan', ''),
  (1822, 10, 92, 1262, 'Bulakamba', 'Kecamatan', ''),
  (1823, 10, 92, 1263, 'Bumiayu', 'Kecamatan', ''),
  (1824, 10, 92, 1264, 'Jatibarang', 'Kecamatan', ''),
  (1825, 10, 92, 1265, 'Kersana', 'Kecamatan', ''),
  (1826, 10, 92, 1266, 'Ketanggungan', 'Kecamatan', ''),
  (1827, 10, 92, 1267, 'Larangan', 'Kecamatan', ''),
  (1828, 10, 92, 1268, 'Losari', 'Kecamatan', ''),
  (1829, 10, 92, 1269, 'Paguyangan', 'Kecamatan', ''),
  (1830, 10, 92, 1270, 'Salem', 'Kecamatan', ''),
  (1831, 10, 92, 1271, 'Sirampog', 'Kecamatan', ''),
  (1832, 10, 92, 1272, 'Songgom', 'Kecamatan', ''),
  (1833, 10, 92, 1273, 'Tanjung', 'Kecamatan', ''),
  (1834, 10, 92, 1274, 'Tonjong', 'Kecamatan', ''),
  (1835, 10, 92, 1275, 'Wanasari', 'Kecamatan', ''),
  (1836, 10, 105, 1437, 'Adipala', 'Kecamatan', ''),
  (1837, 10, 105, 1438, 'Bantarsari', 'Kecamatan', ''),
  (1838, 10, 105, 1439, 'Binangun', 'Kecamatan', ''),
  (1839, 10, 105, 1440, 'Cilacap Selatan', 'Kecamatan', ''),
  (1840, 10, 105, 1441, 'Cilacap Tengah', 'Kecamatan', ''),
  (1841, 10, 105, 1442, 'Cilacap Utara', 'Kecamatan', ''),
  (1842, 10, 105, 1443, 'Cimanggu', 'Kecamatan', ''),
  (1843, 10, 105, 1444, 'Cipari', 'Kecamatan', ''),
  (1844, 10, 105, 1445, 'Dayeuhluhur', 'Kecamatan', ''),
  (1845, 10, 105, 1446, 'Gandrungmangu', 'Kecamatan', ''),
  (1846, 10, 105, 1447, 'Jeruklegi', 'Kecamatan', ''),
  (1847, 10, 105, 1448, 'Kampung Laut', 'Kecamatan', ''),
  (1848, 10, 105, 1449, 'Karangpucung', 'Kecamatan', ''),
  (1849, 10, 105, 1450, 'Kawunganten', 'Kecamatan', ''),
  (1850, 10, 105, 1451, 'Kedungreja', 'Kecamatan', ''),
  (1851, 10, 105, 1452, 'Kesugihan', 'Kecamatan', ''),
  (1852, 10, 105, 1453, 'Kroya', 'Kecamatan', ''),
  (1853, 10, 105, 1454, 'Majenang', 'Kecamatan', ''),
  (1854, 10, 105, 1455, 'Maos', 'Kecamatan', ''),
  (1855, 10, 105, 1456, 'Nusawungu', 'Kecamatan', ''),
  (1856, 10, 105, 1457, 'Patimuan', 'Kecamatan', ''),
  (1857, 10, 105, 1458, 'Sampang', 'Kecamatan', ''),
  (1858, 10, 105, 1459, 'Sidareja', 'Kecamatan', ''),
  (1859, 10, 105, 1460, 'Wanareja', 'Kecamatan', ''),
  (1860, 10, 113, 1559, 'Bonang', 'Kecamatan', ''),
  (1861, 10, 113, 1560, 'Demak', 'Kecamatan', ''),
  (1862, 10, 113, 1561, 'Dempet', 'Kecamatan', ''),
  (1863, 10, 113, 1562, 'Gajah', 'Kecamatan', ''),
  (1864, 10, 113, 1563, 'Guntur', 'Kecamatan', ''),
  (1865, 10, 113, 1564, 'Karang Tengah', 'Kecamatan', ''),
  (1866, 10, 113, 1565, 'Karanganyar', 'Kecamatan', ''),
  (1867, 10, 113, 1566, 'Karangawen', 'Kecamatan', ''),
  (1868, 10, 113, 1567, 'Kebonagung', 'Kecamatan', ''),
  (1869, 10, 113, 1568, 'Mijen', 'Kecamatan', ''),
  (1870, 10, 113, 1569, 'Mranggen', 'Kecamatan', ''),
  (1871, 10, 113, 1570, 'Sayung', 'Kecamatan', ''),
  (1872, 10, 113, 1571, 'Wedung', 'Kecamatan', ''),
  (1873, 10, 113, 1572, 'Wonosalam', 'Kecamatan', ''),
  (1874, 10, 134, 1846, 'Brati', 'Kecamatan', ''),
  (1875, 10, 134, 1847, 'Gabus', 'Kecamatan', ''),
  (1876, 10, 134, 1848, 'Geyer', 'Kecamatan', ''),
  (1877, 10, 134, 1849, 'Godong', 'Kecamatan', ''),
  (1878, 10, 134, 1850, 'Grobogan', 'Kecamatan', ''),
  (1879, 10, 134, 1851, 'Gubug', 'Kecamatan', ''),
  (1880, 10, 134, 1852, 'Karangrayung', 'Kecamatan', ''),
  (1881, 10, 134, 1853, 'Kedungjati', 'Kecamatan', ''),
  (1882, 10, 134, 1854, 'Klambu', 'Kecamatan', ''),
  (1883, 10, 134, 1855, 'Kradenan', 'Kecamatan', ''),
  (1884, 10, 134, 1856, 'Ngaringan', 'Kecamatan', ''),
  (1885, 10, 134, 1857, 'Penawangan', 'Kecamatan', ''),
  (1886, 10, 134, 1858, 'Pulokulon', 'Kecamatan', ''),
  (1887, 10, 134, 1859, 'Purwodadi', 'Kecamatan', ''),
  (1888, 10, 134, 1860, 'Tanggungharjo', 'Kecamatan', ''),
  (1889, 10, 134, 1861, 'Tawangharjo', 'Kecamatan', ''),
  (1890, 10, 134, 1862, 'Tegowanu', 'Kecamatan', ''),
  (1891, 10, 134, 1863, 'Toroh', 'Kecamatan', ''),
  (1892, 10, 134, 1864, 'Wirosari', 'Kecamatan', ''),
  (1893, 10, 163, 2248, 'Bangsri', 'Kecamatan', ''),
  (1894, 10, 163, 2249, 'Batealit', 'Kecamatan', ''),
  (1895, 10, 163, 2250, 'Donorojo', 'Kecamatan', ''),
  (1896, 10, 163, 2251, 'Jepara', 'Kecamatan', ''),
  (1897, 10, 163, 2252, 'Kalinyamatan', 'Kecamatan', ''),
  (1898, 10, 163, 2253, 'Karimunjawa', 'Kecamatan', ''),
  (1899, 10, 163, 2254, 'Kedung', 'Kecamatan', ''),
  (1900, 10, 163, 2255, 'Keling', 'Kecamatan', ''),
  (1901, 10, 163, 2256, 'Kembang', 'Kecamatan', ''),
  (1902, 10, 163, 2257, 'Mayong', 'Kecamatan', ''),
  (1903, 10, 163, 2258, 'Mlonggo', 'Kecamatan', ''),
  (1904, 10, 163, 2259, 'Nalumsari', 'Kecamatan', ''),
  (1905, 10, 163, 2260, 'Pakis Aji', 'Kecamatan', ''),
  (1906, 10, 163, 2261, 'Pecangaan', 'Kecamatan', ''),
  (1907, 10, 163, 2262, 'Tahunan', 'Kecamatan', ''),
  (1908, 10, 163, 2263, 'Welahan', 'Kecamatan', ''),
  (1909, 10, 169, 2353, 'Colomadu', 'Kecamatan', ''),
  (1910, 10, 169, 2354, 'Gondangrejo', 'Kecamatan', ''),
  (1911, 10, 169, 2355, 'Jaten', 'Kecamatan', ''),
  (1912, 10, 169, 2356, 'Jatipuro', 'Kecamatan', ''),
  (1913, 10, 169, 2357, 'Jatiyoso', 'Kecamatan', ''),
  (1914, 10, 169, 2358, 'Jenawi', 'Kecamatan', ''),
  (1915, 10, 169, 2359, 'Jumantono', 'Kecamatan', ''),
  (1916, 10, 169, 2360, 'Jumapolo', 'Kecamatan', ''),
  (1917, 10, 169, 2361, 'Karanganyar', 'Kecamatan', ''),
  (1918, 10, 169, 2362, 'Karangpandan', 'Kecamatan', ''),
  (1919, 10, 169, 2363, 'Kebakkramat', 'Kecamatan', ''),
  (1920, 10, 169, 2364, 'Kerjo', 'Kecamatan', ''),
  (1921, 10, 169, 2365, 'Matesih', 'Kecamatan', ''),
  (1922, 10, 169, 2366, 'Mojogedang', 'Kecamatan', ''),
  (1923, 10, 169, 2367, 'Ngargoyoso', 'Kecamatan', ''),
  (1924, 10, 169, 2368, 'Tasikmadu', 'Kecamatan', ''),
  (1925, 10, 169, 2369, 'Tawangmangu', 'Kecamatan', ''),
  (1926, 10, 177, 2471, 'Adimulyo', 'Kecamatan', ''),
  (1927, 10, 177, 2472, 'Alian/Aliyan', 'Kecamatan', ''),
  (1928, 10, 177, 2473, 'Ambal', 'Kecamatan', ''),
  (1929, 10, 177, 2474, 'Ayah', 'Kecamatan', ''),
  (1930, 10, 177, 2475, 'Bonorowo', 'Kecamatan', ''),
  (1931, 10, 177, 2476, 'Buayan', 'Kecamatan', ''),
  (1932, 10, 177, 2477, 'Buluspesantren', 'Kecamatan', ''),
  (1933, 10, 177, 2478, 'Gombong', 'Kecamatan', ''),
  (1934, 10, 177, 2479, 'Karanganyar', 'Kecamatan', ''),
  (1935, 10, 177, 2480, 'Karanggayam', 'Kecamatan', ''),
  (1936, 10, 177, 2481, 'Karangsambung', 'Kecamatan', ''),
  (1937, 10, 177, 2482, 'Kebumen', 'Kecamatan', ''),
  (1938, 10, 177, 2483, 'Klirong', 'Kecamatan', ''),
  (1939, 10, 177, 2484, 'Kutowinangun', 'Kecamatan', ''),
  (1940, 10, 177, 2485, 'Kuwarasan', 'Kecamatan', ''),
  (1941, 10, 177, 2486, 'Mirit', 'Kecamatan', ''),
  (1942, 10, 177, 2487, 'Padureso', 'Kecamatan', ''),
  (1943, 10, 177, 2488, 'Pejagoan', 'Kecamatan', ''),
  (1944, 10, 177, 2489, 'Petanahan', 'Kecamatan', ''),
  (1945, 10, 177, 2490, 'Poncowarno', 'Kecamatan', ''),
  (1946, 10, 177, 2491, 'Prembun', 'Kecamatan', ''),
  (1947, 10, 177, 2492, 'Puring', 'Kecamatan', ''),
  (1948, 10, 177, 2493, 'Rowokele', 'Kecamatan', ''),
  (1949, 10, 177, 2494, 'Sadang', 'Kecamatan', ''),
  (1950, 10, 177, 2495, 'Sempor', 'Kecamatan', ''),
  (1951, 10, 177, 2496, 'Sruweng', 'Kecamatan', ''),
  (1952, 10, 181, 2533, 'Boja', 'Kecamatan', ''),
  (1953, 10, 181, 2534, 'Brangsong', 'Kecamatan', ''),
  (1954, 10, 181, 2535, 'Cepiring', 'Kecamatan', ''),
  (1955, 10, 181, 2536, 'Gemuh', 'Kecamatan', ''),
  (1956, 10, 181, 2537, 'Kaliwungu', 'Kecamatan', ''),
  (1957, 10, 181, 2538, 'Kaliwungu Selatan', 'Kecamatan', ''),
  (1958, 10, 181, 2539, 'Kangkung', 'Kecamatan', ''),
  (1959, 10, 181, 2540, 'Kendal', 'Kecamatan', ''),
  (1960, 10, 181, 2541, 'Limbangan', 'Kecamatan', ''),
  (1961, 10, 181, 2542, 'Ngampel', 'Kecamatan', ''),
  (1962, 10, 181, 2543, 'Pagerruyung', 'Kecamatan', ''),
  (1963, 10, 181, 2544, 'Patean', 'Kecamatan', ''),
  (1964, 10, 181, 2545, 'Patebon', 'Kecamatan', ''),
  (1965, 10, 181, 2546, 'Pegandon', 'Kecamatan', ''),
  (1966, 10, 181, 2547, 'Plantungan', 'Kecamatan', ''),
  (1967, 10, 181, 2548, 'Ringinarum', 'Kecamatan', ''),
  (1968, 10, 181, 2549, 'Rowosari', 'Kecamatan', ''),
  (1969, 10, 181, 2550, 'Singorojo', 'Kecamatan', ''),
  (1970, 10, 181, 2551, 'Sukorejo', 'Kecamatan', ''),
  (1971, 10, 181, 2552, 'Weleri', 'Kecamatan', ''),
  (1972, 10, 196, 2722, 'Bayat', 'Kecamatan', ''),
  (1973, 10, 196, 2723, 'Cawas', 'Kecamatan', ''),
  (1974, 10, 196, 2724, 'Ceper', 'Kecamatan', ''),
  (1975, 10, 196, 2725, 'Delanggu', 'Kecamatan', ''),
  (1976, 10, 196, 2726, 'Gantiwarno', 'Kecamatan', ''),
  (1977, 10, 196, 2727, 'Jatinom', 'Kecamatan', ''),
  (1978, 10, 196, 2728, 'Jogonalan', 'Kecamatan', ''),
  (1979, 10, 196, 2729, 'Juwiring', 'Kecamatan', ''),
  (1980, 10, 196, 2730, 'Kalikotes', 'Kecamatan', ''),
  (1981, 10, 196, 2731, 'Karanganom', 'Kecamatan', ''),
  (1982, 10, 196, 2732, 'Karangdowo', 'Kecamatan', ''),
  (1983, 10, 196, 2733, 'Karangnongko', 'Kecamatan', ''),
  (1984, 10, 196, 2734, 'Kebonarum', 'Kecamatan', ''),
  (1985, 10, 196, 2735, 'Kemalang', 'Kecamatan', ''),
  (1986, 10, 196, 2736, 'Klaten Selatan', 'Kecamatan', ''),
  (1987, 10, 196, 2737, 'Klaten Tengah', 'Kecamatan', ''),
  (1988, 10, 196, 2738, 'Klaten Utara', 'Kecamatan', ''),
  (1989, 10, 196, 2739, 'Manisrenggo', 'Kecamatan', ''),
  (1990, 10, 196, 2740, 'Ngawen', 'Kecamatan', ''),
  (1991, 10, 196, 2741, 'Pedan', 'Kecamatan', ''),
  (1992, 10, 196, 2742, 'Polanharjo', 'Kecamatan', ''),
  (1993, 10, 196, 2743, 'Prambanan', 'Kecamatan', ''),
  (1994, 10, 196, 2744, 'Trucuk', 'Kecamatan', ''),
  (1995, 10, 196, 2745, 'Tulung', 'Kecamatan', ''),
  (1996, 10, 196, 2746, 'Wedi', 'Kecamatan', ''),
  (1997, 10, 196, 2747, 'Wonosari', 'Kecamatan', ''),
  (1998, 10, 209, 2921, 'Bae', 'Kecamatan', ''),
  (1999, 10, 209, 2922, 'Dawe', 'Kecamatan', ''),
  (2000, 10, 209, 2923, 'Gebog', 'Kecamatan', ''),
  (2001, 10, 209, 2924, 'Jati', 'Kecamatan', ''),
  (2002, 10, 209, 2925, 'Jekulo', 'Kecamatan', ''),
  (2003, 10, 209, 2926, 'Kaliwungu', 'Kecamatan', ''),
  (2004, 10, 209, 2927, 'Kudus Kota', 'Kecamatan', ''),
  (2005, 10, 209, 2928, 'Mejobo', 'Kecamatan', '');
INSERT INTO `location` (`location_id`, `province_id`, `city_id`, `subdistrict_id`, `name`, `type`, `postcode`)
VALUES
  (2006, 10, 209, 2929, 'Undaan', 'Kecamatan', ''),
  (2007, 10, 249, 3511, 'Bandongan', 'Kecamatan', ''),
  (2008, 10, 249, 3512, 'Borobudur', 'Kecamatan', ''),
  (2009, 10, 249, 3513, 'Candimulyo', 'Kecamatan', ''),
  (2010, 10, 249, 3514, 'Dukun', 'Kecamatan', ''),
  (2011, 10, 249, 3515, 'Grabag', 'Kecamatan', ''),
  (2012, 10, 249, 3516, 'Kajoran', 'Kecamatan', ''),
  (2013, 10, 249, 3517, 'Kaliangkrik', 'Kecamatan', ''),
  (2014, 10, 249, 3518, 'Mertoyudan', 'Kecamatan', ''),
  (2015, 10, 249, 3519, 'Mungkid', 'Kecamatan', ''),
  (2016, 10, 249, 3520, 'Muntilan', 'Kecamatan', ''),
  (2017, 10, 249, 3521, 'Ngablak', 'Kecamatan', ''),
  (2018, 10, 249, 3522, 'Ngluwar', 'Kecamatan', ''),
  (2019, 10, 249, 3523, 'Pakis', 'Kecamatan', ''),
  (2020, 10, 249, 3524, 'Salam', 'Kecamatan', ''),
  (2021, 10, 249, 3525, 'Salaman', 'Kecamatan', ''),
  (2022, 10, 249, 3526, 'Sawangan', 'Kecamatan', ''),
  (2023, 10, 249, 3527, 'Secang', 'Kecamatan', ''),
  (2024, 10, 249, 3528, 'Srumbung', 'Kecamatan', ''),
  (2025, 10, 249, 3529, 'Tegalrejo', 'Kecamatan', ''),
  (2026, 10, 249, 3530, 'Tempuran', 'Kecamatan', ''),
  (2027, 10, 249, 3531, 'Windusari', 'Kecamatan', ''),
  (2028, 10, 250, 3532, 'Magelang Selatan', 'Kecamatan', ''),
  (2029, 10, 250, 3533, 'Magelang Tengah', 'Kecamatan', ''),
  (2030, 10, 250, 3534, 'Magelang Utara', 'Kecamatan', ''),
  (2031, 10, 344, 4821, 'Batangan', 'Kecamatan', ''),
  (2032, 10, 344, 4822, 'Cluwak', 'Kecamatan', ''),
  (2033, 10, 344, 4823, 'Dukuhseti', 'Kecamatan', ''),
  (2034, 10, 344, 4824, 'Gabus', 'Kecamatan', ''),
  (2035, 10, 344, 4825, 'Gembong', 'Kecamatan', ''),
  (2036, 10, 344, 4826, 'Gunungwungkal', 'Kecamatan', ''),
  (2037, 10, 344, 4827, 'Jaken', 'Kecamatan', ''),
  (2038, 10, 344, 4828, 'Jakenan', 'Kecamatan', ''),
  (2039, 10, 344, 4829, 'Juwana', 'Kecamatan', ''),
  (2040, 10, 344, 4830, 'Kayen', 'Kecamatan', ''),
  (2041, 10, 344, 4831, 'Margorejo', 'Kecamatan', ''),
  (2042, 10, 344, 4832, 'Margoyoso', 'Kecamatan', ''),
  (2043, 10, 344, 4833, 'Pati', 'Kecamatan', ''),
  (2044, 10, 344, 4834, 'Pucakwangi', 'Kecamatan', ''),
  (2045, 10, 344, 4835, 'Sukolilo', 'Kecamatan', ''),
  (2046, 10, 344, 4836, 'Tambakromo', 'Kecamatan', ''),
  (2047, 10, 344, 4837, 'Tayu', 'Kecamatan', ''),
  (2048, 10, 344, 4838, 'Tlogowungu', 'Kecamatan', ''),
  (2049, 10, 344, 4839, 'Trangkil', 'Kecamatan', ''),
  (2050, 10, 344, 4840, 'Wedarijaksa', 'Kecamatan', ''),
  (2051, 10, 344, 4841, 'Winong', 'Kecamatan', ''),
  (2052, 10, 348, 4891, 'Bojong', 'Kecamatan', ''),
  (2053, 10, 348, 4892, 'Buaran', 'Kecamatan', ''),
  (2054, 10, 348, 4893, 'Doro', 'Kecamatan', ''),
  (2055, 10, 348, 4894, 'Kajen', 'Kecamatan', ''),
  (2056, 10, 348, 4895, 'Kandangserang', 'Kecamatan', ''),
  (2057, 10, 348, 4896, 'Karanganyar', 'Kecamatan', ''),
  (2058, 10, 348, 4897, 'Karangdadap', 'Kecamatan', ''),
  (2059, 10, 348, 4898, 'Kedungwuni', 'Kecamatan', ''),
  (2060, 10, 348, 4899, 'Kesesi', 'Kecamatan', ''),
  (2061, 10, 348, 4900, 'Lebakbarang', 'Kecamatan', ''),
  (2062, 10, 348, 4901, 'Paninggaran', 'Kecamatan', ''),
  (2063, 10, 348, 4902, 'Petungkriono/Petungkriyono', 'Kecamatan', ''),
  (2064, 10, 348, 4903, 'Siwalan', 'Kecamatan', ''),
  (2065, 10, 348, 4904, 'Sragi', 'Kecamatan', ''),
  (2066, 10, 348, 4905, 'Talun', 'Kecamatan', ''),
  (2067, 10, 348, 4906, 'Tirto', 'Kecamatan', ''),
  (2068, 10, 348, 4907, 'Wiradesa', 'Kecamatan', ''),
  (2069, 10, 348, 4908, 'Wonokerto', 'Kecamatan', ''),
  (2070, 10, 348, 4909, 'Wonopringgo', 'Kecamatan', ''),
  (2071, 10, 349, 4910, 'Pekalongan Barat', 'Kecamatan', ''),
  (2072, 10, 349, 4911, 'Pekalongan Selatan', 'Kecamatan', ''),
  (2073, 10, 349, 4912, 'Pekalongan Timur', 'Kecamatan', ''),
  (2074, 10, 349, 4913, 'Pekalongan Utara', 'Kecamatan', ''),
  (2075, 10, 352, 4938, 'Ampelgading', 'Kecamatan', ''),
  (2076, 10, 352, 4939, 'Bantarbolang', 'Kecamatan', ''),
  (2077, 10, 352, 4940, 'Belik', 'Kecamatan', ''),
  (2078, 10, 352, 4941, 'Bodeh', 'Kecamatan', ''),
  (2079, 10, 352, 4942, 'Comal', 'Kecamatan', ''),
  (2080, 10, 352, 4943, 'Moga', 'Kecamatan', ''),
  (2081, 10, 352, 4944, 'Pemalang', 'Kecamatan', ''),
  (2082, 10, 352, 4945, 'Petarukan', 'Kecamatan', ''),
  (2083, 10, 352, 4946, 'Pulosari', 'Kecamatan', ''),
  (2084, 10, 352, 4947, 'Randudongkal', 'Kecamatan', ''),
  (2085, 10, 352, 4948, 'Taman', 'Kecamatan', ''),
  (2086, 10, 352, 4949, 'Ulujami', 'Kecamatan', ''),
  (2087, 10, 352, 4950, 'Warungpring', 'Kecamatan', ''),
  (2088, 10, 352, 4951, 'Watukumpul', 'Kecamatan', ''),
  (2089, 10, 375, 5199, 'Bobotsari', 'Kecamatan', ''),
  (2090, 10, 375, 5200, 'Bojongsari', 'Kecamatan', ''),
  (2091, 10, 375, 5201, 'Bukateja', 'Kecamatan', ''),
  (2092, 10, 375, 5202, 'Kaligondang', 'Kecamatan', ''),
  (2093, 10, 375, 5203, 'Kalimanah', 'Kecamatan', ''),
  (2094, 10, 375, 5204, 'Karanganyar', 'Kecamatan', ''),
  (2095, 10, 375, 5205, 'Karangjambu', 'Kecamatan', ''),
  (2096, 10, 375, 5206, 'Karangmoncol', 'Kecamatan', ''),
  (2097, 10, 375, 5207, 'Karangreja', 'Kecamatan', ''),
  (2098, 10, 375, 5208, 'Kejobong', 'Kecamatan', ''),
  (2099, 10, 375, 5209, 'Kemangkon', 'Kecamatan', ''),
  (2100, 10, 375, 5210, 'Kertanegara', 'Kecamatan', ''),
  (2101, 10, 375, 5211, 'Kutasari', 'Kecamatan', ''),
  (2102, 10, 375, 5212, 'Mrebet', 'Kecamatan', ''),
  (2103, 10, 375, 5213, 'Padamara', 'Kecamatan', ''),
  (2104, 10, 375, 5214, 'Pengadegan', 'Kecamatan', ''),
  (2105, 10, 375, 5215, 'Purbalingga', 'Kecamatan', ''),
  (2106, 10, 375, 5216, 'Rembang', 'Kecamatan', ''),
  (2107, 10, 377, 5234, 'Bagelen', 'Kecamatan', ''),
  (2108, 10, 377, 5235, 'Banyuurip', 'Kecamatan', ''),
  (2109, 10, 377, 5236, 'Bayan', 'Kecamatan', ''),
  (2110, 10, 377, 5237, 'Bener', 'Kecamatan', ''),
  (2111, 10, 377, 5238, 'Bruno', 'Kecamatan', ''),
  (2112, 10, 377, 5239, 'Butuh', 'Kecamatan', ''),
  (2113, 10, 377, 5240, 'Gebang', 'Kecamatan', ''),
  (2114, 10, 377, 5241, 'Grabag', 'Kecamatan', ''),
  (2115, 10, 377, 5242, 'Kaligesing', 'Kecamatan', ''),
  (2116, 10, 377, 5243, 'Kemiri', 'Kecamatan', ''),
  (2117, 10, 377, 5244, 'Kutoarjo', 'Kecamatan', ''),
  (2118, 10, 377, 5245, 'Loano', 'Kecamatan', ''),
  (2119, 10, 377, 5246, 'Ngombol', 'Kecamatan', ''),
  (2120, 10, 377, 5247, 'Pituruh', 'Kecamatan', ''),
  (2121, 10, 377, 5248, 'Purwodadi', 'Kecamatan', ''),
  (2122, 10, 377, 5249, 'Purworejo', 'Kecamatan', ''),
  (2123, 10, 380, 5289, 'Bulu', 'Kecamatan', ''),
  (2124, 10, 380, 5290, 'Gunem', 'Kecamatan', ''),
  (2125, 10, 380, 5291, 'Kaliori', 'Kecamatan', ''),
  (2126, 10, 380, 5292, 'Kragan', 'Kecamatan', ''),
  (2127, 10, 380, 5293, 'Lasem', 'Kecamatan', ''),
  (2128, 10, 380, 5294, 'Pamotan', 'Kecamatan', ''),
  (2129, 10, 380, 5295, 'Pancur', 'Kecamatan', ''),
  (2130, 10, 380, 5296, 'Rembang', 'Kecamatan', ''),
  (2131, 10, 380, 5297, 'Sale', 'Kecamatan', ''),
  (2132, 10, 380, 5298, 'Sarang', 'Kecamatan', ''),
  (2133, 10, 380, 5299, 'Sedan', 'Kecamatan', ''),
  (2134, 10, 380, 5300, 'Sluke', 'Kecamatan', ''),
  (2135, 10, 380, 5301, 'Sulang', 'Kecamatan', ''),
  (2136, 10, 380, 5302, 'Sumber', 'Kecamatan', ''),
  (2137, 10, 386, 5352, 'Argomulyo', 'Kecamatan', ''),
  (2138, 10, 386, 5353, 'Sidomukti', 'Kecamatan', ''),
  (2139, 10, 386, 5354, 'Sidorejo', 'Kecamatan', ''),
  (2140, 10, 386, 5355, 'Tingkir', 'Kecamatan', ''),
  (2141, 10, 398, 5479, 'Ambarawa', 'Kecamatan', ''),
  (2142, 10, 398, 5480, 'Bancak', 'Kecamatan', ''),
  (2143, 10, 398, 5481, 'Bandungan', 'Kecamatan', ''),
  (2144, 10, 398, 5482, 'Banyubiru', 'Kecamatan', ''),
  (2145, 10, 398, 5483, 'Bawen', 'Kecamatan', ''),
  (2146, 10, 398, 5484, 'Bergas', 'Kecamatan', ''),
  (2147, 10, 398, 5485, 'Bringin', 'Kecamatan', ''),
  (2148, 10, 398, 5486, 'Getasan', 'Kecamatan', ''),
  (2149, 10, 398, 5487, 'Jambu', 'Kecamatan', ''),
  (2150, 10, 398, 5488, 'Kaliwungu', 'Kecamatan', ''),
  (2151, 10, 398, 5489, 'Pabelan', 'Kecamatan', ''),
  (2152, 10, 398, 5490, 'Pringapus', 'Kecamatan', ''),
  (2153, 10, 398, 5491, 'Sumowono', 'Kecamatan', ''),
  (2154, 10, 398, 5492, 'Suruh', 'Kecamatan', ''),
  (2155, 10, 398, 5493, 'Susukan', 'Kecamatan', ''),
  (2156, 10, 398, 5494, 'Tengaran', 'Kecamatan', ''),
  (2157, 10, 398, 5495, 'Tuntang', 'Kecamatan', ''),
  (2158, 10, 398, 5496, 'Ungaran Barat', 'Kecamatan', ''),
  (2159, 10, 398, 5497, 'Ungaran Timur', 'Kecamatan', ''),
  (2160, 10, 399, 5498, 'Banyumanik', 'Kecamatan', ''),
  (2161, 10, 399, 5499, 'Candisari', 'Kecamatan', ''),
  (2162, 10, 399, 5500, 'Gajah Mungkur', 'Kecamatan', ''),
  (2163, 10, 399, 5501, 'Gayamsari', 'Kecamatan', ''),
  (2164, 10, 399, 5502, 'Genuk', 'Kecamatan', ''),
  (2165, 10, 399, 5503, 'Gunungpati', 'Kecamatan', ''),
  (2166, 10, 399, 5504, 'Mijen', 'Kecamatan', ''),
  (2167, 10, 399, 5505, 'Ngaliyan', 'Kecamatan', ''),
  (2168, 10, 399, 5506, 'Pedurungan', 'Kecamatan', ''),
  (2169, 10, 399, 5507, 'Semarang Barat', 'Kecamatan', ''),
  (2170, 10, 399, 5508, 'Semarang Selatan', 'Kecamatan', ''),
  (2171, 10, 399, 5509, 'Semarang Tengah', 'Kecamatan', ''),
  (2172, 10, 399, 5510, 'Semarang Timur', 'Kecamatan', ''),
  (2173, 10, 399, 5511, 'Semarang Utara', 'Kecamatan', ''),
  (2174, 10, 399, 5512, 'Tembalang', 'Kecamatan', ''),
  (2175, 10, 399, 5513, 'Tugu', 'Kecamatan', ''),
  (2176, 10, 427, 5863, 'Gemolong', 'Kecamatan', ''),
  (2177, 10, 427, 5864, 'Gesi', 'Kecamatan', ''),
  (2178, 10, 427, 5865, 'Gondang', 'Kecamatan', ''),
  (2179, 10, 427, 5866, 'Jenar', 'Kecamatan', ''),
  (2180, 10, 427, 5867, 'Kalijambe', 'Kecamatan', ''),
  (2181, 10, 427, 5868, 'Karangmalang', 'Kecamatan', ''),
  (2182, 10, 427, 5869, 'Kedawung', 'Kecamatan', ''),
  (2183, 10, 427, 5870, 'Masaran', 'Kecamatan', ''),
  (2184, 10, 427, 5871, 'Miri', 'Kecamatan', ''),
  (2185, 10, 427, 5872, 'Mondokan', 'Kecamatan', ''),
  (2186, 10, 427, 5873, 'Ngrampal', 'Kecamatan', ''),
  (2187, 10, 427, 5874, 'Plupuh', 'Kecamatan', ''),
  (2188, 10, 427, 5875, 'Sambirejo', 'Kecamatan', ''),
  (2189, 10, 427, 5876, 'Sambung Macan', 'Kecamatan', ''),
  (2190, 10, 427, 5877, 'Sidoharjo', 'Kecamatan', ''),
  (2191, 10, 427, 5878, 'Sragen', 'Kecamatan', ''),
  (2192, 10, 427, 5879, 'Sukodono', 'Kecamatan', ''),
  (2193, 10, 427, 5880, 'Sumberlawang', 'Kecamatan', ''),
  (2194, 10, 427, 5881, 'Tangen', 'Kecamatan', ''),
  (2195, 10, 427, 5882, 'Tanon', 'Kecamatan', ''),
  (2196, 10, 433, 5977, 'Baki', 'Kecamatan', ''),
  (2197, 10, 433, 5978, 'Bendosari', 'Kecamatan', ''),
  (2198, 10, 433, 5979, 'Bulu', 'Kecamatan', ''),
  (2199, 10, 433, 5980, 'Gatak', 'Kecamatan', ''),
  (2200, 10, 433, 5981, 'Grogol', 'Kecamatan', ''),
  (2201, 10, 433, 5982, 'Kartasura', 'Kecamatan', ''),
  (2202, 10, 433, 5983, 'Mojolaban', 'Kecamatan', ''),
  (2203, 10, 433, 5984, 'Nguter', 'Kecamatan', ''),
  (2204, 10, 433, 5985, 'Polokarto', 'Kecamatan', ''),
  (2205, 10, 433, 5986, 'Sukoharjo', 'Kecamatan', ''),
  (2206, 10, 433, 5987, 'Tawangsari', 'Kecamatan', ''),
  (2207, 10, 433, 5988, 'Weru', 'Kecamatan', ''),
  (2208, 10, 445, 6162, 'Banjarsari', 'Kecamatan', ''),
  (2209, 10, 445, 6163, 'Jebres', 'Kecamatan', ''),
  (2210, 10, 445, 6164, 'Laweyan', 'Kecamatan', ''),
  (2211, 10, 445, 6165, 'Pasar Kliwon', 'Kecamatan', ''),
  (2212, 10, 445, 6166, 'Serengan', 'Kecamatan', ''),
  (2213, 10, 472, 6502, 'Adiwerna', 'Kecamatan', ''),
  (2214, 10, 472, 6503, 'Balapulang', 'Kecamatan', ''),
  (2215, 10, 472, 6504, 'Bojong', 'Kecamatan', ''),
  (2216, 10, 472, 6505, 'Bumijawa', 'Kecamatan', ''),
  (2217, 10, 472, 6506, 'Dukuhturi', 'Kecamatan', ''),
  (2218, 10, 472, 6507, 'Dukuhwaru', 'Kecamatan', ''),
  (2219, 10, 472, 6508, 'Jatinegara', 'Kecamatan', ''),
  (2220, 10, 472, 6509, 'Kedung Banteng', 'Kecamatan', ''),
  (2221, 10, 472, 6510, 'Kramat', 'Kecamatan', ''),
  (2222, 10, 472, 6511, 'Lebaksiu', 'Kecamatan', ''),
  (2223, 10, 472, 6512, 'Margasari', 'Kecamatan', ''),
  (2224, 10, 472, 6513, 'Pagerbarang', 'Kecamatan', ''),
  (2225, 10, 472, 6514, 'Pangkah', 'Kecamatan', ''),
  (2226, 10, 472, 6515, 'Slawi', 'Kecamatan', ''),
  (2227, 10, 472, 6516, 'Surodadi', 'Kecamatan', ''),
  (2228, 10, 472, 6517, 'Talang', 'Kecamatan', ''),
  (2229, 10, 472, 6518, 'Tarub', 'Kecamatan', ''),
  (2230, 10, 472, 6519, 'Warurejo', 'Kecamatan', ''),
  (2231, 10, 473, 6520, 'Margadana', 'Kecamatan', ''),
  (2232, 10, 473, 6521, 'Tegal Barat', 'Kecamatan', ''),
  (2233, 10, 473, 6522, 'Tegal Selatan', 'Kecamatan', ''),
  (2234, 10, 473, 6523, 'Tegal Timur', 'Kecamatan', ''),
  (2235, 10, 476, 6561, 'Bansari', 'Kecamatan', ''),
  (2236, 10, 476, 6562, 'Bejen', 'Kecamatan', ''),
  (2237, 10, 476, 6563, 'Bulu', 'Kecamatan', ''),
  (2238, 10, 476, 6564, 'Candiroto', 'Kecamatan', ''),
  (2239, 10, 476, 6565, 'Gemawang', 'Kecamatan', ''),
  (2240, 10, 476, 6566, 'Jumo', 'Kecamatan', ''),
  (2241, 10, 476, 6567, 'Kaloran', 'Kecamatan', ''),
  (2242, 10, 476, 6568, 'Kandangan', 'Kecamatan', ''),
  (2243, 10, 476, 6569, 'Kedu', 'Kecamatan', ''),
  (2244, 10, 476, 6570, 'Kledung', 'Kecamatan', ''),
  (2245, 10, 476, 6571, 'Kranggan', 'Kecamatan', ''),
  (2246, 10, 476, 6572, 'Ngadirejo', 'Kecamatan', ''),
  (2247, 10, 476, 6573, 'Parakan', 'Kecamatan', ''),
  (2248, 10, 476, 6574, 'Pringsurat', 'Kecamatan', ''),
  (2249, 10, 476, 6575, 'Selopampang', 'Kecamatan', ''),
  (2250, 10, 476, 6576, 'Temanggung', 'Kecamatan', ''),
  (2251, 10, 476, 6577, 'Tembarak', 'Kecamatan', ''),
  (2252, 10, 476, 6578, 'Tlogomulyo', 'Kecamatan', ''),
  (2253, 10, 476, 6579, 'Tretep', 'Kecamatan', ''),
  (2254, 10, 476, 6580, 'Wonoboyo', 'Kecamatan', ''),
  (2255, 10, 497, 6885, 'Baturetno', 'Kecamatan', ''),
  (2256, 10, 497, 6886, 'Batuwarno', 'Kecamatan', ''),
  (2257, 10, 497, 6887, 'Bulukerto', 'Kecamatan', ''),
  (2258, 10, 497, 6888, 'Eromoko', 'Kecamatan', ''),
  (2259, 10, 497, 6889, 'Girimarto', 'Kecamatan', ''),
  (2260, 10, 497, 6890, 'Giritontro', 'Kecamatan', ''),
  (2261, 10, 497, 6891, 'Giriwoyo', 'Kecamatan', ''),
  (2262, 10, 497, 6892, 'Jatipurno', 'Kecamatan', ''),
  (2263, 10, 497, 6893, 'Jatiroto', 'Kecamatan', ''),
  (2264, 10, 497, 6894, 'Jatisrono', 'Kecamatan', ''),
  (2265, 10, 497, 6895, 'Karangtengah', 'Kecamatan', ''),
  (2266, 10, 497, 6896, 'Kismantoro', 'Kecamatan', ''),
  (2267, 10, 497, 6897, 'Manyaran', 'Kecamatan', ''),
  (2268, 10, 497, 6898, 'Ngadirojo', 'Kecamatan', ''),
  (2269, 10, 497, 6899, 'Nguntoronadi', 'Kecamatan', ''),
  (2270, 10, 497, 6900, 'Paranggupito', 'Kecamatan', ''),
  (2271, 10, 497, 6901, 'Pracimantoro', 'Kecamatan', ''),
  (2272, 10, 497, 6902, 'Puhpelem', 'Kecamatan', ''),
  (2273, 10, 497, 6903, 'Purwantoro', 'Kecamatan', ''),
  (2274, 10, 497, 6904, 'Selogiri', 'Kecamatan', ''),
  (2275, 10, 497, 6905, 'Sidoharjo', 'Kecamatan', ''),
  (2276, 10, 497, 6906, 'Slogohimo', 'Kecamatan', ''),
  (2277, 10, 497, 6907, 'Tirtomoyo', 'Kecamatan', ''),
  (2278, 10, 497, 6908, 'Wonogiri', 'Kecamatan', ''),
  (2279, 10, 497, 6909, 'Wuryantoro', 'Kecamatan', ''),
  (2280, 10, 498, 6910, 'Garung', 'Kecamatan', ''),
  (2281, 10, 498, 6911, 'Kalibawang', 'Kecamatan', ''),
  (2282, 10, 498, 6912, 'Kalikajar', 'Kecamatan', ''),
  (2283, 10, 498, 6913, 'Kaliwiro', 'Kecamatan', ''),
  (2284, 10, 498, 6914, 'Kejajar', 'Kecamatan', ''),
  (2285, 10, 498, 6915, 'Kepil', 'Kecamatan', ''),
  (2286, 10, 498, 6916, 'Kertek', 'Kecamatan', ''),
  (2287, 10, 498, 6917, 'Leksono', 'Kecamatan', ''),
  (2288, 10, 498, 6918, 'Mojotengah', 'Kecamatan', ''),
  (2289, 10, 498, 6919, 'Sapuran', 'Kecamatan', ''),
  (2290, 10, 498, 6920, 'Selomerto', 'Kecamatan', ''),
  (2291, 10, 498, 6921, 'Sukoharjo', 'Kecamatan', ''),
  (2292, 10, 498, 6922, 'Wadaslintang', 'Kecamatan', ''),
  (2293, 10, 498, 6923, 'Watumalang', 'Kecamatan', ''),
  (2294, 10, 498, 6924, 'Wonosobo', 'Kecamatan', ''),
  (2295, 11, 31, 454, 'Arosbaya', 'Kecamatan', ''),
  (2296, 11, 31, 455, 'Bangkalan', 'Kecamatan', ''),
  (2297, 11, 31, 456, 'Blega', 'Kecamatan', ''),
  (2298, 11, 31, 457, 'Burneh', 'Kecamatan', ''),
  (2299, 11, 31, 458, 'Galis', 'Kecamatan', ''),
  (2300, 11, 31, 459, 'Geger', 'Kecamatan', ''),
  (2301, 11, 31, 460, 'Kamal', 'Kecamatan', ''),
  (2302, 11, 31, 461, 'Klampis', 'Kecamatan', ''),
  (2303, 11, 31, 462, 'Kokop', 'Kecamatan', ''),
  (2304, 11, 31, 463, 'Konang', 'Kecamatan', ''),
  (2305, 11, 31, 464, 'Kwanyar', 'Kecamatan', ''),
  (2306, 11, 31, 465, 'Labang', 'Kecamatan', ''),
  (2307, 11, 31, 466, 'Modung', 'Kecamatan', ''),
  (2308, 11, 31, 467, 'Sepulu', 'Kecamatan', ''),
  (2309, 11, 31, 468, 'Socah', 'Kecamatan', ''),
  (2310, 11, 31, 469, 'Tanah Merah', 'Kecamatan', ''),
  (2311, 11, 31, 470, 'Tanjungbumi', 'Kecamatan', ''),
  (2312, 11, 31, 471, 'Tragah', 'Kecamatan', ''),
  (2313, 11, 42, 600, 'Bangorejo', 'Kecamatan', ''),
  (2314, 11, 42, 601, 'Banyuwangi', 'Kecamatan', ''),
  (2315, 11, 42, 602, 'Cluring', 'Kecamatan', ''),
  (2316, 11, 42, 603, 'Gambiran', 'Kecamatan', ''),
  (2317, 11, 42, 604, 'Genteng', 'Kecamatan', ''),
  (2318, 11, 42, 605, 'Giri', 'Kecamatan', ''),
  (2319, 11, 42, 606, 'Glagah', 'Kecamatan', ''),
  (2320, 11, 42, 607, 'Glenmore', 'Kecamatan', ''),
  (2321, 11, 42, 608, 'Kabat', 'Kecamatan', ''),
  (2322, 11, 42, 609, 'Kalibaru', 'Kecamatan', ''),
  (2323, 11, 42, 610, 'Kalipuro', 'Kecamatan', ''),
  (2324, 11, 42, 611, 'Licin', 'Kecamatan', ''),
  (2325, 11, 42, 612, 'Muncar', 'Kecamatan', ''),
  (2326, 11, 42, 613, 'Pesanggaran', 'Kecamatan', ''),
  (2327, 11, 42, 614, 'Purwoharjo', 'Kecamatan', ''),
  (2328, 11, 42, 615, 'Rogojampi', 'Kecamatan', ''),
  (2329, 11, 42, 616, 'Sempu', 'Kecamatan', ''),
  (2330, 11, 42, 617, 'Siliragung', 'Kecamatan', ''),
  (2331, 11, 42, 618, 'Singojuruh', 'Kecamatan', ''),
  (2332, 11, 42, 619, 'Songgon', 'Kecamatan', ''),
  (2333, 11, 42, 620, 'Srono', 'Kecamatan', ''),
  (2334, 11, 42, 621, 'Tegaldlimo', 'Kecamatan', ''),
  (2335, 11, 42, 622, 'Tegalsari', 'Kecamatan', ''),
  (2336, 11, 42, 623, 'Wongsorejo', 'Kecamatan', ''),
  (2337, 11, 74, 974, 'Bakung', 'Kecamatan', ''),
  (2338, 11, 74, 975, 'Binangun', 'Kecamatan', ''),
  (2339, 11, 74, 976, 'Doko', 'Kecamatan', ''),
  (2340, 11, 74, 977, 'Gandusari', 'Kecamatan', ''),
  (2341, 11, 74, 978, 'Garum', 'Kecamatan', ''),
  (2342, 11, 74, 979, 'Kademangan', 'Kecamatan', ''),
  (2343, 11, 74, 980, 'Kanigoro', 'Kecamatan', ''),
  (2344, 11, 74, 981, 'Kesamben', 'Kecamatan', ''),
  (2345, 11, 74, 982, 'Nglegok', 'Kecamatan', ''),
  (2346, 11, 74, 983, 'Panggungrejo', 'Kecamatan', ''),
  (2347, 11, 74, 984, 'Ponggok', 'Kecamatan', ''),
  (2348, 11, 74, 985, 'Sanan Kulon', 'Kecamatan', ''),
  (2349, 11, 74, 986, 'Selopuro', 'Kecamatan', ''),
  (2350, 11, 74, 987, 'Selorejo', 'Kecamatan', ''),
  (2351, 11, 74, 988, 'Srengat', 'Kecamatan', ''),
  (2352, 11, 74, 989, 'Sutojayan', 'Kecamatan', ''),
  (2353, 11, 74, 990, 'Talun', 'Kecamatan', ''),
  (2354, 11, 74, 991, 'Udanawu', 'Kecamatan', ''),
  (2355, 11, 74, 992, 'Wates', 'Kecamatan', ''),
  (2356, 11, 74, 993, 'Wlingi', 'Kecamatan', ''),
  (2357, 11, 74, 994, 'Wonodadi', 'Kecamatan', ''),
  (2358, 11, 74, 995, 'Wonotirto', 'Kecamatan', ''),
  (2359, 11, 80, 1068, 'Balen', 'Kecamatan', ''),
  (2360, 11, 80, 1069, 'Baureno', 'Kecamatan', ''),
  (2361, 11, 80, 1070, 'Bojonegoro', 'Kecamatan', ''),
  (2362, 11, 80, 1071, 'Bubulan', 'Kecamatan', ''),
  (2363, 11, 80, 1072, 'Dander', 'Kecamatan', ''),
  (2364, 11, 80, 1073, 'Gayam', 'Kecamatan', ''),
  (2365, 11, 80, 1074, 'Gondang', 'Kecamatan', ''),
  (2366, 11, 80, 1075, 'Kalitidu', 'Kecamatan', ''),
  (2367, 11, 80, 1076, 'Kanor', 'Kecamatan', ''),
  (2368, 11, 80, 1077, 'Kapas', 'Kecamatan', ''),
  (2369, 11, 80, 1078, 'Kasiman', 'Kecamatan', ''),
  (2370, 11, 80, 1079, 'Kedewan', 'Kecamatan', ''),
  (2371, 11, 80, 1080, 'Kedungadem', 'Kecamatan', ''),
  (2372, 11, 80, 1081, 'Kepoh Baru', 'Kecamatan', ''),
  (2373, 11, 80, 1082, 'Malo', 'Kecamatan', ''),
  (2374, 11, 80, 1083, 'Margomulyo', 'Kecamatan', ''),
  (2375, 11, 80, 1084, 'Ngambon', 'Kecamatan', ''),
  (2376, 11, 80, 1085, 'Ngasem', 'Kecamatan', ''),
  (2377, 11, 80, 1086, 'Ngraho', 'Kecamatan', ''),
  (2378, 11, 80, 1087, 'Padangan', 'Kecamatan', ''),
  (2379, 11, 80, 1088, 'Purwosari', 'Kecamatan', ''),
  (2380, 11, 80, 1089, 'Sekar', 'Kecamatan', ''),
  (2381, 11, 80, 1090, 'Sugihwaras', 'Kecamatan', ''),
  (2382, 11, 80, 1091, 'Sukosewu', 'Kecamatan', ''),
  (2383, 11, 80, 1092, 'Sumberrejo', 'Kecamatan', ''),
  (2384, 11, 80, 1093, 'Tambakrejo', 'Kecamatan', ''),
  (2385, 11, 80, 1094, 'Temayang', 'Kecamatan', ''),
  (2386, 11, 80, 1095, 'Trucuk', 'Kecamatan', ''),
  (2387, 11, 86, 1149, 'Binakal', 'Kecamatan', ''),
  (2388, 11, 86, 1150, 'Bondowoso', 'Kecamatan', ''),
  (2389, 11, 86, 1151, 'Botolinggo', 'Kecamatan', ''),
  (2390, 11, 86, 1152, 'Cermee', 'Kecamatan', ''),
  (2391, 11, 86, 1153, 'Curahdami', 'Kecamatan', ''),
  (2392, 11, 86, 1154, 'Grujugan', 'Kecamatan', ''),
  (2393, 11, 86, 1155, 'Jambe Sari Darus Sholah', 'Kecamatan', ''),
  (2394, 11, 86, 1156, 'Klabang', 'Kecamatan', ''),
  (2395, 11, 86, 1157, 'Maesan', 'Kecamatan', ''),
  (2396, 11, 86, 1158, 'Pakem', 'Kecamatan', ''),
  (2397, 11, 86, 1159, 'Prajekan', 'Kecamatan', ''),
  (2398, 11, 86, 1160, 'Pujer', 'Kecamatan', ''),
  (2399, 11, 86, 1161, 'Sempol', 'Kecamatan', ''),
  (2400, 11, 86, 1162, 'Sukosari', 'Kecamatan', ''),
  (2401, 11, 86, 1163, 'Sumber Wringin', 'Kecamatan', ''),
  (2402, 11, 86, 1164, 'Taman Krocok', 'Kecamatan', ''),
  (2403, 11, 86, 1165, 'Tamanan', 'Kecamatan', ''),
  (2404, 11, 86, 1166, 'Tapen', 'Kecamatan', ''),
  (2405, 11, 86, 1167, 'Tegalampel', 'Kecamatan', ''),
  (2406, 11, 86, 1168, 'Tenggarang', 'Kecamatan', ''),
  (2407, 11, 86, 1169, 'Tlogosari', 'Kecamatan', ''),
  (2408, 11, 86, 1170, 'Wonosari', 'Kecamatan', ''),
  (2409, 11, 86, 1171, 'Wringin', 'Kecamatan', ''),
  (2410, 11, 133, 1828, 'Balong Panggang', 'Kecamatan', ''),
  (2411, 11, 133, 1829, 'Benjeng', 'Kecamatan', ''),
  (2412, 11, 133, 1830, 'Bungah', 'Kecamatan', ''),
  (2413, 11, 133, 1831, 'Cerme', 'Kecamatan', ''),
  (2414, 11, 133, 1832, 'Driyorejo', 'Kecamatan', ''),
  (2415, 11, 133, 1833, 'Duduk Sampeyan', 'Kecamatan', ''),
  (2416, 11, 133, 1834, 'Dukun', 'Kecamatan', ''),
  (2417, 11, 133, 1835, 'Gresik', 'Kecamatan', ''),
  (2418, 11, 133, 1836, 'Kebomas', 'Kecamatan', ''),
  (2419, 11, 133, 1837, 'Kedamean', 'Kecamatan', ''),
  (2420, 11, 133, 1838, 'Manyar', 'Kecamatan', ''),
  (2421, 11, 133, 1839, 'Menganti', 'Kecamatan', ''),
  (2422, 11, 133, 1840, 'Panceng', 'Kecamatan', ''),
  (2423, 11, 133, 1841, 'Sangkapura', 'Kecamatan', ''),
  (2424, 11, 133, 1842, 'Sidayu', 'Kecamatan', ''),
  (2425, 11, 133, 1843, 'Tambak', 'Kecamatan', ''),
  (2426, 11, 133, 1844, 'Ujung Pangkah', 'Kecamatan', ''),
  (2427, 11, 133, 1845, 'Wringin Anom', 'Kecamatan', ''),
  (2428, 11, 160, 2201, 'Ajung', 'Kecamatan', ''),
  (2429, 11, 160, 2202, 'Ambulu', 'Kecamatan', ''),
  (2430, 11, 160, 2203, 'Arjasa', 'Kecamatan', ''),
  (2431, 11, 160, 2204, 'Balung', 'Kecamatan', ''),
  (2432, 11, 160, 2205, 'Bangsalsari', 'Kecamatan', ''),
  (2433, 11, 160, 2206, 'Gumuk Mas', 'Kecamatan', ''),
  (2434, 11, 160, 2207, 'Jelbuk', 'Kecamatan', ''),
  (2435, 11, 160, 2208, 'Jenggawah', 'Kecamatan', ''),
  (2436, 11, 160, 2209, 'Jombang', 'Kecamatan', ''),
  (2437, 11, 160, 2210, 'Kalisat', 'Kecamatan', ''),
  (2438, 11, 160, 2211, 'Kaliwates', 'Kecamatan', ''),
  (2439, 11, 160, 2212, 'Kencong', 'Kecamatan', ''),
  (2440, 11, 160, 2213, 'Ledokombo', 'Kecamatan', ''),
  (2441, 11, 160, 2214, 'Mayang', 'Kecamatan', ''),
  (2442, 11, 160, 2215, 'Mumbulsari', 'Kecamatan', ''),
  (2443, 11, 160, 2216, 'Pakusari', 'Kecamatan', ''),
  (2444, 11, 160, 2217, 'Panti', 'Kecamatan', ''),
  (2445, 11, 160, 2218, 'Patrang', 'Kecamatan', ''),
  (2446, 11, 160, 2219, 'Puger', 'Kecamatan', ''),
  (2447, 11, 160, 2220, 'Rambipuji', 'Kecamatan', ''),
  (2448, 11, 160, 2221, 'Semboro', 'Kecamatan', ''),
  (2449, 11, 160, 2222, 'Silo', 'Kecamatan', ''),
  (2450, 11, 160, 2223, 'Sukorambi', 'Kecamatan', ''),
  (2451, 11, 160, 2224, 'Sukowono', 'Kecamatan', ''),
  (2452, 11, 160, 2225, 'Sumber Baru', 'Kecamatan', ''),
  (2453, 11, 160, 2226, 'Sumber Jambe', 'Kecamatan', ''),
  (2454, 11, 160, 2227, 'Sumber Sari', 'Kecamatan', ''),
  (2455, 11, 160, 2228, 'Tanggul', 'Kecamatan', ''),
  (2456, 11, 160, 2229, 'Tempurejo', 'Kecamatan', ''),
  (2457, 11, 160, 2230, 'Umbulsari', 'Kecamatan', ''),
  (2458, 11, 160, 2231, 'Wuluhan', 'Kecamatan', ''),
  (2459, 11, 164, 2264, 'Bandar Kedung Mulyo', 'Kecamatan', ''),
  (2460, 11, 164, 2265, 'Bareng', 'Kecamatan', ''),
  (2461, 11, 164, 2266, 'Diwek', 'Kecamatan', ''),
  (2462, 11, 164, 2267, 'Gudo', 'Kecamatan', ''),
  (2463, 11, 164, 2268, 'Jogoroto', 'Kecamatan', ''),
  (2464, 11, 164, 2269, 'Jombang', 'Kecamatan', ''),
  (2465, 11, 164, 2270, 'Kabuh', 'Kecamatan', ''),
  (2466, 11, 164, 2271, 'Kesamben', 'Kecamatan', ''),
  (2467, 11, 164, 2272, 'Kudu', 'Kecamatan', ''),
  (2468, 11, 164, 2273, 'Megaluh', 'Kecamatan', ''),
  (2469, 11, 164, 2274, 'Mojoagung', 'Kecamatan', ''),
  (2470, 11, 164, 2275, 'Mojowarno', 'Kecamatan', ''),
  (2471, 11, 164, 2276, 'Ngoro', 'Kecamatan', ''),
  (2472, 11, 164, 2277, 'Ngusikan', 'Kecamatan', ''),
  (2473, 11, 164, 2278, 'Perak', 'Kecamatan', ''),
  (2474, 11, 164, 2279, 'Peterongan', 'Kecamatan', ''),
  (2475, 11, 164, 2280, 'Plandaan', 'Kecamatan', ''),
  (2476, 11, 164, 2281, 'Ploso', 'Kecamatan', ''),
  (2477, 11, 164, 2282, 'Sumobito', 'Kecamatan', ''),
  (2478, 11, 164, 2283, 'Tembelang', 'Kecamatan', ''),
  (2479, 11, 164, 2284, 'Wonosalam', 'Kecamatan', ''),
  (2480, 11, 222, 3113, 'Babat', 'Kecamatan', ''),
  (2481, 11, 222, 3114, 'Bluluk', 'Kecamatan', ''),
  (2482, 11, 222, 3115, 'Brondong', 'Kecamatan', ''),
  (2483, 11, 222, 3116, 'Deket', 'Kecamatan', ''),
  (2484, 11, 222, 3117, 'Glagah', 'Kecamatan', ''),
  (2485, 11, 222, 3118, 'Kalitengah', 'Kecamatan', ''),
  (2486, 11, 222, 3119, 'Karang Geneng', 'Kecamatan', ''),
  (2487, 11, 222, 3120, 'Karangbinangun', 'Kecamatan', ''),
  (2488, 11, 222, 3121, 'Kedungpring', 'Kecamatan', ''),
  (2489, 11, 222, 3122, 'Kembangbahu', 'Kecamatan', ''),
  (2490, 11, 222, 3123, 'Lamongan', 'Kecamatan', ''),
  (2491, 11, 222, 3124, 'Laren', 'Kecamatan', ''),
  (2492, 11, 222, 3125, 'Maduran', 'Kecamatan', ''),
  (2493, 11, 222, 3126, 'Mantup', 'Kecamatan', ''),
  (2494, 11, 222, 3127, 'Modo', 'Kecamatan', ''),
  (2495, 11, 222, 3128, 'Ngimbang', 'Kecamatan', ''),
  (2496, 11, 222, 3129, 'Paciran', 'Kecamatan', ''),
  (2497, 11, 222, 3130, 'Pucuk', 'Kecamatan', ''),
  (2498, 11, 222, 3131, 'Sambeng', 'Kecamatan', ''),
  (2499, 11, 222, 3132, 'Sarirejo', 'Kecamatan', ''),
  (2500, 11, 222, 3133, 'Sekaran', 'Kecamatan', ''),
  (2501, 11, 222, 3134, 'Solokuro', 'Kecamatan', ''),
  (2502, 11, 222, 3135, 'Sugio', 'Kecamatan', ''),
  (2503, 11, 222, 3136, 'Sukodadi', 'Kecamatan', ''),
  (2504, 11, 222, 3137, 'Sukorame', 'Kecamatan', ''),
  (2505, 11, 222, 3138, 'Tikung', 'Kecamatan', ''),
  (2506, 11, 222, 3139, 'Turi', 'Kecamatan', ''),
  (2507, 11, 243, 3427, 'Candipuro', 'Kecamatan', ''),
  (2508, 11, 243, 3428, 'Gucialit', 'Kecamatan', ''),
  (2509, 11, 243, 3429, 'Jatiroto', 'Kecamatan', ''),
  (2510, 11, 243, 3430, 'Kedungjajang', 'Kecamatan', ''),
  (2511, 11, 243, 3431, 'Klakah', 'Kecamatan', ''),
  (2512, 11, 243, 3432, 'Kunir', 'Kecamatan', ''),
  (2513, 11, 243, 3433, 'Lumajang', 'Kecamatan', ''),
  (2514, 11, 243, 3434, 'Padang', 'Kecamatan', ''),
  (2515, 11, 243, 3435, 'Pasirian', 'Kecamatan', ''),
  (2516, 11, 243, 3436, 'Pasrujambe/Pasujambe', 'Kecamatan', ''),
  (2517, 11, 243, 3437, 'Pronojiwo', 'Kecamatan', ''),
  (2518, 11, 243, 3438, 'Randuagung', 'Kecamatan', ''),
  (2519, 11, 243, 3439, 'Ranuyoso', 'Kecamatan', ''),
  (2520, 11, 243, 3440, 'Rowokangkung', 'Kecamatan', ''),
  (2521, 11, 243, 3441, 'Senduro', 'Kecamatan', ''),
  (2522, 11, 243, 3442, 'Sukodono', 'Kecamatan', ''),
  (2523, 11, 243, 3443, 'Sumbersuko', 'Kecamatan', ''),
  (2524, 11, 243, 3444, 'Tekung', 'Kecamatan', ''),
  (2525, 11, 243, 3445, 'Tempeh', 'Kecamatan', ''),
  (2526, 11, 243, 3446, 'Tempursari', 'Kecamatan', ''),
  (2527, 11, 243, 3447, 'Yosowilangun', 'Kecamatan', ''),
  (2528, 11, 247, 3493, 'Balerejo', 'Kecamatan', ''),
  (2529, 11, 247, 3494, 'Dagangan', 'Kecamatan', ''),
  (2530, 11, 247, 3495, 'Dolopo', 'Kecamatan', ''),
  (2531, 11, 247, 3496, 'Geger', 'Kecamatan', ''),
  (2532, 11, 247, 3497, 'Gemarang', 'Kecamatan', ''),
  (2533, 11, 247, 3498, 'Jiwan', 'Kecamatan', ''),
  (2534, 11, 247, 3499, 'Kare', 'Kecamatan', ''),
  (2535, 11, 247, 3500, 'Kebonsari', 'Kecamatan', ''),
  (2536, 11, 247, 3501, 'Madiun', 'Kecamatan', ''),
  (2537, 11, 247, 3502, 'Mejayan', 'Kecamatan', ''),
  (2538, 11, 247, 3503, 'Pilangkenceng', 'Kecamatan', ''),
  (2539, 11, 247, 3504, 'Saradan', 'Kecamatan', ''),
  (2540, 11, 247, 3505, 'Sawahan', 'Kecamatan', ''),
  (2541, 11, 247, 3506, 'Wonoasri', 'Kecamatan', ''),
  (2542, 11, 247, 3507, 'Wungu', 'Kecamatan', ''),
  (2543, 11, 251, 3535, 'Barat', 'Kecamatan', ''),
  (2544, 11, 251, 3536, 'Bendo', 'Kecamatan', ''),
  (2545, 11, 251, 3537, 'Karangrejo', 'Kecamatan', ''),
  (2546, 11, 251, 3538, 'Karas', 'Kecamatan', ''),
  (2547, 11, 251, 3539, 'Kartoharjo (Kertoharjo)', 'Kecamatan', ''),
  (2548, 11, 251, 3540, 'Kawedanan', 'Kecamatan', ''),
  (2549, 11, 251, 3541, 'Lembeyan', 'Kecamatan', ''),
  (2550, 11, 251, 3542, 'Magetan', 'Kecamatan', ''),
  (2551, 11, 251, 3543, 'Maospati', 'Kecamatan', ''),
  (2552, 11, 251, 3544, 'Ngariboyo', 'Kecamatan', ''),
  (2553, 11, 251, 3545, 'Nguntoronadi', 'Kecamatan', ''),
  (2554, 11, 251, 3546, 'Panekan', 'Kecamatan', ''),
  (2555, 11, 251, 3547, 'Parang', 'Kecamatan', ''),
  (2556, 11, 251, 3548, 'Plaosan', 'Kecamatan', ''),
  (2557, 11, 251, 3549, 'Poncol', 'Kecamatan', ''),
  (2558, 11, 251, 3550, 'Sidorejo', 'Kecamatan', ''),
  (2559, 11, 251, 3551, 'Sukomoro', 'Kecamatan', ''),
  (2560, 11, 251, 3552, 'Takeran', 'Kecamatan', ''),
  (2561, 11, 255, 3601, 'Ampelgading', 'Kecamatan', ''),
  (2562, 11, 255, 3602, 'Bantur', 'Kecamatan', ''),
  (2563, 11, 255, 3603, 'Bululawang', 'Kecamatan', ''),
  (2564, 11, 255, 3604, 'Dampit', 'Kecamatan', ''),
  (2565, 11, 255, 3605, 'Dau', 'Kecamatan', ''),
  (2566, 11, 255, 3606, 'Donomulyo', 'Kecamatan', ''),
  (2567, 11, 255, 3607, 'Gedangan', 'Kecamatan', ''),
  (2568, 11, 255, 3608, 'Gondanglegi', 'Kecamatan', ''),
  (2569, 11, 255, 3609, 'Jabung', 'Kecamatan', ''),
  (2570, 11, 255, 3610, 'Kalipare', 'Kecamatan', ''),
  (2571, 11, 255, 3611, 'Karangploso', 'Kecamatan', ''),
  (2572, 11, 255, 3612, 'Kasembon', 'Kecamatan', ''),
  (2573, 11, 255, 3613, 'Kepanjen', 'Kecamatan', ''),
  (2574, 11, 255, 3614, 'Kromengan', 'Kecamatan', ''),
  (2575, 11, 255, 3615, 'Lawang', 'Kecamatan', ''),
  (2576, 11, 255, 3616, 'Ngajung (Ngajum)', 'Kecamatan', ''),
  (2577, 11, 255, 3617, 'Ngantang', 'Kecamatan', ''),
  (2578, 11, 255, 3618, 'Pagak', 'Kecamatan', ''),
  (2579, 11, 255, 3619, 'Pagelaran', 'Kecamatan', ''),
  (2580, 11, 255, 3620, 'Pakis', 'Kecamatan', ''),
  (2581, 11, 255, 3621, 'Pakisaji', 'Kecamatan', ''),
  (2582, 11, 255, 3622, 'Poncokusumo', 'Kecamatan', ''),
  (2583, 11, 255, 3623, 'Pujon', 'Kecamatan', ''),
  (2584, 11, 255, 3624, 'Singosari', 'Kecamatan', ''),
  (2585, 11, 255, 3625, 'Sumbermanjing Wetan', 'Kecamatan', ''),
  (2586, 11, 255, 3626, 'Sumberpucung', 'Kecamatan', ''),
  (2587, 11, 255, 3627, 'Tajinan', 'Kecamatan', ''),
  (2588, 11, 255, 3628, 'Tirtoyudo', 'Kecamatan', ''),
  (2589, 11, 255, 3629, 'Tumpang', 'Kecamatan', ''),
  (2590, 11, 255, 3630, 'Turen', 'Kecamatan', ''),
  (2591, 11, 255, 3631, 'Wagir', 'Kecamatan', ''),
  (2592, 11, 255, 3632, 'Wajak', 'Kecamatan', ''),
  (2593, 11, 255, 3633, 'Wonosari', 'Kecamatan', ''),
  (2594, 11, 289, 4070, 'Bangsal', 'Kecamatan', ''),
  (2595, 11, 289, 4071, 'Dawar Blandong', 'Kecamatan', ''),
  (2596, 11, 289, 4072, 'Dlanggu', 'Kecamatan', ''),
  (2597, 11, 289, 4073, 'Gedeg', 'Kecamatan', ''),
  (2598, 11, 289, 4074, 'Gondang', 'Kecamatan', ''),
  (2599, 11, 289, 4075, 'Jatirejo', 'Kecamatan', ''),
  (2600, 11, 289, 4076, 'Jetis', 'Kecamatan', ''),
  (2601, 11, 289, 4077, 'Kemlagi', 'Kecamatan', ''),
  (2602, 11, 289, 4078, 'Kutorejo', 'Kecamatan', ''),
  (2603, 11, 289, 4079, 'Mojoanyar', 'Kecamatan', ''),
  (2604, 11, 289, 4080, 'Mojosari', 'Kecamatan', ''),
  (2605, 11, 289, 4081, 'Ngoro', 'Kecamatan', ''),
  (2606, 11, 289, 4082, 'Pacet', 'Kecamatan', ''),
  (2607, 11, 289, 4083, 'Pungging', 'Kecamatan', ''),
  (2608, 11, 289, 4084, 'Puri', 'Kecamatan', ''),
  (2609, 11, 289, 4085, 'Sooko', 'Kecamatan', ''),
  (2610, 11, 289, 4086, 'Trawas', 'Kecamatan', ''),
  (2611, 11, 289, 4087, 'Trowulan', 'Kecamatan', ''),
  (2612, 11, 305, 4323, 'Bagor', 'Kecamatan', ''),
  (2613, 11, 305, 4324, 'Baron', 'Kecamatan', ''),
  (2614, 11, 305, 4325, 'Berbek', 'Kecamatan', ''),
  (2615, 11, 305, 4326, 'Gondang', 'Kecamatan', ''),
  (2616, 11, 305, 4327, 'Jatikalen', 'Kecamatan', ''),
  (2617, 11, 305, 4328, 'Kertosono', 'Kecamatan', ''),
  (2618, 11, 305, 4329, 'Lengkong', 'Kecamatan', ''),
  (2619, 11, 305, 4330, 'Loceret', 'Kecamatan', ''),
  (2620, 11, 305, 4331, 'Nganjuk', 'Kecamatan', ''),
  (2621, 11, 305, 4332, 'Ngetos', 'Kecamatan', ''),
  (2622, 11, 305, 4333, 'Ngluyu', 'Kecamatan', ''),
  (2623, 11, 305, 4334, 'Ngronggot', 'Kecamatan', ''),
  (2624, 11, 305, 4335, 'Pace', 'Kecamatan', ''),
  (2625, 11, 305, 4336, 'Patianrowo', 'Kecamatan', ''),
  (2626, 11, 305, 4337, 'Prambon', 'Kecamatan', ''),
  (2627, 11, 305, 4338, 'Rejoso', 'Kecamatan', ''),
  (2628, 11, 305, 4339, 'Sawahan', 'Kecamatan', ''),
  (2629, 11, 305, 4340, 'Sukomoro', 'Kecamatan', ''),
  (2630, 11, 305, 4341, 'Tanjunganom', 'Kecamatan', ''),
  (2631, 11, 305, 4342, 'Wilangan', 'Kecamatan', ''),
  (2632, 11, 306, 4343, 'Bringin', 'Kecamatan', ''),
  (2633, 11, 306, 4344, 'Geneng', 'Kecamatan', ''),
  (2634, 11, 306, 4345, 'Gerih', 'Kecamatan', ''),
  (2635, 11, 306, 4346, 'Jogorogo', 'Kecamatan', ''),
  (2636, 11, 306, 4347, 'Karanganyar', 'Kecamatan', ''),
  (2637, 11, 306, 4348, 'Karangjati', 'Kecamatan', ''),
  (2638, 11, 306, 4349, 'Kasreman', 'Kecamatan', ''),
  (2639, 11, 306, 4350, 'Kedunggalar', 'Kecamatan', ''),
  (2640, 11, 306, 4351, 'Kendal', 'Kecamatan', ''),
  (2641, 11, 306, 4352, 'Kwadungan', 'Kecamatan', ''),
  (2642, 11, 306, 4353, 'Mantingan', 'Kecamatan', ''),
  (2643, 11, 306, 4354, 'Ngawi', 'Kecamatan', ''),
  (2644, 11, 306, 4355, 'Ngrambe', 'Kecamatan', ''),
  (2645, 11, 306, 4356, 'Padas', 'Kecamatan', ''),
  (2646, 11, 306, 4357, 'Pangkur', 'Kecamatan', ''),
  (2647, 11, 306, 4358, 'Paron', 'Kecamatan', ''),
  (2648, 11, 306, 4359, 'Pitu', 'Kecamatan', ''),
  (2649, 11, 306, 4360, 'Sine', 'Kecamatan', ''),
  (2650, 11, 306, 4361, 'Widodaren', 'Kecamatan', ''),
  (2651, 11, 317, 4522, 'Arjosari', 'Kecamatan', ''),
  (2652, 11, 317, 4523, 'Bandar', 'Kecamatan', ''),
  (2653, 11, 317, 4524, 'Donorojo', 'Kecamatan', ''),
  (2654, 11, 317, 4525, 'Kebon Agung', 'Kecamatan', ''),
  (2655, 11, 317, 4526, 'Nawangan', 'Kecamatan', ''),
  (2656, 11, 317, 4527, 'Ngadirojo', 'Kecamatan', ''),
  (2657, 11, 317, 4528, 'Pacitan', 'Kecamatan', ''),
  (2658, 11, 317, 4529, 'Pringkuku', 'Kecamatan', ''),
  (2659, 11, 317, 4530, 'Punung', 'Kecamatan', ''),
  (2660, 11, 317, 4531, 'Sudimoro', 'Kecamatan', ''),
  (2661, 11, 317, 4532, 'Tegalombo', 'Kecamatan', ''),
  (2662, 11, 317, 4533, 'Tulakan', 'Kecamatan', ''),
  (2663, 11, 330, 4642, 'Batumarmar', 'Kecamatan', ''),
  (2664, 11, 330, 4643, 'Galis', 'Kecamatan', ''),
  (2665, 11, 330, 4644, 'Kadur', 'Kecamatan', ''),
  (2666, 11, 330, 4645, 'Larangan', 'Kecamatan', ''),
  (2667, 11, 330, 4646, 'Pademawu', 'Kecamatan', ''),
  (2668, 11, 330, 4647, 'Pakong', 'Kecamatan', ''),
  (2669, 11, 330, 4648, 'Palenga\' an ', ' Kecamatan ', ''),
(2670, 11, 330, 4649, ' Pamekasan ', ' Kecamatan ', ''),
(2671, 11, 330, 4650, ' Pasean ', ' Kecamatan ', ''),
(2672, 11, 330, 4651, ' Pegantenan ', ' Kecamatan ', ''),
(2673, 11, 330, 4652, ' Proppo ', ' Kecamatan ', ''),
(2674, 11, 330, 4653, ' Tlanakan ', ' Kecamatan ', ''),
(2675, 11, 330, 4654, ' Waru ', ' Kecamatan ', ''),
(2676, 11, 342, 4793, ' Bangil ', ' Kecamatan ', ''),
(2677, 11, 342, 4794, ' Beji ', ' Kecamatan ', ''),
(2678, 11, 342, 4795, ' Gempol ', ' Kecamatan ', ''),
(2679, 11, 342, 4796, ' Gondang Wetan ', ' Kecamatan ', ''),
(2680, 11, 342, 4797, ' Grati ', ' Kecamatan ', ''),
(2681, 11, 342, 4798, ' Kejayan ', ' Kecamatan ', ''),
(2682, 11, 342, 4799, ' Kraton ', ' Kecamatan ', ''),
(2683, 11, 342, 4800, ' Lekok ', ' Kecamatan ', ''),
(2684, 11, 342, 4801, ' Lumbang ', ' Kecamatan ', ''),
(2685, 11, 342, 4802, ' Nguling ', ' Kecamatan ', ''),
(2686, 11, 342, 4803, ' Pandaan ', ' Kecamatan ', ''),
(2687, 11, 342, 4804, ' Pasrepan ', ' Kecamatan ', ''),
(2688, 11, 342, 4805, ' Pohjentrek ', ' Kecamatan ', ''),
(2689, 11, 342, 4806, ' Prigen ', ' Kecamatan ', ''),
(2690, 11, 342, 4807, ' Purwodadi ', ' Kecamatan ', ''),
(2691, 11, 342, 4808, ' Purwosari ', ' Kecamatan ', ''),
(2692, 11, 342, 4809, ' Puspo ', ' Kecamatan ', ''),
(2693, 11, 342, 4810, ' Rejoso ', ' Kecamatan ', ''),
(2694, 11, 342, 4811, ' Rembang ', ' Kecamatan ', ''),
(2695, 11, 342, 4812, ' Sukorejo ', ' Kecamatan ', ''),
(2696, 11, 342, 4813, ' Tosari ', ' Kecamatan ', ''),
(2697, 11, 342, 4814, ' Tutur ', ' Kecamatan ', ''),
(2698, 11, 342, 4815, ' Winongan ', ' Kecamatan ', ''),
(2699, 11, 342, 4816, ' Wonorejo ', ' Kecamatan ', ''),
(2700, 11, 363, 5071, ' Babadan ', ' Kecamatan ', ''),
(2701, 11, 363, 5072, ' Badegan ', ' Kecamatan ', ''),
(2702, 11, 363, 5073, ' Balong ', ' Kecamatan ', ''),
(2703, 11, 363, 5074, ' Bungkal ', ' Kecamatan ', ''),
(2704, 11, 363, 5075, ' Jambon ', ' Kecamatan ', ''),
(2705, 11, 363, 5076, ' Jenangan ', ' Kecamatan ', ''),
(2706, 11, 363, 5077, ' Jetis ', ' Kecamatan ', ''),
(2707, 11, 363, 5078, ' Kauman ', ' Kecamatan ', ''),
(2708, 11, 363, 5079, ' Mlarak ', ' Kecamatan ', ''),
(2709, 11, 363, 5080, ' Ngebel ', ' Kecamatan ', ''),
(2710, 11, 363, 5081, ' Ngrayun ', ' Kecamatan ', ''),
(2711, 11, 363, 5082, ' Ponorogo ', ' Kecamatan ', ''),
(2712, 11, 363, 5083, ' Pudak ', ' Kecamatan ', ''),
(2713, 11, 363, 5084, ' Pulung ', ' Kecamatan ', ''),
(2714, 11, 363, 5085, ' Sambit ', ' Kecamatan ', ''),
(2715, 11, 363, 5086, ' Sampung ', ' Kecamatan ', ''),
(2716, 11, 363, 5087, ' Sawoo ', ' Kecamatan ', ''),
(2717, 11, 363, 5088, ' Siman ', ' Kecamatan ', ''),
(2718, 11, 363, 5089, ' Slahung ', ' Kecamatan ', ''),
(2719, 11, 363, 5090, ' Sooko ', ' Kecamatan ', ''),
(2720, 11, 363, 5091, ' Sukorejo ', ' Kecamatan ', ''),
(2721, 11, 369, 5141, ' Bantaran ', ' Kecamatan ', ''),
(2722, 11, 369, 5142, ' Banyu Anyar ', ' Kecamatan ', ''),
(2723, 11, 369, 5143, ' Besuk ', ' Kecamatan ', ''),
(2724, 11, 369, 5144, ' Dringu ', ' Kecamatan ', ''),
(2725, 11, 369, 5145, ' Gading ', ' Kecamatan ', ''),
(2726, 11, 369, 5146, ' Gending ', ' Kecamatan ', ''),
(2727, 11, 369, 5147, ' Kota Anyar ', ' Kecamatan ', ''),
(2728, 11, 369, 5148, ' Kraksaan ', ' Kecamatan ', ''),
(2729, 11, 369, 5149, ' Krejengan ', ' Kecamatan ', ''),
(2730, 11, 369, 5150, ' Krucil ', ' Kecamatan ', ''),
(2731, 11, 369, 5151, ' Kuripan ', ' Kecamatan ', ''),
(2732, 11, 369, 5152, ' Leces ', ' Kecamatan ', ''),
(2733, 11, 369, 5153, ' Lumbang ', ' Kecamatan ', ''),
(2734, 11, 369, 5154, ' Maron ', ' Kecamatan ', ''),
(2735, 11, 369, 5155, ' Paiton ', ' Kecamatan ', ''),
(2736, 11, 369, 5156, ' Pajarakan ', ' Kecamatan ', ''),
(2737, 11, 369, 5157, ' Pakuniran ', ' Kecamatan ', ''),
(2738, 11, 369, 5158, ' Sukapura ', ' Kecamatan ', ''),
(2739, 11, 369, 5159, ' Sumber ', ' Kecamatan ', ''),
(2740, 11, 369, 5160, ' Sumberasih ', ' Kecamatan ', ''),
(2741, 11, 369, 5161, ' Tegal Siwalan ', ' Kecamatan ', ''),
(2742, 11, 369, 5162, ' Tiris ', ' Kecamatan ', ''),
(2743, 11, 369, 5163, ' Tongas ', ' Kecamatan ', ''),
(2744, 11, 369, 5164, ' Wonomerto ', ' Kecamatan ', ''),
(2745, 11, 390, 5394, ' Banyuates ', ' Kecamatan ', ''),
(2746, 11, 390, 5395, ' Camplong ', ' Kecamatan ', ''),
(2747, 11, 390, 5396, ' Jrengik ', ' Kecamatan ', ''),
(2748, 11, 390, 5397, ' Karang Penang ', ' Kecamatan ', ''),
(2749, 11, 390, 5398, ' Kedungdung ', ' Kecamatan ', ''),
(2750, 11, 390, 5399, ' Ketapang ', ' Kecamatan ', ''),
(2751, 11, 390, 5400, ' Omben ', ' Kecamatan ', ''),
(2752, 11, 390, 5401, ' Pangarengan ', ' Kecamatan ', ''),
(2753, 11, 390, 5402, ' Robatal ', ' Kecamatan ', ''),
(2754, 11, 390, 5403, ' Sampang ', ' Kecamatan ', ''),
(2755, 11, 390, 5404, ' Sokobanah ', ' Kecamatan ', ''),
(2756, 11, 390, 5405, ' Sreseh ', ' Kecamatan ', ''),
(2757, 11, 390, 5406, ' Tambelangan ', ' Kecamatan ', ''),
(2758, 11, 390, 5407, ' Torjun ', ' Kecamatan ', ''),
(2759, 11, 409, 5631, ' Balongbendo ', ' Kecamatan ', ''),
(2760, 11, 409, 5632, ' Buduran ', ' Kecamatan ', ''),
(2761, 11, 409, 5633, ' Candi ', ' Kecamatan ', ''),
(2762, 11, 409, 5634, ' Gedangan ', ' Kecamatan ', ''),
(2763, 11, 409, 5635, ' Jabon ', ' Kecamatan ', ''),
(2764, 11, 409, 5636, ' Krembung ', ' Kecamatan ', ''),
(2765, 11, 409, 5637, ' Krian ', ' Kecamatan ', ''),
(2766, 11, 409, 5638, ' Porong ', ' Kecamatan ', ''),
(2767, 11, 409, 5639, ' Prambon ', ' Kecamatan ', ''),
(2768, 11, 409, 5640, ' Sedati ', ' Kecamatan ', ''),
(2769, 11, 409, 5641, ' Sidoarjo ', ' Kecamatan ', ''),
(2770, 11, 409, 5642, ' Sukodono ', ' Kecamatan ', ''),
(2771, 11, 409, 5643, ' Taman ', ' Kecamatan ', ''),
(2772, 11, 409, 5644, ' Tanggulangin ', ' Kecamatan ', ''),
(2773, 11, 409, 5645, ' Tarik ', ' Kecamatan ', ''),
(2774, 11, 409, 5646, ' Tulangan ', ' Kecamatan ', ''),
(2775, 11, 409, 5647, ' Waru ', ' Kecamatan ', ''),
(2776, 11, 409, 5648, ' Wonoayu ', ' Kecamatan ', ''),
(2777, 11, 418, 5762, ' Arjasa ', ' Kecamatan ', ''),
(2778, 11, 418, 5763, ' Asembagus ', ' Kecamatan ', ''),
(2779, 11, 418, 5764, ' Banyuglugur ', ' Kecamatan ', ''),
(2780, 11, 418, 5765, ' Banyuputih ', ' Kecamatan ', ''),
(2781, 11, 418, 5766, ' Besuki ', ' Kecamatan ', ''),
(2782, 11, 418, 5767, ' Bungatan ', ' Kecamatan ', ''),
(2783, 11, 418, 5768, ' Jangkar ', ' Kecamatan ', ''),
(2784, 11, 418, 5769, ' Jatibanteng ', ' Kecamatan ', ''),
(2785, 11, 418, 5770, ' Kapongan ', ' Kecamatan ', ''),
(2786, 11, 418, 5771, ' Kendit ', ' Kecamatan ', ''),
(2787, 11, 418, 5772, ' Mangaran ', ' Kecamatan ', ''),
(2788, 11, 418, 5773, ' Mlandingan ', ' Kecamatan ', ''),
(2789, 11, 418, 5774, ' Panarukan ', ' Kecamatan ', ''),
(2790, 11, 418, 5775, ' Panji ', ' Kecamatan ', ''),
(2791, 11, 418, 5776, ' Situbondo ', ' Kecamatan ', ''),
(2792, 11, 418, 5777, ' Suboh ', ' Kecamatan ', ''),
(2793, 11, 418, 5778, ' Sumbermalang ', ' Kecamatan ', ''),
(2794, 11, 441, 6091, ' Ambunten ', ' Kecamatan ', ''),
(2795, 11, 441, 6092, ' Arjasa ', ' Kecamatan ', ''),
(2796, 11, 441, 6093, ' Batang Batang ', ' Kecamatan ', ''),
(2797, 11, 441, 6094, ' Batuan ', ' Kecamatan ', ''),
(2798, 11, 441, 6095, ' Batuputih ', ' Kecamatan ', ''),
(2799, 11, 441, 6096, ' Bluto ', ' Kecamatan ', ''),
(2800, 11, 441, 6097, ' Dasuk ', ' Kecamatan ', ''),
(2801, 11, 441, 6098, ' Dungkek ', ' Kecamatan ', ''),
(2802, 11, 441, 6099, ' Ganding ', ' Kecamatan ', ''),
(2803, 11, 441, 6100, ' Gapura ', ' Kecamatan ', ''),
(2804, 11, 441, 6101, ' Gayam ', ' Kecamatan ', ''),
(2805, 11, 441, 6102, ' Gili Ginting (Giligenteng)', ' Kecamatan ', ''),
(2806, 11, 441, 6103, ' Guluk Guluk ', ' Kecamatan ', ''),
(2807, 11, 441, 6104, ' Kalianget ', ' Kecamatan ', ''),
(2808, 11, 441, 6105, ' Kangayan ', ' Kecamatan ', ''),
(2809, 11, 441, 6106, ' Kota Sumenep ', ' Kecamatan ', ''),
(2810, 11, 441, 6107, ' Lenteng ', ' Kecamatan ', ''),
(2811, 11, 441, 6108, ' Manding ', ' Kecamatan ', ''),
(2812, 11, 441, 6109, ' Masalembu ', ' Kecamatan ', ''),
(2813, 11, 441, 6110, ' Nonggunong ', ' Kecamatan ', ''),
(2814, 11, 441, 6111, ' Pasongsongan ', ' Kecamatan ', ''),
(2815, 11, 441, 6112, ' Pragaan ', ' Kecamatan ', ''),
(2816, 11, 441, 6113, ' Ra\'as (Raas)', 'Kecamatan', ''),
  (2817, 11, 441, 6114, 'Rubaru', 'Kecamatan', ''),
  (2818, 11, 441, 6115, 'Sapeken', 'Kecamatan', ''),
  (2819, 11, 441, 6116, 'Saronggi', 'Kecamatan', ''),
  (2820, 11, 441, 6117, 'Talango', 'Kecamatan', ''),
  (2821, 11, 487, 6759, 'Bendungan', 'Kecamatan', ''),
  (2822, 11, 487, 6760, 'Dongko', 'Kecamatan', ''),
  (2823, 11, 487, 6761, 'Durenan', 'Kecamatan', ''),
  (2824, 11, 487, 6762, 'Gandusari', 'Kecamatan', ''),
  (2825, 11, 487, 6763, 'Kampak', 'Kecamatan', ''),
  (2826, 11, 487, 6764, 'Karangan', 'Kecamatan', ''),
  (2827, 11, 487, 6765, 'Munjungan', 'Kecamatan', ''),
  (2828, 11, 487, 6766, 'Panggul', 'Kecamatan', ''),
  (2829, 11, 487, 6767, 'Pogalan', 'Kecamatan', ''),
  (2830, 11, 487, 6768, 'Pule', 'Kecamatan', ''),
  (2831, 11, 487, 6769, 'Suruh', 'Kecamatan', ''),
  (2832, 11, 487, 6770, 'Trenggalek', 'Kecamatan', ''),
  (2833, 11, 487, 6771, 'Tugu', 'Kecamatan', ''),
  (2834, 11, 487, 6772, 'Watulimo', 'Kecamatan', ''),
  (2835, 11, 492, 6821, 'Bandung', 'Kecamatan', ''),
  (2836, 11, 492, 6822, 'Besuki', 'Kecamatan', ''),
  (2837, 11, 492, 6823, 'Boyolangu', 'Kecamatan', ''),
  (2838, 11, 492, 6824, 'Campur Darat', 'Kecamatan', ''),
  (2839, 11, 492, 6825, 'Gondang', 'Kecamatan', ''),
  (2840, 11, 492, 6826, 'Kalidawir', 'Kecamatan', ''),
  (2841, 11, 492, 6827, 'Karang Rejo', 'Kecamatan', ''),
  (2842, 11, 492, 6828, 'Kauman', 'Kecamatan', ''),
  (2843, 11, 492, 6829, 'Kedungwaru', 'Kecamatan', ''),
  (2844, 11, 492, 6830, 'Ngantru', 'Kecamatan', ''),
  (2845, 11, 492, 6831, 'Ngunut', 'Kecamatan', ''),
  (2846, 11, 492, 6832, 'Pagerwojo', 'Kecamatan', ''),
  (2847, 11, 492, 6833, 'Pakel', 'Kecamatan', ''),
  (2848, 11, 492, 6834, 'Pucanglaban', 'Kecamatan', ''),
  (2849, 11, 492, 6835, 'Rejotangan', 'Kecamatan', ''),
  (2850, 11, 492, 6836, 'Sendang', 'Kecamatan', ''),
  (2851, 11, 492, 6837, 'Sumbergempol', 'Kecamatan', ''),
  (2852, 11, 492, 6838, 'Tanggung Gunung', 'Kecamatan', ''),
  (2853, 11, 492, 6839, 'Tulungagung', 'Kecamatan', ''),
  (2854, 11, 489, 6778, 'Bancar', 'Kecamatan', ''),
  (2855, 11, 489, 6779, 'Bangilan', 'Kecamatan', ''),
  (2856, 11, 489, 6780, 'Grabagan', 'Kecamatan', ''),
  (2857, 11, 489, 6781, 'Jatirogo', 'Kecamatan', ''),
  (2858, 11, 489, 6782, 'Jenu', 'Kecamatan', ''),
  (2859, 11, 489, 6783, 'Kenduruan', 'Kecamatan', ''),
  (2860, 11, 489, 6784, 'Kerek', 'Kecamatan', ''),
  (2861, 11, 489, 6785, 'Merakurak', 'Kecamatan', ''),
  (2862, 11, 489, 6786, 'Montong', 'Kecamatan', ''),
  (2863, 11, 489, 6787, 'Palang', 'Kecamatan', ''),
  (2864, 11, 489, 6788, 'Parengan', 'Kecamatan', ''),
  (2865, 11, 489, 6789, 'Plumpang', 'Kecamatan', ''),
  (2866, 11, 489, 6790, 'Rengel', 'Kecamatan', ''),
  (2867, 11, 489, 6791, 'Semanding', 'Kecamatan', ''),
  (2868, 11, 489, 6792, 'Senori', 'Kecamatan', ''),
  (2869, 11, 489, 6793, 'Singgahan', 'Kecamatan', ''),
  (2870, 11, 489, 6794, 'Soko', 'Kecamatan', ''),
  (2871, 11, 489, 6795, 'Tambakboyo', 'Kecamatan', ''),
  (2872, 11, 489, 6796, 'Tuban', 'Kecamatan', ''),
  (2873, 11, 489, 6797, 'Widang', 'Kecamatan', ''),
  (2874, 11, 75, 996, 'Kepanjen Kidul', 'Kecamatan', ''),
  (2875, 11, 75, 997, 'Sanan Wetan', 'Kecamatan', ''),
  (2876, 11, 75, 998, 'Sukorejo', 'Kecamatan', ''),
  (2877, 11, 179, 2523, 'Kediri Kota', 'Kecamatan', ''),
  (2878, 11, 179, 2524, 'Mojoroto', 'Kecamatan', ''),
  (2879, 11, 179, 2525, 'Pesantren', 'Kecamatan', ''),
  (2880, 11, 248, 3508, 'Kartoharjo', 'Kecamatan', ''),
  (2881, 11, 248, 3509, 'Manguharjo', 'Kecamatan', ''),
  (2882, 11, 248, 3510, 'Taman', 'Kecamatan', ''),
  (2883, 11, 290, 4088, 'Magersari', 'Kecamatan', ''),
  (2884, 11, 290, 4089, 'Prajurit Kulon', 'Kecamatan', ''),
  (2885, 11, 343, 4817, 'Bugul Kidul', 'Kecamatan', ''),
  (2886, 11, 343, 4818, 'Gadingrejo', 'Kecamatan', ''),
  (2887, 11, 343, 4819, 'Panggungrejo', 'Kecamatan', ''),
  (2888, 11, 343, 4820, 'Purworejo', 'Kecamatan', ''),
  (2889, 11, 370, 5165, 'Kademangan', 'Kecamatan', ''),
  (2890, 11, 370, 5166, 'Kanigaran', 'Kecamatan', ''),
  (2891, 11, 370, 5167, 'Kedopok (Kedopak)', 'Kecamatan', ''),
  (2892, 11, 370, 5168, 'Mayangan', 'Kecamatan', ''),
  (2893, 11, 370, 5169, 'Wonoasih', 'Kecamatan', ''),
  (2894, 11, 444, 6131, 'Asemrowo', 'Kecamatan', ''),
  (2895, 11, 444, 6132, 'Benowo', 'Kecamatan', ''),
  (2896, 11, 444, 6133, 'Bubutan', 'Kecamatan', ''),
  (2897, 11, 444, 6134, 'Bulak', 'Kecamatan', ''),
  (2898, 11, 444, 6135, 'Dukuh Pakis', 'Kecamatan', ''),
  (2899, 11, 444, 6136, 'Gayungan', 'Kecamatan', ''),
  (2900, 11, 444, 6137, 'Genteng', 'Kecamatan', ''),
  (2901, 11, 444, 6138, 'Gubeng', 'Kecamatan', ''),
  (2902, 11, 444, 6139, 'Gununganyar', 'Kecamatan', ''),
  (2903, 11, 444, 6140, 'Jambangan', 'Kecamatan', ''),
  (2904, 11, 444, 6141, 'Karangpilang', 'Kecamatan', ''),
  (2905, 11, 444, 6142, 'Kenjeran', 'Kecamatan', ''),
  (2906, 11, 444, 6143, 'Krembangan', 'Kecamatan', ''),
  (2907, 11, 444, 6144, 'Lakar Santri', 'Kecamatan', ''),
  (2908, 11, 444, 6145, 'Mulyorejo', 'Kecamatan', ''),
  (2909, 11, 444, 6146, 'Pabean Cantikan', 'Kecamatan', ''),
  (2910, 11, 444, 6147, 'Pakal', 'Kecamatan', ''),
  (2911, 11, 444, 6148, 'Rungkut', 'Kecamatan', ''),
  (2912, 11, 444, 6149, 'Sambikerep', 'Kecamatan', ''),
  (2913, 11, 444, 6150, 'Sawahan', 'Kecamatan', ''),
  (2914, 11, 444, 6151, 'Semampir', 'Kecamatan', ''),
  (2915, 11, 444, 6152, 'Simokerto', 'Kecamatan', ''),
  (2916, 11, 444, 6153, 'Sukolilo', 'Kecamatan', ''),
  (2917, 11, 444, 6154, 'Sukomanunggal', 'Kecamatan', ''),
  (2918, 11, 444, 6155, 'Tambaksari', 'Kecamatan', ''),
  (2919, 11, 444, 6156, 'Tandes', 'Kecamatan', ''),
  (2920, 11, 444, 6157, 'Tegalsari', 'Kecamatan', ''),
  (2921, 11, 444, 6158, 'Tenggilis Mejoyo', 'Kecamatan', ''),
  (2922, 11, 444, 6159, 'Wiyung', 'Kecamatan', ''),
  (2923, 11, 444, 6160, 'Wonocolo', 'Kecamatan', ''),
  (2924, 11, 444, 6161, 'Wonokromo', 'Kecamatan', ''),
  (2925, 12, 61, 0, 'Bengkayang', 'Kabupaten', '79213'),
  (2926, 12, 168, 0, 'Kapuas Hulu', 'Kabupaten', '78719'),
  (2927, 12, 176, 0, 'Kayong Utara', 'Kabupaten', '78852'),
  (2928, 12, 195, 0, 'Ketapang', 'Kabupaten', '78874'),
  (2929, 12, 208, 0, 'Kubu Raya', 'Kabupaten', '78311'),
  (2930, 12, 228, 0, 'Landak', 'Kabupaten', '78319'),
  (2931, 12, 279, 0, 'Melawi', 'Kabupaten', '78619'),
  (2932, 12, 364, 0, 'Pontianak', 'Kabupaten', '78971'),
  (2933, 12, 365, 0, 'Pontianak', 'Kota', '78112'),
  (2934, 12, 388, 0, 'Sambas', 'Kabupaten', '79453'),
  (2935, 12, 391, 0, 'Sanggau', 'Kabupaten', '78557'),
  (2936, 12, 395, 0, 'Sekadau', 'Kabupaten', '79583'),
  (2937, 12, 415, 0, 'Singkawang', 'Kota', '79117'),
  (2938, 12, 417, 0, 'Sintang', 'Kabupaten', '78619'),
  (2939, 12, 61, 815, 'Bengkayang', 'Kecamatan', ''),
  (2940, 12, 61, 816, 'Capkala', 'Kecamatan', ''),
  (2941, 12, 61, 817, 'Jagoi Babang', 'Kecamatan', ''),
  (2942, 12, 61, 818, 'Ledo', 'Kecamatan', ''),
  (2943, 12, 61, 819, 'Lembah Bawang', 'Kecamatan', ''),
  (2944, 12, 61, 820, 'Lumar', 'Kecamatan', ''),
  (2945, 12, 61, 821, 'Monterado', 'Kecamatan', ''),
  (2946, 12, 61, 822, 'Samalantan', 'Kecamatan', ''),
  (2947, 12, 61, 823, 'Sanggau Ledo', 'Kecamatan', ''),
  (2948, 12, 61, 824, 'Seluas', 'Kecamatan', ''),
  (2949, 12, 61, 825, 'Siding', 'Kecamatan', ''),
  (2950, 12, 61, 826, 'Sungai Betung', 'Kecamatan', ''),
  (2951, 12, 61, 827, 'Sungai Raya', 'Kecamatan', ''),
  (2952, 12, 61, 828, 'Sungai Raya Kepulauan', 'Kecamatan', ''),
  (2953, 12, 61, 829, 'Suti Semarang', 'Kecamatan', ''),
  (2954, 12, 61, 830, 'Teriak', 'Kecamatan', ''),
  (2955, 12, 61, 831, 'Tujuh Belas', 'Kecamatan', ''),
  (2956, 12, 176, 2465, 'Kepulauan Karimata', 'Kecamatan', ''),
  (2957, 12, 176, 2466, 'Pulau Maya (Pulau Maya Karimata)', 'Kecamatan', ''),
  (2958, 12, 176, 2467, 'Seponti', 'Kecamatan', ''),
  (2959, 12, 176, 2468, 'Simpang Hilir', 'Kecamatan', ''),
  (2960, 12, 176, 2469, 'Sukadana', 'Kecamatan', ''),
  (2961, 12, 176, 2470, 'Teluk Batang', 'Kecamatan', ''),
  (2962, 12, 168, 2330, 'Badau', 'Kecamatan', ''),
  (2963, 12, 168, 2331, 'Batang Lupar', 'Kecamatan', ''),
  (2964, 12, 168, 2332, 'Bika', 'Kecamatan', ''),
  (2965, 12, 168, 2333, 'Boyan Tanjung', 'Kecamatan', ''),
  (2966, 12, 168, 2334, 'Bunut Hilir', 'Kecamatan', ''),
  (2967, 12, 168, 2335, 'Bunut Hulu', 'Kecamatan', ''),
  (2968, 12, 168, 2336, 'Embaloh Hilir', 'Kecamatan', ''),
  (2969, 12, 168, 2337, 'Embaloh Hulu', 'Kecamatan', ''),
  (2970, 12, 168, 2338, 'Empanang', 'Kecamatan', ''),
  (2971, 12, 168, 2339, 'Hulu Gurung', 'Kecamatan', ''),
  (2972, 12, 168, 2340, 'Jongkong (Jengkong)', 'Kecamatan', ''),
  (2973, 12, 168, 2341, 'Kalis', 'Kecamatan', ''),
  (2974, 12, 168, 2342, 'Mentebah', 'Kecamatan', ''),
  (2975, 12, 168, 2343, 'Pengkadan (Batu Datu)', 'Kecamatan', ''),
  (2976, 12, 168, 2344, 'Puring Kencana', 'Kecamatan', ''),
  (2977, 12, 168, 2345, 'Putussibau Selatan', 'Kecamatan', ''),
  (2978, 12, 168, 2346, 'Putussibau Utara', 'Kecamatan', ''),
  (2979, 12, 168, 2347, 'Seberuang', 'Kecamatan', ''),
  (2980, 12, 168, 2348, 'Selimbau', 'Kecamatan', ''),
  (2981, 12, 168, 2349, 'Semitau', 'Kecamatan', ''),
  (2982, 12, 168, 2350, 'Silat Hilir', 'Kecamatan', ''),
  (2983, 12, 168, 2351, 'Silat Hulu', 'Kecamatan', ''),
  (2984, 12, 168, 2352, 'Suhaid', 'Kecamatan', ''),
  (2985, 12, 195, 2702, 'Air Upas', 'Kecamatan', ''),
  (2986, 12, 195, 2703, 'Benua Kayong', 'Kecamatan', ''),
  (2987, 12, 195, 2704, 'Delta Pawan', 'Kecamatan', ''),
  (2988, 12, 195, 2705, 'Hulu Sungai', 'Kecamatan', ''),
  (2989, 12, 195, 2706, 'Jelai Hulu', 'Kecamatan', ''),
  (2990, 12, 195, 2707, 'Kendawangan', 'Kecamatan', ''),
  (2991, 12, 195, 2708, 'Manis Mata', 'Kecamatan', ''),
  (2992, 12, 195, 2709, 'Marau', 'Kecamatan', ''),
  (2993, 12, 195, 2710, 'Matan Hilir Selatan', 'Kecamatan', ''),
  (2994, 12, 195, 2711, 'Matan Hilir Utara', 'Kecamatan', ''),
  (2995, 12, 195, 2712, 'Muara Pawan', 'Kecamatan', ''),
  (2996, 12, 195, 2713, 'Nanga Tayap', 'Kecamatan', ''),
  (2997, 12, 195, 2714, 'Pemahan', 'Kecamatan', ''),
  (2998, 12, 195, 2715, 'Sandai', 'Kecamatan', ''),
  (2999, 12, 195, 2716, 'Simpang Dua', 'Kecamatan', ''),
  (3000, 12, 195, 2717, 'Simpang Hulu', 'Kecamatan', ''),
  (3001, 12, 195, 2718, 'Singkup', 'Kecamatan', ''),
  (3002, 12, 195, 2719, 'Sungai Laur', 'Kecamatan', '');
INSERT INTO `location` (`location_id`, `province_id`, `city_id`, `subdistrict_id`, `name`, `type`, `postcode`)
VALUES
  (3003, 12, 195, 2720, 'Sungai Melayu Rayak', 'Kecamatan', ''),
  (3004, 12, 195, 2721, 'Tumbang Titi', 'Kecamatan', ''),
  (3005, 12, 208, 2912, 'Batu Ampar', 'Kecamatan', ''),
  (3006, 12, 208, 2913, 'Kuala Mandor-B', 'Kecamatan', ''),
  (3007, 12, 208, 2914, 'Kubu', 'Kecamatan', ''),
  (3008, 12, 208, 2915, 'Rasau Jaya', 'Kecamatan', ''),
  (3009, 12, 208, 2916, 'Sei/Sungai Ambawang', 'Kecamatan', ''),
  (3010, 12, 208, 2917, 'Sei/Sungai Kakap', 'Kecamatan', ''),
  (3011, 12, 208, 2918, 'Sei/Sungai Raya', 'Kecamatan', ''),
  (3012, 12, 208, 2919, 'Teluk/Telok Pakedai', 'Kecamatan', ''),
  (3013, 12, 208, 2920, 'Terentang', 'Kecamatan', ''),
  (3014, 12, 228, 3247, 'Air Besar', 'Kecamatan', ''),
  (3015, 12, 228, 3248, 'Banyuke Hulu', 'Kecamatan', ''),
  (3016, 12, 228, 3249, 'Jelimpo', 'Kecamatan', ''),
  (3017, 12, 228, 3250, 'Kuala Behe', 'Kecamatan', ''),
  (3018, 12, 228, 3251, 'Mandor', 'Kecamatan', ''),
  (3019, 12, 228, 3252, 'Mempawah Hulu', 'Kecamatan', ''),
  (3020, 12, 228, 3253, 'Menjalin', 'Kecamatan', ''),
  (3021, 12, 228, 3254, 'Menyuke', 'Kecamatan', ''),
  (3022, 12, 228, 3255, 'Meranti', 'Kecamatan', ''),
  (3023, 12, 228, 3256, 'Ngabang', 'Kecamatan', ''),
  (3024, 12, 228, 3257, 'Sebangki', 'Kecamatan', ''),
  (3025, 12, 228, 3258, 'Sengah Temila', 'Kecamatan', ''),
  (3026, 12, 228, 3259, 'Sompak', 'Kecamatan', ''),
  (3027, 12, 279, 3927, 'Belimbing', 'Kecamatan', ''),
  (3028, 12, 279, 3928, 'Belimbing Hulu', 'Kecamatan', ''),
  (3029, 12, 279, 3929, 'Ella Hilir', 'Kecamatan', ''),
  (3030, 12, 279, 3930, 'Menukung', 'Kecamatan', ''),
  (3031, 12, 279, 3931, 'Nanga Pinoh', 'Kecamatan', ''),
  (3032, 12, 279, 3932, 'Pinoh Selatan', 'Kecamatan', ''),
  (3033, 12, 279, 3933, 'Pinoh Utara', 'Kecamatan', ''),
  (3034, 12, 279, 3934, 'Sayan', 'Kecamatan', ''),
  (3035, 12, 279, 3935, 'Sokan', 'Kecamatan', ''),
  (3036, 12, 279, 3936, 'Tanah Pinoh', 'Kecamatan', ''),
  (3037, 12, 279, 3937, 'Tanah Pinoh Barat', 'Kecamatan', ''),
  (3038, 12, 364, 5092, 'Anjongan', 'Kecamatan', ''),
  (3039, 12, 364, 5093, 'Mempawah Hilir', 'Kecamatan', ''),
  (3040, 12, 364, 5094, 'Mempawah Timur', 'Kecamatan', ''),
  (3041, 12, 364, 5095, 'Sadaniang', 'Kecamatan', ''),
  (3042, 12, 364, 5096, 'Segedong', 'Kecamatan', ''),
  (3043, 12, 364, 5097, 'Sei/Sungai Kunyit', 'Kecamatan', ''),
  (3044, 12, 364, 5098, 'Sei/Sungai Pinyuh', 'Kecamatan', ''),
  (3045, 12, 364, 5099, 'Siantan', 'Kecamatan', ''),
  (3046, 12, 364, 5100, 'Toho', 'Kecamatan', ''),
  (3047, 12, 365, 5101, 'Pontianak Barat', 'Kecamatan', ''),
  (3048, 12, 365, 5102, 'Pontianak Kota', 'Kecamatan', ''),
  (3049, 12, 365, 5103, 'Pontianak Selatan', 'Kecamatan', ''),
  (3050, 12, 365, 5104, 'Pontianak Tenggara', 'Kecamatan', ''),
  (3051, 12, 365, 5105, 'Pontianak Timur', 'Kecamatan', ''),
  (3052, 12, 365, 5106, 'Pontianak Utara', 'Kecamatan', ''),
  (3053, 12, 388, 5366, 'Galing', 'Kecamatan', ''),
  (3054, 12, 388, 5367, 'Jawai', 'Kecamatan', ''),
  (3055, 12, 388, 5368, 'Jawai Selatan', 'Kecamatan', ''),
  (3056, 12, 388, 5369, 'Paloh', 'Kecamatan', ''),
  (3057, 12, 388, 5370, 'Pemangkat', 'Kecamatan', ''),
  (3058, 12, 388, 5371, 'Sajad', 'Kecamatan', ''),
  (3059, 12, 388, 5372, 'Sajingan Besar', 'Kecamatan', ''),
  (3060, 12, 388, 5373, 'Salatiga', 'Kecamatan', ''),
  (3061, 12, 388, 5374, 'Sambas', 'Kecamatan', ''),
  (3062, 12, 388, 5375, 'Sebawi', 'Kecamatan', ''),
  (3063, 12, 388, 5376, 'Sejangkung', 'Kecamatan', ''),
  (3064, 12, 388, 5377, 'Selakau', 'Kecamatan', ''),
  (3065, 12, 388, 5378, 'Selakau Timur', 'Kecamatan', ''),
  (3066, 12, 388, 5379, 'Semparuk', 'Kecamatan', ''),
  (3067, 12, 388, 5380, 'Subah', 'Kecamatan', ''),
  (3068, 12, 388, 5381, 'Tangaran', 'Kecamatan', ''),
  (3069, 12, 388, 5382, 'Tebas', 'Kecamatan', ''),
  (3070, 12, 388, 5383, 'Tekarang', 'Kecamatan', ''),
  (3071, 12, 388, 5384, 'Teluk Keramat', 'Kecamatan', ''),
  (3072, 12, 391, 5408, 'Balai', 'Kecamatan', ''),
  (3073, 12, 391, 5409, 'Beduai (Beduwai)', 'Kecamatan', ''),
  (3074, 12, 391, 5410, 'Bonti', 'Kecamatan', ''),
  (3075, 12, 391, 5411, 'Entikong', 'Kecamatan', ''),
  (3076, 12, 391, 5412, 'Jangkang', 'Kecamatan', ''),
  (3077, 12, 391, 5413, 'Kapuas (Sanggau Kapuas)', 'Kecamatan', ''),
  (3078, 12, 391, 5414, 'Kembayan', 'Kecamatan', ''),
  (3079, 12, 391, 5415, 'Meliau', 'Kecamatan', ''),
  (3080, 12, 391, 5416, 'Mukok', 'Kecamatan', ''),
  (3081, 12, 391, 5417, 'Noyan', 'Kecamatan', ''),
  (3082, 12, 391, 5418, 'Parindu', 'Kecamatan', ''),
  (3083, 12, 391, 5419, 'Sekayam', 'Kecamatan', ''),
  (3084, 12, 391, 5420, 'Tayan Hilir', 'Kecamatan', ''),
  (3085, 12, 391, 5421, 'Tayan Hulu', 'Kecamatan', ''),
  (3086, 12, 391, 5422, 'Toba', 'Kecamatan', ''),
  (3087, 12, 395, 5447, 'Belitang', 'Kecamatan', ''),
  (3088, 12, 395, 5448, 'Belitang Hilir', 'Kecamatan', ''),
  (3089, 12, 395, 5449, 'Belitang Hulu', 'Kecamatan', ''),
  (3090, 12, 395, 5450, 'Nanga Mahap', 'Kecamatan', ''),
  (3091, 12, 395, 5451, 'Nanga Taman', 'Kecamatan', ''),
  (3092, 12, 395, 5452, 'Sekadau Hilir', 'Kecamatan', ''),
  (3093, 12, 395, 5453, 'Sekadau Hulu', 'Kecamatan', ''),
  (3094, 12, 415, 5734, 'Singkawang Barat', 'Kecamatan', ''),
  (3095, 12, 415, 5735, 'Singkawang Selatan', 'Kecamatan', ''),
  (3096, 12, 415, 5736, 'Singkawang Tengah', 'Kecamatan', ''),
  (3097, 12, 415, 5737, 'Singkawang Timur', 'Kecamatan', ''),
  (3098, 12, 415, 5738, 'Singkawang Utara', 'Kecamatan', ''),
  (3099, 12, 417, 5748, 'Ambalau', 'Kecamatan', ''),
  (3100, 12, 417, 5749, 'Binjai Hulu', 'Kecamatan', ''),
  (3101, 12, 417, 5750, 'Dedai', 'Kecamatan', ''),
  (3102, 12, 417, 5751, 'Kayan Hilir', 'Kecamatan', ''),
  (3103, 12, 417, 5752, 'Kayan Hulu', 'Kecamatan', ''),
  (3104, 12, 417, 5753, 'Kelam Permai', 'Kecamatan', ''),
  (3105, 12, 417, 5754, 'Ketungau Hilir', 'Kecamatan', ''),
  (3106, 12, 417, 5755, 'Ketungau Hulu', 'Kecamatan', ''),
  (3107, 12, 417, 5756, 'Ketungau Tengah', 'Kecamatan', ''),
  (3108, 12, 417, 5757, 'Sepauk', 'Kecamatan', ''),
  (3109, 12, 417, 5758, 'Serawai (Nanga Serawai)', 'Kecamatan', ''),
  (3110, 12, 417, 5759, 'Sintang', 'Kecamatan', ''),
  (3111, 12, 417, 5760, 'Sungai Tebelian', 'Kecamatan', ''),
  (3112, 12, 417, 5761, 'Tempunak', 'Kecamatan', ''),
  (3113, 13, 18, 0, 'Balangan', 'Kabupaten', '71611'),
  (3114, 13, 33, 0, 'Banjar', 'Kabupaten', '70619'),
  (3115, 13, 35, 0, 'Banjarbaru', 'Kota', '70712'),
  (3116, 13, 36, 0, 'Banjarmasin', 'Kota', '70117'),
  (3117, 13, 43, 0, 'Barito Kuala', 'Kabupaten', '70511'),
  (3118, 13, 143, 0, 'Hulu Sungai Selatan', 'Kabupaten', '71212'),
  (3119, 13, 144, 0, 'Hulu Sungai Tengah', 'Kabupaten', '71313'),
  (3120, 13, 145, 0, 'Hulu Sungai Utara', 'Kabupaten', '71419'),
  (3121, 13, 203, 0, 'Kotabaru', 'Kabupaten', '72119'),
  (3122, 13, 446, 0, 'Tabalong', 'Kabupaten', '71513'),
  (3123, 13, 452, 0, 'Tanah Bumbu', 'Kabupaten', '72211'),
  (3124, 13, 454, 0, 'Tanah Laut', 'Kabupaten', '70811'),
  (3125, 13, 466, 0, 'Tapin', 'Kabupaten', '71119'),
  (3126, 13, 18, 264, 'Awayan', 'Kecamatan', ''),
  (3127, 13, 18, 265, 'Batu Mandi', 'Kecamatan', ''),
  (3128, 13, 18, 266, 'Halong', 'Kecamatan', ''),
  (3129, 13, 18, 267, 'Juai', 'Kecamatan', ''),
  (3130, 13, 18, 268, 'Lampihong', 'Kecamatan', ''),
  (3131, 13, 18, 269, 'Paringin', 'Kecamatan', ''),
  (3132, 13, 18, 270, 'Paringin Selatan', 'Kecamatan', ''),
  (3133, 13, 18, 271, 'Tebing Tinggi', 'Kecamatan', ''),
  (3134, 13, 33, 476, 'Aluh-Aluh', 'Kecamatan', ''),
  (3135, 13, 33, 477, 'Aranio', 'Kecamatan', ''),
  (3136, 13, 33, 478, 'Astambul', 'Kecamatan', ''),
  (3137, 13, 33, 479, 'Beruntung Baru', 'Kecamatan', ''),
  (3138, 13, 33, 480, 'Gambut', 'Kecamatan', ''),
  (3139, 13, 33, 481, 'Karang Intan', 'Kecamatan', ''),
  (3140, 13, 33, 482, 'Kertak Hanyar', 'Kecamatan', ''),
  (3141, 13, 33, 483, 'Martapura Barat', 'Kecamatan', ''),
  (3142, 13, 33, 484, 'Martapura Kota', 'Kecamatan', ''),
  (3143, 13, 33, 485, 'Martapura Timur', 'Kecamatan', ''),
  (3144, 13, 33, 486, 'Mataraman', 'Kecamatan', ''),
  (3145, 13, 33, 487, 'Pengaron', 'Kecamatan', ''),
  (3146, 13, 33, 488, 'Peramasan', 'Kecamatan', ''),
  (3147, 13, 33, 489, 'Sambung Makmur', 'Kecamatan', ''),
  (3148, 13, 33, 490, 'Sei/Sungai Pinang', 'Kecamatan', ''),
  (3149, 13, 33, 491, 'Sei/Sungai Tabuk', 'Kecamatan', ''),
  (3150, 13, 33, 492, 'Simpang Empat', 'Kecamatan', ''),
  (3151, 13, 33, 493, 'Tatah Makmur', 'Kecamatan', ''),
  (3152, 13, 33, 494, 'Telaga Bauntung', 'Kecamatan', ''),
  (3153, 13, 35, 499, 'Banjar Baru Selatan', 'Kecamatan', ''),
  (3154, 13, 35, 500, 'Banjar Baru Utara', 'Kecamatan', ''),
  (3155, 13, 35, 501, 'Cempaka', 'Kecamatan', ''),
  (3156, 13, 35, 502, 'Landasan Ulin', 'Kecamatan', ''),
  (3157, 13, 35, 503, 'Liang Anggang', 'Kecamatan', ''),
  (3158, 13, 36, 504, 'Banjarmasin Barat', 'Kecamatan', ''),
  (3159, 13, 36, 505, 'Banjarmasin Selatan', 'Kecamatan', ''),
  (3160, 13, 36, 506, 'Banjarmasin Tengah', 'Kecamatan', ''),
  (3161, 13, 36, 507, 'Banjarmasin Timur', 'Kecamatan', ''),
  (3162, 13, 36, 508, 'Banjarmasin Utara', 'Kecamatan', ''),
  (3163, 13, 43, 624, 'Alalak', 'Kecamatan', ''),
  (3164, 13, 43, 625, 'Anjir Muara', 'Kecamatan', ''),
  (3165, 13, 43, 626, 'Anjir Pasar', 'Kecamatan', ''),
  (3166, 13, 43, 627, 'Bakumpai', 'Kecamatan', ''),
  (3167, 13, 43, 628, 'Barambai', 'Kecamatan', ''),
  (3168, 13, 43, 629, 'Belawang', 'Kecamatan', ''),
  (3169, 13, 43, 630, 'Cerbon', 'Kecamatan', ''),
  (3170, 13, 43, 631, 'Jejangkit', 'Kecamatan', ''),
  (3171, 13, 43, 632, 'Kuripan', 'Kecamatan', ''),
  (3172, 13, 43, 633, 'Mandastana', 'Kecamatan', ''),
  (3173, 13, 43, 634, 'Marabahan', 'Kecamatan', ''),
  (3174, 13, 43, 635, 'Mekar Sari', 'Kecamatan', ''),
  (3175, 13, 43, 636, 'Rantau Badauh', 'Kecamatan', ''),
  (3176, 13, 43, 637, 'Tabukan', 'Kecamatan', ''),
  (3177, 13, 43, 638, 'Tabunganen', 'Kecamatan', ''),
  (3178, 13, 43, 639, 'Tamban', 'Kecamatan', ''),
  (3179, 13, 43, 640, 'Wanaraya', 'Kecamatan', ''),
  (3180, 13, 143, 1974, 'Angkinang', 'Kecamatan', ''),
  (3181, 13, 143, 1975, 'Daha Barat', 'Kecamatan', ''),
  (3182, 13, 143, 1976, 'Daha Selatan', 'Kecamatan', ''),
  (3183, 13, 143, 1977, 'Daha Utara', 'Kecamatan', ''),
  (3184, 13, 143, 1978, 'Kalumpang (Kelumpang)', 'Kecamatan', ''),
  (3185, 13, 143, 1979, 'Kandangan', 'Kecamatan', ''),
  (3186, 13, 143, 1980, 'Loksado', 'Kecamatan', ''),
  (3187, 13, 143, 1981, 'Padang Batung', 'Kecamatan', ''),
  (3188, 13, 143, 1982, 'Simpur', 'Kecamatan', ''),
  (3189, 13, 143, 1983, 'Sungai Raya', 'Kecamatan', ''),
  (3190, 13, 143, 1984, 'Telaga Langsat', 'Kecamatan', ''),
  (3191, 13, 144, 1985, 'Barabai', 'Kecamatan', ''),
  (3192, 13, 144, 1986, 'Batang Alai Selatan', 'Kecamatan', ''),
  (3193, 13, 144, 1987, 'Batang Alai Timur', 'Kecamatan', ''),
  (3194, 13, 144, 1988, 'Batang Alai Utara', 'Kecamatan', ''),
  (3195, 13, 144, 1989, 'Batu Benawa', 'Kecamatan', ''),
  (3196, 13, 144, 1990, 'Hantakan', 'Kecamatan', ''),
  (3197, 13, 144, 1991, 'Haruyan', 'Kecamatan', ''),
  (3198, 13, 144, 1992, 'Labuan Amas Selatan', 'Kecamatan', ''),
  (3199, 13, 144, 1993, 'Labuan Amas Utara', 'Kecamatan', ''),
  (3200, 13, 144, 1994, 'Limpasu', 'Kecamatan', ''),
  (3201, 13, 144, 1995, 'Pandawan', 'Kecamatan', ''),
  (3202, 13, 145, 1996, 'Amuntai Selatan', 'Kecamatan', ''),
  (3203, 13, 145, 1997, 'Amuntai Tengah', 'Kecamatan', ''),
  (3204, 13, 145, 1998, 'Amuntai Utara', 'Kecamatan', ''),
  (3205, 13, 145, 1999, 'Babirik', 'Kecamatan', ''),
  (3206, 13, 145, 2000, 'Banjang', 'Kecamatan', ''),
  (3207, 13, 145, 2001, 'Danau Panggang', 'Kecamatan', ''),
  (3208, 13, 145, 2002, 'Haur Gading', 'Kecamatan', ''),
  (3209, 13, 145, 2003, 'Paminggir', 'Kecamatan', ''),
  (3210, 13, 145, 2004, 'Sungai Pandan', 'Kecamatan', ''),
  (3211, 13, 145, 2005, 'Sungai Tabukan', 'Kecamatan', ''),
  (3212, 13, 203, 2849, 'Hampang', 'Kecamatan', ''),
  (3213, 13, 203, 2850, 'Kelumpang Barat', 'Kecamatan', ''),
  (3214, 13, 203, 2851, 'Kelumpang Hilir', 'Kecamatan', ''),
  (3215, 13, 203, 2852, 'Kelumpang Hulu', 'Kecamatan', ''),
  (3216, 13, 203, 2853, 'Kelumpang Selatan', 'Kecamatan', ''),
  (3217, 13, 203, 2854, 'Kelumpang Tengah', 'Kecamatan', ''),
  (3218, 13, 203, 2855, 'Kelumpang Utara', 'Kecamatan', ''),
  (3219, 13, 203, 2856, 'Pamukan Barat', 'Kecamatan', ''),
  (3220, 13, 203, 2857, 'Pamukan Selatan', 'Kecamatan', ''),
  (3221, 13, 203, 2858, 'Pamukan Utara', 'Kecamatan', ''),
  (3222, 13, 203, 2859, 'Pulau Laut Barat', 'Kecamatan', ''),
  (3223, 13, 203, 2860, 'Pulau Laut Kepulauan', 'Kecamatan', ''),
  (3224, 13, 203, 2861, 'Pulau Laut Selatan', 'Kecamatan', ''),
  (3225, 13, 203, 2862, 'Pulau Laut Tanjung Selayar', 'Kecamatan', ''),
  (3226, 13, 203, 2863, 'Pulau Laut Tengah', 'Kecamatan', ''),
  (3227, 13, 203, 2864, 'Pulau Laut Timur', 'Kecamatan', ''),
  (3228, 13, 203, 2865, 'Pulau Laut Utara', 'Kecamatan', ''),
  (3229, 13, 203, 2866, 'Pulau Sebuku', 'Kecamatan', ''),
  (3230, 13, 203, 2867, 'Pulau Sembilan', 'Kecamatan', ''),
  (3231, 13, 203, 2868, 'Sampanahan', 'Kecamatan', ''),
  (3232, 13, 203, 2869, 'Sungai Durian', 'Kecamatan', ''),
  (3233, 13, 446, 6167, 'Banua Lawas', 'Kecamatan', ''),
  (3234, 13, 446, 6168, 'Bintang Ara', 'Kecamatan', ''),
  (3235, 13, 446, 6169, 'Haruai', 'Kecamatan', ''),
  (3236, 13, 446, 6170, 'Jaro', 'Kecamatan', ''),
  (3237, 13, 446, 6171, 'Kelua (Klua)', 'Kecamatan', ''),
  (3238, 13, 446, 6172, 'Muara Harus', 'Kecamatan', ''),
  (3239, 13, 446, 6173, 'Muara Uya', 'Kecamatan', ''),
  (3240, 13, 446, 6174, 'Murung Pudak', 'Kecamatan', ''),
  (3241, 13, 446, 6175, 'Pugaan', 'Kecamatan', ''),
  (3242, 13, 446, 6176, 'Tanjung', 'Kecamatan', ''),
  (3243, 13, 446, 6177, 'Tanta', 'Kecamatan', ''),
  (3244, 13, 446, 6178, 'Upau', 'Kecamatan', ''),
  (3245, 13, 452, 6233, 'Angsana', 'Kecamatan', ''),
  (3246, 13, 452, 6234, 'Batulicin', 'Kecamatan', ''),
  (3247, 13, 452, 6235, 'Karang Bintang', 'Kecamatan', ''),
  (3248, 13, 452, 6236, 'Kuranji', 'Kecamatan', ''),
  (3249, 13, 452, 6237, 'Kusan Hilir', 'Kecamatan', ''),
  (3250, 13, 452, 6238, 'Kusan Hulu', 'Kecamatan', ''),
  (3251, 13, 452, 6239, 'Mantewe', 'Kecamatan', ''),
  (3252, 13, 452, 6240, 'Satui', 'Kecamatan', ''),
  (3253, 13, 452, 6241, 'Simpang Empat', 'Kecamatan', ''),
  (3254, 13, 452, 6242, 'Sungai Loban', 'Kecamatan', ''),
  (3255, 13, 454, 6257, 'Bajuin', 'Kecamatan', ''),
  (3256, 13, 454, 6258, 'Bati-Bati', 'Kecamatan', ''),
  (3257, 13, 454, 6259, 'Batu Ampar', 'Kecamatan', ''),
  (3258, 13, 454, 6260, 'Bumi Makmur', 'Kecamatan', ''),
  (3259, 13, 454, 6261, 'Jorong', 'Kecamatan', ''),
  (3260, 13, 454, 6262, 'Kintap', 'Kecamatan', ''),
  (3261, 13, 454, 6263, 'Kurau', 'Kecamatan', ''),
  (3262, 13, 454, 6264, 'Panyipatan', 'Kecamatan', ''),
  (3263, 13, 454, 6265, 'Pelaihari', 'Kecamatan', ''),
  (3264, 13, 454, 6266, 'Takisung', 'Kecamatan', ''),
  (3265, 13, 454, 6267, 'Tambang Ulang', 'Kecamatan', ''),
  (3266, 13, 466, 6420, 'Bakarangan', 'Kecamatan', ''),
  (3267, 13, 466, 6421, 'Binuang', 'Kecamatan', ''),
  (3268, 13, 466, 6422, 'Bungur', 'Kecamatan', ''),
  (3269, 13, 466, 6423, 'Candi Laras Selatan', 'Kecamatan', ''),
  (3270, 13, 466, 6424, 'Candi Laras Utara', 'Kecamatan', ''),
  (3271, 13, 466, 6425, 'Hatungun', 'Kecamatan', ''),
  (3272, 13, 466, 6426, 'Lokpaikat', 'Kecamatan', ''),
  (3273, 13, 466, 6427, 'Piani', 'Kecamatan', ''),
  (3274, 13, 466, 6428, 'Salam Babaris', 'Kecamatan', ''),
  (3275, 13, 466, 6429, 'Tapin Selatan', 'Kecamatan', ''),
  (3276, 13, 466, 6430, 'Tapin Tengah', 'Kecamatan', ''),
  (3277, 13, 466, 6431, 'Tapin Utara', 'Kecamatan', ''),
  (3278, 14, 44, 0, 'Barito Selatan', 'Kabupaten', '73711'),
  (3279, 14, 45, 0, 'Barito Timur', 'Kabupaten', '73671'),
  (3280, 14, 46, 0, 'Barito Utara', 'Kabupaten', '73881'),
  (3281, 14, 136, 0, 'Gunung Mas', 'Kabupaten', '74511'),
  (3282, 14, 167, 0, 'Kapuas', 'Kabupaten', '73583'),
  (3283, 14, 174, 0, 'Katingan', 'Kabupaten', '74411'),
  (3284, 14, 205, 0, 'Kotawaringin Barat', 'Kabupaten', '74119'),
  (3285, 14, 206, 0, 'Kotawaringin Timur', 'Kabupaten', '74364'),
  (3286, 14, 221, 0, 'Lamandau', 'Kabupaten', '74611'),
  (3287, 14, 296, 0, 'Murung Raya', 'Kabupaten', '73911'),
  (3288, 14, 326, 0, 'Palangka Raya', 'Kota', '73112'),
  (3289, 14, 371, 0, 'Pulang Pisau', 'Kabupaten', '74811'),
  (3290, 14, 405, 0, 'Seruyan', 'Kabupaten', '74211'),
  (3291, 14, 432, 0, 'Sukamara', 'Kabupaten', '74712'),
  (3292, 14, 44, 641, 'Dusun Hilir', 'Kecamatan', ''),
  (3293, 14, 44, 642, 'Dusun Selatan', 'Kecamatan', ''),
  (3294, 14, 44, 643, 'Dusun Utara', 'Kecamatan', ''),
  (3295, 14, 44, 644, 'Gunung Bintang Awai', 'Kecamatan', ''),
  (3296, 14, 44, 645, 'Jenamas', 'Kecamatan', ''),
  (3297, 14, 44, 646, 'Karau Kuala', 'Kecamatan', ''),
  (3298, 14, 45, 647, 'Awang', 'Kecamatan', ''),
  (3299, 14, 45, 648, 'Benua Lima', 'Kecamatan', ''),
  (3300, 14, 45, 649, 'Dusun Tengah', 'Kecamatan', ''),
  (3301, 14, 45, 650, 'Dusun Timur', 'Kecamatan', ''),
  (3302, 14, 45, 651, 'Karusen Janang', 'Kecamatan', ''),
  (3303, 14, 45, 652, 'Paju Epat', 'Kecamatan', ''),
  (3304, 14, 45, 653, 'Paku', 'Kecamatan', ''),
  (3305, 14, 45, 654, 'Patangkep Tutui', 'Kecamatan', ''),
  (3306, 14, 45, 655, 'Pematang Karau', 'Kecamatan', ''),
  (3307, 14, 45, 656, 'Raren Batuah', 'Kecamatan', ''),
  (3308, 14, 46, 657, 'Gunung Purei', 'Kecamatan', ''),
  (3309, 14, 46, 658, 'Gunung Timang', 'Kecamatan', ''),
  (3310, 14, 46, 659, 'Lahei', 'Kecamatan', ''),
  (3311, 14, 46, 660, 'Lahei Barat', 'Kecamatan', ''),
  (3312, 14, 46, 661, 'Montallat (Montalat)', 'Kecamatan', ''),
  (3313, 14, 46, 662, 'Teweh Baru', 'Kecamatan', ''),
  (3314, 14, 46, 663, 'Teweh Selatan', 'Kecamatan', ''),
  (3315, 14, 46, 664, 'Teweh Tengah', 'Kecamatan', ''),
  (3316, 14, 46, 665, 'Teweh Timur', 'Kecamatan', ''),
  (3317, 14, 136, 1883, 'Damang Batu', 'Kecamatan', ''),
  (3318, 14, 136, 1884, 'Kahayan Hulu Utara', 'Kecamatan', ''),
  (3319, 14, 136, 1885, 'Kurun', 'Kecamatan', ''),
  (3320, 14, 136, 1886, 'Manuhing', 'Kecamatan', ''),
  (3321, 14, 136, 1887, 'Manuhing Raya', 'Kecamatan', ''),
  (3322, 14, 136, 1888, 'Mihing Raya', 'Kecamatan', ''),
  (3323, 14, 136, 1889, 'Miri Manasa', 'Kecamatan', ''),
  (3324, 14, 136, 1890, 'Rungan', 'Kecamatan', ''),
  (3325, 14, 136, 1891, 'Rungan Barat', 'Kecamatan', ''),
  (3326, 14, 136, 1892, 'Rungan Hulu', 'Kecamatan', ''),
  (3327, 14, 136, 1893, 'Sepang (Sepang Simin)', 'Kecamatan', ''),
  (3328, 14, 136, 1894, 'Tewah', 'Kecamatan', ''),
  (3329, 14, 167, 2313, 'Basarang', 'Kecamatan', ''),
  (3330, 14, 167, 2314, 'Bataguh', 'Kecamatan', ''),
  (3331, 14, 167, 2315, 'Dadahup', 'Kecamatan', ''),
  (3332, 14, 167, 2316, 'Kapuas Barat', 'Kecamatan', ''),
  (3333, 14, 167, 2317, 'Kapuas Hilir', 'Kecamatan', ''),
  (3334, 14, 167, 2318, 'Kapuas Hulu', 'Kecamatan', ''),
  (3335, 14, 167, 2319, 'Kapuas Kuala', 'Kecamatan', ''),
  (3336, 14, 167, 2320, 'Kapuas Murung', 'Kecamatan', ''),
  (3337, 14, 167, 2321, 'Kapuas Tengah', 'Kecamatan', ''),
  (3338, 14, 167, 2322, 'Kapuas Timur', 'Kecamatan', ''),
  (3339, 14, 167, 2323, 'Mandau Talawang', 'Kecamatan', ''),
  (3340, 14, 167, 2324, 'Mantangai', 'Kecamatan', ''),
  (3341, 14, 167, 2325, 'Pasak Talawang', 'Kecamatan', ''),
  (3342, 14, 167, 2326, 'Pulau Petak', 'Kecamatan', ''),
  (3343, 14, 167, 2327, 'Selat', 'Kecamatan', ''),
  (3344, 14, 167, 2328, 'Tamban Catur', 'Kecamatan', ''),
  (3345, 14, 167, 2329, 'Timpah', 'Kecamatan', ''),
  (3346, 14, 174, 2437, 'Bukit Raya', 'Kecamatan', ''),
  (3347, 14, 174, 2438, 'Kamipang', 'Kecamatan', ''),
  (3348, 14, 174, 2439, 'Katingan Hilir', 'Kecamatan', ''),
  (3349, 14, 174, 2440, 'Katingan Hulu', 'Kecamatan', ''),
  (3350, 14, 174, 2441, 'Katingan Kuala', 'Kecamatan', ''),
  (3351, 14, 174, 2442, 'Katingan Tengah', 'Kecamatan', ''),
  (3352, 14, 174, 2443, 'Marikit', 'Kecamatan', ''),
  (3353, 14, 174, 2444, 'Mendawai', 'Kecamatan', ''),
  (3354, 14, 174, 2445, 'Petak Malai', 'Kecamatan', ''),
  (3355, 14, 174, 2446, 'Pulau Malan', 'Kecamatan', ''),
  (3356, 14, 174, 2447, 'Sanaman Mantikei (Senamang Mantikei)', 'Kecamatan', ''),
  (3357, 14, 174, 2448, 'Tasik Payawan', 'Kecamatan', ''),
  (3358, 14, 174, 2449, 'Tewang Sanggalang Garing (Sangalang)', 'Kecamatan', ''),
  (3359, 14, 205, 2874, 'Arut Selatan', 'Kecamatan', ''),
  (3360, 14, 205, 2875, 'Arut Utara', 'Kecamatan', ''),
  (3361, 14, 205, 2876, 'Kotawaringin Lama', 'Kecamatan', ''),
  (3362, 14, 205, 2877, 'Kumai', 'Kecamatan', ''),
  (3363, 14, 205, 2878, 'Pangkalan Banteng', 'Kecamatan', ''),
  (3364, 14, 205, 2879, 'Pangkalan Lada', 'Kecamatan', ''),
  (3365, 14, 206, 2880, 'Antang Kalang', 'Kecamatan', ''),
  (3366, 14, 206, 2881, 'Baamang', 'Kecamatan', ''),
  (3367, 14, 206, 2882, 'Bukit Santuei', 'Kecamatan', ''),
  (3368, 14, 206, 2883, 'Cempaga', 'Kecamatan', ''),
  (3369, 14, 206, 2884, 'Cempaga Hulu', 'Kecamatan', ''),
  (3370, 14, 206, 2885, 'Kota Besi', 'Kecamatan', ''),
  (3371, 14, 206, 2886, 'Mentawa Baru (Ketapang)', 'Kecamatan', ''),
  (3372, 14, 206, 2887, 'Mentaya Hilir Selatan', 'Kecamatan', ''),
  (3373, 14, 206, 2888, 'Mentaya Hilir Utara', 'Kecamatan', ''),
  (3374, 14, 206, 2889, 'Mentaya Hulu', 'Kecamatan', ''),
  (3375, 14, 206, 2890, 'Parenggean', 'Kecamatan', ''),
  (3376, 14, 206, 2891, 'Pulau Hanaut', 'Kecamatan', ''),
  (3377, 14, 206, 2892, 'Seranau', 'Kecamatan', ''),
  (3378, 14, 206, 2893, 'Telaga Antang', 'Kecamatan', ''),
  (3379, 14, 206, 2894, 'Telawang', 'Kecamatan', ''),
  (3380, 14, 206, 2895, 'Teluk Sampit', 'Kecamatan', ''),
  (3381, 14, 206, 2896, 'Tualan Hulu', 'Kecamatan', ''),
  (3382, 14, 221, 3105, 'Batangkawa', 'Kecamatan', ''),
  (3383, 14, 221, 3106, 'Belantikan Raya', 'Kecamatan', ''),
  (3384, 14, 221, 3107, 'Bulik', 'Kecamatan', ''),
  (3385, 14, 221, 3108, 'Bulik Timur', 'Kecamatan', ''),
  (3386, 14, 221, 3109, 'Delang', 'Kecamatan', ''),
  (3387, 14, 221, 3110, 'Lamandau', 'Kecamatan', ''),
  (3388, 14, 221, 3111, 'Menthobi Raya', 'Kecamatan', ''),
  (3389, 14, 221, 3112, 'Sematu Jaya', 'Kecamatan', ''),
  (3390, 14, 296, 4190, 'Barito Tuhup Raya', 'Kecamatan', ''),
  (3391, 14, 296, 4191, 'Laung Tuhup', 'Kecamatan', ''),
  (3392, 14, 296, 4192, 'Murung', 'Kecamatan', ''),
  (3393, 14, 296, 4193, 'Permata Intan', 'Kecamatan', ''),
  (3394, 14, 296, 4194, 'Seribu Riam', 'Kecamatan', ''),
  (3395, 14, 296, 4195, 'Sumber Barito', 'Kecamatan', ''),
  (3396, 14, 296, 4196, 'Sungai Babuat', 'Kecamatan', ''),
  (3397, 14, 296, 4197, 'Tanah Siang', 'Kecamatan', ''),
  (3398, 14, 296, 4198, 'Tanah Siang Selatan', 'Kecamatan', ''),
  (3399, 14, 296, 4199, 'Uut Murung', 'Kecamatan', ''),
  (3400, 14, 326, 4604, 'Bukit Batu', 'Kecamatan', ''),
  (3401, 14, 326, 4605, 'Jekan Raya', 'Kecamatan', ''),
  (3402, 14, 326, 4606, 'Pahandut', 'Kecamatan', ''),
  (3403, 14, 326, 4607, 'Rakumpit', 'Kecamatan', ''),
  (3404, 14, 326, 4608, 'Sebangau', 'Kecamatan', ''),
  (3405, 14, 371, 5170, 'Banama Tingang', 'Kecamatan', ''),
  (3406, 14, 371, 5171, 'Jabiren Raya', 'Kecamatan', ''),
  (3407, 14, 371, 5172, 'Kahayan Hilir', 'Kecamatan', ''),
  (3408, 14, 371, 5173, 'Kahayan Kuala', 'Kecamatan', ''),
  (3409, 14, 371, 5174, 'Kahayan Tengah', 'Kecamatan', ''),
  (3410, 14, 371, 5175, 'Maliku', 'Kecamatan', ''),
  (3411, 14, 371, 5176, 'Pandih Batu', 'Kecamatan', ''),
  (3412, 14, 371, 5177, 'Sebangau Kuala', 'Kecamatan', ''),
  (3413, 14, 405, 5592, 'Batu Ampar', 'Kecamatan', ''),
  (3414, 14, 405, 5593, 'Danau Seluluk', 'Kecamatan', ''),
  (3415, 14, 405, 5594, 'Danau Sembuluh', 'Kecamatan', ''),
  (3416, 14, 405, 5595, 'Hanau', 'Kecamatan', ''),
  (3417, 14, 405, 5596, 'Seruyan Hilir', 'Kecamatan', ''),
  (3418, 14, 405, 5597, 'Seruyan Hilir Timur', 'Kecamatan', ''),
  (3419, 14, 405, 5598, 'Seruyan Hulu', 'Kecamatan', ''),
  (3420, 14, 405, 5599, 'Seruyan Raya', 'Kecamatan', ''),
  (3421, 14, 405, 5600, 'Seruyan Tengah', 'Kecamatan', ''),
  (3422, 14, 405, 5601, 'Suling Tambun', 'Kecamatan', ''),
  (3423, 14, 432, 5972, 'Balai Riam', 'Kecamatan', ''),
  (3424, 14, 432, 5973, 'Jelai', 'Kecamatan', ''),
  (3425, 14, 432, 5974, 'Pantai Lunci', 'Kecamatan', ''),
  (3426, 14, 432, 5975, 'Permata Kecubung', 'Kecamatan', ''),
  (3427, 14, 432, 5976, 'Sukamara', 'Kecamatan', ''),
  (3428, 15, 19, 0, 'Balikpapan', 'Kota', '76111'),
  (3429, 15, 66, 0, 'Berau', 'Kabupaten', '77311'),
  (3430, 15, 89, 0, 'Bontang', 'Kota', '75313'),
  (3431, 15, 214, 0, 'Kutai Barat', 'Kabupaten', '75711'),
  (3432, 15, 215, 0, 'Kutai Kartanegara', 'Kabupaten', '75511'),
  (3433, 15, 216, 0, 'Kutai Timur', 'Kabupaten', '75611'),
  (3434, 15, 341, 0, 'Paser', 'Kabupaten', '76211'),
  (3435, 15, 354, 0, 'Penajam Paser Utara', 'Kabupaten', '76311'),
  (3436, 15, 387, 0, 'Samarinda', 'Kota', '75133'),
  (3437, 15, 19, 272, 'Balikpapan Barat', 'Kecamatan', ''),
  (3438, 15, 19, 273, 'Balikpapan Kota', 'Kecamatan', ''),
  (3439, 15, 19, 274, 'Balikpapan Selatan', 'Kecamatan', ''),
  (3440, 15, 19, 275, 'Balikpapan Tengah', 'Kecamatan', ''),
  (3441, 15, 19, 276, 'Balikpapan Timur', 'Kecamatan', ''),
  (3442, 15, 19, 277, 'Balikpapan Utara', 'Kecamatan', ''),
  (3443, 15, 66, 879, 'Batu Putih', 'Kecamatan', ''),
  (3444, 15, 66, 880, 'Biatan', 'Kecamatan', ''),
  (3445, 15, 66, 881, 'Biduk-Biduk', 'Kecamatan', ''),
  (3446, 15, 66, 882, 'Derawan (Pulau Derawan)', 'Kecamatan', ''),
  (3447, 15, 66, 883, 'Gunung Tabur', 'Kecamatan', ''),
  (3448, 15, 66, 884, 'Kelay', 'Kecamatan', ''),
  (3449, 15, 66, 885, 'Maratua', 'Kecamatan', ''),
  (3450, 15, 66, 886, 'Sambaliung', 'Kecamatan', ''),
  (3451, 15, 66, 887, 'Segah', 'Kecamatan', ''),
  (3452, 15, 66, 888, 'Tabalar', 'Kecamatan', ''),
  (3453, 15, 66, 889, 'Talisayan', 'Kecamatan', ''),
  (3454, 15, 66, 890, 'Tanjung Redeb', 'Kecamatan', ''),
  (3455, 15, 66, 891, 'Teluk Bayur', 'Kecamatan', ''),
  (3456, 15, 89, 1217, 'Bontang Barat', 'Kecamatan', ''),
  (3457, 15, 89, 1218, 'Bontang Selatan', 'Kecamatan', ''),
  (3458, 15, 89, 1219, 'Bontang Utara', 'Kecamatan', ''),
  (3459, 15, 214, 3004, 'Barong Tongkok', 'Kecamatan', ''),
  (3460, 15, 214, 3005, 'Bentian Besar', 'Kecamatan', ''),
  (3461, 15, 214, 3006, 'Bongan', 'Kecamatan', ''),
  (3462, 15, 214, 3007, 'Damai', 'Kecamatan', ''),
  (3463, 15, 214, 3008, 'Jempang', 'Kecamatan', ''),
  (3464, 15, 214, 3009, 'Laham', 'Kecamatan', ''),
  (3465, 15, 214, 3010, 'Linggang Bigung', 'Kecamatan', ''),
  (3466, 15, 214, 3011, 'Long Apari', 'Kecamatan', ''),
  (3467, 15, 214, 3012, 'Long Bagun', 'Kecamatan', ''),
  (3468, 15, 214, 3013, 'Long Hubung', 'Kecamatan', ''),
  (3469, 15, 214, 3014, 'Long Iram', 'Kecamatan', ''),
  (3470, 15, 214, 3015, 'Long Pahangai', 'Kecamatan', ''),
  (3471, 15, 214, 3016, 'Manor Bulatin (Mook Manaar Bulatn)', 'Kecamatan', ''),
  (3472, 15, 214, 3017, 'Melak', 'Kecamatan', ''),
  (3473, 15, 214, 3018, 'Muara Lawa', 'Kecamatan', ''),
  (3474, 15, 214, 3019, 'Muara Pahu', 'Kecamatan', ''),
  (3475, 15, 214, 3020, 'Nyuatan', 'Kecamatan', ''),
  (3476, 15, 214, 3021, 'Penyinggahan', 'Kecamatan', ''),
  (3477, 15, 214, 3022, 'Sekolaq Darat', 'Kecamatan', ''),
  (3478, 15, 214, 3023, 'Siluq Ngurai', 'Kecamatan', ''),
  (3479, 15, 214, 3024, 'Tering', 'Kecamatan', ''),
  (3480, 15, 215, 3025, 'Anggana', 'Kecamatan', ''),
  (3481, 15, 215, 3026, 'Kembang Janggut', 'Kecamatan', ''),
  (3482, 15, 215, 3027, 'Kenohan', 'Kecamatan', ''),
  (3483, 15, 215, 3028, 'Kota Bangun', 'Kecamatan', ''),
  (3484, 15, 215, 3029, 'Loa Janan', 'Kecamatan', ''),
  (3485, 15, 215, 3030, 'Loa Kulu', 'Kecamatan', ''),
  (3486, 15, 215, 3031, 'Marang Kayu', 'Kecamatan', ''),
  (3487, 15, 215, 3032, 'Muara Badak', 'Kecamatan', ''),
  (3488, 15, 215, 3033, 'Muara Jawa', 'Kecamatan', ''),
  (3489, 15, 215, 3034, 'Muara Kaman', 'Kecamatan', ''),
  (3490, 15, 215, 3035, 'Muara Muntai', 'Kecamatan', ''),
  (3491, 15, 215, 3036, 'Muara Wis', 'Kecamatan', ''),
  (3492, 15, 215, 3037, 'Samboja (Semboja)', 'Kecamatan', ''),
  (3493, 15, 215, 3038, 'Sanga-Sanga', 'Kecamatan', ''),
  (3494, 15, 215, 3039, 'Sebulu', 'Kecamatan', ''),
  (3495, 15, 215, 3040, 'Tabang', 'Kecamatan', ''),
  (3496, 15, 215, 3041, 'Tenggarong', 'Kecamatan', ''),
  (3497, 15, 215, 3042, 'Tenggarong Seberang', 'Kecamatan', ''),
  (3498, 15, 216, 3043, 'Batu Ampar', 'Kecamatan', ''),
  (3499, 15, 216, 3044, 'Bengalon', 'Kecamatan', ''),
  (3500, 15, 216, 3045, 'Busang', 'Kecamatan', ''),
  (3501, 15, 216, 3046, 'Kaliorang', 'Kecamatan', ''),
  (3502, 15, 216, 3047, 'Karangan', 'Kecamatan', ''),
  (3503, 15, 216, 3048, 'Kaubun', 'Kecamatan', ''),
  (3504, 15, 216, 3049, 'Kongbeng', 'Kecamatan', ''),
  (3505, 15, 216, 3050, 'Long Mesangat (Mesengat)', 'Kecamatan', ''),
  (3506, 15, 216, 3051, 'Muara Ancalong', 'Kecamatan', ''),
  (3507, 15, 216, 3052, 'Muara Bengkal', 'Kecamatan', ''),
  (3508, 15, 216, 3053, 'Muara Wahau', 'Kecamatan', ''),
  (3509, 15, 216, 3054, 'Rantau Pulung', 'Kecamatan', ''),
  (3510, 15, 216, 3055, 'Sandaran', 'Kecamatan', ''),
  (3511, 15, 216, 3056, 'Sangatta Selatan', 'Kecamatan', ''),
  (3512, 15, 216, 3057, 'Sangatta Utara', 'Kecamatan', ''),
  (3513, 15, 216, 3058, 'Sangkulirang', 'Kecamatan', ''),
  (3514, 15, 216, 3059, 'Telen', 'Kecamatan', ''),
  (3515, 15, 216, 3060, 'Teluk Pandan', 'Kecamatan', ''),
  (3516, 15, 341, 4783, 'Batu Engau', 'Kecamatan', ''),
  (3517, 15, 341, 4784, 'Batu Sopang', 'Kecamatan', ''),
  (3518, 15, 341, 4785, 'Kuaro', 'Kecamatan', ''),
  (3519, 15, 341, 4786, 'Long Ikis', 'Kecamatan', ''),
  (3520, 15, 341, 4787, 'Long Kali', 'Kecamatan', ''),
  (3521, 15, 341, 4788, 'Muara Komam', 'Kecamatan', ''),
  (3522, 15, 341, 4789, 'Muara Samu', 'Kecamatan', ''),
  (3523, 15, 341, 4790, 'Pasir Belengkong', 'Kecamatan', ''),
  (3524, 15, 341, 4791, 'Tanah Grogot', 'Kecamatan', ''),
  (3525, 15, 341, 4792, 'Tanjung Harapan', 'Kecamatan', ''),
  (3526, 15, 354, 4960, 'Babulu', 'Kecamatan', ''),
  (3527, 15, 354, 4961, 'Penajam', 'Kecamatan', ''),
  (3528, 15, 354, 4962, 'Sepaku', 'Kecamatan', ''),
  (3529, 15, 354, 4963, 'Waru', 'Kecamatan', ''),
  (3530, 15, 387, 5356, 'Loa Janan Ilir', 'Kecamatan', ''),
  (3531, 15, 387, 5357, 'Palaran', 'Kecamatan', ''),
  (3532, 15, 387, 5358, 'Samarinda Ilir', 'Kecamatan', ''),
  (3533, 15, 387, 5359, 'Samarinda Kota', 'Kecamatan', ''),
  (3534, 15, 387, 5360, 'Samarinda Seberang', 'Kecamatan', ''),
  (3535, 15, 387, 5361, 'Samarinda Ulu', 'Kecamatan', ''),
  (3536, 15, 387, 5362, 'Samarinda Utara', 'Kecamatan', ''),
  (3537, 15, 387, 5363, 'Sambutan', 'Kecamatan', ''),
  (3538, 15, 387, 5364, 'Sungai Kunjang', 'Kecamatan', ''),
  (3539, 15, 387, 5365, 'Sungai Pinang', 'Kecamatan', ''),
  (3540, 16, 96, 0, 'Bulungan (Bulongan)', 'Kabupaten', '77211'),
  (3541, 16, 257, 0, 'Malinau', 'Kabupaten', '77511'),
  (3542, 16, 311, 0, 'Nunukan', 'Kabupaten', '77421'),
  (3543, 16, 450, 0, 'Tana Tidung', 'Kabupaten', '77611'),
  (3544, 16, 467, 0, 'Tarakan', 'Kota', '77114'),
  (3545, 16, 96, 1298, 'Peso', 'Kecamatan', ''),
  (3546, 16, 96, 1299, 'Peso Hilir/Ilir', 'Kecamatan', ''),
  (3547, 16, 96, 1300, 'Pulau Bunyu', 'Kecamatan', ''),
  (3548, 16, 96, 1301, 'Sekatak', 'Kecamatan', ''),
  (3549, 16, 96, 1302, 'Tanjung Palas', 'Kecamatan', ''),
  (3550, 16, 96, 1303, 'Tanjung Palas Barat', 'Kecamatan', ''),
  (3551, 16, 96, 1304, 'Tanjung Palas Tengah', 'Kecamatan', ''),
  (3552, 16, 96, 1305, 'Tanjung Palas Timur', 'Kecamatan', ''),
  (3553, 16, 96, 1306, 'Tanjung Palas Utara', 'Kecamatan', ''),
  (3554, 16, 96, 1307, 'Tanjung Selor', 'Kecamatan', ''),
  (3555, 16, 257, 3639, 'Bahau Hulu', 'Kecamatan', ''),
  (3556, 16, 257, 3640, 'Kayan Hilir', 'Kecamatan', ''),
  (3557, 16, 257, 3641, 'Kayan Hulu', 'Kecamatan', ''),
  (3558, 16, 257, 3642, 'Kayan Selatan', 'Kecamatan', ''),
  (3559, 16, 257, 3643, 'Malinau Barat', 'Kecamatan', ''),
  (3560, 16, 257, 3644, 'Malinau Kota', 'Kecamatan', ''),
  (3561, 16, 257, 3645, 'Malinau Selatan', 'Kecamatan', ''),
  (3562, 16, 257, 3646, 'Malinau Selatan Hilir', 'Kecamatan', ''),
  (3563, 16, 257, 3647, 'Malinau Selatan Hulu', 'Kecamatan', ''),
  (3564, 16, 257, 3648, 'Malinau Utara', 'Kecamatan', ''),
  (3565, 16, 257, 3649, 'Mentarang', 'Kecamatan', ''),
  (3566, 16, 257, 3650, 'Mentarang Hulu', 'Kecamatan', ''),
  (3567, 16, 257, 3651, 'Pujungan', 'Kecamatan', ''),
  (3568, 16, 257, 3652, 'Sungai Boh', 'Kecamatan', ''),
  (3569, 16, 257, 3653, 'Sungai Tubu', 'Kecamatan', ''),
  (3570, 16, 311, 4422, 'Krayan', 'Kecamatan', ''),
  (3571, 16, 311, 4423, 'Krayan Selatan', 'Kecamatan', ''),
  (3572, 16, 311, 4424, 'Lumbis', 'Kecamatan', ''),
  (3573, 16, 311, 4425, 'Lumbis Ogong', 'Kecamatan', ''),
  (3574, 16, 311, 4426, 'Nunukan', 'Kecamatan', ''),
  (3575, 16, 311, 4427, 'Nunukan Selatan', 'Kecamatan', ''),
  (3576, 16, 311, 4428, 'Sebatik', 'Kecamatan', ''),
  (3577, 16, 311, 4429, 'Sebatik Barat', 'Kecamatan', ''),
  (3578, 16, 311, 4430, 'Sebatik Tengah', 'Kecamatan', ''),
  (3579, 16, 311, 4431, 'Sebatik Timur', 'Kecamatan', ''),
  (3580, 16, 311, 4432, 'Sebatik Utara', 'Kecamatan', ''),
  (3581, 16, 311, 4433, 'Sebuku', 'Kecamatan', ''),
  (3582, 16, 311, 4434, 'Sei Menggaris', 'Kecamatan', ''),
  (3583, 16, 311, 4435, 'Sembakung', 'Kecamatan', ''),
  (3584, 16, 311, 4436, 'Tulin Onsoi', 'Kecamatan', ''),
  (3585, 16, 450, 6210, 'Betayau', 'Kecamatan', ''),
  (3586, 16, 450, 6211, 'Sesayap', 'Kecamatan', ''),
  (3587, 16, 450, 6212, 'Sesayap Hilir', 'Kecamatan', ''),
  (3588, 16, 450, 6213, 'Tana Lia (Tanah Lia)', 'Kecamatan', ''),
  (3589, 16, 467, 6432, 'Tarakan Barat', 'Kecamatan', ''),
  (3590, 16, 467, 6433, 'Tarakan Tengah', 'Kecamatan', ''),
  (3591, 16, 467, 6434, 'Tarakan Timur', 'Kecamatan', ''),
  (3592, 16, 467, 6435, 'Tarakan Utara', 'Kecamatan', ''),
  (3593, 17, 48, 0, 'Batam', 'Kota', '29413'),
  (3594, 17, 71, 0, 'Bintan', 'Kabupaten', '29135'),
  (3595, 17, 172, 0, 'Karimun', 'Kabupaten', '29611'),
  (3596, 17, 184, 0, 'Kepulauan Anambas', 'Kabupaten', '29991'),
  (3597, 17, 237, 0, 'Lingga', 'Kabupaten', '29811'),
  (3598, 17, 302, 0, 'Natuna', 'Kabupaten', '29711'),
  (3599, 17, 462, 0, 'Tanjung Pinang', 'Kota', '29111'),
  (3600, 17, 48, 673, 'Batam Kota', 'Kecamatan', ''),
  (3601, 17, 48, 674, 'Batu Aji', 'Kecamatan', ''),
  (3602, 17, 48, 675, 'Batu Ampar', 'Kecamatan', ''),
  (3603, 17, 48, 676, 'Belakang Padang', 'Kecamatan', ''),
  (3604, 17, 48, 677, 'Bengkong', 'Kecamatan', ''),
  (3605, 17, 48, 678, 'Bulang', 'Kecamatan', ''),
  (3606, 17, 48, 679, 'Galang', 'Kecamatan', ''),
  (3607, 17, 48, 680, 'Lubuk Baja', 'Kecamatan', ''),
  (3608, 17, 48, 681, 'Nongsa', 'Kecamatan', ''),
  (3609, 17, 48, 682, 'Sagulung', 'Kecamatan', ''),
  (3610, 17, 48, 683, 'Sei/Sungai Beduk', 'Kecamatan', ''),
  (3611, 17, 48, 684, 'Sekupang', 'Kecamatan', ''),
  (3612, 17, 71, 939, 'Bintan Pesisir', 'Kecamatan', ''),
  (3613, 17, 71, 940, 'Bintan Timur', 'Kecamatan', ''),
  (3614, 17, 71, 941, 'Bintan Utara', 'Kecamatan', ''),
  (3615, 17, 71, 942, 'Gunung Kijang', 'Kecamatan', ''),
  (3616, 17, 71, 943, 'Mantang', 'Kecamatan', ''),
  (3617, 17, 71, 944, 'Seri/Sri Kuala Lobam', 'Kecamatan', ''),
  (3618, 17, 71, 945, 'Tambelan', 'Kecamatan', ''),
  (3619, 17, 71, 946, 'Teluk Bintan', 'Kecamatan', ''),
  (3620, 17, 71, 947, 'Teluk Sebong', 'Kecamatan', ''),
  (3621, 17, 71, 948, 'Toapaya', 'Kecamatan', ''),
  (3622, 17, 172, 2408, 'Belat', 'Kecamatan', ''),
  (3623, 17, 172, 2409, 'Buru', 'Kecamatan', ''),
  (3624, 17, 172, 2410, 'Durai', 'Kecamatan', ''),
  (3625, 17, 172, 2411, 'Karimun', 'Kecamatan', ''),
  (3626, 17, 172, 2412, 'Kundur', 'Kecamatan', ''),
  (3627, 17, 172, 2413, 'Kundur Barat', 'Kecamatan', ''),
  (3628, 17, 172, 2414, 'Kundur Utara', 'Kecamatan', ''),
  (3629, 17, 172, 2415, 'Meral', 'Kecamatan', ''),
  (3630, 17, 172, 2416, 'Meral Barat', 'Kecamatan', ''),
  (3631, 17, 172, 2417, 'Moro', 'Kecamatan', ''),
  (3632, 17, 172, 2418, 'Tebing', 'Kecamatan', ''),
  (3633, 17, 172, 2419, 'Ungar', 'Kecamatan', ''),
  (3634, 17, 184, 2571, 'Jemaja', 'Kecamatan', ''),
  (3635, 17, 184, 2572, 'Jemaja Timur', 'Kecamatan', ''),
  (3636, 17, 184, 2573, 'Palmatak', 'Kecamatan', ''),
  (3637, 17, 184, 2574, 'Siantan', 'Kecamatan', ''),
  (3638, 17, 184, 2575, 'Siantan Selatan', 'Kecamatan', ''),
  (3639, 17, 184, 2576, 'Siantan Tengah', 'Kecamatan', ''),
  (3640, 17, 184, 2577, 'Siantan Timur', 'Kecamatan', ''),
  (3641, 17, 237, 3364, 'Lingga', 'Kecamatan', ''),
  (3642, 17, 237, 3365, 'Lingga Timur', 'Kecamatan', ''),
  (3643, 17, 237, 3366, 'Lingga Utara', 'Kecamatan', ''),
  (3644, 17, 237, 3367, 'Selayar', 'Kecamatan', ''),
  (3645, 17, 237, 3368, 'Senayang', 'Kecamatan', ''),
  (3646, 17, 237, 3369, 'Singkep', 'Kecamatan', ''),
  (3647, 17, 237, 3370, 'Singkep Barat', 'Kecamatan', ''),
  (3648, 17, 237, 3371, 'Singkep Pesisir', 'Kecamatan', ''),
  (3649, 17, 302, 4267, 'Bunguran Barat', 'Kecamatan', ''),
  (3650, 17, 302, 4268, 'Bunguran Selatan', 'Kecamatan', ''),
  (3651, 17, 302, 4269, 'Bunguran Tengah', 'Kecamatan', ''),
  (3652, 17, 302, 4270, 'Bunguran Timur', 'Kecamatan', ''),
  (3653, 17, 302, 4271, 'Bunguran Timur Laut', 'Kecamatan', ''),
  (3654, 17, 302, 4272, 'Bunguran Utara', 'Kecamatan', ''),
  (3655, 17, 302, 4273, 'Midai', 'Kecamatan', ''),
  (3656, 17, 302, 4274, 'Pulau Laut', 'Kecamatan', ''),
  (3657, 17, 302, 4275, 'Pulau Tiga', 'Kecamatan', ''),
  (3658, 17, 302, 4276, 'Serasan', 'Kecamatan', ''),
  (3659, 17, 302, 4277, 'Serasan Timur', 'Kecamatan', ''),
  (3660, 17, 302, 4278, 'Subi', 'Kecamatan', ''),
  (3661, 17, 462, 6367, 'Bukit Bestari', 'Kecamatan', ''),
  (3662, 17, 462, 6368, 'Tanjung Pinang Barat', 'Kecamatan', ''),
  (3663, 17, 462, 6369, 'Tanjung Pinang Kota', 'Kecamatan', ''),
  (3664, 17, 462, 6370, 'Tanjung Pinang Timur', 'Kecamatan', ''),
  (3665, 18, 21, 0, 'Bandar Lampung', 'Kota', '35139'),
  (3666, 18, 223, 0, 'Lampung Barat', 'Kabupaten', '34814'),
  (3667, 18, 224, 0, 'Lampung Selatan', 'Kabupaten', '35511'),
  (3668, 18, 225, 0, 'Lampung Tengah', 'Kabupaten', '34212'),
  (3669, 18, 226, 0, 'Lampung Timur', 'Kabupaten', '34319'),
  (3670, 18, 227, 0, 'Lampung Utara', 'Kabupaten', '34516'),
  (3671, 18, 282, 0, 'Mesuji', 'Kabupaten', '34911'),
  (3672, 18, 283, 0, 'Metro', 'Kota', '34111'),
  (3673, 18, 355, 0, 'Pesawaran', 'Kabupaten', '35312'),
  (3674, 18, 356, 0, 'Pesisir Barat', 'Kabupaten', '35974'),
  (3675, 18, 368, 0, 'Pringsewu', 'Kabupaten', '35719'),
  (3676, 18, 458, 0, 'Tanggamus', 'Kabupaten', '35619'),
  (3677, 18, 490, 0, 'Tulang Bawang', 'Kabupaten', '34613'),
  (3678, 18, 491, 0, 'Tulang Bawang Barat', 'Kabupaten', '34419'),
  (3679, 18, 496, 0, 'Way Kanan', 'Kabupaten', '34711'),
  (3680, 18, 21, 287, 'Bumi Waras', 'Kecamatan', ''),
  (3681, 18, 21, 288, 'Enggal', 'Kecamatan', ''),
  (3682, 18, 21, 289, 'Kedamaian', 'Kecamatan', ''),
  (3683, 18, 21, 290, 'Kedaton', 'Kecamatan', ''),
  (3684, 18, 21, 291, 'Kemiling', 'Kecamatan', ''),
  (3685, 18, 21, 292, 'Labuhan Ratu', 'Kecamatan', ''),
  (3686, 18, 21, 293, 'Langkapura', 'Kecamatan', ''),
  (3687, 18, 21, 294, 'Panjang', 'Kecamatan', ''),
  (3688, 18, 21, 295, 'Rajabasa', 'Kecamatan', ''),
  (3689, 18, 21, 296, 'Sukabumi', 'Kecamatan', ''),
  (3690, 18, 21, 297, 'Sukarame', 'Kecamatan', ''),
  (3691, 18, 21, 298, 'Tanjung Karang Barat', 'Kecamatan', ''),
  (3692, 18, 21, 299, 'Tanjung Karang Pusat', 'Kecamatan', ''),
  (3693, 18, 21, 300, 'Tanjung Karang Timur', 'Kecamatan', ''),
  (3694, 18, 21, 301, 'Tanjung Senang', 'Kecamatan', ''),
  (3695, 18, 21, 302, 'Telukbetung Barat', 'Kecamatan', ''),
  (3696, 18, 21, 303, 'Telukbetung Selatan', 'Kecamatan', ''),
  (3697, 18, 21, 304, 'Telukbetung Timur', 'Kecamatan', ''),
  (3698, 18, 21, 305, 'Telukbetung Utara', 'Kecamatan', ''),
  (3699, 18, 21, 306, 'Way Halim', 'Kecamatan', ''),
  (3700, 18, 223, 3140, 'Air Hitam', 'Kecamatan', ''),
  (3701, 18, 223, 3141, 'Balik Bukit', 'Kecamatan', ''),
  (3702, 18, 223, 3142, 'Bandar Negeri Suoh', 'Kecamatan', ''),
  (3703, 18, 223, 3143, 'Batu Brak', 'Kecamatan', ''),
  (3704, 18, 223, 3144, 'Batu Ketulis', 'Kecamatan', ''),
  (3705, 18, 223, 3145, 'Belalau', 'Kecamatan', ''),
  (3706, 18, 223, 3146, 'Gedung Surian', 'Kecamatan', ''),
  (3707, 18, 223, 3147, 'Kebun Tebu', 'Kecamatan', ''),
  (3708, 18, 223, 3148, 'Lumbok Seminung', 'Kecamatan', ''),
  (3709, 18, 223, 3149, 'Pagar Dewa', 'Kecamatan', ''),
  (3710, 18, 223, 3150, 'Sekincau', 'Kecamatan', ''),
  (3711, 18, 223, 3151, 'Sukau', 'Kecamatan', ''),
  (3712, 18, 223, 3152, 'Sumber Jaya', 'Kecamatan', ''),
  (3713, 18, 223, 3153, 'Suoh', 'Kecamatan', ''),
  (3714, 18, 223, 3154, 'Way Tenong', 'Kecamatan', ''),
  (3715, 18, 224, 3155, 'Bakauheni', 'Kecamatan', ''),
  (3716, 18, 224, 3156, 'Candipuro', 'Kecamatan', ''),
  (3717, 18, 224, 3157, 'Jati Agung', 'Kecamatan', ''),
  (3718, 18, 224, 3158, 'Kalianda', 'Kecamatan', ''),
  (3719, 18, 224, 3159, 'Katibung', 'Kecamatan', ''),
  (3720, 18, 224, 3160, 'Ketapang', 'Kecamatan', ''),
  (3721, 18, 224, 3161, 'Merbau Mataram', 'Kecamatan', ''),
  (3722, 18, 224, 3162, 'Natar', 'Kecamatan', ''),
  (3723, 18, 224, 3163, 'Palas', 'Kecamatan', ''),
  (3724, 18, 224, 3164, 'Penengahan', 'Kecamatan', ''),
  (3725, 18, 224, 3165, 'Rajabasa', 'Kecamatan', ''),
  (3726, 18, 224, 3166, 'Sidomulyo', 'Kecamatan', ''),
  (3727, 18, 224, 3167, 'Sragi', 'Kecamatan', ''),
  (3728, 18, 224, 3168, 'Tanjung Bintang', 'Kecamatan', ''),
  (3729, 18, 224, 3169, 'Tanjung Sari', 'Kecamatan', ''),
  (3730, 18, 224, 3170, 'Way Panji', 'Kecamatan', ''),
  (3731, 18, 224, 3171, 'Way Sulan', 'Kecamatan', ''),
  (3732, 18, 225, 3172, 'Anak Ratu Aji', 'Kecamatan', ''),
  (3733, 18, 225, 3173, 'Anak Tuha', 'Kecamatan', ''),
  (3734, 18, 225, 3174, 'Bandar Mataram', 'Kecamatan', ''),
  (3735, 18, 225, 3175, 'Bandar Surabaya', 'Kecamatan', ''),
  (3736, 18, 225, 3176, 'Bangunrejo', 'Kecamatan', ''),
  (3737, 18, 225, 3177, 'Bekri', 'Kecamatan', ''),
  (3738, 18, 225, 3178, 'Bumi Nabung', 'Kecamatan', ''),
  (3739, 18, 225, 3179, 'Bumi Ratu Nuban', 'Kecamatan', ''),
  (3740, 18, 225, 3180, 'Gunung Sugih', 'Kecamatan', ''),
  (3741, 18, 225, 3181, 'Kalirejo', 'Kecamatan', ''),
  (3742, 18, 225, 3182, 'Kota Gajah', 'Kecamatan', ''),
  (3743, 18, 225, 3183, 'Padang Ratu', 'Kecamatan', ''),
  (3744, 18, 225, 3184, 'Pubian', 'Kecamatan', ''),
  (3745, 18, 225, 3185, 'Punggur', 'Kecamatan', ''),
  (3746, 18, 225, 3186, 'Putra Rumbia', 'Kecamatan', ''),
  (3747, 18, 225, 3187, 'Rumbia', 'Kecamatan', ''),
  (3748, 18, 225, 3188, 'Selagai Lingga', 'Kecamatan', ''),
  (3749, 18, 225, 3189, 'Sendang Agung', 'Kecamatan', ''),
  (3750, 18, 225, 3190, 'Seputih Agung', 'Kecamatan', ''),
  (3751, 18, 225, 3191, 'Seputih Banyak', 'Kecamatan', ''),
  (3752, 18, 225, 3192, 'Seputih Mataram', 'Kecamatan', ''),
  (3753, 18, 225, 3193, 'Seputih Raman', 'Kecamatan', ''),
  (3754, 18, 225, 3194, 'Seputih Surabaya', 'Kecamatan', ''),
  (3755, 18, 225, 3195, 'Terbanggi Besar', 'Kecamatan', ''),
  (3756, 18, 225, 3196, 'Terusan Nunyai', 'Kecamatan', ''),
  (3757, 18, 225, 3197, 'Trimurjo', 'Kecamatan', ''),
  (3758, 18, 225, 3198, 'Way Pangubuan (Pengubuan)', 'Kecamatan', ''),
  (3759, 18, 225, 3199, 'Way Seputih', 'Kecamatan', ''),
  (3760, 18, 226, 3200, 'Bandar Sribawono', 'Kecamatan', ''),
  (3761, 18, 226, 3201, 'Batanghari', 'Kecamatan', ''),
  (3762, 18, 226, 3202, 'Batanghari Nuban', 'Kecamatan', ''),
  (3763, 18, 226, 3203, 'Braja Slebah', 'Kecamatan', ''),
  (3764, 18, 226, 3204, 'Bumi Agung', 'Kecamatan', ''),
  (3765, 18, 226, 3205, 'Gunung Pelindung', 'Kecamatan', ''),
  (3766, 18, 226, 3206, 'Jabung', 'Kecamatan', ''),
  (3767, 18, 226, 3207, 'Labuhan Maringgai', 'Kecamatan', ''),
  (3768, 18, 226, 3208, 'Labuhan Ratu', 'Kecamatan', ''),
  (3769, 18, 226, 3209, 'Marga Sekampung', 'Kecamatan', ''),
  (3770, 18, 226, 3210, 'Margatiga', 'Kecamatan', ''),
  (3771, 18, 226, 3211, 'Mataram Baru', 'Kecamatan', ''),
  (3772, 18, 226, 3212, 'Melinting', 'Kecamatan', ''),
  (3773, 18, 226, 3213, 'Metro Kibang', 'Kecamatan', ''),
  (3774, 18, 226, 3214, 'Pasir Sakti', 'Kecamatan', ''),
  (3775, 18, 226, 3215, 'Pekalongan', 'Kecamatan', ''),
  (3776, 18, 226, 3216, 'Purbolinggo', 'Kecamatan', ''),
  (3777, 18, 226, 3217, 'Raman Utara', 'Kecamatan', ''),
  (3778, 18, 226, 3218, 'Sekampung', 'Kecamatan', ''),
  (3779, 18, 226, 3219, 'Sekampung Udik', 'Kecamatan', ''),
  (3780, 18, 226, 3220, 'Sukadana', 'Kecamatan', ''),
  (3781, 18, 226, 3221, 'Waway Karya', 'Kecamatan', ''),
  (3782, 18, 226, 3222, 'Way Bungur (Purbolinggo Utara)', 'Kecamatan', ''),
  (3783, 18, 226, 3223, 'Way Jepara', 'Kecamatan', ''),
  (3784, 18, 227, 3224, 'Abung Barat', 'Kecamatan', ''),
  (3785, 18, 227, 3225, 'Abung Kunang', 'Kecamatan', ''),
  (3786, 18, 227, 3226, 'Abung Pekurun', 'Kecamatan', ''),
  (3787, 18, 227, 3227, 'Abung Selatan', 'Kecamatan', ''),
  (3788, 18, 227, 3228, 'Abung Semuli', 'Kecamatan', ''),
  (3789, 18, 227, 3229, 'Abung Surakarta', 'Kecamatan', ''),
  (3790, 18, 227, 3230, 'Abung Tengah', 'Kecamatan', ''),
  (3791, 18, 227, 3231, 'Abung Timur', 'Kecamatan', ''),
  (3792, 18, 227, 3232, 'Abung Tinggi', 'Kecamatan', ''),
  (3793, 18, 227, 3233, 'Blambangan Pagar', 'Kecamatan', ''),
  (3794, 18, 227, 3234, 'Bukit Kemuning', 'Kecamatan', ''),
  (3795, 18, 227, 3235, 'Bunga Mayang', 'Kecamatan', ''),
  (3796, 18, 227, 3236, 'Hulu Sungkai', 'Kecamatan', ''),
  (3797, 18, 227, 3237, 'Kotabumi', 'Kecamatan', ''),
  (3798, 18, 227, 3238, 'Kotabumi Selatan', 'Kecamatan', ''),
  (3799, 18, 227, 3239, 'Kotabumi Utara', 'Kecamatan', ''),
  (3800, 18, 227, 3240, 'Muara Sungkai', 'Kecamatan', ''),
  (3801, 18, 227, 3241, 'Sungkai Barat', 'Kecamatan', ''),
  (3802, 18, 227, 3242, 'Sungkai Jaya', 'Kecamatan', ''),
  (3803, 18, 227, 3243, 'Sungkai Selatan', 'Kecamatan', ''),
  (3804, 18, 227, 3244, 'Sungkai Tengah', 'Kecamatan', ''),
  (3805, 18, 227, 3245, 'Sungkai Utara', 'Kecamatan', ''),
  (3806, 18, 227, 3246, 'Tanjung Raja', 'Kecamatan', ''),
  (3807, 18, 282, 3982, 'Mesuji', 'Kecamatan', ''),
  (3808, 18, 282, 3983, 'Mesuji Timur', 'Kecamatan', ''),
  (3809, 18, 282, 3984, 'Panca Jaya', 'Kecamatan', ''),
  (3810, 18, 282, 3985, 'Rawa Jitu Utara', 'Kecamatan', ''),
  (3811, 18, 282, 3986, 'Simpang Pematang', 'Kecamatan', ''),
  (3812, 18, 282, 3987, 'Tanjung Raya', 'Kecamatan', ''),
  (3813, 18, 282, 3988, 'Way Serdang', 'Kecamatan', ''),
  (3814, 18, 283, 3989, 'Metro Barat', 'Kecamatan', ''),
  (3815, 18, 283, 3990, 'Metro Pusat', 'Kecamatan', ''),
  (3816, 18, 283, 3991, 'Metro Selatan', 'Kecamatan', ''),
  (3817, 18, 283, 3992, 'Metro Timur', 'Kecamatan', ''),
  (3818, 18, 283, 3993, 'Metro Utara', 'Kecamatan', ''),
  (3819, 18, 355, 4964, 'Gedong Tataan (Gedung Tataan)', 'Kecamatan', ''),
  (3820, 18, 355, 4965, 'Kedondong', 'Kecamatan', ''),
  (3821, 18, 355, 4966, 'Marga Punduh', 'Kecamatan', ''),
  (3822, 18, 355, 4967, 'Negeri Katon', 'Kecamatan', ''),
  (3823, 18, 355, 4968, 'Padang Cermin', 'Kecamatan', ''),
  (3824, 18, 355, 4969, 'Punduh Pidada (Pedada)', 'Kecamatan', ''),
  (3825, 18, 355, 4970, 'Tegineneng', 'Kecamatan', ''),
  (3826, 18, 355, 4971, 'Way Khilau', 'Kecamatan', ''),
  (3827, 18, 355, 4972, 'Way Lima', 'Kecamatan', ''),
  (3828, 18, 356, 4973, 'Bengkunat', 'Kecamatan', ''),
  (3829, 18, 356, 4974, 'Bengkunat Belimbing', 'Kecamatan', ''),
  (3830, 18, 356, 4975, 'Karya Penggawa', 'Kecamatan', ''),
  (3831, 18, 356, 4976, 'Krui Selatan', 'Kecamatan', ''),
  (3832, 18, 356, 4977, 'Lemong', 'Kecamatan', ''),
  (3833, 18, 356, 4978, 'Ngambur', 'Kecamatan', ''),
  (3834, 18, 356, 4979, 'Pesisir Selatan', 'Kecamatan', ''),
  (3835, 18, 356, 4980, 'Pesisir Tengah', 'Kecamatan', ''),
  (3836, 18, 356, 4981, 'Pesisir Utara', 'Kecamatan', ''),
  (3837, 18, 356, 4982, 'Pulau Pisang', 'Kecamatan', ''),
  (3838, 18, 356, 4983, 'Way Krui', 'Kecamatan', ''),
  (3839, 18, 368, 5132, 'Adi Luwih', 'Kecamatan', ''),
  (3840, 18, 368, 5133, 'Ambarawa', 'Kecamatan', ''),
  (3841, 18, 368, 5134, 'Banyumas', 'Kecamatan', ''),
  (3842, 18, 368, 5135, 'Gading Rejo', 'Kecamatan', ''),
  (3843, 18, 368, 5136, 'Pagelaran', 'Kecamatan', ''),
  (3844, 18, 368, 5137, 'Pagelaran Utara', 'Kecamatan', ''),
  (3845, 18, 368, 5138, 'Pardasuka', 'Kecamatan', ''),
  (3846, 18, 368, 5139, 'Pringsewu', 'Kecamatan', ''),
  (3847, 18, 368, 5140, 'Sukoharjo', 'Kecamatan', ''),
  (3848, 18, 458, 6317, 'Air Naningan', 'Kecamatan', ''),
  (3849, 18, 458, 6318, 'Bandar Negeri Semuong', 'Kecamatan', ''),
  (3850, 18, 458, 6319, 'Bulok', 'Kecamatan', ''),
  (3851, 18, 458, 6320, 'Cukuh Balak', 'Kecamatan', ''),
  (3852, 18, 458, 6321, 'Gisting', 'Kecamatan', ''),
  (3853, 18, 458, 6322, 'Gunung Alip', 'Kecamatan', ''),
  (3854, 18, 458, 6323, 'Kelumbayan', 'Kecamatan', ''),
  (3855, 18, 458, 6324, 'Kelumbayan Barat', 'Kecamatan', ''),
  (3856, 18, 458, 6325, 'Kota Agung (Kota Agung Pusat)', 'Kecamatan', ''),
  (3857, 18, 458, 6326, 'Kota Agung Barat', 'Kecamatan', ''),
  (3858, 18, 458, 6327, 'Kota Agung Timur', 'Kecamatan', ''),
  (3859, 18, 458, 6328, 'Limau', 'Kecamatan', ''),
  (3860, 18, 458, 6329, 'Pematang Sawa', 'Kecamatan', ''),
  (3861, 18, 458, 6330, 'Pugung', 'Kecamatan', ''),
  (3862, 18, 458, 6331, 'Pulau Panggung', 'Kecamatan', ''),
  (3863, 18, 458, 6332, 'Semaka', 'Kecamatan', ''),
  (3864, 18, 458, 6333, 'Sumberejo', 'Kecamatan', ''),
  (3865, 18, 458, 6334, 'Talang Padang', 'Kecamatan', ''),
  (3866, 18, 458, 6335, 'Ulubelu', 'Kecamatan', ''),
  (3867, 18, 458, 6336, 'Wonosobo', 'Kecamatan', ''),
  (3868, 18, 490, 6798, 'Banjar Agung', 'Kecamatan', ''),
  (3869, 18, 490, 6799, 'Banjar Baru', 'Kecamatan', ''),
  (3870, 18, 490, 6800, 'Banjar Margo', 'Kecamatan', ''),
  (3871, 18, 490, 6801, 'Dente Teladas', 'Kecamatan', ''),
  (3872, 18, 490, 6802, 'Gedung Aji', 'Kecamatan', ''),
  (3873, 18, 490, 6803, 'Gedung Aji Baru', 'Kecamatan', ''),
  (3874, 18, 490, 6804, 'Gedung Meneng', 'Kecamatan', ''),
  (3875, 18, 490, 6805, 'Menggala', 'Kecamatan', ''),
  (3876, 18, 490, 6806, 'Menggala Timur', 'Kecamatan', ''),
  (3877, 18, 490, 6807, 'Meraksa Aji', 'Kecamatan', ''),
  (3878, 18, 490, 6808, 'Penawar Aji', 'Kecamatan', ''),
  (3879, 18, 490, 6809, 'Penawar Tama', 'Kecamatan', ''),
  (3880, 18, 490, 6810, 'Rawa Pitu', 'Kecamatan', ''),
  (3881, 18, 490, 6811, 'Rawajitu Selatan', 'Kecamatan', ''),
  (3882, 18, 490, 6812, 'Rawajitu Timur', 'Kecamatan', ''),
  (3883, 18, 491, 6813, 'Gunung Agung', 'Kecamatan', ''),
  (3884, 18, 491, 6814, 'Gunung Terang', 'Kecamatan', ''),
  (3885, 18, 491, 6815, 'Lambu Kibang', 'Kecamatan', ''),
  (3886, 18, 491, 6816, 'Pagar Dewa', 'Kecamatan', ''),
  (3887, 18, 491, 6817, 'Tulang Bawang Tengah', 'Kecamatan', ''),
  (3888, 18, 491, 6818, 'Tulang Bawang Udik', 'Kecamatan', ''),
  (3889, 18, 491, 6819, 'Tumijajar', 'Kecamatan', ''),
  (3890, 18, 491, 6820, 'Way Kenanga', 'Kecamatan', ''),
  (3891, 18, 496, 6871, 'Bahuga', 'Kecamatan', ''),
  (3892, 18, 496, 6872, 'Banjit', 'Kecamatan', ''),
  (3893, 18, 496, 6873, 'Baradatu', 'Kecamatan', ''),
  (3894, 18, 496, 6874, 'Blambangan Umpu', 'Kecamatan', ''),
  (3895, 18, 496, 6875, 'Buay Bahuga', 'Kecamatan', ''),
  (3896, 18, 496, 6876, 'Bumi Agung', 'Kecamatan', ''),
  (3897, 18, 496, 6877, 'Gunung Labuhan', 'Kecamatan', ''),
  (3898, 18, 496, 6878, 'Kasui', 'Kecamatan', ''),
  (3899, 18, 496, 6879, 'Negara Batin', 'Kecamatan', ''),
  (3900, 18, 496, 6880, 'Negeri Agung', 'Kecamatan', ''),
  (3901, 18, 496, 6881, 'Negeri Besar', 'Kecamatan', ''),
  (3902, 18, 496, 6882, 'Pakuan Ratu', 'Kecamatan', ''),
  (3903, 18, 496, 6883, 'Rebang Tangkas', 'Kecamatan', ''),
  (3904, 18, 496, 6884, 'Way Tuba', 'Kecamatan', ''),
  (3905, 19, 14, 0, 'Ambon', 'Kota', '97222'),
  (3906, 19, 99, 0, 'Buru', 'Kabupaten', '97371'),
  (3907, 19, 100, 0, 'Buru Selatan', 'Kabupaten', '97351'),
  (3908, 19, 185, 0, 'Kepulauan Aru', 'Kabupaten', '97681'),
  (3909, 19, 258, 0, 'Maluku Barat Daya', 'Kabupaten', '97451'),
  (3910, 19, 259, 0, 'Maluku Tengah', 'Kabupaten', '97513'),
  (3911, 19, 260, 0, 'Maluku Tenggara', 'Kabupaten', '97651'),
  (3912, 19, 261, 0, 'Maluku Tenggara Barat', 'Kabupaten', '97465'),
  (3913, 19, 400, 0, 'Seram Bagian Barat', 'Kabupaten', '97561'),
  (3914, 19, 401, 0, 'Seram Bagian Timur', 'Kabupaten', '97581'),
  (3915, 19, 488, 0, 'Tual', 'Kota', '97612'),
  (3916, 19, 14, 209, 'Baguala', 'Kecamatan', ''),
  (3917, 19, 14, 210, 'Leitimur Selatan', 'Kecamatan', ''),
  (3918, 19, 14, 211, 'Nusaniwe (Nusanive)', 'Kecamatan', ''),
  (3919, 19, 14, 212, 'Sirimau', 'Kecamatan', ''),
  (3920, 19, 14, 213, 'Teluk Ambon', 'Kecamatan', ''),
  (3921, 19, 99, 1336, 'Airbuaya', 'Kecamatan', ''),
  (3922, 19, 99, 1337, 'Batabual', 'Kecamatan', ''),
  (3923, 19, 99, 1338, 'Fena Leisela', 'Kecamatan', ''),
  (3924, 19, 99, 1339, 'Lilialy', 'Kecamatan', ''),
  (3925, 19, 99, 1340, 'Lolong Guba', 'Kecamatan', ''),
  (3926, 19, 99, 1341, 'Namlea', 'Kecamatan', ''),
  (3927, 19, 99, 1342, 'Teluk Kaiely', 'Kecamatan', ''),
  (3928, 19, 99, 1343, 'Waeapo', 'Kecamatan', ''),
  (3929, 19, 99, 1344, 'Waelata', 'Kecamatan', ''),
  (3930, 19, 99, 1345, 'Waplau', 'Kecamatan', ''),
  (3931, 19, 100, 1346, 'Ambalau', 'Kecamatan', ''),
  (3932, 19, 100, 1347, 'Fena Fafan', 'Kecamatan', ''),
  (3933, 19, 100, 1348, 'Kepala Madan', 'Kecamatan', ''),
  (3934, 19, 100, 1349, 'Leksula', 'Kecamatan', ''),
  (3935, 19, 100, 1350, 'Namrole', 'Kecamatan', ''),
  (3936, 19, 100, 1351, 'Waesama', 'Kecamatan', ''),
  (3937, 19, 185, 2578, 'Aru Selatan', 'Kecamatan', ''),
  (3938, 19, 185, 2579, 'Aru Selatan Timur', 'Kecamatan', ''),
  (3939, 19, 185, 2580, 'Aru Selatan Utara', 'Kecamatan', ''),
  (3940, 19, 185, 2581, 'Aru Tengah', 'Kecamatan', ''),
  (3941, 19, 185, 2582, 'Aru Tengah Selatan', 'Kecamatan', ''),
  (3942, 19, 185, 2583, 'Aru Tengah Timur', 'Kecamatan', '');
INSERT INTO `location` (`location_id`, `province_id`, `city_id`, `subdistrict_id`, `name`, `type`, `postcode`)
VALUES
  (3943, 19, 185, 2584, 'Aru Utara', 'Kecamatan', ''),
  (3944, 19, 185, 2585, 'Aru Utara Timur Batuley', 'Kecamatan', ''),
  (3945, 19, 185, 2586, 'Pulau-Pulau Aru', 'Kecamatan', ''),
  (3946, 19, 185, 2587, 'Sir-Sir', 'Kecamatan', ''),
  (3947, 19, 258, 3654, 'Damer', 'Kecamatan', ''),
  (3948, 19, 258, 3655, 'Dawelor Dawera', 'Kecamatan', ''),
  (3949, 19, 258, 3656, 'Kepulauan Romang', 'Kecamatan', ''),
  (3950, 19, 258, 3657, 'Kisar Utara', 'Kecamatan', ''),
  (3951, 19, 258, 3658, 'Mdona Hyera/Hiera', 'Kecamatan', ''),
  (3952, 19, 258, 3659, 'Moa Lakor', 'Kecamatan', ''),
  (3953, 19, 258, 3660, 'Pulau Lakor', 'Kecamatan', ''),
  (3954, 19, 258, 3661, 'Pulau Letti (Leti Moa Lakor)', 'Kecamatan', ''),
  (3955, 19, 258, 3662, 'Pulau Masela', 'Kecamatan', ''),
  (3956, 19, 258, 3663, 'Pulau Pulau Babar', 'Kecamatan', ''),
  (3957, 19, 258, 3664, 'Pulau Pulau Terselatan', 'Kecamatan', ''),
  (3958, 19, 258, 3665, 'Pulau Wetang', 'Kecamatan', ''),
  (3959, 19, 258, 3666, 'Pulau-Pulau Babar Timur', 'Kecamatan', ''),
  (3960, 19, 258, 3667, 'Wetar', 'Kecamatan', ''),
  (3961, 19, 258, 3668, 'Wetar Barat', 'Kecamatan', ''),
  (3962, 19, 258, 3669, 'Wetar Timur', 'Kecamatan', ''),
  (3963, 19, 258, 3670, 'Wetar Utara', 'Kecamatan', ''),
  (3964, 19, 259, 3671, 'Amahai', 'Kecamatan', ''),
  (3965, 19, 259, 3672, 'Banda', 'Kecamatan', ''),
  (3966, 19, 259, 3673, 'Leihitu', 'Kecamatan', ''),
  (3967, 19, 259, 3674, 'Leihitu Barat', 'Kecamatan', ''),
  (3968, 19, 259, 3675, 'Masohi Kota', 'Kecamatan', ''),
  (3969, 19, 259, 3676, 'Nusalaut', 'Kecamatan', ''),
  (3970, 19, 259, 3677, 'Pulau Haruku', 'Kecamatan', ''),
  (3971, 19, 259, 3678, 'Salahutu', 'Kecamatan', ''),
  (3972, 19, 259, 3679, 'Saparua', 'Kecamatan', ''),
  (3973, 19, 259, 3680, 'Saparua Timur', 'Kecamatan', ''),
  (3974, 19, 259, 3681, 'Seram Utara', 'Kecamatan', ''),
  (3975, 19, 259, 3682, 'Seram Utara Barat', 'Kecamatan', ''),
  (3976, 19, 259, 3683, 'Seram Utara Timur Kobi', 'Kecamatan', ''),
  (3977, 19, 259, 3684, 'Seram Utara Timur Seti', 'Kecamatan', ''),
  (3978, 19, 259, 3685, 'Tehoru', 'Kecamatan', ''),
  (3979, 19, 259, 3686, 'Teluk Elpaputih', 'Kecamatan', ''),
  (3980, 19, 259, 3687, 'Telutih', 'Kecamatan', ''),
  (3981, 19, 259, 3688, 'Teon Nila Serua', 'Kecamatan', ''),
  (3982, 19, 260, 3689, 'Hoat Sorbay', 'Kecamatan', ''),
  (3983, 19, 260, 3690, 'Kei Besar', 'Kecamatan', ''),
  (3984, 19, 260, 3691, 'Kei Besar Selatan', 'Kecamatan', ''),
  (3985, 19, 260, 3692, 'Kei Besar Selatan Barat', 'Kecamatan', ''),
  (3986, 19, 260, 3693, 'Kei Besar Utara Barat', 'Kecamatan', ''),
  (3987, 19, 260, 3694, 'Kei Besar Utara Timur', 'Kecamatan', ''),
  (3988, 19, 260, 3695, 'Kei Kecil', 'Kecamatan', ''),
  (3989, 19, 260, 3696, 'Kei Kecil Barat', 'Kecamatan', ''),
  (3990, 19, 260, 3697, 'Kei Kecil Timur', 'Kecamatan', ''),
  (3991, 19, 260, 3698, 'Kei Kecil Timur Selatan', 'Kecamatan', ''),
  (3992, 19, 260, 3699, 'Manyeuw', 'Kecamatan', ''),
  (3993, 19, 261, 3700, 'Kormomolin', 'Kecamatan', ''),
  (3994, 19, 261, 3701, 'Molu Maru', 'Kecamatan', ''),
  (3995, 19, 261, 3702, 'Nirunmas', 'Kecamatan', ''),
  (3996, 19, 261, 3703, 'Selaru', 'Kecamatan', ''),
  (3997, 19, 261, 3704, 'Tanimbar Selatan', 'Kecamatan', ''),
  (3998, 19, 261, 3705, 'Tanimbar Utara', 'Kecamatan', ''),
  (3999, 19, 261, 3706, 'Wermakatian (Wer Maktian)', 'Kecamatan', ''),
  (4000, 19, 261, 3707, 'Wertamrian', 'Kecamatan', ''),
  (4001, 19, 261, 3708, 'Wuarlabobar', 'Kecamatan', ''),
  (4002, 19, 261, 3709, 'Yaru', 'Kecamatan', ''),
  (4003, 19, 400, 5514, 'Amalatu', 'Kecamatan', ''),
  (4004, 19, 400, 5515, 'Elpaputih', 'Kecamatan', ''),
  (4005, 19, 400, 5516, 'Huamual', 'Kecamatan', ''),
  (4006, 19, 400, 5517, 'Huamual Belakang (Waisala)', 'Kecamatan', ''),
  (4007, 19, 400, 5518, 'Inamosol', 'Kecamatan', ''),
  (4008, 19, 400, 5519, 'Kairatu', 'Kecamatan', ''),
  (4009, 19, 400, 5520, 'Kairatu Barat', 'Kecamatan', ''),
  (4010, 19, 400, 5521, 'Kepulauan Manipa', 'Kecamatan', ''),
  (4011, 19, 400, 5522, 'Seram Barat', 'Kecamatan', ''),
  (4012, 19, 400, 5523, 'Taniwel', 'Kecamatan', ''),
  (4013, 19, 400, 5524, 'Taniwel Timur', 'Kecamatan', ''),
  (4014, 19, 401, 5525, 'Bula', 'Kecamatan', ''),
  (4015, 19, 401, 5526, 'Bula Barat', 'Kecamatan', ''),
  (4016, 19, 401, 5527, 'Gorom Timur', 'Kecamatan', ''),
  (4017, 19, 401, 5528, 'Kian Darat', 'Kecamatan', ''),
  (4018, 19, 401, 5529, 'Kilmury', 'Kecamatan', ''),
  (4019, 19, 401, 5530, 'Pulau Gorong (Gorom)', 'Kecamatan', ''),
  (4020, 19, 401, 5531, 'Pulau Panjang', 'Kecamatan', ''),
  (4021, 19, 401, 5532, 'Seram Timur', 'Kecamatan', ''),
  (4022, 19, 401, 5533, 'Siritaun Wida Timur', 'Kecamatan', ''),
  (4023, 19, 401, 5534, 'Siwalalat', 'Kecamatan', ''),
  (4024, 19, 401, 5535, 'Teluk Waru', 'Kecamatan', ''),
  (4025, 19, 401, 5536, 'Teor', 'Kecamatan', ''),
  (4026, 19, 401, 5537, 'Tutuk Tolu', 'Kecamatan', ''),
  (4027, 19, 401, 5538, 'Wakate', 'Kecamatan', ''),
  (4028, 19, 401, 5539, 'Werinama', 'Kecamatan', ''),
  (4029, 19, 488, 6773, 'Kur Selatan', 'Kecamatan', ''),
  (4030, 19, 488, 6774, 'Pulau Dullah Selatan', 'Kecamatan', ''),
  (4031, 19, 488, 6775, 'Pulau Dullah Utara', 'Kecamatan', ''),
  (4032, 19, 488, 6776, 'Pulau Tayando Tam', 'Kecamatan', ''),
  (4033, 19, 488, 6777, 'Pulau-Pulau Kur', 'Kecamatan', ''),
  (4034, 20, 138, 0, 'Halmahera Barat', 'Kabupaten', '97757'),
  (4035, 20, 139, 0, 'Halmahera Selatan', 'Kabupaten', '97911'),
  (4036, 20, 140, 0, 'Halmahera Tengah', 'Kabupaten', '97853'),
  (4037, 20, 141, 0, 'Halmahera Timur', 'Kabupaten', '97862'),
  (4038, 20, 142, 0, 'Halmahera Utara', 'Kabupaten', '97762'),
  (4039, 20, 191, 0, 'Kepulauan Sula', 'Kabupaten', '97995'),
  (4040, 20, 372, 0, 'Pulau Morotai', 'Kabupaten', '97771'),
  (4041, 20, 477, 0, 'Ternate', 'Kota', '97714'),
  (4042, 20, 478, 0, 'Tidore Kepulauan', 'Kota', '97815'),
  (4043, 20, 138, 1901, 'Ibu', 'Kecamatan', ''),
  (4044, 20, 138, 1902, 'Ibu Selatan', 'Kecamatan', ''),
  (4045, 20, 138, 1903, 'Ibu Utara', 'Kecamatan', ''),
  (4046, 20, 138, 1904, 'Jailolo', 'Kecamatan', ''),
  (4047, 20, 138, 1905, 'Jailolo Selatan', 'Kecamatan', ''),
  (4048, 20, 138, 1906, 'Loloda', 'Kecamatan', ''),
  (4049, 20, 138, 1907, 'Sahu', 'Kecamatan', ''),
  (4050, 20, 138, 1908, 'Sahu Timur', 'Kecamatan', ''),
  (4051, 20, 139, 1909, 'Bacan', 'Kecamatan', ''),
  (4052, 20, 139, 1910, 'Bacan Barat', 'Kecamatan', ''),
  (4053, 20, 139, 1911, 'Bacan Barat Utara', 'Kecamatan', ''),
  (4054, 20, 139, 1912, 'Bacan Selatan', 'Kecamatan', ''),
  (4055, 20, 139, 1913, 'Bacan Timur', 'Kecamatan', ''),
  (4056, 20, 139, 1914, 'Bacan Timur Selatan', 'Kecamatan', ''),
  (4057, 20, 139, 1915, 'Bacan Timur Tengah', 'Kecamatan', ''),
  (4058, 20, 139, 1916, 'Gane Barat', 'Kecamatan', ''),
  (4059, 20, 139, 1917, 'Gane Barat Selatan', 'Kecamatan', ''),
  (4060, 20, 139, 1918, 'Gane Barat Utara', 'Kecamatan', ''),
  (4061, 20, 139, 1919, 'Gane Timur', 'Kecamatan', ''),
  (4062, 20, 139, 1920, 'Gane Timur Selatan', 'Kecamatan', ''),
  (4063, 20, 139, 1921, 'Gane Timur Tengah', 'Kecamatan', ''),
  (4064, 20, 139, 1922, 'Kasiruta Barat', 'Kecamatan', ''),
  (4065, 20, 139, 1923, 'Kasiruta Timur', 'Kecamatan', ''),
  (4066, 20, 139, 1924, 'Kayoa', 'Kecamatan', ''),
  (4067, 20, 139, 1925, 'Kayoa Barat', 'Kecamatan', ''),
  (4068, 20, 139, 1926, 'Kayoa Selatan', 'Kecamatan', ''),
  (4069, 20, 139, 1927, 'Kayoa Utara', 'Kecamatan', ''),
  (4070, 20, 139, 1928, 'Kepulauan Botanglomang', 'Kecamatan', ''),
  (4071, 20, 139, 1929, 'Kepulauan Joronga', 'Kecamatan', ''),
  (4072, 20, 139, 1930, 'Makian (Pulau Makian)', 'Kecamatan', ''),
  (4073, 20, 139, 1931, 'Makian Barat (Pulau Makian)', 'Kecamatan', ''),
  (4074, 20, 139, 1932, 'Mandioli Selatan', 'Kecamatan', ''),
  (4075, 20, 139, 1933, 'Mandioli Utara', 'Kecamatan', ''),
  (4076, 20, 139, 1934, 'Obi', 'Kecamatan', ''),
  (4077, 20, 139, 1935, 'Obi Barat', 'Kecamatan', ''),
  (4078, 20, 139, 1936, 'Obi Selatan', 'Kecamatan', ''),
  (4079, 20, 139, 1937, 'Obi Timur', 'Kecamatan', ''),
  (4080, 20, 139, 1938, 'Obi Utara', 'Kecamatan', ''),
  (4081, 20, 140, 1939, 'Patani', 'Kecamatan', ''),
  (4082, 20, 140, 1940, 'Patani Barat', 'Kecamatan', ''),
  (4083, 20, 140, 1941, 'Patani Utara', 'Kecamatan', ''),
  (4084, 20, 140, 1942, 'Pulau Gebe', 'Kecamatan', ''),
  (4085, 20, 140, 1943, 'Weda', 'Kecamatan', ''),
  (4086, 20, 140, 1944, 'Weda Selatan', 'Kecamatan', ''),
  (4087, 20, 140, 1945, 'Weda Tengah', 'Kecamatan', ''),
  (4088, 20, 140, 1946, 'Weda Utara', 'Kecamatan', ''),
  (4089, 20, 142, 1957, 'Galela', 'Kecamatan', ''),
  (4090, 20, 142, 1958, 'Galela Barat', 'Kecamatan', ''),
  (4091, 20, 142, 1959, 'Galela Selatan', 'Kecamatan', ''),
  (4092, 20, 142, 1960, 'Galela Utara', 'Kecamatan', ''),
  (4093, 20, 142, 1961, 'Kao', 'Kecamatan', ''),
  (4094, 20, 142, 1962, 'Kao Barat', 'Kecamatan', ''),
  (4095, 20, 142, 1963, 'Kao Teluk', 'Kecamatan', ''),
  (4096, 20, 142, 1964, 'Kao Utara', 'Kecamatan', ''),
  (4097, 20, 142, 1965, 'Loloda Kepulauan', 'Kecamatan', ''),
  (4098, 20, 142, 1966, 'Loloda Utara', 'Kecamatan', ''),
  (4099, 20, 142, 1967, 'Malifut', 'Kecamatan', ''),
  (4100, 20, 142, 1968, 'Tobelo', 'Kecamatan', ''),
  (4101, 20, 142, 1969, 'Tobelo Barat', 'Kecamatan', ''),
  (4102, 20, 142, 1970, 'Tobelo Selatan', 'Kecamatan', ''),
  (4103, 20, 142, 1971, 'Tobelo Tengah', 'Kecamatan', ''),
  (4104, 20, 142, 1972, 'Tobelo Timur', 'Kecamatan', ''),
  (4105, 20, 142, 1973, 'Tobelo Utara', 'Kecamatan', ''),
  (4106, 20, 141, 1947, 'Kota Maba', 'Kecamatan', ''),
  (4107, 20, 141, 1948, 'Maba', 'Kecamatan', ''),
  (4108, 20, 141, 1949, 'Maba Selatan', 'Kecamatan', ''),
  (4109, 20, 141, 1950, 'Maba Tengah', 'Kecamatan', ''),
  (4110, 20, 141, 1951, 'Maba Utara', 'Kecamatan', ''),
  (4111, 20, 141, 1952, 'Wasile', 'Kecamatan', ''),
  (4112, 20, 141, 1953, 'Wasile Selatan', 'Kecamatan', ''),
  (4113, 20, 141, 1954, 'Wasile Tengah', 'Kecamatan', ''),
  (4114, 20, 141, 1955, 'Wasile Timur', 'Kecamatan', ''),
  (4115, 20, 141, 1956, 'Wasile Utara', 'Kecamatan', ''),
  (4116, 20, 191, 2634, 'Lede', 'Kecamatan', ''),
  (4117, 20, 191, 2635, 'Mangoli Barat', 'Kecamatan', ''),
  (4118, 20, 191, 2636, 'Mangoli Selatan', 'Kecamatan', ''),
  (4119, 20, 191, 2637, 'Mangoli Tengah', 'Kecamatan', ''),
  (4120, 20, 191, 2638, 'Mangoli Timur', 'Kecamatan', ''),
  (4121, 20, 191, 2639, 'Mangoli Utara', 'Kecamatan', ''),
  (4122, 20, 191, 2640, 'Mangoli Utara Timur', 'Kecamatan', ''),
  (4123, 20, 191, 2641, 'Sanana', 'Kecamatan', ''),
  (4124, 20, 191, 2642, 'Sanana Utara', 'Kecamatan', ''),
  (4125, 20, 191, 2643, 'Sulabesi Barat', 'Kecamatan', ''),
  (4126, 20, 191, 2644, 'Sulabesi Selatan', 'Kecamatan', ''),
  (4127, 20, 191, 2645, 'Sulabesi Tengah', 'Kecamatan', ''),
  (4128, 20, 191, 2646, 'Sulabesi Timur', 'Kecamatan', ''),
  (4129, 20, 191, 2647, 'Taliabu Barat', 'Kecamatan', ''),
  (4130, 20, 191, 2648, 'Taliabu Barat Laut', 'Kecamatan', ''),
  (4131, 20, 191, 2649, 'Taliabu Selatan', 'Kecamatan', ''),
  (4132, 20, 191, 2650, 'Taliabu Timur', 'Kecamatan', ''),
  (4133, 20, 191, 2651, 'Taliabu Timur Selatan', 'Kecamatan', ''),
  (4134, 20, 191, 2652, 'Taliabu Utara', 'Kecamatan', ''),
  (4135, 20, 372, 5178, 'Morotai Jaya', 'Kecamatan', ''),
  (4136, 20, 372, 5179, 'Morotai Selatan', 'Kecamatan', ''),
  (4137, 20, 372, 5180, 'Morotai Selatan Barat', 'Kecamatan', ''),
  (4138, 20, 372, 5181, 'Morotai Timur', 'Kecamatan', ''),
  (4139, 20, 372, 5182, 'Morotai Utara', 'Kecamatan', ''),
  (4140, 20, 477, 6581, 'Moti (Pulau Moti)', 'Kecamatan', ''),
  (4141, 20, 477, 6582, 'Pulau Batang Dua', 'Kecamatan', ''),
  (4142, 20, 477, 6583, 'Pulau Hiri', 'Kecamatan', ''),
  (4143, 20, 477, 6584, 'Pulau Ternate', 'Kecamatan', ''),
  (4144, 20, 477, 6585, 'Ternate Selatan (Kota)', 'Kecamatan', ''),
  (4145, 20, 477, 6586, 'Ternate Tengah (Kota)', 'Kecamatan', ''),
  (4146, 20, 477, 6587, 'Ternate Utara (Kota)', 'Kecamatan', ''),
  (4147, 20, 478, 6588, 'Oba', 'Kecamatan', ''),
  (4148, 20, 478, 6589, 'Oba Selatan', 'Kecamatan', ''),
  (4149, 20, 478, 6590, 'Oba Tengah', 'Kecamatan', ''),
  (4150, 20, 478, 6591, 'Oba Utara', 'Kecamatan', ''),
  (4151, 20, 478, 6592, 'Tidore (Pulau Tidore)', 'Kecamatan', ''),
  (4152, 20, 478, 6593, 'Tidore Selatan', 'Kecamatan', ''),
  (4153, 20, 478, 6594, 'Tidore Timur (Pulau Tidore)', 'Kecamatan', ''),
  (4154, 20, 478, 6595, 'Tidore Utara', 'Kecamatan', ''),
  (4155, 21, 1, 0, 'Aceh Barat', 'Kabupaten', '23681'),
  (4156, 21, 2, 0, 'Aceh Barat Daya', 'Kabupaten', '23764'),
  (4157, 21, 3, 0, 'Aceh Besar', 'Kabupaten', '23951'),
  (4158, 21, 4, 0, 'Aceh Jaya', 'Kabupaten', '23654'),
  (4159, 21, 5, 0, 'Aceh Selatan', 'Kabupaten', '23719'),
  (4160, 21, 6, 0, 'Aceh Singkil', 'Kabupaten', '24785'),
  (4161, 21, 7, 0, 'Aceh Tamiang', 'Kabupaten', '24476'),
  (4162, 21, 8, 0, 'Aceh Tengah', 'Kabupaten', '24511'),
  (4163, 21, 9, 0, 'Aceh Tenggara', 'Kabupaten', '24611'),
  (4164, 21, 10, 0, 'Aceh Timur', 'Kabupaten', '24454'),
  (4165, 21, 11, 0, 'Aceh Utara', 'Kabupaten', '24382'),
  (4166, 21, 20, 0, 'Banda Aceh', 'Kota', '23238'),
  (4167, 21, 59, 0, 'Bener Meriah', 'Kabupaten', '24581'),
  (4168, 21, 72, 0, 'Bireuen', 'Kabupaten', '24219'),
  (4169, 21, 127, 0, 'Gayo Lues', 'Kabupaten', '24653'),
  (4170, 21, 230, 0, 'Langsa', 'Kota', '24412'),
  (4171, 21, 235, 0, 'Lhokseumawe', 'Kota', '24352'),
  (4172, 21, 300, 0, 'Nagan Raya', 'Kabupaten', '23674'),
  (4173, 21, 358, 0, 'Pidie', 'Kabupaten', '24116'),
  (4174, 21, 359, 0, 'Pidie Jaya', 'Kabupaten', '24186'),
  (4175, 21, 384, 0, 'Sabang', 'Kota', '23512'),
  (4176, 21, 414, 0, 'Simeulue', 'Kabupaten', '23891'),
  (4177, 21, 429, 0, 'Subulussalam', 'Kota', '24882'),
  (4178, 21, 1, 1, 'Arongan Lambalek', 'Kecamatan', ''),
  (4179, 21, 1, 2, 'Bubon', 'Kecamatan', ''),
  (4180, 21, 1, 3, 'Johan Pahlawan', 'Kecamatan', ''),
  (4181, 21, 1, 4, 'Kaway XVI', 'Kecamatan', ''),
  (4182, 21, 1, 5, 'Meureubo', 'Kecamatan', ''),
  (4183, 21, 1, 6, 'Pante Ceureumen (Pantai Ceuremen)', 'Kecamatan', ''),
  (4184, 21, 1, 7, 'Panton Reu', 'Kecamatan', ''),
  (4185, 21, 1, 8, 'Samatiga', 'Kecamatan', ''),
  (4186, 21, 1, 9, 'Sungai Mas', 'Kecamatan', ''),
  (4187, 21, 1, 10, 'Woyla', 'Kecamatan', ''),
  (4188, 21, 1, 11, 'Woyla Barat', 'Kecamatan', ''),
  (4189, 21, 1, 12, 'Woyla Timur', 'Kecamatan', ''),
  (4190, 21, 2, 13, 'Babah Rot', 'Kecamatan', ''),
  (4191, 21, 2, 14, 'Blang Pidie', 'Kecamatan', ''),
  (4192, 21, 2, 15, 'Jeumpa', 'Kecamatan', ''),
  (4193, 21, 2, 16, 'Kuala Batee', 'Kecamatan', ''),
  (4194, 21, 2, 17, 'Lembah Sabil', 'Kecamatan', ''),
  (4195, 21, 2, 18, 'Manggeng', 'Kecamatan', ''),
  (4196, 21, 2, 19, 'Setia', 'Kecamatan', ''),
  (4197, 21, 2, 20, 'Susoh', 'Kecamatan', ''),
  (4198, 21, 2, 21, 'Tangan-Tangan', 'Kecamatan', ''),
  (4199, 21, 3, 22, 'Baitussalam', 'Kecamatan', ''),
  (4200, 21, 3, 23, 'Blank Bintang', 'Kecamatan', ''),
  (4201, 21, 3, 24, 'Darul Imarah', 'Kecamatan', ''),
  (4202, 21, 3, 25, 'Darul Kamal', 'Kecamatan', ''),
  (4203, 21, 3, 26, 'Darussalam', 'Kecamatan', ''),
  (4204, 21, 3, 27, 'Indrapuri', 'Kecamatan', ''),
  (4205, 21, 3, 28, 'Ingin Jaya', 'Kecamatan', ''),
  (4206, 21, 3, 29, 'Kota Cot Glie (Kuta Cot Glie)', 'Kecamatan', ''),
  (4207, 21, 3, 30, 'Kota Jantho', 'Kecamatan', ''),
  (4208, 21, 3, 31, 'Kota Malaka (Kuta Malaka)', 'Kecamatan', ''),
  (4209, 21, 3, 32, 'Krueng Barona Jaya', 'Kecamatan', ''),
  (4210, 21, 3, 33, 'Kuta Baro', 'Kecamatan', ''),
  (4211, 21, 3, 34, 'Lembah Seulawah', 'Kecamatan', ''),
  (4212, 21, 3, 35, 'Leupung', 'Kecamatan', ''),
  (4213, 21, 3, 36, 'Lhoknga (Lho\' nga)
  ', '
  Kecamatan
  ', ''),
(4214, 21, 3, 37, '
  Lhoong
  ', '
  Kecamatan
  ', ''),
(4215, 21, 3, 38, '
  Mantasiek (
  Montasik)
  ', '
  Kecamatan
  ', ''),
(4216, 21, 3, 39, '
  Mesjid
  Raya
  ', '
  Kecamatan
  ', ''),
(4217, 21, 3, 40, '
  Peukan
  Bada
  ', '
  Kecamatan
  ', ''),
(4218, 21, 3, 41, '
  Pulo
  Aceh
  ', '
  Kecamatan
  ', ''),
(4219, 21, 3, 42, '
  Seulimeum
  ', '
  Kecamatan
  ', ''),
(4220, 21, 3, 43, '
  Simpang
  Tiga
  ', '
  Kecamatan
  ', ''),
(4221, 21, 3, 44, '
  Suka
  Makmur
  ', '
  Kecamatan
  ', ''),
(4222, 21, 4, 45, '
  Darul
  Hikmah
  ', '
  Kecamatan
  ', ''),
(4223, 21, 4, 46, '
  Indra
  Jaya
  ', '
  Kecamatan
  ', ''),
(4224, 21, 4, 47, '
  Jaya
  ', '
  Kecamatan
  ', ''),
(4225, 21, 4, 48, '
  Keude
  Panga
  ', '
  Kecamatan
  ', ''),
(4226, 21, 4, 49, '
  Krueng
  Sabee
  ', '
  Kecamatan
  ', ''),
(4227, 21, 4, 50, '
  Pasie
  Raya
  ', '
  Kecamatan
  ', ''),
(4228, 21, 4, 51, '
  Sampoiniet
  ', '
  Kecamatan
  ', ''),
(4229, 21, 4, 52, '
  Setia
  Bakti
  ', '
  Kecamatan
  ', ''),
(4230, 21, 4, 53, '
  Teunom
  ', '
  Kecamatan
  ', ''),
(4231, 21, 5, 54, '
  Bakongan
  ', '
  Kecamatan
  ', ''),
(4232, 21, 5, 55, '
  Bakongan
  Timur
  ', '
  Kecamatan
  ', ''),
(4233, 21, 5, 56, '
  Kluet
  Selatan
  ', '
  Kecamatan
  ', ''),
(4234, 21, 5, 57, '
  Kluet
  Tengah
  ', '
  Kecamatan
  ', ''),
(4235, 21, 5, 58, '
  Kluet
  Timur
  ', '
  Kecamatan
  ', ''),
(4236, 21, 5, 59, '
  Kluet
  Utara
  ', '
  Kecamatan
  ', ''),
(4237, 21, 5, 60, '
  Kota
  Bahagia
  ', '
  Kecamatan
  ', ''),
(4238, 21, 5, 61, '
  Labuhan
  Haji
  ', '
  Kecamatan
  ', ''),
(4239, 21, 5, 62, '
  Labuhan
  Haji
  Barat
  ', '
  Kecamatan
  ', ''),
(4240, 21, 5, 63, '
  Labuhan
  Haji
  Timur
  ', '
  Kecamatan
  ', ''),
(4241, 21, 5, 64, '
  Meukek
  ', '
  Kecamatan
  ', ''),
(4242, 21, 5, 65, '
  Pasie
  Raja
  ', '
  Kecamatan
  ', ''),
(4243, 21, 5, 66, '
  Sama
  Dua
  ', '
  Kecamatan
  ', ''),
(4244, 21, 5, 67, '
  Sawang
  ', '
  Kecamatan
  ', ''),
(4245, 21, 5, 68, '
  Tapak
  Tuan
  ', '
  Kecamatan
  ', ''),
(4246, 21, 5, 69, '
  Trumon
  ', '
  Kecamatan
  ', ''),
(4247, 21, 5, 70, '
  Trumon
  Tengah
  ', '
  Kecamatan
  ', ''),
(4248, 21, 5, 71, '
  Trumon
  Timur
  ', '
  Kecamatan
  ', ''),
(4249, 21, 6, 72, '
  Danau
  Paris
  ', '
  Kecamatan
  ', ''),
(4250, 21, 6, 73, '
  Gunung
  Meriah (
  Mariah)
  ', '
  Kecamatan
  ', ''),
(4251, 21, 6, 74, '
  Kota
  Baharu
  ', '
  Kecamatan
  ', ''),
(4252, 21, 6, 75, '
  Kuala
  Baru
  ', '
  Kecamatan
  ', ''),
(4253, 21, 6, 76, '
  Pulau
  Banyak
  ', '
  Kecamatan
  ', ''),
(4254, 21, 6, 77, '
  Pulau
  Banyak
  Barat
  ', '
  Kecamatan
  ', ''),
(4255, 21, 6, 78, '
  Simpang
  Kanan
  ', '
  Kecamatan
  ', ''),
(4256, 21, 6, 79, '
  Singkil
  ', '
  Kecamatan
  ', ''),
(4257, 21, 6, 80, '
  Singkil
  Utara
  ', '
  Kecamatan
  ', ''),
(4258, 21, 6, 81, '
  Singkohor
  ', '
  Kecamatan
  ', ''),
(4259, 21, 6, 82, '
  Suro
  Makmur
  ', '
  Kecamatan
  ', ''),
(4260, 21, 8, 95, '
  Atu
  Lintang
  ', '
  Kecamatan
  ', ''),
(4261, 21, 8, 96, '
  Bebesen
  ', '
  Kecamatan
  ', ''),
(4262, 21, 8, 97, '
  Bies
  ', '
  Kecamatan
  ', ''),
(4263, 21, 8, 98, '
  Bintang
  ', '
  Kecamatan
  ', ''),
(4264, 21, 8, 99, '
  Celala
  ', '
  Kecamatan
  ', ''),
(4265, 21, 8, 100, '
  Jagong
  Jeget
  ', '
  Kecamatan
  ', ''),
(4266, 21, 8, 101, '
  Kebayakan
  ', '
  Kecamatan
  ', ''),
(4267, 21, 8, 102, '
  Ketol
  ', '
  Kecamatan
  ', ''),
(4268, 21, 8, 103, '
  Kute
  Panang
  ', '
  Kecamatan
  ', ''),
(4269, 21, 8, 104, '
  Linge
  ', '
  Kecamatan
  ', ''),
(4270, 21, 8, 105, '
  Lut
  Tawar
  ', '
  Kecamatan
  ', ''),
(4271, 21, 8, 106, '
  Pegasing
  ', '
  Kecamatan
  ', ''),
(4272, 21, 8, 107, '
  Rusip
  Antara
  ', '
  Kecamatan
  ', ''),
(4273, 21, 8, 108, '
  Silih
  Nara
  ', '
  Kecamatan
  ', ''),
(4274, 21, 7, 83, '
  Banda
  Mulia
  ', '
  Kecamatan
  ', ''),
(4275, 21, 7, 84, '
  Bandar
  Pusaka
  ', '
  Kecamatan
  ', ''),
(4276, 21, 7, 85, '
  Bendahara
  ', '
  Kecamatan
  ', ''),
(4277, 21, 7, 86, '
  Karang
  Baru
  ', '
  Kecamatan
  ', ''),
(4278, 21, 7, 87, '
  Kejuruan
  Muda
  ', '
  Kecamatan
  ', ''),
(4279, 21, 7, 88, '
  Kota
  Kuala
  Simpang
  ', '
  Kecamatan
  ', ''),
(4280, 21, 7, 89, '
  Manyak
  Payed
  ', '
  Kecamatan
  ', ''),
(4281, 21, 7, 90, '
  Rantau
  ', '
  Kecamatan
  ', ''),
(4282, 21, 7, 91, '
  Sekerak
  ', '
  Kecamatan
  ', ''),
(4283, 21, 7, 92, '
  Seruway
  ', '
  Kecamatan
  ', ''),
(4284, 21, 7, 93, '
  Tamiang
  Hulu
  ', '
  Kecamatan
  ', ''),
(4285, 21, 7, 94, '
  Tenggulun
  ', '
  Kecamatan
  ', ''),
(4286, 21, 9, 109, '
  Babul
  Makmur
  ', '
  Kecamatan
  ', ''),
(4287, 21, 9, 110, '
  Babul
  Rahmah
  ', '
  Kecamatan
  ', ''),
(4288, 21, 9, 111, '
  Babussalam
  ', '
  Kecamatan
  ', ''),
(4289, 21, 9, 112, '
  Badar
  ', '
  Kecamatan
  ', ''),
(4290, 21, 9, 113, '
  Bambel
  ', '
  Kecamatan
  ', ''),
(4291, 21, 9, 114, '
  Bukit
  Tusam
  ', '
  Kecamatan
  ', ''),
(4292, 21, 9, 115, '
  Darul
  Hasanah
  ', '
  Kecamatan
  ', ''),
(4293, 21, 9, 116, '
  Deleng
  Pokhisen
  ', '
  Kecamatan
  ', ''),
(4294, 21, 9, 117, '
  Ketambe
  ', '
  Kecamatan
  ', ''),
(4295, 21, 9, 118, '
  Lawe
  Alas
  ', '
  Kecamatan
  ', ''),
(4296, 21, 9, 119, '
  Lawe
  Bulan
  ', '
  Kecamatan
  ', ''),
(4297, 21, 9, 120, '
  Lawe
  Sigala
  -
  Gala
  ', '
  Kecamatan
  ', ''),
(4298, 21, 9, 121, '
  Lawe
  Sumur
  ', '
  Kecamatan
  ', ''),
(4299, 21, 9, 122, '
  Leuser
  ', '
  Kecamatan
  ', ''),
(4300, 21, 9, 123, '
  Semadam
  ', '
  Kecamatan
  ', ''),
(4301, 21, 9, 124, '
  Tanah
  Alas
  ', '
  Kecamatan
  ', ''),
(4302, 21, 10, 125, '
  Banda
  Alam
  ', '
  Kecamatan
  ', ''),
(4303, 21, 10, 126, '
  Birem
  Bayeun
  ', '
  Kecamatan
  ', ''),
(4304, 21, 10, 127, '
  Darul
  Aman
  ', '
  Kecamatan
  ', ''),
(4305, 21, 10, 128, '
  Darul
  Falah
  ', '
  Kecamatan
  ', ''),
(4306, 21, 10, 129, '
  Darul
  Iksan (
  Ihsan)
  ', '
  Kecamatan
  ', ''),
(4307, 21, 10, 130, '
  Idi
  Rayeuk
  ', '
  Kecamatan
  ', ''),
(4308, 21, 10, 131, '
  Idi
  Timur
  ', '
  Kecamatan
  ', ''),
(4309, 21, 10, 132, '
  Idi
  Tunong
  ', '
  Kecamatan
  ', ''),
(4310, 21, 10, 133, '
  Indra
  Makmur
  ', '
  Kecamatan
  ', ''),
(4311, 21, 10, 134, '
  Julok
  ', '
  Kecamatan
  ', ''),
(4312, 21, 10, 135, '
  Madat
  ', '
  Kecamatan
  ', ''),
(4313, 21, 10, 136, '
  Nurussalam
  ', '
  Kecamatan
  ', ''),
(4314, 21, 10, 137, '
  Pante
  Bidari (
  Beudari)
  ', '
  Kecamatan
  ', ''),
(4315, 21, 10, 138, '
  Peudawa
  ', '
  Kecamatan
  ', ''),
(4316, 21, 10, 139, '
  Peunaron
  ', '
  Kecamatan
  ', ''),
(4317, 21, 10, 140, '
  Peureulak
  ', '
  Kecamatan
  ', ''),
(4318, 21, 10, 141, '
  Peureulak
  Barat
  ', '
  Kecamatan
  ', ''),
(4319, 21, 10, 142, '
  Peureulak
  Timur
  ', '
  Kecamatan
  ', ''),
(4320, 21, 10, 143, '
  Rantau
  Selamat
  ', '
  Kecamatan
  ', ''),
(4321, 21, 10, 144, '
  Ranto
  Peureulak
  ', '
  Kecamatan
  ', ''),
(4322, 21, 10, 145, '
  Serba
  Jadi
  ', '
  Kecamatan
  ', ''),
(4323, 21, 10, 146, '
  Simpang
  Jernih
  ', '
  Kecamatan
  ', ''),
(4324, 21, 10, 147, '
  Simpang
  Ulim
  ', '
  Kecamatan
  ', ''),
(4325, 21, 10, 148, '
  Sungai
  Raya
  ', '
  Kecamatan
  ', ''),
(4326, 21, 11, 149, '
  Baktiya
  ', '
  Kecamatan
  ', ''),
(4327, 21, 11, 150, '
  Baktiya
  Barat
  ', '
  Kecamatan
  ', ''),
(4328, 21, 11, 151, '
  Banda
  Baro
  ', '
  Kecamatan
  ', ''),
(4329, 21, 11, 152, '
  Cot
  Girek
  ', '
  Kecamatan
  ', ''),
(4330, 21, 11, 153, '
  Dewantara
  ', '
  Kecamatan
  ', ''),
(4331, 21, 11, 154, '
  Geuredong
  Pase
  ', '
  Kecamatan
  ', ''),
(4332, 21, 11, 155, '
  Kuta
  Makmur
  ', '
  Kecamatan
  ', ''),
(4333, 21, 11, 156, '
  Langkahan
  ', '
  Kecamatan
  ', ''),
(4334, 21, 11, 157, '
  Lapang
  ', '
  Kecamatan
  ', ''),
(4335, 21, 11, 158, '
  Lhoksukon
  ', '
  Kecamatan
  ', ''),
(4336, 21, 11, 159, '
  Matangkuli
  ', '
  Kecamatan
  ', ''),
(4337, 21, 11, 160, '
  Meurah
  Mulia
  ', '
  Kecamatan
  ', ''),
(4338, 21, 11, 161, '
  Muara
  Batu
  ', '
  Kecamatan
  ', ''),
(4339, 21, 11, 162, '
  Nibong
  ', '
  Kecamatan
  ', ''),
(4340, 21, 11, 163, '
  Nisam
  ', '
  Kecamatan
  ', ''),
(4341, 21, 11, 164, '
  Nisam
  Antara
  ', '
  Kecamatan
  ', ''),
(4342, 21, 11, 165, '
  Paya
  Bakong
  ', '
  Kecamatan
  ', ''),
(4343, 21, 11, 166, '
  Pirak
  Timur
  ', '
  Kecamatan
  ', ''),
(4344, 21, 11, 167, '
  Samudera
  ', '
  Kecamatan
  ', ''),
(4345, 21, 11, 168, '
  Sawang
  ', '
  Kecamatan
  ', ''),
(4346, 21, 11, 169, '
  Seunuddon (
  Seunudon)
  ', '
  Kecamatan
  ', ''),
(4347, 21, 11, 170, '
  Simpang
  Kramat (
  Keramat)
  ', '
  Kecamatan
  ', ''),
(4348, 21, 11, 171, '
  Syamtalira
  Aron
  ', '
  Kecamatan
  ', ''),
(4349, 21, 11, 172, '
  Syamtalira
  Bayu
  ', '
  Kecamatan
  ', ''),
(4350, 21, 11, 173, '
  Tanah
  Jambo
  Aye
  ', '
  Kecamatan
  ', ''),
(4351, 21, 11, 174, '
  Tanah
  Luas
  ', '
  Kecamatan
  ', ''),
(4352, 21, 11, 175, '
  Tanah
  Pasir
  ', '
  Kecamatan
  ', ''),
(4353, 21, 20, 278, '
  Baiturrahman
  ', '
  Kecamatan
  ', ''),
(4354, 21, 20, 279, '
  Banda
  Raya
  ', '
  Kecamatan
  ', ''),
(4355, 21, 20, 280, '
  Jaya
  Baru
  ', '
  Kecamatan
  ', ''),
(4356, 21, 20, 281, '
  Kuta
  Alam
  ', '
  Kecamatan
  ', ''),
(4357, 21, 20, 282, '
  Kuta
  Raja
  ', '
  Kecamatan
  ', ''),
(4358, 21, 20, 283, '
  Lueng
  Bata
  ', '
  Kecamatan
  ', ''),
(4359, 21, 20, 284, '
  Meuraxa
  ', '
  Kecamatan
  ', ''),
(4360, 21, 20, 285, '
  Syiah
  Kuala
  ', '
  Kecamatan
  ', ''),
(4361, 21, 20, 286, '
  Ulee
  Kareng
  ', '
  Kecamatan
  ', ''),
(4362, 21, 59, 797, '
  Bandar
  ', '
  Kecamatan
  ', ''),
(4363, 21, 59, 798, '
  Bener
  Kelipah
  ', '
  Kecamatan
  ', ''),
(4364, 21, 59, 799, '
  Bukit
  ', '
  Kecamatan
  ', ''),
(4365, 21, 59, 800, '
  Gajah
  Putih
  ', '
  Kecamatan
  ', ''),
(4366, 21, 59, 801, '
  Mesidah
  ', '
  Kecamatan
  ', ''),
(4367, 21, 59, 802, '
  Permata
  ', '
  Kecamatan
  ', ''),
(4368, 21, 59, 803, '
  Pintu
  Rime
  Gayo
  ', '
  Kecamatan
  ', ''),
(4369, 21, 59, 804, '
  Syiah
  Utama
  ', '
  Kecamatan
  ', ''),
(4370, 21, 59, 805, '
  Timang
  Gajah
  ', '
  Kecamatan
  ', ''),
(4371, 21, 59, 806, '
  Wih
  Pesam
  ', '
  Kecamatan
  ', ''),
(4372, 21, 72, 949, '
  Ganda
  Pura
  ', '
  Kecamatan
  ', ''),
(4373, 21, 72, 950, '
  Jangka
  ', '
  Kecamatan
  ', ''),
(4374, 21, 72, 951, '
  Jeumpa
  ', '
  Kecamatan
  ', ''),
(4375, 21, 72, 952, '
  Jeunieb
  ', '
  Kecamatan
  ', ''),
(4376, 21, 72, 953, '
  Juli
  ', '
  Kecamatan
  ', ''),
(4377, 21, 72, 954, '
  Kota
  Juang
  ', '
  Kecamatan
  ', ''),
(4378, 21, 72, 955, '
  Kuala
  ', '
  Kecamatan
  ', ''),
(4379, 21, 72, 956, '
  Kuta
  Blang
  ', '
  Kecamatan
  ', ''),
(4380, 21, 72, 957, '
  Makmur
  ', '
  Kecamatan
  ', ''),
(4381, 21, 72, 958, '
  Pandrah
  ', '
  Kecamatan
  ', ''),
(4382, 21, 72, 959, '
  Peudada
  ', '
  Kecamatan
  ', ''),
(4383, 21, 72, 960, '
  Peulimbang (
  Plimbang)
  ', '
  Kecamatan
  ', ''),
(4384, 21, 72, 961, '
  Peusangan
  ', '
  Kecamatan
  ', ''),
(4385, 21, 72, 962, '
  Peusangan
  Selatan
  ', '
  Kecamatan
  ', ''),
(4386, 21, 72, 963, '
  Peusangan
  Siblah
  Krueng
  ', '
  Kecamatan
  ', ''),
(4387, 21, 72, 964, '
  Samalanga
  ', '
  Kecamatan
  ', ''),
(4388, 21, 72, 965, '
  Simpang
  Mamplam
  ', '
  Kecamatan
  ', ''),
(4389, 21, 127, 1753, '
  Blang
  Jerango
  ', '
  Kecamatan
  ', ''),
(4390, 21, 127, 1754, '
  Blang
  Kejeren
  ', '
  Kecamatan
  ', ''),
(4391, 21, 127, 1755, '
  Blang
  Pegayon
  ', '
  Kecamatan
  ', ''),
(4392, 21, 127, 1756, '
  Dabun
  Gelang (
  Debun
  Gelang)
  ', '
  Kecamatan
  ', ''),
(4393, 21, 127, 1757, '
  Kuta
  Panjang
  ', '
  Kecamatan
  ', ''),
(4394, 21, 127, 1758, '
  Pantan
  Cuaca
  ', '
  Kecamatan
  ', ''),
(4395, 21, 127, 1759, '
  Pining (
  Pinding)
  ', '
  Kecamatan
  ', ''),
(4396, 21, 127, 1760, '
  Putri
  Betung
  ', '
  Kecamatan
  ', ''),
(4397, 21, 127, 1761, '
  Rikit
  Gaib
  ', '
  Kecamatan
  ', ''),
(4398, 21, 127, 1762, '
  Terangun (
  Terangon)
  ', '
  Kecamatan
  ', ''),
(4399, 21, 127, 1763, '
  Teripe
  /
  Tripe
  Jaya
  ', '
  Kecamatan
  ', ''),
(4400, 21, 230, 3283, '
  Langsa
  Barat
  ', '
  Kecamatan
  ', ''),
(4401, 21, 230, 3284, '
  Langsa
  Baro
  ', '
  Kecamatan
  ', ''),
(4402, 21, 230, 3285, '
  Langsa
  Kota
  ', '
  Kecamatan
  ', ''),
(4403, 21, 230, 3286, '
  Langsa
  Lama
  ', '
  Kecamatan
  ', ''),
(4404, 21, 230, 3287, '
  Langsa
  Timur
  ', '
  Kecamatan
  ', ''),
(4405, 21, 235, 3347, '
  Banda
  Sakti
  ', '
  Kecamatan
  ', ''),
(4406, 21, 235, 3348, '
  Blang
  Mangat
  ', '
  Kecamatan
  ', ''),
(4407, 21, 235, 3349, '
  Muara
  Dua
  ', '
  Kecamatan
  ', ''),
(4408, 21, 235, 3350, '
  Muara
  Satu
  ', '
  Kecamatan
  ', ''),
(4409, 21, 300, 4250, '
  Beutong
  ', '
  Kecamatan
  ', ''),
(4410, 21, 300, 4251, '
  Beutong
  Ateuh
  Banggalang
  ', '
  Kecamatan
  ', ''),
(4411, 21, 300, 4252, '
  Darul
  Makmur
  ', '
  Kecamatan
  ', ''),
(4412, 21, 300, 4253, '
  Kuala
  ', '
  Kecamatan
  ', ''),
(4413, 21, 300, 4254, '
  Kuala
  Pesisir
  ', '
  Kecamatan
  ', ''),
(4414, 21, 300, 4255, '
  Seunagan
  ', '
  Kecamatan
  ', ''),
(4415, 21, 300, 4256, '
  Seunagan
  Timur
  ', '
  Kecamatan
  ', ''),
(4416, 21, 300, 4257, '
  Suka
  Makmue
  ', '
  Kecamatan
  ', ''),
(4417, 21, 300, 4258, '
  Tadu
  Raya
  ', '
  Kecamatan
  ', ''),
(4418, 21, 300, 4259, '
  Tripa
  Makmur
  ', '
  Kecamatan
  ', ''),
(4419, 21, 358, 4999, '
  Batee
  ', '
  Kecamatan
  ', ''),
(4420, 21, 358, 5000, '
  Delima
  ', '
  Kecamatan
  ', ''),
(4421, 21, 358, 5001, '
  Geumpang
  ', '
  Kecamatan
  ', ''),
(4422, 21, 358, 5002, '
  Glumpang
  Baro
  ', '
  Kecamatan
  ', ''),
(4423, 21, 358, 5003, '
  Glumpang
  Tiga (
  Geulumpang
  Tiga)
  ', '
  Kecamatan
  ', ''),
(4424, 21, 358, 5004, '
  Grong
  Grong
  ', '
  Kecamatan
  ', ''),
(4425, 21, 358, 5005, '
  Indrajaya
  ', '
  Kecamatan
  ', ''),
(4426, 21, 358, 5006, '
  Kembang
  Tanjong (
  Tanjung)
  ', '
  Kecamatan
  ', ''),
(4427, 21, 358, 5007, '
  Keumala
  ', '
  Kecamatan
  ', ''),
(4428, 21, 358, 5008, '
  Kota
  Sigli
  ', '
  Kecamatan
  ', ''),
(4429, 21, 358, 5009, '
  Mane
  ', '
  Kecamatan
  ', ''),
(4430, 21, 358, 5010, '
  Mila
  ', '
  Kecamatan
  ', ''),
(4431, 21, 358, 5011, '
  Muara
  Tiga
  ', '
  Kecamatan
  ', ''),
(4432, 21, 358, 5012, '
  Mutiara
  ', '
  Kecamatan
  ', ''),
(4433, 21, 358, 5013, '
  Mutiara
  Timur
  ', '
  Kecamatan
  ', ''),
(4434, 21, 358, 5014, '
  Padang
  Tiji
  ', '
  Kecamatan
  ', ''),
(4435, 21, 358, 5015, '
  Peukan
  Baro
  ', '
  Kecamatan
  ', ''),
(4436, 21, 358, 5016, '
  Pidie
  ', '
  Kecamatan
  ', ''),
(4437, 21, 358, 5017, '
  Sakti
  ', '
  Kecamatan
  ', ''),
(4438, 21, 358, 5018, '
  Simpang
  Tiga
  ', '
  Kecamatan
  ', ''),
(4439, 21, 358, 5019, '
  Tangse
  ', '
  Kecamatan
  ', ''),
(4440, 21, 358, 5020, '
  Tiro
  /
  Truseb
  ', '
  Kecamatan
  ', ''),
(4441, 21, 358, 5021, '
  Titeue
  ', '
  Kecamatan
  ', ''),
(4442, 21, 359, 5022, '
  Bandar
  Baru
  ', '
  Kecamatan
  ', ''),
(4443, 21, 359, 5023, '
  Bandar
  Dua
  ', '
  Kecamatan
  ', ''),
(4444, 21, 359, 5024, '
  Jangka
  Buya
  ', '
  Kecamatan
  ', ''),
(4445, 21, 359, 5025, '
  Meurah
  Dua
  ', '
  Kecamatan
  ', ''),
(4446, 21, 359, 5026, '
  Meureudu
  ', '
  Kecamatan
  ', ''),
(4447, 21, 359, 5027, '
  Panteraja
  ', '
  Kecamatan
  ', ''),
(4448, 21, 359, 5028, '
  Trienggadeng
  ', '
  Kecamatan
  ', ''),
(4449, 21, 359, 5029, '
  Ulim
  ', '
  Kecamatan
  ', ''),
(4450, 21, 384, 5344, '
  Sukajaya
  ', '
  Kecamatan
  ', ''),
(4451, 21, 384, 5345, '
  Sukakarya
  ', '
  Kecamatan
  ', ''),
(4452, 21, 414, 5724, '
  Alapan (
  Alafan)
  ', '
  Kecamatan
  ', ''),
(4453, 21, 414, 5725, '
  Salang
  ', '
  Kecamatan
  ', ''),
(4454, 21, 414, 5726, '
  Simeuleu
  Barat
  ', '
  Kecamatan
  ', ''),
(4455, 21, 414, 5727, '
  Simeuleu
  Tengah
  ', '
  Kecamatan
  ', ''),
(4456, 21, 414, 5728, '
  Simeuleu
  Timur
  ', '
  Kecamatan
  ', ''),
(4457, 21, 414, 5729, '
  Simeulue
  Cut
  ', '
  Kecamatan
  ', ''),
(4458, 21, 414, 5730, '
  Teluk
  Dalam
  ', '
  Kecamatan
  ', ''),
(4459, 21, 414, 5731, '
  Teupah
  Barat
  ', '
  Kecamatan
  ', ''),
(4460, 21, 414, 5732, '
  Teupah
  Selatan
  ', '
  Kecamatan
  ', ''),
(4461, 21, 414, 5733, '
  Teupah
  Tengah
  ', '
  Kecamatan
  ', ''),
(4462, 21, 429, 5913, '
  Longkib
  ', '
  Kecamatan
  ', ''),
(4463, 21, 429, 5914, '
  Penanggalan
  ', '
  Kecamatan
  ', ''),
(4464, 21, 429, 5915, '
  Rundeng
  ', '
  Kecamatan
  ', ''),
(4465, 21, 429, 5916, '
  Simpang
  Kiri
  ', '
  Kecamatan
  ', ''),
(4466, 21, 429, 5917, '
  Sultan
  Daulat
  ', '
  Kecamatan
  ', ''),
(4467, 34, 229, 3260, '
  Babalan
  ', '
  Kecamatan
  ', ''),
(4468, 34, 229, 3261, '
  Bahorok
  ', '
  Kecamatan
  ', ''),
(4469, 34, 229, 3262, '
  Batang
  Serangan
  ', '
  Kecamatan
  ', ''),
(4470, 34, 229, 3263, '
  Besitang
  ', '
  Kecamatan
  ', ''),
(4471, 34, 229, 3264, '
  Binjai
  ', '
  Kecamatan
  ', ''),
(4472, 34, 229, 3265, '
  Brandan
  Barat
  ', '
  Kecamatan
  ', ''),
(4473, 34, 229, 3266, '
  Gebang
  ', '
  Kecamatan
  ', ''),
(4474, 34, 229, 3267, '
  Hinai
  ', '
  Kecamatan
  ', ''),
(4475, 34, 229, 3268, '
  Kuala
  ', '
  Kecamatan
  ', ''),
(4476, 34, 229, 3269, '
  Kutambaru
  ', '
  Kecamatan
  ', ''),
(4477, 34, 229, 3270, '
  Padang
  Tualang
  ', '
  Kecamatan
  ', ''),
(4478, 34, 229, 3271, '
  Pangkalan
  Susu
  ', '
  Kecamatan
  ', ''),
(4479, 34, 229, 3272, '
  Pematang
  Jaya
  ', '
  Kecamatan
  ', ''),
(4480, 34, 229, 3273, '
  Salapian
  ', '
  Kecamatan
  ', ''),
(4481, 34, 229, 3274, '
  Sawit
  Seberang
  ', '
  Kecamatan
  ', ''),
(4482, 34, 229, 3275, '
  Secanggang
  ', '
  Kecamatan
  ', ''),
(4483, 34, 229, 3276, '
  Sei
  Binge (
  Bingai)
  ', '
  Kecamatan
  ', ''),
(4484, 34, 229, 3277, '
  Sei
  Lepan
  ', '
  Kecamatan
  ', ''),
(4485, 34, 229, 3278, '
  Selesai
  ', '
  Kecamatan
  ', ''),
(4486, 34, 229, 3279, '
  Sirapit (
  Serapit)
  ', '
  Kecamatan
  ', ''),
(4487, 34, 229, 3280, '
  Stabat
  ', '
  Kecamatan
  ', ''),
(4488, 34, 229, 3281, '
  Tanjungpura
  ', '
  Kecamatan
  ', ''),
(4489, 34, 229, 3282, '
  Wampu
  ', '
  Kecamatan
  ', ''),
(4490, 34, 52, 711, '
  Air
  Putih
  ', '
  Kecamatan
  ', ''),
(4491, 34, 52, 712, '
  Limapuluh
  ', '
  Kecamatan
  ', ''),
(4492, 34, 52, 713, '
  Medang
  Deras
  ', '
  Kecamatan
  ', ''),
(4493, 34, 52, 714, '
  Sei
  Balai
  ', '
  Kecamatan
  ', ''),
(4494, 34, 52, 715, '
  Sei
  Suka
  ', '
  Kecamatan
  ', ''),
(4495, 34, 52, 716, '
  Talawi
  ', '
  Kecamatan
  ', ''),
(4496, 34, 52, 717, '
  Tanjung
  Tiram
  ', '
  Kecamatan
  ', ''),
(4497, 34, 15, 214, '
  Aek
  Kuasan
  ', '
  Kecamatan
  ', ''),
(4498, 34, 15, 215, '
  Aek
  Ledong
  ', '
  Kecamatan
  ', ''),
(4499, 34, 15, 216, '
  Aek
  Songsongan
  ', '
  Kecamatan
  ', ''),
(4500, 34, 15, 217, '
  Air
  Batu
  ', '
  Kecamatan
  ', ''),
(4501, 34, 15, 218, '
  Air
  Joman
  ', '
  Kecamatan
  ', ''),
(4502, 34, 15, 219, '
  Bandar
  Pasir
  Mandoge
  ', '
  Kecamatan
  ', ''),
(4503, 34, 15, 220, '
  Bandar
  Pulau
  ', '
  Kecamatan
  ', ''),
(4504, 34, 15, 221, '
  Buntu
  Pane
  ', '
  Kecamatan
  ', ''),
(4505, 34, 15, 222, '
  Kisaran
  Barat
  Kota
  ', '
  Kecamatan
  ', ''),
(4506, 34, 15, 223, '
  Kisaran
  Timur
  Kota
  ', '
  Kecamatan
  ', ''),
(4507, 34, 15, 224, '
  Meranti
  ', '
  Kecamatan
  ', ''),
(4508, 34, 15, 225, '
  Pulau
  Rakyat
  ', '
  Kecamatan
  ', ''),
(4509, 34, 15, 226, '
  Pulo
  Bandring
  ', '
  Kecamatan
  ', ''),
(4510, 34, 15, 227, '
  Rahuning
  ', '
  Kecamatan
  ', ''),
(4511, 34, 15, 228, '
  Rawang
  Panca
  Arga
  ', '
  Kecamatan
  ', ''),
(4512, 34, 15, 229, '
  Sei
  Dadap
  ', '
  Kecamatan
  ', ''),
(4513, 34, 15, 230, '
  Sei
  Kepayang
  ', '
  Kecamatan
  ', ''),
(4514, 34, 15, 231, '
  Sei
  Kepayang
  Barat
  ', '
  Kecamatan
  ', ''),
(4515, 34, 15, 232, '
  Sei
  Kepayang
  Timur
  ', '
  Kecamatan
  ', ''),
(4516, 34, 15, 233, '
  Setia
  Janji
  ', '
  Kecamatan
  ', ''),
(4517, 34, 15, 234, '
  Silau
  Laut
  ', '
  Kecamatan
  ', ''),
(4518, 34, 15, 235, '
  Simpang
  Empat
  ', '
  Kecamatan
  ', ''),
(4519, 34, 15, 236, '
  Tanjung
  Balai
  ', '
  Kecamatan
  ', ''),
(4520, 34, 15, 237, '
  Teluk
  Dalam
  ', '
  Kecamatan
  ', ''),
(4521, 34, 15, 238, '
  Tinggi
  Raja
  ', '
  Kecamatan
  ', ''),
(4522, 34, 110, 1517, '
  Berampu (
  Brampu)
  ', '
  Kecamatan
  ', ''),
(4523, 34, 110, 1518, '
  Gunung
  Sitember
  ', '
  Kecamatan
  ', ''),
(4524, 34, 110, 1519, '
  Lae
  Parira
  ', '
  Kecamatan
  ', ''),
(4525, 34, 110, 1520, '
  Parbuluan
  ', '
  Kecamatan
  ', ''),
(4526, 34, 110, 1521, '
  Pegagan
  Hilir
  ', '
  Kecamatan
  ', ''),
(4527, 34, 110, 1522, '
  Sidikalang
  ', '
  Kecamatan
  ', ''),
(4528, 34, 110, 1523, '
  Siempat
  Nempu
  ', '
  Kecamatan
  ', ''),
(4529, 34, 110, 1524, '
  Siempat
  Nempu
  Hilir
  ', '
  Kecamatan
  ', ''),
(4530, 34, 110, 1525, '
  Siempat
  Nempu
  Hulu
  ', '
  Kecamatan
  ', ''),
(4531, 34, 110, 1526, '
  Silahi
  Sabungan
  ', '
  Kecamatan
  ', ''),
(4532, 34, 110, 1527, '
  Silima
  Pungga
  -
  Pungga
  ', '
  Kecamatan
  ', ''),
(4533, 34, 110, 1528, '
  Sitinjo
  ', '
  Kecamatan
  ', ''),
(4534, 34, 110, 1529, '
  Sumbul
  ', '
  Kecamatan
  ', ''),
(4535, 34, 110, 1530, '
  Tanah
  Pinem
  ', '
  Kecamatan
  ', ''),
(4536, 34, 110, 1531, '
  Tiga
  Lingga
  ', '
  Kecamatan
  ', ''),
(4537, 34, 112, 1537, '
  Bangun
  Purba
  ', '
  Kecamatan
  ', ''),
(4538, 34, 112, 1538, '
  Batang
  Kuis
  ', '
  Kecamatan
  ', ''),
(4539, 34, 112, 1539, '
  Beringin
  ', '
  Kecamatan
  ', ''),
(4540, 34, 112, 1540, '
  Biru
  -
  Biru
  ', '
  Kecamatan
  ', ''),
(4541, 34, 112, 1541, '
  Deli
  Tua
  ', '
  Kecamatan
  ', ''),
(4542, 34, 112, 1542, '
  Galang
  ', '
  Kecamatan
  ', ''),
(4543, 34, 112, 1543, '
  Gunung
  Meriah
  ', '
  Kecamatan
  ', ''),
(4544, 34, 112, 1544, '
  Hamparan
  Perak
  ', '
  Kecamatan
  ', ''),
(4545, 34, 112, 1545, '
  Kutalimbaru
  ', '
  Kecamatan
  ', ''),
(4546, 34, 112, 1546, '
  Labuhan
  Deli
  ', '
  Kecamatan
  ', ''),
(4547, 34, 112, 1547, '
  Lubuk
  Pakam
  ', '
  Kecamatan
  ', ''),
(4548, 34, 112, 1548, '
  Namo
  Rambe
  ', '
  Kecamatan
  ', ''),
(4549, 34, 112, 1549, '
  Pagar
  Merbau
  ', '
  Kecamatan
  ', ''),
(4550, 34, 112, 1550, '
  Pancur
  Batu
  ', '
  Kecamatan
  ', ''),
(4551, 34, 112, 1551, '
  Pantai
  Labu
  ', '
  Kecamatan
  ', ''),
(4552, 34, 112, 1552, '
  Patumbak
  ', '
  Kecamatan
  ', ''),
(4553, 34, 112, 1553, '
  Percut
  Sei
  Tuan
  ', '
  Kecamatan
  ', ''),
(4554, 34, 112, 1554, '
  Sibolangit
  ', '
  Kecamatan
  ', ''),
(4555, 34, 112, 1555, '
  Sinembah
  Tanjung
  Muda
  Hilir
  ', '
  Kecamatan
  ', ''),
(4556, 34, 112, 1556, '
  Sinembah
  Tanjung
  Muda
  Hulu
  ', '
  Kecamatan
  ', ''),
(4557, 34, 112, 1557, '
  Sunggal
  ', '
  Kecamatan
  ', ''),
(4558, 34, 112, 1558, '
  Tanjung
  Morawa
  ', '
  Kecamatan
  ', ''),
(4559, 34, 146, 2006, '
  Bakti
  Raja
  ', '
  Kecamatan
  ', ''),
(4560, 34, 146, 2007, '
  Dolok
  Sanggul
  ', '
  Kecamatan
  ', ''),
(4561, 34, 146, 2008, '
  Lintong
  Nihuta
  ', '
  Kecamatan
  ', ''),
(4562, 34, 146, 2009, '
  Onan
  Ganjang
  ', '
  Kecamatan
  ', ''),
(4563, 34, 146, 2010, '
  Pakkat
  ', '
  Kecamatan
  ', ''),
(4564, 34, 146, 2011, '
  Paranginan
  ', '
  Kecamatan
  ', ''),
(4565, 34, 146, 2012, '
  Parlilitan
  ', '
  Kecamatan
  ', ''),
(4566, 34, 146, 2013, '
  Pollung
  ', '
  Kecamatan
  ', ''),
(4567, 34, 146, 2014, '
  Sijama
  Polang
  ', '
  Kecamatan
  ', ''),
(4568, 34, 146, 2015, '
  Tara
  Bintang
  ', '
  Kecamatan
  ', ''),
(4569, 34, 173, 2420, '
  Barus
  Jahe
  ', '
  Kecamatan
  ', ''),
(4570, 34, 173, 2421, '
  Brastagi (
  Berastagi)
  ', '
  Kecamatan
  ', ''),
(4571, 34, 173, 2422, '
  Dolat
  Rayat
  ', '
  Kecamatan
  ', ''),
(4572, 34, 173, 2423, '
  Juhar
  ', '
  Kecamatan
  ', ''),
(4573, 34, 173, 2424, '
  Kabanjahe
  ', '
  Kecamatan
  ', ''),
(4574, 34, 173, 2425, '
  Kuta
  Buluh
  ', '
  Kecamatan
  ', ''),
(4575, 34, 173, 2426, '
  Laubaleng
  ', '
  Kecamatan
  ', ''),
(4576, 34, 173, 2427, '
  Mardinding
  ', '
  Kecamatan
  ', ''),
(4577, 34, 173, 2428, '
  Merdeka
  ', '
  Kecamatan
  ', ''),
(4578, 34, 173, 2429, '
  Merek
  ', '
  Kecamatan
  ', ''),
(4579, 34, 173, 2430, '
  Munte
  ', '
  Kecamatan
  ', ''),
(4580, 34, 173, 2431, '
  Nama
  Teran
  ', '
  Kecamatan
  ', ''),
(4581, 34, 173, 2432, '
  Payung
  ', '
  Kecamatan
  ', ''),
(4582, 34, 173, 2433, '
  Simpang
  Empat
  ', '
  Kecamatan
  ', ''),
(4583, 34, 173, 2434, '
  Tiga
  Binanga
  ', '
  Kecamatan
  ', ''),
(4584, 34, 173, 2435, '
  Tiga
  Panah
  ', '
  Kecamatan
  ', ''),
(4585, 34, 173, 2436, '
  Tiganderket
  ', '
  Kecamatan
  ', ''),
(4586, 34, 217, 3061, '
  Bilah
  Barat
  ', '
  Kecamatan
  ', ''),
(4587, 34, 217, 3062, '
  Bilah
  Hilir
  ', '
  Kecamatan
  ', ''),
(4588, 34, 217, 3063, '
  Bilah
  Hulu
  ', '
  Kecamatan
  ', ''),
(4589, 34, 217, 3064, '
  Panai
  Hilir
  ', '
  Kecamatan
  ', ''),
(4590, 34, 217, 3065, '
  Panai
  Hulu
  ', '
  Kecamatan
  ', ''),
(4591, 34, 217, 3066, '
  Panai
  Tengah
  ', '
  Kecamatan
  ', ''),
(4592, 34, 217, 3067, '
  Pangkatan
  ', '
  Kecamatan
  ', ''),
(4593, 34, 217, 3068, '
  Rantau
  Selatan
  ', '
  Kecamatan
  ', ''),
(4594, 34, 217, 3069, '
  Rantau
  Utara
  ', '
  Kecamatan
  ', ''),
(4595, 34, 218, 3070, '
  Kampung
  Rakyat
  ', '
  Kecamatan
  ', ''),
(4596, 34, 218, 3071, '
  Kota
  Pinang
  ', '
  Kecamatan
  ', ''),
(4597, 34, 218, 3072, '
  Sei
  /
  Sungai
  Kanan
  ', '
  Kecamatan
  ', ''),
(4598, 34, 218, 3073, '
  Silangkitang
  ', '
  Kecamatan
  ', ''),
(4599, 34, 218, 3074, '
  Torgamba
  ', '
  Kecamatan
  ', ''),
(4600, 34, 219, 3075, '
  Aek
  Kuo
  ', '
  Kecamatan
  ', ''),
(4601, 34, 219, 3076, '
  Aek
  Natas
  ', '
  Kecamatan
  ', ''),
(4602, 34, 219, 3077, '
  Kuala
  Ledong (
  Kualuh
  Leidong)
  ', '
  Kecamatan
  ', ''),
(4603, 34, 219, 3078, '
  Kualuh
  Hilir
  ', '
  Kecamatan
  ', ''),
(4604, 34, 219, 3079, '
  Kualuh
  Hulu
  ', '
  Kecamatan
  ', ''),
(4605, 34, 219, 3080, '
  Kualuh
  Selatan
  ', '
  Kecamatan
  ', ''),
(4606, 34, 219, 3081, '
  Marbau
  ', '
  Kecamatan
  ', ''),
(4607, 34, 219, 3082, '
  Na
  IX
  -
  X', '
  Kecamatan
  ', ''),
(4608, 34, 268, 3779, '
  Batahan
  ', '
  Kecamatan
  ', ''),
(4609, 34, 268, 3780, '
  Batang
  Natal
  ', '
  Kecamatan
  ', ''),
(4610, 34, 268, 3781, '
  Bukit
  Malintang
  ', '
  Kecamatan
  ', ''),
(4611, 34, 268, 3782, '
  Huta
  Bargot
  ', '
  Kecamatan
  ', ''),
(4612, 34, 268, 3783, '
  Kotanopan
  ', '
  Kecamatan
  ', ''),
(4613, 34, 268, 3784, '
  Langga
  Bayu (
  Lingga
  Bayu)
  ', '
  Kecamatan
  ', ''),
(4614, 34, 268, 3785, '
  Lembah
  Sorik
  Merapi
  ', '
  Kecamatan
  ', ''),
(4615, 34, 268, 3786, '
  Muara
  Batang
  Gadis
  ', '
  Kecamatan
  ', ''),
(4616, 34, 268, 3787, '
  Muara
  Sipongi
  ', '
  Kecamatan
  ', ''),
(4617, 34, 268, 3788, '
  Naga
  Juang
  ', '
  Kecamatan
  ', ''),
(4618, 34, 268, 3789, '
  Natal
  ', '
  Kecamatan
  ', ''),
(4619, 34, 268, 3790, '
  Pakantan
  ', '
  Kecamatan
  ', ''),
(4620, 34, 268, 3791, '
  Panyabungan
  Barat
  ', '
  Kecamatan
  ', ''),
(4621, 34, 268, 3792, '
  Panyabungan
  Kota
  ', '
  Kecamatan
  ', ''),
(4622, 34, 268, 3793, '
  Panyabungan
  Selatan
  ', '
  Kecamatan
  ', ''),
(4623, 34, 268, 3794, '
  Panyabungan
  Timur
  ', '
  Kecamatan
  ', ''),
(4624, 34, 268, 3795, '
  Panyabungan
  Utara
  ', '
  Kecamatan
  ', ''),
(4625, 34, 268, 3796, '
  Puncak
  Sorik
  Marapi
  /
  Merapi
  ', '
  Kecamatan
  ', ''),
(4626, 34, 268, 3797, '
  Ranto
  Baek
  /
  Baik
  ', '
  Kecamatan
  ', ''),
(4627, 34, 268, 3798, '
  Siabu
  ', '
  Kecamatan
  ', ''),
(4628, 34, 268, 3799, '
  Sinunukan
  ', '
  Kecamatan
  ', ''),
(4629, 34, 268, 3800, '
  Tambangan
  ', '
  Kecamatan
  ', ''),
(4630, 34, 268, 3801, '
  Ulu
  Pungkut
  ', '
  Kecamatan
  ', ''),
(4631, 34, 307, 4362, '
  Bawolato
  ', '
  Kecamatan
  ', ''),
(4632, 34, 307, 4363, '
  Botomuzoi
  ', '
  Kecamatan
  ', ''),
(4633, 34, 307, 4364, '
  Gido
  ', '
  Kecamatan
  ', ''),
(4634, 34, 307, 4365, '
  Hili
  Serangkai (
  Hilisaranggu)
  ', '
  Kecamatan
  ', ''),
(4635, 34, 307, 4366, '
  Hiliduho
  ', '
  Kecamatan
  ', ''),
(4636, 34, 307, 4367, '
  Idano
  Gawo
  ', '
  Kecamatan
  ', ''),
(4637, 34, 307, 4368, '
  Ma
  \
  'u',
  'Kecamatan',
  ''
)
,
(
4638
,
34
,
307
,
4369
,
'Sogae Adu (Sogaeadu)'
,
'Kecamatan'
,
''
)
,
(
4639
,
34
,
307
,
4370
,
'Somolo-Molo (Samolo)'
,
'Kecamatan'
,
''
)
,
(
4640
,
34
,
307
,
4371
,
'Ulugawo'
,
'Kecamatan'
,
''
)
,
(
4641
,
34
,
308
,
4372
,
'Lahomi (Gahori)'
,
'Kecamatan'
,
''
)
,
(
4642
,
34
,
308
,
4373
,
'Lolofitu Moi'
,
'Kecamatan'
,
''
)
,
(
4643
,
34
,
308
,
4374
,
'Mandrehe'
,
'Kecamatan'
,
''
)
,
(
4644
,
34
,
308
,
4375
,
'Mandrehe Barat'
,
'Kecamatan'
,
''
)
,
(
4645
,
34
,
308
,
4376
,
'Mandrehe Utara'
,
'Kecamatan'
,
''
)
,
(
4646
,
34
,
308
,
4377
,
'Moro\'
o
', '
Kecamatan
', ''),
(4647, 34, 308, 4378, '
Sirombu
', '
Kecamatan
', ''),
(4648, 34, 308, 4379, '
Ulu
Moro
\
'o (Ulu Narwo)'
,
'Kecamatan'
,
''
)
,
(
4649
,
34
,
309
,
4380
,
'Amandraya'
,
'Kecamatan'
,
''
)
,
(
4650
,
34
,
309
,
4381
,
'Aramo'
,
'Kecamatan'
,
''
)
,
(
4651
,
34
,
309
,
4382
,
'Boronadu'
,
'Kecamatan'
,
''
)
,
(
4652
,
34
,
309
,
4383
,
'Fanayama'
,
'Kecamatan'
,
''
)
,
(
4653
,
34
,
309
,
4384
,
'Gomo'
,
'Kecamatan'
,
''
)
,
(
4654
,
34
,
309
,
4385
,
'Hibala'
,
'Kecamatan'
,
''
)
,
(
4655
,
34
,
309
,
4386
,
'Hilimegai'
,
'Kecamatan'
,
''
)
,
(
4656
,
34
,
309
,
4387
,
'Hilisalawa\'
ahe
(
Hilisalawaahe
)
', '
Kecamatan
', ''),
(4657, 34, 309, 4388, '
Huruna
', '
Kecamatan
', ''),
(4658, 34, 309, 4389, '
Lahusa
', '
Kecamatan
', ''),
(4659, 34, 309, 4390, '
Lolomatua
', '
Kecamatan
', ''),
(4660, 34, 309, 4391, '
Lolowau
', '
Kecamatan
', ''),
(4661, 34, 309, 4392, '
Maniamolo
', '
Kecamatan
', ''),
(4662, 34, 309, 4393, '
Mazino
', '
Kecamatan
', ''),
(4663, 34, 309, 4394, '
Mazo
', '
Kecamatan
', ''),
(4664, 34, 309, 4395, '
O
\
'o\'
u
(
Oou
)
', '
Kecamatan
', ''),
(4665, 34, 309, 4396, '
Onohazumba
', '
Kecamatan
', ''),
(4666, 34, 309, 4397, '
Pulau
-
Pulau
Batu
', '
Kecamatan
', ''),
(4667, 34, 309, 4398, '
Pulau
-
Pulau
Batu
Barat
', '
Kecamatan
', ''),
(4668, 34, 309, 4399, '
Pulau
-
Pulau
Batu
Timur
', '
Kecamatan
', ''),
(4669, 34, 309, 4400, '
Pulau
-
Pulau
Batu
Utara
', '
Kecamatan
', ''),
(4670, 34, 309, 4401, '
Sidua
\
'ori'
,
'Kecamatan'
,
''
)
,
(
4671
,
34
,
309
,
4402
,
'Simuk'
,
'Kecamatan'
,
''
)
,
(
4672
,
34
,
309
,
4403
,
'Somambawa'
,
'Kecamatan'
,
''
)
,
(
4673
,
34
,
309
,
4404
,
'Susua'
,
'Kecamatan'
,
''
)
,
(
4674
,
34
,
309
,
4405
,
'Tanah Masa'
,
'Kecamatan'
,
''
)
,
(
4675
,
34
,
309
,
4406
,
'Teluk Dalam'
,
'Kecamatan'
,
''
)
,
(
4676
,
34
,
309
,
4407
,
'Toma'
,
'Kecamatan'
,
''
)
,
(
4677
,
34
,
309
,
4408
,
'Ulunoyo'
,
'Kecamatan'
,
''
)
,
(
4678
,
34
,
309
,
4409
,
'Ulususua'
,
'Kecamatan'
,
''
)
,
(
4679
,
34
,
309
,
4410
,
'Umbunasi'
,
'Kecamatan'
,
''
)
,
(
4680
,
34
,
310
,
4411
,
'Afulu'
,
'Kecamatan'
,
''
)
,
(
4681
,
34
,
310
,
4412
,
'Alasa'
,
'Kecamatan'
,
''
)
,
(
4682
,
34
,
310
,
4413
,
'Alasa Talumuzoi'
,
'Kecamatan'
,
''
)
,
(
4683
,
34
,
310
,
4414
,
'Lahewa'
,
'Kecamatan'
,
''
)
,
(
4684
,
34
,
310
,
4415
,
'Lahewa Timur'
,
'Kecamatan'
,
''
)
,
(
4685
,
34
,
310
,
4416
,
'Lotu'
,
'Kecamatan'
,
''
)
,
(
4686
,
34
,
310
,
4417
,
'Namohalu Esiwa'
,
'Kecamatan'
,
''
)
,
(
4687
,
34
,
310
,
4418
,
'Sawo'
,
'Kecamatan'
,
''
)
,
(
4688
,
34
,
310
,
4419
,
'Sitolu Ori'
,
'Kecamatan'
,
''
)
,
(
4689
,
34
,
310
,
4420
,
'Tugala Oyo'
,
'Kecamatan'
,
''
)
,
(
4690
,
34
,
310
,
4421
,
'Tuhemberua'
,
'Kecamatan'
,
''
)
,
(
4691
,
34
,
319
,
4545
,
'Aek Nabara Barumun'
,
'Kecamatan'
,
''
)
,
(
4692
,
34
,
319
,
4546
,
'Barumun'
,
'Kecamatan'
,
''
)
,
(
4693
,
34
,
319
,
4547
,
'Barumun Selatan'
,
'Kecamatan'
,
''
)
,
(
4694
,
34
,
319
,
4548
,
'Barumun Tengah'
,
'Kecamatan'
,
''
)
,
(
4695
,
34
,
319
,
4549
,
'Batang Lubu Sutam'
,
'Kecamatan'
,
''
)
,
(
4696
,
34
,
319
,
4550
,
'Huristak'
,
'Kecamatan'
,
''
)
,
(
4697
,
34
,
319
,
4551
,
'Huta Raja Tinggi'
,
'Kecamatan'
,
''
)
,
(
4698
,
34
,
319
,
4552
,
'Lubuk Barumun'
,
'Kecamatan'
,
''
)
,
(
4699
,
34
,
319
,
4553
,
'Sihapas Barumun'
,
'Kecamatan'
,
''
)
,
(
4700
,
34
,
319
,
4554
,
'Sosa'
,
'Kecamatan'
,
''
)
,
(
4701
,
34
,
319
,
4555
,
'Sosopan'
,
'Kecamatan'
,
''
)
,
(
4702
,
34
,
319
,
4556
,
'Ulu Barumun'
,
'Kecamatan'
,
''
)
,
(
4703
,
34
,
320
,
4557
,
'Batang Onang'
,
'Kecamatan'
,
''
)
,
(
4704
,
34
,
320
,
4558
,
'Dolok'
,
'Kecamatan'
,
''
)
,
(
4705
,
34
,
320
,
4559
,
'Dolok Sigompulon'
,
'Kecamatan'
,
''
)
,
(
4706
,
34
,
320
,
4560
,
'Halongonan'
,
'Kecamatan'
,
''
)
,
(
4707
,
34
,
320
,
4561
,
'Hulu Sihapas'
,
'Kecamatan'
,
''
)
,
(
4708
,
34
,
320
,
4562
,
'Padang Bolak'
,
'Kecamatan'
,
''
)
,
(
4709
,
34
,
320
,
4563
,
'Padang Bolak Julu'
,
'Kecamatan'
,
''
)
,
(
4710
,
34
,
320
,
4564
,
'Portibi'
,
'Kecamatan'
,
''
)
,
(
4711
,
34
,
320
,
4565
,
'Simangambat'
,
'Kecamatan'
,
''
)
,
(
4712
,
34
,
325
,
4596
,
'Kerajaan'
,
'Kecamatan'
,
''
)
,
(
4713
,
34
,
325
,
4597
,
'Pagindar'
,
'Kecamatan'
,
''
)
,
(
4714
,
34
,
325
,
4598
,
'Pergetteng Getteng Sengkut'
,
'Kecamatan'
,
''
)
,
(
4715
,
34
,
325
,
4599
,
'Salak'
,
'Kecamatan'
,
''
)
,
(
4716
,
34
,
325
,
4600
,
'Siempat Rube'
,
'Kecamatan'
,
''
)
,
(
4717
,
34
,
325
,
4601
,
'Sitellu Tali Urang Jehe'
,
'Kecamatan'
,
''
)
,
(
4718
,
34
,
325
,
4602
,
'Sitellu Tali Urang Julu'
,
'Kecamatan'
,
''
)
,
(
4719
,
34
,
325
,
4603
,
'Tinada'
,
'Kecamatan'
,
''
)
,
(
4720
,
34
,
389
,
5385
,
'Harian'
,
'Kecamatan'
,
''
)
,
(
4721
,
34
,
389
,
5386
,
'Nainggolan'
,
'Kecamatan'
,
''
)
,
(
4722
,
34
,
389
,
5387
,
'Onan Runggu'
,
'Kecamatan'
,
''
)
,
(
4723
,
34
,
389
,
5388
,
'Palipi'
,
'Kecamatan'
,
''
)
,
(
4724
,
34
,
389
,
5389
,
'Pangururan'
,
'Kecamatan'
,
''
)
,
(
4725
,
34
,
389
,
5390
,
'Ronggur Nihuta'
,
'Kecamatan'
,
''
)
,
(
4726
,
34
,
389
,
5391
,
'Sianjur Mula-Mula'
,
'Kecamatan'
,
''
)
,
(
4727
,
34
,
389
,
5392
,
'Simanindo'
,
'Kecamatan'
,
''
)
,
(
4728
,
34
,
389
,
5393
,
'Sitio-Tio'
,
'Kecamatan'
,
''
)
,
(
4729
,
34
,
404
,
5575
,
'Bandar Khalifah'
,
'Kecamatan'
,
''
)
,
(
4730
,
34
,
404
,
5576
,
'Bintang Bayu'
,
'Kecamatan'
,
''
)
,
(
4731
,
34
,
404
,
5577
,
'Dolok Masihul'
,
'Kecamatan'
,
''
)
,
(
4732
,
34
,
404
,
5578
,
'Dolok Merawan'
,
'Kecamatan'
,
''
)
,
(
4733
,
34
,
404
,
5579
,
'Kotarih'
,
'Kecamatan'
,
''
)
,
(
4734
,
34
,
404
,
5580
,
'Pantai Cermin'
,
'Kecamatan'
,
''
)
,
(
4735
,
34
,
404
,
5581
,
'Pegajahan'
,
'Kecamatan'
,
''
)
,
(
4736
,
34
,
404
,
5582
,
'Perbaungan'
,
'Kecamatan'
,
''
)
,
(
4737
,
34
,
404
,
5583
,
'Sei Bamban'
,
'Kecamatan'
,
''
)
,
(
4738
,
34
,
404
,
5584
,
'Sei Rampah'
,
'Kecamatan'
,
''
)
,
(
4739
,
34
,
404
,
5585
,
'Serba Jadi'
,
'Kecamatan'
,
''
)
,
(
4740
,
34
,
404
,
5586
,
'Silinda'
,
'Kecamatan'
,
''
)
,
(
4741
,
34
,
404
,
5587
,
'Sipispis'
,
'Kecamatan'
,
''
)
,
(
4742
,
34
,
404
,
5588
,
'Tanjung Beringin'
,
'Kecamatan'
,
''
)
,
(
4743
,
34
,
404
,
5589
,
'Tebing Syahbandar'
,
'Kecamatan'
,
''
)
,
(
4744
,
34
,
404
,
5590
,
'Tebing Tinggi'
,
'Kecamatan'
,
''
)
,
(
4745
,
34
,
404
,
5591
,
'Teluk Mengkudu'
,
'Kecamatan'
,
''
)
,
(
4746
,
34
,
413
,
5693
,
'Bandar'
,
'Kecamatan'
,
''
)
,
(
4747
,
34
,
413
,
5694
,
'Bandar Huluan'
,
'Kecamatan'
,
''
)
,
(
4748
,
34
,
413
,
5695
,
'Bandar Masilam'
,
'Kecamatan'
,
''
)
,
(
4749
,
34
,
413
,
5696
,
'Bosar Maligas'
,
'Kecamatan'
,
''
)
,
(
4750
,
34
,
413
,
5697
,
'Dolok Batu Nanggar'
,
'Kecamatan'
,
''
)
,
(
4751
,
34
,
413
,
5698
,
'Dolok Panribuan'
,
'Kecamatan'
,
''
)
,
(
4752
,
34
,
413
,
5699
,
'Dolok Pardamean'
,
'Kecamatan'
,
''
)
,
(
4753
,
34
,
413
,
5700
,
'Dolok Silau'
,
'Kecamatan'
,
''
)
,
(
4754
,
34
,
413
,
5701
,
'Girsang Sipangan Bolon'
,
'Kecamatan'
,
''
)
,
(
4755
,
34
,
413
,
5702
,
'Gunung Malela'
,
'Kecamatan'
,
''
)
,
(
4756
,
34
,
413
,
5703
,
'Gunung Maligas'
,
'Kecamatan'
,
''
)
,
(
4757
,
34
,
413
,
5704
,
'Haranggaol Horison'
,
'Kecamatan'
,
''
)
,
(
4758
,
34
,
413
,
5705
,
'Hatonduhan'
,
'Kecamatan'
,
''
)
,
(
4759
,
34
,
413
,
5706
,
'Huta Bayu Raja'
,
'Kecamatan'
,
''
)
,
(
4760
,
34
,
413
,
5707
,
'Jawa Maraja Bah Jambi'
,
'Kecamatan'
,
''
)
,
(
4761
,
34
,
413
,
5708
,
'Jorlang Hataran'
,
'Kecamatan'
,
''
)
,
(
4762
,
34
,
413
,
5709
,
'Panei'
,
'Kecamatan'
,
''
)
,
(
4763
,
34
,
413
,
5710
,
'Panombeian Panei'
,
'Kecamatan'
,
''
)
,
(
4764
,
34
,
413
,
5711
,
'Pematang Bandar'
,
'Kecamatan'
,
''
)
,
(
4765
,
34
,
413
,
5712
,
'Pematang Sidamanik'
,
'Kecamatan'
,
''
)
,
(
4766
,
34
,
413
,
5713
,
'Pematang Silima Huta'
,
'Kecamatan'
,
''
)
,
(
4767
,
34
,
413
,
5714
,
'Purba'
,
'Kecamatan'
,
''
)
,
(
4768
,
34
,
413
,
5715
,
'Raya'
,
'Kecamatan'
,
''
)
,
(
4769
,
34
,
413
,
5716
,
'Raya Kahean'
,
'Kecamatan'
,
''
)
,
(
4770
,
34
,
413
,
5717
,
'Siantar'
,
'Kecamatan'
,
''
)
,
(
4771
,
34
,
413
,
5718
,
'Sidamanik'
,
'Kecamatan'
,
''
)
,
(
4772
,
34
,
413
,
5719
,
'Silimakuta'
,
'Kecamatan'
,
''
)
,
(
4773
,
34
,
413
,
5720
,
'Silou Kahean'
,
'Kecamatan'
,
''
)
,
(
4774
,
34
,
413
,
5721
,
'Tanah Jawa'
,
'Kecamatan'
,
''
)
,
(
4775
,
34
,
413
,
5722
,
'Tapian Dolok'
,
'Kecamatan'
,
''
)
,
(
4776
,
34
,
413
,
5723
,
'Ujung Padang'
,
'Kecamatan'
,
''
)
,
(
4777
,
34
,
463
,
6371
,
'Aek Bilah'
,
'Kecamatan'
,
''
)
,
(
4778
,
34
,
463
,
6372
,
'Angkola Barat (Padang Sidempuan)'
,
'Kecamatan'
,
''
)
,
(
4779
,
34
,
463
,
6373
,
'Angkola Sangkunur'
,
'Kecamatan'
,
''
)
,
(
4780
,
34
,
463
,
6374
,
'Angkola Selatan (Siais)'
,
'Kecamatan'
,
''
)
,
(
4781
,
34
,
463
,
6375
,
'Angkola Timur (Padang Sidempuan)'
,
'Kecamatan'
,
''
)
,
(
4782
,
34
,
463
,
6376
,
'Arse'
,
'Kecamatan'
,
''
)
,
(
4783
,
34
,
463
,
6377
,
'Batang Angkola'
,
'Kecamatan'
,
''
)
,
(
4784
,
34
,
463
,
6378
,
'Batang Toru'
,
'Kecamatan'
,
''
)
,
(
4785
,
34
,
463
,
6379
,
'Marancar'
,
'Kecamatan'
,
''
)
,
(
4786
,
34
,
463
,
6380
,
'Muara Batang Toru'
,
'Kecamatan'
,
''
)
,
(
4787
,
34
,
463
,
6381
,
'Saipar Dolok Hole'
,
'Kecamatan'
,
''
)
,
(
4788
,
34
,
463
,
6382
,
'Sayur Matinggi'
,
'Kecamatan'
,
''
)
,
(
4789
,
34
,
463
,
6383
,
'Sipirok'
,
'Kecamatan'
,
''
)
,
(
4790
,
34
,
463
,
6384
,
'Tano Tombangan Angkola'
,
'Kecamatan'
,
''
)
,
(
4791
,
34
,
464
,
6385
,
'Andam Dewi'
,
'Kecamatan'
,
''
)
,
(
4792
,
34
,
464
,
6386
,
'Badiri'
,
'Kecamatan'
,
''
)
,
(
4793
,
34
,
464
,
6387
,
'Barus'
,
'Kecamatan'
,
''
)
,
(
4794
,
34
,
464
,
6388
,
'Barus Utara'
,
'Kecamatan'
,
''
)
,
(
4795
,
34
,
464
,
6389
,
'Kolang'
,
'Kecamatan'
,
''
)
,
(
4796
,
34
,
464
,
6390
,
'Lumut'
,
'Kecamatan'
,
''
)
,
(
4797
,
34
,
464
,
6391
,
'Manduamas'
,
'Kecamatan'
,
''
)
,
(
4798
,
34
,
464
,
6392
,
'Pandan'
,
'Kecamatan'
,
''
)
,
(
4799
,
34
,
464
,
6393
,
'Pasaribu Tobing'
,
'Kecamatan'
,
''
)
,
(
4800
,
34
,
464
,
6394
,
'Pinangsori'
,
'Kecamatan'
,
''
)
,
(
4801
,
34
,
464
,
6395
,
'Sarudik'
,
'Kecamatan'
,
''
)
,
(
4802
,
34
,
464
,
6396
,
'Sibabangun'
,
'Kecamatan'
,
''
)
,
(
4803
,
34
,
464
,
6397
,
'Sirandorung'
,
'Kecamatan'
,
''
)
,
(
4804
,
34
,
464
,
6398
,
'Sitahuis'
,
'Kecamatan'
,
''
)
,
(
4805
,
34
,
464
,
6399
,
'Sorkam'
,
'Kecamatan'
,
''
)
,
(
4806
,
34
,
464
,
6400
,
'Sorkam Barat'
,
'Kecamatan'
,
''
)
,
(
4807
,
34
,
464
,
6401
,
'Sosor Gadong'
,
'Kecamatan'
,
''
)
,
(
4808
,
34
,
464
,
6402
,
'Suka Bangun'
,
'Kecamatan'
,
''
)
,
(
4809
,
34
,
464
,
6403
,
'Tapian Nauli'
,
'Kecamatan'
,
''
)
,
(
4810
,
34
,
464
,
6404
,
'Tukka'
,
'Kecamatan'
,
''
)
,
(
4811
,
34
,
465
,
6405
,
'Adian Koting'
,
'Kecamatan'
,
''
)
,
(
4812
,
34
,
465
,
6406
,
'Garoga'
,
'Kecamatan'
,
''
)
,
(
4813
,
34
,
465
,
6407
,
'Muara'
,
'Kecamatan'
,
''
)
,
(
4814
,
34
,
465
,
6408
,
'Pagaran'
,
'Kecamatan'
,
''
)
,
(
4815
,
34
,
465
,
6409
,
'Pahae Jae'
,
'Kecamatan'
,
''
)
,
(
4816
,
34
,
465
,
6410
,
'Pahae Julu'
,
'Kecamatan'
,
''
)
,
(
4817
,
34
,
465
,
6411
,
'Pangaribuan'
,
'Kecamatan'
,
''
)
,
(
4818
,
34
,
465
,
6412
,
'Parmonangan'
,
'Kecamatan'
,
''
)
,
(
4819
,
34
,
465
,
6413
,
'Purbatua'
,
'Kecamatan'
,
''
)
,
(
4820
,
34
,
465
,
6414
,
'Siatas Barita'
,
'Kecamatan'
,
''
)
,
(
4821
,
34
,
465
,
6415
,
'Siborong-Borong'
,
'Kecamatan'
,
''
)
,
(
4822
,
34
,
465
,
6416
,
'Simangumban'
,
'Kecamatan'
,
''
)
,
(
4823
,
34
,
465
,
6417
,
'Sipahutar'
,
'Kecamatan'
,
''
)
,
(
4824
,
34
,
465
,
6418
,
'Sipoholon'
,
'Kecamatan'
,
''
)
,
(
4825
,
34
,
465
,
6419
,
'Tarutung'
,
'Kecamatan'
,
''
)
,
(
4826
,
34
,
481
,
6652
,
'Ajibata'
,
'Kecamatan'
,
''
)
,
(
4827
,
34
,
481
,
6653
,
'Balige'
,
'Kecamatan'
,
''
)
,
(
4828
,
34
,
481
,
6654
,
'Bonatua Lunasi'
,
'Kecamatan'
,
''
)
,
(
4829
,
34
,
481
,
6655
,
'Bor-Bor'
,
'Kecamatan'
,
''
)
,
(
4830
,
34
,
481
,
6656
,
'Habinsaran'
,
'Kecamatan'
,
''
)
,
(
4831
,
34
,
481
,
6657
,
'Laguboti'
,
'Kecamatan'
,
''
)
,
(
4832
,
34
,
481
,
6658
,
'Lumban Julu'
,
'Kecamatan'
,
''
)
,
(
4833
,
34
,
481
,
6659
,
'Nassau'
,
'Kecamatan'
,
''
)
,
(
4834
,
34
,
481
,
6660
,
'Parmaksian'
,
'Kecamatan'
,
''
)
,
(
4835
,
34
,
481
,
6661
,
'Pintu Pohan Meranti'
,
'Kecamatan'
,
''
)
,
(
4836
,
34
,
481
,
6662
,
'Porsea'
,
'Kecamatan'
,
''
)
,
(
4837
,
34
,
481
,
6663
,
'Siantar Narumonda'
,
'Kecamatan'
,
''
)
,
(
4838
,
34
,
481
,
6664
,
'Sigumpar'
,
'Kecamatan'
,
''
)
,
(
4839
,
34
,
481
,
6665
,
'Silaen'
,
'Kecamatan'
,
''
)
,
(
4840
,
34
,
481
,
6666
,
'Tampahan'
,
'Kecamatan'
,
''
)
,
(
4841
,
34
,
481
,
6667
,
'Uluan'
,
'Kecamatan'
,
''
)
,
(
4842
,
34
,
70
,
934
,
'Binjai Barat'
,
'Kecamatan'
,
''
)
,
(
4843
,
34
,
70
,
935
,
'Binjai Kota'
,
'Kecamatan'
,
''
)
,
(
4844
,
34
,
70
,
936
,
'Binjai Selatan'
,
'Kecamatan'
,
''
)
,
(
4845
,
34
,
70
,
937
,
'Binjai Timur'
,
'Kecamatan'
,
''
)
,
(
4846
,
34
,
70
,
938
,
'Binjai Utara'
,
'Kecamatan'
,
''
)
,
(
4847
,
34
,
137
,
1895
,
'Gunungsitoli'
,
'Kecamatan'
,
''
)
,
(
4848
,
34
,
137
,
1896
,
'Gunungsitoli Alo\'
oa
', '
Kecamatan
', ''),
(4849, 34, 137, 1897, '
Gunungsitoli
Barat
', '
Kecamatan
', ''),
(4850, 34, 137, 1898, '
Gunungsitoli
Idanoi
', '
Kecamatan
', ''),
(4851, 34, 137, 1899, '
Gunungsitoli
Selatan
', '
Kecamatan
', ''),
(4852, 34, 137, 1900, '
Gunungsitoli
Utara
', '
Kecamatan
', ''),
(4853, 34, 323, 4585, '
Padang
Sidempuan
Angkola
Julu
', '
Kecamatan
', ''),
(4854, 34, 323, 4586, '
Padang
Sidempuan
Batunadua
', '
Kecamatan
', ''),
(4855, 34, 323, 4587, '
Padang
Sidempuan
Hutaimbaru
', '
Kecamatan
', ''),
(4856, 34, 323, 4588, '
Padang
Sidempuan
Selatan
', '
Kecamatan
', ''),
(4857, 34, 323, 4589, '
Padang
Sidempuan
Tenggara
', '
Kecamatan
', ''),
(4858, 34, 323, 4590, '
Padang
Sidempuan
Utara
(
Padangsidimpuan
)
', '
Kecamatan
', ''),
(4859, 34, 353, 4952, '
Siantar
Barat
', '
Kecamatan
', ''),
(4860, 34, 353, 4953, '
Siantar
Marihat
', '
Kecamatan
', ''),
(4861, 34, 353, 4954, '
Siantar
Marimbun
', '
Kecamatan
', ''),
(4862, 34, 353, 4955, '
Siantar
Martoba
', '
Kecamatan
', ''),
(4863, 34, 353, 4956, '
Siantar
Selatan
', '
Kecamatan
', ''),
(4864, 34, 353, 4957, '
Siantar
Sitalasari
', '
Kecamatan
', ''),
(4865, 34, 353, 4958, '
Siantar
Timur
', '
Kecamatan
', ''),
(4866, 34, 353, 4959, '
Siantar
Utara
', '
Kecamatan
', ''),
(4867, 34, 407, 5616, '
Sibolga
Kota
', '
Kecamatan
', ''),
(4868, 34, 407, 5617, '
Sibolga
Sambas
', '
Kecamatan
', ''),
(4869, 34, 407, 5618, '
Sibolga
Selatan
', '
Kecamatan
', ''),
(4870, 34, 407, 5619, '
Sibolga
Utara
', '
Kecamatan
', ''),
(4871, 34, 459, 6337, '
Datuk
Bandar
', '
Kecamatan
', ''),
(4872, 34, 459, 6338, '
Datuk
Bandar
Timur
', '
Kecamatan
', ''),
(4873, 34, 459, 6339, '
Sei
Tualang
Raso
', '
Kecamatan
', ''),
(4874, 34, 459, 6340, '
Tanjung
Balai
Selatan
', '
Kecamatan
', ''),
(4875, 34, 459, 6341, '
Tanjung
Balai
Utara
', '
Kecamatan
', ''),
(4876, 34, 459, 6342, '
Teluk
Nibung
', '
Kecamatan
', ''),
(4877, 34, 470, 6485, '
Bajenis
', '
Kecamatan
', ''),
(4878, 34, 470, 6486, '
Padang
Hilir
', '
Kecamatan
', ''),
(4879, 34, 470, 6487, '
Padang
Hulu
', '
Kecamatan
', ''),
(4880, 34, 470, 6488, '
Rambutan
', '
Kecamatan
', ''),
(4881, 34, 470, 6489, '
Tebing
Tinggi
Kota
', '
Kecamatan
', ''),
(4882, 22, 68, 911, '
Ambalawi
', '
Kecamatan
', ''),
(4883, 22, 68, 912, '
Belo
', '
Kecamatan
', ''),
(4884, 22, 68, 913, '
Bolo
', '
Kecamatan
', ''),
(4885, 22, 68, 914, '
Donggo
', '
Kecamatan
', '');
INSERT INTO `location` (`location_id`, `province_id`, `city_id`, `subdistrict_id`, `name`, `type`, `postcode`) VALUES
(4886, 22, 68, 915, '
Lambitu
', '
Kecamatan
', ''),
(4887, 22, 68, 916, '
Lambu
', '
Kecamatan
', ''),
(4888, 22, 68, 917, '
Langgudu
', '
Kecamatan
', ''),
(4889, 22, 68, 918, '
Madapangga
', '
Kecamatan
', ''),
(4890, 22, 68, 919, '
Monta
', '
Kecamatan
', ''),
(4891, 22, 68, 920, '
Palibelo
', '
Kecamatan
', ''),
(4892, 22, 68, 921, '
Parado
', '
Kecamatan
', ''),
(4893, 22, 68, 922, '
Sanggar
', '
Kecamatan
', ''),
(4894, 22, 68, 923, '
Sape
', '
Kecamatan
', ''),
(4895, 22, 68, 924, '
Soromandi
', '
Kecamatan
', ''),
(4896, 22, 68, 925, '
Tambora
', '
Kecamatan
', ''),
(4897, 22, 68, 926, '
Wawo
', '
Kecamatan
', ''),
(4898, 22, 68, 927, '
Wera
', '
Kecamatan
', ''),
(4899, 22, 68, 928, '
Woha
', '
Kecamatan
', ''),
(4900, 22, 118, 1609, '
Dompu
', '
Kecamatan
', ''),
(4901, 22, 118, 1610, '
Hu
\
'u'
,
'Kecamatan'
,
''
)
,
(
4902
,
22
,
118
,
1611
,
'Kempo'
,
'Kecamatan'
,
''
)
,
(
4903
,
22
,
118
,
1612
,
'Kilo'
,
'Kecamatan'
,
''
)
,
(
4904
,
22
,
118
,
1613
,
'Menggelewa (Manggelewa)'
,
'Kecamatan'
,
''
)
,
(
4905
,
22
,
118
,
1614
,
'Pajo'
,
'Kecamatan'
,
''
)
,
(
4906
,
22
,
118
,
1615
,
'Pekat'
,
'Kecamatan'
,
''
)
,
(
4907
,
22
,
118
,
1616
,
'Woja'
,
'Kecamatan'
,
''
)
,
(
4908
,
22
,
238
,
3372
,
'Batu Layar'
,
'Kecamatan'
,
''
)
,
(
4909
,
22
,
238
,
3373
,
'Gerung'
,
'Kecamatan'
,
''
)
,
(
4910
,
22
,
238
,
3374
,
'Gunungsari'
,
'Kecamatan'
,
''
)
,
(
4911
,
22
,
238
,
3375
,
'Kediri'
,
'Kecamatan'
,
''
)
,
(
4912
,
22
,
238
,
3376
,
'Kuripan'
,
'Kecamatan'
,
''
)
,
(
4913
,
22
,
238
,
3377
,
'Labuapi'
,
'Kecamatan'
,
''
)
,
(
4914
,
22
,
238
,
3378
,
'Lembar'
,
'Kecamatan'
,
''
)
,
(
4915
,
22
,
238
,
3379
,
'Lingsar'
,
'Kecamatan'
,
''
)
,
(
4916
,
22
,
238
,
3380
,
'Narmada'
,
'Kecamatan'
,
''
)
,
(
4917
,
22
,
238
,
3381
,
'Sekotong'
,
'Kecamatan'
,
''
)
,
(
4918
,
22
,
239
,
3382
,
'Batukliang'
,
'Kecamatan'
,
''
)
,
(
4919
,
22
,
239
,
3383
,
'Batukliang Utara'
,
'Kecamatan'
,
''
)
,
(
4920
,
22
,
239
,
3384
,
'Janapria'
,
'Kecamatan'
,
''
)
,
(
4921
,
22
,
239
,
3385
,
'Jonggat'
,
'Kecamatan'
,
''
)
,
(
4922
,
22
,
239
,
3386
,
'Kopang'
,
'Kecamatan'
,
''
)
,
(
4923
,
22
,
239
,
3387
,
'Praya'
,
'Kecamatan'
,
''
)
,
(
4924
,
22
,
239
,
3388
,
'Praya Barat'
,
'Kecamatan'
,
''
)
,
(
4925
,
22
,
239
,
3389
,
'Praya Barat Daya'
,
'Kecamatan'
,
''
)
,
(
4926
,
22
,
239
,
3390
,
'Praya Tengah'
,
'Kecamatan'
,
''
)
,
(
4927
,
22
,
239
,
3391
,
'Praya Timur'
,
'Kecamatan'
,
''
)
,
(
4928
,
22
,
239
,
3392
,
'Pringgarata'
,
'Kecamatan'
,
''
)
,
(
4929
,
22
,
239
,
3393
,
'Pujut'
,
'Kecamatan'
,
''
)
,
(
4930
,
22
,
240
,
3394
,
'Aikmel'
,
'Kecamatan'
,
''
)
,
(
4931
,
22
,
240
,
3395
,
'Jerowaru'
,
'Kecamatan'
,
''
)
,
(
4932
,
22
,
240
,
3396
,
'Keruak'
,
'Kecamatan'
,
''
)
,
(
4933
,
22
,
240
,
3397
,
'Labuhan Haji'
,
'Kecamatan'
,
''
)
,
(
4934
,
22
,
240
,
3398
,
'Masbagik'
,
'Kecamatan'
,
''
)
,
(
4935
,
22
,
240
,
3399
,
'Montong Gading'
,
'Kecamatan'
,
''
)
,
(
4936
,
22
,
240
,
3400
,
'Pringgabaya'
,
'Kecamatan'
,
''
)
,
(
4937
,
22
,
240
,
3401
,
'Pringgasela'
,
'Kecamatan'
,
''
)
,
(
4938
,
22
,
240
,
3402
,
'Sakra'
,
'Kecamatan'
,
''
)
,
(
4939
,
22
,
240
,
3403
,
'Sakra Barat'
,
'Kecamatan'
,
''
)
,
(
4940
,
22
,
240
,
3404
,
'Sakra Timur'
,
'Kecamatan'
,
''
)
,
(
4941
,
22
,
240
,
3405
,
'Sambalia (Sambelia)'
,
'Kecamatan'
,
''
)
,
(
4942
,
22
,
240
,
3406
,
'Selong'
,
'Kecamatan'
,
''
)
,
(
4943
,
22
,
240
,
3407
,
'Sembalun'
,
'Kecamatan'
,
''
)
,
(
4944
,
22
,
240
,
3408
,
'Sikur'
,
'Kecamatan'
,
''
)
,
(
4945
,
22
,
240
,
3409
,
'Suela (Suwela)'
,
'Kecamatan'
,
''
)
,
(
4946
,
22
,
240
,
3410
,
'Sukamulia'
,
'Kecamatan'
,
''
)
,
(
4947
,
22
,
240
,
3411
,
'Suralaga'
,
'Kecamatan'
,
''
)
,
(
4948
,
22
,
240
,
3412
,
'Terara'
,
'Kecamatan'
,
''
)
,
(
4949
,
22
,
240
,
3413
,
'Wanasaba'
,
'Kecamatan'
,
''
)
,
(
4950
,
22
,
241
,
3414
,
'Bayan'
,
'Kecamatan'
,
''
)
,
(
4951
,
22
,
241
,
3415
,
'Gangga'
,
'Kecamatan'
,
''
)
,
(
4952
,
22
,
241
,
3416
,
'Kayangan'
,
'Kecamatan'
,
''
)
,
(
4953
,
22
,
241
,
3417
,
'Pemenang'
,
'Kecamatan'
,
''
)
,
(
4954
,
22
,
241
,
3418
,
'Tanjung'
,
'Kecamatan'
,
''
)
,
(
4955
,
22
,
438
,
6033
,
'Alas'
,
'Kecamatan'
,
''
)
,
(
4956
,
22
,
438
,
6034
,
'Alas Barat'
,
'Kecamatan'
,
''
)
,
(
4957
,
22
,
438
,
6035
,
'Batulanteh'
,
'Kecamatan'
,
''
)
,
(
4958
,
22
,
438
,
6036
,
'Buer'
,
'Kecamatan'
,
''
)
,
(
4959
,
22
,
438
,
6037
,
'Empang'
,
'Kecamatan'
,
''
)
,
(
4960
,
22
,
438
,
6038
,
'Labangka'
,
'Kecamatan'
,
''
)
,
(
4961
,
22
,
438
,
6039
,
'Labuhan Badas'
,
'Kecamatan'
,
''
)
,
(
4962
,
22
,
438
,
6040
,
'Lantung'
,
'Kecamatan'
,
''
)
,
(
4963
,
22
,
438
,
6041
,
'Lape (Lape Lopok)'
,
'Kecamatan'
,
''
)
,
(
4964
,
22
,
438
,
6042
,
'Lenangguar'
,
'Kecamatan'
,
''
)
,
(
4965
,
22
,
438
,
6043
,
'Lopok'
,
'Kecamatan'
,
''
)
,
(
4966
,
22
,
438
,
6044
,
'Lunyuk'
,
'Kecamatan'
,
''
)
,
(
4967
,
22
,
438
,
6045
,
'Maronge'
,
'Kecamatan'
,
''
)
,
(
4968
,
22
,
438
,
6046
,
'Moyo Hilir'
,
'Kecamatan'
,
''
)
,
(
4969
,
22
,
438
,
6047
,
'Moyo Hulu'
,
'Kecamatan'
,
''
)
,
(
4970
,
22
,
438
,
6048
,
'Moyo Utara'
,
'Kecamatan'
,
''
)
,
(
4971
,
22
,
438
,
6049
,
'Orong Telu'
,
'Kecamatan'
,
''
)
,
(
4972
,
22
,
438
,
6050
,
'Plampang'
,
'Kecamatan'
,
''
)
,
(
4973
,
22
,
438
,
6051
,
'Rhee'
,
'Kecamatan'
,
''
)
,
(
4974
,
22
,
438
,
6052
,
'Ropang'
,
'Kecamatan'
,
''
)
,
(
4975
,
22
,
438
,
6053
,
'Sumbawa'
,
'Kecamatan'
,
''
)
,
(
4976
,
22
,
438
,
6054
,
'Tarano'
,
'Kecamatan'
,
''
)
,
(
4977
,
22
,
438
,
6055
,
'Unter Iwes (Unterwiris)'
,
'Kecamatan'
,
''
)
,
(
4978
,
22
,
438
,
6056
,
'Utan'
,
'Kecamatan'
,
''
)
,
(
4979
,
22
,
439
,
6057
,
'Brang Ene'
,
'Kecamatan'
,
''
)
,
(
4980
,
22
,
439
,
6058
,
'Brang Rea'
,
'Kecamatan'
,
''
)
,
(
4981
,
22
,
439
,
6059
,
'Jereweh'
,
'Kecamatan'
,
''
)
,
(
4982
,
22
,
439
,
6060
,
'Maluk'
,
'Kecamatan'
,
''
)
,
(
4983
,
22
,
439
,
6061
,
'Poto Tano'
,
'Kecamatan'
,
''
)
,
(
4984
,
22
,
439
,
6062
,
'Sateluk (Seteluk)'
,
'Kecamatan'
,
''
)
,
(
4985
,
22
,
439
,
6063
,
'Sekongkang'
,
'Kecamatan'
,
''
)
,
(
4986
,
22
,
439
,
6064
,
'Taliwang'
,
'Kecamatan'
,
''
)
,
(
4987
,
22
,
276
,
3876
,
'Ampenan'
,
'Kecamatan'
,
''
)
,
(
4988
,
22
,
276
,
3877
,
'Cakranegara'
,
'Kecamatan'
,
''
)
,
(
4989
,
22
,
276
,
3878
,
'Mataram'
,
'Kecamatan'
,
''
)
,
(
4990
,
22
,
276
,
3879
,
'Sandubaya (Sandujaya)'
,
'Kecamatan'
,
''
)
,
(
4991
,
22
,
276
,
3880
,
'Sekarbela'
,
'Kecamatan'
,
''
)
,
(
4992
,
22
,
276
,
3881
,
'Selaparang (Selaprang)'
,
'Kecamatan'
,
''
)
,
(
4993
,
22
,
69
,
929
,
'Asakota'
,
'Kecamatan'
,
''
)
,
(
4994
,
22
,
69
,
930
,
'Mpunda'
,
'Kecamatan'
,
''
)
,
(
4995
,
22
,
69
,
931
,
'Raba'
,
'Kecamatan'
,
''
)
,
(
4996
,
22
,
69
,
932
,
'Rasanae Barat'
,
'Kecamatan'
,
''
)
,
(
4997
,
22
,
69
,
933
,
'Rasanae Timur'
,
'Kecamatan'
,
''
)
,
(
4998
,
23
,
13
,
0
,
'Alor'
,
'Kabupaten'
,
'85811'
)
,
(
4999
,
23
,
58
,
0
,
'Belu'
,
'Kabupaten'
,
'85711'
)
,
(
5000
,
23
,
122
,
0
,
'Ende'
,
'Kabupaten'
,
'86351'
)
,
(
5001
,
23
,
125
,
0
,
'Flores Timur'
,
'Kabupaten'
,
'86213'
)
,
(
5002
,
23
,
212
,
0
,
'Kupang'
,
'Kabupaten'
,
'85362'
)
,
(
5003
,
23
,
213
,
0
,
'Kupang'
,
'Kota'
,
'85119'
)
,
(
5004
,
23
,
234
,
0
,
'Lembata'
,
'Kabupaten'
,
'86611'
)
,
(
5005
,
23
,
269
,
0
,
'Manggarai'
,
'Kabupaten'
,
'86551'
)
,
(
5006
,
23
,
270
,
0
,
'Manggarai Barat'
,
'Kabupaten'
,
'86711'
)
,
(
5007
,
23
,
271
,
0
,
'Manggarai Timur'
,
'Kabupaten'
,
'86811'
)
,
(
5008
,
23
,
301
,
0
,
'Nagekeo'
,
'Kabupaten'
,
'86911'
)
,
(
5009
,
23
,
304
,
0
,
'Ngada'
,
'Kabupaten'
,
'86413'
)
,
(
5010
,
23
,
383
,
0
,
'Rote Ndao'
,
'Kabupaten'
,
'85982'
)
,
(
5011
,
23
,
385
,
0
,
'Sabu Raijua'
,
'Kabupaten'
,
'85391'
)
,
(
5012
,
23
,
412
,
0
,
'Sikka'
,
'Kabupaten'
,
'86121'
)
,
(
5013
,
23
,
434
,
0
,
'Sumba Barat'
,
'Kabupaten'
,
'87219'
)
,
(
5014
,
23
,
435
,
0
,
'Sumba Barat Daya'
,
'Kabupaten'
,
'87453'
)
,
(
5015
,
23
,
436
,
0
,
'Sumba Tengah'
,
'Kabupaten'
,
'87358'
)
,
(
5016
,
23
,
437
,
0
,
'Sumba Timur'
,
'Kabupaten'
,
'87112'
)
,
(
5017
,
23
,
479
,
0
,
'Timor Tengah Selatan'
,
'Kabupaten'
,
'85562'
)
,
(
5018
,
23
,
480
,
0
,
'Timor Tengah Utara'
,
'Kabupaten'
,
'85612'
)
,
(
5019
,
23
,
13
,
192
,
'Alor Barat Daya'
,
'Kecamatan'
,
''
)
,
(
5020
,
23
,
13
,
193
,
'Alor Barat Laut'
,
'Kecamatan'
,
''
)
,
(
5021
,
23
,
13
,
194
,
'Alor Selatan'
,
'Kecamatan'
,
''
)
,
(
5022
,
23
,
13
,
195
,
'Alor Tengah Utara'
,
'Kecamatan'
,
''
)
,
(
5023
,
23
,
13
,
196
,
'Alor Timur'
,
'Kecamatan'
,
''
)
,
(
5024
,
23
,
13
,
197
,
'Alor Timur Laut'
,
'Kecamatan'
,
''
)
,
(
5025
,
23
,
13
,
198
,
'Kabola'
,
'Kecamatan'
,
''
)
,
(
5026
,
23
,
13
,
199
,
'Lembur'
,
'Kecamatan'
,
''
)
,
(
5027
,
23
,
13
,
200
,
'Mataru'
,
'Kecamatan'
,
''
)
,
(
5028
,
23
,
13
,
201
,
'Pantar'
,
'Kecamatan'
,
''
)
,
(
5029
,
23
,
13
,
202
,
'Pantar Barat'
,
'Kecamatan'
,
''
)
,
(
5030
,
23
,
13
,
203
,
'Pantar Barat Laut'
,
'Kecamatan'
,
''
)
,
(
5031
,
23
,
13
,
204
,
'Pantar Tengah'
,
'Kecamatan'
,
''
)
,
(
5032
,
23
,
13
,
205
,
'Pantar Timur'
,
'Kecamatan'
,
''
)
,
(
5033
,
23
,
13
,
206
,
'Pulau Pura'
,
'Kecamatan'
,
''
)
,
(
5034
,
23
,
13
,
207
,
'Pureman'
,
'Kecamatan'
,
''
)
,
(
5035
,
23
,
13
,
208
,
'Teluk Mutiara'
,
'Kecamatan'
,
''
)
,
(
5036
,
23
,
58
,
773
,
'Atambua Barat'
,
'Kecamatan'
,
''
)
,
(
5037
,
23
,
58
,
774
,
'Atambua Kota'
,
'Kecamatan'
,
''
)
,
(
5038
,
23
,
58
,
775
,
'Atambua Selatan'
,
'Kecamatan'
,
''
)
,
(
5039
,
23
,
58
,
776
,
'Botin Leo Bele'
,
'Kecamatan'
,
''
)
,
(
5040
,
23
,
58
,
777
,
'Io Kufeu'
,
'Kecamatan'
,
''
)
,
(
5041
,
23
,
58
,
778
,
'Kakuluk Mesak'
,
'Kecamatan'
,
''
)
,
(
5042
,
23
,
58
,
779
,
'Kobalima'
,
'Kecamatan'
,
''
)
,
(
5043
,
23
,
58
,
780
,
'Kobalima Timur'
,
'Kecamatan'
,
''
)
,
(
5044
,
23
,
58
,
781
,
'Laen Manen'
,
'Kecamatan'
,
''
)
,
(
5045
,
23
,
58
,
782
,
'Lamaknen'
,
'Kecamatan'
,
''
)
,
(
5046
,
23
,
58
,
783
,
'Lamaknen Selatan'
,
'Kecamatan'
,
''
)
,
(
5047
,
23
,
58
,
784
,
'Lasiolat'
,
'Kecamatan'
,
''
)
,
(
5048
,
23
,
58
,
785
,
'Malaka Barat'
,
'Kecamatan'
,
''
)
,
(
5049
,
23
,
58
,
786
,
'Malaka Tengah'
,
'Kecamatan'
,
''
)
,
(
5050
,
23
,
58
,
787
,
'Malaka Timur'
,
'Kecamatan'
,
''
)
,
(
5051
,
23
,
58
,
788
,
'Nanaet Duabesi'
,
'Kecamatan'
,
''
)
,
(
5052
,
23
,
58
,
789
,
'Raihat'
,
'Kecamatan'
,
''
)
,
(
5053
,
23
,
58
,
790
,
'Raimanuk'
,
'Kecamatan'
,
''
)
,
(
5054
,
23
,
58
,
791
,
'Rinhat'
,
'Kecamatan'
,
''
)
,
(
5055
,
23
,
58
,
792
,
'Sasitamean'
,
'Kecamatan'
,
''
)
,
(
5056
,
23
,
58
,
793
,
'Tasifeto Barat'
,
'Kecamatan'
,
''
)
,
(
5057
,
23
,
58
,
794
,
'Tasifeto Timur'
,
'Kecamatan'
,
''
)
,
(
5058
,
23
,
58
,
795
,
'Weliman'
,
'Kecamatan'
,
''
)
,
(
5059
,
23
,
58
,
796
,
'Wewiku'
,
'Kecamatan'
,
''
)
,
(
5060
,
23
,
122
,
1650
,
'Detukeli'
,
'Kecamatan'
,
''
)
,
(
5061
,
23
,
122
,
1651
,
'Detusoko'
,
'Kecamatan'
,
''
)
,
(
5062
,
23
,
122
,
1652
,
'Ende'
,
'Kecamatan'
,
''
)
,
(
5063
,
23
,
122
,
1653
,
'Ende Selatan'
,
'Kecamatan'
,
''
)
,
(
5064
,
23
,
122
,
1654
,
'Ende Tengah'
,
'Kecamatan'
,
''
)
,
(
5065
,
23
,
122
,
1655
,
'Ende Timur'
,
'Kecamatan'
,
''
)
,
(
5066
,
23
,
122
,
1656
,
'Ende Utara'
,
'Kecamatan'
,
''
)
,
(
5067
,
23
,
122
,
1657
,
'Kelimutu'
,
'Kecamatan'
,
''
)
,
(
5068
,
23
,
122
,
1658
,
'Kotabaru'
,
'Kecamatan'
,
''
)
,
(
5069
,
23
,
122
,
1659
,
'Lepembusu Kelisoke'
,
'Kecamatan'
,
''
)
,
(
5070
,
23
,
122
,
1660
,
'Lio Timur'
,
'Kecamatan'
,
''
)
,
(
5071
,
23
,
122
,
1661
,
'Maukaro'
,
'Kecamatan'
,
''
)
,
(
5072
,
23
,
122
,
1662
,
'Maurole'
,
'Kecamatan'
,
''
)
,
(
5073
,
23
,
122
,
1663
,
'Nangapanda'
,
'Kecamatan'
,
''
)
,
(
5074
,
23
,
122
,
1664
,
'Ndona'
,
'Kecamatan'
,
''
)
,
(
5075
,
23
,
122
,
1665
,
'Ndona Timur'
,
'Kecamatan'
,
''
)
,
(
5076
,
23
,
122
,
1666
,
'Ndori'
,
'Kecamatan'
,
''
)
,
(
5077
,
23
,
122
,
1667
,
'Pulau Ende'
,
'Kecamatan'
,
''
)
,
(
5078
,
23
,
122
,
1668
,
'Wewaria'
,
'Kecamatan'
,
''
)
,
(
5079
,
23
,
122
,
1669
,
'Wolojita'
,
'Kecamatan'
,
''
)
,
(
5080
,
23
,
122
,
1670
,
'Wolowaru'
,
'Kecamatan'
,
''
)
,
(
5081
,
23
,
125
,
1692
,
'Adonara'
,
'Kecamatan'
,
''
)
,
(
5082
,
23
,
125
,
1693
,
'Adonara Barat'
,
'Kecamatan'
,
''
)
,
(
5083
,
23
,
125
,
1694
,
'Adonara Tengah'
,
'Kecamatan'
,
''
)
,
(
5084
,
23
,
125
,
1695
,
'Adonara Timur'
,
'Kecamatan'
,
''
)
,
(
5085
,
23
,
125
,
1696
,
'Demon Pagong'
,
'Kecamatan'
,
''
)
,
(
5086
,
23
,
125
,
1697
,
'Ile Boleng'
,
'Kecamatan'
,
''
)
,
(
5087
,
23
,
125
,
1698
,
'Ile Bura'
,
'Kecamatan'
,
''
)
,
(
5088
,
23
,
125
,
1699
,
'Ile Mandiri'
,
'Kecamatan'
,
''
)
,
(
5089
,
23
,
125
,
1700
,
'Kelubagolit (Klubagolit)'
,
'Kecamatan'
,
''
)
,
(
5090
,
23
,
125
,
1701
,
'Larantuka'
,
'Kecamatan'
,
''
)
,
(
5091
,
23
,
125
,
1702
,
'Lewolema'
,
'Kecamatan'
,
''
)
,
(
5092
,
23
,
125
,
1703
,
'Solor Barat'
,
'Kecamatan'
,
''
)
,
(
5093
,
23
,
125
,
1704
,
'Solor Selatan'
,
'Kecamatan'
,
''
)
,
(
5094
,
23
,
125
,
1705
,
'Solor Timur'
,
'Kecamatan'
,
''
)
,
(
5095
,
23
,
125
,
1706
,
'Tanjung Bunga'
,
'Kecamatan'
,
''
)
,
(
5096
,
23
,
125
,
1707
,
'Titehena'
,
'Kecamatan'
,
''
)
,
(
5097
,
23
,
125
,
1708
,
'Witihama (Watihama)'
,
'Kecamatan'
,
''
)
,
(
5098
,
23
,
125
,
1709
,
'Wotan Ulumado'
,
'Kecamatan'
,
''
)
,
(
5099
,
23
,
125
,
1710
,
'Wulanggitang'
,
'Kecamatan'
,
''
)
,
(
5100
,
23
,
212
,
2974
,
'Amabi Oefeto'
,
'Kecamatan'
,
''
)
,
(
5101
,
23
,
212
,
2975
,
'Amabi Oefeto Timur'
,
'Kecamatan'
,
''
)
,
(
5102
,
23
,
212
,
2976
,
'Amarasi'
,
'Kecamatan'
,
''
)
,
(
5103
,
23
,
212
,
2977
,
'Amarasi Barat'
,
'Kecamatan'
,
''
)
,
(
5104
,
23
,
212
,
2978
,
'Amarasi Selatan'
,
'Kecamatan'
,
''
)
,
(
5105
,
23
,
212
,
2979
,
'Amarasi Timur'
,
'Kecamatan'
,
''
)
,
(
5106
,
23
,
212
,
2980
,
'Amfoang Barat Daya'
,
'Kecamatan'
,
''
)
,
(
5107
,
23
,
212
,
2981
,
'Amfoang Barat Laut'
,
'Kecamatan'
,
''
)
,
(
5108
,
23
,
212
,
2982
,
'Amfoang Selatan'
,
'Kecamatan'
,
''
)
,
(
5109
,
23
,
212
,
2983
,
'Amfoang Tengah'
,
'Kecamatan'
,
''
)
,
(
5110
,
23
,
212
,
2984
,
'Amfoang Timur'
,
'Kecamatan'
,
''
)
,
(
5111
,
23
,
212
,
2985
,
'Amfoang Utara'
,
'Kecamatan'
,
''
)
,
(
5112
,
23
,
212
,
2986
,
'Fatuleu'
,
'Kecamatan'
,
''
)
,
(
5113
,
23
,
212
,
2987
,
'Fatuleu Barat'
,
'Kecamatan'
,
''
)
,
(
5114
,
23
,
212
,
2988
,
'Fatuleu Tengah'
,
'Kecamatan'
,
''
)
,
(
5115
,
23
,
212
,
2989
,
'Kupang Barat'
,
'Kecamatan'
,
''
)
,
(
5116
,
23
,
212
,
2990
,
'Kupang Tengah'
,
'Kecamatan'
,
''
)
,
(
5117
,
23
,
212
,
2991
,
'Kupang Timur'
,
'Kecamatan'
,
''
)
,
(
5118
,
23
,
212
,
2992
,
'Nekamese'
,
'Kecamatan'
,
''
)
,
(
5119
,
23
,
212
,
2993
,
'Semau'
,
'Kecamatan'
,
''
)
,
(
5120
,
23
,
212
,
2994
,
'Semau Selatan'
,
'Kecamatan'
,
''
)
,
(
5121
,
23
,
212
,
2995
,
'Sulamu'
,
'Kecamatan'
,
''
)
,
(
5122
,
23
,
212
,
2996
,
'Taebenu'
,
'Kecamatan'
,
''
)
,
(
5123
,
23
,
212
,
2997
,
'Takari'
,
'Kecamatan'
,
''
)
,
(
5124
,
23
,
213
,
2998
,
'Alak'
,
'Kecamatan'
,
''
)
,
(
5125
,
23
,
213
,
2999
,
'Kelapa Lima'
,
'Kecamatan'
,
''
)
,
(
5126
,
23
,
213
,
3000
,
'Kota Lama'
,
'Kecamatan'
,
''
)
,
(
5127
,
23
,
213
,
3001
,
'Kota Raja'
,
'Kecamatan'
,
''
)
,
(
5128
,
23
,
213
,
3002
,
'Maulafa'
,
'Kecamatan'
,
''
)
,
(
5129
,
23
,
213
,
3003
,
'Oebobo'
,
'Kecamatan'
,
''
)
,
(
5130
,
23
,
234
,
3338
,
'Atadei'
,
'Kecamatan'
,
''
)
,
(
5131
,
23
,
234
,
3339
,
'Buyasuri (Buyasari)'
,
'Kecamatan'
,
''
)
,
(
5132
,
23
,
234
,
3340
,
'Ile Ape'
,
'Kecamatan'
,
''
)
,
(
5133
,
23
,
234
,
3341
,
'Ile Ape Timur'
,
'Kecamatan'
,
''
)
,
(
5134
,
23
,
234
,
3342
,
'Lebatukan'
,
'Kecamatan'
,
''
)
,
(
5135
,
23
,
234
,
3343
,
'Nagawutung'
,
'Kecamatan'
,
''
)
,
(
5136
,
23
,
234
,
3344
,
'Nubatukan'
,
'Kecamatan'
,
''
)
,
(
5137
,
23
,
234
,
3345
,
'Omesuri'
,
'Kecamatan'
,
''
)
,
(
5138
,
23
,
234
,
3346
,
'Wulandoni (Wulandioni)'
,
'Kecamatan'
,
''
)
,
(
5139
,
23
,
269
,
3802
,
'Cibal'
,
'Kecamatan'
,
''
)
,
(
5140
,
23
,
269
,
3803
,
'Cibal Barat'
,
'Kecamatan'
,
''
)
,
(
5141
,
23
,
269
,
3804
,
'Langke Rembong'
,
'Kecamatan'
,
''
)
,
(
5142
,
23
,
269
,
3805
,
'Lelak'
,
'Kecamatan'
,
''
)
,
(
5143
,
23
,
269
,
3806
,
'Rahong Utara'
,
'Kecamatan'
,
''
)
,
(
5144
,
23
,
269
,
3807
,
'Reok'
,
'Kecamatan'
,
''
)
,
(
5145
,
23
,
269
,
3808
,
'Reok Barat'
,
'Kecamatan'
,
''
)
,
(
5146
,
23
,
269
,
3809
,
'Ruteng'
,
'Kecamatan'
,
''
)
,
(
5147
,
23
,
269
,
3810
,
'Satar Mese'
,
'Kecamatan'
,
''
)
,
(
5148
,
23
,
269
,
3811
,
'Satar Mese Barat'
,
'Kecamatan'
,
''
)
,
(
5149
,
23
,
269
,
3812
,
'Wae Rii'
,
'Kecamatan'
,
''
)
,
(
5150
,
23
,
270
,
3813
,
'Boleng'
,
'Kecamatan'
,
''
)
,
(
5151
,
23
,
270
,
3814
,
'Komodo'
,
'Kecamatan'
,
''
)
,
(
5152
,
23
,
270
,
3815
,
'Kuwus'
,
'Kecamatan'
,
''
)
,
(
5153
,
23
,
270
,
3816
,
'Lembor'
,
'Kecamatan'
,
''
)
,
(
5154
,
23
,
270
,
3817
,
'Lembor Selatan'
,
'Kecamatan'
,
''
)
,
(
5155
,
23
,
270
,
3818
,
'Macang Pacar'
,
'Kecamatan'
,
''
)
,
(
5156
,
23
,
270
,
3819
,
'Mbeliling'
,
'Kecamatan'
,
''
)
,
(
5157
,
23
,
270
,
3820
,
'Ndoso'
,
'Kecamatan'
,
''
)
,
(
5158
,
23
,
270
,
3821
,
'Sano Nggoang'
,
'Kecamatan'
,
''
)
,
(
5159
,
23
,
270
,
3822
,
'Welak'
,
'Kecamatan'
,
''
)
,
(
5160
,
23
,
271
,
3823
,
'Borong'
,
'Kecamatan'
,
''
)
,
(
5161
,
23
,
271
,
3824
,
'Elar'
,
'Kecamatan'
,
''
)
,
(
5162
,
23
,
271
,
3825
,
'Elar Selatan'
,
'Kecamatan'
,
''
)
,
(
5163
,
23
,
271
,
3826
,
'Kota Komba'
,
'Kecamatan'
,
''
)
,
(
5164
,
23
,
271
,
3827
,
'Lamba Leda'
,
'Kecamatan'
,
''
)
,
(
5165
,
23
,
271
,
3828
,
'Poco Ranaka'
,
'Kecamatan'
,
''
)
,
(
5166
,
23
,
271
,
3829
,
'Poco Ranaka Timur'
,
'Kecamatan'
,
''
)
,
(
5167
,
23
,
271
,
3830
,
'Rana Mese'
,
'Kecamatan'
,
''
)
,
(
5168
,
23
,
271
,
3831
,
'Sambi Rampas'
,
'Kecamatan'
,
''
)
,
(
5169
,
23
,
301
,
4260
,
'Aesesa'
,
'Kecamatan'
,
''
)
,
(
5170
,
23
,
301
,
4261
,
'Aesesa Selatan'
,
'Kecamatan'
,
''
)
,
(
5171
,
23
,
301
,
4262
,
'Boawae'
,
'Kecamatan'
,
''
)
,
(
5172
,
23
,
301
,
4263
,
'Keo Tengah'
,
'Kecamatan'
,
''
)
,
(
5173
,
23
,
301
,
4264
,
'Mauponggo'
,
'Kecamatan'
,
''
)
,
(
5174
,
23
,
301
,
4265
,
'Nangaroro'
,
'Kecamatan'
,
''
)
,
(
5175
,
23
,
301
,
4266
,
'Wolowae'
,
'Kecamatan'
,
''
)
,
(
5176
,
23
,
304
,
4311
,
'Aimere'
,
'Kecamatan'
,
''
)
,
(
5177
,
23
,
304
,
4312
,
'Bajawa'
,
'Kecamatan'
,
''
)
,
(
5178
,
23
,
304
,
4313
,
'Bajawa Utara'
,
'Kecamatan'
,
''
)
,
(
5179
,
23
,
304
,
4314
,
'Golewa'
,
'Kecamatan'
,
''
)
,
(
5180
,
23
,
304
,
4315
,
'Golewa Barat'
,
'Kecamatan'
,
''
)
,
(
5181
,
23
,
304
,
4316
,
'Golewa Selatan'
,
'Kecamatan'
,
''
)
,
(
5182
,
23
,
304
,
4317
,
'Inerie'
,
'Kecamatan'
,
''
)
,
(
5183
,
23
,
304
,
4318
,
'Jerebuu'
,
'Kecamatan'
,
''
)
,
(
5184
,
23
,
304
,
4319
,
'Riung'
,
'Kecamatan'
,
''
)
,
(
5185
,
23
,
304
,
4320
,
'Riung Barat'
,
'Kecamatan'
,
''
)
,
(
5186
,
23
,
304
,
4321
,
'Soa'
,
'Kecamatan'
,
''
)
,
(
5187
,
23
,
304
,
4322
,
'Wolomeze (Riung Selatan)'
,
'Kecamatan'
,
''
)
,
(
5188
,
23
,
383
,
5334
,
'Landu Leko'
,
'Kecamatan'
,
''
)
,
(
5189
,
23
,
383
,
5335
,
'Lobalain'
,
'Kecamatan'
,
''
)
,
(
5190
,
23
,
383
,
5336
,
'Ndao Nuse'
,
'Kecamatan'
,
''
)
,
(
5191
,
23
,
383
,
5337
,
'Pantai Baru'
,
'Kecamatan'
,
''
)
,
(
5192
,
23
,
383
,
5338
,
'Rote Barat'
,
'Kecamatan'
,
''
)
,
(
5193
,
23
,
383
,
5339
,
'Rote Barat Daya'
,
'Kecamatan'
,
''
)
,
(
5194
,
23
,
383
,
5340
,
'Rote Barat Laut'
,
'Kecamatan'
,
''
)
,
(
5195
,
23
,
383
,
5341
,
'Rote Selatan'
,
'Kecamatan'
,
''
)
,
(
5196
,
23
,
383
,
5342
,
'Rote Tengah'
,
'Kecamatan'
,
''
)
,
(
5197
,
23
,
383
,
5343
,
'Rote Timur'
,
'Kecamatan'
,
''
)
,
(
5198
,
23
,
385
,
5346
,
'Hawu Mehara'
,
'Kecamatan'
,
''
)
,
(
5199
,
23
,
385
,
5347
,
'Raijua'
,
'Kecamatan'
,
''
)
,
(
5200
,
23
,
385
,
5348
,
'Sabu Barat'
,
'Kecamatan'
,
''
)
,
(
5201
,
23
,
385
,
5349
,
'Sabu Liae'
,
'Kecamatan'
,
''
)
,
(
5202
,
23
,
385
,
5350
,
'Sabu Tengah'
,
'Kecamatan'
,
''
)
,
(
5203
,
23
,
385
,
5351
,
'Sabu Timur'
,
'Kecamatan'
,
''
)
,
(
5204
,
23
,
412
,
5672
,
'Alok'
,
'Kecamatan'
,
''
)
,
(
5205
,
23
,
412
,
5673
,
'Alok Barat'
,
'Kecamatan'
,
''
)
,
(
5206
,
23
,
412
,
5674
,
'Alok Timur'
,
'Kecamatan'
,
''
)
,
(
5207
,
23
,
412
,
5675
,
'Bola'
,
'Kecamatan'
,
''
)
,
(
5208
,
23
,
412
,
5676
,
'Doreng'
,
'Kecamatan'
,
''
)
,
(
5209
,
23
,
412
,
5677
,
'Hewokloang'
,
'Kecamatan'
,
''
)
,
(
5210
,
23
,
412
,
5678
,
'Kangae'
,
'Kecamatan'
,
''
)
,
(
5211
,
23
,
412
,
5679
,
'Kewapante'
,
'Kecamatan'
,
''
)
,
(
5212
,
23
,
412
,
5680
,
'Koting'
,
'Kecamatan'
,
''
)
,
(
5213
,
23
,
412
,
5681
,
'Lela'
,
'Kecamatan'
,
''
)
,
(
5214
,
23
,
412
,
5682
,
'Magepanda'
,
'Kecamatan'
,
''
)
,
(
5215
,
23
,
412
,
5683
,
'Mapitara'
,
'Kecamatan'
,
''
)
,
(
5216
,
23
,
412
,
5684
,
'Mego'
,
'Kecamatan'
,
''
)
,
(
5217
,
23
,
412
,
5685
,
'Nelle (Maumerei)'
,
'Kecamatan'
,
''
)
,
(
5218
,
23
,
412
,
5686
,
'Nita'
,
'Kecamatan'
,
''
)
,
(
5219
,
23
,
412
,
5687
,
'Paga'
,
'Kecamatan'
,
''
)
,
(
5220
,
23
,
412
,
5688
,
'Palue'
,
'Kecamatan'
,
''
)
,
(
5221
,
23
,
412
,
5689
,
'Talibura'
,
'Kecamatan'
,
''
)
,
(
5222
,
23
,
412
,
5690
,
'Tana Wawo'
,
'Kecamatan'
,
''
)
,
(
5223
,
23
,
412
,
5691
,
'Waiblama'
,
'Kecamatan'
,
''
)
,
(
5224
,
23
,
412
,
5692
,
'Waigete'
,
'Kecamatan'
,
''
)
,
(
5225
,
23
,
434
,
5989
,
'Kota Waikabubak'
,
'Kecamatan'
,
''
)
,
(
5226
,
23
,
434
,
5990
,
'Lamboya'
,
'Kecamatan'
,
''
)
,
(
5227
,
23
,
434
,
5991
,
'Lamboya Barat'
,
'Kecamatan'
,
''
)
,
(
5228
,
23
,
434
,
5992
,
'Loli'
,
'Kecamatan'
,
''
)
,
(
5229
,
23
,
434
,
5993
,
'Tana Righu'
,
'Kecamatan'
,
''
)
,
(
5230
,
23
,
434
,
5994
,
'Wanokaka'
,
'Kecamatan'
,
''
)
,
(
5231
,
23
,
435
,
5995
,
'Kodi'
,
'Kecamatan'
,
''
)
,
(
5232
,
23
,
435
,
5996
,
'Kodi Balaghar'
,
'Kecamatan'
,
''
)
,
(
5233
,
23
,
435
,
5997
,
'Kodi Bangedo'
,
'Kecamatan'
,
''
)
,
(
5234
,
23
,
435
,
5998
,
'Kodi Utara'
,
'Kecamatan'
,
''
)
,
(
5235
,
23
,
435
,
5999
,
'Kota Tambolaka'
,
'Kecamatan'
,
''
)
,
(
5236
,
23
,
435
,
6000
,
'Loura (Laura)'
,
'Kecamatan'
,
''
)
,
(
5237
,
23
,
435
,
6001
,
'Wewewa Barat'
,
'Kecamatan'
,
''
)
,
(
5238
,
23
,
435
,
6002
,
'Wewewa Selatan'
,
'Kecamatan'
,
''
)
,
(
5239
,
23
,
435
,
6003
,
'Wewewa Tengah (Wewera Tengah)'
,
'Kecamatan'
,
''
)
,
(
5240
,
23
,
435
,
6004
,
'Wewewa Timur'
,
'Kecamatan'
,
''
)
,
(
5241
,
23
,
435
,
6005
,
'Wewewa Utara'
,
'Kecamatan'
,
''
)
,
(
5242
,
23
,
436
,
6006
,
'Katikutana'
,
'Kecamatan'
,
''
)
,
(
5243
,
23
,
436
,
6007
,
'Katikutana Selatan'
,
'Kecamatan'
,
''
)
,
(
5244
,
23
,
436
,
6008
,
'Mamboro'
,
'Kecamatan'
,
''
)
,
(
5245
,
23
,
436
,
6009
,
'Umbu Ratu Nggay'
,
'Kecamatan'
,
''
)
,
(
5246
,
23
,
436
,
6010
,
'Umbu Ratu Nggay Barat'
,
'Kecamatan'
,
''
)
,
(
5247
,
23
,
437
,
6011
,
'Haharu'
,
'Kecamatan'
,
''
)
,
(
5248
,
23
,
437
,
6012
,
'Kahaunguweti (Kahaungu Eti)'
,
'Kecamatan'
,
''
)
,
(
5249
,
23
,
437
,
6013
,
'Kambata Mapambuhang'
,
'Kecamatan'
,
''
)
,
(
5250
,
23
,
437
,
6014
,
'Kambera'
,
'Kecamatan'
,
''
)
,
(
5251
,
23
,
437
,
6015
,
'Kanatang'
,
'Kecamatan'
,
''
)
,
(
5252
,
23
,
437
,
6016
,
'Karera'
,
'Kecamatan'
,
''
)
,
(
5253
,
23
,
437
,
6017
,
'Katala Hamu Lingu'
,
'Kecamatan'
,
''
)
,
(
5254
,
23
,
437
,
6018
,
'Kota Waingapu'
,
'Kecamatan'
,
''
)
,
(
5255
,
23
,
437
,
6019
,
'Lewa'
,
'Kecamatan'
,
''
)
,
(
5256
,
23
,
437
,
6020
,
'Lewa Tidahu'
,
'Kecamatan'
,
''
)
,
(
5257
,
23
,
437
,
6021
,
'Mahu'
,
'Kecamatan'
,
''
)
,
(
5258
,
23
,
437
,
6022
,
'Matawai Lappau (La Pawu)'
,
'Kecamatan'
,
''
)
,
(
5259
,
23
,
437
,
6023
,
'Ngadu Ngala'
,
'Kecamatan'
,
''
)
,
(
5260
,
23
,
437
,
6024
,
'Nggaha Oriangu'
,
'Kecamatan'
,
''
)
,
(
5261
,
23
,
437
,
6025
,
'Paberiwai'
,
'Kecamatan'
,
''
)
,
(
5262
,
23
,
437
,
6026
,
'Pahunga Lodu'
,
'Kecamatan'
,
''
)
,
(
5263
,
23
,
437
,
6027
,
'Pandawai'
,
'Kecamatan'
,
''
)
,
(
5264
,
23
,
437
,
6028
,
'Pinupahar (Pirapahar)'
,
'Kecamatan'
,
''
)
,
(
5265
,
23
,
437
,
6029
,
'Rindi'
,
'Kecamatan'
,
''
)
,
(
5266
,
23
,
437
,
6030
,
'Tabundung'
,
'Kecamatan'
,
''
)
,
(
5267
,
23
,
437
,
6031
,
'Umalulu'
,
'Kecamatan'
,
''
)
,
(
5268
,
23
,
437
,
6032
,
'Wula Waijelu'
,
'Kecamatan'
,
''
)
,
(
5269
,
23
,
479
,
6596
,
'Amanatun Selatan'
,
'Kecamatan'
,
''
)
,
(
5270
,
23
,
479
,
6597
,
'Amanatun Utara'
,
'Kecamatan'
,
''
)
,
(
5271
,
23
,
479
,
6598
,
'Amanuban Barat'
,
'Kecamatan'
,
''
)
,
(
5272
,
23
,
479
,
6599
,
'Amanuban Selatan'
,
'Kecamatan'
,
''
)
,
(
5273
,
23
,
479
,
6600
,
'Amanuban Tengah'
,
'Kecamatan'
,
''
)
,
(
5274
,
23
,
479
,
6601
,
'Amanuban Timur'
,
'Kecamatan'
,
''
)
,
(
5275
,
23
,
479
,
6602
,
'Batu Putih'
,
'Kecamatan'
,
''
)
,
(
5276
,
23
,
479
,
6603
,
'Boking'
,
'Kecamatan'
,
''
)
,
(
5277
,
23
,
479
,
6604
,
'Fatukopa'
,
'Kecamatan'
,
''
)
,
(
5278
,
23
,
479
,
6605
,
'Fatumnasi'
,
'Kecamatan'
,
''
)
,
(
5279
,
23
,
479
,
6606
,
'Fautmolo'
,
'Kecamatan'
,
''
)
,
(
5280
,
23
,
479
,
6607
,
'Kie (Ki\'
e
)
', '
Kecamatan
', ''),
(5281, 23, 479, 6608, '
Kok
Baun
', '
Kecamatan
', ''),
(5282, 23, 479, 6609, '
Kolbano
', '
Kecamatan
', ''),
(5283, 23, 479, 6610, '
Kot
Olin
', '
Kecamatan
', ''),
(5284, 23, 479, 6611, '
Kota
Soe
', '
Kecamatan
', ''),
(5285, 23, 479, 6612, '
Kualin
', '
Kecamatan
', ''),
(5286, 23, 479, 6613, '
Kuanfatu
', '
Kecamatan
', ''),
(5287, 23, 479, 6614, '
Kuatnana
', '
Kecamatan
', ''),
(5288, 23, 479, 6615, '
Mollo
Barat
', '
Kecamatan
', ''),
(5289, 23, 479, 6616, '
Mollo
Selatan
', '
Kecamatan
', ''),
(5290, 23, 479, 6617, '
Mollo
Tengah
', '
Kecamatan
', ''),
(5291, 23, 479, 6618, '
Mollo
Utara
', '
Kecamatan
', ''),
(5292, 23, 479, 6619, '
Noebana
', '
Kecamatan
', ''),
(5293, 23, 479, 6620, '
Noebeba
', '
Kecamatan
', ''),
(5294, 23, 479, 6621, '
Nunbena
', '
Kecamatan
', ''),
(5295, 23, 479, 6622, '
Nunkolo
', '
Kecamatan
', ''),
(5296, 23, 479, 6623, '
Oenino
', '
Kecamatan
', ''),
(5297, 23, 479, 6624, '
Polen
', '
Kecamatan
', ''),
(5298, 23, 479, 6625, '
Santian
', '
Kecamatan
', ''),
(5299, 23, 479, 6626, '
Tobu
', '
Kecamatan
', ''),
(5300, 23, 479, 6627, '
Toianas
', '
Kecamatan
', ''),
(5301, 23, 480, 6628, '
Biboki
Anleu
', '
Kecamatan
', ''),
(5302, 23, 480, 6629, '
Biboki
Feotleu
', '
Kecamatan
', ''),
(5303, 23, 480, 6630, '
Biboki
Moenleu
', '
Kecamatan
', ''),
(5304, 23, 480, 6631, '
Biboki
Selatan
', '
Kecamatan
', ''),
(5305, 23, 480, 6632, '
Biboki
Tan
Pah
', '
Kecamatan
', ''),
(5306, 23, 480, 6633, '
Biboki
Utara
', '
Kecamatan
', ''),
(5307, 23, 480, 6634, '
Bikomi
Nilulat
', '
Kecamatan
', ''),
(5308, 23, 480, 6635, '
Bikomi
Selatan
', '
Kecamatan
', ''),
(5309, 23, 480, 6636, '
Bikomi
Tengah
', '
Kecamatan
', ''),
(5310, 23, 480, 6637, '
Bikomi
Utara
', '
Kecamatan
', ''),
(5311, 23, 480, 6638, '
Insana
', '
Kecamatan
', ''),
(5312, 23, 480, 6639, '
Insana
Barat
', '
Kecamatan
', ''),
(5313, 23, 480, 6640, '
Insana
Fafinesu
', '
Kecamatan
', ''),
(5314, 23, 480, 6641, '
Insana
Tengah
', '
Kecamatan
', ''),
(5315, 23, 480, 6642, '
Insana
Utara
', '
Kecamatan
', ''),
(5316, 23, 480, 6643, '
Kota
Kefamenanu
', '
Kecamatan
', ''),
(5317, 23, 480, 6644, '
Miomaffo
Barat
', '
Kecamatan
', ''),
(5318, 23, 480, 6645, '
Miomaffo
Tengah
', '
Kecamatan
', ''),
(5319, 23, 480, 6646, '
Miomaffo
Timur
', '
Kecamatan
', ''),
(5320, 23, 480, 6647, '
Musi
', '
Kecamatan
', ''),
(5321, 23, 480, 6648, '
Mutis
', '
Kecamatan
', ''),
(5322, 23, 480, 6649, '
Naibenu
', '
Kecamatan
', ''),
(5323, 23, 480, 6650, '
Noemuti
', '
Kecamatan
', ''),
(5324, 23, 480, 6651, '
Noemuti
Timur
', '
Kecamatan
', ''),
(5325, 24, 16, 0, '
Asmat
', '
Kabupaten
', '
99777
'),
(5326, 24, 67, 0, '
Biak
Numfor
', '
Kabupaten
', '
98119
'),
(5327, 24, 90, 0, '
Boven
Digoel
', '
Kabupaten
', '
99662
'),
(5328, 24, 111, 0, '
Deiyai
(
Deliyai
)
', '
Kabupaten
', '
98784
'),
(5329, 24, 117, 0, '
Dogiyai
', '
Kabupaten
', '
98866
'),
(5330, 24, 150, 0, '
Intan
Jaya
', '
Kabupaten
', '
98771
'),
(5331, 24, 157, 0, '
Jayapura
', '
Kabupaten
', '
99352
'),
(5332, 24, 158, 0, '
Jayapura
', '
Kota
', '
99114
'),
(5333, 24, 159, 0, '
Jayawijaya
', '
Kabupaten
', '
99511
'),
(5334, 24, 180, 0, '
Keerom
', '
Kabupaten
', '
99461
'),
(5335, 24, 193, 0, '
Kepulauan
Yapen
(
Yapen
Waropen
)
', '
Kabupaten
', '
98211
'),
(5336, 24, 231, 0, '
Lanny
Jaya
', '
Kabupaten
', '
99531
'),
(5337, 24, 263, 0, '
Mamberamo
Raya
', '
Kabupaten
', '
99381
'),
(5338, 24, 264, 0, '
Mamberamo
Tengah
', '
Kabupaten
', '
99553
'),
(5339, 24, 274, 0, '
Mappi
', '
Kabupaten
', '
99853
'),
(5340, 24, 281, 0, '
Merauke
', '
Kabupaten
', '
99613
'),
(5341, 24, 284, 0, '
Mimika
', '
Kabupaten
', '
99962
'),
(5342, 24, 299, 0, '
Nabire
', '
Kabupaten
', '
98816
'),
(5343, 24, 303, 0, '
Nduga
', '
Kabupaten
', '
99541
'),
(5344, 24, 335, 0, '
Paniai
', '
Kabupaten
', '
98765
'),
(5345, 24, 347, 0, '
Pegunungan
Bintang
', '
Kabupaten
', '
99573
'),
(5346, 24, 373, 0, '
Puncak
', '
Kabupaten
', '
98981
'),
(5347, 24, 374, 0, '
Puncak
Jaya
', '
Kabupaten
', '
98979
'),
(5348, 24, 392, 0, '
Sarmi
', '
Kabupaten
', '
99373
'),
(5349, 24, 443, 0, '
Supiori
', '
Kabupaten
', '
98164
'),
(5350, 24, 484, 0, '
Tolikara
', '
Kabupaten
', '
99411
'),
(5351, 24, 495, 0, '
Waropen
', '
Kabupaten
', '
98269
'),
(5352, 24, 499, 0, '
Yahukimo
', '
Kabupaten
', '
99041
'),
(5353, 24, 500, 0, '
Yalimo
', '
Kabupaten
', '
99481
'),
(5354, 24, 16, 239, '
Agats
', '
Kecamatan
', ''),
(5355, 24, 16, 240, '
Akat
', '
Kecamatan
', ''),
(5356, 24, 16, 241, '
Atsy
/
Atsj
', '
Kecamatan
', ''),
(5357, 24, 16, 242, '
Ayip
', '
Kecamatan
', ''),
(5358, 24, 16, 243, '
Betcbamu
', '
Kecamatan
', ''),
(5359, 24, 16, 244, '
Der
Koumur
', '
Kecamatan
', ''),
(5360, 24, 16, 245, '
Fayit
', '
Kecamatan
', ''),
(5361, 24, 16, 246, '
Jetsy
', '
Kecamatan
', ''),
(5362, 24, 16, 247, '
Joerat
', '
Kecamatan
', ''),
(5363, 24, 16, 248, '
Kolf
Braza
', '
Kecamatan
', ''),
(5364, 24, 16, 249, '
Kopay
', '
Kecamatan
', ''),
(5365, 24, 16, 250, '
Pantai
Kasuari
', '
Kecamatan
', ''),
(5366, 24, 16, 251, '
Pulau
Tiga
', '
Kecamatan
', ''),
(5367, 24, 16, 252, '
Safan
', '
Kecamatan
', ''),
(5368, 24, 16, 253, '
Sawa
Erma
', '
Kecamatan
', ''),
(5369, 24, 16, 254, '
Sirets
', '
Kecamatan
', ''),
(5370, 24, 16, 255, '
Suator
', '
Kecamatan
', ''),
(5371, 24, 16, 256, '
Suru
-
suru
', '
Kecamatan
', ''),
(5372, 24, 16, 257, '
Unir
Sirau
', '
Kecamatan
', ''),
(5373, 24, 67, 892, '
Aimando
Padaido
', '
Kecamatan
', ''),
(5374, 24, 67, 893, '
Andey
(
Andei
)
', '
Kecamatan
', ''),
(5375, 24, 67, 894, '
Biak
Barat
', '
Kecamatan
', ''),
(5376, 24, 67, 895, '
Biak
Kota
', '
Kecamatan
', ''),
(5377, 24, 67, 896, '
Biak
Timur
', '
Kecamatan
', ''),
(5378, 24, 67, 897, '
Biak
Utara
', '
Kecamatan
', ''),
(5379, 24, 67, 898, '
Bondifuar
', '
Kecamatan
', ''),
(5380, 24, 67, 899, '
Bruyadori
', '
Kecamatan
', ''),
(5381, 24, 67, 900, '
Numfor
Barat
', '
Kecamatan
', ''),
(5382, 24, 67, 901, '
Numfor
Timur
', '
Kecamatan
', ''),
(5383, 24, 67, 902, '
Oridek
', '
Kecamatan
', ''),
(5384, 24, 67, 903, '
Orkeri
', '
Kecamatan
', ''),
(5385, 24, 67, 904, '
Padaido
', '
Kecamatan
', ''),
(5386, 24, 67, 905, '
Poiru
', '
Kecamatan
', ''),
(5387, 24, 67, 906, '
Samofa
', '
Kecamatan
', ''),
(5388, 24, 67, 907, '
Swandiwe
', '
Kecamatan
', ''),
(5389, 24, 67, 908, '
Warsa
', '
Kecamatan
', ''),
(5390, 24, 67, 909, '
Yawosi
', '
Kecamatan
', ''),
(5391, 24, 67, 910, '
Yendidori
', '
Kecamatan
', ''),
(5392, 24, 90, 1220, '
Ambatkwi
(
Ambatkui
)
', '
Kecamatan
', ''),
(5393, 24, 90, 1221, '
Arimop
', '
Kecamatan
', ''),
(5394, 24, 90, 1222, '
Bomakia
', '
Kecamatan
', ''),
(5395, 24, 90, 1223, '
Firiwage
', '
Kecamatan
', ''),
(5396, 24, 90, 1224, '
Fofi
', '
Kecamatan
', ''),
(5397, 24, 90, 1225, '
Iniyandit
', '
Kecamatan
', ''),
(5398, 24, 90, 1226, '
Jair
', '
Kecamatan
', ''),
(5399, 24, 90, 1227, '
Kawagit
', '
Kecamatan
', ''),
(5400, 24, 90, 1228, '
Ki
', '
Kecamatan
', ''),
(5401, 24, 90, 1229, '
Kombay
', '
Kecamatan
', ''),
(5402, 24, 90, 1230, '
Kombut
', '
Kecamatan
', ''),
(5403, 24, 90, 1231, '
Kouh
', '
Kecamatan
', ''),
(5404, 24, 90, 1232, '
Mandobo
', '
Kecamatan
', ''),
(5405, 24, 90, 1233, '
Manggelum
', '
Kecamatan
', ''),
(5406, 24, 90, 1234, '
Mindiptana
', '
Kecamatan
', ''),
(5407, 24, 90, 1235, '
Ninati
', '
Kecamatan
', ''),
(5408, 24, 90, 1236, '
Sesnuk
', '
Kecamatan
', ''),
(5409, 24, 90, 1237, '
Subur
', '
Kecamatan
', ''),
(5410, 24, 90, 1238, '
Waropko
', '
Kecamatan
', ''),
(5411, 24, 90, 1239, '
Yaniruma
', '
Kecamatan
', ''),
(5412, 24, 111, 1532, '
Bowobado
', '
Kecamatan
', ''),
(5413, 24, 111, 1533, '
Kapiraya
', '
Kecamatan
', ''),
(5414, 24, 111, 1534, '
Tigi
', '
Kecamatan
', ''),
(5415, 24, 111, 1535, '
Tigi
Barat
', '
Kecamatan
', ''),
(5416, 24, 111, 1536, '
Tigi
Timur
', '
Kecamatan
', ''),
(5417, 24, 117, 1599, '
Dogiyai
', '
Kecamatan
', ''),
(5418, 24, 117, 1600, '
Kamu
', '
Kecamatan
', ''),
(5419, 24, 117, 1601, '
Kamu
Selatan
', '
Kecamatan
', ''),
(5420, 24, 117, 1602, '
Kamu
Timur
', '
Kecamatan
', ''),
(5421, 24, 117, 1603, '
Kamu
Utara
(
Ikrar
/
Ikrat
)
', '
Kecamatan
', ''),
(5422, 24, 117, 1604, '
Mapia
', '
Kecamatan
', ''),
(5423, 24, 117, 1605, '
Mapia
Barat
', '
Kecamatan
', ''),
(5424, 24, 117, 1606, '
Mapia
Tengah
', '
Kecamatan
', ''),
(5425, 24, 117, 1607, '
Piyaiye
(
Sukikai
)
', '
Kecamatan
', ''),
(5426, 24, 117, 1608, '
Sukikai
Selatan
', '
Kecamatan
', ''),
(5427, 24, 150, 2081, '
Agisiga
', '
Kecamatan
', ''),
(5428, 24, 150, 2082, '
Biandoga
', '
Kecamatan
', ''),
(5429, 24, 150, 2083, '
Hitadipa
', '
Kecamatan
', ''),
(5430, 24, 150, 2084, '
Homeo
(
Homeyo
)
', '
Kecamatan
', ''),
(5431, 24, 150, 2085, '
Sugapa
', '
Kecamatan
', ''),
(5432, 24, 150, 2086, '
Wandai
', '
Kecamatan
', ''),
(5433, 24, 157, 2137, '
Airu
', '
Kecamatan
', ''),
(5434, 24, 157, 2138, '
Demta
', '
Kecamatan
', ''),
(5435, 24, 157, 2139, '
Depapre
', '
Kecamatan
', ''),
(5436, 24, 157, 2140, '
Ebungfau
(
Ebungfa
)
', '
Kecamatan
', ''),
(5437, 24, 157, 2141, '
Gresi
Selatan
', '
Kecamatan
', ''),
(5438, 24, 157, 2142, '
Kaureh
', '
Kecamatan
', ''),
(5439, 24, 157, 2143, '
Kemtuk
', '
Kecamatan
', ''),
(5440, 24, 157, 2144, '
Kemtuk
Gresi
', '
Kecamatan
', ''),
(5441, 24, 157, 2145, '
Nambluong
', '
Kecamatan
', ''),
(5442, 24, 157, 2146, '
Nimbokrang
', '
Kecamatan
', ''),
(5443, 24, 157, 2147, '
Nimboran
', '
Kecamatan
', ''),
(5444, 24, 157, 2148, '
Ravenirara
', '
Kecamatan
', ''),
(5445, 24, 157, 2149, '
Sentani
', '
Kecamatan
', ''),
(5446, 24, 157, 2150, '
Sentani
Barat
', '
Kecamatan
', ''),
(5447, 24, 157, 2151, '
Sentani
Timur
', '
Kecamatan
', ''),
(5448, 24, 157, 2152, '
Unurum
Guay
', '
Kecamatan
', ''),
(5449, 24, 157, 2153, '
Waibu
', '
Kecamatan
', ''),
(5450, 24, 157, 2154, '
Yapsi
', '
Kecamatan
', ''),
(5451, 24, 157, 2155, '
Yokari
', '
Kecamatan
', ''),
(5452, 24, 158, 2156, '
Abepura
', '
Kecamatan
', ''),
(5453, 24, 158, 2157, '
Heram
', '
Kecamatan
', ''),
(5454, 24, 158, 2158, '
Jayapura
Selatan
', '
Kecamatan
', ''),
(5455, 24, 158, 2159, '
Jayapura
Utara
', '
Kecamatan
', ''),
(5456, 24, 158, 2160, '
Muara
Tami
', '
Kecamatan
', ''),
(5457, 24, 159, 2161, '
Asologaima
(
Asalogaima
)
', '
Kecamatan
', ''),
(5458, 24, 159, 2162, '
Asolokobal
', '
Kecamatan
', ''),
(5459, 24, 159, 2163, '
Asotipo
', '
Kecamatan
', ''),
(5460, 24, 159, 2164, '
Bolakme
', '
Kecamatan
', ''),
(5461, 24, 159, 2165, '
Bpiri
', '
Kecamatan
', ''),
(5462, 24, 159, 2166, '
Bugi
', '
Kecamatan
', ''),
(5463, 24, 159, 2167, '
Hubikiak
', '
Kecamatan
', ''),
(5464, 24, 159, 2168, '
Hubikosi
(
Hobikosi
)
', '
Kecamatan
', ''),
(5465, 24, 159, 2169, '
Ibele
', '
Kecamatan
', ''),
(5466, 24, 159, 2170, '
Itlay
Hisage
', '
Kecamatan
', ''),
(5467, 24, 159, 2171, '
Koragi
', '
Kecamatan
', ''),
(5468, 24, 159, 2172, '
Kurulu
', '
Kecamatan
', ''),
(5469, 24, 159, 2173, '
Libarek
', '
Kecamatan
', ''),
(5470, 24, 159, 2174, '
Maima
', '
Kecamatan
', ''),
(5471, 24, 159, 2175, '
Molagalome
', '
Kecamatan
', ''),
(5472, 24, 159, 2176, '
Muliama
', '
Kecamatan
', ''),
(5473, 24, 159, 2177, '
Musatfak
', '
Kecamatan
', ''),
(5474, 24, 159, 2178, '
Napua
', '
Kecamatan
', ''),
(5475, 24, 159, 2179, '
Pelebaga
', '
Kecamatan
', ''),
(5476, 24, 159, 2180, '
Piramid
', '
Kecamatan
', ''),
(5477, 24, 159, 2181, '
Pisugi
', '
Kecamatan
', ''),
(5478, 24, 159, 2182, '
Popugoba
', '
Kecamatan
', ''),
(5479, 24, 159, 2183, '
Siepkosi
', '
Kecamatan
', ''),
(5480, 24, 159, 2184, '
Silo
Karno
Doga
', '
Kecamatan
', ''),
(5481, 24, 159, 2185, '
Taelarek
', '
Kecamatan
', ''),
(5482, 24, 159, 2186, '
Tagime
', '
Kecamatan
', ''),
(5483, 24, 159, 2187, '
Tagineri
', '
Kecamatan
', ''),
(5484, 24, 159, 2188, '
Trikora
', '
Kecamatan
', ''),
(5485, 24, 159, 2189, '
Usilimo
', '
Kecamatan
', ''),
(5486, 24, 159, 2190, '
Wadangku
', '
Kecamatan
', ''),
(5487, 24, 159, 2191, '
Walaik
', '
Kecamatan
', ''),
(5488, 24, 159, 2192, '
Walelagama
', '
Kecamatan
', ''),
(5489, 24, 159, 2193, '
Wame
', '
Kecamatan
', ''),
(5490, 24, 159, 2194, '
Wamena
', '
Kecamatan
', ''),
(5491, 24, 159, 2195, '
Welesi
', '
Kecamatan
', ''),
(5492, 24, 159, 2196, '
Wesaput
', '
Kecamatan
', ''),
(5493, 24, 159, 2197, '
Wita
Waya
', '
Kecamatan
', ''),
(5494, 24, 159, 2198, '
Wollo
(
Wolo
)
', '
Kecamatan
', ''),
(5495, 24, 159, 2199, '
Wouma
', '
Kecamatan
', ''),
(5496, 24, 159, 2200, '
Yalengga
', '
Kecamatan
', ''),
(5497, 24, 180, 2526, '
Arso
', '
Kecamatan
', ''),
(5498, 24, 180, 2527, '
Arso
Timur
', '
Kecamatan
', ''),
(5499, 24, 180, 2528, '
Senggi
', '
Kecamatan
', ''),
(5500, 24, 180, 2529, '
Skamto
(
Skanto
)
', '
Kecamatan
', ''),
(5501, 24, 180, 2530, '
Towe
', '
Kecamatan
', ''),
(5502, 24, 180, 2531, '
Waris
', '
Kecamatan
', ''),
(5503, 24, 180, 2532, '
Web
', '
Kecamatan
', ''),
(5504, 24, 193, 2672, '
Angkaisera
', '
Kecamatan
', ''),
(5505, 24, 193, 2673, '
Kepulauan
Ambai
', '
Kecamatan
', ''),
(5506, 24, 193, 2674, '
Kosiwo
', '
Kecamatan
', ''),
(5507, 24, 193, 2675, '
Poom
', '
Kecamatan
', ''),
(5508, 24, 193, 2676, '
Pulau
Kurudu
', '
Kecamatan
', ''),
(5509, 24, 193, 2677, '
Pulau
Yerui
', '
Kecamatan
', ''),
(5510, 24, 193, 2678, '
Raimbawi
', '
Kecamatan
', ''),
(5511, 24, 193, 2679, '
Teluk
Ampimoi
', '
Kecamatan
', ''),
(5512, 24, 193, 2680, '
Windesi
', '
Kecamatan
', ''),
(5513, 24, 193, 2681, '
Wonawa
', '
Kecamatan
', ''),
(5514, 24, 193, 2682, '
Yapen
Barat
', '
Kecamatan
', ''),
(5515, 24, 193, 2683, '
Yapen
Selatan
', '
Kecamatan
', ''),
(5516, 24, 193, 2684, '
Yapen
Timur
', '
Kecamatan
', ''),
(5517, 24, 193, 2685, '
Yapen
Utara
', '
Kecamatan
', ''),
(5518, 24, 231, 3288, '
Balingga
', '
Kecamatan
', ''),
(5519, 24, 231, 3289, '
Dimba
', '
Kecamatan
', ''),
(5520, 24, 231, 3290, '
Gamelia
', '
Kecamatan
', ''),
(5521, 24, 231, 3291, '
Kuyawage
', '
Kecamatan
', ''),
(5522, 24, 231, 3292, '
Makki
(
Maki
)
', '
Kecamatan
', ''),
(5523, 24, 231, 3293, '
Malagaineri
(
Malagineri
)
', '
Kecamatan
', ''),
(5524, 24, 231, 3294, '
Pirime
', '
Kecamatan
', ''),
(5525, 24, 231, 3295, '
Poga
', '
Kecamatan
', ''),
(5526, 24, 231, 3296, '
Tiom
', '
Kecamatan
', ''),
(5527, 24, 231, 3297, '
Tiomneri
', '
Kecamatan
', ''),
(5528, 24, 263, 3727, '
Benuki
', '
Kecamatan
', ''),
(5529, 24, 263, 3728, '
Mamberamo
Hilir
/
Ilir
', '
Kecamatan
', ''),
(5530, 24, 263, 3729, '
Mamberamo
Hulu
/
Ulu
', '
Kecamatan
', ''),
(5531, 24, 263, 3730, '
Mamberamo
Tengah
', '
Kecamatan
', ''),
(5532, 24, 263, 3731, '
Mamberamo
Tengah
Timur
', '
Kecamatan
', ''),
(5533, 24, 263, 3732, '
Rofaer
(
Rufaer
)
', '
Kecamatan
', ''),
(5534, 24, 263, 3733, '
Sawai
', '
Kecamatan
', ''),
(5535, 24, 263, 3734, '
Waropen
Atas
', '
Kecamatan
', ''),
(5536, 24, 264, 3735, '
Eragayam
', '
Kecamatan
', ''),
(5537, 24, 264, 3736, '
Ilugwa
', '
Kecamatan
', ''),
(5538, 24, 264, 3737, '
Kelila
', '
Kecamatan
', ''),
(5539, 24, 264, 3738, '
Kobakma
', '
Kecamatan
', ''),
(5540, 24, 264, 3739, '
Megabilis
(
Megambilis
)
', '
Kecamatan
', ''),
(5541, 24, 274, 3847, '
Assue
', '
Kecamatan
', ''),
(5542, 24, 274, 3848, '
Bamgi
', '
Kecamatan
', ''),
(5543, 24, 274, 3849, '
Citakmitak
', '
Kecamatan
', ''),
(5544, 24, 274, 3850, '
Edera
', '
Kecamatan
', ''),
(5545, 24, 274, 3851, '
Haju
', '
Kecamatan
', ''),
(5546, 24, 274, 3852, '
Kaibar
', '
Kecamatan
', ''),
(5547, 24, 274, 3853, '
Minyamur
', '
Kecamatan
', ''),
(5548, 24, 274, 3854, '
Nambioman
Bapai
(
Mambioman
)
', '
Kecamatan
', ''),
(5549, 24, 274, 3855, '
Obaa
', '
Kecamatan
', ''),
(5550, 24, 274, 3856, '
Passue
', '
Kecamatan
', ''),
(5551, 24, 274, 3857, '
Passue
Bawah
', '
Kecamatan
', ''),
(5552, 24, 274, 3858, '
Syahcame
', '
Kecamatan
', ''),
(5553, 24, 274, 3859, '
Ti
Zain
', '
Kecamatan
', ''),
(5554, 24, 274, 3860, '
Venaha
', '
Kecamatan
', ''),
(5555, 24, 274, 3861, '
Yakomi
', '
Kecamatan
', ''),
(5556, 24, 281, 3962, '
Animha
', '
Kecamatan
', ''),
(5557, 24, 281, 3963, '
Eligobel
', '
Kecamatan
', ''),
(5558, 24, 281, 3964, '
Ilyawab
', '
Kecamatan
', ''),
(5559, 24, 281, 3965, '
Jagebob
', '
Kecamatan
', ''),
(5560, 24, 281, 3966, '
Kaptel
', '
Kecamatan
', ''),
(5561, 24, 281, 3967, '
Kimaam
', '
Kecamatan
', ''),
(5562, 24, 281, 3968, '
Kurik
', '
Kecamatan
', ''),
(5563, 24, 281, 3969, '
Malind
', '
Kecamatan
', ''),
(5564, 24, 281, 3970, '
Merauke
', '
Kecamatan
', ''),
(5565, 24, 281, 3971, '
Muting
', '
Kecamatan
', ''),
(5566, 24, 281, 3972, '
Naukenjerai
', '
Kecamatan
', ''),
(5567, 24, 281, 3973, '
Ngguti
(
Nggunti
)
', '
Kecamatan
', ''),
(5568, 24, 281, 3974, '
Okaba
', '
Kecamatan
', ''),
(5569, 24, 281, 3975, '
Semangga
', '
Kecamatan
', ''),
(5570, 24, 281, 3976, '
Sota
', '
Kecamatan
', ''),
(5571, 24, 281, 3977, '
Tabonji
', '
Kecamatan
', ''),
(5572, 24, 281, 3978, '
Tanah
Miring
', '
Kecamatan
', ''),
(5573, 24, 281, 3979, '
Tubang
', '
Kecamatan
', ''),
(5574, 24, 281, 3980, '
Ulilin
', '
Kecamatan
', ''),
(5575, 24, 281, 3981, '
Waan
', '
Kecamatan
', ''),
(5576, 24, 284, 3994, '
Agimuga
', '
Kecamatan
', ''),
(5577, 24, 284, 3995, '
Jila
', '
Kecamatan
', ''),
(5578, 24, 284, 3996, '
Jita
', '
Kecamatan
', ''),
(5579, 24, 284, 3997, '
Kuala
Kencana
', '
Kecamatan
', ''),
(5580, 24, 284, 3998, '
Mimika
Barat
(
Mibar
)
', '
Kecamatan
', ''),
(5581, 24, 284, 3999, '
Mimika
Barat
Jauh
', '
Kecamatan
', ''),
(5582, 24, 284, 4000, '
Mimika
Barat
Tengah
', '
Kecamatan
', ''),
(5583, 24, 284, 4001, '
Mimika
Baru
', '
Kecamatan
', ''),
(5584, 24, 284, 4002, '
Mimika
Timur
', '
Kecamatan
', ''),
(5585, 24, 284, 4003, '
Mimika
Timur
Jauh
', '
Kecamatan
', ''),
(5586, 24, 284, 4004, '
Mimika
Timur
Tengah
', '
Kecamatan
', ''),
(5587, 24, 284, 4005, '
Tembagapura
', '
Kecamatan
', ''),
(5588, 24, 299, 4235, '
Dipa
', '
Kecamatan
', ''),
(5589, 24, 299, 4236, '
Makimi
', '
Kecamatan
', ''),
(5590, 24, 299, 4237, '
Menou
', '
Kecamatan
', ''),
(5591, 24, 299, 4238, '
Moora
', '
Kecamatan
', ''),
(5592, 24, 299, 4239, '
Nabire
', '
Kecamatan
', ''),
(5593, 24, 299, 4240, '
Nabire
Barat
', '
Kecamatan
', ''),
(5594, 24, 299, 4241, '
Napan
', '
Kecamatan
', ''),
(5595, 24, 299, 4242, '
Siriwo
', '
Kecamatan
', ''),
(5596, 24, 299, 4243, '
Teluk
Kimi
', '
Kecamatan
', ''),
(5597, 24, 299, 4244, '
Teluk
Umar
', '
Kecamatan
', ''),
(5598, 24, 299, 4245, '
Uwapa
', '
Kecamatan
', ''),
(5599, 24, 299, 4246, '
Wanggar
', '
Kecamatan
', ''),
(5600, 24, 299, 4247, '
Wapoga
', '
Kecamatan
', ''),
(5601, 24, 299, 4248, '
Yaro
(
Yaro
Kabisay
)
', '
Kecamatan
', ''),
(5602, 24, 299, 4249, '
Yaur
', '
Kecamatan
', ''),
(5603, 24, 303, 4279, '
Alama
', '
Kecamatan
', ''),
(5604, 24, 303, 4280, '
Dal
', '
Kecamatan
', ''),
(5605, 24, 303, 4281, '
Embetpen
', '
Kecamatan
', ''),
(5606, 24, 303, 4282, '
Gearek
', '
Kecamatan
', ''),
(5607, 24, 303, 4283, '
Geselma
(
Geselema
)
', '
Kecamatan
', ''),
(5608, 24, 303, 4284, '
Inikgal
', '
Kecamatan
', ''),
(5609, 24, 303, 4285, '
Iniye
', '
Kecamatan
', ''),
(5610, 24, 303, 4286, '
Kegayem
', '
Kecamatan
', ''),
(5611, 24, 303, 4287, '
Kenyam
', '
Kecamatan
', ''),
(5612, 24, 303, 4288, '
Kilmid
', '
Kecamatan
', ''),
(5613, 24, 303, 4289, '
Kora
', '
Kecamatan
', ''),
(5614, 24, 303, 4290, '
Koroptak
', '
Kecamatan
', ''),
(5615, 24, 303, 4291, '
Krepkuri
', '
Kecamatan
', ''),
(5616, 24, 303, 4292, '
Mam
', '
Kecamatan
', ''),
(5617, 24, 303, 4293, '
Mapenduma
', '
Kecamatan
', ''),
(5618, 24, 303, 4294, '
Mbua
(
Mbuga
)
', '
Kecamatan
', ''),
(5619, 24, 303, 4295, '
Mbua
Tengah
', '
Kecamatan
', ''),
(5620, 24, 303, 4296, '
Mbulmu
Yalma
', '
Kecamatan
', ''),
(5621, 24, 303, 4297, '
Mebarok
', '
Kecamatan
', ''),
(5622, 24, 303, 4298, '
Moba
', '
Kecamatan
', ''),
(5623, 24, 303, 4299, '
Mugi
', '
Kecamatan
', ''),
(5624, 24, 303, 4300, '
Nenggeagin
', '
Kecamatan
', ''),
(5625, 24, 303, 4301, '
Nirkuri
', '
Kecamatan
', ''),
(5626, 24, 303, 4302, '
Paro
', '
Kecamatan
', ''),
(5627, 24, 303, 4303, '
Pasir
Putih
', '
Kecamatan
', ''),
(5628, 24, 303, 4304, '
Pija
', '
Kecamatan
', ''),
(5629, 24, 303, 4305, '
Wosak
', '
Kecamatan
', ''),
(5630, 24, 303, 4306, '
Wusi
', '
Kecamatan
', ''),
(5631, 24, 303, 4307, '
Wutpaga
', '
Kecamatan
', ''),
(5632, 24, 303, 4308, '
Yal
', '
Kecamatan
', ''),
(5633, 24, 303, 4309, '
Yenggelo
', '
Kecamatan
', ''),
(5634, 24, 303, 4310, '
Yigi
', '
Kecamatan
', ''),
(5635, 24, 335, 4720, '
Aradide
', '
Kecamatan
', ''),
(5636, 24, 335, 4721, '
Bibida
', '
Kecamatan
', ''),
(5637, 24, 335, 4722, '
Bogobaida
', '
Kecamatan
', ''),
(5638, 24, 335, 4723, '
Dumadama
', '
Kecamatan
', ''),
(5639, 24, 335, 4724, '
Ekadide
', '
Kecamatan
', ''),
(5640, 24, 335, 4725, '
Kebo
', '
Kecamatan
', ''),
(5641, 24, 335, 4726, '
Paniai
Barat
', '
Kecamatan
', ''),
(5642, 24, 335, 4727, '
Paniai
Timur
', '
Kecamatan
', ''),
(5643, 24, 335, 4728, '
Siriwo
', '
Kecamatan
', ''),
(5644, 24, 335, 4729, '
Yatamo
', '
Kecamatan
', ''),
(5645, 24, 347, 4857, '
Aboy
', '
Kecamatan
', ''),
(5646, 24, 347, 4858, '
Alemsom
', '
Kecamatan
', ''),
(5647, 24, 347, 4859, '
Awinbon
', '
Kecamatan
', ''),
(5648, 24, 347, 4860, '
Batani
', '
Kecamatan
', ''),
(5649, 24, 347, 4861, '
Batom
', '
Kecamatan
', ''),
(5650, 24, 347, 4862, '
Bime
', '
Kecamatan
', ''),
(5651, 24, 347, 4863, '
Borme
', '
Kecamatan
', ''),
(5652, 24, 347, 4864, '
Eipumek
', '
Kecamatan
', ''),
(5653, 24, 347, 4865, '
Iwur
(
Okiwur
)
', '
Kecamatan
', ''),
(5654, 24, 347, 4866, '
Jetfa
', '
Kecamatan
', ''),
(5655, 24, 347, 4867, '
Kalomdol
', '
Kecamatan
', ''),
(5656, 24, 347, 4868, '
Kawor
', '
Kecamatan
', ''),
(5657, 24, 347, 4869, '
Kiwirok
', '
Kecamatan
', ''),
(5658, 24, 347, 4870, '
Kiwirok
Timur
', '
Kecamatan
', ''),
(5659, 24, 347, 4871, '
Mofinop
', '
Kecamatan
', ''),
(5660, 24, 347, 4872, '
Murkim
', '
Kecamatan
', ''),
(5661, 24, 347, 4873, '
Nongme
', '
Kecamatan
', ''),
(5662, 24, 347, 4874, '
Ok
Aom
', '
Kecamatan
', ''),
(5663, 24, 347, 4875, '
Okbab
', '
Kecamatan
', ''),
(5664, 24, 347, 4876, '
Okbape
', '
Kecamatan
', ''),
(5665, 24, 347, 4877, '
Okbemtau
', '
Kecamatan
', ''),
(5666, 24, 347, 4878, '
Okbibab
', '
Kecamatan
', ''),
(5667, 24, 347, 4879, '
Okhika
', '
Kecamatan
', ''),
(5668, 24, 347, 4880, '
Oklip
', '
Kecamatan
', ''),
(5669, 24, 347, 4881, '
Oksamol
', '
Kecamatan
', ''),
(5670, 24, 347, 4882, '
Oksebang
', '
Kecamatan
', ''),
(5671, 24, 347, 4883, '
Oksibil
', '
Kecamatan
', ''),
(5672, 24, 347, 4884, '
Oksop
', '
Kecamatan
', ''),
(5673, 24, 347, 4885, '
Pamek
', '
Kecamatan
', ''),
(5674, 24, 347, 4886, '
Pepera
', '
Kecamatan
', ''),
(5675, 24, 347, 4887, '
Serambakon
', '
Kecamatan
', ''),
(5676, 24, 347, 4888, '
Tarup
', '
Kecamatan
', ''),
(5677, 24, 347, 4889, '
Teiraplu
', '
Kecamatan
', ''),
(5678, 24, 347, 4890, '
Weime
', '
Kecamatan
', ''),
(5679, 24, 373, 5183, '
Agadugume
', '
Kecamatan
', ''),
(5680, 24, 373, 5184, '
Beoga
', '
Kecamatan
', ''),
(5681, 24, 373, 5185, '
Doufu
', '
Kecamatan
', ''),
(5682, 24, 373, 5186, '
Gome
', '
Kecamatan
', ''),
(5683, 24, 373, 5187, '
Ilaga
', '
Kecamatan
', ''),
(5684, 24, 373, 5188, '
Pogoma
', '
Kecamatan
', ''),
(5685, 24, 373, 5189, '
Sinak
', '
Kecamatan
', ''),
(5686, 24, 373, 5190, '
Wangbe
', '
Kecamatan
', ''),
(5687, 24, 374, 5191, '
Fawi
', '
Kecamatan
', ''),
(5688, 24, 374, 5192, '
Ilu
', '
Kecamatan
', ''),
(5689, 24, 374, 5193, '
Jigonikme
', '
Kecamatan
', ''),
(5690, 24, 374, 5194, '
Mewoluk
(
Mewulok
)
', '
Kecamatan
', ''),
(5691, 24, 374, 5195, '
Mulia
', '
Kecamatan
', ''),
(5692, 24, 374, 5196, '
Tingginambut
', '
Kecamatan
', ''),
(5693, 24, 374, 5197, '
Torere
', '
Kecamatan
', ''),
(5694, 24, 374, 5198, '
Yamo
', '
Kecamatan
', ''),
(5695, 24, 392, 5423, '
Apawer
Hulu
', '
Kecamatan
', ''),
(5696, 24, 392, 5424, '
Bonggo
', '
Kecamatan
', ''),
(5697, 24, 392, 5425, '
Bonggo
Timur
', '
Kecamatan
', ''),
(5698, 24, 392, 5426, '
Pantai
Barat
', '
Kecamatan
', ''),
(5699, 24, 392, 5427, '
Pantai
Timur
', '
Kecamatan
', ''),
(5700, 24, 392, 5428, '
Pantai
Timur
Barat
', '
Kecamatan
', ''),
(5701, 24, 392, 5429, '
Sarmi
', '
Kecamatan
', ''),
(5702, 24, 392, 5430, '
Sarmi
Selatan
', '
Kecamatan
', ''),
(5703, 24, 392, 5431, '
Sarmi
Timur
', '
Kecamatan
', ''),
(5704, 24, 392, 5432, '
Tor
Atas
', '
Kecamatan
', ''),
(5705, 24, 443, 6126, '
Kepulauan
Aruri
', '
Kecamatan
', ''),
(5706, 24, 443, 6127, '
Supiori
Barat
', '
Kecamatan
', ''),
(5707, 24, 443, 6128, '
Supiori
Selatan
', '
Kecamatan
', ''),
(5708, 24, 443, 6129, '
Supiori
Timur
', '
Kecamatan
', ''),
(5709, 24, 443, 6130, '
Supiori
Utara
', '
Kecamatan
', ''),
(5710, 24, 484, 6687, '
Airgaram
', '
Kecamatan
', ''),
(5711, 24, 484, 6688, '
Anawi
', '
Kecamatan
', ''),
(5712, 24, 484, 6689, '
Aweku
', '
Kecamatan
', ''),
(5713, 24, 484, 6690, '
Bewani
', '
Kecamatan
', ''),
(5714, 24, 484, 6691, '
Biuk
', '
Kecamatan
', ''),
(5715, 24, 484, 6692, '
Bogonuk
', '
Kecamatan
', ''),
(5716, 24, 484, 6693, '
Bokondini
', '
Kecamatan
', ''),
(5717, 24, 484, 6694, '
Bokoneri
', '
Kecamatan
', ''),
(5718, 24, 484, 6695, '
Danime
', '
Kecamatan
', ''),
(5719, 24, 484, 6696, '
Dow
', '
Kecamatan
', ''),
(5720, 24, 484, 6697, '
Dundu
(
Ndundu
)
', '
Kecamatan
', ''),
(5721, 24, 484, 6698, '
Egiam
', '
Kecamatan
', ''),
(5722, 24, 484, 6699, '
Geya
', '
Kecamatan
', ''),
(5723, 24, 484, 6700, '
Gika
', '
Kecamatan
', ''),
(5724, 24, 484, 6701, '
Gilubandu
(
Gilumbandu
/
Gilimbandu
)
', '
Kecamatan
', ''),
(5725, 24, 484, 6702, '
Goyage
', '
Kecamatan
', ''),
(5726, 24, 484, 6703, '
Gundagi
(
Gudage
)
', '
Kecamatan
', ''),
(5727, 24, 484, 6704, '
Kai
', '
Kecamatan
', ''),
(5728, 24, 484, 6705, '
Kamboneri
', '
Kecamatan
', ''),
(5729, 24, 484, 6706, '
Kanggime
(
Kanggima
)
', '
Kecamatan
', ''),
(5730, 24, 484, 6707, '
Karubaga
', '
Kecamatan
', ''),
(5731, 24, 484, 6708, '
Kembu
', '
Kecamatan
', ''),
(5732, 24, 484, 6709, '
Kondaga
(
Konda
)
', '
Kecamatan
', ''),
(5733, 24, 484, 6710, '
Kuari
', '
Kecamatan
', ''),
(5734, 24, 484, 6711, '
Kubu
', '
Kecamatan
', ''),
(5735, 24, 484, 6712, '
Li
Anogomma
', '
Kecamatan
', ''),
(5736, 24, 484, 6713, '
Nabunage
', '
Kecamatan
', ''),
(5737, 24, 484, 6714, '
Nelawi
', '
Kecamatan
', ''),
(5738, 24, 484, 6715, '
Numba
', '
Kecamatan
', ''),
(5739, 24, 484, 6716, '
Nunggawi
(
Munggawi
)
', '
Kecamatan
', ''),
(5740, 24, 484, 6717, '
Panaga
', '
Kecamatan
', ''),
(5741, 24, 484, 6718, '
Poganeri
', '
Kecamatan
', ''),
(5742, 24, 484, 6719, '
Tagime
', '
Kecamatan
', ''),
(5743, 24, 484, 6720, '
Tagineri
', '
Kecamatan
', ''),
(5744, 24, 484, 6721, '
Telenggeme
', '
Kecamatan
', ''),
(5745, 24, 484, 6722, '
Timori
', '
Kecamatan
', ''),
(5746, 24, 484, 6723, '
Umagi
', '
Kecamatan
', ''),
(5747, 24, 484, 6724, '
Wakuwo
', '
Kecamatan
', ''),
(5748, 24, 484, 6725, '
Wari
(
Taiyeve
)
', '
Kecamatan
', ''),
(5749, 24, 484, 6726, '
Wenam
', '
Kecamatan
', ''),
(5750, 24, 484, 6727, '
Wina
', '
Kecamatan
', ''),
(5751, 24, 484, 6728, '
Wonoki
(
Woniki
)
', '
Kecamatan
', ''),
(5752, 24, 484, 6729, '
Wugi
', '
Kecamatan
', ''),
(5753, 24, 484, 6730, '
Wunin
(
Wumin
)
', '
Kecamatan
', ''),
(5754, 24, 484, 6731, '
Yuko
', '
Kecamatan
', ''),
(5755, 24, 484, 6732, '
Yuneri
', '
Kecamatan
', ''),
(5756, 24, 495, 6862, '
Demba
Masirei
', '
Kecamatan
', ''),
(5757, 24, 495, 6863, '
Inggerus
', '
Kecamatan
', ''),
(5758, 24, 495, 6864, '
Kirihi
', '
Kecamatan
', ''),
(5759, 24, 495, 6865, '
Masirei
', '
Kecamatan
', ''),
(5760, 24, 495, 6866, '
Oudate
Waropen
', '
Kecamatan
', ''),
(5761, 24, 495, 6867, '
Risei
Sayati
', '
Kecamatan
', ''),
(5762, 24, 495, 6868, '
Ureifasei
', '
Kecamatan
', ''),
(5763, 24, 495, 6869, '
Wapoga
Inggerus
', '
Kecamatan
', ''),
(5764, 24, 495, 6870, '
Waropen
Bawah
', '
Kecamatan
', ''),
(5765, 24, 499, 6925, '
Amuma
', '
Kecamatan
', ''),
(5766, 24, 499, 6926, '
Anggruk
', '
Kecamatan
', ''),
(5767, 24, 499, 6927, '
Bomela
', '
Kecamatan
', ''),
(5768, 24, 499, 6928, '
Dekai
', '
Kecamatan
', ''),
(5769, 24, 499, 6929, '
Dirwemna
(
Diruwena
)
', '
Kecamatan
', ''),
(5770, 24, 499, 6930, '
Duram
', '
Kecamatan
', ''),
(5771, 24, 499, 6931, '
Endomen
', '
Kecamatan
', ''),
(5772, 24, 499, 6932, '
Hereapini
(
Hereanini
)
', '
Kecamatan
', ''),
(5773, 24, 499, 6933, '
Hilipuk
', '
Kecamatan
', ''),
(5774, 24, 499, 6934, '
Hogio
(
Hugio
)
', '
Kecamatan
', ''),
(5775, 24, 499, 6935, '
Holuon
', '
Kecamatan
', ''),
(5776, 24, 499, 6936, '
Kabianggama
(
Kabianggema
)
', '
Kecamatan
', ''),
(5777, 24, 499, 6937, '
Kayo
', '
Kecamatan
', ''),
(5778, 24, 499, 6938, '
Kona
', '
Kecamatan
', ''),
(5779, 24, 499, 6939, '
Koropun
(
Korupun
)
', '
Kecamatan
', ''),
(5780, 24, 499, 6940, '
Kosarek
', '
Kecamatan
', ''),
(5781, 24, 499, 6941, '
Kurima
', '
Kecamatan
', ''),
(5782, 24, 499, 6942, '
Kwelemdua
(
Kwelamdua
)
', '
Kecamatan
', ''),
(5783, 24, 499, 6943, '
Kwikma
', '
Kecamatan
', ''),
(5784, 24, 499, 6944, '
Langda
', '
Kecamatan
', ''),
(5785, 24, 499, 6945, '
Lolat
', '
Kecamatan
', ''),
(5786, 24, 499, 6946, '
Mugi
', '
Kecamatan
', ''),
(5787, 24, 499, 6947, '
Musaik
', '
Kecamatan
', ''),
(5788, 24, 499, 6948, '
Nalca
', '
Kecamatan
', ''),
(5789, 24, 499, 6949, '
Ninia
', '
Kecamatan
', ''),
(5790, 24, 499, 6950, '
Nipsan
', '
Kecamatan
', ''),
(5791, 24, 499, 6951, '
Obio
', '
Kecamatan
', ''),
(5792, 24, 499, 6952, '
Panggema
', '
Kecamatan
', ''),
(5793, 24, 499, 6953, '
Pasema
', '
Kecamatan
', ''),
(5794, 24, 499, 6954, '
Pronggoli
(
Proggoli
)
', '
Kecamatan
', ''),
(5795, 24, 499, 6955, '
Puldama
', '
Kecamatan
', ''),
(5796, 24, 499, 6956, '
Samenage
', '
Kecamatan
', ''),
(5797, 24, 499, 6957, '
Sela
', '
Kecamatan
', ''),
(5798, 24, 499, 6958, '
Seredela
(
Seredala
)
', '
Kecamatan
', ''),
(5799, 24, 499, 6959, '
Silimo
', '
Kecamatan
', ''),
(5800, 24, 499, 6960, '
Soba
', '
Kecamatan
', ''),
(5801, 24, 499, 6961, '
Sobaham
', '
Kecamatan
', ''),
(5802, 24, 499, 6962, '
Soloikma
', '
Kecamatan
', ''),
(5803, 24, 499, 6963, '
Sumo
', '
Kecamatan
', ''),
(5804, 24, 499, 6964, '
Suntamon
', '
Kecamatan
', ''),
(5805, 24, 499, 6965, '
Suru
Suru
', '
Kecamatan
', ''),
(5806, 24, 499, 6966, '
Talambo
', '
Kecamatan
', ''),
(5807, 24, 499, 6967, '
Tangma
', '
Kecamatan
', ''),
(5808, 24, 499, 6968, '
Ubahak
', '
Kecamatan
', ''),
(5809, 24, 499, 6969, '
Ubalihi
', '
Kecamatan
', ''),
(5810, 24, 499, 6970, '
Ukha
', '
Kecamatan
', ''),
(5811, 24, 499, 6971, '
Walma
', '
Kecamatan
', ''),
(5812, 24, 499, 6972, '
Werima
', '
Kecamatan
', ''),
(5813, 24, 499, 6973, '
Wusuma
', '
Kecamatan
', ''),
(5814, 24, 499, 6974, '
Yahuliambut
', '
Kecamatan
', ''),
(5815, 24, 499, 6975, '
Yogosem
', '
Kecamatan
', ''),
(5816, 24, 500, 6976, '
Abenaho
', '
Kecamatan
', ''),
(5817, 24, 500, 6977, '
Apalapsili
', '
Kecamatan
', ''),
(5818, 24, 500, 6978, '
Benawa
', '
Kecamatan
', ''),
(5819, 24, 500, 6979, '
Elelim
', '
Kecamatan
', ''),
(5820, 24, 500, 6980, '
Welarek
', '
Kecamatan
', ''),
(5821, 25, 124, 0, '
Fakfak
', '
Kabupaten
', '
98651
'),
(5822, 25, 165, 0, '
Kaimana
', '
Kabupaten
', '
98671
'),
(5823, 25, 272, 0, '
Manokwari
', '
Kabupaten
', '
98311
'),
(5824, 25, 273, 0, '
Manokwari
Selatan
', '
Kabupaten
', '
98355
'),
(5825, 25, 277, 0, '
Maybrat
', '
Kabupaten
', '
98051
'),
(5826, 25, 346, 0, '
Pegunungan
Arfak
', '
Kabupaten
', '
98354
'),
(5827, 25, 378, 0, '
Raja
Ampat
', '
Kabupaten
', '
98489
'),
(5828, 25, 424, 0, '
Sorong
', '
Kabupaten
', '
98431
'),
(5829, 25, 425, 0, '
Sorong
', '
Kota
', '
98411
'),
(5830, 25, 426, 0, '
Sorong
Selatan
', '
Kabupaten
', '
98454
'),
(5831, 25, 449, 0, '
Tambrauw
', '
Kabupaten
', '
98475
'),
(5832, 25, 474, 0, '
Teluk
Bintuni
', '
Kabupaten
', '
98551
'),
(5833, 25, 475, 0, '
Teluk
Wondama
', '
Kabupaten
', '
98591
'),
(5834, 25, 124, 1683, '
Bombarai
(
Bomberay
)
', '
Kecamatan
', ''),
(5835, 25, 124, 1684, '
Fakfak
', '
Kecamatan
', ''),
(5836, 25, 124, 1685, '
Fakfak
Barat
', '
Kecamatan
', ''),
(5837, 25, 124, 1686, '
Fakfak
Tengah
', '
Kecamatan
', ''),
(5838, 25, 124, 1687, '
Fakfak
Timur
', '
Kecamatan
', ''),
(5839, 25, 124, 1688, '
Karas
', '
Kecamatan
', ''),
(5840, 25, 124, 1689, '
Kokas
', '
Kecamatan
', ''),
(5841, 25, 124, 1690, '
Kramongmongga
(
Kramamongga
)
', '
Kecamatan
', ''),
(5842, 25, 124, 1691, '
Teluk
Patipi
', '
Kecamatan
', ''),
(5843, 25, 165, 2285, '
Buruway
', '
Kecamatan
', ''),
(5844, 25, 165, 2286, '
Kaimana
', '
Kecamatan
', ''),
(5845, 25, 165, 2287, '
Kambraw
(
Kamberau
)
', '
Kecamatan
', ''),
(5846, 25, 165, 2288, '
Teluk
Arguni
Atas
', '
Kecamatan
', ''),
(5847, 25, 165, 2289, '
Teluk
Arguni
Bawah
(
Yerusi
)
', '
Kecamatan
', ''),
(5848, 25, 165, 2290, '
Teluk
Etna
', '
Kecamatan
', ''),
(5849, 25, 165, 2291, '
Yamor
', '
Kecamatan
', ''),
(5850, 25, 272, 3832, '
Manokwari
Barat
', '
Kecamatan
', ''),
(5851, 25, 272, 3833, '
Manokwari
Selatan
', '
Kecamatan
', ''),
(5852, 25, 272, 3834, '
Manokwari
Timur
', '
Kecamatan
', ''),
(5853, 25, 272, 3835, '
Manokwari
Utara
', '
Kecamatan
', ''),
(5854, 25, 272, 3836, '
Masni
', '
Kecamatan
', ''),
(5855, 25, 272, 3837, '
Prafi
', '
Kecamatan
', ''),
(5856, 25, 272, 3838, '
Sidey
', '
Kecamatan
', ''),
(5857, 25, 272, 3839, '
Tanah
Rubuh
', '
Kecamatan
', ''),
(5858, 25, 272, 3840, '
Warmare
', '
Kecamatan
', ''),
(5859, 25, 273, 3841, '
Dataran
Isim
', '
Kecamatan
', ''),
(5860, 25, 273, 3842, '
Momi
Waren
', '
Kecamatan
', ''),
(5861, 25, 273, 3843, '
Neney
(
Nenei
)
', '
Kecamatan
', '');
INSERT INTO `location` (`location_id`, `province_id`, `city_id`, `subdistrict_id`, `name`, `type`, `postcode`) VALUES
(5862, 25, 273, 3844, '
Oransbari
', '
Kecamatan
', ''),
(5863, 25, 273, 3845, '
Ransiki
', '
Kecamatan
', ''),
(5864, 25, 273, 3846, '
Tahota
(
Tohota
)
', '
Kecamatan
', ''),
(5865, 25, 277, 3882, '
Aifat
', '
Kecamatan
', ''),
(5866, 25, 277, 3883, '
Aifat
Selatan
', '
Kecamatan
', ''),
(5867, 25, 277, 3884, '
Aifat
Timur
', '
Kecamatan
', ''),
(5868, 25, 277, 3885, '
Aifat
Timur
Jauh
', '
Kecamatan
', ''),
(5869, 25, 277, 3886, '
Aifat
Timur
Selatan
', '
Kecamatan
', ''),
(5870, 25, 277, 3887, '
Aifat
Timur
Tengah
', '
Kecamatan
', ''),
(5871, 25, 277, 3888, '
Aifat
Utara
', '
Kecamatan
', ''),
(5872, 25, 277, 3889, '
Aitinyo
', '
Kecamatan
', ''),
(5873, 25, 277, 3890, '
Aitinyo
Barat
', '
Kecamatan
', ''),
(5874, 25, 277, 3891, '
Aitinyo
Raya
', '
Kecamatan
', ''),
(5875, 25, 277, 3892, '
Aitinyo
Tengah
', '
Kecamatan
', ''),
(5876, 25, 277, 3893, '
Aitinyo
Utara
', '
Kecamatan
', ''),
(5877, 25, 277, 3894, '
Ayamaru
', '
Kecamatan
', ''),
(5878, 25, 277, 3895, '
Ayamaru
Barat
', '
Kecamatan
', ''),
(5879, 25, 277, 3896, '
Ayamaru
Jaya
', '
Kecamatan
', ''),
(5880, 25, 277, 3897, '
Ayamaru
Selatan
', '
Kecamatan
', ''),
(5881, 25, 277, 3898, '
Ayamaru
Selatan
Jaya
', '
Kecamatan
', ''),
(5882, 25, 277, 3899, '
Ayamaru
Tengah
', '
Kecamatan
', ''),
(5883, 25, 277, 3900, '
Ayamaru
Timur
', '
Kecamatan
', ''),
(5884, 25, 277, 3901, '
Ayamaru
Timur
Selatan
', '
Kecamatan
', ''),
(5885, 25, 277, 3902, '
Ayamaru
Utara
', '
Kecamatan
', ''),
(5886, 25, 277, 3903, '
Ayamaru
Utara
Timur
', '
Kecamatan
', ''),
(5887, 25, 277, 3904, '
Mare
', '
Kecamatan
', ''),
(5888, 25, 277, 3905, '
Mare
Selatan
', '
Kecamatan
', ''),
(5889, 25, 346, 4847, '
Anggi
', '
Kecamatan
', ''),
(5890, 25, 346, 4848, '
Anggi
Gida
', '
Kecamatan
', ''),
(5891, 25, 346, 4849, '
Catubouw
', '
Kecamatan
', ''),
(5892, 25, 346, 4850, '
Didohu
', '
Kecamatan
', ''),
(5893, 25, 346, 4851, '
Hingk
', '
Kecamatan
', ''),
(5894, 25, 346, 4852, '
Membey
', '
Kecamatan
', ''),
(5895, 25, 346, 4853, '
Menyambouw
(
Minyambouw
)
', '
Kecamatan
', ''),
(5896, 25, 346, 4854, '
Sururey
', '
Kecamatan
', ''),
(5897, 25, 346, 4855, '
Taige
', '
Kecamatan
', ''),
(5898, 25, 346, 4856, '
Testega
', '
Kecamatan
', ''),
(5899, 25, 378, 5250, '
Ayau
', '
Kecamatan
', ''),
(5900, 25, 378, 5251, '
Batanta
Selatan
', '
Kecamatan
', ''),
(5901, 25, 378, 5252, '
Batanta
Utara
', '
Kecamatan
', ''),
(5902, 25, 378, 5253, '
Kepulauan
Ayau
', '
Kecamatan
', ''),
(5903, 25, 378, 5254, '
Kepulauan
Sembilan
', '
Kecamatan
', ''),
(5904, 25, 378, 5255, '
Kofiau
', '
Kecamatan
', ''),
(5905, 25, 378, 5256, '
Kota
Waisai
', '
Kecamatan
', ''),
(5906, 25, 378, 5257, '
Meos
Mansar
', '
Kecamatan
', ''),
(5907, 25, 378, 5258, '
Misool
(
Misool
Utara
)
', '
Kecamatan
', ''),
(5908, 25, 378, 5259, '
Misool
Barat
', '
Kecamatan
', ''),
(5909, 25, 378, 5260, '
Misool
Selatan
', '
Kecamatan
', ''),
(5910, 25, 378, 5261, '
Misool
Timur
', '
Kecamatan
', ''),
(5911, 25, 378, 5262, '
Salawati
Barat
', '
Kecamatan
', ''),
(5912, 25, 378, 5263, '
Salawati
Tengah
', '
Kecamatan
', ''),
(5913, 25, 378, 5264, '
Salawati
Utara
(
Samate
)
', '
Kecamatan
', ''),
(5914, 25, 378, 5265, '
Supnin
', '
Kecamatan
', ''),
(5915, 25, 378, 5266, '
Teluk
Mayalibit
', '
Kecamatan
', ''),
(5916, 25, 378, 5267, '
Tiplol
Mayalibit
', '
Kecamatan
', ''),
(5917, 25, 378, 5268, '
Waigeo
Barat
', '
Kecamatan
', ''),
(5918, 25, 378, 5269, '
Waigeo
Barat
Kepulauan
', '
Kecamatan
', ''),
(5919, 25, 378, 5270, '
Waigeo
Selatan
', '
Kecamatan
', ''),
(5920, 25, 378, 5271, '
Waigeo
Timur
', '
Kecamatan
', ''),
(5921, 25, 378, 5272, '
Waigeo
Utara
', '
Kecamatan
', ''),
(5922, 25, 378, 5273, '
Warwabomi
', '
Kecamatan
', ''),
(5923, 25, 424, 5827, '
Aimas
', '
Kecamatan
', ''),
(5924, 25, 424, 5828, '
Beraur
', '
Kecamatan
', ''),
(5925, 25, 424, 5829, '
Klabot
', '
Kecamatan
', ''),
(5926, 25, 424, 5830, '
Klamono
', '
Kecamatan
', ''),
(5927, 25, 424, 5831, '
Klaso
', '
Kecamatan
', ''),
(5928, 25, 424, 5832, '
Klawak
', '
Kecamatan
', ''),
(5929, 25, 424, 5833, '
Klayili
', '
Kecamatan
', ''),
(5930, 25, 424, 5834, '
Makbon
', '
Kecamatan
', ''),
(5931, 25, 424, 5835, '
Mariat
', '
Kecamatan
', ''),
(5932, 25, 424, 5836, '
Maudus
', '
Kecamatan
', ''),
(5933, 25, 424, 5837, '
Mayamuk
', '
Kecamatan
', ''),
(5934, 25, 424, 5838, '
Moisegen
', '
Kecamatan
', ''),
(5935, 25, 424, 5839, '
Salawati
', '
Kecamatan
', ''),
(5936, 25, 424, 5840, '
Salawati
Selatan
', '
Kecamatan
', ''),
(5937, 25, 424, 5841, '
Sayosa
', '
Kecamatan
', ''),
(5938, 25, 424, 5842, '
Seget
', '
Kecamatan
', ''),
(5939, 25, 424, 5843, '
Segun
', '
Kecamatan
', ''),
(5940, 25, 425, 5844, '
Sorong
', '
Kecamatan
', ''),
(5941, 25, 425, 5845, '
Sorong
Barat
', '
Kecamatan
', ''),
(5942, 25, 425, 5846, '
Sorong
Kepulauan
', '
Kecamatan
', ''),
(5943, 25, 425, 5847, '
Sorong
Manoi
', '
Kecamatan
', ''),
(5944, 25, 425, 5848, '
Sorong
Timur
', '
Kecamatan
', ''),
(5945, 25, 425, 5849, '
Sorong
Utara
', '
Kecamatan
', ''),
(5946, 25, 426, 5850, '
Fokour
', '
Kecamatan
', ''),
(5947, 25, 426, 5851, '
Inanwatan
(
Inawatan
)
', '
Kecamatan
', ''),
(5948, 25, 426, 5852, '
Kais
(
Matemani
Kais
)
', '
Kecamatan
', ''),
(5949, 25, 426, 5853, '
Kokoda
', '
Kecamatan
', ''),
(5950, 25, 426, 5854, '
Kokoda
Utara
', '
Kecamatan
', ''),
(5951, 25, 426, 5855, '
Konda
', '
Kecamatan
', ''),
(5952, 25, 426, 5856, '
Matemani
', '
Kecamatan
', ''),
(5953, 25, 426, 5857, '
Moswaren
', '
Kecamatan
', ''),
(5954, 25, 426, 5858, '
Saifi
', '
Kecamatan
', ''),
(5955, 25, 426, 5859, '
Sawiat
', '
Kecamatan
', ''),
(5956, 25, 426, 5860, '
Seremuk
', '
Kecamatan
', ''),
(5957, 25, 426, 5861, '
Teminabuan
', '
Kecamatan
', ''),
(5958, 25, 426, 5862, '
Wayer
', '
Kecamatan
', ''),
(5959, 25, 449, 6198, '
Abun
', '
Kecamatan
', ''),
(5960, 25, 449, 6199, '
Amberbaken
', '
Kecamatan
', ''),
(5961, 25, 449, 6200, '
Fef
(
Peef
)
', '
Kecamatan
', ''),
(5962, 25, 449, 6201, '
Kebar
', '
Kecamatan
', ''),
(5963, 25, 449, 6202, '
Kwoor
', '
Kecamatan
', ''),
(5964, 25, 449, 6203, '
Miyah
(
Meyah
)
', '
Kecamatan
', ''),
(5965, 25, 449, 6204, '
Moraid
', '
Kecamatan
', ''),
(5966, 25, 449, 6205, '
Mubrani
', '
Kecamatan
', ''),
(5967, 25, 449, 6206, '
Sausapor
', '
Kecamatan
', ''),
(5968, 25, 449, 6207, '
Senopi
', '
Kecamatan
', ''),
(5969, 25, 449, 6208, '
Syujak
', '
Kecamatan
', ''),
(5970, 25, 449, 6209, '
Yembun
', '
Kecamatan
', ''),
(5971, 25, 474, 6524, '
Aranday
', '
Kecamatan
', ''),
(5972, 25, 474, 6525, '
Aroba
', '
Kecamatan
', ''),
(5973, 25, 474, 6526, '
Babo
', '
Kecamatan
', ''),
(5974, 25, 474, 6527, '
Bintuni
', '
Kecamatan
', ''),
(5975, 25, 474, 6528, '
Biscoop
', '
Kecamatan
', ''),
(5976, 25, 474, 6529, '
Dataran
Beimes
', '
Kecamatan
', ''),
(5977, 25, 474, 6530, '
Fafurwar
(
Irorutu
)
', '
Kecamatan
', ''),
(5978, 25, 474, 6531, '
Kaitaro
', '
Kecamatan
', ''),
(5979, 25, 474, 6532, '
Kamundan
', '
Kecamatan
', ''),
(5980, 25, 474, 6533, '
Kuri
', '
Kecamatan
', ''),
(5981, 25, 474, 6534, '
Manimeri
', '
Kecamatan
', ''),
(5982, 25, 474, 6535, '
Masyeta
', '
Kecamatan
', ''),
(5983, 25, 474, 6536, '
Mayado
', '
Kecamatan
', ''),
(5984, 25, 474, 6537, '
Merdey
', '
Kecamatan
', ''),
(5985, 25, 474, 6538, '
Moskona
Barat
', '
Kecamatan
', ''),
(5986, 25, 474, 6539, '
Moskona
Selatan
', '
Kecamatan
', ''),
(5987, 25, 474, 6540, '
Moskona
Timur
', '
Kecamatan
', ''),
(5988, 25, 474, 6541, '
Moskona
Utara
', '
Kecamatan
', ''),
(5989, 25, 474, 6542, '
Sumuri
(
Simuri
)
', '
Kecamatan
', ''),
(5990, 25, 474, 6543, '
Tembuni
', '
Kecamatan
', ''),
(5991, 25, 474, 6544, '
Tomu
', '
Kecamatan
', ''),
(5992, 25, 474, 6545, '
Tuhiba
', '
Kecamatan
', ''),
(5993, 25, 474, 6546, '
Wamesa
(
Idoor
)
', '
Kecamatan
', ''),
(5994, 25, 474, 6547, '
Weriagar
', '
Kecamatan
', ''),
(5995, 25, 475, 6548, '
Kuri
Wamesa
', '
Kecamatan
', ''),
(5996, 25, 475, 6549, '
Naikere
(
Wasior
Barat
)
', '
Kecamatan
', ''),
(5997, 25, 475, 6550, '
Nikiwar
', '
Kecamatan
', ''),
(5998, 25, 475, 6551, '
Rasiei
', '
Kecamatan
', ''),
(5999, 25, 475, 6552, '
Roon
', '
Kecamatan
', ''),
(6000, 25, 475, 6553, '
Roswar
', '
Kecamatan
', ''),
(6001, 25, 475, 6554, '
Rumberpon
', '
Kecamatan
', ''),
(6002, 25, 475, 6555, '
Soug
Jaya
', '
Kecamatan
', ''),
(6003, 25, 475, 6556, '
Teluk
Duairi
(
Wasior
Utara
)
', '
Kecamatan
', ''),
(6004, 25, 475, 6557, '
Wamesa
', '
Kecamatan
', ''),
(6005, 25, 475, 6558, '
Wasior
', '
Kecamatan
', ''),
(6006, 25, 475, 6559, '
Windesi
', '
Kecamatan
', ''),
(6007, 25, 475, 6560, '
Wondiboy
(
Wasior
Selatan
)
', '
Kecamatan
', ''),
(6008, 26, 60, 0, '
Bengkalis
', '
Kabupaten
', '
28719
'),
(6009, 26, 120, 0, '
Dumai
', '
Kota
', '
28811
'),
(6010, 26, 147, 0, '
Indragiri
Hilir
', '
Kabupaten
', '
29212
'),
(6011, 26, 148, 0, '
Indragiri
Hulu
', '
Kabupaten
', '
29319
'),
(6012, 26, 166, 0, '
Kampar
', '
Kabupaten
', '
28411
'),
(6013, 26, 187, 0, '
Kepulauan
Meranti
', '
Kabupaten
', '
28791
'),
(6014, 26, 207, 0, '
Kuantan
Singingi
', '
Kabupaten
', '
29519
'),
(6015, 26, 350, 0, '
Pekanbaru
', '
Kota
', '
28112
'),
(6016, 26, 351, 0, '
Pelalawan
', '
Kabupaten
', '
28311
'),
(6017, 26, 381, 0, '
Rokan
Hilir
', '
Kabupaten
', '
28992
'),
(6018, 26, 382, 0, '
Rokan
Hulu
', '
Kabupaten
', '
28511
'),
(6019, 26, 406, 0, '
Siak
', '
Kabupaten
', '
28623
'),
(6020, 26, 60, 807, '
Bantan
', '
Kecamatan
', ''),
(6021, 26, 60, 808, '
Bengkalis
', '
Kecamatan
', ''),
(6022, 26, 60, 809, '
Bukit
Batu
', '
Kecamatan
', ''),
(6023, 26, 60, 810, '
Mandau
', '
Kecamatan
', ''),
(6024, 26, 60, 811, '
Pinggir
', '
Kecamatan
', ''),
(6025, 26, 60, 812, '
Rupat
', '
Kecamatan
', ''),
(6026, 26, 60, 813, '
Rupat
Utara
', '
Kecamatan
', ''),
(6027, 26, 60, 814, '
Siak
Kecil
', '
Kecamatan
', ''),
(6028, 26, 120, 1633, '
Bukit
Kapur
', '
Kecamatan
', ''),
(6029, 26, 120, 1634, '
Dumai
Barat
', '
Kecamatan
', ''),
(6030, 26, 120, 1635, '
Dumai
Kota
', '
Kecamatan
', ''),
(6031, 26, 120, 1636, '
Dumai
Selatan
', '
Kecamatan
', ''),
(6032, 26, 120, 1637, '
Dumai
Timur
', '
Kecamatan
', ''),
(6033, 26, 120, 1638, '
Medang
Kampai
', '
Kecamatan
', ''),
(6034, 26, 120, 1639, '
Sungai
Sembilan
', '
Kecamatan
', ''),
(6035, 26, 147, 2016, '
Batang
Tuaka
', '
Kecamatan
', ''),
(6036, 26, 147, 2017, '
Concong
', '
Kecamatan
', ''),
(6037, 26, 147, 2018, '
Enok
', '
Kecamatan
', ''),
(6038, 26, 147, 2019, '
Gaung
', '
Kecamatan
', ''),
(6039, 26, 147, 2020, '
Gaung
Anak
Serka
', '
Kecamatan
', ''),
(6040, 26, 147, 2021, '
Kateman
', '
Kecamatan
', ''),
(6041, 26, 147, 2022, '
Kempas
', '
Kecamatan
', ''),
(6042, 26, 147, 2023, '
Kemuning
', '
Kecamatan
', ''),
(6043, 26, 147, 2024, '
Keritang
', '
Kecamatan
', ''),
(6044, 26, 147, 2025, '
Kuala
Indragiri
', '
Kecamatan
', ''),
(6045, 26, 147, 2026, '
Mandah
', '
Kecamatan
', ''),
(6046, 26, 147, 2027, '
Pelangiran
', '
Kecamatan
', ''),
(6047, 26, 147, 2028, '
Pulau
Burung
', '
Kecamatan
', ''),
(6048, 26, 147, 2029, '
Reteh
', '
Kecamatan
', ''),
(6049, 26, 147, 2030, '
Sungai
Batang
', '
Kecamatan
', ''),
(6050, 26, 147, 2031, '
Tanah
Merah
', '
Kecamatan
', ''),
(6051, 26, 147, 2032, '
Teluk
Belengkong
', '
Kecamatan
', ''),
(6052, 26, 147, 2033, '
Tembilahan
', '
Kecamatan
', ''),
(6053, 26, 147, 2034, '
Tembilahan
Hulu
', '
Kecamatan
', ''),
(6054, 26, 147, 2035, '
Tempuling
', '
Kecamatan
', ''),
(6055, 26, 148, 2036, '
Batang
Cenaku
', '
Kecamatan
', ''),
(6056, 26, 148, 2037, '
Batang
Gansal
', '
Kecamatan
', ''),
(6057, 26, 148, 2038, '
Batang
Peranap
', '
Kecamatan
', ''),
(6058, 26, 148, 2039, '
Kelayang
', '
Kecamatan
', ''),
(6059, 26, 148, 2040, '
Kuala
Cenaku
', '
Kecamatan
', ''),
(6060, 26, 148, 2041, '
Lirik
', '
Kecamatan
', ''),
(6061, 26, 148, 2042, '
Lubuk
Batu
Jaya
', '
Kecamatan
', ''),
(6062, 26, 148, 2043, '
Pasir
Penyu
', '
Kecamatan
', ''),
(6063, 26, 148, 2044, '
Peranap
', '
Kecamatan
', ''),
(6064, 26, 148, 2045, '
Rakit
Kulim
', '
Kecamatan
', ''),
(6065, 26, 148, 2046, '
Rengat
', '
Kecamatan
', ''),
(6066, 26, 148, 2047, '
Rengat
Barat
', '
Kecamatan
', ''),
(6067, 26, 148, 2048, '
Seberida
', '
Kecamatan
', ''),
(6068, 26, 148, 2049, '
Sungai
Lala
', '
Kecamatan
', ''),
(6069, 26, 166, 2292, '
Bangkinang
', '
Kecamatan
', ''),
(6070, 26, 166, 2293, '
Bangkinang
Seberang
', '
Kecamatan
', ''),
(6071, 26, 166, 2294, '
Gunung
Sahilan
', '
Kecamatan
', ''),
(6072, 26, 166, 2295, '
Kampar
', '
Kecamatan
', ''),
(6073, 26, 166, 2296, '
Kampar
Kiri
', '
Kecamatan
', ''),
(6074, 26, 166, 2297, '
Kampar
Kiri
Hilir
', '
Kecamatan
', ''),
(6075, 26, 166, 2298, '
Kampar
Kiri
Hulu
', '
Kecamatan
', ''),
(6076, 26, 166, 2299, '
Kampar
Kiri
Tengah
', '
Kecamatan
', ''),
(6077, 26, 166, 2300, '
Kampar
Timur
', '
Kecamatan
', ''),
(6078, 26, 166, 2301, '
Kampar
Utara
', '
Kecamatan
', ''),
(6079, 26, 166, 2302, '
Koto
Kampar
Hulu
', '
Kecamatan
', ''),
(6080, 26, 166, 2303, '
Kuok
(
Bangkinang
Barat
)
', '
Kecamatan
', ''),
(6081, 26, 166, 2304, '
Perhentian
Raja
', '
Kecamatan
', ''),
(6082, 26, 166, 2305, '
Rumbio
Jaya
', '
Kecamatan
', ''),
(6083, 26, 166, 2306, '
Salo
', '
Kecamatan
', ''),
(6084, 26, 166, 2307, '
Siak
Hulu
', '
Kecamatan
', ''),
(6085, 26, 166, 2308, '
Tambang
', '
Kecamatan
', ''),
(6086, 26, 166, 2309, '
Tapung
', '
Kecamatan
', ''),
(6087, 26, 166, 2310, '
Tapung
Hilir
', '
Kecamatan
', ''),
(6088, 26, 166, 2311, '
Tapung
Hulu
', '
Kecamatan
', ''),
(6089, 26, 166, 2312, '
XIII
Koto
Kampar
', '
Kecamatan
', ''),
(6090, 26, 187, 2598, '
Merbau
', '
Kecamatan
', ''),
(6091, 26, 187, 2599, '
Pulaumerbau
', '
Kecamatan
', ''),
(6092, 26, 187, 2600, '
Rangsang
', '
Kecamatan
', ''),
(6093, 26, 187, 2601, '
Rangsang
Barat
', '
Kecamatan
', ''),
(6094, 26, 187, 2602, '
Rangsang
Pesisir
', '
Kecamatan
', ''),
(6095, 26, 187, 2603, '
Tasik
Putri
Puyu
', '
Kecamatan
', ''),
(6096, 26, 187, 2604, '
Tebing
Tinggi
', '
Kecamatan
', ''),
(6097, 26, 187, 2605, '
Tebing
Tinggi
Barat
', '
Kecamatan
', ''),
(6098, 26, 187, 2606, '
Tebing
Tinggi
Timur
', '
Kecamatan
', ''),
(6099, 26, 207, 2897, '
Benai
', '
Kecamatan
', ''),
(6100, 26, 207, 2898, '
Cerenti
', '
Kecamatan
', ''),
(6101, 26, 207, 2899, '
Gunung
Toar
', '
Kecamatan
', ''),
(6102, 26, 207, 2900, '
Hulu
Kuantan
', '
Kecamatan
', ''),
(6103, 26, 207, 2901, '
Inuman
', '
Kecamatan
', ''),
(6104, 26, 207, 2902, '
Kuantan
Hilir
', '
Kecamatan
', ''),
(6105, 26, 207, 2903, '
Kuantan
Hilir
Seberang
', '
Kecamatan
', ''),
(6106, 26, 207, 2904, '
Kuantan
Mudik
', '
Kecamatan
', ''),
(6107, 26, 207, 2905, '
Kuantan
Tengah
', '
Kecamatan
', ''),
(6108, 26, 207, 2906, '
Logas
Tanah
Darat
', '
Kecamatan
', ''),
(6109, 26, 207, 2907, '
Pangean
', '
Kecamatan
', ''),
(6110, 26, 207, 2908, '
Pucuk
Rantau
', '
Kecamatan
', ''),
(6111, 26, 207, 2909, '
Sentajo
Raya
', '
Kecamatan
', ''),
(6112, 26, 207, 2910, '
Singingi
', '
Kecamatan
', ''),
(6113, 26, 207, 2911, '
Singingi
Hilir
', '
Kecamatan
', ''),
(6114, 26, 350, 4914, '
Bukit
Raya
', '
Kecamatan
', ''),
(6115, 26, 350, 4915, '
Lima
Puluh
', '
Kecamatan
', ''),
(6116, 26, 350, 4916, '
Marpoyan
Damai
', '
Kecamatan
', ''),
(6117, 26, 350, 4917, '
Payung
Sekaki
', '
Kecamatan
', ''),
(6118, 26, 350, 4918, '
Pekanbaru
Kota
', '
Kecamatan
', ''),
(6119, 26, 350, 4919, '
Rumbai
', '
Kecamatan
', ''),
(6120, 26, 350, 4920, '
Rumbai
Pesisir
', '
Kecamatan
', ''),
(6121, 26, 350, 4921, '
Sail
', '
Kecamatan
', ''),
(6122, 26, 350, 4922, '
Senapelan
', '
Kecamatan
', ''),
(6123, 26, 350, 4923, '
Sukajadi
', '
Kecamatan
', ''),
(6124, 26, 350, 4924, '
Tampan
', '
Kecamatan
', ''),
(6125, 26, 350, 4925, '
Tenayan
Raya
', '
Kecamatan
', ''),
(6126, 26, 351, 4926, '
Bandar
Petalangan
', '
Kecamatan
', ''),
(6127, 26, 351, 4927, '
Bandar
Sei
Kijang
', '
Kecamatan
', ''),
(6128, 26, 351, 4928, '
Bunut
', '
Kecamatan
', ''),
(6129, 26, 351, 4929, '
Kerumutan
', '
Kecamatan
', ''),
(6130, 26, 351, 4930, '
Kuala
Kampar
', '
Kecamatan
', ''),
(6131, 26, 351, 4931, '
Langgam
', '
Kecamatan
', ''),
(6132, 26, 351, 4932, '
Pangkalan
Kerinci
', '
Kecamatan
', ''),
(6133, 26, 351, 4933, '
Pangkalan
Kuras
', '
Kecamatan
', ''),
(6134, 26, 351, 4934, '
Pangkalan
Lesung
', '
Kecamatan
', ''),
(6135, 26, 351, 4935, '
Pelalawan
', '
Kecamatan
', ''),
(6136, 26, 351, 4936, '
Teluk
Meranti
', '
Kecamatan
', ''),
(6137, 26, 351, 4937, '
Ukui
', '
Kecamatan
', ''),
(6138, 26, 382, 5318, '
Bangun
Purba
', '
Kecamatan
', ''),
(6139, 26, 382, 5319, '
Bonai
Darussalam
', '
Kecamatan
', ''),
(6140, 26, 382, 5320, '
Kabun
', '
Kecamatan
', ''),
(6141, 26, 382, 5321, '
Kepenuhan
', '
Kecamatan
', ''),
(6142, 26, 382, 5322, '
Kepenuhan
Hulu
', '
Kecamatan
', ''),
(6143, 26, 382, 5323, '
Kunto
Darussalam
', '
Kecamatan
', ''),
(6144, 26, 382, 5324, '
Pagaran
Tapah
Darussalam
', '
Kecamatan
', ''),
(6145, 26, 382, 5325, '
Pendalian
IV
Koto
', '
Kecamatan
', ''),
(6146, 26, 382, 5326, '
Rambah
', '
Kecamatan
', ''),
(6147, 26, 382, 5327, '
Rambah
Hilir
', '
Kecamatan
', ''),
(6148, 26, 382, 5328, '
Rambah
Samo
', '
Kecamatan
', ''),
(6149, 26, 382, 5329, '
Rokan
IV
Koto
', '
Kecamatan
', ''),
(6150, 26, 382, 5330, '
Tambusai
', '
Kecamatan
', ''),
(6151, 26, 382, 5331, '
Tambusai
Utara
', '
Kecamatan
', ''),
(6152, 26, 382, 5332, '
Tandun
', '
Kecamatan
', ''),
(6153, 26, 382, 5333, '
Ujung
Batu
', '
Kecamatan
', ''),
(6154, 26, 381, 5303, '
Bagan
Sinembah
', '
Kecamatan
', ''),
(6155, 26, 381, 5304, '
Bangko
', '
Kecamatan
', ''),
(6156, 26, 381, 5305, '
Bangko
Pusaka
(
Pusako
)
', '
Kecamatan
', ''),
(6157, 26, 381, 5306, '
Batu
Hampar
', '
Kecamatan
', ''),
(6158, 26, 381, 5307, '
Kubu
', '
Kecamatan
', ''),
(6159, 26, 381, 5308, '
Kubu
Babussalam
', '
Kecamatan
', ''),
(6160, 26, 381, 5309, '
Pasir
Limau
Kapas
', '
Kecamatan
', ''),
(6161, 26, 381, 5310, '
Pekaitan
', '
Kecamatan
', ''),
(6162, 26, 381, 5311, '
Pujud
', '
Kecamatan
', ''),
(6163, 26, 381, 5312, '
Rantau
Kopar
', '
Kecamatan
', ''),
(6164, 26, 381, 5313, '
Rimba
Melintang
', '
Kecamatan
', ''),
(6165, 26, 381, 5314, '
Simpang
Kanan
', '
Kecamatan
', ''),
(6166, 26, 381, 5315, '
Sinaboi
(
Senaboi
)
', '
Kecamatan
', ''),
(6167, 26, 381, 5316, '
Tanah
Putih
', '
Kecamatan
', ''),
(6168, 26, 381, 5317, '
Tanah
Putih
Tanjung
Melawan
', '
Kecamatan
', ''),
(6169, 26, 406, 5602, '
Bunga
Raya
', '
Kecamatan
', ''),
(6170, 26, 406, 5603, '
Dayun
', '
Kecamatan
', ''),
(6171, 26, 406, 5604, '
Kandis
', '
Kecamatan
', ''),
(6172, 26, 406, 5605, '
Kerinci
Kanan
', '
Kecamatan
', ''),
(6173, 26, 406, 5606, '
Koto
Gasib
', '
Kecamatan
', ''),
(6174, 26, 406, 5607, '
Lubuk
Dalam
', '
Kecamatan
', ''),
(6175, 26, 406, 5608, '
Mempura
', '
Kecamatan
', ''),
(6176, 26, 406, 5609, '
Minas
', '
Kecamatan
', ''),
(6177, 26, 406, 5610, '
Pusako
', '
Kecamatan
', ''),
(6178, 26, 406, 5611, '
Sabak
Auh
', '
Kecamatan
', ''),
(6179, 26, 406, 5612, '
Siak
', '
Kecamatan
', ''),
(6180, 26, 406, 5613, '
Sungai
Apit
', '
Kecamatan
', ''),
(6181, 26, 406, 5614, '
Sungai
Mandau
', '
Kecamatan
', ''),
(6182, 26, 406, 5615, '
Tualang
', '
Kecamatan
', ''),
(6183, 27, 253, 0, '
Majene
', '
Kabupaten
', '
91411
'),
(6184, 27, 262, 0, '
Mamasa
', '
Kabupaten
', '
91362
'),
(6185, 27, 265, 0, '
Mamuju
', '
Kabupaten
', '
91519
'),
(6186, 27, 266, 0, '
Mamuju
Utara
', '
Kabupaten
', '
91571
'),
(6187, 27, 362, 0, '
Polewali
Mandar
', '
Kabupaten
', '
91311
'),
(6188, 27, 253, 3579, '
Banggae
', '
Kecamatan
', ''),
(6189, 27, 253, 3580, '
Banggae
Timur
', '
Kecamatan
', ''),
(6190, 27, 253, 3581, '
Malunda
', '
Kecamatan
', ''),
(6191, 27, 253, 3582, '
Pamboang
', '
Kecamatan
', ''),
(6192, 27, 253, 3583, '
Sendana
', '
Kecamatan
', ''),
(6193, 27, 253, 3584, '
Tammeredo
Sendana
', '
Kecamatan
', ''),
(6194, 27, 253, 3585, '
Tubo
(
Tubo
Sendana
)
', '
Kecamatan
', ''),
(6195, 27, 253, 3586, '
Ulumunda
', '
Kecamatan
', ''),
(6196, 27, 262, 3710, '
Aralle
(
Arrale
)
', '
Kecamatan
', ''),
(6197, 27, 262, 3711, '
Balla
', '
Kecamatan
', ''),
(6198, 27, 262, 3712, '
Bambang
', '
Kecamatan
', ''),
(6199, 27, 262, 3713, '
Buntumalangka
', '
Kecamatan
', ''),
(6200, 27, 262, 3714, '
Mamasa
', '
Kecamatan
', ''),
(6201, 27, 262, 3715, '
Mambi
', '
Kecamatan
', ''),
(6202, 27, 262, 3716, '
Mehalaan
', '
Kecamatan
', ''),
(6203, 27, 262, 3717, '
Messawa
', '
Kecamatan
', ''),
(6204, 27, 262, 3718, '
Nosu
', '
Kecamatan
', ''),
(6205, 27, 262, 3719, '
Pana
', '
Kecamatan
', ''),
(6206, 27, 262, 3720, '
Rantebulahan
Timur
', '
Kecamatan
', ''),
(6207, 27, 262, 3721, '
Sesena
Padang
', '
Kecamatan
', ''),
(6208, 27, 262, 3722, '
Sumarorong
', '
Kecamatan
', ''),
(6209, 27, 262, 3723, '
Tabang
', '
Kecamatan
', ''),
(6210, 27, 262, 3724, '
Tabulahan
', '
Kecamatan
', ''),
(6211, 27, 262, 3725, '
Tanduk
Kalua
', '
Kecamatan
', ''),
(6212, 27, 262, 3726, '
Tawalian
', '
Kecamatan
', ''),
(6213, 27, 265, 3740, '
Bonehau
', '
Kecamatan
', ''),
(6214, 27, 265, 3741, '
Budong
-
Budong
', '
Kecamatan
', ''),
(6215, 27, 265, 3742, '
Kalukku
', '
Kecamatan
', ''),
(6216, 27, 265, 3743, '
Kalumpang
', '
Kecamatan
', ''),
(6217, 27, 265, 3744, '
Karossa
', '
Kecamatan
', ''),
(6218, 27, 265, 3745, '
Kep
.
Bala
Balakang
', '
Kecamatan
', ''),
(6219, 27, 265, 3746, '
Mamuju
', '
Kecamatan
', ''),
(6220, 27, 265, 3747, '
Pangale
', '
Kecamatan
', ''),
(6221, 27, 265, 3748, '
Papalang
', '
Kecamatan
', ''),
(6222, 27, 265, 3749, '
Sampaga
', '
Kecamatan
', ''),
(6223, 27, 265, 3750, '
Simboro
dan
Kepulauan
', '
Kecamatan
', ''),
(6224, 27, 265, 3751, '
Tapalang
', '
Kecamatan
', ''),
(6225, 27, 265, 3752, '
Tapalang
Barat
', '
Kecamatan
', ''),
(6226, 27, 265, 3753, '
Tobadak
', '
Kecamatan
', ''),
(6227, 27, 265, 3754, '
Tommo
', '
Kecamatan
', ''),
(6228, 27, 265, 3755, '
Topoyo
', '
Kecamatan
', ''),
(6229, 27, 266, 3756, '
Bambaira
', '
Kecamatan
', ''),
(6230, 27, 266, 3757, '
Bambalamotu
', '
Kecamatan
', ''),
(6231, 27, 266, 3758, '
Baras
', '
Kecamatan
', ''),
(6232, 27, 266, 3759, '
Bulu
Taba
', '
Kecamatan
', ''),
(6233, 27, 266, 3760, '
Dapurang
', '
Kecamatan
', ''),
(6234, 27, 266, 3761, '
Duripoku
', '
Kecamatan
', ''),
(6235, 27, 266, 3762, '
Lariang
', '
Kecamatan
', ''),
(6236, 27, 266, 3763, '
Pasangkayu
', '
Kecamatan
', ''),
(6237, 27, 266, 3764, '
Pedongga
', '
Kecamatan
', ''),
(6238, 27, 266, 3765, '
Sarjo
', '
Kecamatan
', ''),
(6239, 27, 266, 3766, '
Sarudu
', '
Kecamatan
', ''),
(6240, 27, 266, 3767, '
Tikke
Raya
', '
Kecamatan
', ''),
(6241, 27, 362, 5055, '
Alu
(
Allu
)
', '
Kecamatan
', ''),
(6242, 27, 362, 5056, '
Anreapi
', '
Kecamatan
', ''),
(6243, 27, 362, 5057, '
Balanipa
', '
Kecamatan
', ''),
(6244, 27, 362, 5058, '
Binuang
', '
Kecamatan
', ''),
(6245, 27, 362, 5059, '
Bulo
', '
Kecamatan
', ''),
(6246, 27, 362, 5060, '
Campalagian
', '
Kecamatan
', ''),
(6247, 27, 362, 5061, '
Limboro
', '
Kecamatan
', ''),
(6248, 27, 362, 5062, '
Luyo
', '
Kecamatan
', ''),
(6249, 27, 362, 5063, '
Mapilli
', '
Kecamatan
', ''),
(6250, 27, 362, 5064, '
Matakali
', '
Kecamatan
', ''),
(6251, 27, 362, 5065, '
Matangnga
', '
Kecamatan
', ''),
(6252, 27, 362, 5066, '
Polewali
', '
Kecamatan
', ''),
(6253, 27, 362, 5067, '
Tapango
', '
Kecamatan
', ''),
(6254, 27, 362, 5068, '
Tinambung
', '
Kecamatan
', ''),
(6255, 27, 362, 5069, '
Tubbi
Taramanu
(
Tutar
/
Tutallu
)
', '
Kecamatan
', ''),
(6256, 27, 362, 5070, '
Wonomulyo
', '
Kecamatan
', ''),
(6257, 28, 38, 0, '
Bantaeng
', '
Kabupaten
', '
92411
'),
(6258, 28, 47, 0, '
Barru
', '
Kabupaten
', '
90719
'),
(6259, 28, 87, 0, '
Bone
', '
Kabupaten
', '
92713
'),
(6260, 28, 95, 0, '
Bulukumba
', '
Kabupaten
', '
92511
'),
(6261, 28, 123, 0, '
Enrekang
', '
Kabupaten
', '
91719
'),
(6262, 28, 132, 0, '
Gowa
', '
Kabupaten
', '
92111
'),
(6263, 28, 162, 0, '
Jeneponto
', '
Kabupaten
', '
92319
'),
(6264, 28, 244, 0, '
Luwu
', '
Kabupaten
', '
91994
'),
(6265, 28, 245, 0, '
Luwu
Timur
', '
Kabupaten
', '
92981
'),
(6266, 28, 246, 0, '
Luwu
Utara
', '
Kabupaten
', '
92911
'),
(6267, 28, 254, 0, '
Makassar
', '
Kota
', '
90111
'),
(6268, 28, 275, 0, '
Maros
', '
Kabupaten
', '
90511
'),
(6269, 28, 328, 0, '
Palopo
', '
Kota
', '
91911
'),
(6270, 28, 333, 0, '
Pangkajene
Kepulauan
', '
Kabupaten
', '
90611
'),
(6271, 28, 336, 0, '
Parepare
', '
Kota
', '
91123
'),
(6272, 28, 360, 0, '
Pinrang
', '
Kabupaten
', '
91251
'),
(6273, 28, 396, 0, '
Selayar
(
Kepulauan
Selayar
)
', '
Kabupaten
', '
92812
'),
(6274, 28, 408, 0, '
Sidenreng
Rappang
/
Rapang
', '
Kabupaten
', '
91613
'),
(6275, 28, 416, 0, '
Sinjai
', '
Kabupaten
', '
92615
'),
(6276, 28, 423, 0, '
Soppeng
', '
Kabupaten
', '
90812
'),
(6277, 28, 448, 0, '
Takalar
', '
Kabupaten
', '
92212
'),
(6278, 28, 451, 0, '
Tana
Toraja
', '
Kabupaten
', '
91819
'),
(6279, 28, 486, 0, '
Toraja
Utara
', '
Kabupaten
', '
91831
'),
(6280, 28, 493, 0, '
Wajo
', '
Kabupaten
', '
90911
'),
(6281, 28, 38, 529, '
Bantaeng
', '
Kecamatan
', ''),
(6282, 28, 38, 530, '
Bissappu
', '
Kecamatan
', ''),
(6283, 28, 38, 531, '
Eremerasa
', '
Kecamatan
', ''),
(6284, 28, 38, 532, '
Gantarang
Keke
(
Gantareng
Keke
)
', '
Kecamatan
', ''),
(6285, 28, 38, 533, '
Pajukukang
', '
Kecamatan
', ''),
(6286, 28, 38, 534, '
Sinoa
', '
Kecamatan
', ''),
(6287, 28, 38, 535, '
Tompobulu
', '
Kecamatan
', ''),
(6288, 28, 38, 536, '
Uluere
', '
Kecamatan
', ''),
(6289, 28, 47, 666, '
Balusu
', '
Kecamatan
', ''),
(6290, 28, 47, 667, '
Barru
', '
Kecamatan
', ''),
(6291, 28, 47, 668, '
Mallusetasi
', '
Kecamatan
', ''),
(6292, 28, 47, 669, '
Pujananting
', '
Kecamatan
', ''),
(6293, 28, 47, 670, '
Soppeng
Riaja
', '
Kecamatan
', ''),
(6294, 28, 47, 671, '
Tanete
Riaja
', '
Kecamatan
', ''),
(6295, 28, 47, 672, '
Tanete
Rilau
', '
Kecamatan
', ''),
(6296, 28, 87, 1172, '
Ajangale
', '
Kecamatan
', ''),
(6297, 28, 87, 1173, '
Amali
', '
Kecamatan
', ''),
(6298, 28, 87, 1174, '
Awangpone
', '
Kecamatan
', ''),
(6299, 28, 87, 1175, '
Barebbo
', '
Kecamatan
', ''),
(6300, 28, 87, 1176, '
Bengo
', '
Kecamatan
', ''),
(6301, 28, 87, 1177, '
Bontocani
', '
Kecamatan
', ''),
(6302, 28, 87, 1178, '
Cenrana
', '
Kecamatan
', ''),
(6303, 28, 87, 1179, '
Cina
', '
Kecamatan
', ''),
(6304, 28, 87, 1180, '
Dua
Boccoe
', '
Kecamatan
', ''),
(6305, 28, 87, 1181, '
Kahu
', '
Kecamatan
', ''),
(6306, 28, 87, 1182, '
Kajuara
', '
Kecamatan
', ''),
(6307, 28, 87, 1183, '
Lamuru
', '
Kecamatan
', ''),
(6308, 28, 87, 1184, '
Lappariaja
', '
Kecamatan
', ''),
(6309, 28, 87, 1185, '
Libureng
', '
Kecamatan
', ''),
(6310, 28, 87, 1186, '
Mare
', '
Kecamatan
', ''),
(6311, 28, 87, 1187, '
Palakka
', '
Kecamatan
', ''),
(6312, 28, 87, 1188, '
Patimpeng
', '
Kecamatan
', ''),
(6313, 28, 87, 1189, '
Ponre
', '
Kecamatan
', ''),
(6314, 28, 87, 1190, '
Salomekko
', '
Kecamatan
', ''),
(6315, 28, 87, 1191, '
Sibulue
', '
Kecamatan
', ''),
(6316, 28, 87, 1192, '
Tanete
Riattang
', '
Kecamatan
', ''),
(6317, 28, 87, 1193, '
Tanete
Riattang
Barat
', '
Kecamatan
', ''),
(6318, 28, 87, 1194, '
Tanete
Riattang
Timur
', '
Kecamatan
', ''),
(6319, 28, 87, 1195, '
Tellu
Limpoe
', '
Kecamatan
', ''),
(6320, 28, 87, 1196, '
Tellu
Siattinge
', '
Kecamatan
', ''),
(6321, 28, 87, 1197, '
Tonra
', '
Kecamatan
', ''),
(6322, 28, 87, 1198, '
Ulaweng
', '
Kecamatan
', ''),
(6323, 28, 95, 1288, '
Bonto
Bahari
', '
Kecamatan
', ''),
(6324, 28, 95, 1289, '
Bontotiro
', '
Kecamatan
', ''),
(6325, 28, 95, 1290, '
Bulukumba
(
Bulukumpa
)
', '
Kecamatan
', ''),
(6326, 28, 95, 1291, '
Gantorang
/
Gantarang
(
Gangking
)
', '
Kecamatan
', ''),
(6327, 28, 95, 1292, '
Hero
Lange
-
Lange
(
Herlang
)
', '
Kecamatan
', ''),
(6328, 28, 95, 1293, '
Kajang
', '
Kecamatan
', ''),
(6329, 28, 95, 1294, '
Kindang
', '
Kecamatan
', ''),
(6330, 28, 95, 1295, '
Rilau
Ale
', '
Kecamatan
', ''),
(6331, 28, 95, 1296, '
Ujung
Bulu
', '
Kecamatan
', ''),
(6332, 28, 95, 1297, '
Ujung
Loe
', '
Kecamatan
', ''),
(6333, 28, 123, 1671, '
Alla
', '
Kecamatan
', ''),
(6334, 28, 123, 1672, '
Anggeraja
', '
Kecamatan
', ''),
(6335, 28, 123, 1673, '
Baraka
', '
Kecamatan
', ''),
(6336, 28, 123, 1674, '
Baroko
', '
Kecamatan
', ''),
(6337, 28, 123, 1675, '
Bungin
', '
Kecamatan
', ''),
(6338, 28, 123, 1676, '
Buntu
Batu
', '
Kecamatan
', ''),
(6339, 28, 123, 1677, '
Cendana
', '
Kecamatan
', ''),
(6340, 28, 123, 1678, '
Curio
', '
Kecamatan
', ''),
(6341, 28, 123, 1679, '
Enrekang
', '
Kecamatan
', ''),
(6342, 28, 123, 1680, '
Maiwa
', '
Kecamatan
', ''),
(6343, 28, 123, 1681, '
Malua
', '
Kecamatan
', ''),
(6344, 28, 123, 1682, '
Masalle
', '
Kecamatan
', ''),
(6345, 28, 162, 2237, '
Arungkeke
', '
Kecamatan
', ''),
(6346, 28, 162, 2238, '
Bangkala
', '
Kecamatan
', ''),
(6347, 28, 162, 2239, '
Bangkala
Barat
', '
Kecamatan
', ''),
(6348, 28, 162, 2240, '
Batang
', '
Kecamatan
', ''),
(6349, 28, 162, 2241, '
Binamu
', '
Kecamatan
', ''),
(6350, 28, 162, 2242, '
Bontoramba
', '
Kecamatan
', ''),
(6351, 28, 162, 2243, '
Kelara
', '
Kecamatan
', ''),
(6352, 28, 162, 2244, '
Rumbia
', '
Kecamatan
', ''),
(6353, 28, 162, 2245, '
Tamalatea
', '
Kecamatan
', ''),
(6354, 28, 162, 2246, '
Tarowang
', '
Kecamatan
', ''),
(6355, 28, 162, 2247, '
Turatea
', '
Kecamatan
', ''),
(6356, 28, 132, 1810, '
Bajeng
', '
Kecamatan
', ''),
(6357, 28, 132, 1811, '
Bajeng
Barat
', '
Kecamatan
', ''),
(6358, 28, 132, 1812, '
Barombong
', '
Kecamatan
', ''),
(6359, 28, 132, 1813, '
Biringbulu
', '
Kecamatan
', ''),
(6360, 28, 132, 1814, '
Bontolempangang
', '
Kecamatan
', ''),
(6361, 28, 132, 1815, '
Bontomarannu
', '
Kecamatan
', ''),
(6362, 28, 132, 1816, '
Bontonompo
', '
Kecamatan
', ''),
(6363, 28, 132, 1817, '
Bontonompo
Selatan
', '
Kecamatan
', ''),
(6364, 28, 132, 1818, '
Bungaya
', '
Kecamatan
', ''),
(6365, 28, 132, 1819, '
Manuju
', '
Kecamatan
', ''),
(6366, 28, 132, 1820, '
Pallangga
', '
Kecamatan
', ''),
(6367, 28, 132, 1821, '
Parangloe
', '
Kecamatan
', ''),
(6368, 28, 132, 1822, '
Parigi
', '
Kecamatan
', ''),
(6369, 28, 132, 1823, '
Pattallassang
', '
Kecamatan
', ''),
(6370, 28, 132, 1824, '
Somba
Opu
(
Upu
)
', '
Kecamatan
', ''),
(6371, 28, 132, 1825, '
Tinggimoncong
', '
Kecamatan
', ''),
(6372, 28, 132, 1826, '
Tombolo
Pao
', '
Kecamatan
', ''),
(6373, 28, 132, 1827, '
Tompobulu
', '
Kecamatan
', ''),
(6374, 28, 244, 3448, '
Bajo
', '
Kecamatan
', ''),
(6375, 28, 244, 3449, '
Bajo
Barat
', '
Kecamatan
', ''),
(6376, 28, 244, 3450, '
Basse
Sangtempe
Utara
', '
Kecamatan
', ''),
(6377, 28, 244, 3451, '
Bassesang
Tempe
(
Bastem
)
', '
Kecamatan
', ''),
(6378, 28, 244, 3452, '
Belopa
', '
Kecamatan
', ''),
(6379, 28, 244, 3453, '
Belopa
Utara
', '
Kecamatan
', ''),
(6380, 28, 244, 3454, '
Bua
', '
Kecamatan
', ''),
(6381, 28, 244, 3455, '
Bua
Ponrang
(
Bupon
)
', '
Kecamatan
', ''),
(6382, 28, 244, 3456, '
Kamanre
', '
Kecamatan
', ''),
(6383, 28, 244, 3457, '
Lamasi
', '
Kecamatan
', ''),
(6384, 28, 244, 3458, '
Lamasi
Timur
', '
Kecamatan
', ''),
(6385, 28, 244, 3459, '
Larompong
', '
Kecamatan
', ''),
(6386, 28, 244, 3460, '
Larompong
Selatan
', '
Kecamatan
', ''),
(6387, 28, 244, 3461, '
Latimojong
', '
Kecamatan
', ''),
(6388, 28, 244, 3462, '
Ponrang
', '
Kecamatan
', ''),
(6389, 28, 244, 3463, '
Ponrang
Selatan
', '
Kecamatan
', ''),
(6390, 28, 244, 3464, '
Suli
', '
Kecamatan
', ''),
(6391, 28, 244, 3465, '
Suli
Barat
', '
Kecamatan
', ''),
(6392, 28, 244, 3466, '
Walenrang
', '
Kecamatan
', ''),
(6393, 28, 244, 3467, '
Walenrang
Barat
', '
Kecamatan
', ''),
(6394, 28, 244, 3468, '
Walenrang
Timur
', '
Kecamatan
', ''),
(6395, 28, 244, 3469, '
Walenrang
Utara
', '
Kecamatan
', ''),
(6396, 28, 245, 3470, '
Angkona
', '
Kecamatan
', ''),
(6397, 28, 245, 3471, '
Burau
', '
Kecamatan
', ''),
(6398, 28, 245, 3472, '
Kalaena
', '
Kecamatan
', ''),
(6399, 28, 245, 3473, '
Malili
', '
Kecamatan
', ''),
(6400, 28, 245, 3474, '
Mangkutana
', '
Kecamatan
', ''),
(6401, 28, 245, 3475, '
Nuha
', '
Kecamatan
', ''),
(6402, 28, 245, 3476, '
Tomoni
', '
Kecamatan
', ''),
(6403, 28, 245, 3477, '
Tomoni
Timur
', '
Kecamatan
', ''),
(6404, 28, 245, 3478, '
Towuti
', '
Kecamatan
', ''),
(6405, 28, 245, 3479, '
Wasuponda
', '
Kecamatan
', ''),
(6406, 28, 245, 3480, '
Wotu
', '
Kecamatan
', ''),
(6407, 28, 246, 3481, '
Baebunta
', '
Kecamatan
', ''),
(6408, 28, 246, 3482, '
Bone
-
Bone
', '
Kecamatan
', ''),
(6409, 28, 246, 3483, '
Limbong
', '
Kecamatan
', ''),
(6410, 28, 246, 3484, '
Malangke
', '
Kecamatan
', ''),
(6411, 28, 246, 3485, '
Malangke
Barat
', '
Kecamatan
', ''),
(6412, 28, 246, 3486, '
Mappedeceng
', '
Kecamatan
', ''),
(6413, 28, 246, 3487, '
Masamba
', '
Kecamatan
', ''),
(6414, 28, 246, 3488, '
Rampi
', '
Kecamatan
', ''),
(6415, 28, 246, 3489, '
Sabbang
', '
Kecamatan
', ''),
(6416, 28, 246, 3490, '
Seko
', '
Kecamatan
', ''),
(6417, 28, 246, 3491, '
Sukamaju
', '
Kecamatan
', ''),
(6418, 28, 246, 3492, '
Tana
Lili
', '
Kecamatan
', ''),
(6419, 28, 254, 3587, '
Biring
Kanaya
', '
Kecamatan
', ''),
(6420, 28, 254, 3588, '
Bontoala
', '
Kecamatan
', ''),
(6421, 28, 254, 3589, '
Makassar
', '
Kecamatan
', ''),
(6422, 28, 254, 3590, '
Mamajang
', '
Kecamatan
', ''),
(6423, 28, 254, 3591, '
Manggala
', '
Kecamatan
', ''),
(6424, 28, 254, 3592, '
Mariso
', '
Kecamatan
', ''),
(6425, 28, 254, 3593, '
Panakkukang
', '
Kecamatan
', ''),
(6426, 28, 254, 3594, '
Rappocini
', '
Kecamatan
', ''),
(6427, 28, 254, 3595, '
Tallo
', '
Kecamatan
', ''),
(6428, 28, 254, 3596, '
Tamalanrea
', '
Kecamatan
', ''),
(6429, 28, 254, 3597, '
Tamalate
', '
Kecamatan
', ''),
(6430, 28, 254, 3598, '
Ujung
Pandang
', '
Kecamatan
', ''),
(6431, 28, 254, 3599, '
Ujung
Tanah
', '
Kecamatan
', ''),
(6432, 28, 254, 3600, '
Wajo
', '
Kecamatan
', ''),
(6433, 28, 275, 3862, '
Bantimurung
', '
Kecamatan
', ''),
(6434, 28, 275, 3863, '
Bontoa
(
Maros
Utara
)
', '
Kecamatan
', ''),
(6435, 28, 275, 3864, '
Camba
', '
Kecamatan
', ''),
(6436, 28, 275, 3865, '
Cenrana
', '
Kecamatan
', ''),
(6437, 28, 275, 3866, '
Lau
', '
Kecamatan
', ''),
(6438, 28, 275, 3867, '
Mallawa
', '
Kecamatan
', ''),
(6439, 28, 275, 3868, '
Mandai
', '
Kecamatan
', ''),
(6440, 28, 275, 3869, '
Maros
Baru
', '
Kecamatan
', ''),
(6441, 28, 275, 3870, '
Marusu
', '
Kecamatan
', ''),
(6442, 28, 275, 3871, '
Moncongloe
', '
Kecamatan
', ''),
(6443, 28, 275, 3872, '
Simbang
', '
Kecamatan
', ''),
(6444, 28, 275, 3873, '
Tanralili
', '
Kecamatan
', ''),
(6445, 28, 275, 3874, '
Tompu
Bulu
', '
Kecamatan
', ''),
(6446, 28, 275, 3875, '
Turikale
', '
Kecamatan
', ''),
(6447, 28, 328, 4625, '
Bara
', '
Kecamatan
', ''),
(6448, 28, 328, 4626, '
Mungkajang
', '
Kecamatan
', ''),
(6449, 28, 328, 4627, '
Sendana
', '
Kecamatan
', ''),
(6450, 28, 328, 4628, '
Telluwanua
', '
Kecamatan
', ''),
(6451, 28, 328, 4629, '
Wara
', '
Kecamatan
', ''),
(6452, 28, 328, 4630, '
Wara
Barat
', '
Kecamatan
', ''),
(6453, 28, 328, 4631, '
Wara
Selatan
', '
Kecamatan
', ''),
(6454, 28, 328, 4632, '
Wara
Timur
', '
Kecamatan
', ''),
(6455, 28, 328, 4633, '
Wara
Utara
', '
Kecamatan
', ''),
(6456, 28, 333, 4700, '
Balocci
', '
Kecamatan
', ''),
(6457, 28, 333, 4701, '
Bungoro
', '
Kecamatan
', ''),
(6458, 28, 333, 4702, '
Labakkang
', '
Kecamatan
', ''),
(6459, 28, 333, 4703, '
Liukang
Kalmas
(
Kalukuang
Masalima
)
', '
Kecamatan
', ''),
(6460, 28, 333, 4704, '
Liukang
Tangaya
', '
Kecamatan
', ''),
(6461, 28, 333, 4705, '
Liukang
Tupabbiring
', '
Kecamatan
', ''),
(6462, 28, 333, 4706, '
Liukang
Tupabbiring
Utara
', '
Kecamatan
', ''),
(6463, 28, 333, 4707, '
Mandalle
', '
Kecamatan
', ''),
(6464, 28, 333, 4708, '
Marang
(
Ma
Rang
)
', '
Kecamatan
', ''),
(6465, 28, 333, 4709, '
Minasa
Tene
', '
Kecamatan
', ''),
(6466, 28, 333, 4710, '
Pangkajene
', '
Kecamatan
', ''),
(6467, 28, 333, 4711, '
Segeri
', '
Kecamatan
', ''),
(6468, 28, 333, 4712, '
Tondong
Tallasa
', '
Kecamatan
', ''),
(6469, 28, 336, 4730, '
Bacukiki
', '
Kecamatan
', ''),
(6470, 28, 336, 4731, '
Bacukiki
Barat
', '
Kecamatan
', ''),
(6471, 28, 336, 4732, '
Soreang
', '
Kecamatan
', ''),
(6472, 28, 336, 4733, '
Ujung
', '
Kecamatan
', ''),
(6473, 28, 360, 5030, '
Batulappa
', '
Kecamatan
', ''),
(6474, 28, 360, 5031, '
Cempa
', '
Kecamatan
', ''),
(6475, 28, 360, 5032, '
Duampanua
', '
Kecamatan
', ''),
(6476, 28, 360, 5033, '
Lanrisang
', '
Kecamatan
', ''),
(6477, 28, 360, 5034, '
Lembang
', '
Kecamatan
', ''),
(6478, 28, 360, 5035, '
Mattiro
Bulu
', '
Kecamatan
', ''),
(6479, 28, 360, 5036, '
Mattiro
Sompe
', '
Kecamatan
', ''),
(6480, 28, 360, 5037, '
Paleteang
', '
Kecamatan
', ''),
(6481, 28, 360, 5038, '
Patampanua
', '
Kecamatan
', ''),
(6482, 28, 360, 5039, '
Suppa
', '
Kecamatan
', ''),
(6483, 28, 360, 5040, '
Tiroang
', '
Kecamatan
', ''),
(6484, 28, 360, 5041, '
Watang
Sawitto
', '
Kecamatan
', ''),
(6485, 28, 396, 5454, '
Benteng
', '
Kecamatan
', ''),
(6486, 28, 396, 5455, '
Bontoharu
', '
Kecamatan
', ''),
(6487, 28, 396, 5456, '
Bontomanai
', '
Kecamatan
', ''),
(6488, 28, 396, 5457, '
Bontomatene
', '
Kecamatan
', ''),
(6489, 28, 396, 5458, '
Bontosikuyu
', '
Kecamatan
', ''),
(6490, 28, 396, 5459, '
Buki
', '
Kecamatan
', ''),
(6491, 28, 396, 5460, '
Pasilambena
', '
Kecamatan
', ''),
(6492, 28, 396, 5461, '
Pasimarannu
', '
Kecamatan
', ''),
(6493, 28, 396, 5462, '
Pasimassunggu
', '
Kecamatan
', ''),
(6494, 28, 396, 5463, '
Pasimasunggu
Timur
', '
Kecamatan
', ''),
(6495, 28, 396, 5464, '
Takabonerate
', '
Kecamatan
', ''),
(6496, 28, 408, 5620, '
Baranti
', '
Kecamatan
', ''),
(6497, 28, 408, 5621, '
Dua
Pitue
', '
Kecamatan
', ''),
(6498, 28, 408, 5622, '
Kulo
', '
Kecamatan
', ''),
(6499, 28, 408, 5623, '
Maritengngae
', '
Kecamatan
', ''),
(6500, 28, 408, 5624, '
Panca
Lautan
(
Lautang
)
', '
Kecamatan
', ''),
(6501, 28, 408, 5625, '
Panca
Rijang
', '
Kecamatan
', ''),
(6502, 28, 408, 5626, '
Pitu
Raise
/
Riase
', '
Kecamatan
', ''),
(6503, 28, 408, 5627, '
Pitu
Riawa
', '
Kecamatan
', ''),
(6504, 28, 408, 5628, '
Tellu
Limpoe
', '
Kecamatan
', ''),
(6505, 28, 408, 5629, '
Watang
Pulu
', '
Kecamatan
', ''),
(6506, 28, 408, 5630, '
Wattang
Sidenreng
(
Watang
Sidenreng
)
', '
Kecamatan
', ''),
(6507, 28, 416, 5739, '
Bulupoddo
', '
Kecamatan
', ''),
(6508, 28, 416, 5740, '
Pulau
Sembilan
', '
Kecamatan
', ''),
(6509, 28, 416, 5741, '
Sinjai
Barat
', '
Kecamatan
', ''),
(6510, 28, 416, 5742, '
Sinjai
Borong
', '
Kecamatan
', ''),
(6511, 28, 416, 5743, '
Sinjai
Selatan
', '
Kecamatan
', ''),
(6512, 28, 416, 5744, '
Sinjai
Tengah
', '
Kecamatan
', ''),
(6513, 28, 416, 5745, '
Sinjai
Timur
', '
Kecamatan
', ''),
(6514, 28, 416, 5746, '
Sinjai
Utara
', '
Kecamatan
', ''),
(6515, 28, 416, 5747, '
Tellu
Limpoe
', '
Kecamatan
', ''),
(6516, 28, 423, 5819, '
Citta
', '
Kecamatan
', ''),
(6517, 28, 423, 5820, '
Donri
-
Donri
', '
Kecamatan
', ''),
(6518, 28, 423, 5821, '
Ganra
', '
Kecamatan
', ''),
(6519, 28, 423, 5822, '
Lalabata
', '
Kecamatan
', ''),
(6520, 28, 423, 5823, '
Lili
Rilau
', '
Kecamatan
', ''),
(6521, 28, 423, 5824, '
Liliraja
(
Lili
Riaja
)
', '
Kecamatan
', ''),
(6522, 28, 423, 5825, '
Mario
Riawa
', '
Kecamatan
', ''),
(6523, 28, 423, 5826, '
Mario
Riwawo
', '
Kecamatan
', ''),
(6524, 28, 448, 6189, '
Galesong
', '
Kecamatan
', ''),
(6525, 28, 448, 6190, '
Galesong
Selatan
', '
Kecamatan
', ''),
(6526, 28, 448, 6191, '
Galesong
Utara
', '
Kecamatan
', ''),
(6527, 28, 448, 6192, '
Mangara
Bombang
', '
Kecamatan
', ''),
(6528, 28, 448, 6193, '
Mappakasunggu
', '
Kecamatan
', ''),
(6529, 28, 448, 6194, '
Patallassang
', '
Kecamatan
', ''),
(6530, 28, 448, 6195, '
Polombangkeng
Selatan
(
Polobangkeng
)
', '
Kecamatan
', ''),
(6531, 28, 448, 6196, '
Polombangkeng
Utara
(
Polobangkeng
)
', '
Kecamatan
', ''),
(6532, 28, 448, 6197, '
Sanrobone
', '
Kecamatan
', ''),
(6533, 28, 451, 6214, '
Bittuang
', '
Kecamatan
', ''),
(6534, 28, 451, 6215, '
Bonggakaradeng
', '
Kecamatan
', ''),
(6535, 28, 451, 6216, '
Gandang
Batu
Sillanan
', '
Kecamatan
', ''),
(6536, 28, 451, 6217, '
Kurra
', '
Kecamatan
', ''),
(6537, 28, 451, 6218, '
Makale
', '
Kecamatan
', ''),
(6538, 28, 451, 6219, '
Makale
Selatan
', '
Kecamatan
', ''),
(6539, 28, 451, 6220, '
Makale
Utara
', '
Kecamatan
', ''),
(6540, 28, 451, 6221, '
Malimbong
Balepe
', '
Kecamatan
', ''),
(6541, 28, 451, 6222, '
Mappak
', '
Kecamatan
', ''),
(6542, 28, 451, 6223, '
Masanda
', '
Kecamatan
', ''),
(6543, 28, 451, 6224, '
Mengkendek
', '
Kecamatan
', ''),
(6544, 28, 451, 6225, '
Rano
', '
Kecamatan
', ''),
(6545, 28, 451, 6226, '
Rantetayo
', '
Kecamatan
', ''),
(6546, 28, 451, 6227, '
Rembon
', '
Kecamatan
', ''),
(6547, 28, 451, 6228, '
Saluputti
', '
Kecamatan
', ''),
(6548, 28, 451, 6229, '
Sangalla
(
Sanggala
)
', '
Kecamatan
', ''),
(6549, 28, 451, 6230, '
Sangalla
Selatan
', '
Kecamatan
', ''),
(6550, 28, 451, 6231, '
Sangalla
Utara
', '
Kecamatan
', ''),
(6551, 28, 451, 6232, '
Simbuang
', '
Kecamatan
', ''),
(6552, 28, 486, 6738, '
Awan
Rante
Karua
', '
Kecamatan
', ''),
(6553, 28, 486, 6739, '
Balusu
', '
Kecamatan
', ''),
(6554, 28, 486, 6740, '
Bangkelekila
', '
Kecamatan
', ''),
(6555, 28, 486, 6741, '
Baruppu
', '
Kecamatan
', ''),
(6556, 28, 486, 6742, '
Buntao
', '
Kecamatan
', ''),
(6557, 28, 486, 6743, '
Buntu
Pepasan
', '
Kecamatan
', ''),
(6558, 28, 486, 6744, '
Dende
\
' Piongan Napo'
,
'Kecamatan'
,
''
)
,
(
6559
,
28
,
486
,
6745
,
'Kapalla Pitu (Kapala Pitu)'
,
'Kecamatan'
,
''
)
,
(
6560
,
28
,
486
,
6746
,
'Kesu'
,
'Kecamatan'
,
''
)
,
(
6561
,
28
,
486
,
6747
,
'Nanggala'
,
'Kecamatan'
,
''
)
,
(
6562
,
28
,
486
,
6748
,
'Rantebua'
,
'Kecamatan'
,
''
)
,
(
6563
,
28
,
486
,
6749
,
'Rantepao'
,
'Kecamatan'
,
''
)
,
(
6564
,
28
,
486
,
6750
,
'Rindingallo'
,
'Kecamatan'
,
''
)
,
(
6565
,
28
,
486
,
6751
,
'Sa\'
dan
', '
Kecamatan
', ''),
(6566, 28, 486, 6752, '
Sanggalangi
', '
Kecamatan
', ''),
(6567, 28, 486, 6753, '
Sesean
', '
Kecamatan
', ''),
(6568, 28, 486, 6754, '
Sesean
Suloara
', '
Kecamatan
', ''),
(6569, 28, 486, 6755, '
Sopai
', '
Kecamatan
', ''),
(6570, 28, 486, 6756, '
Tallunglipu
', '
Kecamatan
', ''),
(6571, 28, 486, 6757, '
Tikala
', '
Kecamatan
', ''),
(6572, 28, 486, 6758, '
Tondon
', '
Kecamatan
', ''),
(6573, 28, 493, 6840, '
Belawa
', '
Kecamatan
', ''),
(6574, 28, 493, 6841, '
Bola
', '
Kecamatan
', ''),
(6575, 28, 493, 6842, '
Gilireng
', '
Kecamatan
', ''),
(6576, 28, 493, 6843, '
Keera
', '
Kecamatan
', ''),
(6577, 28, 493, 6844, '
Majauleng
', '
Kecamatan
', ''),
(6578, 28, 493, 6845, '
Maniang
Pajo
', '
Kecamatan
', ''),
(6579, 28, 493, 6846, '
Pammana
', '
Kecamatan
', ''),
(6580, 28, 493, 6847, '
Penrang
', '
Kecamatan
', ''),
(6581, 28, 493, 6848, '
Pitumpanua
', '
Kecamatan
', ''),
(6582, 28, 493, 6849, '
Sabbang
Paru
', '
Kecamatan
', ''),
(6583, 28, 493, 6850, '
Sajoanging
', '
Kecamatan
', ''),
(6584, 28, 493, 6851, '
Takkalalla
', '
Kecamatan
', ''),
(6585, 28, 493, 6852, '
Tana
Sitolo
', '
Kecamatan
', ''),
(6586, 28, 493, 6853, '
Tempe
', '
Kecamatan
', ''),
(6587, 29, 25, 0, '
Banggai
', '
Kabupaten
', '
94711
'),
(6588, 29, 26, 0, '
Banggai
Kepulauan
', '
Kabupaten
', '
94881
'),
(6589, 29, 98, 0, '
Buol
', '
Kabupaten
', '
94564
'),
(6590, 29, 119, 0, '
Donggala
', '
Kabupaten
', '
94341
'),
(6591, 29, 291, 0, '
Morowali
', '
Kabupaten
', '
94911
'),
(6592, 29, 329, 0, '
Palu
', '
Kota
', '
94111
'),
(6593, 29, 338, 0, '
Parigi
Moutong
', '
Kabupaten
', '
94411
'),
(6594, 29, 366, 0, '
Poso
', '
Kabupaten
', '
94615
'),
(6595, 29, 410, 0, '
Sigi
', '
Kabupaten
', '
94364
'),
(6596, 29, 482, 0, '
Tojo
Una
-
Una
', '
Kabupaten
', '
94683
'),
(6597, 29, 483, 0, '
Toli
-
Toli
', '
Kabupaten
', '
94542
'),
(6598, 29, 25, 384, '
Balantak
', '
Kecamatan
', ''),
(6599, 29, 25, 385, '
Balantak
Selatan
', '
Kecamatan
', ''),
(6600, 29, 25, 386, '
Balantak
Utara
', '
Kecamatan
', ''),
(6601, 29, 25, 387, '
Batui
', '
Kecamatan
', ''),
(6602, 29, 25, 388, '
Batui
Selatan
', '
Kecamatan
', ''),
(6603, 29, 25, 389, '
Bualemo
(
Boalemo
)
', '
Kecamatan
', ''),
(6604, 29, 25, 390, '
Bunta
', '
Kecamatan
', ''),
(6605, 29, 25, 391, '
Kintom
', '
Kecamatan
', ''),
(6606, 29, 25, 392, '
Lamala
', '
Kecamatan
', ''),
(6607, 29, 25, 393, '
Lobu
', '
Kecamatan
', ''),
(6608, 29, 25, 394, '
Luwuk
', '
Kecamatan
', ''),
(6609, 29, 25, 395, '
Luwuk
Selatan
', '
Kecamatan
', ''),
(6610, 29, 25, 396, '
Luwuk
Timur
', '
Kecamatan
', ''),
(6611, 29, 25, 397, '
Luwuk
Utara
', '
Kecamatan
', ''),
(6612, 29, 25, 398, '
Mantoh
', '
Kecamatan
', ''),
(6613, 29, 25, 399, '
Masama
', '
Kecamatan
', ''),
(6614, 29, 25, 400, '
Moilong
', '
Kecamatan
', ''),
(6615, 29, 25, 401, '
Nambo
', '
Kecamatan
', ''),
(6616, 29, 25, 402, '
Nuhon
', '
Kecamatan
', ''),
(6617, 29, 25, 403, '
Pagimana
', '
Kecamatan
', ''),
(6618, 29, 25, 404, '
Simpang
Raya
', '
Kecamatan
', ''),
(6619, 29, 25, 405, '
Toili
', '
Kecamatan
', ''),
(6620, 29, 25, 406, '
Toili
Barat
', '
Kecamatan
', ''),
(6621, 29, 26, 407, '
Banggai
', '
Kecamatan
', ''),
(6622, 29, 26, 408, '
Banggai
Selatan
', '
Kecamatan
', ''),
(6623, 29, 26, 409, '
Banggai
Tengah
', '
Kecamatan
', ''),
(6624, 29, 26, 410, '
Banggai
Utara
', '
Kecamatan
', ''),
(6625, 29, 26, 411, '
Bangkurung
', '
Kecamatan
', ''),
(6626, 29, 26, 412, '
Bokan
Kepulauan
', '
Kecamatan
', ''),
(6627, 29, 26, 413, '
Buko
', '
Kecamatan
', ''),
(6628, 29, 26, 414, '
Buko
Selatan
', '
Kecamatan
', ''),
(6629, 29, 26, 415, '
Bulagi
', '
Kecamatan
', ''),
(6630, 29, 26, 416, '
Bulagi
Selatan
', '
Kecamatan
', ''),
(6631, 29, 26, 417, '
Bulagi
Utara
', '
Kecamatan
', ''),
(6632, 29, 26, 418, '
Labobo
(
Lobangkurung
)
', '
Kecamatan
', ''),
(6633, 29, 26, 419, '
Liang
', '
Kecamatan
', ''),
(6634, 29, 26, 420, '
Peling
Tengah
', '
Kecamatan
', ''),
(6635, 29, 26, 421, '
Tinangkung
', '
Kecamatan
', ''),
(6636, 29, 26, 422, '
Tinangkung
Selatan
', '
Kecamatan
', ''),
(6637, 29, 26, 423, '
Tinangkung
Utara
', '
Kecamatan
', ''),
(6638, 29, 26, 424, '
Totikum
(
Totikung
)
', '
Kecamatan
', ''),
(6639, 29, 26, 425, '
Totikum
Selatan
', '
Kecamatan
', ''),
(6640, 29, 98, 1325, '
Biau
', '
Kecamatan
', ''),
(6641, 29, 98, 1326, '
Bokat
', '
Kecamatan
', ''),
(6642, 29, 98, 1327, '
Bukal
', '
Kecamatan
', ''),
(6643, 29, 98, 1328, '
Bunobogu
', '
Kecamatan
', ''),
(6644, 29, 98, 1329, '
Gadung
', '
Kecamatan
', ''),
(6645, 29, 98, 1330, '
Karamat
', '
Kecamatan
', ''),
(6646, 29, 98, 1331, '
Lakea
(
Lipunoto
)
', '
Kecamatan
', ''),
(6647, 29, 98, 1332, '
Momunu
', '
Kecamatan
', ''),
(6648, 29, 98, 1333, '
Paleleh
', '
Kecamatan
', ''),
(6649, 29, 98, 1334, '
Paleleh
Barat
', '
Kecamatan
', ''),
(6650, 29, 98, 1335, '
Tiloan
', '
Kecamatan
', ''),
(6651, 29, 119, 1617, '
Balaesang
', '
Kecamatan
', ''),
(6652, 29, 119, 1618, '
Balaesang
Tanjung
', '
Kecamatan
', ''),
(6653, 29, 119, 1619, '
Banawa
', '
Kecamatan
', ''),
(6654, 29, 119, 1620, '
Banawa
Selatan
', '
Kecamatan
', ''),
(6655, 29, 119, 1621, '
Banawa
Tengah
', '
Kecamatan
', ''),
(6656, 29, 119, 1622, '
Damsol
(
Dampelas
Sojol
)
', '
Kecamatan
', ''),
(6657, 29, 119, 1623, '
Labuan
', '
Kecamatan
', ''),
(6658, 29, 119, 1624, '
Pinembani
', '
Kecamatan
', ''),
(6659, 29, 119, 1625, '
Rio
Pakava
(
Riopakawa
)
', '
Kecamatan
', ''),
(6660, 29, 119, 1626, '
Sindue
', '
Kecamatan
', ''),
(6661, 29, 119, 1627, '
Sindue
Tobata
', '
Kecamatan
', ''),
(6662, 29, 119, 1628, '
Sindue
Tombusabora
', '
Kecamatan
', ''),
(6663, 29, 119, 1629, '
Sirenja
', '
Kecamatan
', ''),
(6664, 29, 119, 1630, '
Sojol
', '
Kecamatan
', ''),
(6665, 29, 119, 1631, '
Sojol
Utara
', '
Kecamatan
', ''),
(6666, 29, 119, 1632, '
Tanantovea
', '
Kecamatan
', ''),
(6667, 29, 329, 4634, '
Mantikulore
', '
Kecamatan
', ''),
(6668, 29, 329, 4635, '
Palu
Barat
', '
Kecamatan
', ''),
(6669, 29, 329, 4636, '
Palu
Selatan
', '
Kecamatan
', ''),
(6670, 29, 329, 4637, '
Palu
Timur
', '
Kecamatan
', ''),
(6671, 29, 329, 4638, '
Palu
Utara
', '
Kecamatan
', ''),
(6672, 29, 329, 4639, '
Tatanga
', '
Kecamatan
', ''),
(6673, 29, 329, 4640, '
Tawaeli
', '
Kecamatan
', ''),
(6674, 29, 329, 4641, '
Ulujadi
', '
Kecamatan
', ''),
(6675, 29, 291, 4090, '
Bahodopi
', '
Kecamatan
', ''),
(6676, 29, 291, 4091, '
Bumi
Raya
', '
Kecamatan
', ''),
(6677, 29, 291, 4092, '
Bungku
Barat
', '
Kecamatan
', ''),
(6678, 29, 291, 4093, '
Bungku
Pesisir
', '
Kecamatan
', ''),
(6679, 29, 291, 4094, '
Bungku
Selatan
', '
Kecamatan
', ''),
(6680, 29, 291, 4095, '
Bungku
Tengah
', '
Kecamatan
', ''),
(6681, 29, 291, 4096, '
Bungku
Timur
', '
Kecamatan
', ''),
(6682, 29, 291, 4097, '
Bungku
Utara
', '
Kecamatan
', ''),
(6683, 29, 291, 4098, '
Lembo
', '
Kecamatan
', ''),
(6684, 29, 291, 4099, '
Lembo
Raya
', '
Kecamatan
', ''),
(6685, 29, 291, 4100, '
Mamosalato
', '
Kecamatan
', ''),
(6686, 29, 291, 4101, '
Menui
Kepulauan
', '
Kecamatan
', ''),
(6687, 29, 291, 4102, '
Mori
Atas
', '
Kecamatan
', ''),
(6688, 29, 291, 4103, '
Mori
Utara
', '
Kecamatan
', ''),
(6689, 29, 291, 4104, '
Petasia
', '
Kecamatan
', ''),
(6690, 29, 291, 4105, '
Petasia
Barat
', '
Kecamatan
', ''),
(6691, 29, 291, 4106, '
Petasia
Timur
', '
Kecamatan
', ''),
(6692, 29, 291, 4107, '
Soyo
Jaya
', '
Kecamatan
', ''),
(6693, 29, 291, 4108, '
Wita
Ponda
', '
Kecamatan
', ''),
(6694, 29, 338, 4738, '
Ampibabo
', '
Kecamatan
', ''),
(6695, 29, 338, 4739, '
Balinggi
', '
Kecamatan
', ''),
(6696, 29, 338, 4740, '
Bolano
', '
Kecamatan
', ''),
(6697, 29, 338, 4741, '
Bolano
Lambunu
/
Lambulu
', '
Kecamatan
', ''),
(6698, 29, 338, 4742, '
Kasimbar
', '
Kecamatan
', ''),
(6699, 29, 338, 4743, '
Mepanga
', '
Kecamatan
', ''),
(6700, 29, 338, 4744, '
Moutong
', '
Kecamatan
', ''),
(6701, 29, 338, 4745, '
Ongka
Malino
', '
Kecamatan
', ''),
(6702, 29, 338, 4746, '
Palasa
', '
Kecamatan
', ''),
(6703, 29, 338, 4747, '
Parigi
', '
Kecamatan
', ''),
(6704, 29, 338, 4748, '
Parigi
Barat
', '
Kecamatan
', ''),
(6705, 29, 338, 4749, '
Parigi
Selatan
', '
Kecamatan
', ''),
(6706, 29, 338, 4750, '
Parigi
Tengah
', '
Kecamatan
', ''),
(6707, 29, 338, 4751, '
Parigi
Utara
', '
Kecamatan
', ''),
(6708, 29, 338, 4752, '
Sausu
', '
Kecamatan
', ''),
(6709, 29, 338, 4753, '
Siniu
', '
Kecamatan
', ''),
(6710, 29, 338, 4754, '
Taopa
', '
Kecamatan
', ''),
(6711, 29, 338, 4755, '
Tinombo
', '
Kecamatan
', ''),
(6712, 29, 338, 4756, '
Tinombo
Selatan
', '
Kecamatan
', ''),
(6713, 29, 338, 4757, '
Tomini
', '
Kecamatan
', ''),
(6714, 29, 338, 4758, '
Toribulu
', '
Kecamatan
', ''),
(6715, 29, 338, 4759, '
Torue
', '
Kecamatan
', ''),
(6716, 29, 366, 5107, '
Lage
', '
Kecamatan
', ''),
(6717, 29, 366, 5108, '
Lore
Barat
', '
Kecamatan
', ''),
(6718, 29, 366, 5109, '
Lore
Piore
', '
Kecamatan
', ''),
(6719, 29, 366, 5110, '
Lore
Selatan
', '
Kecamatan
', ''),
(6720, 29, 366, 5111, '
Lore
Tengah
', '
Kecamatan
', ''),
(6721, 29, 366, 5112, '
Lore
Timur
', '
Kecamatan
', ''),
(6722, 29, 366, 5113, '
Lore
Utara
', '
Kecamatan
', ''),
(6723, 29, 366, 5114, '
Pamona
Barat
', '
Kecamatan
', ''),
(6724, 29, 366, 5115, '
Pamona
Puselemba
', '
Kecamatan
', ''),
(6725, 29, 366, 5116, '
Pamona
Selatan
', '
Kecamatan
', ''),
(6726, 29, 366, 5117, '
Pamona
Tenggara
', '
Kecamatan
', ''),
(6727, 29, 366, 5118, '
Pamona
Timur
', '
Kecamatan
', ''),
(6728, 29, 366, 5119, '
Pamona
Utara
', '
Kecamatan
', ''),
(6729, 29, 366, 5120, '
Poso
Kota
', '
Kecamatan
', ''),
(6730, 29, 366, 5121, '
Poso
Kota
Selatan
', '
Kecamatan
', ''),
(6731, 29, 366, 5122, '
Poso
Kota
Utara
', '
Kecamatan
', ''),
(6732, 29, 366, 5123, '
Poso
Pesisir
', '
Kecamatan
', ''),
(6733, 29, 366, 5124, '
Poso
Pesisir
Selatan
', '
Kecamatan
', ''),
(6734, 29, 366, 5125, '
Poso
Pesisir
Utara
', '
Kecamatan
', ''),
(6735, 29, 410, 5649, '
Dolo
', '
Kecamatan
', ''),
(6736, 29, 410, 5650, '
Dolo
Barat
', '
Kecamatan
', ''),
(6737, 29, 410, 5651, '
Dolo
Selatan
', '
Kecamatan
', ''),
(6738, 29, 410, 5652, '
Gumbasa
', '
Kecamatan
', ''),
(6739, 29, 410, 5653, '
Kinovaru
', '
Kecamatan
', ''),
(6740, 29, 410, 5654, '
Kulawi
', '
Kecamatan
', ''),
(6741, 29, 410, 5655, '
Kulawi
Selatan
', '
Kecamatan
', ''),
(6742, 29, 410, 5656, '
Lindu
', '
Kecamatan
', ''),
(6743, 29, 410, 5657, '
Marawola
', '
Kecamatan
', ''),
(6744, 29, 410, 5658, '
Marawola
Barat
', '
Kecamatan
', ''),
(6745, 29, 410, 5659, '
Nokilalaki
', '
Kecamatan
', ''),
(6746, 29, 410, 5660, '
Palolo
', '
Kecamatan
', ''),
(6747, 29, 410, 5661, '
Pipikoro
', '
Kecamatan
', ''),
(6748, 29, 410, 5662, '
Sigi
Biromaru
', '
Kecamatan
', ''),
(6749, 29, 410, 5663, '
Tanambulava
', '
Kecamatan
', ''),
(6750, 29, 482, 6668, '
Ampana
Kota
', '
Kecamatan
', ''),
(6751, 29, 482, 6669, '
Ampana
Tete
', '
Kecamatan
', ''),
(6752, 29, 482, 6670, '
Togean
', '
Kecamatan
', ''),
(6753, 29, 482, 6671, '
Tojo
', '
Kecamatan
', ''),
(6754, 29, 482, 6672, '
Tojo
Barat
', '
Kecamatan
', ''),
(6755, 29, 482, 6673, '
Ulu
Bongka
', '
Kecamatan
', ''),
(6756, 29, 482, 6674, '
Una
-
Una
', '
Kecamatan
', ''),
(6757, 29, 482, 6675, '
Walea
Besar
', '
Kecamatan
', ''),
(6758, 29, 482, 6676, '
Walea
Kepulauan
', '
Kecamatan
', ''),
(6759, 29, 483, 6677, '
Baolan
', '
Kecamatan
', ''),
(6760, 29, 483, 6678, '
Basidondo
', '
Kecamatan
', ''),
(6761, 29, 483, 6679, '
Dako
Pamean
', '
Kecamatan
', ''),
(6762, 29, 483, 6680, '
Dampal
Selatan
', '
Kecamatan
', ''),
(6763, 29, 483, 6681, '
Dampal
Utara
', '
Kecamatan
', ''),
(6764, 29, 483, 6682, '
Dondo
', '
Kecamatan
', ''),
(6765, 29, 483, 6683, '
Galang
', '
Kecamatan
', ''),
(6766, 29, 483, 6684, '
Lampasio
', '
Kecamatan
', ''),
(6767, 29, 483, 6685, '
Ogo
Deide
', '
Kecamatan
', ''),
(6768, 29, 483, 6686, '
Tolitoli
Utara
', '
Kecamatan
', ''),
(6769, 30, 85, 1127, '
Kabaena
', '
Kecamatan
', ''),
(6770, 30, 85, 1128, '
Kabaena
Barat
', '
Kecamatan
', ''),
(6771, 30, 85, 1129, '
Kabaena
Selatan
', '
Kecamatan
', ''),
(6772, 30, 85, 1130, '
Kabaena
Tengah
', '
Kecamatan
', ''),
(6773, 30, 85, 1131, '
Kabaena
Timur
', '
Kecamatan
', ''),
(6774, 30, 85, 1132, '
Kabaena
Utara
', '
Kecamatan
', ''),
(6775, 30, 85, 1133, '
Kepulauan
Masaloka
Raya
', '
Kecamatan
', ''),
(6776, 30, 85, 1134, '
Lentarai
Jaya
S
.
(
Lantari
Jaya
)
', '
Kecamatan
', ''),
(6777, 30, 85, 1135, '
Mata
Oleo
', '
Kecamatan
', ''),
(6778, 30, 85, 1136, '
Mata
Usu
', '
Kecamatan
', ''),
(6779, 30, 85, 1137, '
Poleang
', '
Kecamatan
', ''),
(6780, 30, 85, 1138, '
Poleang
Barat
', '
Kecamatan
', ''),
(6781, 30, 85, 1139, '
Poleang
Selatan
', '
Kecamatan
', ''),
(6782, 30, 85, 1140, '
Poleang
Tengah
', '
Kecamatan
', ''),
(6783, 30, 85, 1141, '
Poleang
Tenggara
', '
Kecamatan
', ''),
(6784, 30, 85, 1142, '
Poleang
Timur
', '
Kecamatan
', ''),
(6785, 30, 85, 1143, '
Poleang
Utara
', '
Kecamatan
', ''),
(6786, 30, 85, 1144, '
Rarowatu
', '
Kecamatan
', ''),
(6787, 30, 85, 1145, '
Rarowatu
Utara
', '
Kecamatan
', ''),
(6788, 30, 85, 1146, '
Rumbia
', '
Kecamatan
', ''),
(6789, 30, 85, 1147, '
Rumbia
Tengah
', '
Kecamatan
', ''),
(6790, 30, 85, 1148, '
Tontonunu
(
Tontonuwu
)
', '
Kecamatan
', ''),
(6791, 30, 101, 1352, '
Batauga
', '
Kecamatan
', ''),
(6792, 30, 101, 1353, '
Batu
Atas
', '
Kecamatan
', ''),
(6793, 30, 101, 1354, '
Gu
', '
Kecamatan
', ''),
(6794, 30, 101, 1355, '
Kadatua
', '
Kecamatan
', ''),
(6795, 30, 101, 1356, '
Kapontori
', '
Kecamatan
', ''),
(6796, 30, 101, 1357, '
Lakudo
', '
Kecamatan
', ''),
(6797, 30, 101, 1358, '
Lapandewa
', '
Kecamatan
', ''),
(6798, 30, 101, 1359, '
Lasalimu
', '
Kecamatan
', ''),
(6799, 30, 101, 1360, '
Lasalimu
Selatan
', '
Kecamatan
', ''),
(6800, 30, 101, 1361, '
Mawasangka
', '
Kecamatan
', ''),
(6801, 30, 101, 1362, '
Mawasangka
Tengah
', '
Kecamatan
', ''),
(6802, 30, 101, 1363, '
Mawasangka
Timur
', '
Kecamatan
', ''),
(6803, 30, 101, 1364, '
Pasar
Wajo
', '
Kecamatan
', ''),
(6804, 30, 101, 1365, '
Sampolawa
', '
Kecamatan
', ''),
(6805, 30, 101, 1366, '
Sangia
Mambulu
', '
Kecamatan
', ''),
(6806, 30, 101, 1367, '
Siompu
', '
Kecamatan
', ''),
(6807, 30, 101, 1368, '
Siompu
Barat
', '
Kecamatan
', ''),
(6808, 30, 101, 1369, '
Siontapia
(
Siontapina
)
', '
Kecamatan
', ''),
(6809, 30, 101, 1370, '
Talaga
Raya
(
Telaga
Raya
)
', '
Kecamatan
', ''),
(6810, 30, 101, 1371, '
Wabula
', '
Kecamatan
', ''),
(6811, 30, 101, 1372, '
Wolowa
', '
Kecamatan
', ''),
(6812, 30, 102, 1373, '
Bonegunu
', '
Kecamatan
', ''),
(6813, 30, 102, 1374, '
Kambowa
', '
Kecamatan
', ''),
(6814, 30, 102, 1375, '
Kulisusu
(
Kalingsusu
/
Kalisusu
)
', '
Kecamatan
', ''),
(6815, 30, 102, 1376, '
Kulisusu
Barat
', '
Kecamatan
', ''),
(6816, 30, 102, 1377, '
Kulisusu
Utara
', '
Kecamatan
', '');
INSERT INTO `location` (`location_id`, `province_id`, `city_id`, `subdistrict_id`, `name`, `type`, `postcode`) VALUES
(6817, 30, 102, 1378, '
Wakorumba
Utara
', '
Kecamatan
', ''),
(6818, 30, 198, 2752, '
Baula
', '
Kecamatan
', ''),
(6819, 30, 198, 2753, '
Kolaka
', '
Kecamatan
', ''),
(6820, 30, 198, 2754, '
Ladongi
', '
Kecamatan
', ''),
(6821, 30, 198, 2755, '
Lalolae
', '
Kecamatan
', ''),
(6822, 30, 198, 2756, '
Lambandia
(
Lambadia
)
', '
Kecamatan
', ''),
(6823, 30, 198, 2757, '
Latambaga
', '
Kecamatan
', ''),
(6824, 30, 198, 2758, '
Loea
', '
Kecamatan
', ''),
(6825, 30, 198, 2759, '
Mowewe
', '
Kecamatan
', ''),
(6826, 30, 198, 2760, '
Poli
Polia
', '
Kecamatan
', ''),
(6827, 30, 198, 2761, '
Polinggona
', '
Kecamatan
', ''),
(6828, 30, 198, 2762, '
Pomalaa
', '
Kecamatan
', ''),
(6829, 30, 198, 2763, '
Samaturu
', '
Kecamatan
', ''),
(6830, 30, 198, 2764, '
Tanggetada
', '
Kecamatan
', ''),
(6831, 30, 198, 2765, '
Tinondo
', '
Kecamatan
', ''),
(6832, 30, 198, 2766, '
Tirawuta
', '
Kecamatan
', ''),
(6833, 30, 198, 2767, '
Toari
', '
Kecamatan
', ''),
(6834, 30, 198, 2768, '
Uluiwoi
', '
Kecamatan
', ''),
(6835, 30, 198, 2769, '
Watumbangga
(
Watubanggo
)
', '
Kecamatan
', ''),
(6836, 30, 198, 2770, '
Wolo
', '
Kecamatan
', ''),
(6837, 30, 198, 2771, '
Wundulako
', '
Kecamatan
', ''),
(6838, 30, 199, 2772, '
Batu
Putih
', '
Kecamatan
', ''),
(6839, 30, 199, 2773, '
Katoi
', '
Kecamatan
', ''),
(6840, 30, 199, 2774, '
Kodeoha
', '
Kecamatan
', ''),
(6841, 30, 199, 2775, '
Lasusua
', '
Kecamatan
', ''),
(6842, 30, 199, 2776, '
Lombai
(
Lambai
)
', '
Kecamatan
', ''),
(6843, 30, 199, 2777, '
Ngapa
', '
Kecamatan
', ''),
(6844, 30, 199, 2778, '
Pakue
', '
Kecamatan
', ''),
(6845, 30, 199, 2779, '
Pakue
Tengah
', '
Kecamatan
', ''),
(6846, 30, 199, 2780, '
Pakue
Utara
', '
Kecamatan
', ''),
(6847, 30, 199, 2781, '
Porehu
', '
Kecamatan
', ''),
(6848, 30, 199, 2782, '
Ranteangin
', '
Kecamatan
', ''),
(6849, 30, 199, 2783, '
Tiwu
', '
Kecamatan
', ''),
(6850, 30, 199, 2784, '
Tolala
', '
Kecamatan
', ''),
(6851, 30, 199, 2785, '
Watunohu
', '
Kecamatan
', ''),
(6852, 30, 199, 2786, '
Wawo
', '
Kecamatan
', ''),
(6853, 30, 200, 2787, '
Abuki
', '
Kecamatan
', ''),
(6854, 30, 200, 2788, '
Amonggedo
', '
Kecamatan
', ''),
(6855, 30, 200, 2789, '
Anggaberi
', '
Kecamatan
', ''),
(6856, 30, 200, 2790, '
Asinua
', '
Kecamatan
', ''),
(6857, 30, 200, 2791, '
Besulutu
', '
Kecamatan
', ''),
(6858, 30, 200, 2792, '
Bondoala
', '
Kecamatan
', ''),
(6859, 30, 200, 2793, '
Kapoiala
(
Kapoyala
)
', '
Kecamatan
', ''),
(6860, 30, 200, 2794, '
Konawe
', '
Kecamatan
', ''),
(6861, 30, 200, 2795, '
Lalonggasumeeto
', '
Kecamatan
', ''),
(6862, 30, 200, 2796, '
Lambuya
', '
Kecamatan
', ''),
(6863, 30, 200, 2797, '
Latoma
', '
Kecamatan
', ''),
(6864, 30, 200, 2798, '
Meluhu
', '
Kecamatan
', ''),
(6865, 30, 200, 2799, '
Onembute
', '
Kecamatan
', ''),
(6866, 30, 200, 2800, '
Pondidaha
', '
Kecamatan
', ''),
(6867, 30, 200, 2801, '
Puriala
', '
Kecamatan
', ''),
(6868, 30, 200, 2802, '
Routa
', '
Kecamatan
', ''),
(6869, 30, 200, 2803, '
Sampara
', '
Kecamatan
', ''),
(6870, 30, 200, 2804, '
Soropia
', '
Kecamatan
', ''),
(6871, 30, 200, 2805, '
Tongauna
', '
Kecamatan
', ''),
(6872, 30, 200, 2806, '
Uepai
(
Uwepai
)
', '
Kecamatan
', ''),
(6873, 30, 200, 2807, '
Unaaha
', '
Kecamatan
', ''),
(6874, 30, 200, 2808, '
Wawonii
Barat
', '
Kecamatan
', ''),
(6875, 30, 200, 2809, '
Wawonii
Selatan
', '
Kecamatan
', ''),
(6876, 30, 200, 2810, '
Wawonii
Tengah
', '
Kecamatan
', ''),
(6877, 30, 200, 2811, '
Wawonii
Tenggara
', '
Kecamatan
', ''),
(6878, 30, 200, 2812, '
Wawonii
Timur
', '
Kecamatan
', ''),
(6879, 30, 200, 2813, '
Wawonii
Timur
Laut
', '
Kecamatan
', ''),
(6880, 30, 200, 2814, '
Wawonii
Utara
', '
Kecamatan
', ''),
(6881, 30, 200, 2815, '
Wawotobi
', '
Kecamatan
', ''),
(6882, 30, 200, 2816, '
Wonggeduku
', '
Kecamatan
', ''),
(6883, 30, 201, 2817, '
Andoolo
', '
Kecamatan
', ''),
(6884, 30, 201, 2818, '
Angata
', '
Kecamatan
', ''),
(6885, 30, 201, 2819, '
Baito
', '
Kecamatan
', ''),
(6886, 30, 201, 2820, '
Basala
', '
Kecamatan
', ''),
(6887, 30, 201, 2821, '
Benua
', '
Kecamatan
', ''),
(6888, 30, 201, 2822, '
Buke
', '
Kecamatan
', ''),
(6889, 30, 201, 2823, '
Kolono
', '
Kecamatan
', ''),
(6890, 30, 201, 2824, '
Konda
', '
Kecamatan
', ''),
(6891, 30, 201, 2825, '
Laeya
', '
Kecamatan
', ''),
(6892, 30, 201, 2826, '
Lainea
', '
Kecamatan
', ''),
(6893, 30, 201, 2827, '
Lalembuu
/
Lalumbuu
', '
Kecamatan
', ''),
(6894, 30, 201, 2828, '
Landono
', '
Kecamatan
', ''),
(6895, 30, 201, 2829, '
Laonti
', '
Kecamatan
', ''),
(6896, 30, 201, 2830, '
Moramo
', '
Kecamatan
', ''),
(6897, 30, 201, 2831, '
Moramo
Utara
', '
Kecamatan
', ''),
(6898, 30, 201, 2832, '
Mowila
', '
Kecamatan
', ''),
(6899, 30, 201, 2833, '
Palangga
', '
Kecamatan
', ''),
(6900, 30, 201, 2834, '
Palangga
Selatan
', '
Kecamatan
', ''),
(6901, 30, 201, 2835, '
Ranomeeto
', '
Kecamatan
', ''),
(6902, 30, 201, 2836, '
Ranomeeto
Barat
', '
Kecamatan
', ''),
(6903, 30, 201, 2837, '
Tinanggea
', '
Kecamatan
', ''),
(6904, 30, 201, 2838, '
Wolasi
', '
Kecamatan
', ''),
(6905, 30, 202, 2839, '
Andowia
', '
Kecamatan
', ''),
(6906, 30, 202, 2840, '
Asera
', '
Kecamatan
', ''),
(6907, 30, 202, 2841, '
Langgikima
', '
Kecamatan
', ''),
(6908, 30, 202, 2842, '
Lasolo
', '
Kecamatan
', ''),
(6909, 30, 202, 2843, '
Lembo
', '
Kecamatan
', ''),
(6910, 30, 202, 2844, '
Molawe
', '
Kecamatan
', ''),
(6911, 30, 202, 2845, '
Motui
', '
Kecamatan
', ''),
(6912, 30, 202, 2846, '
Oheo
', '
Kecamatan
', ''),
(6913, 30, 202, 2847, '
Sawa
', '
Kecamatan
', ''),
(6914, 30, 202, 2848, '
Wiwirano
', '
Kecamatan
', ''),
(6915, 30, 494, 6854, '
Binongko
', '
Kecamatan
', ''),
(6916, 30, 494, 6855, '
Kaledupa
', '
Kecamatan
', ''),
(6917, 30, 494, 6856, '
Kaledupa
Selatan
', '
Kecamatan
', ''),
(6918, 30, 494, 6857, '
Togo
Binongko
', '
Kecamatan
', ''),
(6919, 30, 494, 6858, '
Tomia
', '
Kecamatan
', ''),
(6920, 30, 494, 6859, '
Tomia
Timur
', '
Kecamatan
', ''),
(6921, 30, 494, 6860, '
Wangi
-
Wangi
', '
Kecamatan
', ''),
(6922, 30, 494, 6861, '
Wangi
-
Wangi
Selatan
', '
Kecamatan
', ''),
(6923, 30, 53, 718, '
Batupoaro
', '
Kecamatan
', ''),
(6924, 30, 53, 719, '
Betoambari
', '
Kecamatan
', ''),
(6925, 30, 53, 720, '
Bungi
', '
Kecamatan
', ''),
(6926, 30, 53, 721, '
Kokalukuna
', '
Kecamatan
', ''),
(6927, 30, 53, 722, '
Lea
-
Lea
', '
Kecamatan
', ''),
(6928, 30, 53, 723, '
Murhum
', '
Kecamatan
', ''),
(6929, 30, 53, 724, '
Sora
Walio
(
Sorowalio
)
', '
Kecamatan
', ''),
(6930, 30, 53, 725, '
Wolio
', '
Kecamatan
', ''),
(6931, 30, 182, 2553, '
Abeli
', '
Kecamatan
', ''),
(6932, 30, 182, 2554, '
Baruga
', '
Kecamatan
', ''),
(6933, 30, 182, 2555, '
Kadia
', '
Kecamatan
', ''),
(6934, 30, 182, 2556, '
Kambu
', '
Kecamatan
', ''),
(6935, 30, 182, 2557, '
Kendari
', '
Kecamatan
', ''),
(6936, 30, 182, 2558, '
Kendari
Barat
', '
Kecamatan
', ''),
(6937, 30, 182, 2559, '
Mandonga
', '
Kecamatan
', ''),
(6938, 30, 182, 2560, '
Poasia
', '
Kecamatan
', ''),
(6939, 30, 182, 2561, '
Puuwatu
', '
Kecamatan
', ''),
(6940, 30, 182, 2562, '
Wua
-
Wua
', '
Kecamatan
', ''),
(6941, 31, 73, 0, '
Bitung
', '
Kota
', '
95512
'),
(6942, 31, 81, 0, '
Bolaang
Mongondow
(
Bolmong
)
', '
Kabupaten
', '
95755
'),
(6943, 31, 82, 0, '
Bolaang
Mongondow
Selatan
', '
Kabupaten
', '
95774
'),
(6944, 31, 83, 0, '
Bolaang
Mongondow
Timur
', '
Kabupaten
', '
95783
'),
(6945, 31, 84, 0, '
Bolaang
Mongondow
Utara
', '
Kabupaten
', '
95765
'),
(6946, 31, 188, 0, '
Kepulauan
Sangihe
', '
Kabupaten
', '
95819
'),
(6947, 31, 190, 0, '
Kepulauan
Siau
Tagulandang
Biaro
(
Sitaro
)
', '
Kabupaten
', '
95862
'),
(6948, 31, 192, 0, '
Kepulauan
Talaud
', '
Kabupaten
', '
95885
'),
(6949, 31, 204, 0, '
Kotamobagu
', '
Kota
', '
95711
'),
(6950, 31, 267, 0, '
Manado
', '
Kota
', '
95247
'),
(6951, 31, 285, 0, '
Minahasa
', '
Kabupaten
', '
95614
'),
(6952, 31, 286, 0, '
Minahasa
Selatan
', '
Kabupaten
', '
95914
'),
(6953, 31, 287, 0, '
Minahasa
Tenggara
', '
Kabupaten
', '
95995
'),
(6954, 31, 288, 0, '
Minahasa
Utara
', '
Kabupaten
', '
95316
'),
(6955, 31, 485, 0, '
Tomohon
', '
Kota
', '
95416
'),
(6956, 31, 73, 966, '
Aertembaga
(
Bitung
Timur
)
', '
Kecamatan
', ''),
(6957, 31, 73, 967, '
Girian
', '
Kecamatan
', ''),
(6958, 31, 73, 968, '
Lembeh
Selatan
(
Bitung
Selatan
)
', '
Kecamatan
', ''),
(6959, 31, 73, 969, '
Lembeh
Utara
', '
Kecamatan
', ''),
(6960, 31, 73, 970, '
Madidir
(
Bitung
Tengah
)
', '
Kecamatan
', ''),
(6961, 31, 73, 971, '
Maesa
', '
Kecamatan
', ''),
(6962, 31, 73, 972, '
Matuari
(
Bitung
Barat
)
', '
Kecamatan
', ''),
(6963, 31, 73, 973, '
Ranowulu
(
Bitung
Utara
)
', '
Kecamatan
', ''),
(6964, 31, 81, 1096, '
Bilalang
', '
Kecamatan
', ''),
(6965, 31, 81, 1097, '
Bolaang
', '
Kecamatan
', ''),
(6966, 31, 81, 1098, '
Bolaang
Timur
', '
Kecamatan
', ''),
(6967, 31, 81, 1099, '
Dumoga
', '
Kecamatan
', ''),
(6968, 31, 81, 1100, '
Dumoga
Barat
', '
Kecamatan
', ''),
(6969, 31, 81, 1101, '
Dumoga
Tengah
', '
Kecamatan
', ''),
(6970, 31, 81, 1102, '
Dumoga
Tenggara
', '
Kecamatan
', ''),
(6971, 31, 81, 1103, '
Dumoga
Timur
', '
Kecamatan
', ''),
(6972, 31, 81, 1104, '
Dumoga
Utara
', '
Kecamatan
', ''),
(6973, 31, 81, 1105, '
Lolak
', '
Kecamatan
', ''),
(6974, 31, 81, 1106, '
Lolayan
', '
Kecamatan
', ''),
(6975, 31, 81, 1107, '
Passi
Barat
', '
Kecamatan
', ''),
(6976, 31, 81, 1108, '
Passi
Timur
', '
Kecamatan
', ''),
(6977, 31, 81, 1109, '
Poigar
', '
Kecamatan
', ''),
(6978, 31, 81, 1110, '
Sangtombolang
', '
Kecamatan
', ''),
(6979, 31, 82, 1111, '
Bolaang
Uki
', '
Kecamatan
', ''),
(6980, 31, 82, 1112, '
Pinolosian
', '
Kecamatan
', ''),
(6981, 31, 82, 1113, '
Pinolosian
Tengah
', '
Kecamatan
', ''),
(6982, 31, 82, 1114, '
Pinolosian
Timur
', '
Kecamatan
', ''),
(6983, 31, 82, 1115, '
Posigadan
', '
Kecamatan
', ''),
(6984, 31, 83, 1116, '
Kotabunan
', '
Kecamatan
', ''),
(6985, 31, 83, 1117, '
Modayag
', '
Kecamatan
', ''),
(6986, 31, 83, 1118, '
Modayag
Barat
', '
Kecamatan
', ''),
(6987, 31, 83, 1119, '
Nuangan
', '
Kecamatan
', ''),
(6988, 31, 83, 1120, '
Tutuyan
', '
Kecamatan
', ''),
(6989, 31, 84, 1121, '
Bintauna
', '
Kecamatan
', ''),
(6990, 31, 84, 1122, '
Bolang
Itang
Barat
', '
Kecamatan
', ''),
(6991, 31, 84, 1123, '
Bolang
Itang
Timur
', '
Kecamatan
', ''),
(6992, 31, 84, 1124, '
Kaidipang
', '
Kecamatan
', ''),
(6993, 31, 84, 1125, '
Pinogaluman
', '
Kecamatan
', ''),
(6994, 31, 84, 1126, '
Sangkub
', '
Kecamatan
', ''),
(6995, 31, 188, 2607, '
Kendahe
', '
Kecamatan
', ''),
(6996, 31, 188, 2608, '
Kepulauan
Marore
', '
Kecamatan
', ''),
(6997, 31, 188, 2609, '
Manganitu
', '
Kecamatan
', ''),
(6998, 31, 188, 2610, '
Manganitu
Selatan
', '
Kecamatan
', ''),
(6999, 31, 188, 2611, '
Nusa
Tabukan
', '
Kecamatan
', ''),
(7000, 31, 188, 2612, '
Tabukan
Selatan
', '
Kecamatan
', ''),
(7001, 31, 188, 2613, '
Tabukan
Selatan
Tengah
', '
Kecamatan
', ''),
(7002, 31, 188, 2614, '
Tabukan
Selatan
Tenggara
', '
Kecamatan
', ''),
(7003, 31, 188, 2615, '
Tabukan
Tengah
', '
Kecamatan
', ''),
(7004, 31, 188, 2616, '
Tabukan
Utara
', '
Kecamatan
', ''),
(7005, 31, 188, 2617, '
Tahuna
', '
Kecamatan
', ''),
(7006, 31, 188, 2618, '
Tahuna
Barat
', '
Kecamatan
', ''),
(7007, 31, 188, 2619, '
Tahuna
Timur
', '
Kecamatan
', ''),
(7008, 31, 188, 2620, '
Tamako
', '
Kecamatan
', ''),
(7009, 31, 188, 2621, '
Tatoareng
', '
Kecamatan
', ''),
(7010, 31, 190, 2624, '
Biaro
', '
Kecamatan
', ''),
(7011, 31, 190, 2625, '
Siau
Barat
', '
Kecamatan
', ''),
(7012, 31, 190, 2626, '
Siau
Barat
Selatan
', '
Kecamatan
', ''),
(7013, 31, 190, 2627, '
Siau
Barat
Utara
', '
Kecamatan
', ''),
(7014, 31, 190, 2628, '
Siau
Tengah
', '
Kecamatan
', ''),
(7015, 31, 190, 2629, '
Siau
Timur
', '
Kecamatan
', ''),
(7016, 31, 190, 2630, '
Siau
Timur
Selatan
', '
Kecamatan
', ''),
(7017, 31, 190, 2631, '
Tagulandang
', '
Kecamatan
', ''),
(7018, 31, 190, 2632, '
Tagulandang
Selatan
', '
Kecamatan
', ''),
(7019, 31, 190, 2633, '
Tagulandang
Utara
', '
Kecamatan
', ''),
(7020, 31, 192, 2653, '
Beo
', '
Kecamatan
', ''),
(7021, 31, 192, 2654, '
Beo
Selatan
', '
Kecamatan
', ''),
(7022, 31, 192, 2655, '
Beo
Utara
', '
Kecamatan
', ''),
(7023, 31, 192, 2656, '
Damao
(
Damau
)
', '
Kecamatan
', ''),
(7024, 31, 192, 2657, '
Essang
', '
Kecamatan
', ''),
(7025, 31, 192, 2658, '
Essang
Selatan
', '
Kecamatan
', ''),
(7026, 31, 192, 2659, '
Gemeh
', '
Kecamatan
', ''),
(7027, 31, 192, 2660, '
Kabaruan
', '
Kecamatan
', ''),
(7028, 31, 192, 2661, '
Kalongan
', '
Kecamatan
', ''),
(7029, 31, 192, 2662, '
Lirung
', '
Kecamatan
', ''),
(7030, 31, 192, 2663, '
Melonguane
', '
Kecamatan
', ''),
(7031, 31, 192, 2664, '
Melonguane
Timur
', '
Kecamatan
', ''),
(7032, 31, 192, 2665, '
Miangas
', '
Kecamatan
', ''),
(7033, 31, 192, 2666, '
Moronge
', '
Kecamatan
', ''),
(7034, 31, 192, 2667, '
Nanusa
', '
Kecamatan
', ''),
(7035, 31, 192, 2668, '
Pulutan
', '
Kecamatan
', ''),
(7036, 31, 192, 2669, '
Rainis
', '
Kecamatan
', ''),
(7037, 31, 192, 2670, '
Salibabu
', '
Kecamatan
', ''),
(7038, 31, 192, 2671, '
Tampan
Amma
', '
Kecamatan
', ''),
(7039, 31, 267, 3768, '
Bunaken
', '
Kecamatan
', ''),
(7040, 31, 267, 3769, '
Bunaken
Kepulauan
', '
Kecamatan
', ''),
(7041, 31, 267, 3770, '
Malalayang
', '
Kecamatan
', ''),
(7042, 31, 267, 3771, '
Mapanget
', '
Kecamatan
', ''),
(7043, 31, 267, 3772, '
Paal
Dua
', '
Kecamatan
', ''),
(7044, 31, 267, 3773, '
Sario
', '
Kecamatan
', ''),
(7045, 31, 267, 3774, '
Singkil
', '
Kecamatan
', ''),
(7046, 31, 267, 3775, '
Tikala
', '
Kecamatan
', ''),
(7047, 31, 267, 3776, '
Tuminiting
', '
Kecamatan
', ''),
(7048, 31, 267, 3777, '
Wanea
', '
Kecamatan
', ''),
(7049, 31, 267, 3778, '
Wenang
', '
Kecamatan
', ''),
(7050, 31, 204, 2870, '
Kotamobagu
Barat
', '
Kecamatan
', ''),
(7051, 31, 204, 2871, '
Kotamobagu
Selatan
', '
Kecamatan
', ''),
(7052, 31, 204, 2872, '
Kotamobagu
Timur
', '
Kecamatan
', ''),
(7053, 31, 204, 2873, '
Kotamobagu
Utara
', '
Kecamatan
', ''),
(7054, 31, 285, 4006, '
Eris
', '
Kecamatan
', ''),
(7055, 31, 285, 4007, '
Kakas
', '
Kecamatan
', ''),
(7056, 31, 285, 4008, '
Kakas
Barat
', '
Kecamatan
', ''),
(7057, 31, 285, 4009, '
Kawangkoan
', '
Kecamatan
', ''),
(7058, 31, 285, 4010, '
Kawangkoan
Barat
', '
Kecamatan
', ''),
(7059, 31, 285, 4011, '
Kawangkoan
Utara
', '
Kecamatan
', ''),
(7060, 31, 285, 4012, '
Kombi
', '
Kecamatan
', ''),
(7061, 31, 285, 4013, '
Langowan
Barat
', '
Kecamatan
', ''),
(7062, 31, 285, 4014, '
Langowan
Selatan
', '
Kecamatan
', ''),
(7063, 31, 285, 4015, '
Langowan
Timur
', '
Kecamatan
', ''),
(7064, 31, 285, 4016, '
Langowan
Utara
', '
Kecamatan
', ''),
(7065, 31, 285, 4017, '
Lembean
Timur
', '
Kecamatan
', ''),
(7066, 31, 285, 4018, '
Mandolang
', '
Kecamatan
', ''),
(7067, 31, 285, 4019, '
Pineleng
', '
Kecamatan
', ''),
(7068, 31, 285, 4020, '
Remboken
', '
Kecamatan
', ''),
(7069, 31, 285, 4021, '
Sonder
', '
Kecamatan
', ''),
(7070, 31, 285, 4022, '
Tombariri
', '
Kecamatan
', ''),
(7071, 31, 285, 4023, '
Tombariri
Timur
', '
Kecamatan
', ''),
(7072, 31, 285, 4024, '
Tombulu
', '
Kecamatan
', ''),
(7073, 31, 285, 4025, '
Tompaso
', '
Kecamatan
', ''),
(7074, 31, 285, 4026, '
Tompaso
Barat
', '
Kecamatan
', ''),
(7075, 31, 285, 4027, '
Tondano
Barat
', '
Kecamatan
', ''),
(7076, 31, 285, 4028, '
Tondano
Selatan
', '
Kecamatan
', ''),
(7077, 31, 285, 4029, '
Tondano
Timur
', '
Kecamatan
', ''),
(7078, 31, 285, 4030, '
Tondano
Utara
', '
Kecamatan
', ''),
(7079, 31, 286, 4031, '
Amurang
', '
Kecamatan
', ''),
(7080, 31, 286, 4032, '
Amurang
Barat
', '
Kecamatan
', ''),
(7081, 31, 286, 4033, '
Amurang
Timur
', '
Kecamatan
', ''),
(7082, 31, 286, 4034, '
Kumelembuai
', '
Kecamatan
', ''),
(7083, 31, 286, 4035, '
Maesaan
', '
Kecamatan
', ''),
(7084, 31, 286, 4036, '
Modoinding
', '
Kecamatan
', ''),
(7085, 31, 286, 4037, '
Motoling
', '
Kecamatan
', ''),
(7086, 31, 286, 4038, '
Motoling
Barat
', '
Kecamatan
', ''),
(7087, 31, 286, 4039, '
Motoling
Timur
', '
Kecamatan
', ''),
(7088, 31, 286, 4040, '
Ranoyapo
', '
Kecamatan
', ''),
(7089, 31, 286, 4041, '
Sinonsayang
', '
Kecamatan
', ''),
(7090, 31, 286, 4042, '
Suluun
Tareran
', '
Kecamatan
', ''),
(7091, 31, 286, 4043, '
Tareran
', '
Kecamatan
', ''),
(7092, 31, 286, 4044, '
Tatapaan
', '
Kecamatan
', ''),
(7093, 31, 286, 4045, '
Tenga
', '
Kecamatan
', ''),
(7094, 31, 286, 4046, '
Tompaso
Baru
', '
Kecamatan
', ''),
(7095, 31, 286, 4047, '
Tumpaan
', '
Kecamatan
', ''),
(7096, 31, 287, 4048, '
Belang
', '
Kecamatan
', ''),
(7097, 31, 287, 4049, '
Pasan
', '
Kecamatan
', ''),
(7098, 31, 287, 4050, '
Pusomaen
', '
Kecamatan
', ''),
(7099, 31, 287, 4051, '
Ratahan
', '
Kecamatan
', ''),
(7100, 31, 287, 4052, '
Ratahan
Timur
', '
Kecamatan
', ''),
(7101, 31, 287, 4053, '
Ratatotok
', '
Kecamatan
', ''),
(7102, 31, 287, 4054, '
Silian
Raya
', '
Kecamatan
', ''),
(7103, 31, 287, 4055, '
Tombatu
', '
Kecamatan
', ''),
(7104, 31, 287, 4056, '
Tombatu
Timur
', '
Kecamatan
', ''),
(7105, 31, 287, 4057, '
Tombatu
Utara
', '
Kecamatan
', ''),
(7106, 31, 287, 4058, '
Touluaan
', '
Kecamatan
', ''),
(7107, 31, 287, 4059, '
Touluaan
Selatan
', '
Kecamatan
', ''),
(7108, 31, 288, 4060, '
Airmadidi
', '
Kecamatan
', ''),
(7109, 31, 288, 4061, '
Dimembe
', '
Kecamatan
', ''),
(7110, 31, 288, 4062, '
Kalawat
', '
Kecamatan
', ''),
(7111, 31, 288, 4063, '
Kauditan
', '
Kecamatan
', ''),
(7112, 31, 288, 4064, '
Kema
', '
Kecamatan
', ''),
(7113, 31, 288, 4065, '
Likupang
Barat
', '
Kecamatan
', ''),
(7114, 31, 288, 4066, '
Likupang
Selatan
', '
Kecamatan
', ''),
(7115, 31, 288, 4067, '
Likupang
Timur
', '
Kecamatan
', ''),
(7116, 31, 288, 4068, '
Talawaan
', '
Kecamatan
', ''),
(7117, 31, 288, 4069, '
Wori
', '
Kecamatan
', ''),
(7118, 31, 485, 6733, '
Tomohon
Barat
', '
Kecamatan
', ''),
(7119, 31, 485, 6734, '
Tomohon
Selatan
', '
Kecamatan
', ''),
(7120, 31, 485, 6735, '
Tomohon
Tengah
', '
Kecamatan
', ''),
(7121, 31, 485, 6736, '
Tomohon
Timur
', '
Kecamatan
', ''),
(7122, 31, 485, 6737, '
Tomohon
Utara
', '
Kecamatan
', ''),
(7123, 32, 12, 176, '
Ampek
Nagari
(
IV
Nagari
)
', '
Kecamatan
', ''),
(7124, 32, 12, 177, '
Banuhampu
', '
Kecamatan
', ''),
(7125, 32, 12, 178, '
Baso
', '
Kecamatan
', ''),
(7126, 32, 12, 179, '
Candung
', '
Kecamatan
', ''),
(7127, 32, 12, 180, '
IV
Angkat
Candung
(
Ampek
Angkek
)
', '
Kecamatan
', ''),
(7128, 32, 12, 181, '
IV
Koto
(
Ampek
Koto
)
', '
Kecamatan
', ''),
(7129, 32, 12, 182, '
Kamang
Magek
', '
Kecamatan
', ''),
(7130, 32, 12, 183, '
Lubuk
Basung
', '
Kecamatan
', ''),
(7131, 32, 12, 184, '
Malakak
', '
Kecamatan
', ''),
(7132, 32, 12, 185, '
Matur
', '
Kecamatan
', ''),
(7133, 32, 12, 186, '
Palembayan
', '
Kecamatan
', ''),
(7134, 32, 12, 187, '
Palupuh
', '
Kecamatan
', ''),
(7135, 32, 12, 188, '
Sungai
Pua
(
Puar
)
', '
Kecamatan
', ''),
(7136, 32, 12, 189, '
Tanjung
Mutiara
', '
Kecamatan
', ''),
(7137, 32, 12, 190, '
Tanjung
Raya
', '
Kecamatan
', ''),
(7138, 32, 12, 191, '
Tilatang
Kamang
', '
Kecamatan
', ''),
(7139, 32, 116, 1588, '
Asam
Jujuhan
', '
Kecamatan
', ''),
(7140, 32, 116, 1589, '
Koto
Baru
', '
Kecamatan
', ''),
(7141, 32, 116, 1590, '
Koto
Besar
', '
Kecamatan
', ''),
(7142, 32, 116, 1591, '
Koto
Salak
', '
Kecamatan
', ''),
(7143, 32, 116, 1592, '
Padang
Laweh
', '
Kecamatan
', ''),
(7144, 32, 116, 1593, '
Pulau
Punjung
', '
Kecamatan
', ''),
(7145, 32, 116, 1594, '
Sembilan
Koto
(
IX
Koto
)
', '
Kecamatan
', ''),
(7146, 32, 116, 1595, '
Sitiung
', '
Kecamatan
', ''),
(7147, 32, 116, 1596, '
Sungai
Rumbai
', '
Kecamatan
', ''),
(7148, 32, 116, 1597, '
Timpeh
', '
Kecamatan
', ''),
(7149, 32, 116, 1598, '
Tiumang
', '
Kecamatan
', ''),
(7150, 32, 186, 2588, '
Pagai
Selatan
', '
Kecamatan
', ''),
(7151, 32, 186, 2589, '
Pagai
Utara
', '
Kecamatan
', ''),
(7152, 32, 186, 2590, '
Siberut
Barat
', '
Kecamatan
', ''),
(7153, 32, 186, 2591, '
Siberut
Barat
Daya
', '
Kecamatan
', ''),
(7154, 32, 186, 2592, '
Siberut
Selatan
', '
Kecamatan
', ''),
(7155, 32, 186, 2593, '
Siberut
Tengah
', '
Kecamatan
', ''),
(7156, 32, 186, 2594, '
Siberut
Utara
', '
Kecamatan
', ''),
(7157, 32, 186, 2595, '
Sikakap
', '
Kecamatan
', ''),
(7158, 32, 186, 2596, '
Sipora
Selatan
', '
Kecamatan
', ''),
(7159, 32, 186, 2597, '
Sipora
Utara
', '
Kecamatan
', ''),
(7160, 32, 236, 3351, '
Akabiluru
', '
Kecamatan
', ''),
(7161, 32, 236, 3352, '
Bukik
Barisan
', '
Kecamatan
', ''),
(7162, 32, 236, 3353, '
Guguak
(
Gugu
)
', '
Kecamatan
', ''),
(7163, 32, 236, 3354, '
Gunuang
Omeh
(
Gunung
Mas
)
', '
Kecamatan
', ''),
(7164, 32, 236, 3355, '
Harau
', '
Kecamatan
', ''),
(7165, 32, 236, 3356, '
Kapur
IX
/
Sembilan
', '
Kecamatan
', ''),
(7166, 32, 236, 3357, '
Lareh
Sago
Halaban
', '
Kecamatan
', ''),
(7167, 32, 236, 3358, '
Luak
(
Luhak
)
', '
Kecamatan
', ''),
(7168, 32, 236, 3359, '
Mungka
', '
Kecamatan
', ''),
(7169, 32, 236, 3360, '
Pangkalan
Koto
Baru
', '
Kecamatan
', ''),
(7170, 32, 236, 3361, '
Payakumbuh
', '
Kecamatan
', ''),
(7171, 32, 236, 3362, '
Situjuah
Limo
/
Lima
Nagari
', '
Kecamatan
', ''),
(7172, 32, 236, 3363, '
Suliki
', '
Kecamatan
', ''),
(7173, 32, 322, 4568, '
2
X
11
Enam
Lingkung
', '
Kecamatan
', ''),
(7174, 32, 322, 4569, '
2
X
11
Kayu
Tanam
', '
Kecamatan
', ''),
(7175, 32, 322, 4570, '
Batang
Anai
', '
Kecamatan
', ''),
(7176, 32, 322, 4571, '
Batang
Gasan
', '
Kecamatan
', ''),
(7177, 32, 322, 4572, '
Enam
Lingkung
', '
Kecamatan
', ''),
(7178, 32, 322, 4573, '
IV
Koto
Aur
Malintang
', '
Kecamatan
', ''),
(7179, 32, 322, 4574, '
Lubuk
Alung
', '
Kecamatan
', ''),
(7180, 32, 322, 4575, '
Nan
Sabaris
', '
Kecamatan
', ''),
(7181, 32, 322, 4576, '
Padang
Sago
', '
Kecamatan
', ''),
(7182, 32, 322, 4577, '
Patamuan
', '
Kecamatan
', ''),
(7183, 32, 322, 4578, '
Sintuk
/
Sintuak
Toboh
Gadang
', '
Kecamatan
', ''),
(7184, 32, 322, 4579, '
Sungai
Geringging
/
Garingging
', '
Kecamatan
', ''),
(7185, 32, 322, 4580, '
Sungai
Limau
', '
Kecamatan
', ''),
(7186, 32, 322, 4581, '
Ulakan
Tapakih
/
Tapakis
', '
Kecamatan
', ''),
(7187, 32, 322, 4582, '
V
Koto
Kampung
Dalam
', '
Kecamatan
', ''),
(7188, 32, 322, 4583, '
V
Koto
Timur
', '
Kecamatan
', ''),
(7189, 32, 322, 4584, '
VII
Koto
Sungai
Sarik
', '
Kecamatan
', ''),
(7190, 32, 339, 4760, '
Bonjol
', '
Kecamatan
', ''),
(7191, 32, 339, 4761, '
Duo
Koto
(
II
Koto
)
', '
Kecamatan
', ''),
(7192, 32, 339, 4762, '
Lubuk
Sikaping
', '
Kecamatan
', ''),
(7193, 32, 339, 4763, '
Mapat
Tunggul
', '
Kecamatan
', ''),
(7194, 32, 339, 4764, '
Mapat
Tunggul
Selatan
', '
Kecamatan
', ''),
(7195, 32, 339, 4765, '
Padang
Gelugur
', '
Kecamatan
', ''),
(7196, 32, 339, 4766, '
Panti
', '
Kecamatan
', ''),
(7197, 32, 339, 4767, '
Rao
', '
Kecamatan
', ''),
(7198, 32, 339, 4768, '
Rao
Selatan
', '
Kecamatan
', ''),
(7199, 32, 339, 4769, '
Rao
Utara
', '
Kecamatan
', ''),
(7200, 32, 339, 4770, '
Simpang
Alahan
Mati
', '
Kecamatan
', ''),
(7201, 32, 339, 4771, '
Tigo
Nagari
(
III
Nagari
)
', '
Kecamatan
', ''),
(7202, 32, 340, 4772, '
Gunung
Tuleh
', '
Kecamatan
', ''),
(7203, 32, 340, 4773, '
Kinali
', '
Kecamatan
', ''),
(7204, 32, 340, 4774, '
Koto
Balingka
', '
Kecamatan
', ''),
(7205, 32, 340, 4775, '
Lembah
Melintang
', '
Kecamatan
', ''),
(7206, 32, 340, 4776, '
Luhak
Nan
Duo
', '
Kecamatan
', ''),
(7207, 32, 340, 4777, '
Pasaman
', '
Kecamatan
', ''),
(7208, 32, 340, 4778, '
Ranah
Batahan
', '
Kecamatan
', ''),
(7209, 32, 340, 4779, '
Sasak
Ranah
Pasisir
/
Pesisie
', '
Kecamatan
', ''),
(7210, 32, 340, 4780, '
Sei
Beremas
', '
Kecamatan
', ''),
(7211, 32, 340, 4781, '
Sungai
Aur
', '
Kecamatan
', ''),
(7212, 32, 340, 4782, '
Talamau
', '
Kecamatan
', ''),
(7213, 32, 357, 4984, '
Airpura
', '
Kecamatan
', ''),
(7214, 32, 357, 4985, '
Basa
Ampek
Balai
Tapan
', '
Kecamatan
', ''),
(7215, 32, 357, 4986, '
Batang
Kapas
', '
Kecamatan
', ''),
(7216, 32, 357, 4987, '
Bayang
', '
Kecamatan
', ''),
(7217, 32, 357, 4988, '
IV
Jurai
', '
Kecamatan
', ''),
(7218, 32, 357, 4989, '
IV
Nagari
Bayang
Utara
', '
Kecamatan
', ''),
(7219, 32, 357, 4990, '
Koto
XI
Tarusan
', '
Kecamatan
', ''),
(7220, 32, 357, 4991, '
Lengayang
', '
Kecamatan
', ''),
(7221, 32, 357, 4992, '
Linggo
Sari
Baganti
', '
Kecamatan
', ''),
(7222, 32, 357, 4993, '
Lunang
', '
Kecamatan
', ''),
(7223, 32, 357, 4994, '
Pancung
Soal
', '
Kecamatan
', ''),
(7224, 32, 357, 4995, '
Ranah
Ampek
Hulu
Tapan
', '
Kecamatan
', ''),
(7225, 32, 357, 4996, '
Ranah
Pesisir
', '
Kecamatan
', ''),
(7226, 32, 357, 4997, '
Silaut
', '
Kecamatan
', ''),
(7227, 32, 357, 4998, '
Sutera
', '
Kecamatan
', ''),
(7228, 32, 411, 5664, '
IV
Nagari
', '
Kecamatan
', ''),
(7229, 32, 411, 5665, '
Kamang
Baru
', '
Kecamatan
', ''),
(7230, 32, 411, 5666, '
Koto
VII
', '
Kecamatan
', ''),
(7231, 32, 411, 5667, '
Kupitan
', '
Kecamatan
', ''),
(7232, 32, 411, 5668, '
Lubuak
Tarok
', '
Kecamatan
', ''),
(7233, 32, 411, 5669, '
Sijunjung
', '
Kecamatan
', ''),
(7234, 32, 411, 5670, '
Sumpur
Kudus
', '
Kecamatan
', ''),
(7235, 32, 411, 5671, '
Tanjung
Gadang
', '
Kecamatan
', ''),
(7236, 32, 420, 5796, '
Bukit
Sundi
', '
Kecamatan
', ''),
(7237, 32, 420, 5797, '
Danau
Kembar
', '
Kecamatan
', ''),
(7238, 32, 420, 5798, '
Gunung
Talang
', '
Kecamatan
', ''),
(7239, 32, 420, 5799, '
Hiliran
Gumanti
', '
Kecamatan
', ''),
(7240, 32, 420, 5800, '
IX
Koto
Sei
Lasi
', '
Kecamatan
', ''),
(7241, 32, 420, 5801, '
Junjung
Sirih
', '
Kecamatan
', ''),
(7242, 32, 420, 5802, '
Kubung
', '
Kecamatan
', ''),
(7243, 32, 420, 5803, '
Lembah
Gumanti
', '
Kecamatan
', ''),
(7244, 32, 420, 5804, '
Lembang
Jaya
', '
Kecamatan
', ''),
(7245, 32, 420, 5805, '
Pantai
Cermin
', '
Kecamatan
', ''),
(7246, 32, 420, 5806, '
Payung
Sekaki
', '
Kecamatan
', ''),
(7247, 32, 420, 5807, '
Tigo
Lurah
', '
Kecamatan
', ''),
(7248, 32, 420, 5808, '
X
Koto
Diatas
', '
Kecamatan
', ''),
(7249, 32, 420, 5809, '
X
Koto
Singkarak
', '
Kecamatan
', ''),
(7250, 32, 422, 5812, '
Koto
Parik
Gadang
Diateh
', '
Kecamatan
', ''),
(7251, 32, 422, 5813, '
Pauh
Duo
', '
Kecamatan
', ''),
(7252, 32, 422, 5814, '
Sangir
', '
Kecamatan
', ''),
(7253, 32, 422, 5815, '
Sangir
Balai
Janggo
', '
Kecamatan
', ''),
(7254, 32, 422, 5816, '
Sangir
Batang
Hari
', '
Kecamatan
', ''),
(7255, 32, 422, 5817, '
Sangir
Jujuan
', '
Kecamatan
', ''),
(7256, 32, 422, 5818, '
Sungai
Pagu
', '
Kecamatan
', ''),
(7257, 32, 453, 6243, '
Batipuh
', '
Kecamatan
', ''),
(7258, 32, 453, 6244, '
Batipuh
Selatan
', '
Kecamatan
', ''),
(7259, 32, 453, 6245, '
Lima
Kaum
', '
Kecamatan
', ''),
(7260, 32, 453, 6246, '
Lintau
Buo
', '
Kecamatan
', ''),
(7261, 32, 453, 6247, '
Lintau
Buo
Utara
', '
Kecamatan
', ''),
(7262, 32, 453, 6248, '
Padang
Ganting
', '
Kecamatan
', ''),
(7263, 32, 453, 6249, '
Pariangan
', '
Kecamatan
', ''),
(7264, 32, 453, 6250, '
Rambatan
', '
Kecamatan
', ''),
(7265, 32, 453, 6251, '
Salimpaung
', '
Kecamatan
', ''),
(7266, 32, 453, 6252, '
Sepuluh
Koto
(
X
Koto
)
', '
Kecamatan
', ''),
(7267, 32, 453, 6253, '
Sungai
Tarab
', '
Kecamatan
', ''),
(7268, 32, 453, 6254, '
Sungayang
', '
Kecamatan
', ''),
(7269, 32, 453, 6255, '
Tanjung
Baru
', '
Kecamatan
', ''),
(7270, 32, 453, 6256, '
Tanjung
Emas
', '
Kecamatan
', ''),
(7271, 32, 93, 1276, '
Aur
Birugo
Tigo
Baleh
', '
Kecamatan
', ''),
(7272, 32, 93, 1277, '
Guguk
Panjang
(
Guguak
Panjang
)
', '
Kecamatan
', ''),
(7273, 32, 93, 1278, '
Mandiangin
Koto
Selayan
', '
Kecamatan
', ''),
(7274, 32, 318, 4534, '
Bungus
Teluk
Kabung
', '
Kecamatan
', ''),
(7275, 32, 318, 4535, '
Koto
Tangah
', '
Kecamatan
', ''),
(7276, 32, 318, 4536, '
Kuranji
', '
Kecamatan
', ''),
(7277, 32, 318, 4537, '
Lubuk
Begalung
', '
Kecamatan
', ''),
(7278, 32, 318, 4538, '
Lubuk
Kilangan
', '
Kecamatan
', ''),
(7279, 32, 318, 4539, '
Nanggalo
', '
Kecamatan
', ''),
(7280, 32, 318, 4540, '
Padang
Barat
', '
Kecamatan
', ''),
(7281, 32, 318, 4541, '
Padang
Selatan
', '
Kecamatan
', ''),
(7282, 32, 318, 4542, '
Padang
Timur
', '
Kecamatan
', ''),
(7283, 32, 318, 4543, '
Padang
Utara
', '
Kecamatan
', ''),
(7284, 32, 318, 4544, '
Pauh
', '
Kecamatan
', ''),
(7285, 32, 321, 4566, '
Padang
Panjang
Barat
', '
Kecamatan
', ''),
(7286, 32, 321, 4567, '
Padang
Panjang
Timur
', '
Kecamatan
', ''),
(7287, 32, 337, 4734, '
Pariaman
Selatan
', '
Kecamatan
', ''),
(7288, 32, 337, 4735, '
Pariaman
Tengah
', '
Kecamatan
', ''),
(7289, 32, 337, 4736, '
Pariaman
Timur
', '
Kecamatan
', ''),
(7290, 32, 337, 4737, '
Pariaman
Utara
', '
Kecamatan
', ''),
(7291, 32, 345, 4842, '
Lamposi
Tigo
Nagari
', '
Kecamatan
', ''),
(7292, 32, 345, 4843, '
Payakumbuh
Barat
', '
Kecamatan
', ''),
(7293, 32, 345, 4844, '
Payakumbuh
Selatan
', '
Kecamatan
', ''),
(7294, 32, 345, 4845, '
Payakumbuh
Timur
', '
Kecamatan
', ''),
(7295, 32, 345, 4846, '
Payakumbuh
Utara
', '
Kecamatan
', ''),
(7296, 32, 394, 5443, '
Barangin
', '
Kecamatan
', ''),
(7297, 32, 394, 5444, '
Lembah
Segar
', '
Kecamatan
', ''),
(7298, 32, 394, 5445, '
Silungkang
', '
Kecamatan
', ''),
(7299, 32, 394, 5446, '
Talawi
', '
Kecamatan
', ''),
(7300, 32, 421, 5810, '
Lubuk
Sikarah
', '
Kecamatan
', ''),
(7301, 32, 421, 5811, '
Tanjung
Harapan
', '
Kecamatan
', ''),
(7302, 33, 40, 554, '
Air
Kumbang
', '
Kecamatan
', ''),
(7303, 33, 40, 555, '
Air
Salek
', '
Kecamatan
', ''),
(7304, 33, 40, 556, '
Banyuasin
I
', '
Kecamatan
', ''),
(7305, 33, 40, 557, '
Banyuasin
II
', '
Kecamatan
', ''),
(7306, 33, 40, 558, '
Banyuasin
III
', '
Kecamatan
', ''),
(7307, 33, 40, 559, '
Betung
', '
Kecamatan
', ''),
(7308, 33, 40, 560, '
Makarti
Jaya
', '
Kecamatan
', ''),
(7309, 33, 40, 561, '
Muara
Padang
', '
Kecamatan
', ''),
(7310, 33, 40, 562, '
Muara
Sugihan
', '
Kecamatan
', ''),
(7311, 33, 40, 563, '
Muara
Telang
', '
Kecamatan
', ''),
(7312, 33, 40, 564, '
Pulau
Rimau
', '
Kecamatan
', ''),
(7313, 33, 40, 565, '
Rambutan
', '
Kecamatan
', ''),
(7314, 33, 40, 566, '
Rantau
Bayur
', '
Kecamatan
', ''),
(7315, 33, 40, 567, '
Sembawa
', '
Kecamatan
', ''),
(7316, 33, 40, 568, '
Suak
Tapeh
', '
Kecamatan
', ''),
(7317, 33, 40, 569, '
Sumber
Marga
Telang
', '
Kecamatan
', ''),
(7318, 33, 40, 570, '
Talang
Kelapa
', '
Kecamatan
', ''),
(7319, 33, 40, 571, '
Tanjung
Lago
', '
Kecamatan
', ''),
(7320, 33, 40, 572, '
Tungkal
Ilir
', '
Kecamatan
', ''),
(7321, 33, 121, 1640, '
Lintang
Kanan
', '
Kecamatan
', ''),
(7322, 33, 121, 1641, '
Muara
Pinang
', '
Kecamatan
', ''),
(7323, 33, 121, 1642, '
Pasemah
Air
Keruh
', '
Kecamatan
', ''),
(7324, 33, 121, 1643, '
Pendopo
', '
Kecamatan
', ''),
(7325, 33, 121, 1644, '
Pendopo
Barat
', '
Kecamatan
', ''),
(7326, 33, 121, 1645, '
Saling
', '
Kecamatan
', ''),
(7327, 33, 121, 1646, '
Sikap
Dalam
', '
Kecamatan
', ''),
(7328, 33, 121, 1647, '
Talang
Padang
', '
Kecamatan
', ''),
(7329, 33, 121, 1648, '
Tebing
Tinggi
', '
Kecamatan
', ''),
(7330, 33, 121, 1649, '
Ulu
Musi
', '
Kecamatan
', ''),
(7331, 33, 220, 3083, '
Gumay
Talang
', '
Kecamatan
', ''),
(7332, 33, 220, 3084, '
Gumay
Ulu
', '
Kecamatan
', ''),
(7333, 33, 220, 3085, '
Jarai
', '
Kecamatan
', ''),
(7334, 33, 220, 3086, '
Kikim
Barat
', '
Kecamatan
', ''),
(7335, 33, 220, 3087, '
Kikim
Selatan
', '
Kecamatan
', ''),
(7336, 33, 220, 3088, '
Kikim
Tengah
', '
Kecamatan
', ''),
(7337, 33, 220, 3089, '
Kikim
Timur
', '
Kecamatan
', ''),
(7338, 33, 220, 3090, '
Kota
Agung
', '
Kecamatan
', ''),
(7339, 33, 220, 3091, '
Lahat
', '
Kecamatan
', ''),
(7340, 33, 220, 3092, '
Merapi
Barat
', '
Kecamatan
', ''),
(7341, 33, 220, 3093, '
Merapi
Selatan
', '
Kecamatan
', ''),
(7342, 33, 220, 3094, '
Merapi
Timur
', '
Kecamatan
', ''),
(7343, 33, 220, 3095, '
Muarapayang
', '
Kecamatan
', ''),
(7344, 33, 220, 3096, '
Mulak
Ulu
', '
Kecamatan
', ''),
(7345, 33, 220, 3097, '
Pagar
Gunung
', '
Kecamatan
', ''),
(7346, 33, 220, 3098, '
Pajar
Bulan
', '
Kecamatan
', ''),
(7347, 33, 220, 3099, '
Pseksu
', '
Kecamatan
', ''),
(7348, 33, 220, 3100, '
Pulau
Pinang
', '
Kecamatan
', ''),
(7349, 33, 220, 3101, '
Sukamerindu
', '
Kecamatan
', ''),
(7350, 33, 220, 3102, '
Tanjung
Sakti
Pumi
', '
Kecamatan
', ''),
(7351, 33, 220, 3103, '
Tanjung
Sakti
Pumu
', '
Kecamatan
', ''),
(7352, 33, 220, 3104, '
Tanjung
Tebat
', '
Kecamatan
', ''),
(7353, 33, 292, 4109, '
Abab
', '
Kecamatan
', ''),
(7354, 33, 292, 4110, '
Benakat
', '
Kecamatan
', ''),
(7355, 33, 292, 4111, '
Gelumbang
', '
Kecamatan
', ''),
(7356, 33, 292, 4112, '
Gunung
Megang
', '
Kecamatan
', ''),
(7357, 33, 292, 4113, '
Kelekar
', '
Kecamatan
', ''),
(7358, 33, 292, 4114, '
Lawang
Kidul
', '
Kecamatan
', ''),
(7359, 33, 292, 4115, '
Lembak
', '
Kecamatan
', ''),
(7360, 33, 292, 4116, '
Lubai
', '
Kecamatan
', ''),
(7361, 33, 292, 4117, '
Muara
Belida
', '
Kecamatan
', ''),
(7362, 33, 292, 4118, '
Muara
Enim
', '
Kecamatan
', ''),
(7363, 33, 292, 4119, '
Penukal
(
Penukal
Abab
)
', '
Kecamatan
', ''),
(7364, 33, 292, 4120, '
Penukal
Utara
', '
Kecamatan
', ''),
(7365, 33, 292, 4121, '
Rambang
', '
Kecamatan
', ''),
(7366, 33, 292, 4122, '
Rambang
Dangku
', '
Kecamatan
', ''),
(7367, 33, 292, 4123, '
Semendo
Darat
Laut
', '
Kecamatan
', ''),
(7368, 33, 292, 4124, '
Semendo
Darat
Tengah
', '
Kecamatan
', ''),
(7369, 33, 292, 4125, '
Semendo
Darat
Ulu
', '
Kecamatan
', ''),
(7370, 33, 292, 4126, '
Sungai
Rotan
', '
Kecamatan
', ''),
(7371, 33, 292, 4127, '
Talang
Ubi
', '
Kecamatan
', ''),
(7372, 33, 292, 4128, '
Tanah
Abang
', '
Kecamatan
', ''),
(7373, 33, 292, 4129, '
Tanjung
Agung
', '
Kecamatan
', ''),
(7374, 33, 292, 4130, '
Ujan
Mas
', '
Kecamatan
', ''),
(7375, 33, 298, 4214, '
Batukuning
Lakitan
Ulu
(
BTS
)
/
Cecar
', '
Kecamatan
', ''),
(7376, 33, 298, 4215, '
Jaya
Loka
', '
Kecamatan
', ''),
(7377, 33, 298, 4216, '
Karang
Dapo
', '
Kecamatan
', ''),
(7378, 33, 298, 4217, '
Karang
Jaya
', '
Kecamatan
', ''),
(7379, 33, 298, 4218, '
Megang
Sakti
', '
Kecamatan
', ''),
(7380, 33, 298, 4219, '
Muara
Beliti
', '
Kecamatan
', ''),
(7381, 33, 298, 4220, '
Muara
Kelingi
', '
Kecamatan
', ''),
(7382, 33, 298, 4221, '
Muara
Lakitan
', '
Kecamatan
', ''),
(7383, 33, 298, 4222, '
Nibung
', '
Kecamatan
', ''),
(7384, 33, 298, 4223, '
Purwodadi
', '
Kecamatan
', ''),
(7385, 33, 298, 4224, '
Rawas
Ilir
', '
Kecamatan
', ''),
(7386, 33, 298, 4225, '
Rawas
Ulu
', '
Kecamatan
', ''),
(7387, 33, 298, 4226, '
Rupit
', '
Kecamatan
', ''),
(7388, 33, 298, 4227, '
Selangit
', '
Kecamatan
', ''),
(7389, 33, 298, 4228, '
STL
Ulu
Terawas
', '
Kecamatan
', ''),
(7390, 33, 298, 4229, '
Sukakarya
', '
Kecamatan
', ''),
(7391, 33, 298, 4230, '
Sumber
Harta
', '
Kecamatan
', ''),
(7392, 33, 298, 4231, '
Tiang
Pumpung
Kepungut
', '
Kecamatan
', ''),
(7393, 33, 298, 4232, '
Tuah
Negeri
', '
Kecamatan
', ''),
(7394, 33, 298, 4233, '
Tugumulyo
', '
Kecamatan
', ''),
(7395, 33, 298, 4234, '
Ulu
Rawas
', '
Kecamatan
', ''),
(7396, 33, 297, 4200, '
Babat
Supat
', '
Kecamatan
', ''),
(7397, 33, 297, 4201, '
Babat
Toman
', '
Kecamatan
', ''),
(7398, 33, 297, 4202, '
Batanghari
Leko
', '
Kecamatan
', ''),
(7399, 33, 297, 4203, '
Bayung
Lencir
', '
Kecamatan
', ''),
(7400, 33, 297, 4204, '
Keluang
', '
Kecamatan
', ''),
(7401, 33, 297, 4205, '
Lais
', '
Kecamatan
', ''),
(7402, 33, 297, 4206, '
Lalan
(
Sungai
Lalan
)
', '
Kecamatan
', ''),
(7403, 33, 297, 4207, '
Lawang
Wetan
', '
Kecamatan
', ''),
(7404, 33, 297, 4208, '
Plakat
Tinggi
(
Pelakat
Tinggi
)
', '
Kecamatan
', ''),
(7405, 33, 297, 4209, '
Sanga
Desa
', '
Kecamatan
', ''),
(7406, 33, 297, 4210, '
Sekayu
', '
Kecamatan
', ''),
(7407, 33, 297, 4211, '
Sungai
Keruh
', '
Kecamatan
', ''),
(7408, 33, 297, 4212, '
Sungai
Lilin
', '
Kecamatan
', ''),
(7409, 33, 297, 4213, '
Tungkal
Jaya
', '
Kecamatan
', ''),
(7410, 33, 312, 4437, '
Indralaya
', '
Kecamatan
', ''),
(7411, 33, 312, 4438, '
Indralaya
Selatan
', '
Kecamatan
', ''),
(7412, 33, 312, 4439, '
Indralaya
Utara
', '
Kecamatan
', ''),
(7413, 33, 312, 4440, '
Kandis
', '
Kecamatan
', ''),
(7414, 33, 312, 4441, '
Lubuk
Keliat
', '
Kecamatan
', ''),
(7415, 33, 312, 4442, '
Muara
Kuang
', '
Kecamatan
', ''),
(7416, 33, 312, 4443, '
Payaraman
', '
Kecamatan
', ''),
(7417, 33, 312, 4444, '
Pemulutan
', '
Kecamatan
', ''),
(7418, 33, 312, 4445, '
Pemulutan
Barat
', '
Kecamatan
', ''),
(7419, 33, 312, 4446, '
Pemulutan
Selatan
', '
Kecamatan
', ''),
(7420, 33, 312, 4447, '
Rambang
Kuang
', '
Kecamatan
', ''),
(7421, 33, 312, 4448, '
Rantau
Alai
', '
Kecamatan
', ''),
(7422, 33, 312, 4449, '
Rantau
Panjang
', '
Kecamatan
', ''),
(7423, 33, 312, 4450, '
Sungai
Pinang
', '
Kecamatan
', ''),
(7424, 33, 312, 4451, '
Tanjung
Batu
', '
Kecamatan
', ''),
(7425, 33, 312, 4452, '
Tanjung
Raja
', '
Kecamatan
', ''),
(7426, 33, 313, 4453, '
Air
Sugihan
', '
Kecamatan
', ''),
(7427, 33, 313, 4454, '
Cengal
', '
Kecamatan
', ''),
(7428, 33, 313, 4455, '
Jejawi
', '
Kecamatan
', ''),
(7429, 33, 313, 4456, '
Kayu
Agung
', '
Kecamatan
', ''),
(7430, 33, 313, 4457, '
Lempuing
', '
Kecamatan
', ''),
(7431, 33, 313, 4458, '
Lempuing
Jaya
', '
Kecamatan
', ''),
(7432, 33, 313, 4459, '
Mesuji
', '
Kecamatan
', ''),
(7433, 33, 313, 4460, '
Mesuji
Makmur
', '
Kecamatan
', ''),
(7434, 33, 313, 4461, '
Mesuji
Raya
', '
Kecamatan
', ''),
(7435, 33, 313, 4462, '
Pampangan
', '
Kecamatan
', ''),
(7436, 33, 313, 4463, '
Pangkalan
Lampam
', '
Kecamatan
', ''),
(7437, 33, 313, 4464, '
Pedamaran
', '
Kecamatan
', ''),
(7438, 33, 313, 4465, '
Pedamaran
Timur
', '
Kecamatan
', ''),
(7439, 33, 313, 4466, '
Sirah
Pulau
Padang
', '
Kecamatan
', ''),
(7440, 33, 313, 4467, '
Sungai
Menang
', '
Kecamatan
', ''),
(7441, 33, 313, 4468, '
Tanjung
Lubuk
', '
Kecamatan
', ''),
(7442, 33, 313, 4469, '
Teluk
Gelam
', '
Kecamatan
', ''),
(7443, 33, 313, 4470, '
Tulung
Selapan
', '
Kecamatan
', ''),
(7444, 33, 314, 4471, '
Baturaja
Barat
', '
Kecamatan
', ''),
(7445, 33, 314, 4472, '
Baturaja
Timur
', '
Kecamatan
', ''),
(7446, 33, 314, 4473, '
Lengkiti
', '
Kecamatan
', ''),
(7447, 33, 314, 4474, '
Lubuk
Batang
', '
Kecamatan
', ''),
(7448, 33, 314, 4475, '
Lubuk
Raja
', '
Kecamatan
', ''),
(7449, 33, 314, 4476, '
Muara
Jaya
', '
Kecamatan
', ''),
(7450, 33, 314, 4477, '
Pengandonan
', '
Kecamatan
', ''),
(7451, 33, 314, 4478, '
Peninjauan
', '
Kecamatan
', ''),
(7452, 33, 314, 4479, '
Semidang
Aji
', '
Kecamatan
', ''),
(7453, 33, 314, 4480, '
Sinar
Peninjauan
', '
Kecamatan
', ''),
(7454, 33, 314, 4481, '
Sosoh
Buay
Rayap
', '
Kecamatan
', ''),
(7455, 33, 314, 4482, '
Ulu
Ogan
', '
Kecamatan
', ''),
(7456, 33, 315, 4483, '
Banding
Agung
', '
Kecamatan
', ''),
(7457, 33, 315, 4484, '
Buana
Pemaca
', '
Kecamatan
', ''),
(7458, 33, 315, 4485, '
Buay
Pemaca
', '
Kecamatan
', ''),
(7459, 33, 315, 4486, '
Buay
Pematang
Ribu
Ranau
Tengah
', '
Kecamatan
', ''),
(7460, 33, 315, 4487, '
Buay
Rawan
', '
Kecamatan
', ''),
(7461, 33, 315, 4488, '
Buay
Runjung
', '
Kecamatan
', ''),
(7462, 33, 315, 4489, '
Buay
Sandang
Aji
', '
Kecamatan
', ''),
(7463, 33, 315, 4490, '
Kisam
Ilir
', '
Kecamatan
', ''),
(7464, 33, 315, 4491, '
Kisam
Tinggi
', '
Kecamatan
', ''),
(7465, 33, 315, 4492, '
Mekakau
Ilir
', '
Kecamatan
', ''),
(7466, 33, 315, 4493, '
Muaradua
', '
Kecamatan
', ''),
(7467, 33, 315, 4494, '
Muaradua
Kisam
', '
Kecamatan
', ''),
(7468, 33, 315, 4495, '
Pulau
Beringin
', '
Kecamatan
', ''),
(7469, 33, 315, 4496, '
Runjung
Agung
', '
Kecamatan
', ''),
(7470, 33, 315, 4497, '
Simpang
', '
Kecamatan
', ''),
(7471, 33, 315, 4498, '
Sindang
Danau
', '
Kecamatan
', ''),
(7472, 33, 315, 4499, '
Sungai
Are
', '
Kecamatan
', ''),
(7473, 33, 315, 4500, '
Tiga
Dihaji
', '
Kecamatan
', ''),
(7474, 33, 315, 4501, '
Warkuk
Ranau
Selatan
', '
Kecamatan
', ''),
(7475, 33, 316, 4502, '
Belitang
', '
Kecamatan
', ''),
(7476, 33, 316, 4503, '
Belitang
II
', '
Kecamatan
', ''),
(7477, 33, 316, 4504, '
Belitang
III
', '
Kecamatan
', ''),
(7478, 33, 316, 4505, '
Belitang
Jaya
', '
Kecamatan
', ''),
(7479, 33, 316, 4506, '
Belitang
Madang
Raya
', '
Kecamatan
', ''),
(7480, 33, 316, 4507, '
Belitang
Mulya
', '
Kecamatan
', ''),
(7481, 33, 316, 4508, '
Buay
Madang
', '
Kecamatan
', ''),
(7482, 33, 316, 4509, '
Buay
Madang
Timur
', '
Kecamatan
', ''),
(7483, 33, 316, 4510, '
Buay
Pemuka
Bangsa
Raja
', '
Kecamatan
', ''),
(7484, 33, 316, 4511, '
Buay
Pemuka
Beliung
/
Peliung
', '
Kecamatan
', ''),
(7485, 33, 316, 4512, '
Bunga
Mayang
', '
Kecamatan
', ''),
(7486, 33, 316, 4513, '
Cempaka
', '
Kecamatan
', ''),
(7487, 33, 316, 4514, '
Jayapura
', '
Kecamatan
', ''),
(7488, 33, 316, 4515, '
Madang
Suku
I
', '
Kecamatan
', ''),
(7489, 33, 316, 4516, '
Madang
Suku
II
', '
Kecamatan
', ''),
(7490, 33, 316, 4517, '
Madang
Suku
III
', '
Kecamatan
', ''),
(7491, 33, 316, 4518, '
Martapura
', '
Kecamatan
', ''),
(7492, 33, 316, 4519, '
Semendawai
Barat
', '
Kecamatan
', ''),
(7493, 33, 316, 4520, '
Semendawai
Suku
III
', '
Kecamatan
', ''),
(7494, 33, 316, 4521, '
Semendawai
Timur
', '
Kecamatan
', ''),
(7495, 33, 242, 3419, '
Lubuk
Linggau
Barat
Dua
(
II
)
', '
Kecamatan
', ''),
(7496, 33, 242, 3420, '
Lubuk
Linggau
Barat
Satu
(
I
)
', '
Kecamatan
', ''),
(7497, 33, 242, 3421, '
Lubuk
Linggau
Selatan
Dua
(
II
)
', '
Kecamatan
', ''),
(7498, 33, 242, 3422, '
Lubuk
Linggau
Selatan
Satu
(
I
)
', '
Kecamatan
', ''),
(7499, 33, 242, 3423, '
Lubuk
Linggau
Timur
Dua
(
II
)
', '
Kecamatan
', ''),
(7500, 33, 242, 3424, '
Lubuk
Linggau
Timur
Satu
(
I
)
', '
Kecamatan
', ''),
(7501, 33, 242, 3425, '
Lubuk
Linggau
Utara
Dua
(
II
)
', '
Kecamatan
', ''),
(7502, 33, 242, 3426, '
Lubuk
Linggau
Utara
Satu
(
I
)
', '
Kecamatan
', ''),
(7503, 33, 324, 4591, '
Dempo
Selatan
', '
Kecamatan
', ''),
(7504, 33, 324, 4592, '
Dempo
Tengah
', '
Kecamatan
', ''),
(7505, 33, 324, 4593, '
Dempo
Utara
', '
Kecamatan
', ''),
(7506, 33, 324, 4594, '
Pagar
Alam
Selatan
', '
Kecamatan
', ''),
(7507, 33, 324, 4595, '
Pagar
Alam
Utara
', '
Kecamatan
', ''),
(7508, 33, 327, 4609, '
Alang
-
Alang
Lebar
', '
Kecamatan
', ''),
(7509, 33, 327, 4610, '
Bukit
Kecil
', '
Kecamatan
', ''),
(7510, 33, 327, 4611, '
Gandus
', '
Kecamatan
', ''),
(7511, 33, 327, 4612, '
Ilir
Barat
I
', '
Kecamatan
', ''),
(7512, 33, 327, 4613, '
Ilir
Barat
II
', '
Kecamatan
', ''),
(7513, 33, 327, 4614, '
Ilir
Timur
I
', '
Kecamatan
', ''),
(7514, 33, 327, 4615, '
Ilir
Timur
II
', '
Kecamatan
', ''),
(7515, 33, 327, 4616, '
Kalidoni
', '
Kecamatan
', ''),
(7516, 33, 327, 4617, '
Kemuning
', '
Kecamatan
', ''),
(7517, 33, 327, 4618, '
Kertapati
', '
Kecamatan
', ''),
(7518, 33, 327, 4619, '
Plaju
', '
Kecamatan
', ''),
(7519, 33, 327, 4620, '
Sako
', '
Kecamatan
', ''),
(7520, 33, 327, 4621, '
Seberang
Ulu
I
', '
Kecamatan
', ''),
(7521, 33, 327, 4622, '
Seberang
Ulu
II
', '
Kecamatan
', ''),
(7522, 33, 327, 4623, '
Sematang
Borang
', '
Kecamatan
', ''),
(7523, 33, 327, 4624, '
Sukarami
', '
Kecamatan
', ''),
(7524, 33, 367, 5126, '
Cambai
', '
Kecamatan
', ''),
(7525, 33, 367, 5127, '
Prabumulih
Barat
', '
Kecamatan
', ''),
(7526, 33, 367, 5128, '
Prabumulih
Selatan
', '
Kecamatan
', ''),
(7527, 33, 367, 5129, '
Prabumulih
Timur
', '
Kecamatan
', ''),
(7528, 33, 367, 5130, '
Prabumulih
Utara
', '
Kecamatan
', ''),
(7529, 33, 367, 5131, '
Rambang
Kapak
Tengah
', '
Kecamatan
', '');

--
-- Truncate table before insert `manufacturer`
--

TRUNCATE TABLE `manufacturer`;
--
-- Truncate table before insert `menu`
--

TRUNCATE TABLE `menu`;
--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `sub_id`, `title`, `slug`, `sort_order`, `type`, `page_id`, `icon`, `active`) VALUES
(1, 0, '
Home
', '', 1, 1, 0, '
fa
-
home
', 1),
(2, 0, '
Login
', '
customer
/
login
', 2, 2, 0, '
fa
-
sign
-
in
', 1),
(3, 0, '
Category
', '
#
', 3, 4, 0, '
fa
-
list
', 1),
(4, 0, '
Register
', '
customer
/
register
', 8, 3, 0, '
fa
-
edit
', 1);

--
-- Truncate table before insert `offers_category`
--

TRUNCATE TABLE `offers_category`;
--
-- Truncate table before insert `offers_discount`
--

TRUNCATE TABLE `offers_discount`;
--
-- Truncate table before insert `option`
--

TRUNCATE TABLE `option`;
--
-- Truncate table before insert `option_value`
--

TRUNCATE TABLE `option_value`;
--
-- Truncate table before insert `order`
--

TRUNCATE TABLE `order`;
--
-- Truncate table before insert `order_history`
--

TRUNCATE TABLE `order_history`;
--
-- Truncate table before insert `order_option`
--

TRUNCATE TABLE `order_option`;
--
-- Truncate table before insert `order_product`
--

TRUNCATE TABLE `order_product`;
--
-- Truncate table before insert `order_status`
--

TRUNCATE TABLE `order_status`;
--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`order_status_id`, `name`, `group`) VALUES
(1, '
Pending
', '
payment
'),
(2, '
Belum
Lunas
', '
payment
'),
(3, '
Lunas
', '
payment
'),
(4, '
Diproses
', '
payment
'),
(5, '
Batal
', '
payment
'),
(6, '
Dikirim
', '
payment
'),
(7, '
Selesai
', '
payment
');

--
-- Truncate table before insert `order_total`
--

TRUNCATE TABLE `order_total`;
--
-- Truncate table before insert `page`
--

TRUNCATE TABLE `page`;
--
-- Truncate table before insert `payment`
--

TRUNCATE TABLE `payment`;
--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`code`, `name`, `description`, `version`, `active`) VALUES
('
payment_transfer
', '
Payment
Transfer
', '
Metode
pembayaran
Transfer
', '
1.1
.0
', 1);

--
-- Truncate table before insert `product`
--

TRUNCATE TABLE `product`;
--
-- Truncate table before insert `product_attribute`
--

TRUNCATE TABLE `product_attribute`;
--
-- Truncate table before insert `product_category`
--

TRUNCATE TABLE `product_category`;
--
-- Truncate table before insert `product_discount`
--

TRUNCATE TABLE `product_discount`;
--
-- Truncate table before insert `product_filter`
--

TRUNCATE TABLE `product_filter`;
--
-- Truncate table before insert `product_image`
--

TRUNCATE TABLE `product_image`;
--
-- Truncate table before insert `product_option`
--

TRUNCATE TABLE `product_option`;
--
-- Truncate table before insert `product_option_value`
--

TRUNCATE TABLE `product_option_value`;
--
-- Truncate table before insert `product_related`
--

TRUNCATE TABLE `product_related`;
--
-- Truncate table before insert `product_tab`
--

TRUNCATE TABLE `product_tab`;
--
-- Truncate table before insert `review`
--

TRUNCATE TABLE `review`;
--
-- Truncate table before insert `search`
--

TRUNCATE TABLE `search`;
--
-- Truncate table before insert `session`
--

TRUNCATE TABLE `session`;
--
-- Dumping data for table `session`
--
--
-- Truncate table before insert `setting`
--

TRUNCATE TABLE `setting`;
--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`setting_id`, `group`, `key`, `value`, `serialized`) VALUES
(null, '
config
', '
footer_banner_id
', '
2
', 0),
(null, '
config
', '
footer_banner_id
', '
2
', 0),
(null, '
config
', '
middle_banner_id
', '
1
', 0),
(null, '
config
', '
top_banner_id
', '
1
', 0),
(null, '
config
', '
slideshow_banner_id
', '
3
', 0),
(null, '
config
', '
order_complete_status_id
', '
7
', 0),
(null, '
config
', '
order_delivered_status_id
', '
6
', 0),
(null, '
config
', '
order_process_status_id
', '
4
', 0),
(null, '
config
', '
order_paid_status_id
', '
3
', 0),
(null, '
config
', '
order_cancel_status_id
', '
5
', 0),
(null, '
config
', '
order_status_id
', '
1
', 0),
(null, '
config
', '
google_json
', '', 0),
(null, '
config
', '
facebook_app_id
', '', 0),
(null, '
config
', '
facebook_app_secret
', '', 0),
(null, '
config
', '
rajaongkir_courier
', '
a
:
4
:
{
i
:
0
;
s
:
3
:
"jne"
;
i
:
1
;
s
:
3
:
"pos"
;
i
:
2
;
s
:
4
:
"tiki"
;
i
:
3
;
s
:
3
:
"jnt"
;
}
', 1),
(null, '
config
', '
rajaongkir_weight_class_id
', '
3
', 0),
(null, '
payment_midtrans
', '
cancel_status_id
', '
5
', 0),
(null, '
payment_midtrans
', '
expired_status_id
', '
2
', 0),
(null, '
payment_midtrans
', '
deny_status_id
', '
5
', 0),
(null, '
payment_midtrans
', '
pending_status_id
', '
1
', 0),
(null, '
payment_midtrans
', '
challenge_status_id
', '
4
', 0),
(null, '
payment_midtrans
', '
success_status_id
', '
3
', 0),
(null, '
payment_midtrans
', '
sort_order
', '
3
', 0),
(null, '
payment_midtrans
', '
total
', '
0
', 0),
(null, '
payment_midtrans
', '
server_key
', '', 0),
(null, '
payment_midtrans
', '
client_key
', '', 0),
(null, '
payment_midtrans
', '
display_name
', '
Midtrans
', 0),
(null, '
payment_midtrans
', '
fee
', '
2.9
&
2200
', 0),
(null, '
payment_midtrans
', '
logo
', '', 0),
(null, '
config
', '
subdistrict_id
', '
0
', 0),
(null, '
config
', '
city_id
', '
0
', 0),
(null, '
config
', '
province_id
', '
0
', 0),
(null, '
config
', '
rajaongkir_api_key
', '', 0),
(null, '
config
', '
instagram_user
', '', 0),
(null, '
config
', '
gplus_user
', '', 0),
(null, '
config
', '
youtube_user
', '', 0),
(null, '
config
', '
site_name
', '', 0),
(null, '
config
', '
seo_title
', '', 0),
(null, '
config
', '
meta_keywords
', '', 0),
(null, '
config
', '
meta_description
', '', 0),
(null, '
config
', '
twitter_user
', '', 0),
(null, '
config
', '
facebook_user
', '', 0),
(null, '
config
', '
livechat_code
', '', 0),
(null, '
config
', '
sharing_button_code
', '
<
!-- Go to www.addthis.com/dashboard to customize your tools -->\r\n<div class="addthis_inline_share_toolbox"></div>\r\n\r\n<!-- Go to www.addthis.com/dashboard to customize your tools -->\r\n<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a1c43b417b4fe29"></script>\r\n\r\n\r\n', 0),
(
null
,
'config'
,
'length_class_id'
,
'1'
,
0
)
,
(
null
,
'config'
,
'weight_class_id'
,
'3'
,
0
)
,
(
null
,
'config'
,
'customer_group_id'
,
'3'
,
0
)
,
(
null
,
'config'
,
'smtp_pass'
,
''
,
0
)
,
(
null
,
'config'
,
'smtp_user'
,
''
,
0
)
,
(
null
,
'config'
,
'smtp_port'
,
''
,
0
)
,
(
null
,
'config'
,
'smtp_host'
,
''
,
0
)
,
(
null
,
'config'
,
'company'
,
''
,
0
)
,
(
null
,
'config'
,
'address'
,
''
,
0
)
,
(
null
,
'config'
,
'telephone'
,
''
,
0
)
,
(
null
,
'config'
,
'fax'
,
''
,
0
)
,
(
null
,
'config'
,
'email'
,
''
,
0
)
,
(
null
,
'config'
,
'logo'
,
'logo/logo.jpg'
,
0
)
,
(
null
,
'config'
,
'favicon'
,
'logo/letter-v-logo-27438483.jpg'
,
0
)
,
(
null
,
'config'
,
'smtp_email'
,
''
,
0
)
,
(
null
,
'config'
,
'system_installed'
,
'0'
,
0
)
,
(
null
,
'config'
,
'image_product_width'
,
'300'
,
0
)
,
(
null
,
'config'
,
'image_product_height'
,
'300'
,
0
)
,
(
null
,
'config'
,
'image_detail_product_width'
,
'700'
,
0
)
,
(
null
,
'config'
,
'image_detail_product_height'
,
'700'
,
0
)
,
(
null
,
'config'
,
'image_compare_product_width'
,
'80'
,
0
)
,
(
null
,
'config'
,
'image_compare_product_height'
,
'80'
,
0
)
,
(
null
,
'config'
,
'image_cart_product_width'
,
'100'
,
0
)
,
(
null
,
'config'
,
'image_cart_product_height'
,
'100'
,
0
)
,
(
null
,
'config'
,
'image_product_dod_width'
,
'250'
,
0
)
,
(
null
,
'config'
,
'image_product_dod_height'
,
'250'
,
0
)
,
(
NULL
,
'config'
,
'header_banner_id'
,
'4'
,
0
)
,
(
NULL
,
'config'
,
'delivery_courier'
,
'1'
,
0
)
,
(
NULL
,
'addon_freeshipping'
,
'free_shipping_logo'
,
'logo/free_shipping.jpg'
,
0
)
,
(
NULL
,
'addon_freeshipping'
,
'active'
,
'0'
,
0
)
,
(
NULL
,
'addon_freeshipping'
,
'free_shipping_nominal'
,
'30000'
,
0
)
,
(
null
,
'payment_payment_transfer'
,
'active'
,
'1'
,
0
)
;
--
-- Truncate table before insert `stock_status`
--

TRUNCATE
TABLE
`stock_status`
;
--
-- Dumping data for table `stock_status`
--

INSERT INTO `stock_status` (`stock_status_id`, `name`)
VALUES
  (1,
   'Tersedia'
)
,
(
2
,
'Habis'
)
;

--
-- Truncate table before insert `template`
--

TRUNCATE
TABLE
`template`
;
--
-- Dumping data for table `template`
--

INSERT INTO `template` (`template_id`, `code`, `name`, `location_desktop`, `location_mobile`, `image`, `note`,
                        `version`, `status`)
VALUES
  (1,
   'default',
   'Default',
   'template/default/default.zip',
   'template/default/default_mobile.zip',
   'template/default/default.png',
   'Template Default',
   '',
   1
)
;

TRUNCATE
TABLE
`templateconfig`
;

INSERT INTO `templateconfig` (`id`, `template_id`, `pwd`, `phg`, `pdwd`, `pdhg`, `pcwd`, `pchg`, `pcrwd`, `pcrhg`,
                              `powd`, `pohg`)
VALUES
  (1,
   1,
   300,
   300,
   700,
   700,
   80,
   80,
   100,
   100,
   250,
   250
)
;


TRUNCATE
TABLE
`version`
;
--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `version_no`, `version_date`, `version_code_hash`, `version_manual_book`)
VALUES
  (1,
   '1.0',
   '2018-05-11',
   '$2y$10$7B8SEbMl4EuI45Gi.1egJemhjqh7QZUkaPMRc6UK.7yniawS1Yb0.',
   'manual/Manual Book Ver 1.0.pdf'
)
;


--
-- Truncate table before insert `weight_class`
--

TRUNCATE
TABLE
`weight_class`
;
--
-- Dumping data for table `weight_class`
--

INSERT INTO `weight_class` (`weight_class_id`, `title`, `unit`, `value`)
VALUES
  (3,
   'Gram',
   'gr',
   '1.0000'
)
,
(
4
,
'Kilogram'
,
'kg'
,
'0.0000'
)
;