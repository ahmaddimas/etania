SET
SQL_MODE
=
"NO_AUTO_VALUE_ON_ZERO";
SET
time_zone
=
"+00:00";

--
-- Table structure for table `addon`
--

DROP TABLE IF EXISTS `addon`;
CREATE TABLE IF NOT EXISTS `addon`
(
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `description` text,
  `version` varchar
(
  32
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address`
(
  `address_id` int
(
  11
) NOT NULL,
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `address` varchar
(
  128
) NOT NULL DEFAULT '',
  `telephone` varchar
(
  32
) NOT NULL DEFAULT '',
  `postcode` varchar
(
  10
) NOT NULL DEFAULT '',
  `country_id` int
(
  11
) NOT NULL DEFAULT '0',
  `province_id` int
(
  11
) NOT NULL DEFAULT '0',
  `city_id` int
(
  11
) NOT NULL DEFAULT '0',
  `subdistrict_id` int
(
  11
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Tabel alamat';

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin`
(
  `admin_id` int
(
  11
) NOT NULL,
  `admin_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `image` text,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `email` varchar
(
  255
) NOT NULL DEFAULT '',
  `username` varchar
(
  32
) NOT NULL DEFAULT '',
  `password` varchar
(
  255
) NOT NULL DEFAULT '',
  `salt` varchar
(
  9
) NOT NULL DEFAULT '',
  `code_activation` varchar
(
  40
) NOT NULL DEFAULT '',
  `code_forgotten` varchar
(
  40
) NOT NULL DEFAULT '',
  `ip` varchar
(
  64
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_group`
--

DROP TABLE IF EXISTS `admin_group`;
CREATE TABLE IF NOT EXISTS `admin_group`
(
  `admin_group_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `permission` text
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel Grup Pengguna';

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

DROP TABLE IF EXISTS `attribute`;
CREATE TABLE IF NOT EXISTS `attribute`
(
  `attribute_id` int
(
  11
) NOT NULL,
  `attribute_group_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  64
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_group`
--

DROP TABLE IF EXISTS `attribute_group`;
CREATE TABLE IF NOT EXISTS `attribute_group`
(
  `attribute_group_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  64
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner`
(
  `banner_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `description` text,
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `banner_image`
--

DROP TABLE IF EXISTS `banner_image`;
CREATE TABLE IF NOT EXISTS `banner_image`
(
  `banner_image_id` int
(
  11
) NOT NULL,
  `banner_id` int
(
  11
) NOT NULL DEFAULT '0',
  `title` varchar
(
  64
) NOT NULL DEFAULT '',
  `subtitle` varchar
(
  255
) NOT NULL DEFAULT '',
  `link` varchar
(
  255
) NOT NULL DEFAULT '',
  `link_title` varchar
(
  255
) NOT NULL DEFAULT '',
  `image` varchar
(
  255
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category`
(
  `category_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `description` text,
  `meta_description` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_keyword` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_title` varchar
(
  255
) NOT NULL DEFAULT '',
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `image` varchar
(
  255
) NOT NULL DEFAULT '',
  `menu_image` varchar
(
  255
) NOT NULL DEFAULT '',
  `parent_id` int
(
  11
) NOT NULL DEFAULT '0',
  `top` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `column` int
(
  3
) NOT NULL DEFAULT '0',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `menu_active` int
(
  2
) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category_path`
--

DROP TABLE IF EXISTS `category_path`;
CREATE TABLE IF NOT EXISTS `category_path`
(
  `category_id` int
(
  11
) NOT NULL DEFAULT '0',
  `path_id` int
(
  11
) NOT NULL DEFAULT '0',
  `level` int
(
  11
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency`
(
  `currency_id` int
(
  11
) NOT NULL,
  `title` varchar
(
  32
) NOT NULL DEFAULT '',
  `code` varchar
(
  3
) NOT NULL DEFAULT '',
  `symbol` varchar
(
  12
) NOT NULL DEFAULT '',
  `value` float
(
  15,
  8
) NOT NULL DEFAULT '0.00000000',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer`
(
  `customer_id` int
(
  11
) NOT NULL,
  `customer_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `bank_code` varchar
(
  5
) NOT NULL DEFAULT '',
  `bank_name` varchar
(
  255
) NOT NULL DEFAULT '',
  `bank_account_name` varchar
(
  64
) NOT NULL DEFAULT '',
  `bank_account_number` varchar
(
  64
) NOT NULL DEFAULT '',
  `address_id` int
(
  11
) NOT NULL DEFAULT '0',
  `type` enum
(
  'b',
  's'
) NOT NULL DEFAULT 'b',
  `image` text,
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `email` varchar
(
  255
) NOT NULL DEFAULT '',
  `gender` enum
(
  'm',
  'f'
) NOT NULL DEFAULT 'm',
  `telephone` varchar
(
  32
) NOT NULL DEFAULT '',
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `username` varchar
(
  32
) NOT NULL DEFAULT '',
  `password` varchar
(
  255
) NOT NULL DEFAULT '',
  `salt` varchar
(
  9
) NOT NULL DEFAULT '',
  `code_activation` varchar
(
  40
) NOT NULL DEFAULT '',
  `code_forgotten` varchar
(
  40
) NOT NULL DEFAULT '',
  `ip` varchar
(
  64
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `verified` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `newsletter` tinyint
(
  1
) NOT NULL DEFAULT '1'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_group`
--

DROP TABLE IF EXISTS `customer_group`;
CREATE TABLE IF NOT EXISTS `customer_group`
(
  `customer_group_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  32
) NOT NULL DEFAULT '',
  `description` text,
  `approval` tinyint
(
  1
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_history`
--

DROP TABLE IF EXISTS `customer_history`;
CREATE TABLE IF NOT EXISTS `customer_history`
(
  `customer_history_id` int
(
  11
) NOT NULL,
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `comment` text,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_transaction`
--

DROP TABLE IF EXISTS `customer_transaction`;
CREATE TABLE IF NOT EXISTS `customer_transaction`
(
  `customer_transaction_id` int
(
  11
) NOT NULL,
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `reference` varchar
(
  32
) NOT NULL DEFAULT '',
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `description` text,
  `amount` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_paid` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_method` varchar
(
  32
) NOT NULL DEFAULT '',
  `payment_code` varchar
(
  32
) NOT NULL DEFAULT '',
  `approved` tinyint
(
  1
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_category`
--

DROP TABLE IF EXISTS `custom_category`;
CREATE TABLE IF NOT EXISTS `custom_category`
(
  `custom_category_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0',
  `required` tinyint
(
  1
) NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_part`
--

DROP TABLE IF EXISTS `custom_part`;
CREATE TABLE IF NOT EXISTS `custom_part`
(
  `custom_part_id` int
(
  11
) NOT NULL,
  `custom_category_id` int
(
  11
) NOT NULL DEFAULT '0',
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_part_pair`
--

DROP TABLE IF EXISTS `custom_part_pair`;
CREATE TABLE IF NOT EXISTS `custom_part_pair`
(
  `product_id` int
(
  11
) NOT NULL,
  `pair_id` int
(
  11
) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

DROP TABLE IF EXISTS `filter`;
CREATE TABLE IF NOT EXISTS `filter`
(
  `filter_id` int
(
  11
) NOT NULL,
  `filter_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `name` varchar
(
  64
) NOT NULL DEFAULT ''
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filter_group`
--

DROP TABLE IF EXISTS `filter_group`;
CREATE TABLE IF NOT EXISTS `filter_group`
(
  `filter_group_id` int
(
  11
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `name` varchar
(
  64
) NOT NULL DEFAULT ''
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `length_class`
--

DROP TABLE IF EXISTS `length_class`;
CREATE TABLE IF NOT EXISTS `length_class`
(
  `length_class_id` int
(
  11
) NOT NULL,
  `title` varchar
(
  32
) NOT NULL DEFAULT '',
  `unit` varchar
(
  4
) NOT NULL DEFAULT '',
  `value` decimal
(
  15,
  4
) NOT NULL DEFAULT '0.0000'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
CREATE TABLE IF NOT EXISTS `location`
(
  `location_id` int
(
  11
) NOT NULL,
  `province_id` int
(
  11
) NOT NULL DEFAULT '0',
  `city_id` int
(
  11
) NOT NULL DEFAULT '0',
  `subdistrict_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  128
) NOT NULL DEFAULT '',
  `type` varchar
(
  32
) NOT NULL DEFAULT '',
  `postcode` varchar
(
  8
) NOT NULL DEFAULT '',
  `delivery_cost` VARCHAR
(
  100
) NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
CREATE TABLE IF NOT EXISTS `manufacturer`
(
  `manufacturer_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  64
) NOT NULL,
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `image` varchar
(
  255
) DEFAULT NULL,
  `sort_order` int
(
  3
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu`
(
  `menu_id` int
(
  11
) NOT NULL,
  `sub_id` int
(
  11
) NOT NULL,
  `title` varchar
(
  255
) CHARACTER SET utf8 NOT NULL,
  `slug` varchar
(
  255
) CHARACTER SET utf8 NOT NULL,
  `sort_order` int
(
  11
) NOT NULL,
  `type` int
(
  2
) NOT NULL COMMENT '0 1 2 3 4',
  `page_id` int
(
  11
) NOT NULL DEFAULT '0',
  `icon` varchar
(
  255
) NOT NULL DEFAULT 'fa-arrow-right',
  `active` int
(
  2
) NOT NULL DEFAULT '1'
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `offers_category`
--

DROP TABLE IF EXISTS `offers_category`;
CREATE TABLE IF NOT EXISTS `offers_category`
(
  `offers_category_id` int
(
  11
) NOT NULL,
  `category_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `offers_discount`
--

DROP TABLE IF EXISTS `offers_discount`;
CREATE TABLE IF NOT EXISTS `offers_discount`
(
  `offers_discount_id` int
(
  11
) NOT NULL,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `priority` int
(
  5
) NOT NULL DEFAULT '1',
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `date_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

DROP TABLE IF EXISTS `option`;
CREATE TABLE IF NOT EXISTS `option`
(
  `option_id` int
(
  11
) NOT NULL,
  `type` varchar
(
  32
) NOT NULL,
  `name` varchar
(
  128
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `option_value`
--

DROP TABLE IF EXISTS `option_value`;
CREATE TABLE IF NOT EXISTS `option_value`
(
  `option_value_id` int
(
  11
) NOT NULL,
  `option_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  128
) NOT NULL,
  `image` varchar
(
  255
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order`
(
  `order_id` int
(
  11
) NOT NULL,
  `invoice_prefix` varchar
(
  32
) NOT NULL DEFAULT '',
  `invoice_no` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `email` varchar
(
  255
) NOT NULL DEFAULT '',
  `telephone` varchar
(
  32
) NOT NULL DEFAULT '',
  `address` varchar
(
  128
) NOT NULL DEFAULT '',
  `postcode` varchar
(
  10
) NOT NULL DEFAULT '',
  `province_id` int
(
  11
) NOT NULL DEFAULT '0',
  `province` varchar
(
  128
) NOT NULL DEFAULT '',
  `city_id` int
(
  11
) NOT NULL DEFAULT '0',
  `city` varchar
(
  128
) NOT NULL DEFAULT '',
  `subdistrict_id` int
(
  11
) NOT NULL DEFAULT '0',
  `subdistrict` varchar
(
  128
) NOT NULL DEFAULT '',
  `user_agent` varchar
(
  255
) NOT NULL DEFAULT '',
  `payment_method` varchar
(
  128
) NOT NULL DEFAULT '',
  `payment_code` varchar
(
  128
) NOT NULL DEFAULT '',
  `shipping_method` varchar
(
  128
) NOT NULL DEFAULT '',
  `shipping_code` varchar
(
  128
) NOT NULL DEFAULT '',
  `waybill` varchar
(
  64
) NOT NULL DEFAULT '',
  `currency_id` int
(
  11
) NOT NULL DEFAULT '0',
  `currency_value` decimal
(
  15,
  8
) NOT NULL DEFAULT '0.00000000',
  `comment` text,
  `total` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `order_status_id` int
(
  11
) NOT NULL DEFAULT '0',
  `ip` varchar
(
  40
) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_paid` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

DROP TABLE IF EXISTS `order_history`;
CREATE TABLE IF NOT EXISTS `order_history`
(
  `order_history_id` int
(
  11
) NOT NULL,
  `order_status_id` int
(
  5
) NOT NULL DEFAULT '0',
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `admin_id` int
(
  11
) NOT NULL DEFAULT '0',
  `notify` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `comment` text,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_option`
--

DROP TABLE IF EXISTS `order_option`;
CREATE TABLE IF NOT EXISTS `order_option`
(
  `order_option_id` int
(
  11
) NOT NULL,
  `order_id` int
(
  11
) NOT NULL,
  `order_product_id` int
(
  11
) NOT NULL,
  `product_option_id` int
(
  11
) NOT NULL,
  `product_option_value_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  255
) NOT NULL,
  `value` text NOT NULL,
  `type` varchar
(
  32
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
CREATE TABLE IF NOT EXISTS `order_product`
(
  `order_product_id` int
(
  11
) NOT NULL,
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `sku` varchar
(
  64
) NOT NULL DEFAULT '',
  `quantity` int
(
  4
) NOT NULL DEFAULT '0',
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `total` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
CREATE TABLE IF NOT EXISTS `order_status`
(
  `order_status_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  32
) NOT NULL DEFAULT '',
  `group` varchar
(
  8
) NOT NULL DEFAULT 'payment'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_total`
--

DROP TABLE IF EXISTS `order_total`;
CREATE TABLE IF NOT EXISTS `order_total`
(
  `order_total_id` int
(
  10
) NOT NULL,
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `title` varchar
(
  255
) NOT NULL DEFAULT '',
  `text` varchar
(
  255
) NOT NULL DEFAULT '',
  `value` decimal
(
  15,
  4
) NOT NULL DEFAULT '0.0000',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page`
(
  `page_id` int
(
  11
) NOT NULL,
  `title` varchar
(
  64
) NOT NULL DEFAULT '',
  `content` text,
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_description` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_keyword` varchar
(
  255
) NOT NULL DEFAULT '',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `new_window` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `footer_column` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '1'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment`
(
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `description` text,
  `version` varchar
(
  32
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------


DROP TABLE IF EXISTS `payment_transfer`;
CREATE TABLE IF NOT EXISTS `payment_transfer`
(
  `payment_transfer_id` INT
(
  11
) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `logo` VARCHAR
(
  255
) NOT NULL ,
  `display_name` VARCHAR
(
  255
) NOT NULL ,
  `name` VARCHAR
(
  255
) NOT NULL ,
  `bank_name` VARCHAR
(
  255
) NOT NULL ,
  `rekening_name` VARCHAR
(
  255
) NOT NULL ,
  `rekening_number` VARCHAR
(
  255
) NOT NULL ,
  `instruction` TEXT NOT NULL ,
  `success_status_id` INT
(
  11
) NOT NULL ,
  `total` BIGINT
(
  50
) NOT NULL ,
  `sort_order` INT
(
  11
) NOT NULL DEFAULT '0' ,
  `active` INT
(
  11
) NOT NULL DEFAULT '1'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product`
(
  `product_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `model` varchar
(
  128
) NOT NULL DEFAULT '',
  `sku` varchar
(
  64
) NOT NULL DEFAULT '',
  `description` text,
  `meta_description` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_keyword` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_title` varchar
(
  255
) NOT NULL DEFAULT '',
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `tag` text,
  `image` varchar
(
  255
) DEFAULT NULL,
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `category_id` int
(
  11
) NOT NULL DEFAULT '0',
  `manufacturer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `points` int
(
  11
) NOT NULL DEFAULT '0',
  `weight` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `weight_class_id` int
(
  11
) NOT NULL DEFAULT '0',
  `length` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `width` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `height` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `length_class_id` int
(
  11
) NOT NULL DEFAULT '0',
  `quantity` int
(
  11
) NOT NULL DEFAULT '0',
  `unit_class_id` int
(
  11
) NOT NULL DEFAULT '0',
  `stock_status_id` int
(
  11
) NOT NULL DEFAULT '0',
  `minimum` int
(
  11
) NOT NULL DEFAULT '1',
  `shipping` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int
(
  5
) NOT NULL DEFAULT '0',
  `tab_title` VARCHAR
(
  255
) NOT NULL DEFAULT 'Spesification',
  `type` INT
(
  2
) NOT NULL DEFAULT '0' COMMENT '0 item, 1 digital, 2 other'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute`
--

DROP TABLE IF EXISTS `product_attribute`;
CREATE TABLE IF NOT EXISTS `product_attribute`
(
  `product_id` int
(
  11
) NOT NULL,
  `attribute_id` int
(
  11
) NOT NULL,
  `text` text NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
CREATE TABLE IF NOT EXISTS `product_category`
(
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `category_id` int
(
  11
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_discount`
--

DROP TABLE IF EXISTS `product_discount`;
CREATE TABLE IF NOT EXISTS `product_discount`
(
  `product_discount_id` int
(
  11
) NOT NULL,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `quantity` int
(
  4
) NOT NULL DEFAULT '0',
  `priority` int
(
  5
) NOT NULL DEFAULT '1',
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_filter`
--

DROP TABLE IF EXISTS `product_filter`;
CREATE TABLE IF NOT EXISTS `product_filter`
(
  `product_id` int
(
  11
) NOT NULL,
  `filter_id` int
(
  11
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

DROP TABLE IF EXISTS `product_image`;
CREATE TABLE IF NOT EXISTS `product_image`
(
  `product_image_id` int
(
  11
) NOT NULL,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `image` varchar
(
  255
) NOT NULL DEFAULT '',
  `alt` varchar
(
  64
) NOT NULL DEFAULT '',
  `title` varchar
(
  64
) NOT NULL DEFAULT '',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_option`
--

DROP TABLE IF EXISTS `product_option`;
CREATE TABLE IF NOT EXISTS `product_option`
(
  `product_option_id` int
(
  11
) NOT NULL,
  `product_id` int
(
  11
) NOT NULL,
  `option_id` int
(
  11
) NOT NULL,
  `option_value` text NOT NULL,
  `required` tinyint
(
  1
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_option_value`
--

DROP TABLE IF EXISTS `product_option_value`;
CREATE TABLE IF NOT EXISTS `product_option_value`
(
  `product_option_value_id` int
(
  11
) NOT NULL,
  `product_option_id` int
(
  11
) NOT NULL,
  `product_id` int
(
  11
) NOT NULL,
  `option_id` int
(
  11
) NOT NULL,
  `option_value_id` int
(
  11
) NOT NULL,
  `quantity` int
(
  3
) NOT NULL,
  `subtract` tinyint
(
  1
) NOT NULL,
  `price` decimal
(
  15,
  2
) NOT NULL,
  `points` int
(
  8
) NOT NULL,
  `weight` decimal
(
  15,
  2
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_related`
--

DROP TABLE IF EXISTS `product_related`;
CREATE TABLE IF NOT EXISTS `product_related`
(
  `product_id` int
(
  11
) NOT NULL,
  `related_id` int
(
  11
) NOT NULL,
  `type` varchar
(
  9
) NOT NULL DEFAULT ''
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_tab`
--

DROP TABLE IF EXISTS `product_tab`;
CREATE TABLE IF NOT EXISTS `product_tab`
(
  `product_tab_id` int
(
  11
) NOT NULL,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `title` varchar
(
  128
) NOT NULL DEFAULT '',
  `content` text,
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
CREATE TABLE IF NOT EXISTS `review`
(
  `review_id` int
(
  11
) NOT NULL,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `author` varchar
(
  128
) NOT NULL DEFAULT '',
  `text` text,
  `rating` int
(
  1
) NOT NULL DEFAULT '0',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

DROP TABLE IF EXISTS `search`;
CREATE TABLE IF NOT EXISTS `search`
(
  `search_id` int
(
  11
) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `search` varchar
(
  255
) COLLATE utf8_bin NOT NULL DEFAULT '',
  `phase` int
(
  1
) NOT NULL DEFAULT '0',
  `results` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `ip` varchar
(
  15
) COLLATE utf8_bin NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE =utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE IF NOT EXISTS `session`
(
  `id` varchar
(
  128
) NOT NULL,
  `ip_address` varchar
(
  45
) NOT NULL,
  `timestamp` int
(
  10
) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting`
(
  `setting_id` int
(
  11
) NOT NULL,
  `group` varchar
(
  32
) NOT NULL,
  `key` varchar
(
  64
) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint
(
  1
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stock_status`
--

DROP TABLE IF EXISTS `stock_status`;
CREATE TABLE IF NOT EXISTS `stock_status`
(
  `stock_status_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  32
) NOT NULL DEFAULT ''
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
CREATE TABLE IF NOT EXISTS `template`
(
  `template_id` int
(
  11
) NOT NULL,
  `code` varchar
(
  50
) NOT NULL,
  `name` varchar
(
  100
) NOT NULL,
  `location_desktop` varchar
(
  255
) NOT NULL,
  `location_mobile` varchar
(
  255
) NOT NULL,
  `image` varchar
(
  255
) NOT NULL,
  `note` text NOT NULL,
  `version` varchar
(
  25
) NOT NULL,
  `status` int
(
  2
) NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `templateconfig`
--

DROP TABLE IF EXISTS `templateconfig`;
CREATE TABLE IF NOT EXISTS `templateconfig`
(
  `id` int
(
  11
) NOT NULL,
  `template_id` int
(
  11
) NOT NULL,
  `pwd` int
(
  11
) NOT NULL,
  `phg` int
(
  11
) NOT NULL,
  `pdwd` int
(
  11
) NOT NULL,
  `pdhg` int
(
  11
) NOT NULL,
  `pcwd` int
(
  11
) NOT NULL,
  `pchg` int
(
  11
) NOT NULL,
  `pcrwd` int
(
  11
) NOT NULL,
  `pcrhg` int
(
  11
) NOT NULL,
  `powd` int
(
  11
) NOT NULL,
  `pohg` int
(
  11
) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------


DROP TABLE IF EXISTS `version`;
CREATE TABLE IF NOT EXISTS `version`
(
  `version_id` int
(
  11
) NOT NULL,
  `version_no` varchar
(
  10
) NOT NULL,
  `version_date` date NOT NULL,
  `version_code_hash` varchar
(
  255
) NOT NULL,
  `version_manual_book` varchar
(
  255
) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `weight_class`
--

DROP TABLE IF EXISTS `weight_class`;
CREATE TABLE IF NOT EXISTS `weight_class`
(
  `weight_class_id` int
(
  11
) NOT NULL,
  `title` varchar
(
  32
) NOT NULL DEFAULT '',
  `unit` varchar
(
  4
) NOT NULL DEFAULT '',
  `value` decimal
(
  15,
  4
) NOT NULL DEFAULT '0.0000'
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;


ALTER TABLE `addon`
  ADD PRIMARY KEY (`code`);

ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `email` (`email`,`username`,`active`);

ALTER TABLE `admin_group`
  ADD PRIMARY KEY (`admin_group_id`);

ALTER TABLE `attribute`
  ADD PRIMARY KEY (`attribute_id`);

ALTER TABLE `attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);


ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);


ALTER TABLE `banner_image`
  ADD PRIMARY KEY (`banner_image_id`);


ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);


ALTER TABLE `category_path`
  ADD PRIMARY KEY (`category_id`, `path_id`);


ALTER TABLE `currency`
  ADD PRIMARY KEY (`currency_id`);


ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD KEY `email` (`email`,`username`,`active`);


ALTER TABLE `customer_group`
  ADD PRIMARY KEY (`customer_group_id`);


ALTER TABLE `customer_history`
  ADD PRIMARY KEY (`customer_history_id`);


ALTER TABLE `customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);


ALTER TABLE `custom_category`
  ADD PRIMARY KEY (`custom_category_id`);


ALTER TABLE `custom_part`
  ADD PRIMARY KEY (`custom_part_id`);


ALTER TABLE `custom_part_pair`
  ADD PRIMARY KEY (`product_id`, `pair_id`);


ALTER TABLE `filter`
  ADD PRIMARY KEY (`filter_id`);


ALTER TABLE `filter_group`
  ADD PRIMARY KEY (`filter_group_id`);


ALTER TABLE `length_class`
  ADD PRIMARY KEY (`length_class_id`);


ALTER TABLE `location`
  ADD PRIMARY KEY (`location_id`);


ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);


ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);


ALTER TABLE `offers_category`
  ADD PRIMARY KEY (`offers_category_id`);


ALTER TABLE `offers_discount`
  ADD PRIMARY KEY (`offers_discount_id`),
  ADD KEY `product_id` (`product_id`);


ALTER TABLE `option`
  ADD PRIMARY KEY (`option_id`);


ALTER TABLE `option_value`
  ADD PRIMARY KEY (`option_value_id`);


ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);


ALTER TABLE `order_history`
  ADD PRIMARY KEY (`order_history_id`);


ALTER TABLE `order_option`
  ADD PRIMARY KEY (`order_option_id`);


ALTER TABLE `order_product`
  ADD PRIMARY KEY (`order_product_id`);


ALTER TABLE `order_status`
  ADD PRIMARY KEY (`order_status_id`);


ALTER TABLE `order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `idx_orders_total_orders_id` (`order_id`);


ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);


ALTER TABLE `payment`
  ADD PRIMARY KEY (`code`);


ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);


ALTER TABLE `product_attribute`
  ADD PRIMARY KEY (`product_id`, `attribute_id`);


ALTER TABLE `product_category`
  ADD PRIMARY KEY (`product_id`, `category_id`);


ALTER TABLE `product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);


ALTER TABLE `product_filter`
  ADD PRIMARY KEY (`product_id`, `filter_id`);


ALTER TABLE `product_image`
  ADD PRIMARY KEY (`product_image_id`);


ALTER TABLE `product_option`
  ADD PRIMARY KEY (`product_option_id`);


ALTER TABLE `product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);


ALTER TABLE `product_related`
  ADD PRIMARY KEY (`product_id`, `related_id`);


ALTER TABLE `product_tab`
  ADD PRIMARY KEY (`product_tab_id`);


ALTER TABLE `review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);


ALTER TABLE `search`
  ADD PRIMARY KEY (`search_id`);


ALTER TABLE `session`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);


ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`),
  ADD KEY `group` (`group`),
  ADD KEY `key` (`key`);


ALTER TABLE `stock_status`
  ADD PRIMARY KEY (`stock_status_id`);


ALTER TABLE `template`
  ADD PRIMARY KEY (`template_id`);


ALTER TABLE `templateconfig`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

ALTER TABLE `weight_class`
  ADD PRIMARY KEY (`weight_class_id`);



ALTER TABLE `address`
  MODIFY `address_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `admin`
  MODIFY `admin_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `admin_group`
  MODIFY `admin_group_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `attribute`
  MODIFY `attribute_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `attribute_group`
  MODIFY `attribute_group_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `banner`
  MODIFY `banner_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `banner_image`
  MODIFY `banner_image_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `category`
  MODIFY `category_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `currency`
  MODIFY `currency_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `customer`
  MODIFY `customer_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `customer_group`
  MODIFY `customer_group_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `customer_history`
  MODIFY `customer_history_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `customer_transaction`
  MODIFY `customer_transaction_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `custom_category`
  MODIFY `custom_category_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `custom_part`
  MODIFY `custom_part_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `filter`
  MODIFY `filter_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `filter_group`
  MODIFY `filter_group_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `length_class`
  MODIFY `length_class_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `location`
  MODIFY `location_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `manufacturer`
  MODIFY `manufacturer_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `menu`
  MODIFY `menu_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `offers_category`
  MODIFY `offers_category_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `offers_discount`
  MODIFY `offers_discount_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `option`
  MODIFY `option_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `option_value`
  MODIFY `option_value_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `order`
  MODIFY `order_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_history`
  MODIFY `order_history_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_option`
  MODIFY `order_option_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_product`
  MODIFY `order_product_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_status`
  MODIFY `order_status_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_total`
  MODIFY `order_total_id` int (10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `page`
  MODIFY `page_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `product`
  MODIFY `product_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_discount`
  MODIFY `product_discount_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_image`
  MODIFY `product_image_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_option`
  MODIFY `product_option_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_option_value`
  MODIFY `product_option_value_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_tab`
  MODIFY `product_tab_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `review`
  MODIFY `review_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `search`
  MODIFY `search_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `setting`
  MODIFY `setting_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `stock_status`
  MODIFY `stock_status_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `template`
  MODIFY `template_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `templateconfig`
  MODIFY `id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `version`
  MODIFY `version_id` int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `weight_class`
  MODIFY `weight_class_id` int (11) NOT NULL AUTO_INCREMENT;




