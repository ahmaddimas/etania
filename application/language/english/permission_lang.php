<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['address'] = 'Pengguna &raquo; Alamat';
$lang['admin_group'] = 'Administrator &raquo; Hak Akses';
$lang['admins'] = 'Administrator &raquo; Admin';
$lang['bank'] = 'Pengaturan &raquo; Bank';
$lang['bank_transaction'] = 'Transaksi Bank';
$lang['banner'] = 'Marketing &raquo; Banner';
$lang['blog_article'] = 'Blog &raquo; Artikel';
$lang['blog_category'] = 'Blog &raquo; Kategori Artikel';
$lang['page'] = 'Blog &raquo; Halaman';
$lang['product'] = 'Katalog &raquo; Produk';
$lang['category'] = 'Katalog &raquo; Kategori Produk';
$lang['filter'] = 'Katalog &raquo; Filter Produk';
$lang['variant'] = 'Katalog &raquo; Varian Produk';
$lang['coupon'] = 'Marketing &raquo; Kupon';
$lang['order'] = 'Pesanan &raquo; Pesanan';
$lang['order_status'] = 'Pengaturan &raquo; Status Order';
$lang['setting_payment'] = 'Pengaturan &raquo; Status Order';
$lang['review'] = 'Katalog &raquo; Ulasan';
$lang['settings'] = 'Pengaturan &raquo; Umum';
$lang['stock_status'] = 'Pengaturan &raquo; Status Stok';
$lang['users'] = 'Pengguna &raquo Pelanggan';
$lang['weight_class'] = 'Pengaturan &raquo Satuan Berat';