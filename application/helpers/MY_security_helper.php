<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('hash_string')) {
    /*
     * Generate hashed string for password, etc
     *
     * @access public
     * @param string $str
     * @param string $hash
     * @return string
     */
    function hash_string($string = null, $hash = null)
    {
        if (!$hash) $hash = substr(md5(uniqid(rand(), true)), 0, 9);

        return $string ? do_hash($hash . do_hash($hash . do_hash($string))) : $hash;
    }
}