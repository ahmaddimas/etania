<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $heading_title ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="customer_group_id" value="<?= $customer_group_id ?>">
            <div class="form-group">
                <label class="control-label col-sm-3">Nama Grup</label>
                <div class=" col-sm-9">
                    <input type="text" class="form-control" name="name" value="<?= $name ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Auto Approval</label>
                <div class="toggle lg col-sm-4">
                    <label style="margin-top:5px;">
                        <?php if ($approval) { ?>
                            <input type="checkbox" name="approval" value="1" checked="checked">
                        <?php } else { ?>
                            <input type="checkbox" name="approval" value="1">
                        <?php } ?>
                        <span class="button-indecator"></span>
                    </label>
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
            <button type="button" class="btn btn-success" id="submit"><i class="fa fa-check"></i> Simpan</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('a').tooltip();
        var checks = ['index', 'create', 'edit', 'delete'];
        for (i in checks) {
            toggleCheck(checks[i]);
        }
    });

    function toggleCheck(x) {
        $('#select-all-' + x).click(function () {
            var checkBoxes = $('.check-' + x);
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });
    }

    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });
</script>