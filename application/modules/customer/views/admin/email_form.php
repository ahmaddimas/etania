<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('page') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>
        <a id="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Kirim</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?= form_open($action, 'id="form" class="form-horizontal" role="form"') ?>
                <div id="message"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kepada</label>
                    <div class="col-sm-6">
                        <select name="to" class="form-control">
                            <option value="newsletter">Subcriber Newsletter</option>
                            <option value="customer_all">Semua Pelanggan</option>
                            <option value="customer_group">Grup Pelanggan</option>
                            <option value="customer">Pelanggan</option>
                        </select>
                    </div>
                </div>
                <div id="customer">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pelanggan</label>
                        <div class="col-sm-10">
                            <input type="text" name="customer" value="" class="form-control" autocomplete="off"
                                   placeholder="Ketikkan nama pelanggan...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <div class="controls"
                                 style="border: 1px solid #ddd; height: 200px; background: #fff; overflow-y: scroll;">
                                <table class="table table-striped" width="100%">
                                    <tbody id="customer-list"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="customer_group">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Grup Pelanggan</label>
                        <div class="col-sm-6">
                            <select name="customer_group_id" class="form-control">
                                <?php foreach ($customer_groups as $customer_group) { ?>
                                    <option value="<?= $customer_group['customer_group_id'] ?>"><?= $customer_group['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Judul</label>
                    <div class="col-sm-10">
                        <input type="text" name="subject" value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Konten</label>
                    <div class="col-sm-10">
                        <textarea name="body" id="body" class="form-control summernote"></textarea>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('select[name=\'to\']').on('change', function () {
        $('#customer, #customer_group').hide();
        $('#' + $(this).val()).show();
    });

    $('#submit').bind('click', function () {
        $btn = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $btn.button('loading');
            },
            complete: function () {
                $btn.button('reset');
            },
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\'], select[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\'], select[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').parent().addClass('has-error');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + json['success'] + '</div>');
                }
            }
        });
    });

    $('input[name=\'customer\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('customer/autocomplete')?>",
                data: 'name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    customers = [];
                    map = {};
                    $.each(json, function (i, customer) {
                        map[customer.name] = customer;
                        customers.push(customer.name);
                    });
                    process(customers);
                }
            });
        },
        updater: function (item) {
            $('#customer-list' + map[item].customer_id).remove();
            $('#customer-list').append('<tr id="customer-list' + map[item].customer_id + '"><td>' + item + '<input type="hidden" name="customer[]" value="' + map[item].customer_id + '" /></td><td class="text-right"><a class="btn btn-danger btn-xs btn-flat" onclick="$(\'#customer-list' + map[item].customer_id + '\').remove();"><i class="fa fa-remove "></i></a></td></tr>');
        },
        minLength: 1
    });

    $('select[name=\'to\']').trigger('change');
</script>