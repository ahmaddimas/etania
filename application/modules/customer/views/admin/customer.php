<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('customer/create') ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i>
            Tambah</a>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</a>
        <a href="<?= current_url() ?>#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><i
                    class="fa fa-check-square-o"></i> <?= lang('button_action') ?> <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="<?= admin_url('customer/export') ?>" target="_blank">Cetak</a></li>
            <li><a href="<?= admin_url('customer/export?format=pdf_download') ?>">Export PDF</a></li>
            <li><a href="<?= admin_url('customer/export?format=excel_download') ?>">Export XLS</a></li>
            <hr>
            <li><a href="<?= current_url() ?>#" onclick="bulk.active();"><?= lang('button_enable') ?></a></li>
            <li><a href="<?= current_url() ?>#" onclick="bulk.inactive();"><?= lang('button_disable') ?></a></li>
            <li><a href="<?= current_url() ?>#" onclick="bulk.delete();"><?= lang('button_delete') ?></a></li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?= form_open(admin_url('customer/delete'), 'id="bulk-action"') ?>
                <table class="table table-hover table-bordered" width="100%" id="datatable">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="select-all"></th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>No. Handphone</th>
                        <th>Tanggal Daftar</th>
                        <th>Grup</th>
                        <th>Status</th>
                        <th class="text-right"></th>
                    </tr>
                    </thead>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="modal"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('customer')?>",
                "type": "POST",
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "customer_id"},
                {"data": "name"},
                {"data": "email"},
                {"data": "telephone"},
                {"data": "date_added"},
                {"data": "group"},
                {"data": "active"},
                {"orderable": false, "searchable": false, "data": "customer_id"},
            ],
            "sDom": 'lfr<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<input type="checkbox" class="checkbox" name="customer_id[]" value="' + data.customer_id + '"/>');

                if (data.active === '1') {
                    $('td', row).eq(6).html('<span class="label label-success">Active</span>');
                } else {
                    $('td', row).eq(6).html('<span class="label label-default">Inactive</span>');
                }

                $('td', row).eq(7).html('<a href="<?=admin_url('customer/edit/\'+data.customer_id+\'')?>"><i class="fa fa-cog"></i> <?=lang('button_edit')?></a>').addClass('text-right');
            },
            "order": [[7, 'asc']]
        });
    });

    var bulk = {
        'active': function () {
            action({
                text: "<?=lang('text_confirm_enable')?>",
                action: 'active',
            });
        },
        'inactive': function () {
            action({
                text: "<?=lang('text_confirm_disable')?>",
                action: 'inactive',
            });
        },
        'delete': function () {
            action({
                text: "<?=lang('text_confirm_delete')?>",
                action: 'delete',
            });
        },
    };

    function action(param) {
        swal({
            title: '<?=lang('text_warning')?>',
            text: param.text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('customer/\'+param.action+\'')?>',
                    type: 'post',
                    data: $('#bulk-action').serialize(),
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
        $('#select-all').prop('checked', false);
    }

    function getForm(customer_id, path) {
        $.ajax({
            url: $('base').attr('href') + path,
            data: 'customer_id=' + customer_id,
            dataType: 'json',
            success: function (json) {
                if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
                } else if (json['content']) {
                    $('#modal').html('<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['content'] + '</div>');
                    $('#form-modal').modal('show');
                    $('#form-modal').on('hidden.bs.modal', function (e) {
                        refreshTable();
                    });
                }
            }
        });
    }
</script>