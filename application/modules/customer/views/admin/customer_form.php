<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a onclick="window.location = '<?= admin_url('customer') ?>'" class="btn btn-default"><i
                    class="fa fa-reply"></i> Kembali</a>
        <a id="submit" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Simpan</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?= form_open($action, 'id="form" class="form-horizontal"') ?>
                <input type="hidden" name="customer_id" value="<?= $customer_id ?>">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab" id="default">Biodata</a></li>
                    <?php if ($customer_id) { ?>
                        <li><a href="#tab-2" data-toggle="tab">Alamat</a></li>
                    <?php } ?>
                    <li><a href="#tab-3" data-toggle="tab">Rekening Bank</a></li>
                    <li><a href="#tab-4" data-toggle="tab">Password</a></li>
                </ul>
                <div class="tab-content" style="padding-top:20px;">
                    <div class="tab-pane active" id="tab-1">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Nama Lengkap</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" value="<?= $name ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="email" value="<?= $email ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">No. Telepon</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="telephone" value="<?= $telephone ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Foto Profil</label>
                            <div class="col-md-3">
                                <img id="image" src="<?= $image ?>" class="img-thumbnail">
                                <div class="caption" style="margin-top:10px;">
                                    <button type="button" class="btn btn-default" id="upload">Ubah</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Grup Pelanggan</label>
                            <div class="col-sm-4">
                                <select name="customer_group_id" class="form-control">
                                    <?php foreach ($customer_groups as $customer_group) { ?>
                                        <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
                                            <option value="<?= $customer_group['customer_group_id'] ?>"
                                                    selected="selected"><?= $customer_group['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $customer_group['customer_group_id'] ?>"><?= $customer_group['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Berlangganan Newsletter</label>
                            <div class="toggle lg col-sm-4">
                                <label style="margin-top:5px;">
                                    <?php if ($newsletter) { ?>
                                        <input type="checkbox" name="newsletter" value="1" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="newsletter" value="1">
                                    <?php } ?>
                                    <span class="button-indecator"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Aktif</label>
                            <div class="toggle lg col-sm-4">
                                <label style="margin-top:5px;">
                                    <?php if ($active) { ?>
                                        <input type="checkbox" name="active" value="1" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="active" value="1">
                                    <?php } ?>
                                    <span class="button-indecator"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <?php if ($customer_id) { ?>
                        <div class="tab-pane" id="tab-2">
                            <a onclick="addressForm();" class="label label-success"><i class="fa fa-plus-circle"></i>
                                Tambah Alamat</a>
                            <table class="table table-hover table-bordered" width="100%" id="datatable-address">
                                <thead>
                                <tr>
                                    <th>Alamat</th>
                                    <th>Default</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    <?php } ?>
                    <div class="tab-pane" id="tab-3">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Bank</label>
                            <div class="col-sm-9">
                                <select name="bank_code" class="form-control">
                                    <?php foreach ($banks as $bank) { ?>
                                        <?php if ($bank['code'] == $bank_code) { ?>
                                            <option value="<?= $bank['code'] ?>"
                                                    selected="selected"><?= $bank['code'] ?>
                                                - <?= $bank['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $bank['code'] ?>"><?= $bank['code'] ?>
                                                - <?= $bank['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Atas Nama</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="bank_account_name"
                                       value="<?= $bank_account_name ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">No. Rekening</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="bank_account_number"
                                       value="<?= $bank_account_number ?>">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-4">
                        <div class="alert alert-info">Biarkan password kosong jika tidak ingin mengganti password</div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="password" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Konfirmasi Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="confirm" value="">
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="modal"></div>
<script src="<?= base_url('assets/js/ajaxupload.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var tableAddress = $('#datatable-address').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('customer/get_addresses')?>",
                "type": "POST",
                "data": function (d) {
                    d.customer_id = "<?=$customer_id?>"
                }
            },
            "columns": [
                {"data": "address_id"},
                {"data": "customer_id"},
                {"orderable": false, "searchable": false, "data": "address_id"},
            ],
            "sDom": '<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<address><b>' + data.name + '</b><br>' + data.address + ', ' + data.subdistrict + '<br>' + data.city + ' - ' + data.province + '</address>');
                html = '<div class="btn-group btn-group-xs">';
                html += '<a onclick="addressForm(' + data.address_id + ');" class="btn btn-primary btn-xs"><i class="fa fa-cog"></i> Edit</a>';
                html += '<a onclick="delRecord(' + data.address_id + ');" class="btn btn-warning btn-xs"><i class="fa fa-minus-circle"></i> Hapus</a>';
                html += '</div>';
                $('td', row).eq(2).addClass('text-right').html(html);

                if (data.is_default === '1') {
                    $('td', row).eq(1).html('<i class="fa fa-check" style="color:green;"></i>');
                } else {
                    $('td', row).eq(1).html('');
                }
            },
            "order": [[0, 'asc']]
        });
    });

    new AjaxUpload('#upload', {
        action: "<?=admin_url('customer/upload?customer_id=' . $customer_id);?>",
        name: 'customerfile',
        autoSubmit: false,
        responseType: 'json',
        onChange: function (file, extension) {
            this.submit();
        },
        onSubmit: function (file, extension) {
            $('#upload').append('<span class="wait"><i class="fa fa-refresh"></i></span>');
        },
        onComplete: function (file, json) {
            if (json.success) {
                $('.wait').remove();
                $('#image').attr('src', json.image);
            }
            if (json.error) {
                alert(json.error);
            }
            $('.loading').remove();
        }
    });

    $('#submit').bind('click', function () {
        var btn = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                $('.alert-danger').remove();

                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('select[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                        $('select[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('customer')?>";
                }
            }
        });
    });

    function delRecord(address_id) {
        swal({
            title: "Apakah anda yakin?",
            text: "Data yang sudah dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('customer/address/delete')?>',
                    type: 'post',
                    data: 'address_id=' + address_id,
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal("Terhapus!", json['success'], "success");
                        } else if (json['error']) {
                            swal("Error!", json['error'], "error");
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                        refreshTable();
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable-address').DataTable().ajax.reload();
    }

    function addressForm(address_id) {
        if (typeof (address_id) == 'undefined') {
            var _url = "<?=admin_url('customer/address/create')?>";
        } else {
            var _url = "<?=admin_url('customer/address/edit')?>";
        }

        $.ajax({
            url: _url,
            data: 'address_id=' + address_id + '&customer_id=<?=$customer_id?>',
            dataType: 'json',
            success: function (json) {
                if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
                } else if (json['content']) {
                    $('#modal').html('<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['content'] + '</div>');
                    $('#form-modal').modal('show');
                    $('#form-modal').on('hidden.bs.modal', function (e) {
                        refreshTable();
                    });
                }
            }
        });
    }
</script>