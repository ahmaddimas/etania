<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Delete address
     *
     * @access public
     * @param int $address_id
     * @return void
     */
    public function delete_address($address_id)
    {
        $this->db
            ->where('address_id', (int)$address_id)
            ->where('customer_id', (int)$this->customer->customer_id())
            ->delete('address');
    }

    /**
     * Get address
     *
     * @access public
     * @param int $address_id
     * @return array
     */
    public function get_address($address_id, $customer_id = false)
    {
        $this->db
            ->select("a.*, l3.name as province, CONCAT_WS(' ', l2.type, l2.name) as city, l.name as subdistrict", false)
            ->from('address a')
            ->join('location l', 'l.subdistrict_id = a.subdistrict_id', 'left')
            ->join('location l2', 'l2.city_id = a.city_id', 'left')
            ->join('location l3', 'l3.province_id = a.province_id and l3.city_id = 0', 'left')
            ->where('a.address_id', (int)$address_id);

        if ($customer_id) {
            $this->db->where('a.customer_id', (int)$customer_id);
        }

        return $this->db
            ->get()
            ->row_array();
    }

    /**
     * Get customer addresses
     *
     * @access public
     * @return array
     */
    public function get_addresses($customer_id)
    {
        return $this->db
            ->select("a.*, l3.name as province, CONCAT_WS(' ', l2.type, l2.name) as city, l.name as subdistrict", false)
            ->from('address a')
            ->join('location l', 'l.subdistrict_id = a.subdistrict_id', 'left')
            ->join('location l2', 'l2.city_id = a.city_id and l2.subdistrict_id = 0', 'left')
            ->join('location l3', 'l3.province_id = a.province_id and l3.city_id = 0', 'left')
            ->where('a.customer_id', (int)$customer_id)
            ->get()
            ->result_array();
    }

    /**
     * Count customer addresses
     *
     * @access public
     * @return int
     */
    public function count_addresses($customer_id)
    {
        return $this->db
            ->select('address_id')
            ->where('customer_id', (int)$customer_id)
            ->count_all_results('address');
    }

    /**
     * Check customer address
     *
     * @access public
     * @param int $address_id
     * @param int $customer_id
     * @return bool
     */
    public function check_address($address_id, $customer_id = null)
    {
        return (bool)$this->db
            ->select('address_id')
            ->where('address_id', (int)$address_id)
            ->where('customer_id', (int)$customer_id)
            ->count_all_results('address');
    }

    /**
     * Set defaukt address
     *
     * @access public
     * @param int $address_id
     * @param int $customer_id
     * @return void
     */
    public function set_default($address_id, $customer_id)
    {
        $this->db
            ->where('address_id', (int)$address_id)
            ->set('customer_id', 0)
            ->update('address');
    }
}