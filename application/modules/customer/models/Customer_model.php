<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class customer_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Create new customer
     *
     * @access public
     * @param array $data
     * @return int | bool
     */
    public function create_customer($data = array())
    {
        $this->load->helper('string');
        $this->load->helper('security');

        if (empty($data['password'])) {
            $data['password'] = random_string('alnum', 8);
        }

        $this->session->set_userdata('random_password', $data['password']);

        $data['date_added'] = date('Y-m-d H:i:s', time());
        $data['salt'] = hash_string();
        $data['password'] = hash_string($data['password'], $data['salt']);

        $username = strtolower(str_replace(' ', '', $data['name']));
        $username = substr($username, 0, 5) . random_string('numeric', 4);

        $data['username'] = $username;

        $this->db
            ->set($this->set_data($data))
            ->insert('customer');

        return $this->db->insert_id();
    }

    /**
     * Update existing customer
     *
     * @access public
     * @param array $data
     * @return int | bool
     */
    public function update_customer($customer_id, $data = array())
    {
        $this->load->helper('string');
        $this->load->helper('security');

        if (!empty($data['password'])) {
            $data['salt'] = hash_string();
            $data['password'] = hash_string($data['password'], $data['salt']);
        } else {
            unset($data['password']);
        }

        $this->db
            ->set($this->set_data($data))
            ->where('customer_id', (int)$customer_id)
            ->update('customer');

        return $this->db->affected_rows();
    }

    /**
     * Get customer
     *
     * @access public
     * @param int $customer_id
     * @return array
     */
    public function get_customer($customer_id = null)
    {
        return $this->db
            ->select('customer.customer_id, customer.name, customer.email, customer.image, customer.type, customer.verified')
            ->where('customer.customer_id', (int)$customer_id)
            ->get('customer')
            ->row_array();
    }

    /**
     * Get customer login data
     *
     * @access public
     * @param string $string
     * @param string $password
     * @param bool $force_login
     * @return mixed
     */
    public function login($string = '', $password = '', $force_login = false)
    {
        if (filter_var($string, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $string)) {
            $field = 'email';
        } else {
            $field = 'username';
        }

        $customer = $this->db->select('customer_id, name, email, salt, password, image, type, verified');
        $customer = $this->db->where($field, $string);

        if ($force_login == false) {
            $customer = $this->db->where('active', 1);
        }

        $customer = $this->db->get('customer')->row_array();

        if ($customer) {
            $this->load->helper('security');

            if ($force_login) {
                return $customer;
            }

            if (hash_string($password, $customer['salt']) === $customer['password']) {
                return $customer;
            }

            return false;
        }

        return false;
    }

    /**
     * Set customer active
     *
     * @access public
     * @param int $customer_id
     * @param string $code
     * @return void
     */
    public function active($customer_id, $code = false)
    {
        if ($code !== false) {
            $query = $this->db->select('customer_id')
                ->where('customer_id', (int)$customer_id)
                ->where('code_activation', $code)
                ->count_all_results('customer');

            if ($query !== 1) {
                return false;
            }

            $this->db
                ->where('customer_id', (int)$customer_id)
                ->where('code_activation', $code)
                ->set(array('code_activation' => '', 'active' => 1))
                ->update('customer');
        } else {
            $this->db
                ->where('customer_id', (int)$customer_id)
                ->set(array('code_activation' => '', 'active' => 1))
                ->update('customer');
        }

        return $this->db->affected_rows();
    }

    /**
     * Deactive customer
     *
     * @access public
     * @param int $customer_id
     * @return void
     */
    public function deactivate($customer_id = null)
    {
        if (empty($customer_id)) {
            return false;
        }

        $this->load->helper('security');

        $this->db
            ->where('customer_id', (int)$customer_id)
            ->set(array('code_activation' => hash_string(microtime() . $customer_id), 'active' => 0))
            ->update('customer');

        return $this->db->affected_rows();
    }

    /**
     * Change password
     *
     * @access public
     * @param int $customer_id
     * @param string $old Old password
     * @param string $new New password
     * @return bool
     */
    public function change_password($customer_id, $old, $new)
    {
        $customer = $this->db
            ->select('customer_id, salt, password')
            ->where('customer_id', (int)$customer_id)
            ->get('customer')
            ->row_array();

        if ($customer) {
            $this->load->helper('security');

            if (hash_string($old, $customer['salt']) === $customer['password']) {
                $this->db
                    ->where('customer_id', (int)$customer_id)
                    ->set(array('password' => hash_string($new, $customer['salt'])))
                    ->update('customer');

                return true;
            }

            return false;
        }
    }

    /**
     * Request reset password code
     *
     * @access public
     * @param string $email
     * @return mixed
     */
    public function request_reset_password($email)
    {
        if (empty($email)) {
            return false;
        }

        $this->load->helper('security');

        $code = hash_string(microtime() . $email);

        $this->db
            ->where('email', strtolower($email))
            ->set('code_forgotten', $code)
            ->update('customer');

        return ($this->db->affected_rows() == 1) ? $code : false;
    }

    /**
     * Cancel reset password
     *
     * @access public
     * @param string $code
     * @return bool
     */
    public function cancel_reset_password($code)
    {
        if (empty($code)) {
            return false;
        }

        if ($customer = $this->db->where('code_forgotten', $code)->get('customer')->row_array()) {
            $this->db
                ->where('customer_id', (int)$customer['customer_id'])
                ->set('code_forgotten', '')
                ->update('customer');

            return $this->db->affected_rows();
        }

        return false;
    }

    /**
     * Reset password
     *
     * @access public
     * @param string $code
     * @return mixed
     */
    public function reset_password($code)
    {
        if (empty($code)) {
            return false;
        }

        if ($customer = $this->db->where('code_forgotten', $code)->get('customer')->row_array()) {
            $this->load->helper('string');
            $this->load->helper('security');

            $new_password = random_string('alnum', 8);
            $password = hash_string($new_password, $customer['salt']);

            $this->db
                ->where('customer_id', (int)$customer['customer_id'])
                ->set(array('password' => $password, 'code_forgotten' => '', 'active' => 1))
                ->update('customer');

            return ($this->db->affected_rows() == 1) ? $new_password : false;
        }

        return false;
    }


    /**
     * Update last login
     *
     * @access public
     * @param int $customer_id
     * @return void
     */
    public function update_last_login($customer_id = null)
    {
        $this->db
            ->where('customer_id', (int)$customer_id)
            ->set(array('last_login' => date('Y-m-d H:i:s', time()), 'ip' => $this->input->ip_address()))
            ->update('customer');
    }

    /**
     * Get customer field data
     *
     * @access public
     * @param int $customer_id
     * @param int $field
     * @return void
     */
    public function get_customer_data($customer_id = null, $field = null)
    {
        if (!in_array($field, $this->db->list_fields('customer'))) {
            return false;
        }

        $result = $this->db
            ->select($field)
            ->where('customer_id', (int)$customer_id)
            ->get('customer')
            ->row_array();

        return $result ? $result[$field] : false;
    }

    /**
     * Check email
     *
     * @access public
     * @param string $email
     * @param int $customer_id
     * @return bool
     */
    public function check_email($email, $customer_id = null)
    {
        if ($customer_id) {
            $this->db->where('customer_id !=', (int)$customer_id);
        }

        return (bool)$this->db
            ->select('customer_id')
            ->where('email', strtolower($email))
            ->count_all_results('customer');
    }

    /**
     * Check username
     *
     * @access public
     * @param string $email
     * @param int $customer_id
     * @return bool
     */
    public function check_username($username, $customer_id = null)
    {
        if ($customer_id) {
            $this->db->where('customer_id !=', (int)$customer_id);
        }

        return (bool)$this->db
            ->select('customer_id')
            ->where('username', $username)
            ->count_all_results('customer');
    }

    /**
     * Check telephone
     *
     * @access public
     * @param string $telephone
     * @param int $customer_id
     * @return bool
     */
    public function check_telephone($telephone, $customer_id = null)
    {
        if ($customer_id) {
            $this->db->where('customer_id !=', (int)$customer_id);
        }

        return (bool)$this->db
            ->select('customer_id')
            ->where('telephone', (string)$telephone)
            ->count_all_results('customer');
    }

    /**
     * Get customers autocomplete
     *
     * @access public
     * @param array $data
     * @return array
     */
    public function get_customers_autocomplete($data = array())
    {
        $this->db->from('customer');

        if (!empty($data['type'])) {
            $this->db->where('type', $data['type']);
        }

        if (!empty($data['name'])) {
            $this->db->like('name', $data['name'], 'both');
            $this->db->or_like('customer_id', (int)$data['name'], 'both');
        }

        $this->db->group_by('customer_id');

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        return $this->db->get()->result_array();
    }

    public function set_default_address($customer_id, $address_id)
    {
        $this->db
            ->where('customer_id', (int)$customer_id)
            ->set('address_id', $address_id)
            ->update('customer');
    }
}