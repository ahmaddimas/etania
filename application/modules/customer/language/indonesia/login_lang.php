<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Login Pelanggan';
$lang['text_customer'] = 'Pelanggan';
$lang['text_login'] = 'Login';
$lang['text_register'] = 'Belum punya akun? silahkan daftar di <a href="%s">sini</a>.';
$lang['text_forgotten'] = '<a href="%s">Lupa password?</a>';
$lang['text_login_success'] = 'Sedang mengarahkan ke dashboard...';
$lang['text_login_error'] = 'Otentikasi tidak valid!';
$lang['text_or'] = 'atau';
$lang['entry_email'] = 'Email';
$lang['entry_password'] = 'Password';
$lang['entry_remember'] = 'Ingat saya';

$lang['button_login'] = 'Masuk';
$lang['button_facebook_login'] = 'Masuk dengan Facebook';
$lang['button_google_login'] = 'Masuk dengan Google';
