<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Pesanan';

$lang['text_account'] = 'Akun Pelanggan';
$lang['text_order'] = 'Info Pesanan';
$lang['text_order_detail'] = 'Info Pesanan';
$lang['text_item'] = 'Item Pesanan';
$lang['text_invoice'] = 'No. Invoice';
$lang['text_order_id'] = 'No. Pesanan';
$lang['text_name'] = 'Nama Pelanggan:';
$lang['text_email'] = 'Email';
$lang['text_telephone'] = 'No. Telepon';
$lang['text_status'] = 'Status';
$lang['text_waybill'] = 'No. Resi';
$lang['text_date'] = 'Tanggal';
$lang['text_shipping_address'] = 'Alamat Pengiriman';
$lang['text_shipping_method'] = 'Metode Pengiriman';
$lang['text_payment_address'] = 'Alamat Penagihan';
$lang['text_payment_method'] = 'Metode pembayaran';
$lang['text_comment'] = 'Catatan';
$lang['text_history'] = 'Status Pemesanan';
$lang['text_rating_instruction'] = 'Sebelum melakukan konfirmasi, mohon luangkan waktu sebentar untuk memberikan rating dan ulasan';
$lang['text_success'] = 'Sukses: Anda telah menambah <a href="%s">%s</a> ke <a href="%s">keranjang belanja anda</a>!';
$lang['text_empty'] = 'Anda belum membuat pesanan sebelumnya!';
$lang['text_error'] = 'Pesanan yang anda minta tidak dapat ditemukan!';

$lang['entry_rating'] = 'Rating';
$lang['entry_review'] = 'Ulasan';

$lang['column_order_id'] = 'No. Pesanan';
$lang['column_invoice'] = 'No. Invoice';
$lang['column_customer'] = 'Pelanggan';
$lang['column_name'] = 'Nama Item';
$lang['column_model'] = 'Model';
$lang['column_quantity'] = 'Qty';
$lang['column_price'] = 'Harga';
$lang['column_total'] = 'Total';
$lang['column_date'] = 'Tanggal';
$lang['column_status'] = 'Status';
$lang['column_comment'] = 'Catatan';

$lang['success_review'] = 'Terima kasih atas kesediaan Anda untuk menulis ulasan';

$lang['button_accept'] = 'Konfirmasi Terima';