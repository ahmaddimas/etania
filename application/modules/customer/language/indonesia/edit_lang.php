<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Edit Profil';
$lang['text_account'] = 'Akun Pelanggan';
$lang['button_image'] = 'Ubah Foto';
$lang['entry_name'] = 'Nama Lengkap';
$lang['entry_dob'] = 'Tanggal Lahir';
$lang['entry_telephone'] = 'No. Telepon';
$lang['entry_email'] = 'Email';
$lang['entry_newsletter'] = 'Berlanggan Newsletter';
$lang['entry_image'] = 'Foto Profil';
$lang['success_edit'] = 'Data profil anda telah berhasil diperbarui';
