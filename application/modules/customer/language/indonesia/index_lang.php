<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Akun Pelanggan';
$lang['text_order'] = 'Pesanan';
$lang['text_order_total'] = '%s Pesanan';
$lang['text_credit'] = 'Saldo Kredit';
$lang['text_name'] = 'Nama';
$lang['text_email'] = 'Email';
$lang['text_telephone'] = 'No. Telepon';
$lang['text_since'] = 'Bergabung Sejak';
