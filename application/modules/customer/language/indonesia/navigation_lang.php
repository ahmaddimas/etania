<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_overview'] = 'Ringkasan';
$lang['text_edit'] = 'Edit Profil';
$lang['text_password'] = 'Ubah Password';
$lang['text_address'] = 'Daftar Alamat';
$lang['text_order'] = 'Pesanan';
$lang['text_order_digital'] = 'Product Digital';
$lang['text_transaction'] = 'Transaksi';
$lang['text_logout'] = 'Logout';
