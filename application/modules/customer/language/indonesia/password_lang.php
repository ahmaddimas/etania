<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Ubah Password';
$lang['text_account'] = 'Akun Pelanggan';
$lang['entry_password'] = 'Password';
$lang['entry_confirm'] = 'Konfirmasi Passsword';
$lang['success_edit'] = 'Password anda telah berhasil diperbarui';
