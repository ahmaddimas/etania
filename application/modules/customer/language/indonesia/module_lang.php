<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['customer/admin'] = 'Pelanggan';
$lang['customer/admin_group'] = 'Grup Pelanggan';
$lang['customer/admin_address'] = 'Alamat';
$lang['customer/admin_email'] = 'Kirim Email';
