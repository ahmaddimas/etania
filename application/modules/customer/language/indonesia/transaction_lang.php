<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Transaksi';
$lang['text_account'] = 'Akun Pelanggan';
$lang['text_date'] = 'Tanggal';
$lang['text_description'] = 'Deskripsi';
$lang['text_status'] = 'Status';
$lang['text_in'] = 'Masuk';
$lang['text_out'] = 'Keluar';
$lang['text_balance'] = 'Saldo';