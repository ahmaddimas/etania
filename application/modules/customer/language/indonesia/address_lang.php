<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Daftar Alamat';

$lang['text_account'] = 'Akun Pelanggan';
$lang['text_address_book'] = 'Tambah Alamat';
$lang['text_create'] = 'Tambah Alamat';
$lang['text_edit'] = 'Ubah Alamat';
$lang['text_empty'] = 'Anda belum memiliki alamat di akun Anda.';
$lang['text_address'] = 'Alamat';
$lang['text_default'] = 'Default';

$lang['entry_name'] = 'Nama';
$lang['entry_address'] = 'Alamat';
$lang['entry_postcode'] = 'Kode Pos';
$lang['entry_city'] = 'Kota/Kabupaten';
$lang['entry_subdistrict'] = 'Kecamatan';
$lang['entry_province'] = 'Provinsi';
$lang['entry_country'] = 'Negara';
$lang['entry_default'] = 'Alamat Default';

$lang['success_create'] = 'Alamat anda telah berhasil dimasukkan';
$lang['success_edit'] = 'Alamat anda telah berhasil diperbarui';
$lang['success_delete'] = 'Alamat anda telah berhasil di hapus';
