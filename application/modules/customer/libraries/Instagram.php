<?php
/**
 * Created by ahmad.
 * Date: 8/31/18
 * Time: 11:54 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Instagram
{
    protected $CI;
    protected $redirect_uri;
    protected $client_id;
    protected $client_secret;

    private $authToken;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function initialize($params = array())
    {
        if ($params) {
            foreach ($params as $key => $val) {
                $this->$key = $val;
            }
        }
    }

    function getAuthUrl()
    {
        return "https://api.instagram.com/oauth/authorize/?client_id=" . $this->client_id . "&redirect_uri=" . $this->redirect_uri . "&response_type=code&scope=public_content";
    }

    function setAccessToken($token)
    {
        $this->authToken = $token;
    }

    function getAccessToken()
    {
        return $this->authToken;
    }

    function authenticate($code)
    {
        $url = 'https://api.instagram.com/oauth/access_token';

        $curlPost = 'client_id=' . $this->client_id . '&redirect_uri=' . $this->redirect_uri . '&client_secret=' . $this->client_secret . '&code=' . $code . '&grant_type=authorization_code';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($http_code != '200')
            throw new Exception('Error : Failed to receieve access token');

        return $data['access_token'];
    }

    function getProfileInfo($access_token)
    {
        $url = 'https://api.instagram.com/v1/users/self/?access_token=' . $access_token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($data['meta']['code'] != 200 || $http_code != 200)
            throw new Exception('Error : Failed to get user information');

        return $data['data'];
    }
}