<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer
{
    private $ci;
    private $customer_id = null;
    private $name = '';
    private $type = '';
    private $email = '';
    private $image = null;
    private $verified = false;

    public function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->model('customer/customer_model');
        $this->ci->load->library('encrypt');
        $this->ci->load->helper('cookie');

        if ($customer = $this->ci->customer_model->get_customer($this->ci->session->userdata('customer_id'))) {
            $this->customer_id = (int)$customer['customer_id'];
            $this->name = $customer['name'];
            $this->type = $customer['type'];
            $this->email = $customer['email'];
            $this->image = $customer['image'];
            $this->verified = (bool)$customer['verified'];
        } elseif ($customer = get_cookie('customer')) {
            $data = json_decode($this->ci->encrypt->decode($customer), true);

            $this->login($data['identity'], '', true, true);
        } else {
            $this->logout();
        }
    }

    public function login($string, $password, $remember = false, $override = false)
    {
        if ($customer = $this->ci->customer_model->login($string, $password, $override)) {
            $this->ci->session->set_userdata('customer_id', $customer['customer_id']);

            $this->ci->customer_model->update_last_login($customer['customer_id']);

            $this->customer_id = (int)$customer['customer_id'];
            $this->name = $customer['name'];
            $this->type = $customer['type'];
            $this->email = $customer['email'];
            $this->image = $customer['image'];
            $this->verified = (bool)$customer['verified'];

            if ($remember) {
                $data['identity'] = $customer['email'];

                set_cookie(array(
                    'name' => 'customer',
                    'value' => $this->ci->encrypt->encode(json_encode($data)),
                    'expire' => strtotime('+6 months'),
                ));
            }

            return true;
        }

        return false;
    }

    public function logout()
    {
        $this->ci->session->unset_userdata('customer_id');

        if (get_cookie('customer')) {
            delete_cookie('customer');
        }

        $this->customer_id = null;
        $this->name = '';
        $this->type = '';
        $this->email = '';
        $this->image = null;
        $this->verified = false;
    }

    public function is_logged($redirect = false)
    {
        if ($redirect) {
            $this->ci->session->set_flashdata('redirect', $redirect);
        }

        return (bool)$this->customer_id;
    }

    public function customer_id()
    {
        return (int)$this->customer_id;
    }

    public function name()
    {
        return $this->name;
    }

    public function email()
    {
        return $this->email;
    }

    public function type()
    {
        return $this->type;
    }

    public function is_verified()
    {
        return (bool)$this->verified;
    }

    public function image()
    {
        if ($this->image == '') {
            $image = 'customer.png';
        } else {
            $image = $this->image;
        }

        return $this->ci->image->resize($image, 50, 50);
    }

    public function get_balance()
    {
        $result = $this->ci->db
            ->select_sum('amount')
            ->where('customer_id', (int)$this->customer_id)
            ->where('approved', 1)
            ->get('customer_transaction')
            ->row_array();

        if ($result) {
            return (float)$result['amount'];
        } else {
            return 0;
        }
    }
}