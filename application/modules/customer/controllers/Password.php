<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('customer/password');
        $this->load->model('customer_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    /**
     * Edit profile
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $data['action'] = site_url('customer/password/validate');

        $this->load
            ->title(lang('heading_title'))
            ->breadcrumb(lang('text_home'), site_url())
            ->breadcrumb(lang('text_account'), site_url('customer'))
            ->breadcrumb(lang('heading_title'), site_url('customer/password'))
            ->view('password', $data);
    }

    /**
     * Validate profile
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $this->form_validation
            ->set_rules('password', 'lang:entry_password', 'trim|required|min_length[8]')
            ->set_rules('confirm', 'lang:entry_confirm', 'trim|required|matches[password]')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['errors'][$field] = $error;
            }
        } else {
            $this->customer_model->update_customer($this->customer->customer_id(), $this->input->post(null, true));
            $json['success'] = lang('success_edit');
        }

        $this->output->set_output(json_encode($json));
    }
}