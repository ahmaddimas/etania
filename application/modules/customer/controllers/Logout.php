<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Customer logout
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->customer->logout();
        $this->session->set_flashdata('message', 'You are logged out!');
        redirect('customer/login', 'refresh');
    }
}