<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('customer/transaction');
    }

    public function index()
    {
        $code = $this->input->post('code');

        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('customer_transaction_id, approved, @uti := customer_transaction_id, description, date_added, (CASE WHEN amount > 0 THEN amount ELSE NULL END) AS debit, (CASE WHEN amount < 0 THEN amount * (-1) ELSE NULL END) AS credit, (SELECT SUM(amount) FROM customer_transaction ut WHERE ut.customer_transaction_id < @uti AND ut.customer_id = ' . (int)$this->customer->customer_id() . ') + amount as balance', false)
                ->from('customer_transaction')
                ->where('customer_id', $this->customer->customer_id());

            if ($code) {
                $this->datatables->where('code', $code);
            }

            $this->datatables->edit_column('date_added', '$1', 'format_date(date_added)')
                ->edit_column('debit', '$1', 'format_money(debit)')
                ->edit_column('credit', '$1', 'format_money(credit)')
                ->edit_column('balance', '$1', 'format_money(balance)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            $data['code'] = $code;

            $this->load
                ->title(lang('heading_title'))
                ->css('/assets/plugins/datatables/dataTables.bootstrap.css')
                ->js('/assets/plugins/datatables/jquery.dataTables.min.js')
                ->js('/assets/plugins/datatables/dataTables.bootstrap.min.js')
                ->breadcrumb(lang('text_home'), site_url())
                ->breadcrumb(lang('text_account'), site_url('customer'))
                ->breadcrumb(lang('heading_title'), site_url('customer/transaction'))
                ->view('transaction', $data);
        }
    }
}