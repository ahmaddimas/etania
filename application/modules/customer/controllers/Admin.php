<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller
{
    private $emails;

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('customer_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->CI =& $this;
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('u.customer_id, u.name, u.email, u.telephone, u.active, u.date_added, cg.name as group')
                ->from('customer u')
                ->join('customer_group cg', 'cg.customer_group_id = u.customer_group_id', 'left')
                ->edit_column('date_added', '$1', 'format_date(date_added)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load->title('Pelanggan')->view('admin/customer');
        }
    }

    public function transaction($customer_id = null)
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('customer_transaction_id, approved, @uti := customer_transaction_id, description, date_added, (CASE WHEN amount > 0 THEN amount ELSE NULL END) AS debit, (CASE WHEN amount < 0 THEN amount * (-1) ELSE NULL END) AS credit, (SELECT SUM(amount) FROM customer_transaction ut WHERE ut.customer_transaction_id < @uti AND ut.customer_id = ' . (int)$this->input->post('customer_id') . ') + amount as balance', false)
                ->from('customer_transaction')
                ->where('customer_id', $this->input->post('customer_id'))
                ->edit_column('date_added', '$1', 'format_date(date_added)')
                ->edit_column('debit', '$1', 'format_money(debit)')
                ->edit_column('credit', '$1', 'format_money(credit)')
                ->edit_column('balance', '$1', 'format_money(balance)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            if ($customer = $this->customer_model->get($customer_id)) {
                $data['name'] = $customer['name'];
            } else {
                $data['name'] = '';
            }

            $data['customer_id'] = $customer_id;

            $this->load->title('Daftar Transaksi')->view('admin/customer_transaction', $data);
        }
    }

    public function info($customer_id)
    {
        if ($customer = $this->customer_model->get($customer_id)) {
            foreach ($customer as $key => $value) {
                $data[$key] = $value;
            }

            if ($customer['type'] == 'exhibitor') {
                $data['heading_title'] = 'Exhibitor Detail';
            } else {
                $data['heading_title'] = 'Visitor Detail';
            }

            $this->load->view('admin/customer_info', $data);
        } else {
            show_404();
        }
    }

    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->title('Tambah Pelanggan');

        $this->form();
    }

    public function edit($customer_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->title('Edit Pelanggan');

        $this->form($customer_id);
    }

    public function form($customer_id = null)
    {
        $this->load->library('image');
        $this->load->model('location/location_model');

        if ($customer_id) {
            foreach ($this->customer_model->get($customer_id) as $key => $value) {
                $data[$key] = $value;
            }
        } else {
            foreach ($this->customer_model->list_fields() as $field) {
                $data[$field] = '';
            }
        }

        if (!empty($data['image'])) {
            $data['image'] = $this->image->resize($data['image'], 150, 150);
        } else {
            $data['image'] = $this->image->resize('no_image.jpg', 150, 150);
        }

        $this->load->model('customer_group_model');
        $data['customer_groups'] = $this->customer_group_model->get_all();

        $data['action'] = admin_url('customer/validate');

        $this->load->view('admin/customer_form', $data);
    }

    public function validate()
    {
        check_ajax();

        $json = array();

        if ($this->input->post('password') !== '') {
            $this->form_validation
                ->set_rules('password', 'Password', 'trim|required|min_length[6]')
                ->set_rules('confirm', 'Konfirmasi Password', 'trim|required|matches[password]');
        }

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required')
            ->set_rules('telephone', 'No. Telephone', 'trim|required|callback__check_telephone')
            ->set_rules('email', 'Email', 'trim|required|valid_email|callback__check_email')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            if ($this->input->post('password') !== '') {
                if (form_error('password') !== '') {
                    $json['errors']['password'] = form_error('password');
                }

                if (form_error('confirm') !== '') {
                    $json['errors']['confirm'] = form_error('confirm');
                }
            }

            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['errors'][$field] = $error;
            }
        } else {
            $customer_id = $this->input->post('customer_id');
            $data = $this->input->post(null, true);
            $data['active'] = (bool)$this->input->post('active');
            $data['newsletter'] = (bool)$this->input->post('newsletter');

            if ($customer_id) {
                $this->customer_model->update_customer($customer_id, $data);
                $success = 'Data pelanggan berhasil diperbarui';
            } else {
                $customer_id = $this->customer_model->create_customer($data);
                $success = 'Berhasil menambahkan pelanggan baru';
            }

            $json['success'] = $success;
        }

        $this->output->set_output(json_encode($json));
    }

    public function upload()
    {
        $json = array();

        $upload_path = DIR_IMAGE . 'customer';

        if (!file_exists($upload_path)) {
            @mkdir($upload_path, 0777);
        }

        $this->load->library('upload', array(
            'upload_path' => $upload_path,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size' => '3000',
            'encrypt_name' => true
        ));

        if (!$this->upload->do_upload()) {
            $json['error'] = $this->upload->display_errors('', '');
        } else {
            $this->load->library('image');

            $upload_data = $this->upload->data();
            $image = substr($upload_data['full_path'], strlen(FCPATH . DIR_IMAGE));

            $thumb = $this->image->resize($image, 128, 128);
            $this->customer_model->update($this->input->get('customer_id'), array('image' => $image));

            $json['image'] = $thumb;
            $json['success'] = 'File berhasil diupload!';
        }

        $this->output->set_output(json_encode($json));
    }

    public function _check_email($email)
    {
        if ($this->customer_model->check_email($email, $this->input->post('customer_id'))) {
            $this->form_validation->set_message('_check_email', 'Email ini tidak tersedia!');
            return false;
        }

        return true;
    }

    public function _check_telephone($telephone)
    {
        if ($this->customer_model->check_telephone($telephone, $this->input->post('customer_id'))) {
            $this->form_validation->set_message('_check_telephone', 'No. telepon ini sudah ada yang menggunakan!');
            return false;
        }

        return true;
    }

    public function delete()
    {
        check_ajax();

        $json = array();

        $customer_id = $this->input->post('customer_id');

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->customer_model->delete($customer_id);
            $json['success'] = 'Berhasil: customer telah berhasil dihapus!';
        }

        $this->output->set_output(json_encode($json));
    }

    public function autocomplete()
    {
        $json = array();

        if ($this->input->get('name')) {
            $params = array(
                'name' => $this->input->get('name'),
                'type' => $this->input->get('type'),
                'start' => 0,
                'limit' => 20
            );

            foreach ($this->customer_model->get_customers_autocomplete($params) as $customer) {
                $json[$customer['customer_id']] = array(
                    'customer_id' => (int)$customer['customer_id'],
                    'name' => $customer['customer_id'] . ' - ' . strip_tags(html_entity_decode($customer['name'], ENT_QUOTES, 'UTF-8')),
                    'type' => strip_tags(html_entity_decode(ucwords($customer['type']), ENT_QUOTES, 'UTF-8')),
                    'email' => $customer['email'],
                    'telephone' => $customer['telephone']
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->output->set_output(json_encode($json));
    }

    public function get_addresses()
    {
        check_ajax();

        if ($this->input->post()) {
            $this->load->library('datatables');

            $this->datatables
                ->select("a.*, l3.name as province, CONCAT_WS(' ', l2.type, l2.name) as city, l.name as subdistrict, (CASE WHEN u.address_id = a.address_id THEN 1 ELSE 0 END) as is_default", false)
                ->from('address a')
                ->join('location l', 'l.subdistrict_id = a.subdistrict_id', 'left')
                ->join('location l2', 'l2.city_id = a.city_id and l2.subdistrict_id = 0', 'left')
                ->join('location l3', 'l3.province_id = a.province_id and l3.city_id = 0', 'left')
                ->join('customer u', 'u.customer_id = a.customer_id', 'left')
                ->where('a.customer_id', $this->input->post('customer_id'));

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            $json = array();

            $this->load->model('address_model');

            if ($addresses = $this->address_model->get_addresses($this->input->get('customer_id'))) {
                foreach ($addresses as $address) {
                    $json[] = $address;
                }
            }

            $this->output->set_output(json_encode($json));
        }
    }

    public function export()
    {
        if (!$this->admin_auth->has_permission('index')) {
            show_401($this->uri->uri_string());
        }

        $this->load->helper('format');

        $data['reports'] = array();

        $results = $this->db
            ->select('u.customer_id, u.name, u.email, u.telephone, u.active, u.date_added, cg.name as group')
            ->from('customer u')
            ->join('customer_group cg', 'cg.customer_group_id = u.customer_group_id', 'left')
            ->get()
            ->result_array();

        $total = 0;

        foreach ($results as $result) {
            $result['date_added'] = format_date($result['date_added']);
            $result['status'] = $result['active'] ? 'Aktif' : 'Non Aktif';
            $data['reports'][] = $result;
            $total++;
        }

        $data['total'] = (int)$total;
        $format = $this->input->get('format');

        switch ($format) {
            case 'pdf':
                $this->load->library('pdf');
                $file_name = 'customer-report-' . date('dmYHis', time());
                $filename = APPPATH . 'cache/docs/' . $file_name . '.pdf';
                $this->pdf->pdf_create($this->load->layout(false)->view('admin/customer_export', $data, true), $file_name, true);
                break;

            case 'pdf_download':
                $this->load->library('pdf');
                $file_name = 'customer-report-' . date('dmYHis', time());
                $filename = APPPATH . 'cache/docs/' . $file_name . '.pdf';
                $this->pdf->pdf_create($this->load->layout(false)->view('admin/customer_export', $data, true), $filename, false);

                if (file_exists($filename)) {
                    $this->load->helper('download');

                    $content = file_get_contents($filename);
                    force_download($file_name . '.pdf', $content);
                }
                break;

            case 'excel_download':
                $this->load->library('PHPExcel');

                require_once(APPPATH . 'libraries/PHPExcel.php');
                require_once(APPPATH . 'libraries/PHPExcel/IOFactory.php');

                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
                $objPHPExcel->setActiveSheetIndex(0);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Data Pelanggan');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'Total:');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $data['total']);

                $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, 'ID');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, 'Name Lengkap');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, 'No. Handphone');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 4, 'Email');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 4, 'Tanggal Daftar');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 4, 'Grup');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 4, 'Status');

                $objPHPExcel->getActiveSheet()->getStyle('A4:G4')->getFont()->setBold(true);

                $row = 5;

                foreach ($data['reports'] as $report) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $report['customer_id']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $report['name']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $report['telephone']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $report['email']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, format_date($report['date_added']));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $report['group']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $report['status']);

                    $row++;
                }

                $objPHPExcel->setActiveSheetIndex(0);

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="customer-report-' . date('dmYHis') . '.xls"');
                header('Cache-Control: max-age=0');

                $objWriter->save('php://output');
                break;

            default:
                $this->load->layout(false)->view('admin/customer_export', $data);
                break;
        }
    }
} 