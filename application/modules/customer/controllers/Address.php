<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('address');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('address_model');
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post()) {
            $this->load->library('datatables');

            $this->datatables
                ->select("a.*, l3.name as province, CONCAT_WS(' ', l2.type, l2.name) as city, l.name as subdistrict, (CASE WHEN u.address_id = a.address_id THEN 1 ELSE 0 END) as is_default", false)
                ->from('address a')
                ->join('location l', 'l.subdistrict_id = a.subdistrict_id', 'left')
                ->join('location l2', 'l2.city_id = a.city_id and l2.subdistrict_id = 0', 'left')
                ->join('location l3', 'l3.province_id = a.province_id and l3.city_id = 0', 'left')
                ->join('customer u', 'u.customer_id = a.customer_id', 'left')
                ->where('a.customer_id', $this->customer->customer_id());

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            $this->load
                ->title(lang('heading_title'))
                ->css('/assets/plugins/datatables/dataTables.bootstrap.css')
                ->js('/assets/plugins/jquery.dataTables.min.js')
                ->js('/assets/plugins/dataTables.bootstrap.min.js')
                ->breadcrumb(lang('text_home'), site_url())
                ->breadcrumb(lang('text_account'), site_url('customer'))
                ->breadcrumb(lang('heading_title'), site_url('customer/address'))
                ->view('address');
        }
    }

    public function get_address()
    {
        $this->load->model('address_model');

        $this->output->set_output(json_encode($this->address_model->get($this->input->get('address_id'))));
    }

    public function create()
    {
        check_ajax();

        $this->load->vars(array('heading_title' => lang('text_create')));

        $this->form();
    }

    public function edit()
    {
        check_ajax();

        $this->load->vars(array('heading_title' => lang('text_edit')));

        $this->form($this->input->get('address_id'));
    }

    public function form($address_id = null)
    {
        $this->load->model('location/location_model');

        if ($address_id) {
            foreach ($this->address_model->get($address_id) as $key => $value) {
                $data[$key] = $value;
            }
        } else {
            foreach ($this->address_model->list_fields() as $field) {
                $data[$field] = '';
            }
        }

        $data['default'] = false;

        if ($customer = $this->customer_model->get($this->customer->customer_id())) {
            if ($customer['address_id'] == $address_id) {
                $data['default'] = true;
            }
        }

        $data['customer_id'] = $this->customer->customer_id();
        $data['action'] = site_url('customer/address/validate');
        $data['provinces'] = $this->location_model->get_provinces();

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(false)->view('address_form', $data, true)
        )));
    }

    public function validate()
    {
        check_ajax();

        $json = array();

        $this->form_validation
            ->set_rules('name', 'lang:entry_name', 'trim|required')
            ->set_rules('address', 'lang:entry_address', 'trim|required')
            ->set_rules('postcode', 'lang:entry_postcode', 'trim|required|numeric|min_length[5]')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['errors'][$field] = $error;
            }
        } else {
            $address_id = $this->input->post('address_id');
            $data = $this->input->post(null, true);
            $data['customer_id'] = $this->customer->customer_id();

            if ($address_id) {
                $this->address_model->update($address_id, $data);
                $success = lang('success_edit');
            } else {
                $address_id = $this->address_model->insert($data);
                $success = lang('success_create');
            }

            if ($this->input->post('default')) {
                $this->customer_model->set_default_address($this->customer->customer_id(), $address_id);
            }

            $json['success'] = $success;
        }

        $this->output->set_output(json_encode($json));
    }

    public function delete()
    {
        check_ajax();

        $json = array();

        $address_id = $this->input->post('address_id');

        $this->address_model->delete_by(array('address_id' => $address_id, 'customer_id' => $this->customer->customer_id()));
        $json['success'] = lang('success_delete');

        $this->output->set_output(json_encode($json));
    }
} 