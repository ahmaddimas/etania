<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook_Login extends Front_Controller
{
    private $app_id = '';
    private $app_secret = '';
    private $redirect_url = '';

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->config('facebook');

        if ($this->config->item('app_id', 'facebook') != '') {
            $this->app_id = $this->config->item('app_id', 'facebook');
        } else {
            $this->app_id = $this->config->item('facebook_app_id');
        }

        if ($this->config->item('app_secret', 'facebook') != '') {
            $this->app_secret = $this->config->item('app_secret', 'facebook');
        } else {
            $this->app_secret = $this->config->item('facebook_app_secret');
        }

        if ($this->config->item('redirect_url', 'facebook') != '') {
            $this->redirect_url = $this->config->item('redirect_url', 'facebook');
        } else {
            $this->redirect_url = site_url('customer/facebook_login/callback');
        }

        if (!$this->app_id || !$this->app_secret) {
            show_404();
        }
    }

    public function callback()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $fb = new Facebook\Facebook(array(
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret
        ));

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error

            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues

            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        try {
            // Get the Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $response = $fb->get('/me?fields=id,name,email,first_name,last_name,gender', (string)$accessToken);
            //  print_r($response);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'ERROR: Graph ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'ERROR: validation fails ' . $e->getMessage();
            exit;
        }

        $me = $response->getGraphUser();

        if (!$this->customer->is_logged()) {
            if ($this->customer_model->check_email($me->getProperty('email'))) {
                $this->customer->login($me->getProperty('email'), '', true, true);
                $this->redirect();
            } else {
                $this->load->helper('string');
                $this->load->helper('security');

                $password = random_string('alnum', 8);
                $salt = hash_string();

                $image = 'https://graph.facebook.com/' . $me->getProperty('id') . '/picture?type=large';

                $new_user = array(
                    'name' => $me->getProperty('first_name') . ' ' . $me->getProperty('last_name'),
                    'gender' => $me->getProperty('gender') == 'male' ? 'm' : 'f',
                    'email' => $me->getProperty('email'),
                    'image' => $image,
                    'salt' => $salt,
                    'password' => hash_string($password, $salt),
                    'active' => 1
                );

                if ($user_id = $this->customer_model->insert($new_user)) {
                    copy($image, FCPATH . DIR_IMAGE . 'user/image-' . $user_id . '.jpg');
                    if (file_exists(FCPATH . DIR_IMAGE . 'user/image-' . $user_id . '.jpg')) {
                        $this->customer_model->update($user_id, array('image' => 'user/image-' . $user_id . '.jpg'));
                    }

                    $this->customer->login($me->getProperty('email'), $password, true, true);
                    $this->redirect();
                }
            }
        } else {
            $this->index();
        }
    }

    public function index()
    {
        if ($this->customer->is_logged()) {
            $this->redirect();
        }

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $fb = new Facebook\Facebook(array(
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret
        ));

        $helper = $fb->getRedirectLoginHelper();

        $loginUrl = $helper->getLoginUrl($this->redirect_url, $this->config->item('permissions', 'facebook'));
        redirect($loginUrl);
    }

    private function redirect()
    {
        $redirect = PATH_USER;

        if ($this->session->userdata('redirect')) {
            $redirect = $this->session->userdata('redirect');
            $this->session->unset_userdata('redirect');
        }

        redirect($redirect);
    }
}