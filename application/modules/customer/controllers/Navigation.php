<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Navigation extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->lang->load('customer/navigation');

        return $this->load->layout(false)->view('navigation', array(), true);
    }
}