<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google_Login extends Front_Controller
{
    private $client;

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        require APPPATH . 'vendor/google/api-client/Google_Client.php';
        require APPPATH . 'vendor/google/api-client/contrib/Google_Oauth2Service.php';

        $this->client = new Google_Client();
        $this->client->setApplicationName($this->config->item('site_name'));
        $this->client->setClientId($this->config->item('google_client_id'));
        $this->client->setClientSecret($this->config->item('google_client_secret'));
        $this->client->setRedirectUri(base_url('customer/' . $this->router->fetch_class()));
        $this->client->setDeveloperKey($this->config->item('google_api_key'));
        $this->client->setAccessType('online');
        $this->client->setApprovalPrompt('auto');
    }

    /**
     * Google login
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if (!$this->customer->is_logged()) {
            $user = new Google_Oauth2Service($this->client);

            if ($this->input->get('code')) {
                $this->client->authenticate($this->input->get('code'));
                $this->session->set_userdata('google_token', $this->client->getAccessToken());
            }

            if ($this->session->userdata('google_token')) {
                $this->client->setAccessToken($this->session->userdata('google_token'));
            }

            if ($this->client->getAccessToken()) {
                $user_info = $user->userinfo->get();

                $user_info = (object)$user_info;

                if ($this->customer_model->check_email($user_info->email)) {
                    $this->customer->login($user_info->email, '', true, true);
                    redirect(PATH_USER);
                } else {
                    $this->load->helper('string');
                    $this->load->helper('security');

                    $password = random_string('alnum', 8);
                    $salt = hash_string();
                    $photo = $user_info->picture;

                    $new_user = array(
                        'name' => $user_info->given_name . ' ' . $user_info->family_name,
                        'gender' => $user_info->gender == 'male' ? 'm' : 'f',
                        'email' => $user_info->email,
                        'photo' => $photo,
                        'salt' => $salt,
                        'password' => hash_string($password, $salt),
                        'active' => 1
                    );

                    if ($user_id = $this->customer_model->insert($new_user)) {
                        copy($photo, FCPATH . DIR_IMAGE . 'user/photo_' . $user_id . '.jpg');
                        if (file_exists(FCPATH . DIR_IMAGE . 'user/photo_' . $user_id . '.jpg')) {
                            $this->customer_model->update($user_id, array('photo' => 'user/photo_' . $user_id . '.jpg'));
                        }

                        $this->customer->login($user_info->email, $password, true, true);

                        $this->redirect();
                    }
                }
            } else {
                redirect($this->client->createAuthUrl());
            }
        } else {
            $this->redirect();
        }
    }

    /**
     * Redirect
     *
     * @access private
     * @return void
     */
    private function redirect()
    {
        $redirect = PATH_USER;

        if ($this->session->userdata('redirect')) {
            $redirect = $this->session->userdata('redirect');
            $this->session->unset_userdata('redirect');
        }

        redirect($redirect);
    }
}