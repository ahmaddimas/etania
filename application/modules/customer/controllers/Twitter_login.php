<?php
/**
 * Created by ahmad.
 * Date: 8/31/18
 * Time: 12:38 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Twitter_login extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (!$this->customer->is_logged()) {
            include_once APPPATH . "vendor/twitter/TwitterOAuth.php";

            $consumerKey = $this->config->item('twitter_consumer_key');
            $consumerSecret = $this->config->item('twitter_consumer_secret');
            $oauthCallback = base_url('customer/' . $this->router->fetch_class());

            $sessToken = $this->session->userdata('twitter_token');
            $sessTokenSecret = $this->session->userdata('twitter_token_secret');

            if (isset($_REQUEST['oauth_token']) && $sessToken == $_REQUEST['oauth_token']) {
                $connection = new TwitterOAuth($consumerKey, $consumerSecret, $sessToken, $sessTokenSecret);

                if ($connection->http_code == '200') {
                    $userInfo = $connection->get('account/verify_credentials');

                    if ($this->customer_model->check_email($userInfo->email)) {
                        $this->customer->login($userInfo->email, '', true, true);
                        redirect(PATH_USER);
                    } else {
                        $this->load->helper('string');
                        $this->load->helper('security');

                        $password = random_string('alnum', 8);
                        $salt = hash_string();
                        $photo = $userInfo->profile_image_url;

                        $new_user = array(
                            'name' => $userInfo->name,
                            'gender' => $userInfo->gender == 'male' ? 'm' : 'f',
                            'email' => $userInfo->email,
                            'photo' => $photo,
                            'salt' => $salt,
                            'password' => hash_string($password, $salt),
                            'active' => 1
                        );

                        if ($user_id = $this->customer_model->insert($new_user)) {
                            copy($photo, FCPATH . DIR_IMAGE . 'user/photo_' . $user_id . '.jpg');
                            if (file_exists(FCPATH . DIR_IMAGE . 'user/photo_' . $user_id . '.jpg')) {
                                $this->customer_model->update($user_id, array('photo' => 'user/photo_' . $user_id . '.jpg'));
                            }

                            $this->customer->login($userInfo->email, $password, true, true);

                            $this->redirect();
                        }
                    }
                }
            } else {
                $this->session->unset_userdata('twitter_token');
                $this->session->unset_userdata('twitter_token_secret');

                //Fresh authentication
                $connection = new TwitterOAuth($consumerKey, $consumerSecret);
                $requestToken = $connection->getRequestToken($oauthCallback);

                $this->session->set_userdata('twitter_token', $requestToken['oauth_token']);
                $this->session->set_userdata('twitter_token_secret', $requestToken['oauth_token_secret']);

                if ($connection->http_code == '200') {
                    $redirectUrl = $connection->getAuthorizeURL($requestToken['oauth_token']);
                } else {
                    $redirectUrl = base_url('customer/login');
                }

                redirect($redirectUrl);
            }
        } else {
            $this->redirect();
        }
    }

    /**
     * Redirect
     *
     * @access private
     * @return void
     */
    private function redirect()
    {
        $redirect = PATH_USER;

        if ($this->session->userdata('redirect')) {
            $redirect = $this->session->userdata('redirect');
            $this->session->unset_userdata('redirect');
        }

        redirect($redirect);
    }
}