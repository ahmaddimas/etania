<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('customer/edit');
        $this->load->model('customer_model');
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->CI =& $this;
    }

    /**
     * Edit profile
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->load->library('image');

        foreach ($this->customer_model->get($this->customer->customer_id()) as $key => $value) {
            $data[$key] = $value;
        }

        if (!empty($data['image'])) {
            $data['image'] = $this->image->resize($data['image'], 150, 150);
        } else {
            $data['image'] = $this->image->resize('no_image.jpg', 150, 150);
        }

        $data['action'] = site_url('customer/edit/validate');
        $data['banks'] = array();
        $data['customer_id'] = $this->customer->customer_id();
        $data['dob_year'] = date('Y', strtotime($data['dob']));
        $data['dob_month'] = date('m', strtotime($data['dob']));
        $data['dob_date'] = date('d', strtotime($data['dob']));

        $this->lang->load('calendar');

        $data['months'] = array(
            '01' => lang('cal_january'),
            '02' => lang('cal_february'),
            '03' => lang('cal_march'),
            '04' => lang('cal_april'),
            '05' => lang('cal_may'),
            '06' => lang('cal_june'),
            '07' => lang('cal_july'),
            '08' => lang('cal_august'),
            '09' => lang('cal_september'),
            '10' => lang('cal_october'),
            '11' => lang('cal_november'),
            '12' => lang('cal_december'),
        );

        $this->load
            ->title(lang('heading_title'))
            ->breadcrumb(lang('text_home'), site_url())
            ->breadcrumb(lang('text_account'), site_url('customer'))
            ->breadcrumb(lang('heading_title'), site_url('customer/edit'))
            ->view('edit', $data);
    }

    /**
     * Validate profile
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required')
            ->set_rules('telephone', 'No. Telephone', 'trim|required')
            ->set_rules('email', 'Email', 'trim|required|valid_email|callback__check_email')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            if ($this->input->post('password') !== '') {
                if (form_error('password') !== '') {
                    $json['errors']['password'] = form_error('password');
                }

                if (form_error('confirm') !== '') {
                    $json['errors']['confirm'] = form_error('confirm');
                }
            }

            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['errors'][$field] = $error;
            }
        } else {
            $post = $this->input->post(null, true);
            $post['dob'] = $this->input->post('dob_year') . '-' . $this->input->post('dob_month') . '-' . $this->input->post('dob_date');
            $post['newsletter'] = (int)$this->input->post('newsletter');

            $this->customer_model->update_customer($this->customer->customer_id(), $post);
            $json['success'] = 'Data profil Anda telah berhasil diperbarui';
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Upload image file
     *
     * @access public
     * @return void
     */
    public function upload()
    {
        $json = array();

        $upload_path = DIR_IMAGE . 'customer';

        if (!file_exists($upload_path)) {
            @mkdir($upload_path, 0777);
        }

        $this->load->library('upload', array(
            'upload_path' => $upload_path,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size' => '3000',
            'encrypt_name' => true
        ));

        if (!$this->upload->do_upload()) {
            $json['error'] = $this->upload->display_errors('', '');
        } else {
            $this->load->library('image');

            $upload_data = $this->upload->data();
            $image = substr($upload_data['full_path'], strlen(FCPATH . DIR_IMAGE));

            $thumb = $this->image->resize($image, 150, 150);
            $this->customer_model->update($this->customer->customer_id(), array('image' => $image));

            $json['image'] = $thumb;
            $json['success'] = 'File berhasil diupload!';
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Callback check username
     *
     * @access public
     * @param string $username
     * @return bool
     */
    public function _check_username($username)
    {
        if ($this->customer_model->check_username($username, $this->customer->customer_id())) {
            $this->form_validation->set_message('_check_username', 'username ini sudah digunakan!');

            return false;
        }

        return true;
    }

    /**
     * Callback check username
     *
     * @access public
     * @param string $username
     * @return bool
     */
    public function _check_email($email)
    {
        if ($this->customer_model->check_email($email, $this->customer->customer_id())) {
            $this->form_validation->set_message('_check_email', 'Email ini sudah digunakan!');

            return false;
        }

        return true;
    }
}