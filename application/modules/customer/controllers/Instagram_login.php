<?php
/**
 * Created by ahmad.
 * Date: 8/31/18
 * Time: 11:30 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Instagram_login extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("customer/instagram");

        $options = array(
            'redirect_uri' => base_url('customer/' . $this->router->fetch_class()),
            'client_id' => $this->config->item('instagram_client_id'),
            'client_secret' => $this->config->item('instagram_client_secret')
        );

        $this->instagram->initialize($options);
    }

    public function index()
    {
        if (!$this->customer->is_logged()) {
            if ($this->input->get('code')) {
                $authToken = $this->instagram->authenticate($this->input->get('code'));
                $this->session->set_userdata('instagram_token', $authToken);
            }

            if ($this->session->userdata('instagram_token')) {
                $this->instagram->setAccessToken($this->session->userdata('instagram_token'));
            }

            if ($this->instagram->getAccessToken()) {
                $authToken = $this->instagram->getAccessToken();

                if ($authToken) {
                    $user_info = $this->instagram->getProfileInfo($authToken);
                    $user_info = (object)$user_info;

                    if ($this->customer_model->check_username($user_info->username)) {
                        $this->customer->login($user_info->username, '', true, true);
                        redirect(PATH_USER);
                    } else {
                        $this->load->helper('string');
                        $this->load->helper('security');

                        $password = random_string('alnum', 8);
                        $salt = hash_string();
                        $photo = $user_info->profile_picture;

                        $new_user = array(
                            'name' => $user_info->full_name,
                            'username' => $user_info->username,
                            'photo' => $photo,
                            'salt' => $salt,
                            'password' => hash_string($password, $salt),
                            'active' => 1
                        );

                        if ($user_id = $this->customer_model->insert($new_user)) {
                            copy($photo, FCPATH . DIR_IMAGE . 'user/photo_' . $user_id . '.jpg');
                            if (file_exists(FCPATH . DIR_IMAGE . 'user/photo_' . $user_id . '.jpg')) {
                                $this->customer_model->update($user_id, array('photo' => 'user/photo_' . $user_id . '.jpg'));
                            }

                            $this->customer->login($user_info->username, $password, true, true);

                            $this->redirect();
                        }
                    }
                }
            } else {
                redirect($this->instagram->getAuthUrl());
            }
        } else {
            $this->redirect();
        }
    }

    /**
     * Redirect
     *
     * @access private
     * @return void
     */
    private function redirect()
    {
        $redirect = PATH_USER;

        if ($this->session->userdata('redirect')) {
            $redirect = $this->session->userdata('redirect');
            $this->session->unset_userdata('redirect');
        }

        redirect($redirect);
    }
}