<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->layout('default');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('customer_model');
        $this->lang->load('customer/reset');

        $this->form_validation->CI =& $this;
    }

    /**
     * Request form
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $json = array();

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->form_validation
                ->set_rules('telephone', 'lang:entry_telephone', 'trim|required|callback__check_telephone')
                ->set_rules('captcha', 'lang:entry_captcha', 'trim|required|callback__check_captcha')
                ->set_error_delimiters('', '');

            if ($this->form_validation->run() == true) {
                if ($customer = $this->customer_model->get_by(array('telephone' => $this->input->post('telephone'), 'active' => 1))) {
                    $this->load->helper('string');
                    $this->load->library('sms');
                    $this->lang->load('customer_sms');

                    $code = random_string('numeric', 6);
                    $date = time() + 3600;

                    $message = sprintf(lang('sms_reset_password'), $code, date('d/m/Y H:i', $date));

                    if ($this->sms->send($customer['telephone'], $message)) {
                        $this->load->model('verification_model');

                        $verification = array(
                            'customer_id' => (int)$customer['customer_id'],
                            'date_expired' => date('Y-m-d H:i:s', $date),
                            'telephone' => $customer['telephone'],
                            'code' => $code,
                            'type' => 'reset_password',
                        );

                        $this->verification_model->insert($verification);

                        $forgotten_code = $this->customer_model->request_reset_password($customer['email']);

                        $json['redirect'] = site_url('customer/reset/change_password/' . $forgotten_code);
                    } else {
                        $json['warning'] = lang('error_system');
                    }
                } else {
                    $json['warning'] = lang('error_telephone');
                }
            } else {
                $json['warning'] = validation_errors();
            }

            $this->output->set_output(json_encode($json));
        } else {
            if ($this->session->flashdata('message')) {
                $data['message'] = $this->session->flashdata('message');
            } else {
                $data['message'] = false;
            }

            $data['text_login'] = sprintf(lang('text_login'), site_url('customer/login'));

            $this->load->view('reset', $data);
        }
    }

    /**
     * Check telephone callback
     *
     * @access public
     * @param string $telephone
     * @return bool
     */
    public function _check_telephone($telephone)
    {
        if (!$this->customer_model->check_telephone($telephone)) {
            $this->form_validation->set_message('_check_telephone', lang('error_telephone_not_found'));
            return false;
        }

        return true;
    }

    /**
     * Check captcha
     *
     * @access public
     * @param string $word
     * @return bool
     */
    public function _check_captcha($word)
    {
        $captcha = $this->session->userdata('captcha');

        if (isset($captcha['word'])) {
            if ($captcha['word'] == $word) {
                return true;
            } else {
                $this->form_validation->set_message('_check_captcha', lang('error_captcha'));
                return false;
            }
        } else {
            $this->form_validation->set_message('_check_captcha', lang('error_captcha'));
            return false;
        }
    }

    /**
     * Check verification code
     *
     * @access public
     * @param string $code
     * @return bool
     */
    public function _check_code($code)
    {
        $this->load->model('verification_model');

        $param = array(
            'code' => $code,
            'type' => 'reset_password',
            'customer_id' => $this->customer_id
        );

        if ($verification = $this->verification_model->get_by($param)) {
            if (strtotime($verification['date_expired']) > strtotime(date('Y-d-m H:i:s', time()))) {
                return true;
            } else {
                $this->form_validation->set_message('_check_code', lang('error_code_expired'));
                return false;
            }
        } else {
            $this->form_validation->set_message('_check_code', lang('error_code_invalid'));
            return false;
        }
    }

    /**
     * Change reset password
     *
     * @access public
     * @param string $code
     * @return void
     */
    public function change_password($code = null)
    {
        if (is_null($code)) {
            show_404();
        }

        $json = array();

        $customer = $this->db->where('code_forgotten', $code)->get('customer')->row_array();

        if ($customer) {
            $this->customer_id = $customer['customer_id'];

            $this->form_validation
                ->set_rules('code', 'lang:entry_code', 'trim|required|callback__check_code')
                ->set_rules('password', 'lang:entry_new_password', 'trim|required|min_length[8]')
                ->set_error_delimiters('', '');

            if ($this->form_validation->run() == true) {
                $new_password = $this->input->post('password');
                $old_password = $this->customer_model->reset_password($code);

                $this->customer_model->change_password($customer['customer_id'], $old_password, $new_password);

                $this->load->model('verification_model');
                $this->verification_model->delete_by(array('customer_id' => $customer['customer_id'], 'type' => 'reset_password'));

                if ($this->customer->login($customer['email'], $new_password)) {
                    $redirect = customer_url();
                } else {
                    $redirect = site_url('customer/login');
                }

                if ($this->input->is_ajax_request()) {
                    $json['redirect'] = $redirect;
                } else {
                    redirect($redirect);
                }
            } else {
                $error = validation_errors();

                if ($this->input->is_ajax_request()) {
                    $json['warning'] = $error;
                } else {
                    $data['action'] = site_url('customer/reset/change_password/' . $code);
                    $data['warning'] = $error;

                    $this->load->view('change_password', $data);
                }
            }
        } else {
            if ($this->input->is_ajax_request()) {
                $json['error'] = 'Bad request!';
            } else {
                show_404();
            }
        }

        if ($this->input->is_ajax_request()) {
            $this->output->set_output(json_encode($json));
        }
    }
}