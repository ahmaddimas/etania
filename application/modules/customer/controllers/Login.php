<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->layout('default');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('language');
        $this->lang->load('customer/login');

        $this->form_validation->CI =& $this;
    }

    /**
     * customer login
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->form_validation
            ->set_rules('email', 'lang:entry_email', 'trim|required')
            ->set_rules('password', 'lang:entry_password', 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($this->customer->login($this->input->post('email'), $this->input->post('password'), $this->input->post('remember'))) {
                if ($this->session->userdata('redirect')) {
                    $redirect = $this->session->userdata('redirect');
                    $this->session->unset_userdata('redirect');
                } else {
                    $redirect = site_url('customer');
                }

                redirect($redirect);
            } else {
                $this->session->set_flashdata('error', lang('text_login_error'));
                redirect('customer/login');
            }
        } else {
            if (validation_errors()) {
                $data['error'] = validation_errors();
            } else {
                $data['error'] = $this->session->flashdata('error');
            }

            $data['text_register'] = sprintf(lang('text_register'), site_url('customer/register'));
            $data['text_forgotten'] = sprintf(lang('text_forgotten'), site_url('customer/reset'));
            $data['action'] = site_url('customer/login');

            $this->load
                ->breadcrumb(lang('text_home'), site_url())
                ->breadcrumb(lang('text_customer'), site_url('customer'))
                ->breadcrumb(lang('text_login'), site_url('customer/login'))
                ->view('login', $data);
        }
    }
}