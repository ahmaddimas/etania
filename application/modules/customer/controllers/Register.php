<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->layout('default');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('language');
        $this->lang->load('customer/register');

        $this->form_validation->CI =& $this;
    }

    /**
     * customer register
     *
     * @access public
     * @return void
     */
    public function index($type = null)
    {
        if ($this->customer->is_logged()) {
            redirect(customer_url());
        }

        $json = array();

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->form_validation
                ->set_rules('name', 'lang:entry_name', 'trim|required')
                ->set_rules('email', 'lang:entry_email', 'trim|required|valid_email|callback__check_email')
                ->set_rules('password', 'lang:entry_password', 'trim|required|min_length[8]')
                ->set_rules('gender', 'lang:entry_gender', 'trim|required')
                ->set_rules('telephone', 'lang:entry_telephone', 'trim|required|min_length[3]|callback__check_telephone')
                ->set_error_delimiters('', '');

            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->get_errors() as $field => $error) {
                    $json['error'][$field] = $error;
                }
            } else {
                $post = $this->input->post(null, true);

                $post['name'] = ucwords($post['name']);
                $post['ip'] = $this->input->ip_address();
                $post['dob'] = $this->input->post('dob_year') . '-' . $this->input->post('dob_month') . '-' . $this->input->post('dob_date');
                $post['type'] = 'b';
                $post['image'] = 'no_image.jpg';
                $post['customer_group_id'] = $this->config->item('customer_group_id');
                $post['active'] = 0;

                $this->load->model('customer/customer_group_model');
                $customer_group = $this->customer_group_model->get($this->config->item('customer_group_id'));

                if ($customer_group) {
                    if ($customer_group['approval']) {
                        $post['active'] = $customer_group['approval'];
                    }
                }

                $this->load->model('customer_model');

                if ($this->customer_model->create_customer($post)) {
                    $this->load->library('email');

                    $content = lang('email_success_body');
                    $find = array('{name}', '{email}', '{password}', '{login}');
                    $replace = array(
                        'name' => $post['name'],
                        'email' => $post['email'],
                        'password' => $post['password'],
                        'login' => site_url('customer/login')
                    );

                    $data['subject'] = lang('email_success_subject');
                    $data['content'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $content))));

                    $this->email->initialize();
                    $this->email->from($this->config->item('smtp_email'), $this->config->item('company'));
                    $this->email->to($post['email']);
                    $this->email->subject(lang('email_success_subject'));
                    $this->email->message($this->load->layout(false)->view('email', $data, true));
                    $this->email->send();

                    $this->session->set_flashdata('register_success', true);

                    // if ($this->session->userdata('redirect')) {
                    // 	$json['redirect'] = site_url($this->session->userdata('redirect'));
                    // } else {
                    $json['redirect'] = site_url('customer/register/success');
                    // }
                } else {
                    $json['warning'] = 'Sorry, we can\'t processing your application at this moment!';
                }
            }

            $this->output->set_output(json_encode($json));
        } else {
            $data['heading_title'] = 'Pendaftaran Akun';

            $this->lang->load('calendar');

            $data['months'] = array(
                '01' => lang('cal_january'),
                '02' => lang('cal_february'),
                '03' => lang('cal_march'),
                '04' => lang('cal_april'),
                '05' => lang('cal_may'),
                '06' => lang('cal_june'),
                '07' => lang('cal_july'),
                '08' => lang('cal_august'),
                '09' => lang('cal_september'),
                '10' => lang('cal_october'),
                '11' => lang('cal_november'),
                '12' => lang('cal_december'),
            );

            $data['type'] = $type;
            $data['action'] = site_url('customer/register/' . $type);
            $data['text_register'] = sprintf(lang('text_register'), $this->config->item('company'));
            $data['text_account_already'] = sprintf(lang('text_account_already'), site_url('customer/login'));

            $this->load
                ->breadcrumb(lang('text_home'), site_url())
                ->breadcrumb(lang('heading_title'), site_url('customer/register'))
                ->view('register', $data);
        }
    }

    /**
     * Check email callback
     *
     * @access public
     * @param string $email
     * @return bool True if email exists otherwise false
     */
    public function _check_email($email)
    {
        $this->load->model('customer_model');

        if ($this->customer_model->check_email($email)) {
            $this->form_validation->set_message('_check_email', 'Alamat email sudah digunakan!');

            return false;
        }

        return true;
    }

    /**
     * Check telephone callback
     *
     * @access public
     * @param string $telephone
     * @return bool
     */
    public function _check_telephone($telephone)
    {
        if ($this->customer_model->check_telephone($telephone)) {
            $this->form_validation->set_message('_check_telephone', 'No. handphone ini sudah digunakan!');
            return false;
        }

        return true;
    }

    /**
     * Register success
     *
     * @access public
     * @return void
     */
    public function success()
    {
        // if ($this->session->flashdata('register_success')) {
        $this->load->view('register_success');
        // } else {
        // show_404();
        // }
    }
}