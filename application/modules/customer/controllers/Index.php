<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->lang->load('index');
        $this->load->library('currency');
        $this->load->helper('format');

        $data['customer'] = $this->customer_model->get($this->customer->customer_id());
        $data['balance'] = $this->currency->format($this->customer->get_balance());

        $data['count_orders'] = $this->db
            ->select('COUNT(DISTINCT(o.order_id)) as total', false)
            ->from('order o')
            ->where('SUBSTRING_INDEX(o.date_added,"-",1) = "' . date('Y', time()) . '"', null, false)
            ->where('customer_id', $this->customer->customer_id())
            ->get()->row()->total;

        $data['upgradable'] = false;

        $this->load
            ->title(lang('heading_title'))
            ->breadcrumb(lang('text_home'), site_url())
            ->breadcrumb(lang('heading_title'), site_url('customer'))
            ->view('index', $data);
    }
}