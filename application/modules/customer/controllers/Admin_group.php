<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_group extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('customer_group_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    /**
     * Index customer groups data
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('customer_group_id, name')
                ->from('customer_group');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load
                ->title('Grup Pelanggan')
                ->view('admin/customer_group');
        }
    }

    /**
     * Create new customer group
     *
     * @access public
     * @return void
     */
    public function create()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('admin_error_create'))));
        }

        $this->load->vars(array('heading_title' => 'Add customer group'));

        $this->form();
    }

    /**
     * Edit existing customer group
     *
     * @access public
     * @param int $customer_group_id
     * @return void
     */
    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('admin_error_edit'))));
        }

        $this->load->vars(array('heading_title' => 'Edit customer group'));

        $this->form($this->input->get('customer_group_id'));
    }

    /**
     * Load customer group form
     *
     * @access private
     * @param int $customer_group_id
     * @return void
     */
    private function form($customer_group_id = null)
    {
        $this->lang->load('permission');

        $data['action'] = admin_url('customer/group/validate');
        $data['customer_group_id'] = null;
        $data['name'] = '';
        $data['approval'] = false;

        if ($customer_group = $this->customer_group_model->get($customer_group_id)) {
            $data['customer_group_id'] = (int)$customer_group['customer_group_id'];
            $data['name'] = $customer_group['name'];
            $data['approval'] = $customer_group['approval'];
        }

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(false)->view('admin/customer_group_form', $data, true)
        )));
    }

    /**
     * Validate customer group form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $json = array();

        $customer_group_id = $this->input->post('customer_group_id');

        $this->form_validation
            ->set_rules('name', 'Name', 'trim|required|min_length[3]')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            $json['error']['name'] = form_error('name');
        } else {
            $data['customer_group_id'] = (int)$customer_group_id;
            $data['name'] = $this->input->post('name');
            $data['approval'] = (int)$this->input->post('approval');

            if ($customer_group_id) {
                $this->customer_group_model->update($customer_group_id, $data);
                $json['success'] = 'Customer group has been updated!';
            } else {
                if ($this->customer_group_model->insert($data)) {
                    $json['success'] = 'Customer group has been created!';
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete customer group
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        $json = array();

        $this->load->model('customer_group_model');

        $customer_group_ids = $this->input->post('customer_group_id');

        if (!$customer_group_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->customer_group_model->delete($customer_group_ids);
            $json['success'] = 'Customer group has been deleted!';
        }

        $this->output->set_output(json_encode($json));
    }
} 