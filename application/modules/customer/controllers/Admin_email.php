<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_email extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('customer_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->CI =& $this;
    }

    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $json = array();

            $this->form_validation->set_rules('subject', 'Judul', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('body', 'Konten', 'trim|required|min_length[64]');

            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->get_errors() as $field => $error) {
                    $json['errors'][$field] = $error;
                }
            } else {
                $post = $this->input->post(null, true);

                $customers = array();

                switch ($post['to']) {
                    case 'newsletter':
                        $customers = $this->customer_model->get_all_by(array('newsletter' => 1));
                        break;

                    case 'customer_all':
                        $customers = $this->customer_model->get_all();
                        break;

                    case 'customer_group':
                        $customers = $this->customer_model->get_all_by(array('customer_group_id' => $this->input->post('customer_group_id')));
                        break;

                    case 'customer':
                        $post['customer'] = $this->input->post('customer') ? $this->input->post('customer') : array();
                        foreach ($post['customer'] as $customer_id) {
                            $customers[] = $this->customer_model->get($customer_id);
                        }
                        break;
                }

                if ($customers) {
                    $this->load->library('email');
                    $this->email->initialize();
                    $this->email->from($this->config->item('smtp_email'), $this->config->item('company'));

                    $to = array();

                    foreach ($customers as $customer) {
                        $to = $customer['email'];
                    }

                    $this->email->to($to);

                    $this->email->subject(html_entity_decode($this->input->post('subject'), ENT_QUOTES, 'UTF-8'));
                    $this->email->message(html_entity_decode($this->input->post('body'), ENT_QUOTES, 'UTF-8'));

                    if ($this->email->send()) {
                        $success = 'Email berhasil dikirim!';
                        $this->session->set_flashdata('success', $success);
                        $json['success'] = $success;
                    } else {
                        exit($this->email->print_debugger());
                    }
                } else {
                    $json['errors']['to'] = 'Tidak ada data pelanggan yang terpilih untuk kategori penerima yang ditentukan!';
                }
            }

            $this->output->set_output(json_encode($json));
        } else {
            $data['action'] = admin_url('customer/email');

            $this->load->model('customer/customer_group_model');
            $data['customer_groups'] = $this->customer_group_model->get_all();

            $this->load
                ->title('Kirim Email')
                ->js('/assets/js/bootstrap-typeahead.min.js')
                ->view('admin/email_form', $data);
        }
    }
} 