<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['customer/admin/group(:any)?'] = 'admin_group$1';
$route['customer/admin/address(:any)?'] = 'admin_address$1';
$route['customer/admin/email'] = 'admin_email';