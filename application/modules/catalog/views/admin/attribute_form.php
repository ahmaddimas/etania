<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a onclick="$('#form').submit();" class="btn btn-success"><i class="fa fa-check"></i> <?= lang('button_save') ?>
        </a>
        <a href="<?= $cancel ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?= lang('button_cancel') ?></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?php if ($error) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                        </button><?= $error ?></div>
                <?php } ?>
                <?= form_open($action, 'id="form" class="form-horizontal"') ?>
                <input type="hidden" name="attribute_group_id" value="<?= $attribute_group_id ?>">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Grup Atribut</label>
                    <div class="col-sm-6">
                        <input type="text" name="name" value="<?= $name ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Urutan</label>
                    <div class="col-sm-2">
                        <input type="text" name="sort_order" value="<?= $sort_order ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Atribut</label>
                    <div class="col-sm-10">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Nama Atribut
                                </td>
                                <th>Urutan</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $attribute_row = 0; ?>
                            <?php foreach ($attributes as $attribute) { ?>
                                <tr id="attribute-row<?= $attribute_row ?>">
                                    <td class="left"><input type="hidden"
                                                            name="attribute[<?php echo $attribute_row; ?>][attribute_id]"
                                                            value="<?= $attribute['attribute_id'] ?>"/><input
                                                type="text" name="attribute[<?php echo $attribute_row; ?>][name]"
                                                value="<?= $attribute['name'] ?>" class="form-control"/></td>
                                    <td class="right"><input type="text"
                                                             name="attribute[<?php echo $attribute_row; ?>][sort_order]"
                                                             value="<?= $attribute['sort_order'] ?>"
                                                             class="form-control"/></td>
                                    <td class="right"><a class="btn btn-small btn-danger"
                                                         onclick="$('#attribute-row<?php echo $attribute_row; ?>').remove();"
                                                         class="button"><i class="fa fa-remove"></i></a></td>
                                </tr>
                                <?php $attribute_row++; ?>
                            <?php } ?>
                            <tr id="attribute">
                                <td colspan="2"></td>
                                <td class="right"><a class="btn btn-success" onclick="addAttribute();"><i
                                                class="fa fa-plus"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var attribute_row = <?php echo $attribute_row; ?>;

    function addAttribute() {
        html = '<tr id="attribute-row' + attribute_row + '">';
        html += '	<td class="left"><input type="hidden" name="attribute[' + attribute_row + '][attribute_id]" value="" />';
        html += '		<input type="text" name="attribute[' + attribute_row + '][name]" value="" class="form-control" />';
        html += '	</td>';
        html += '	<td class="right"><input type="text" name="attribute[' + attribute_row + '][sort_order]" value="" class="form-control" /></td>';
        html += '	<td class="right"><a class="btn btn-small btn-danger" onclick="$(\'#attribute-row' + attribute_row + '\').remove();" class="button"><i class="fa fa-remove"></i></a></td>';
        html += '</tr>';

        $('#attribute').before(html);

        attribute_row++;
    }
</script>