<style type="text/css" media="screen">
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('catalog/category/create') ?>" class="btn btn-success"><i
                    class="fa fa-plus-circle"></i> <?= lang('button_add') ?></a>
        <a onclick="repair();" class="btn btn-default"><i class="fa fa-refresh"></i> <?= lang('button_refresh') ?></a>
        <a href="<?= current_url() ?>#" onclick="bulk.delete();" class="btn btn-danger"><i
                    class="fa fa-trash"></i> <?= lang('button_delete') ?></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?php if ($success) { ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="icon fa fa-check"></i> <?= $success ?></div>
                <?php } ?>
                <?= form_open(admin_url('catalog/category/delete'), 'id="bulk-action"') ?>
                <table class="table table-hover table-bordered" id="datatable" width="100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th><input type="checkbox" id="select-all"></th>
                        <th>Nama</th>
                        <th>Susunan</th>
                        <th>Status</th>
                        <th>Urutan</th>
                        <th class="text-right"></th>
                    </tr>
                    </thead>
                </table>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>
<div id="modal"></div>
<script type="text/javascript">
    function showtr(id) {
        $('#hide' + id).show();
        $('#show' + id).hide();
        $('#' + id).show();
    }


    function hidetr(id) {
        $('#show' + id).show();
        $('#hide' + id).hide();
        $('#' + id).hide();
    }

    function format(d) {
        // `d` is the original data object for the row

        //   	$.ajax({
        // 	url : "<?=admin_url('catalog/category/getchild')?>",
        // 	type : 'post',
        // 	data: "category_id="+d.category_id,
        // 	dataType: 'json',
        // 	success: function(json) {
        //   			return json;
        // 	}
        // });
        return d;
    }


    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('catalog/category')?>",
                "type": "POST",
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"orderable": false, "searchable": false, "data": "category_id"},
                {"data": "name"},
                {"data": "level", "searchable": false},
                {"data": "active"},
                {"data": "sort_order"},
                {"orderable": false, "searchable": false, "data": "category_id"},
            ],
            "sDom": 'lfr<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                $('td', row).eq(1).html('<input type="checkbox" name="category_id[]" class="checkbox" value="' + data.category_id + '"/> <span class="pull-right">' + data.category_id + '</span>');
                if (data.active === '1') {
                    $('td', row).eq(4).html('<span class="label label-success">AKTIF</span>');
                } else {
                    $('td', row).eq(4).html('<span class="label label-default">NON AKTIF</span>');
                }
                $('td', row).eq(6).html('<a href="<?=admin_url('catalog/category/edit/\'+data.category_id+\'')?>"><i class="fa fa-cog"></i> <?=lang('button_edit')?></a>').addClass('text-right');
            },
            "order": [[1, 'asc']],
        });

        $('#select-all').click(function () {
            var checkBoxes = $('.checkbox');
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });


        // Add event listener for opening and closing details
        $('#datatable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                $.ajax({
                    url: "<?=admin_url('catalog/category/getchild')?>",
                    type: 'post',
                    data: {params: row.data()},
                    dataType: 'json',
                    success: function (json) {
                        row.child(format(json)).show();
                        tr.addClass('shown');
                    }
                });

            }
        });
    });

    var bulk = {
        'active': function () {
            action({
                text: "<?=lang('text_confirm_enable')?>",
                action: 'active',
            });
        },
        'inactive': function () {
            action({
                text: "<?=lang('text_confirm_disable')?>",
                action: 'inactive',
            });
        },
        'delete': function () {
            action({
                text: "<?=lang('text_confirm_delete')?>",
                action: 'delete',
            });
        },
    };

    function action(param) {
        swal({
            title: '<?=lang('text_warning')?>',
            text: param.text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('catalog/category/\'+param.action+\'')?>',
                    type: 'post',
                    data: $('#bulk-action').serialize(),
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
        $('#select-all').prop('checked', false);
    }

    function repair() {
        $.ajax({
            url: $('base').attr('href') + 'catalog/category/repair',
            type: 'post',
            dataType: 'json',
            success: function (json) {
                if (json['success']) {
                    $('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                } else if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
                } else if (json['redirect']) {
                    window.location = json['redirect'];
                }
                refreshTable();
            }
        });
    }


</script>