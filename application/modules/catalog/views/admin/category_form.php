<div class="page-title">
    <div>
        <h1><?= $heading_title ?></h1>
    </div>
    <div class="btn-group">
        <button id="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
        <button onclick="window.location = '<?= admin_url('catalog/category') ?>';" class="btn btn-default"><i
                    class="fa fa-reply"></i> Batal
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <?= form_open($action, 'role="form" id="form" class="form-horizontal"') ?>
            <input type="hidden" name="category_id" value="<?= $category_id ?>">
            <div class="card-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab">General</a></li>
                    <li><a href="#tab-2" data-toggle="tab">Data</a></li>
                    <li><a href="#tab-4" data-toggle="tab">Konfigurasi</a></li>
                </ul>
                <div class="tab-content" style="padding-top:20px;">
                    <div id="message"></div>
                    <div class="tab-pane active" id="tab-1">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama Kategori</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" value="<?= $name ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Deskripsi</label>
                            <div class="col-sm-10">
                                <textarea name="description" class="form-control summernote"
                                          rows="5"><?= $description ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Deskripsi Meta</label>
                            <div class="col-sm-10">
                                <textarea name="meta_description" class="form-control"
                                          rows="5"><?= $meta_description ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Keyword Meta</label>
                            <div class="col-sm-10">
                                <textarea name="meta_keyword" class="form-control"
                                          rows="5"><?= $meta_keyword ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Meta Title</label>
                            <div class="col-sm-10">
                                <input type="text" name="meta_title" value="<?= $meta_title ?>" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-2">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Induk Kategori</label>
                            <div class="col-sm-10">
                                <input type="text" name="parent" value="<?= $path ?>" class="form-control"
                                       autocomplete="off">
                                <input type="hidden" name="parent_id" value="<?= $parent_id ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Image</label>
                            <div class="col-sm-3">
                                <div class="thumbnail">
                                    <a href="#" class="img-thumbnail" id="thumb-detail" data-toggle="image"><img
                                                src="<?= $thumb_detail ?>" data-placeholder="<?= $thumb_detail ?>"/></a>
                                    <input type="hidden" id="image" name="image" value="<?= $image ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Icon</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input name="menu_image" data-placement="topRight" class="form-control icp-auto"
                                           value="<?= $menu_image ?>" type="text" readonly="readonly"/>
                                    <span class="input-group-addon"><i class="fa <?= $menu_image ?>"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-4">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jumlah Kolom<br><span class="help">Jumlah kolom sub kategori</span></label>
                            <div class="col-sm-2">
                                <input type="text" name="column" value="<?= $column ?>" class="form-control"></span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Urutan</label>
                            <div class="col-sm-2">
                                <input type="text" name="sort_order" value="<?= $sort_order ?>"
                                       class="form-control"></span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Menu Utama<br><span class="help">Tampilkan pada menu utama.</span></label>
                            <div class="toggle lg col-sm-4">
                                <label style="margin-top:5px;">
                                    <?php if ($top) { ?>
                                        <input type="checkbox" name="top" value="1" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="top" value="1">
                                    <?php } ?>
                                    <span class="button-indecator"></span>
                                </label>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Aktif</label>
                            <div class="toggle lg col-sm-4">
                                <label style="margin-top:5px;">
                                    <?php if ($active) { ?>
                                        <input type="checkbox" name="active" value="1" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="active" value="1">
                                    <?php } ?>
                                    <span class="button-indecator"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/plugins/bootstrap-typeahead.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.js') ?>"></script>
<link href="<?= base_url('assets/plugins/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css') ?>"
      rel="stylesheet">
<script type="text/javascript">
    $('.icp-auto').iconpicker();
    $('input[name=\'parent\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('catalog/category/auto_complete')?>",
                data: 'filter_name=' + query + '&category_id=' + '<?=$category_id?>',
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    categories = [];
                    map = {};
                    $.each(json, function (i, category) {
                        map[category.name] = category;
                        categories.push(category.name);
                    });
                    process(categories);
                }
            });
        },
        updater: function (item) {
            $('input[name=\'parent\']').val(map[item].name);
            $('input[name=\'parent_id\']').val(map[item].category_id);

            return map[item].name;
        },
        minLength: 1
    });

    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('catalog/category')?>";
                }
            }
        });
    });
</script>