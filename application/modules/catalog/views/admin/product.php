<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('catalog/product/create') ?>" class="btn btn-success"><i
                    class="fa fa-plus-circle"></i> <?= lang('button_add') ?></a>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> <?= lang('button_refresh') ?>
        </a>
        <button class="btn btn-info" id="upload"><i class="fa fa-file-excel-o"></i> Import Excel</button>
        <a href="<?= admin_url('catalog/product/xls_export') ?>" class="btn btn-info"><i class="fa fa-file-excel-o"></i>
            Export Excel</a>
        <a href="<?= current_url() ?>#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><i
                    class="fa fa-check-square-o"></i> <?= lang('button_action') ?> <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="<?= current_url() ?>#" onclick="bulk.copy();"><?= lang('button_copy') ?></a></li>
            <li><a href="<?= current_url() ?>#" onclick="bulk.active();"><?= lang('button_enable') ?></a></li>
            <li><a href="<?= current_url() ?>#" onclick="bulk.inactive();"><?= lang('button_disable') ?></a></li>
            <li><a href="<?= current_url() ?>#" onclick="bulk.delete();"><?= lang('button_delete') ?></a></li>
        </ul>
    </div>
</div>
<section class="content">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="message"></div>
                    <?php if ($success) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="icon fa fa-check"></i> <?= $success ?></div>
                    <?php } ?>
                    <?php if ($message) { ?>
                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button><?= $message ?></div>
                    <?php } ?>
                    <?php if ($error) { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="icon fa fa-ban"></i> <?= $error ?></div>
                    <?php } ?>
                    <?= form_open(admin_url('catalog/product/delete'), 'id="bulk-action"') ?>
                    <table class="table table-hover table-bordered" id="datatable" width="100%">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="select-all"></th>
                            <th>ID</th>
                            <th>Nama Produk</th>
                            <th>Model</th>
                            <th>Stock</th>
                            <th>Kategori</th>
                            <th>Status</th>
                            <th class="text-right"></th>
                        </tr>
                        </thead>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="modal"></div>
<script src="<?= base_url('assets/js/ajaxupload.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('catalog/product')?>",
                "type": "POST",
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "product_id"},
                {"data": "product_id"},
                {"data": "name"},
                {"data": "model"},
                {"data": "quantity"},
                {"data": "category"},
                {"data": "active"},
                {"orderable": false, "searchable": false, "data": "product_id"},
            ],
            "sDom": 'lfr<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<input type="checkbox" class="checkbox" name="product_id[]" value="' + data.product_id + '"/>');

                html = '<div class="btn-group btn-group-xs">';
                if (data.active === '1') {
                    $('td', row).eq(6).html('<span class="label label-success"><?=lang('text_enabled')?></span>');
                } else {
                    $('td', row).eq(6).html('<span class="label label-default"><?=lang('text_disabled')?></span>');
                }

                $('td', row).eq(7).html('<a href="<?=admin_url('catalog/product/edit/\'+data.product_id+\'')?>"><i class="fa fa-cog"></i> <?=lang('button_edit')?></a>').addClass('text-right');
            },
            "order": [[7, 'desc']],
        });

        $('#select-all').click(function () {
            var checkBoxes = $('.checkbox');
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });
    });

    var bulk = {
        'copy': function () {
            action({
                text: "<?=lang('text_confirm_copy')?>",
                action: 'copy',
            });
        },
        'active': function () {
            action({
                text: "<?=lang('text_confirm_enable')?>",
                action: 'active',
            });
        },
        'inactive': function () {
            action({
                text: "<?=lang('text_confirm_disable')?>",
                action: 'inactive',
            });
        },
        'delete': function () {
            action({
                text: "<?=lang('text_confirm_delete')?>",
                action: 'delete',
            });
        },
    };

    function action(param) {
        swal({
            title: '<?=lang('text_warning')?>',
            text: param.text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('catalog/product/\'+param.action+\'')?>',
                    type: 'post',
                    data: $('#bulk-action').serialize(),
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
        $('#select-all').prop('checked', false);
    }

    new AjaxUpload('#upload', {
        action: "<?=admin_url('catalog/product/xls_import')?>",
        name: 'userfile',
        autoSubmit: false,
        responseType: 'json',
        onChange: function (file, extension) {
            this.setData({'token': ''});
            this.submit();
        },
        onSubmit: function (file, extension) {
            $('.alert').remove();
            $('#upload').html('<i class="fa fa-refresh fa-spin"></i> Proses restore...');
            $('#upload').attr('disabled', true);
        },
        onComplete: function (file, json) {
            if (json.success) {
                $('.wait').remove();
                $('#message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Berhasil:</strong> ' + json.success + '</div>');
                refreshTable();
            }

            if (json.error) {
                $('#message').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Kesalahan:</strong> ' + json.error + '</div>');
            }

            $('#upload').html('<i class="fa fa-file-excel-o"></i> Import Excel');
            $('#upload').attr('disabled', false);
            $('.wait').remove();
        }
    });
</script>