<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a onclick="$('#form').submit();" class="btn btn-success"><i class="fa fa-check"></i> <?= lang('button_save') ?>
        </a>
        <a href="<?= $cancel ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?= lang('button_cancel') ?></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?php if ($error) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                        </button><?= $error ?></div>
                <?php } ?>
                <?= form_open($action, 'id="form" class="form-horizontal"') ?>
                <input type="hidden" name="filter_group_id" value="<?= $filter_group_id ?>">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Grup Filter</label>
                    <div class="col-sm-6">
                        <input type="text" name="name" value="<?= $name ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Urutan</label>
                    <div class="col-sm-2">
                        <input type="text" name="sort_order" value="<?= $sort_order ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Filter</label>
                    <div class="col-sm-10">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Nama Filter
                                </td>
                                <th>Urutan</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $filter_row = 0; ?>
                            <?php foreach ($filters as $filter) { ?>
                                <tr id="filter-row<?= $filter_row ?>">
                                    <td class="left"><input type="hidden"
                                                            name="filter[<?php echo $filter_row; ?>][filter_id]"
                                                            value="<?= $filter['filter_id'] ?>"/><input type="text"
                                                                                                        name="filter[<?php echo $filter_row; ?>][name]"
                                                                                                        value="<?= $filter['name'] ?>"
                                                                                                        class="form-control"/>
                                    </td>
                                    <td class="right"><input type="text"
                                                             name="filter[<?php echo $filter_row; ?>][sort_order]"
                                                             value="<?= $filter['sort_order'] ?>" class="form-control"/>
                                    </td>
                                    <td class="right"><a class="btn btn-small btn-danger"
                                                         onclick="$('#filter-row<?php echo $filter_row; ?>').remove();"
                                                         class="button"><i class="fa fa-remove"></i></a></td>
                                </tr>
                                <?php $filter_row++; ?>
                            <?php } ?>
                            <tr id="filter">
                                <td colspan="2"></td>
                                <td class="right"><a class="btn btn-success" onclick="addFilter();"><i
                                                class="fa fa-plus"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var filter_row = <?php echo $filter_row; ?>;

    function addFilter() {
        html = '<tr id="filter-row' + filter_row + '">';
        html += '	<td class="left"><input type="hidden" name="filter[' + filter_row + '][filter_id]" value="" />';
        html += '		<input type="text" name="filter[' + filter_row + '][name]" value="" class="form-control" />';
        html += '	</td>';
        html += '	<td class="right"><input type="text" name="filter[' + filter_row + '][sort_order]" value="" class="form-control" /></td>';
        html += '	<td class="right"><a class="btn btn-small btn-danger" onclick="$(\'#filter-row' + filter_row + '\').remove();" class="button"><i class="fa fa-remove"></i></a></td>';
        html += '</tr>';

        $('#filter').before(html);

        filter_row++;
    }
</script>