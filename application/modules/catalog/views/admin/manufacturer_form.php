<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $template['title'] ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="manufacturer_id" value="<?= $manufacturer_id ?>">
            <div class="form-group">
                <label class="control-label col-sm-4">Nama</label>
                <div class=" col-sm-8">
                    <input type="text" class="form-control" name="name" value="<?= $name ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Urutan</label>
                <div class=" col-sm-2">
                    <input type="text" class="form-control" name="sort_order" value="<?= $sort_order ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Logo</label>
                <div class="col-sm-8">
                    <div class="thumbnail">
                        <a href="#" class="img-thumbnail" id="thumb" data-toggle="image"><img src="<?= $thumb ?>"
                                                                                              data-placeholder="<?= $thumb ?>"/></a>
                        <input type="hidden" id="image" name="image" value="<?= $image ?>">
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i
                        class="fa fa-ban"></i> <?= lang('button_cancel') ?></button>
            <button type="button" class="btn btn-primary" id="submit"><i
                        class="fa fa-check"></i> <?= lang('button_save') ?></button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });
</script>