<style>.additional-image {
        margin-bottom: 25px;
    }</style>
<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a onclick="window.location = '<?= admin_url('catalog/product') ?>';" class="btn btn-default"><i
                    class="fa fa-reply"></i> <?= lang('button_cancel') ?></a>
        <button id="submit" type="button" class="btn btn-primary"><i class="fa fa-check"></i> <?= lang('button_save') ?>
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <?= form_open($action, 'role="form" id="form" class="form-horizontal"') ?>
            <input type="hidden" name="product_id" value="<?= $product_id ?>">
            <div class="card-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab">Umum</a></li>
                    <li><a href="#tab-2" data-toggle="tab">Detil</a></li>
                    <li><a href="#tab-3" data-toggle="tab">Diskon</a></li>
                    <li><a href="#tab-5" data-toggle="tab">Foto</a></li>
                    <li><a href="#tab-6" data-toggle="tab">Produk Terkait</a></li>
                    <li><a href="#tab-7" data-toggle="tab">Opsi</a></li>
                    <li><a href="#tab-8" data-toggle="tab">Filter</a></li>
                    <li><a href="#tab-9" data-toggle="tab">Atribut</a></li>
                    <li><a href="#tab-10" data-toggle="tab">Custom Tab</a></li>
                    <li id="fortab-11" style="<?php if ($type == 0) echo 'display:none'; ?>"><a href="#tab-11"
                                                                                                data-toggle="tab">Digital
                            Product</a></li>
                </ul>
                <div class="tab-content" style="padding-top:20px;">
                    <div class="tab-pane active" id="tab-1">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nama Produk</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" value="<?= $name ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Brand</label>
                            <div class="col-sm-9">
                                <select name="manufacturer_id" class="form-control">
                                    <?php foreach ($manufacturers as $manufacturer) { ?>
                                        <?php if ($manufacturer['manufacturer_id'] == $manufacturer_id) { ?>
                                            <option value="<?= $manufacturer['manufacturer_id'] ?>"
                                                    selected="selected"><?= $manufacturer['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $manufacturer['manufacturer_id'] ?>"><?= $manufacturer['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Model</label>
                            <div class="col-sm-9">
                                <input type="text" name="model" value="<?= $model ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">SKU</label>
                            <div class="col-sm-9">
                                <input type="text" name="sku" value="<?= $sku ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Deskripsi</label>
                            <div class="col-sm-9">
                                <textarea id="description" name="description" class="summernote form-control"
                                          rows="5"><?= $description ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Status</label>
                            <div class="toggle lg col-sm-4">
                                <label style="margin-top:5px;">
                                    <?php if ($active) { ?>
                                        <input type="checkbox" name="active" value="1" checked="checked">
                                    <?php } else { ?>
                                        <input type="checkbox" name="active" value="1">
                                    <?php } ?>
                                    <span class="button-indecator"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-2">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kategori Default</label>
                            <div class="col-sm-9">
                                <input type="text" name="category" value="<?= $category ?>" class="form-control"
                                       autocomplete="off" placeholder="Auto complete...">
                                <input type="hidden" name="category_id" value="<?= $category_id ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kategori Tambahan</label>
                            <div class="col-sm-9">
                                <div class="controls"
                                     style="border: 1px solid #ddd; height: 200px; background: #fff; overflow-y: scroll;">
                                    <table class="table table-striped" width="100%">
                                        <tbody>
                                        <?php foreach ($categories as $category) { ?>
                                            <tr>
                                                <td>
                                                    <?php if (in_array($category['category_id'], $product_categories)) { ?>
                                                        <input type="checkbox" name="product_category[]"
                                                               value="<?= $category['category_id'] ?>"
                                                               checked="checked">
                                                    <?php } else { ?>
                                                        <input type="checkbox" name="product_category[]"
                                                               value="<?= $category['category_id'] ?>">
                                                    <?php } ?>
                                                </td>
                                                <td><?= $category['name'] ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Harga</label>
                            <div class="col-sm-3">
                                <input type="text" name="price" value="<?= $price ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Berat</label>
                            <div class="col-sm-3">
                                <input type="text" name="weight" value="<?= $weight ?>" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <select name="weight_class_id" class="form-control">
                                    <?php foreach ($weight_classes as $weight_class) { ?>
                                        <?php if ($weight_class['weight_class_id'] == $weight_class_id) { ?>
                                            <option value="<?= $weight_class['weight_class_id'] ?>"
                                                    selected="selected"><?= $weight_class['title'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $weight_class['weight_class_id'] ?>"><?= $weight_class['title'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dimensi (P x L x T)</label>
                            <div class="col-sm-2">
                                <input type="text" name="length" value="<?= $length ?>" class="form-control"
                                       placeholder="Panjang">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="width" value="<?= $width ?>" class="form-control"
                                       placeholder="Lebar">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="height" value="<?= $height ?>" class="form-control"
                                       placeholder="Tinggi">
                            </div>
                            <div class="col-sm-3">
                                <select name="length_class_id" class="form-control">
                                    <?php foreach ($length_classes as $length_class) { ?>
                                        <?php if ($length_class['length_class_id'] == $length_class_id) { ?>
                                            <option value="<?= $length_class['length_class_id'] ?>"
                                                    selected="selected"><?= $length_class['title'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $length_class['length_class_id'] ?>"><?= $length_class['title'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <?php
                        $addon_digital = $this->config->item('addon_digital');
                        if (!empty($addon_digital['active'])) {
                            ?>
                            <div class="form-group" <?php if ($product_id <> "") echo " " ?>> <!--style=display:none -->
                                <label class="col-sm-3 control-label">Tipe Product</label>
                                <div class="col-sm-3">
                                    <select name="type" id="type" class="form-control"
                                            onchange="changetype()" <?php if ($type <> "") echo ""; ?>> <!--disabled -->
                                        <?php foreach ($product_type as $key => $val) { ?>
                                            <?php if ($key == $type) { ?>
                                                <option value="<?= $key ?>" selected="selected"><?= $val ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $key ?>"><?= $val ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>

                                </div>
                            </div>
                        <?php } ?>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Jumlah Stok</label>
                            <div class="col-sm-3">
                                <input type="text" id="quantity" name="quantity" value="<?= $quantity ?>"
                                       class="form-control" <?php if ($type == 1) echo 'disabled'; ?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status Stok Habis</label>
                            <div class="col-sm-3">
                                <select name="stock_status_id" class="form-control">
                                    <?php foreach ($stock_statuses as $stock_status) { ?>
                                        <?php if ($stock_status['stock_status_id'] == $stock_status_id) { ?>
                                            <option value="<?= $stock_status['stock_status_id'] ?>"
                                                    selected="selected"><?= $stock_status['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $stock_status['stock_status_id'] ?>"><?= $stock_status['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-3">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-right">Qty</th>
                                <th class="text-right">Prioritas</th>
                                <th class="text-right">Harga</th>
                                <th>Mulai</th>
                                <th>Berakhir</th>
                                <th class="text-right" style="width:1%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $discount_row = 0; ?>
                            <?php foreach ($discounts as $discount) { ?>
                                <tr id="discount-row<?= $discount_row ?>">
                                    <td><input type="text" name="product_discount[<?= $discount_row ?>][quantity]"
                                               value="<?= $discount['quantity'] ?>" class="form-control text-right">
                                    </td>
                                    <td><input type="text" name="product_discount[<?= $discount_row ?>][priority]"
                                               value="<?= $discount['priority'] ?>" class="form-control text-right">
                                    </td>
                                    <td><input type="text" name="product_discount[<?= $discount_row ?>][price]"
                                               value="<?= $discount['price'] ?>" class="form-control text-right"></td>
                                    <td><input type="text" name="product_discount[<?= $discount_row ?>][date_start]"
                                               value="<?= $discount['date_start'] ?>" class="form-control"
                                               id="dps<?= $discount_row ?>" data-date-format="dd-mm-yyyy"></td>
                                    <td><input type="text" name="product_discount[<?= $discount_row ?>][date_end]"
                                               value="<?= $discount['date_end'] ?>" class="form-control"
                                               id="dpe<?= $discount_row ?>" data-date-format="dd-mm-yyyy"></td>
                                    <td class="text-right"><a onclick="$('#discount-row<?= $discount_row ?>').remove();"
                                                              class="btn btn-danger"><i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                                <script type="text/javascript">
                                    $('#dps<?=$discount_row?>').datepicker({language: 'id', autoclose: true});
                                    $('#dpe<?=$discount_row?>').datepicker({language: 'id', autoclose: true});
                                </script>
                                <?php $discount_row++; ?>
                            <?php } ?>
                            <tr id="discount-add">
                                <td colspan="5"></td>
                                <td class="text-right"><a onclick="addDiscount();" class="btn btn-success"><i
                                                class="fa fa-plus"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab-5">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Foto Utama</label>
                            <div class="col-sm-9">
                                <div class="col-xs-6 col-md-3">
                                    <div class="thumbnail">
                                        <a href="#" id="thumb-image" data-toggle="image"><img src="<?= $thumb ?>"
                                                                                              id="thumb"
                                                                                              data-placeholder="<?= $no_image ?>"/></a>
                                        <input type="hidden" id="image" name="image" value="<?= $image ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Foto Tambahan</label>
                            <div class="col-sm-9">
                                <?php $image_row = 0; ?>
                                <?php foreach ($images as $image) { ?>
                                    <div class="col-xs-6 col-md-3 additional-image">
                                        <div class="thumbnail">
                                            <a href="#" id="thumb-image<?= $image_row ?>" data-toggle="image"><img
                                                        src="<?= $image['thumb'] ?>" alt="" title=""
                                                        data-placeholder="<?= $no_image ?>"/></a>
                                            <input type="hidden" name="product_image[<?= $image_row ?>][image]"
                                                   value="<?= $image['image'] ?>" id="input-image<?= $image_row ?>"/>
                                        </div>
                                    </div>
                                    <?php $image_row++; ?>
                                <?php } ?>
                                <div class="col-xs-6 col-md-3" id="image-add">
                                    <a onclick="addImage();"
                                       class="btn btn-success btn-small"><?= lang('button_image_add') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Produk Terkait</label>
                            <div class="col-sm-10">
                                <input type="text" name="product" value="" class="form-control" autocomplete="off"
                                       placeholder="Ketikkan nama produk atau ID produk...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <div class="controls"
                                     style="border: 1px solid #ddd; height: 200px; background: #fff; overflow-y: scroll;">
                                    <table class="table table-striped" width="100%">
                                        <tbody id="product-related">
                                        <?php foreach ($related as $product) { ?>
                                            <tr id="product-related<?= $product['product_id'] ?>">
                                                <td><?= $product['name'] ?><input type="hidden" name="product_related[]"
                                                                                  value="<?= $product['product_id'] ?>">
                                                </td>
                                                <td class="text-right"><a class="btn btn-danger btn-xs btn-flat"
                                                                          onclick="$('#product-related<?= $product['product_id'] ?>').remove();"><i
                                                                class="fa fa-remove"></i></a></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-7">
                        <div class="row">
                            <div class="col-sm-3">
                                <ul class="nav nav-pills nav-stacked" id="option">
                                    <?php $option_row = 0; ?>
                                    <?php foreach ($product_options as $product_option) { ?>
                                        <li><a href="#tab-7<?= $option_row ?>" data-toggle="tab"><i
                                                        class="fa fa-minus-circle"
                                                        onclick="$('a[href=\'#tab-7<?= $option_row ?>\']').parent().remove(); $('#tab-7<?= $option_row ?>').remove(); $('#option a:first').tab('show');"></i> <?= $product_option['name'] ?>
                                            </a></li>
                                        <?php $option_row++; ?>
                                    <?php } ?>
                                    <li>
                                        <input type="text" name="option" value="" placeholder="Ketik opsi..."
                                               id="input-option" class="form-control"/>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-9">
                                <div class="tab-content">
                                    <?php $option_row = 0; ?>
                                    <?php $option_value_row = 0; ?>
                                    <?php foreach ($product_options as $product_option) { ?>
                                        <div class="tab-pane" id="tab-7<?= $option_row ?>">
                                            <input type="hidden"
                                                   name="product_option[<?= $option_row ?>][product_option_id]"
                                                   value="<?= $product_option['product_option_id'] ?>"/>
                                            <input type="hidden" name="product_option[<?= $option_row ?>][name]"
                                                   value="<?= $product_option['name'] ?>"/>
                                            <input type="hidden" name="product_option[<?= $option_row ?>][option_id]"
                                                   value="<?= $product_option['option_id'] ?>"/>
                                            <input type="hidden" name="product_option[<?= $option_row ?>][type]"
                                                   value="<?= $product_option['type'] ?>"/>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"
                                                       for="input-required<?= $option_row ?>">Opsi Wajib</label>
                                                <div class="col-sm-2">
                                                    <select name="product_option[<?= $option_row ?>][required]"
                                                            id="input-required<?= $option_row ?>" class="form-control">
                                                        <?php if ($product_option['required']) { ?>
                                                            <option value="1"
                                                                    selected="selected"><?= lang('text_yes') ?></option>
                                                            <option value="0"><?= lang('text_no') ?></option>
                                                        <?php } else { ?>
                                                            <option value="1"><?= lang('text_yes') ?></option>
                                                            <option value="0"
                                                                    selected="selected"><?= lang('text_no') ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table id="option-value<?= $option_row ?>"
                                                       class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-left">Pilihan</th>
                                                        <th class="text-right">Stok</th>
                                                        <th class="text-left">Kurangi Stok</th>
                                                        <th class="text-right">Harga</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($product_option['product_option_value'] as $product_option_value) { ?>
                                                        <tr id="option-value-row<?= $option_value_row ?>">
                                                            <td class="text-left">
                                                                <select name="product_option[<?= $option_row ?>][product_option_value][<?= $option_value_row ?>][option_value_id]"
                                                                        class="form-control">
                                                                    <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                                                        <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                                                            <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
                                                                                <option value="<?= $option_value['option_value_id'] ?>"
                                                                                        selected="selected"><?= $option_value['name'] ?></option>
                                                                            <?php } else { ?>
                                                                                <option value="<?= $option_value['option_value_id'] ?>"><?= $option_value['name'] ?></option>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </select>
                                                                <input type="hidden"
                                                                       name="product_option[<?= $option_row ?>][product_option_value][<?= $option_value_row ?>][product_option_value_id]"
                                                                       value="<?= $product_option_value['product_option_value_id'] ?>"/>
                                                            </td>
                                                            <td class="text-right"><input type="text"
                                                                                          name="product_option[<?= $option_row ?>][product_option_value][<?= $option_value_row ?>][quantity]"
                                                                                          value="<?= $product_option_value['quantity'] ?>"
                                                                                          placeholder="Stok"
                                                                                          class="form-control text-right"/>
                                                            </td>
                                                            <td class="text-left">
                                                                <select name="product_option[<?= $option_row ?>][product_option_value][<?= $option_value_row ?>][subtract]"
                                                                        class="form-control">
                                                                    <?php if ($product_option_value['subtract']) { ?>
                                                                        <option value="1"
                                                                                selected="selected"><?= lang('text_yes') ?></option>
                                                                        <option value="0"><?= lang('text_no') ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="1"><?= lang('text_yes') ?></option>
                                                                        <option value="0"
                                                                                selected="selected"><?= lang('text_no') ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </td>
                                                            <td class="text-right"><input type="text"
                                                                                          name="product_option[<?= $option_row ?>][product_option_value][<?= $option_value_row ?>][price]"
                                                                                          value="<?= $product_option_value['price'] ?>"
                                                                                          placeholder="Harga"
                                                                                          class="form-control text-right"/>
                                                            </td>
                                                            <td class="text-right">
                                                                <button type="button"
                                                                        onclick="$(this).tooltip('destroy');$('#option-value-row<?= $option_value_row ?>').remove();"
                                                                        data-toggle="tooltip" title="Hapus pilihan"
                                                                        class="btn btn-danger"><i
                                                                            class="fa fa-minus-circle"></i></button>
                                                            </td>
                                                        </tr>
                                                        <?php $option_value_row++; ?>
                                                    <?php } ?>
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="4"></td>
                                                        <td class="text-right">
                                                            <button type="button"
                                                                    onclick="addOptionValue('<?= $option_row ?>');"
                                                                    data-toggle="tooltip" title="Tambah pilihan"
                                                                    class="btn btn-primary"><i
                                                                        class="fa fa-plus-circle"></i></button>
                                                        </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <select id="option-values<?= $option_row ?>" style="display: none;">
                                                <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                                    <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                                        <option value="<?= $option_value['option_value_id'] ?>"><?= $option_value['name'] ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <?php $option_row++; ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-8">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Filter</label>
                            <div class="col-sm-10">
                                <input type="text" name="filter" value="" class="form-control" autocomplete="off"
                                       placeholder="Ketikkan nama filter...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <div class="controls"
                                     style="border: 1px solid #ddd; height: 200px; background: #fff; overflow-y: scroll;">
                                    <table class="table table-striped" width="100%">
                                        <tbody id="product-filter">
                                        <?php foreach ($filters as $filter) { ?>
                                            <tr id="product-filter<?= $filter['filter_id'] ?>">
                                                <td><?= $filter['name'] ?><input type="hidden" name="product_filter[]"
                                                                                 value="<?= $filter['filter_id'] ?>">
                                                </td>
                                                <td class="text-right"><a class="btn btn-danger btn-xs btn-flat"
                                                                          onclick="$('#product-filter<?= $filter['filter_id'] ?>').remove();"><i
                                                                class="fa fa-remove"></i></a></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-9">

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tab Title</label>
                            <div class="col-sm-9">
                                <input type="text" name="tab_title" value="<?= $tab_title ?>" class="form-control">
                            </div>
                        </div>


                        <table id="attribute" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Atribut</th>
                                <th>Teks</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $attribute_row = 0; ?>
                            <?php foreach ($attributes as $attribute) { ?>
                                <tr id="attribute-row<?= $attribute_row ?>">
                                    <td><input type="text" name="product_attribute[<?= $attribute_row ?>][name]"
                                               value="<?php echo $attribute['name']; ?>" class="form-control"
                                               placeholder="Ketikkan nama atribut..."/><input type="hidden"
                                                                                              name="product_attribute[<?= $attribute_row ?>][attribute_id]"
                                                                                              value="<?php echo $attribute['attribute_id']; ?>"/>
                                    </td>
                                    <td><input name="product_attribute[<?= $attribute_row ?>][text]"
                                               class="form-control"
                                               value="<?php echo isset($attribute['text']) ? $attribute['text'] : ''; ?>">
                                    </td>
                                    <td class="text-right">
                                        <button type="button"
                                                onclick="$(this).tooltip('destroy');$('#attribute-row<?= $attribute_row ?>').remove();"
                                                data-toggle="tooltip" title="Hapus atribut" class="btn btn-danger"><i
                                                    class="fa fa-minus-circle"></i></button>
                                    </td>
                                </tr>
                                <?php $attribute_row++; ?>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td class="text-right">
                                    <button type="button" onclick="addAttribute();" data-toggle="tooltip"
                                            title="Tambah atribut" class="btn btn-primary"><i
                                                class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab-10">
                        <?php $product_tab_row = 0; ?>
                        <div id="product-tab">
                            <?php foreach ($product_tabs as $product_tab) { ?>
                                <div class="panel panel-default" id="product-tab-<?= $product_tab_row ?>">
                                    <div class="panel-heading">
                                        <button type="button"
                                                onclick="$('#product-tab-<?= $product_tab_row ?>').remove();"
                                                class="btn btn-danger btn-remove"><i class="fa fa-trash"></i></button>
                                    </div>
                                    <div class="panel-body">
                                        <input type="hidden" name="product_tab[<?= $product_tab_row ?>][product_tab_id]"
                                               value="<?= $product_tab['product_tab_id'] ?>">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Nama Tab</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="product_tab[<?= $product_tab_row ?>][title]"
                                                       value="<?= $product_tab['title'] ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Konten</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control summernote"
                                                          name="product_tab[<?= $product_tab_row ?>][content]"><?= $product_tab['content'] ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Urutan</label>
                                            <div class="col-sm-2">
                                                <input type="text"
                                                       name="product_tab[<?= $product_tab_row ?>][sort_order]"
                                                       value="<?= $product_tab['sort_order'] ?>" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $product_tab_row++; ?>
                            <?php } ?>
                        </div>
                        <button type="button" onclick="addProductTab();" class="btn btn-success btn-remove"><i
                                    class="fa fa-plus-circle"></i></button>
                    </div>
                    <div class="tab-pane" id="tab-11">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="controls"
                                     style="border: 1px solid #ddd; height: 400px; background: #fff; overflow-y: scroll;">
                                    <table id="digital" class="table table-striped" width="100%">
                                        <thead>
                                        <tr>
                                            <th>File</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $digital_row = 0; ?>
                                        <?php foreach ($digitals as $digital) { ?>
                                            <?php if ($digital['customer_id'] == 0) { ?>
                                                <tr id="digital-row<?= $digital_row ?>">
                                                    <td><input type="hidden"
                                                               name="product_digital[<?= $digital_row ?>][product_digital_id]"
                                                               value="<?php echo isset($digital['product_digital_id']) ? $digital['product_digital_id'] : ''; ?>"/><?php echo isset($digital['file']) ? $digital['file'] : ''; ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <button type="button"
                                                                onclick="delteproductdigital(<?= $digital['product_digital_id'] ?>); $(this).tooltip('destroy');$('#digital-row<?= $digital_row ?>').remove();"
                                                                data-toggle="tooltip" title="Hapus product"
                                                                class="btn btn-xs btn-danger"><i
                                                                    class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <?php $digital_row++; ?>
                                        <?php } ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <?php if ($product_id) { ?>
                                                <td>
                                                    <div class="input-group"><input type="text" id="input-file"
                                                                                    name="file" value=""
                                                                                    class="form-control"
                                                                                    readonly="readonly"><span
                                                                class="input-group-btn"><button type="button"
                                                                                                class="btn btn-default btn-block"
                                                                                                id="upload-file">Upload File</button></span>
                                                    </div>
                                                    <br>
                                                    <blockquote>Tipe file yang di perbolehkan : txt|jpg|jpeg|png
                                                    </blockquote>
                                                </td>
                                            <?php } else { ?>
                                                <td>Silahkan simpan product terlebih dahulu untuk memulai mengupload
                                                    product digital
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="controls"
                                     style="border: 1px solid #ddd; height: 400px; background: #fff; overflow-y: scroll;">
                                    <table class="table table-striped" width="100%">
                                        <thead>
                                        <tr>
                                            <th>File Sold</th>
                                            <th>Customer</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $digital_row = 0; ?>
                                        <?php foreach ($digitalsolds as $digital) { ?>
                                            <tr id="digital-2-row<?= $digital_row ?>">
                                                <td><?php echo isset($digital['file']) ? $digital['file'] : ''; ?> </td>
                                                <td><?php echo isset($digital['name']) ? $digital['name'] : ''; ?> </td>
                                                <td class="text-right">
                                                    <button type="button"
                                                            onclick="delteproductdigital(<?= $digital['product_digital_id'] ?>); $(this).tooltip('destroy');$('#digital-2-row<?= $digital_row ?>').remove();"
                                                            data-toggle="tooltip" title="Hapus product"
                                                            class="btn btn-xs btn-danger"><i
                                                                class="fa fa-minus-circle"></i></button>
                                                </td>
                                            </tr>
                                            <?php $digital_row++; ?>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        var btn = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['errors']) {
                    alert(json['errors']);
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<small class="text-danger"><i>' + json['errors'][i] + '</i></small>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('catalog/product')?>";
                }
            }
        });
    });

    var discount_row = "<?=$discount_row?>";

    function addDiscount() {
        html = '<tr id="discount-row' + discount_row + '">';
        html += '<td><input type="text" name="product_discount[' + discount_row + '][quantity]" value=""class="form-control text-right"></td>';
        html += '<td><input type="text" name="product_discount[' + discount_row + '][priority]" value=""class="form-control text-right"></td>';
        html += '<td><input type="text" name="product_discount[' + discount_row + '][price]" value=""class="form-control text-right"></td>';
        html += '<td><input type="text" name="product_discount[' + discount_row + '][date_start]" value=""class="form-control" id="dps' + discount_row + '" data-date-format="dd-mm-yyyy"></td>';
        html += '<td><input type="text" name="product_discount[' + discount_row + '][date_end]" value=""class="form-control" id="dpe' + discount_row + '" data-date-format="dd-mm-yyyy"></td>';
        html += '<td class="text-right"><a onclick="$(\'#discount-row' + discount_row + '\').remove();" class="btn btn-danger"><i class="fa fa-remove"></i></a></td>';
        html += '</tr>';

        $('#discount-add').before(html);
        $('#dps' + discount_row).datepicker({language: 'id', autoclose: true});
        $('#dpe' + discount_row).datepicker({language: 'id', autoclose: true});

        discount_row++;
    }

    var image_row = "<?=$image_row?>";

    function addImage() {
        html = '<div class="col-xs-6 col-md-3 additional-image">';
        html += '	<div class="thumbnail">';
        html += '		<a href="#" id="thumb-image' + image_row + '"data-toggle="image"><img src="<?=$no_image?>" alt="" title="" data-placeholder="<?=$no_image?>" /></a><input type="hidden" name="product_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" />';
        html += '	</div>';
        html += '</div>';

        $('#image-add').before(html);

        image_row++;
    }

    $('#upload-images').on('click', function () {
        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="userfile" value="" multiple="multiple" /></form>');
        $('#form-upload input[name=\'userfile\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'userfile\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: "<?=admin_url('catalog/product/upload')?>",
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-images i').replaceWith('<i class="fa fa-refresh fa-spin"></i>');
                        $('#upload-images').prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-images i').replaceWith('<i class="fa fa-upload"></i>');
                        $('#upload-images').prop('disabled', false);
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            addImage(json['image'], json['thumb']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });

    $('#upload-image').on('click', function () {
        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="userfile" value="" multiple="multiple" /></form>');
        $('#form-upload input[name=\'userfile\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'userfile\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: "<?=admin_url('catalog/product/upload')?>",
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-image i').replaceWith('<i class="fa fa-refresh fa-spin"></i>');
                        $('#upload-image').prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-image i').replaceWith('<i class="fa fa-upload"></i>');
                        $('#upload-image').prop('disabled', false);
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {

                            addImage(json['image'], json['thumb']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });

    $(document).delegate('a[class="thumb-image"]', 'click', function (e) {
        e.preventDefault();

        $('.popover').popover('hide', function () {
            $('.popover').remove();
        });

        var element = this;

        $(element).popover({
            html: true,
            placement: 'right',
            trigger: 'manual',
            content: function () {
                return '<button type="button" id="delete-image" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
            }
        });

        $(element).popover('show');

        $('#delete-image').on('click', function () {
            parentElement = $(element).parent().parent();
            parentElement.remove();
        });
    });

    $('input[name=\'product\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('catalog/product/auto_complete')?>",
                data: 'filter_name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    products = [];
                    map = {};
                    $.each(json, function (i, product) {
                        map[product.name] = product;
                        products.push(product.name);
                    });
                    process(products);
                }
            });
        },
        updater: function (item) {
            $('#product-related' + map[item].product_id).remove();
            $('#product-related').append('<tr id="product-related' + map[item].product_id + '"><td>' + item + '<input type="hidden" name="product_related[]" value="' + map[item].product_id + '" /></td><td class="text-right"><a class="btn btn-danger btn-xs btn-flat" onclick="$(\'#product-related' + map[item].product_id + '\').remove();"><i class="fa fa-remove "></i></a></td></tr>');
        },
        minLength: 1
    });

    $('input[name=\'category\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('catalog/category/auto_complete')?>",
                data: 'filter_name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    categories = [];
                    map = {};
                    $.each(json, function (i, category) {
                        map[category.name] = category;
                        categories.push(category.name);
                    });
                    process(categories);
                }
            });
        },
        updater: function (item) {
            $('input[name=\'category_id\']').val(map[item].category_id);
            return item;
        },
        minLength: 1
    });

    $('input[name=\'filter\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('catalog/filter/auto_complete')?>",
                data: 'filter_name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    filters = [];
                    map = {};
                    $.each(json, function (i, filter) {
                        map[filter.name] = filter;
                        filters.push(filter.name);
                    });
                    process(filters);
                }
            });
        },
        updater: function (item) {
            $('#product-filter' + map[item].filter_id).remove();
            $('#product-filter').append('<tr id="product-filter' + map[item].filter_id + '"><td>' + item + '<input type="hidden" name="product_filter[]" value="' + map[item].filter_id + '" /></td><td class="text-right"><a class="btn btn-danger btn-xs btn-flat" onclick="$(\'#product-filter' + map[item].filter_id + '\').remove();"><i class="fa fa-remove "></i></a></td></tr>');
        },
        minLength: 1
    });

    $('#option li:first-child a').trigger('click');

    var option_row = <?=$option_row?>;

    $('input[name=\'option\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('catalog/option/auto_complete')?>",
                data: 'filter_name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    options = [];
                    map = {};
                    $.each(json, function (i, option) {
                        map[option.name] = option;
                        options.push(option.name);
                    });
                    process(options);
                }
            });
        },
        updater: function (item) {
            html = '<div class="tab-pane" id="tab-7' + option_row + '">';
            html += '<input type="hidden" name="product_option[' + option_row + '][product_option_id]" value="" />';
            html += '<input type="hidden" name="product_option[' + option_row + '][name]" value="' + map[item].name + '" />';
            html += '<input type="hidden" name="product_option[' + option_row + '][option_id]" value="' + map[item].option_id + '" />';
            html += '<input type="hidden" name="product_option[' + option_row + '][type]" value="' + map[item].type + '" />';

            html += '<div class="form-group">';
            html += '<label class="col-sm-2 control-label" for="input-required' + option_row + '">Opsi Wajib</label>';
            html += '<div class="col-sm-2">';
            html += '<select name="product_option[' + option_row + '][required]" id="input-required' + option_row + '" class="form-control">';
            html += '<option value="1"><?=lang('text_yes')?></option>';
            html += '<option value="0"><?=lang('text_no')?></option>';
            html += '</select>';
            html += '</div>';
            html += '</div>';

            html += '<div class="table-responsive">';
            html += '<table id="option-value' + option_row + '" class="table table-bordered table-striped table-hover">';
            html += '<thead>';
            html += '<tr>';
            html += '<th class="text-left">Pilihan</th>';
            html += '<th class="text-right">Stok</th>';
            html += '<th class="text-left">Kurangi Stok</th>';
            html += '<th class="text-right">Harga</th>';
            html += '<th></th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            html += '</tbody>';
            html += '<tfoot>';
            html += '<tr>';
            html += '<td colspan="4"></td>';
            html += '<td class="text-left"><button type="button" onclick="addOptionValue(' + option_row + ');" data-toggle="tooltip" title="Tambah pilihan" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>';
            html += '</tr>';
            html += '</tfoot>';
            html += '</table>';
            html += '</div>';
            html += '<select id="option-values' + option_row + '" style="display: none;">';

            for (i = 0; i < map[item].option_value.length; i++) {
                html += '<option value="' + map[item].option_value[i]['option_value_id'] + '">' + map[item].option_value[i]['name'] + '</option>';
            }

            html += '</select>';
            html += '</div>';

            $('#tab-7 .tab-content').append(html);
            $('#option > li:last-child').before('<li><a href="#tab-7' + option_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'a[href=\\\'#tab-7' + option_row + '\\\']\').parent().remove(); $(\'#tab-7' + option_row + '\').remove(); $(\'#option a:first\').tab(\'show\')"></i> ' + map[item].name + '</li>');
            $('#option a[href=\'#tab-7' + option_row + '\']').tab('show');

            $('[data-toggle=\'tooltip\']').tooltip({
                container: 'body',
                html: true
            });

            option_row++;
        },
        minLength: 1
    });

    var option_value_row = <?=$option_value_row?>;

    function addOptionValue(option_row) {
        html = '<tr id="option-value-row' + option_value_row + '">';
        html += '<td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_value_id]" class="form-control">';
        html += $('#option-values' + option_row).html();
        html += '</select><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_option_value_id]" value="" /></td>';
        html += '<td class="text-right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][quantity]" value="0" class="form-control text-right" /></td>';
        html += '<td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][subtract]" class="form-control">';
        html += '<option value="1"><?=lang('text_yes')?></option>';
        html += '<option value="0"><?=lang('text_no')?></option>';
        html += '</select></td>';
        html += '<td class="text-right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price]" value="0" class="form-control text-right" /></td>';
        html += '<td class="text-left"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#option-value-row' + option_value_row + '\').remove();" data-toggle="tooltip" rel="tooltip" title="Hapus pilihan" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#option-value' + option_row + ' tbody').append(html);
        $('[rel=tooltip]').tooltip();

        option_value_row++;
    }

    var attribute_row = <?php echo $attribute_row; ?>;

    function addAttribute() {
        html = '<tr id="attribute-row' + attribute_row + '">';
        html += '<td><input type="text" name="product_attribute[' + attribute_row + '][name]" value="" class="form-control" placeholder="Ketikkan nama atribut..."/><input type="hidden" name="product_attribute[' + attribute_row + '][attribute_id]" value=""/></td>';
        html += '<td><input name="product_attribute[' + attribute_row + '][text]" class="form-control"></td>';
        html += '<td class="text-right"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#attribute-row' + attribute_row + '\').remove();" data-toggle="tooltip" title="Hapus atribut" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#attribute tbody').append(html);

        attributeautocomplete(attribute_row);

        attribute_row++;
    }

    function attributeautocomplete(attribute_row) {
        $('input[name=\'product_attribute[' + attribute_row + '][name]\']').typeahead({
            source: function (query, process) {
                $.ajax({
                    url: "<?=admin_url('catalog/attribute/auto_complete')?>",
                    data: 'filter_name=' + query,
                    type: 'get',
                    dataType: 'json',
                    success: function (json) {
                        attributes = [];
                        map = {};
                        $.each(json, function (i, attribute) {
                            map[attribute.name] = attribute;
                            attributes.push(attribute.name);
                        });
                        process(attributes);
                    }
                });
            },
            updater: function (item) {
                $('input[name=\'product_attribute[' + attribute_row + '][name]\']').attr('value', map[item].name);
                $('input[name=\'product_attribute[' + attribute_row + '][attribute_id]\']').attr('value', map[item].attribute_id);

                return map[item].name;
            },
            minLength: 1
        });
    }

    $('#attribute tbody').each(function (index, element) {
        attributeautocomplete(index);
    });

    var product_tab_row = "<?=$product_tab_row?>";

    function addProductTab() {
        html = '<div class="panel panel-default" id="product-tab-' + product_tab_row + '">';
        html += '			<div class="panel-heading">';
        html += '				<button type="button" onclick="$(\'#product-tab-' + product_tab_row + '\').remove();" class="btn btn-danger btn-remove"><i class="fa fa-trash"></i></button>';
        html += '			</div>';
        html += '			<div class="panel-body">';
        html += '				<input type="hidden" name="product_tab[' + product_tab_row + '][product_tab_id]" value="">';
        html += '				<div class="form-group">';
        html += '					<label class="col-sm-3 control-label">Nama Tab</label>';
        html += '					<div class="col-sm-9">';
        html += '						<input type="text" name="product_tab[' + product_tab_row + '][title]" value="" class="form-control">';
        html += '					</div>';
        html += '				</div>';
        html += '				<div class="form-group">';
        html += '					<label class="col-sm-3 control-label">Konten</label>';
        html += '					<div class="col-sm-9">';
        html += '						<textarea class="form-control summernote" name="product_tab[' + product_tab_row + '][content]"></textarea>';
        html += '					</div>';
        html += '				</div>';
        html += '				<div class="form-group">';
        html += '					<label class="col-sm-3 control-label">Urutan</label>';
        html += '					<div class="col-sm-2">';
        html += '						<input type="text" name="product_tab[' + product_tab_row + '][sort_order]" value="" class="form-control">';
        html += '					</div>';
        html += '				</div>';
        html += '			</div>';
        html += '		</div>';

        $('#product-tab').append(html);
        $('.summernote').summernote({
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'image', 'video']],
                ['view', ['fullscreen', 'codeview']]
            ],
            buttons: {
                image: function () {
                    var ui = $.summernote.ui;
                    var button = ui.button({
                        contents: '<i class="fa fa-image" />',
                        tooltip: "Image Manager",
                        click: function () {
                            $(this).parents('.note-editor').find('.note-editable').focus();
                            $('#modal-image').remove();
                            $.ajax({
                                url: adminUrl + 'tool/image_manager',
                                dataType: 'html',
                                success: function (html) {
                                    $('body').append('<div id="modal-image" class="modal">' + html + '</div>');
                                    $('#modal-image').modal('show');
                                }
                            });
                        }
                    });
                    return button.render();
                }
            }
        });

        product_tab_row++;
    }


    function changetype() {
        var type = $('#type').val();
        $('#quantity').val(0);
        if (type == 1) {
            $('#quantity').attr('disabled', 'true');
            $('#fortab-11').show();
        } else {
            $('#quantity').prop('disabled', false);
            $('#fortab-11').hide();
        }

        alert("Harap simpan perubahan terlebih dahulu sebelum mengisi stock");
    }


    var digital_row = <?php echo $digital_row; ?>;

    function addDigital() {
        html = '<tr id="digital-row' + digital_row + '">';
        html += '<td><input type="hidden" name="product_digital[' + digital_row + '][product_digital_id]" value="" ><div class="input-group"><input type="text" id="input-file" name="product_digital[' + digital_row + '][file]" value="" class="form-control" readonly="readonly"><span class="input-group-btn"><button type="button" class="btn btn-default btn-block" id="upload-file">Upload File</button></span></div></td>';
        html += '<td class="text-right"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#digital-row' + digital_row + '\').remove();" data-toggle="tooltip" title="Hapus product" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#digital tbody').append(html);

        digital_row++;
    }

    function delteproductdigital(id) {
        $.ajax({
            url: "<?=admin_url('digital/product/deleteproductdigital')?>",
            type: 'post',
            dataType: 'json',
            data: 'product_digital_id=' + id,
            success: function (json) {
                location.reload();
            }
        });
    }
</script>


<script src="<?= base_url('assets/js/ajaxupload.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    new AjaxUpload('#upload-file', {
        action: "<?=admin_url('digital/product/editupload/' . $product_id);?>",
        name: 'userfile',
        autoSubmit: false,
        responseType: 'json',
        onChange: function (file, extension) {
            this.submit();
        },
        onSubmit: function (file, extension) {
            $('#upload-file').append('<span class="wait"><i class="fa fa-refresh"></i></span>');
        },
        onComplete: function (file, json) {
            if (json.success) {
                location.reload();
                $('.wait').remove();
                $('#input-file').val(json.file);
            }
            if (json.error) {
                alert(json.error);
                $('.wait').remove();
            }

            $('.loading').remove();

        }
    });

</script>