<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Pencarian';
$lang['text_refine'] = 'Perbaiki Pencarian';
$lang['text_search'] = 'Produk yang memenuhi kriteria pencarian';
$lang['text_keyword'] = 'Kata Kunci';
$lang['text_category'] = 'Semua Kategori';
$lang['text_sub_category'] = 'Cari dalam subkategori';
$lang['text_empty'] = 'Produk yang dicari tidak ada.';
$lang['text_reviews'] = 'Berdasarkan %s ulasan.';
$lang['text_compare'] = 'Komparasi Produk (%s)';
$lang['text_sort'] = 'Urut dari:';
$lang['text_default'] = 'Standar';
$lang['text_name_asc'] = 'Nama (A - Z)';
$lang['text_name_desc'] = 'Nama (Z - A)';
$lang['text_price_asc'] = 'Harga (Rendah &gt; Tinggi)';
$lang['text_price_desc'] = 'Harga (Tinggi &gt; Rendah)';
$lang['text_rating_asc'] = 'Nilai (terendah)';
$lang['text_rating_desc'] = 'Nilai (tertinggi)';
$lang['text_model_asc'] = 'Model (A - Z)';
$lang['text_model_desc'] = 'Model (Z - A)';
$lang['text_limit'] = 'Tampilkan:';
$lang['entry_search'] = 'Kriteria pencarian';
$lang['entry_description'] = 'Cari dalam deskripsi produk';