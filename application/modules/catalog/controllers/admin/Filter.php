<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filter extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('filter_model');
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('filter_group_id, name, sort_order')
                ->from('filter_group');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load->helper('form');

            $data['success'] = $this->session->flashdata('success');

            $this->load
                ->title('Filter Produk')
                ->view('admin/filter', $data);
        }
    }

    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->form();
    }

    public function edit($filter_group_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->form($filter_group_id);
    }

    private function form($filter_group_id = null)
    {
        $this->load->library('form_validation');

        $url = $this->input->get('page') ? '?page=' . $this->input->get('page') : '';

        $data['action'] = admin_url('catalog/filter/' . ($filter_group_id ? 'edit/' . $filter_group_id : 'create') . $url);
        $data['cancel'] = admin_url('catalog/filter' . $url);

        $data['filter_group_id'] = null;
        $data['name'] = '';
        $data['sort_order'] = '';
        $data['filters'] = array();

        if ($filter_group = $this->filter_model->get_filter_group($filter_group_id)) {
            $data['filter_group_id'] = $filter_group['filter_group_id'];
            $data['name'] = $filter_group['name'];
            $data['sort_order'] = (int)$filter_group['sort_order'];
            $data['filters'] = $this->filter_model->get_filters(array('filter_group_id' => $filter_group_id));
        }

        $this->form_validation->set_rules('name', 'Nama Group', 'trim|required');

        if ($this->input->post('filter')) {
            foreach ($this->input->post('filter') as $key => $filter) {
                $this->form_validation->set_rules('filter[' . $key . '][name]', 'Nama Filter', 'trim|required');
            }
        }

        $this->form_validation->set_message('required', '%s tidak boleh kosong!');

        if ($this->form_validation->run() == false) {
            $data['error'] = (validation_errors()) ? validation_errors() : false;

            $this->load
                ->title($filter_group_id ? 'Edit Filter' : 'Tambah Filter')
                ->view('admin/filter_form', $data);
        } else {
            $save['name'] = $this->input->post('name');
            $save['sort_order'] = (int)$this->input->post('sort_order');
            $save['status'] = (int)$this->input->post('status');
            $save['filter'] = $this->input->post('filter');

            if ($filter_group_id) {
                $this->session->set_flashdata('success', '<b>Berhasil : </b>Data filter telah berhasil diupdate!');
                $this->filter_model->edit_filter($filter_group_id, $save);
            } else {
                $this->session->set_flashdata('success', '<b>Berhasil : </b>Data filter telah berhasil diupdate!');
                $this->filter_model->add_filter($save);
            }

            redirect(admin_url('catalog/filter'));
        }
    }

    /**
     * Auto complete filters
     *
     * @access public
     * @return json
     */
    public function auto_complete()
    {
        $json = array();

        if ($this->input->get('filter_name')) {
            $params = array(
                'filter_name' => $this->input->get('filter_name'),
                'start' => 0,
                'limit' => 20
            );

            foreach ($this->filter_model->get_filters($params) as $filter) {
                $json[] = array(
                    'filter_id' => $filter['filter_id'],
                    'name' => $filter['group_name'] . ' > ' . $filter['name']
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete filter
     *
     * @access public
     * @return json
     */
    public function delete()
    {
        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $filter_group_ids = $this->input->post('filter_group_id') ? $this->input->post('filter_group_id') : array();

        if (!$filter_group_ids) {
            $json['error'] = 'Tidak ada item yang dipilih!';
        }

        if (empty($json['error'])) {
            $this->filter_model->delete_filter_group($filter_group_ids);
            $json['success'] = 'Filter produk telah berhasil dihapus!';
        }

        $this->output->set_output(json_encode($json));
    }
}