<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('catalog/product_model');
        $this->load->model('catalog/category_model');
        $this->load->model('catalog/manufacturer_model');

        $this->form_validation->CI =& $this;
    }

    /**
     * Index product
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('p.product_id, p.name, p.active, p.model, p.sku, c.name as category, p.quantity')
                ->join('category c', 'c.category_id = p.category_id', 'left')
                ->from('product p')
                ->edit_column('price', '$1', 'format_money(price)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');
            $data['message'] = $this->session->userdata('message');

            $this->load
                ->title('Produk')
                ->view('admin/product', $data);
        }
    }

    /**
     * Create new product
     *
     * @access public
     * @return void
     */
    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->title('Tambah Produk');

        $this->form();
    }

    /**
     * Edit product
     *
     * @access public
     * @param int $product_id
     * @return void
     */
    public function edit($product_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->title('Edit Produk');

        $this->form($product_id);
    }

    /**
     * Form product
     *
     * @access private
     * @param mixed $product_id
     * @return void
     */
    private function form($product_id = null)
    {
        $fields = $this->product_model->list_fields();

        foreach ($fields as $field) {
            $data[$field] = '';
        }

        $data['action'] = admin_url('catalog/product/validate');
        $data['discounts'] = array();
        $data['images'] = array();
        $data['related'] = array();
        $data['filters'] = array();
        $data['attributes'] = array();
        $data['digitals'] = array();
        $data['digitalsolds'] = array();
        $data['category'] = '';
        $data['product_options'] = array();
        $data['product_tabs'] = array();
        $data['tab_title'] = '';
        $data['type'] = '';
        $data['product_categories'] = array();

        $product_image = 'no_image.jpg';

        $this->load->library('image');

        if ($product_id) {
            if ($product = $this->product_model->get_by(array('product_id' => (int)$product_id))) {
                foreach ($product as $key => $value) {
                    $data[$key] = $value;
                }

                $product_image = $product['image'] ? $product['image'] : 'no_image.jpg';

                if ($category = $this->category_model->get_category($product['category_id'])) {
                    if ($category['path']) {
                        $data['category'] = $category['path'] . ' &raquo; ' . $category['name'];
                    } else {
                        $data['category'] = $category['name'];
                    }
                }

                foreach ($this->product_model->get_product_discounts($product_id) as $discount) {
                    $discount['date_start'] = ($discount['date_start'] != '0000-00-00') ? date('d-m-Y', strtotime($discount['date_start'])) : '';
                    $discount['date_end'] = ($discount['date_end'] != '0000-00-00') ? date('d-m-Y', strtotime($discount['date_end'])) : '';

                    $data['discounts'][] = $discount;
                }

                foreach ($this->product_model->get_product_images($product_id) as $image) {
                    $data_image = $image['image'] ? $image['image'] : 'no_image.jpg';
                    $image['thumb'] = $this->image->resize($data_image, 150, 150);
                    $data['images'][] = $image;
                }

                $data['attributes'] = $this->product_model->get_product_attributes($product_id);
                $addon_digital = $this->config->item('addon_digital');
                if (!empty($addon_digital['active'])) {
                    $this->load->model('digital/product_digital_model');
                    $data['digitals'] = $this->product_digital_model->get_product_digitals($product_id);
                    $data['digitalsolds'] = $this->product_digital_model->get_product_digital_solds($product_id);
                }
                $data['filters'] = $this->product_model->get_filters($product_id);
                $data['related'] = $this->product_model->get_relates($product_id);
                $data['product_tabs'] = $this->product_model->get_product_tabs($product_id);
                $data['product_categories'] = $this->product_model->get_product_categories($product_id);
                $data['tab_title'] = $product['tab_title'];
                $data['type'] = $product['type'];


                $data['product_options'] = array();

                $this->load->model('option_model');

                foreach ($this->product_model->get_product_options($product_id) as $product_option) {
                    $data['product_options'][] = $product_option;
                    $data['option_values'][$product_option['option_id']] = $this->option_model->get_option_values($product_option['option_id']);
                }
            } else {
                show_404();
            }
        }


        $data['thumb'] = $this->image->resize($product_image, 150, 150);
        $data['no_image'] = $this->image->resize('no_image.jpg', 150, 150);

        $this->load->model('system/weight_class_model');
        $this->load->model('system/length_class_model');
        $this->load->model('system/stock_status_model');

        $data['weight_classes'] = $this->weight_class_model->get_all();
        $data['length_classes'] = $this->length_class_model->get_all();
        $data['product_type'] = array(0 => 'Item', 1 => 'Digital Product');
        $data['stock_statuses'] = $this->stock_status_model->get_all();
        $data['manufacturers'] = $this->manufacturer_model->get_all();
        $data['categories'] = $this->category_model->get_categories();

        $this->load
            ->js('/assets/js/bootstrap-typeahead.min.js')
            ->view('admin/product_form', $data);
    }

    /**
     * Validate form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        $json = array();

        $this->form_validation
            ->set_rules('name', 'Nama Produk', 'trim|required')
            ->set_rules('model', 'Model', 'trim|required')
            ->set_rules('sku', 'SKU', 'trim|required|callback__check_sku')
            ->set_error_delimiters('', '');

        // if($this->input->post('type')==1){
        // 	$qty = count($this->input->post('product_digital'));
        // 	if($qty==0){
        // 		$json['errors'] = "Harap tambahkan file produk digital pada tab Digital Product";
        // 		$this->output->set_output(json_encode($json));
        // 	}
        // }

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['errors'][$field] = $error;
            }
        } else {
            $post = $this->input->post(null, false);
            $post['active'] = (bool)$this->input->post('active');

            if ($post['product_id']) {
                $this->session->set_flashdata('success', lang('success_update'));
                $this->product_model->edit_product($post['product_id'], $post);
                $json['success'] = lang('success_update');
            } else {
                $this->session->set_flashdata('success', lang('success_create'));
                $this->product_model->create_product($post);
                $json['success'] = lang('success_create');
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Callback check SKU
     *
     * @access public
     * @param string $sku
     * @return bool
     */
    public function _check_sku($sku)
    {
        if ($this->product_model->check_sku($sku, $this->input->post('product_id'))) {
            $this->form_validation->set_message('_check_sku', 'Kode SKU ini tidak tersedia!');
            return false;
        }

        return true;
    }

    public function _check_sn($sn)
    {
        if ($this->product_model->check_sku($sku, $this->input->post('product_id'))) {
            $this->form_validation->set_message('_check_sku', 'Kode SKU ini tidak tersedia!');
            return false;
        }

        return true;
    }

    public function _check_skey($skey)
    {
        if ($this->product_model->check_sku($sku, $this->input->post('product_id'))) {
            $this->form_validation->set_message('_check_sku', 'Kode SKU ini tidak tersedia!');
            return false;
        }

        return true;
    }

    /**
     * Set to active
     *
     * @access public
     * @return json
     */
    public function active()
    {
        check_ajax();

        $json = array();

        $product_ids = $this->input->post('product_id');

        if (!$this->admin_auth->has_permission('edit')) {
            $json['error'] = lang('admin_error_edit');
        }

        if (!$product_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            if (is_array($product_ids)) {
                foreach ($product_ids as $product_id) {
                    $this->product_model->update($product_id, array('active' => 1));
                }
            } else {
                $this->product_model->update($product_ids, array('active' => 1));
            }

            $json['success'] = lang('success_enable');
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Set to inactive
     *
     * @access public
     * @return json
     */
    public function inactive()
    {
        check_ajax();

        $json = array();

        $product_ids = $this->input->post('product_id');

        if (!$this->admin_auth->has_permission('edit')) {
            $json['error'] = lang('admin_error_edit');
        }

        if (!$product_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            if (is_array($product_ids)) {
                foreach ($product_ids as $product_id) {
                    $this->product_model->update($product_id, array('active' => 0));
                }
            } else {
                $this->product_model->update($product_ids, array('active' => 0));
            }

            $json['success'] = lang('success_disable');
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Copy
     *
     * @access public
     * @return json
     */
    public function copy()
    {
        check_ajax();

        $json = array();

        $product_ids = $this->input->post('product_id');

        if (!$this->admin_auth->has_permission('edit')) {
            $json['error'] = lang('admin_error_edit');
        }

        if (!$product_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            if (is_array($product_ids)) {
                foreach ($product_ids as $product_id) {
                    $this->product_model->copy_product($product_id);
                }
            } else {
                $this->product_model->copy_product($product_ids);
            }

            $json['success'] = lang('success_copy');
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return json
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $product_ids = $this->input->post('product_id');

        if (!$product_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->product_model->delete($product_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Auto complete
     *
     * @access public
     * @return json
     */
    public function auto_complete()
    {
        $json = array();

        if ($this->input->get('filter_name')) {
            $params = array(
                'filter_name' => $this->input->get('filter_name'),
                'start' => 0,
                'limit' => 20
            );

            foreach ($this->product_model->get_products_autocomplete($params) as $product) {
                $json[] = array(
                    'product_id' => $product['product_id'],
                    'name' => strip_tags(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')),
                    'sku' => $product['sku'],
                    'price' => $product['price'],
                );

            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->output->set_output(json_encode($json));
    }

    /**
     * XLS exporter
     *
     * @access public
     * @return void
     */
    public function xls_export()
    {
        if (!$this->admin_auth->has_permission('index')) {
            show_401($this->uri->uri_string());
        }

        $this->load->helper('format');

        $results = $this->db
            ->order_by('name', 'asc')
            ->get('product')
            ->result_array();

        $format = $this->input->get('format');

        $this->load->library('PHPExcel');
        $this->load->library('weight');
        $this->load->library('length');

        require_once(APPPATH . 'libraries/PHPExcel.php');
        require_once(APPPATH . 'libraries/PHPExcel/IOFactory.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Nama Produk');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Model');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'SKU');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'Harga');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'Stok');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'ID Kategori');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'Berat (' . $this->weight->get_unit($this->config->item('weight_class_id')) . ')');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'Panjang (' . $this->length->get_unit($this->config->item('length_class_id')) . ')');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 1, 'Lebar (' . $this->length->get_unit($this->config->item('length_class_id')) . ')');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 1, 'Tinggi (' . $this->length->get_unit($this->config->item('length_class_id')) . ')');

        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

        $row = 2;

        foreach ($results as $result) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $result['product_id']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $result['name']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $result['model']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $result['sku']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $result['price']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $result['quantity']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $result['category_id']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $this->weight->convert($result['weight'], $result['weight_class_id'], $this->config->item('weight_class_id')));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $this->weight->convert($result['length'], $result['length_class_id'], $this->config->item('length_class_id')));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $this->weight->convert($result['width'], $result['length_class_id'], $this->config->item('length_class_id')));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $this->weight->convert($result['height'], $result['length_class_id'], $this->config->item('length_class_id')));

            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="product-export-' . date('dmYHis') . '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save('php://output');
    }

    /**
     * Import data from XLS
     *
     * @access public
     * @return void
     */
    public function xls_import()
    {
        $json = array();

        if (!$this->admin_auth->has_permission('edit')) {
            $json['error'] = lang('admin_error_edit');
        }

        if (!isset($json['error'])) {
            $cache_path = APPPATH . 'cache/xls/';
            $xls_path = FCPATH . $cache_path;

            if (!file_exists($cache_path)) {
                @mkdir($cache_path, 0777);
            }

            $config['upload_path'] = $cache_path;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size'] = '2048';
            $config['overwrite'] = true;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $json['error'] = $this->upload->display_errors('', '');
            } else {
                $file_data = $this->upload->data();

                if (file_exists($file_data['file_path'] . $file_data['raw_name'] . $file_data['file_ext'])) {
                    $this->load->library('PHPExcel');

                    require_once(APPPATH . 'libraries/PHPExcel.php');
                    require_once(APPPATH . 'libraries/PHPExcel/IOFactory.php');

                    $objPHPExcel = PHPExcel_IOFactory::load($file_data['file_path'] . $file_data['raw_name'] . $file_data['file_ext']);

                    $products = array();

                    foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
                        $highestRow = $worksheet->getHighestRow();

                        for ($row = 2; $row <= $highestRow; $row++) {
                            $products[] = array(
                                'product_id' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
                                'name' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
                                'model' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                                'sku' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                                'price' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                                'quantity' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
                                'category_id' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                                'weight' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                                'length' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
                                'height' => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
                                'height' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
                                'weight_class_id' => $this->config->item('weight_class_id'),
                                'length_class_id' => $this->config->item('length_class_id')
                            );
                        }
                    }

                    $this->product_model->import($products);

                    unlink($file_data['file_path'] . $file_data['raw_name'] . $file_data['file_ext']);

                    $json['success'] = 'Proses import telah berhasil';
                } else {
                    system('rm -rf ' . escapeshellarg($xls_path), $return);

                    $json['error'] = 'Proses restore gagal!';
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

}