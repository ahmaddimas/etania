<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('option_model');
        $this->load->library('form_validation');
        $this->load->helper('form');

        $this->form_validation->CI =& $this;
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('option_id, type, name, sort_order')
                ->from('option')
                ->edit_column('type', '$1', 'format_option_type(type)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');

            $this->load
                ->title('Opsi Produk')
                ->view('admin/option', $data);
        }
    }

    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars(array('heading_title' => 'Tambah Opsi'));

        $this->form();
    }

    public function edit($option_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars(array('heading_title' => 'Edit Opsi'));

        $this->form($option_id);
    }

    private function form($option_id = null)
    {
        $data['action'] = admin_url('catalog/option/' . ($option_id ? 'edit/' . $option_id : 'create'));
        $data['cancel'] = admin_url('catalog/option');
        $data['option_id'] = null;
        $data['type'] = '';
        $data['name'] = '';
        $data['sort_order'] = 0;
        $data['option_values'] = array();
        $data['types'] = $this->config->item('option_types');

        if ($option = $this->option_model->get_option($option_id)) {
            $data['option_id'] = $option['option_id'];
            $data['type'] = $option['type'];
            $data['name'] = $option['name'];
            $data['sort_order'] = (int)$option['sort_order'];

            foreach ($this->option_model->get_option_values($option_id) as $option_value) {
                $data['option_values'][] = array(
                    'option_value_id' => $option_value['option_value_id'],
                    'option_id' => $option_value['option_id'],
                    'name' => $option_value['name'],
                    'sort_order' => $option_value['sort_order']
                );
            }
        }

        $this->form_validation->set_rules('name', 'Nama Opsi', 'trim|required');

        if ($this->input->post('option_value')) {
            foreach ($this->input->post('option_value') as $key => $trash) {
                $this->form_validation->set_rules('option_value[' . $key . '][name]', 'Nama Pilihan', 'trim|required');
            }
        }

        if ($this->form_validation->run() == false) {
            $data['error'] = (validation_errors()) ? validation_errors() : false;

            $this->load
                ->title($option_id ? 'Edit Opsi' : 'Tambah Opsi')
                ->view('admin/option_form', $data);
        } else {
            $save['type'] = $this->input->post('type');
            $save['name'] = $this->input->post('name');
            $save['sort_order'] = (int)$this->input->post('sort_order');
            $save['option_value'] = $this->input->post('option_value');

            if ($option_id) {
                $this->session->set_flashdata('success', 'Data opsi telah berhasil diupdate!');
                $this->option_model->edit_option($option_id, $save);
            } else {
                $this->session->set_flashdata('success', 'Data opsi telah berhasil diupdate!');
                $this->option_model->create_option($save);
            }

            redirect(admin_url('catalog/option'));
        }
    }

    /**
     * Auto complete options
     *
     * @access public
     * @return json
     */
    public function auto_complete()
    {
        $json = array();

        if ($this->input->get('filter_name')) {
            $params = array(
                'filter_name' => $this->input->get('filter_name'),
                'start' => 0,
                'limit' => 20
            );

            foreach ($this->option_model->get_options($params) as $option) {
                $option_value = $this->option_model->get_option_values($option['option_id']);

                $json[] = array(
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'type' => $option['type'],
                    'option_value' => $option_value
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return json
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $option_ids = $this->input->post('option_id');

        if (!$option_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->option_model->delete_option($option_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
}

/**
 * Format option type
 *
 * @access public
 * @param string $type
 * @return string
 */
function format_option_type($type)
{
    $ci =& get_instance();

    $types = $ci->config->item('option_types');

    if (isset($types[$type])) {
        return $types[$type];
    } else {
        return '';
    }
}