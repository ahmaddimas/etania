<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attribute extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('attribute_model');
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('attribute_group_id, name, sort_order')
                ->from('attribute_group');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->flashdata('success');

            $this->load
                ->title('Atribut Produk')
                ->view('admin/attribute', $data);
        }
    }

    /**
     * Create
     *
     * @access public
     * @return void
     */
    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->form();
    }

    /**
     * Edit
     *
     * @access public
     * @param mixed $attribute_group_id (default: null)
     * @return void
     */
    public function edit($attribute_group_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->form($attribute_group_id);
    }

    /**
     * Form
     *
     * @access private
     * @param mixed $attribute_group_id (default: null)
     * @return void
     */
    private function form($attribute_group_id = null)
    {
        $data['action'] = admin_url('catalog/attribute/' . ($attribute_group_id ? 'edit/' . $attribute_group_id : 'create'));
        $data['cancel'] = admin_url('catalog/attribute');

        $data['attribute_group_id'] = null;
        $data['name'] = '';
        $data['sort_order'] = '';
        $data['attributes'] = array();

        if ($attribute_group = $this->attribute_model->get_attribute_group($attribute_group_id)) {
            $data['attribute_group_id'] = $attribute_group['attribute_group_id'];
            $data['name'] = $attribute_group['name'];
            $data['sort_order'] = (int)$attribute_group['sort_order'];
            $data['attributes'] = $this->attribute_model->get_attributes(array('attribute_group_id' => $attribute_group_id));
        }

        $this->form_validation->set_rules('name', 'Nama Grup', 'trim|required');

        if ($this->input->post('attribute')) {
            foreach ($this->input->post('attribute') as $key => $attribute) {
                $this->form_validation->set_rules('attribute[' . $key . '][name]', 'Nama atribut', 'trim|required');
            }
        }

        if ($this->form_validation->run() == false) {
            $data['error'] = (validation_errors()) ? validation_errors() : false;

            $this->load
                ->title($attribute_group_id ? 'Edit atribut' : 'Tambah atribut')
                ->view('admin/attribute_form', $data);
        } else {
            $post['name'] = $this->input->post('name');
            $post['sort_order'] = (int)$this->input->post('sort_order');
            $post['status'] = (int)$this->input->post('status');
            $post['attribute'] = $this->input->post('attribute');

            if ($attribute_group_id) {
                $this->session->set_flashdata('success', lang('success_update'));
                $this->attribute_model->edit_attribute($attribute_group_id, $post);
            } else {
                $this->session->set_flashdata('success', lang('success_create'));
                $this->attribute_model->add_attribute($post);
            }

            redirect(admin_url('catalog/attribute'));
        }
    }

    /**
     * Auto complete attributes
     *
     * @access public
     * @return json
     */
    public function auto_complete()
    {
        $json = array();

        if ($this->input->get('filter_name')) {
            $params = array(
                'filter_name' => $this->input->get('filter_name'),
                'start' => 0,
                'limit' => 20
            );

            foreach ($this->attribute_model->get_attributes($params) as $attribute) {
                $json[] = array(
                    'attribute_id' => $attribute['attribute_id'],
                    'name' => $attribute['group_name'] . ' > ' . $attribute['name']
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete attributes
     *
     * @access public
     * @return json
     */
    public function delete()
    {
        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $attribute_group_ids = $this->input->post('attribute_group_id') ? $this->input->post('attribute_group_id') : array();

        if (!$attribute_group_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->attribute_model->delete_attribute_group($attribute_group_ids);
            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
}