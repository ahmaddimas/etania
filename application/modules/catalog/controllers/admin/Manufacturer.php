<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturer extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');

        $this->form_validation->CI =& $this;
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('manufacturer_id, name, sort_order')
                ->from('manufacturer');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load
                ->title('Brand / Manufaktur')
                ->view('admin/manufacturer');
        }
    }

    /**
     * Create new manufacturer
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Brand');

        $this->form();
    }

    /**
     * Edit existing manufacturer
     *
     * @access public
     * @param int $manufacturer_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Brand');

        $this->form($this->input->get('manufacturer_id'));
    }

    /**
     * Load manufacturer form
     *
     * @access private
     * @param int $manufacturer_id
     * @return void
     */
    private function form($manufacturer_id = null)
    {
        $this->load->model('manufacturer_model');

        $data['action'] = admin_url('catalog/manufacturer/validate');
        $data['manufacturer_id'] = null;
        $data['image'] = 'no_image.jpg';
        $data['name'] = '';
        $data['sort_order'] = '';

        if ($manufacturer = $this->manufacturer_model->get($manufacturer_id)) {
            $data['manufacturer_id'] = (int)$manufacturer['manufacturer_id'];
            $data['name'] = $manufacturer['name'];
            $data['image'] = $manufacturer['image'];
            $data['sort_order'] = (int)$manufacturer['sort_order'];
        }

        $data['thumb'] = $this->image->resize($data['image'], 150, 150);

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/manufacturer_form', $data, true, true)
        )));
    }

    /**
     * Validate manufacturer form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $manufacturer_id = $this->input->post('manufacturer_id');

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required|min_length[3]|max_length[32]')
            ->set_rules('sort_order', 'Urutan', 'trim|required|numeric')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $this->load->model('manufacturer_model');

            $data['name'] = $this->input->post('name');
            $data['sort_order'] = $this->input->post('sort_order');
            $data['image'] = $this->input->post('image');
            $data['slug'] = $this->manufacturer_model->validate_slug(strtolower(url_title($data['name'])), $manufacturer_id);

            if ($manufacturer_id) {
                $this->manufacturer_model->update($manufacturer_id, $data);

                $json['success'] = lang('success_update');
            } else {
                if ($this->manufacturer_model->insert($data)) {
                    $json['success'] = lang('success_create');
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $manufacturer_ids = $this->input->post('manufacturer_id');

        if (!$manufacturer_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('manufacturer_model');
            $this->manufacturer_model->delete($manufacturer_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
} 