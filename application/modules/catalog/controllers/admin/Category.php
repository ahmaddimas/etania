<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('category_model');
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('c2.name as name, cp.category_id as category_id, group_concat(c1.name order by cp.level separator " &raquo; ") as level, c.parent_id, c2.sort_order, c.active', false)
                ->from('category_path cp')
                ->join('category c', 'cp.path_id = c.category_id', 'left')
                ->join('category c1', 'c.category_id = c1.category_id', 'left')
                ->join('category c2', 'cp.category_id = c2.category_id', 'left')
                ->where('c2.parent_id', 0)
                ->group_by('cp.category_id');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');

            $this->load
                ->title('Kategori Produk')
                ->view('admin/category', $data);
        }
    }

    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars(array('heading_title' => 'Tambah Kategori'));

        $this->form();
    }

    public function edit($category_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars(array('heading_title' => 'Edit Kategori'));

        $this->form($category_id);
    }

    private function form($category_id = null)
    {
        if ($this->input->is_ajax_request()) {
            $json = array();

            $this->form_validation->set_rules('name', 'Nama Kategori', 'trim|required');
            $this->form_validation->set_rules('column', 'Jumlah Kolom', 'trim|numeric');
            $this->form_validation->set_rules('sort_order', 'Urutan', 'trim|numeric');

            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->get_errors() as $field => $error) {
                    $json['errors'][$field] = $error;
                }
            } else {
                $post = $this->input->post(null, true);
                $post['active'] = (bool)$this->input->post('active');

                if ($category_id) {
                    $this->category_model->edit_category($category_id, $post);
                    $json['success'] = 'Kategori telah berhasil diupdate!';
                } else {
                    $this->category_model->create_category($post);
                    $json['success'] = 'Kategori baru telah berhasil ditambahkan!';
                }
            }

            $this->output->set_output(json_encode($json));
        } else {
            $data['action'] = admin_url('catalog/category/create');
            $data['category_id'] = null;
            $data['name'] = '';
            $data['description'] = '';
            $data['meta_description'] = '';
            $data['meta_keyword'] = '';
            $data['meta_title'] = '';
            $data['slug'] = '';
            $data['image'] = '';
            $data['menu_image'] = 'fa-caret-right';
            $data['parent_id'] = 0;
            $data['top'] = 0;
            $data['column'] = 0;
            $data['sort_order'] = 0;
            $data['active'] = 0;
            $data['path'] = '';

            $image = 'no_image.jpg';
            $menu_image = 'no_image.jpg';

            $this->load->library('image');

            if ($category = $this->category_model->get_category($category_id)) {
                $data['action'] = admin_url('catalog/category/edit/' . $category_id);
                $data['category_id'] = null;
                $data['name'] = $category['name'];
                $data['description'] = $category['description'];
                $data['meta_description'] = $category['meta_description'];
                $data['meta_keyword'] = $category['meta_keyword'];
                $data['meta_title'] = $category['meta_title'];
                $data['slug'] = $category['slug'];
                $data['image'] = $category['image'];
                $data['menu_image'] = $category['menu_image'] ? $category['menu_image'] : 'fa-caret-right';
                $data['parent_id'] = (int)$category['parent_id'];
                $data['top'] = (int)$category['top'];
                $data['column'] = (int)$category['column'];
                $data['sort_order'] = (int)$category['sort_order'];
                $data['active'] = (int)$category['active'];
                $data['path'] = $category['path'];

                $image = $category['image'] ? $category['image'] : 'no_image.jpg';
                $menu_image = $category['menu_image'] ? $category['menu_image'] : 'no_image.jpg';
            }

            $data['thumb_detail'] = $this->image->resize($image, 150, 150);
            $data['thumb_menu'] = $this->image->resize($menu_image, 150, 150);
            $data['no_image'] = $this->image->resize('no_image.jpg', 150, 150);

            $this->load->view('admin/category_form', $data);
        }
    }

    public function delete()
    {
        check_ajax();

        $json = array();

        $category_id = $this->input->post('category_id');

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->category_model->delete($category_id);

            $json['success'] = 'Berhasil menghapus Kategori!';
        }

        $this->output->set_output(json_encode($json));
    }

    public function repair()
    {
        check_ajax();

        $json = array();

        $category_id = $this->input->post('category_id');

        if (!$this->admin_auth->has_permission('edit')) {
            $json['error'] = lang('admin_error_edit');
        }

        if (empty($json['error'])) {
            $this->category_model->repair_categories();

            $json['success'] = 'Berhasil memperbaiki data kategori!';
        }

        $this->output->set_output(json_encode($json));
    }

    public function auto_complete()
    {
        $json = array();

        if ($this->input->get('filter_name')) {
            $params = array(
                'filter_name' => $this->input->get('filter_name'),
                'start' => 0,
                'limit' => 20
            );

            foreach ($this->category_model->get_categories($params) as $category) {
                $json[] = array(
                    'category_id' => $category['category_id'],
                    'name' => strip_tags(html_entity_decode($category['name'], ENT_QUOTES, 'UTF-8'))
                );
            }

            $json[] = array(
                'category_id' => 0,
                'name' => 'None'
            );
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->output->set_output(json_encode($json));
    }


    public function getchild()
    {
        $category_id = $this->input->post('params[category_id]');
        $name = $this->input->post('params[name]');
        $sub = $this->category_model->get_subcategories($category_id);

        $data = '<table class="table table-hover table-bordered">';
        $subdata = "";
        foreach ($sub as $sb) {
            if ($sb['active'] == 1) {
                $stat = '<span class="label label-success">AKTIF</span>';
            } else {
                $stat = '<span class="label label-default">NON AKTIF</span>';
            }


            $sub2 = $this->category_model->get_subcategories($sb['category_id']);
            $subdata = '<tr id="' . $category_id . $sb['category_id'] . '" style=display:none><td colspan=7><table class="table table-hover table-bordered">';
            foreach ($sub2 as $sb2) {
                if ($sb2['active'] == 1) {
                    $stat = '<span class="label label-success">AKTIF</span>';
                } else {
                    $stat = '<span class="label label-default">NON AKTIF</span>';
                }

                $subdata = $subdata . '<tr>
									            <td width=11%><input type="checkbox" name="category_id[]" class="checkbox" value="' . $sb2['category_id'] . '"/><span class="pull-right">' . $sb2['category_id'] . '</span></td>
									            <td width=13%>' . $sb2['name'] . '</td>
									            <td>' . $name . ' &raquo; ' . $sb['name'] . ' &raquo; ' . $sb2['name'] . '</td>
									            <td width=13%>' . $stat . '</td>
									            <td width=13%>' . $sb2['sort_order'] . '</td>
									            <td width=11% class=text-right><a href="' . admin_url('catalog/category/edit/' . $sb2['category_id']) . '"><i class="fa fa-cog"></i> ' . lang('button_edit') . '</a></td>
									       </tr>';

            }
            $subdata = $subdata . '</table></td></tr>';


            $data = $data . '<tr>
					            <td width=5%><a id="show' . $category_id . $sb['category_id'] . '" onclick="showtr(' . $category_id . $sb['category_id'] . ')"><img src="https://datatables.net/examples/resources/details_open.png"></a> <a style="display:none" id="hide' . $category_id . $sb['category_id'] . '" onclick="hidetr(' . $category_id . $sb['category_id'] . ')"><img src="https://datatables.net/examples/resources/details_close.png"></a></td>
					            <td width=8%><input type="checkbox" name="category_id[]" class="checkbox" value="' . $sb['category_id'] . '"/>  <span class="pull-right">' . $sb['category_id'] . '</span></td>
					            <td width=13%>' . $sb['name'] . '</td>
					            <td>' . $name . ' &raquo; ' . $sb['name'] . '</td>
					            <td width=13%>' . $stat . '</td>
					            <td width=13%>' . $sb['sort_order'] . '</td>
					            <td width=11% class=text-right><a href="' . admin_url('catalog/category/edit/' . $sb['category_id']) . '"><i class="fa fa-cog"></i> ' . lang('button_edit') . '</a></td>
					       </tr>' . $subdata;
        }
        $data = $data . '</table>';

        $this->output->set_output(json_encode($data));
    }
}