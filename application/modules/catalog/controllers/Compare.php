<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compare extends Front_Controller
{
    private $product_compare = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('compare');

        $this->load->library('length');
        $this->load->library('weight');
        $this->load->library('currency');

        $this->load->model('catalog/product_model');
        $this->load->model('catalog/category_model');

        if ($this->session->userdata('compare')) {
            $this->product_compare = $this->session->userdata('compare');
        }
    }

    /**
     * Comparison
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->get('remove')) {
            $key = array_search($this->input->get('remove'), $this->product_compare);

            if ($key !== false) {
                unset($this->product_compare[$key]);
                $this->session->set_userdata('compare', $this->product_compare);
            }

            $this->session->set_flashdata('success', lang('success_remove'));
            redirect('product/compare');
        }

        $data['products'] = array();
        $data['attribute_groups'] = array();

        $pcwd = $this->config->item('image_product_compare_width');
        $pchg = $this->config->item('image_product_compare_height');

        foreach ($this->product_compare as $key => $product_id) {
            if ($product = $this->product_model->get_product($product_id)) {
                if ($product['image']) {
                    $image = $this->image->resize($product['image'], $pcwd, $pchg);
                } else {
                    $image = false;
                }

                $attribute_data = array();

                $attribute_groups = $this->product_model->get_attributes($product_id);

                foreach ($attribute_groups as $attribute_group) {
                    foreach ($attribute_group['attribute'] as $attribute) {
                        $attribute_data[$attribute['attribute_id']] = $attribute['text'];
                    }
                }

                foreach ($attribute_groups as $attribute_group) {
                    $data['attribute_groups'][$attribute_group['attribute_group_id']]['name'] = $attribute_group['name'];

                    foreach ($attribute_group['attribute'] as $attribute) {
                        $data['attribute_groups'][$attribute_group['attribute_group_id']]['attribute'][$attribute['attribute_id']]['name'] = $attribute['name'];
                    }
                }

                $data['products'][$product_id] = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'thumb' => $image,
                    'price' => $this->currency->format($product['price']),
                    'special' => $product['discount'] ? $this->currency->format($product['discount']) : false,
                    'description' => substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, 200) . '...',
                    'model' => $product['model'],
                    'availability' => ($product['quantity'] <= 0) ? $product['stock_status'] : lang('text_instock'),
                    'rating' => (int)$product['rating'],
                    'reviews' => sprintf(lang('text_reviews'), (int)$product['reviews']),
                    'weight' => $this->weight->format($product['weight'], $product['weight_class_id']),
                    'dimension' => $this->length->format($product['length'], $product['length_class_id']) . ' x ' . $this->length->format($product['width'], $product['length_class_id']) . ' x ' . $this->length->format($product['height'], $product['length_class_id']),
                    'attribute' => $attribute_data,
                    'href' => $this->get_product_link($product['category_id'], $product['slug']),
                    'remove' => site_url('product/compare') . '?remove=' . $product_id
                );
            } else {
                unset($this->product_compare[$key]);
            }
        }

        $this->load
            ->title(lang('heading_title'))
            ->breadcrumb(lang('text_home'), site_url())
            ->breadcrumb(lang('heading_title'), site_url('product/compare'))
            ->view('compare', $data);
    }

    /**
     * Add product comparison
     *
     * @access public
     * @return void
     */
    public function add()
    {
        $json = array();

        if ($product = $this->product_model->get_product($this->input->post('product_id'))) {
            if (!in_array($product['product_id'], $this->product_compare)) {
                if (count($this->product_compare) >= 4) array_shift($this->product_compare);

                $this->product_compare[] = $product['product_id'];
                $this->session->set_userdata('compare', $this->product_compare);
            }

            $json['success'] = sprintf(lang('success_add'), $this->get_product_link($product['category_id'], $product['slug']), $product['name'], site_url('product/compare'));
            $json['total'] = sprintf(lang('text_compare'), (($this->product_compare) ? count($this->product_compare) : 0));

            $this->output->set_output(json_encode($json));
        }
    }

    /**
     * Get product link
     *
     * @access private
     * @param int $category_id Category ID
     * @param string $slug Product slug
     * @return string
     */
    private function get_product_link($category_id, $slug)
    {
        $path = 'product/';

        if ($category = $this->category_model->get_category($category_id)) {
            if ($category['path_id']) {
                $category_ids = explode('-', $category['path_id']);
                foreach ($category_ids as $category_id) {
                    if ($category_path = $this->category_model->get_category($category_id)) {
                        $path .= $category_path['slug'] . '/';
                    }
                }
            }

            $path .= $category['slug'] . '/';
        }

        $path .= $slug;

        return site_url($path);
    }
}