<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Front_Controller
{
    private $limit = 12;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('currency');
        $this->load->model('catalog/product_model');
        $this->load->model('catalog/category_model');
        $this->lang->load('catalog/search');
    }

    public function index()
    {
        $products = array();

        $this->load->library('pagination');

        $search = $this->input->get('search') ? $this->input->get('search') : false;
        $term = $this->input->get('term') ? $this->input->get('term') : false;
        $category_id = $this->input->get('category_id') ? $this->input->get('category_id') : false;
        $sort = ($this->input->get('sort')) ? $this->input->get('sort') : 'sort_order';
        $order = ($this->input->get('order')) ? $this->input->get('order') : 'asc';
        $page = ($this->input->get('page')) ? $this->input->get('page') : 1;
        $filters = ($this->input->get('filter')) ? $this->input->get('filter') : array();

        $params = array(
            'search' => $search,
            'category_id' => $category_id,
            'filter' => $filters,
            'term' => $term,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->limit,
            'limit' => $this->limit
        );

        $filter_query = '';
        $filter_query .= '&term=' . $term;

        foreach ($filters as $filter_id) {
            $filter_query .= '&filter[]=' . $filter_id;
        }

        $sorts = array();
        $sorts[] = array(
            'text' => 'Default',
            'value' => 'sort_order-asc',
            'href' => site_url('search?sort=sort_order&order=asc' . $filter_query)
        );

        $sorts[] = array(
            'text' => lang('text_name_asc'),
            'value' => 'name-asc',
            'href' => site_url('search?sort=name&order=asc' . $filter_query)
        );

        $sorts[] = array(
            'text' => lang('text_name_desc'),
            'value' => 'name-desc',
            'href' => site_url('search?sort=name&order=desc' . $filter_query)
        );

        $sorts[] = array(
            'text' => lang('text_price_asc'),
            'value' => 'price-asc',
            'href' => site_url('search?sort=price&order=asc' . $filter_query)
        );

        $sorts[] = array(
            'text' => lang('text_price_desc'),
            'value' => 'price-desc',
            'href' => site_url('search?sort=price&order=desc' . $filter_query)
        );

        $sorts[] = array(
            'text' => lang('text_rating_asc'),
            'value' => 'rating-desc',
            'href' => site_url('search?sort=rating&order=desc' . $filter_query)
        );

        $sorts[] = array(
            'text' => lang('text_rating_desc'),
            'value' => 'rating-asc',
            'href' => site_url('search?sort=rating&order=asc' . $filter_query)
        );

        //addon freeshipping
        $freeshipping = 0;

        foreach ($this->product_model->get_products_search($params) as $product) {

            //addon freeshipping
            $addon_offers = $this->config->item('addon_freeshipping');
            if (!empty($addon_offers['active'])) {
                $this->load->model('freeshipping/product_free_shipping_model');
                $ct = $this->product_free_shipping_model->cek_category($product['category_id']);
                $pr = $this->product_free_shipping_model->cek_product($product['product_id']);

                if (count($ct) <> 0 OR count($pr) <> 0) {
                    $freeshipping = 1;
                } else {
                    $freeshipping = 0;
                }
            }


            $path = 'product/';

            if ($category = $this->category_model->get_category($product['category_id'])) {
                if ($category['path_id']) {
                    $category_ids = explode('-', $category['path_id']);
                    foreach ($category_ids as $category_id) {
                        if ($category_path = $this->category_model->get_category($category_id)) {
                            $path .= $category_path['slug'] . '/';
                        }
                    }
                }

                $path .= $category['slug'] . '/';
            }

            $path .= $product['slug'];

            $powd = $this->config->item('image_product_dod_width');
            $pohg = $this->config->item('image_product_dod_height');

            $products[] = array(
                'product_id' => $product['product_id'],
                'thumb' => $this->image->resize($product['image'] ? $product['image'] : 'no_image.jpg', $powd, $pohg),
                'name' => $product['name'],
                'description' => $product['description'],
                'price' => $this->currency->format($product['price']),
                'discount' => (float)$product['discount'] ? $this->currency->format($product['discount']) : false,
                'wholesaler' => ($product['wholesaler'] > 1) ? true : false,
                'discount_percent' => (float)$product['discount'] ? ceil(($product['price'] - $product['discount']) / $product['price'] * 100) . '%' : false,
                'rating' => $product['rating'],
                'reviews' => (int)$product['reviews'],
                'href' => site_url($path),
                'quickview' => site_url('catalog/product/quickview/' . $product['product_id']),
                'free_shipping' => $freeshipping
            );
        }

        $query = '';
        $query .= '?sort=' . $sort;
        $query .= '&order=' . $order;
        $query .= $filter_query;

        $config = array(
            'base_url' => site_url('search') . $query,
            'total_rows' => $this->product_model->count_products($params),
            'per_page' => $this->limit,
            'query_string_segment' => 'page',
            'page_query_string' => true,
            'full_tag_open' => '<ul class="pagination">',
            'full_tag_close' => '</ul>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><a>',
            'cur_tag_close' => '</a></li>',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'use_page_numbers' => true
        );

        $this->pagination->initialize($config);

        $data['products'] = $products;
        $data['sorts'] = $sorts;
        $data['order'] = $order;
        $data['sort'] = $sort;
        $data['page'] = $page;
        $data['pagination'] = $this->pagination->create_links();
        $data['filters'] = $filters;
        $data['compare'] = sprintf(lang('text_compare'), count($this->session->userdata('compare')));

        $this->load->model('filter_model');
        $data['filter_groups'] = $this->filter_model->get_filter_menus();

        //addon freeshipping logo
        $data['free_shipping'] = $this->config->item('addon_freeshipping');

        $this->load->helper('format');

        if ($this->input->is_ajax_request()) {
            $this->output->set_output($this->load->layout(null)->view('search', $data, true));
        } else {
            $this->load
                ->title(lang('heading_title'))
                ->breadcrumb(lang('text_home'), site_url())
                ->breadcrumb(lang('heading_title'), site_url('search'))
                ->view('search', $data);
        }
    }

    public function ajax()
    {
        check_ajax();

        $json = array();

        if ($this->input->get('query')) {
            $params = array(
                'filter_name' => $this->input->get('query'),
                'filter_description' => $this->input->get('query'),
                'start' => 0,
                'limit' => 5
            );

            foreach ($this->product_model->get_products_autocomplete($params) as $product) {
                $json[] = array(
                    'id' => $product['product_id'],
                    'name' => strip_tags(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')),
                    'href' => site_url('search?term=' . strtolower($product['name'])),
                    'category' => 'Produk',
                );
            }

            foreach ($this->category_model->get_categories($params) as $category) {
                $json[] = array(
                    'id' => $category['category_id'],
                    'name' => strip_tags(html_entity_decode($category['name'], ENT_QUOTES, 'UTF-8')),
                    'href' => site_url('category/' . $category['path']),
                    'category' => 'Kategori',
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->output->set_output(json_encode($json));
    }
}