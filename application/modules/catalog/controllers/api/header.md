## Introduction
The MedanSoft eCommerce System Restful API allows you to access and download your data as well as develop applications. Currently it’s returning response in JSON format only.

To use the API simply use the example code below with your required resource. For example:
```
https://www.diskonabis.com/catalog/api/{resource}
```

## Issuing Token
To request new access token, you will need your admin's username or email and admin's password. For example:
```
 curl -d '{"username":"myusername", "password":"mypassword"}' 
 -H "Content-Type: application/x-www-form-urlencoded" 
 -X POST https://www.diskonabis.com/catalog/api/token
```

> please note that all access tokens will be expired in 24 hours

Example for refreshing access token:
```
 curl
 -H "Accept: application/json" 
 -H "Authorization: Bearer myaccesstoken123" https://www.diskonabis.com/catalog/api/token/refresh
```