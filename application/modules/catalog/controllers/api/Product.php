<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends API_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('catalog/product_model');
        $this->load->library('form_validation');

        $this->form_validation->CI =& $this;
    }

    /**
     * @api {get} /product/{product_id} Get Product
     * @apiVersion 1.0.0
     * @apiName GetProduct
     * @apiGroup Product
     * @apiDescription Get product data. If <code>product_id</code> is ommited, list of products will be returned.
     * @apiExample {curl} cURL
     * curl
     * -H "Accept: application/json"
     * -H "Authorization: Bearer access-token-contoh-123" https://www.diskonabis.com/catalog/api/product
     *
     * @apiExample {php} PHP
     * $ch = curl_init();
     * curl_setopt($ch, CURLOPT_URL, 'https://www.diskonabis.com/catalog/api/product');
     * curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer access-token-contoh-123']);
     * $response = curl_exec($ch);
     * curl_close($ch);
     *
     * @apiParam {integer} limit Limit number of records returned. Maximum 1000 records
     * @apiParam {integer} offset Start position for fetching records
     * @apiParam {integer} order Sort order. Valid values are <code>name</code>, <code>sort_order</code>
     * @apiParam {integer} category_id Category ID. If given, list of products that filtered by this category will be returned.
     *
     * @apiSuccess {integer}    product_id Product ID
     * @apiSuccess {string} name Product name
     * @apiSuccess {string} model Product model
     * @apiSuccess {string} sku Produck SKU code
     * @apiSuccess {string}    description Product description
     * @apiSuccess {float} price Base price
     * @apiSuccess {integer} quantity Quantity of stocks
     * @apiSuccess {integer} sort_order Sort order or sequence
     * @apiSuccess {integer}    active Product status. 1 is active, 0 is inactive
     * @apiSuccess {string} stock_status Stock status
     * @apiSuccess {string} manufacturer Manufacturer/brand name
     *
     * @apiSuccessExample Success Response (Single):
     *   {
     *    "result": {
     *        "product_id": "100",
     *        "name": "HDD 500GB 5400",
     *        "model": "D945",
     *        "sku": "212312445",
     *        "description": "",
     *        "price": "1400000.00",
     *        "quantity": "300",
     *        "sort_order": "0",
     *        "active": "1",
     *        "stock_status": "Tersedia",
     *        "manufacturer": null
     *    }
     *    }
     *
     * @apiSuccessExample Success Response (Multiple):
     *   {
     *    "paging": {
     *        "offset": 0,
     *        "limit": "2",
     *        "total_records": 6
     *    },
     *    "result": [
     *        {
     *            "product_id": "100",
     *            "name": "HDD 500GB 5400",
     *            "model": "D945",
     *            "sku": "212312445",
     *            "description": "",
     *            "price": "1400000.00",
     *            "quantity": "300",
     *            "sort_order": "0",
     *            "active": "1",
     *            "stock_status": "Tersedia",
     *            "manufacturer": "Seagate"
     *        },
     *        {
     *            "product_id": "106",
     *            "name": "Motherboard Asus",
     *            "model": "M5-A78l",
     *            "sku": "2342235424",
     *            "description": "",
     *            "price": "850000.00",
     *            "quantity": "234",
     *            "sort_order": "0",
     *            "active": "1",
     *            "stock_status": "Tersedia",
     *            "manufacturer": "Asus"
     *        }
     *    ]
     *    }
     *
     * @apiErrorExample Error Response:
     *    HTTP/1.1 404 Not Found
     *   {
     *        "result": null
     *    }
     *
     */
    public function index_get($product_id = null)
    {
        $response = array();

        if ($product_id) {
            $product = $this->product_model->get_product($product_id);

            if ($product) {
                $result = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'sku' => $product['sku'],
                    'description' => $product['description'],
                    'price' => $product['price'],
                    'quantity' => $product['quantity'],
                    'sort_order' => $product['sort_order'],
                    'active' => $product['active'],
                    'stock_status' => $product['stock_status'],
                    'manufacturer' => $product['manufacturer'],
                );
            } else {
                $result = false;
            }

            $response = array(
                'result' => $result
            );
        } else {
            $limit = $this->get('limit') ? $this->get('limit') : 1000;
            $order = $this->get('order') ? $this->get('order') : 'product_id';
            $offset = $this->get('offset') ? $this->get('offset') : 0;

            $params = array(
                'category_id' => $this->get('category_id') ? $this->get('category_id') : false,
                'sort' => 'ASC',
                'order' => $order,
                'start' => $offset,
                'limit' => $limit
            );

            $result = array();

            foreach ($this->product_model->get_products($params) as $product) {
                $result[] = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'sku' => $product['sku'],
                    'description' => $product['description'],
                    'price' => $product['price'],
                    'quantity' => $product['quantity'],
                    'sort_order' => $product['sort_order'],
                    'active' => $product['active'],
                    'stock_status' => $product['stock_status'],
                    'manufacturer' => $product['manufacturer'],
                );
            }

            $total_records = $this->product_model->count_products($params);

            $response = array(
                'paging' => array(
                    'offset' => $offset,
                    'limit' => $limit,
                    'total_records' => $total_records
                ),
                'result' => $result
            );
        }

        $code = $result ? REST_Controller::HTTP_OK : REST_Controller::HTTP_NOT_FOUND;

        $this->response($response, $code);
    }

    /**
     * @api {post} /product Create Product
     * @apiVersion 1.0.0
     * @apiName CreateProduct
     * @apiGroup Product
     * @apiDescription Create new product record
     *
     * @apiParam {string} name <code>Required</code> Product name
     * @apiParam {string} model <code>Required</code> Product model
     * @apiParam {string} sku Produck SKU code
     * @apiParam {string} description Product description
     * @apiParam {float} price Base price
     * @apiParam {integer} quantity Quantity of stocks
     * @apiParam {integer} sort_order Sort order or sequence
     * @apiParam {integer} active Product status. 1 is active, 0 is inactive
     * @apiParam {mixed} category_id Category ID(s). For multiple values, add ":" as delimiter. Example: 2:3:5 etc
     * @apiParam {string} image Product image url. Must be accessible from internet/online
     *
     * @apiSuccessExample Success Response:
     *   {
     *    "status": true,
     *    "success": "Produk baru telah berhasil ditambahkan",
     *    "result": {
     *        "product_id": "136",
     *        "name": "Test Produk From API",
     *        "model": "Test model",
     *        "sku": "TEST",
     *        "description": "",
     *        "price": "150000.00",
     *        "quantity": "100",
     *        "sort_order": "0",
     *        "active": "1"
     *    }
     *    }
     *
     * @apiErrorExample Error Response:
     *   HTTP/1.1 400 Bad Request
     *   {
     *    "error": {
     *        "name": "Bidang Nama Produk dibutuhkan.",
     *        "model": "Bidang Model dibutuhkan."
     *    }
     *    }
     *
     */
    public function index_post()
    {
        $response = array();

        $this->form_validation
            ->set_rules('name', 'Nama Produk', 'trim|required')
            ->set_rules('model', 'Model', 'trim|required')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $response['error'][$field] = $error;
            }

            $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $post = $this->post();
            $post['active'] = $this->post('active');
            $post['description'] = isset($post['description']) ? $post['description'] : '';

            if (isset($post['image'])) {
                if (!file_exists(DIR_IMAGE . 'data/products')) {
                    @mkdir(DIR_IMAGE . 'data/products', 0777);
                }

                $filename = basename($post['image']);

                copy($post['image'], DIR_IMAGE . 'data/products/' . $filename);

                $post['image'] = 'data/products/' . $filename;
            }

            if (isset($post['category_id'])) {
                $categories = explode(':', $post['category_id']);

                foreach ($categories as $category_id) {
                    $post['product_category'][] = (int)$category_id;
                }

                if (isset($categories[0])) {
                    $post['category_id'] = $categories[0];
                }
            }

            $product_id = $this->product_model->create_product($post);

            if ($product_id) {
                $product = $this->product_model->get($product_id);

                $result = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'sku' => $product['sku'],
                    'description' => $product['description'],
                    'price' => $product['price'],
                    'quantity' => $product['quantity'],
                    'sort_order' => $product['sort_order'],
                    'active' => $product['active'],
                );

                $response = array(
                    'status' => true,
                    'success' => 'Produk baru telah berhasil ditambahkan',
                    'result' => $result
                );

                $this->response($response, REST_Controller::HTTP_OK);
            } else {
                $response = array(
                    'status' => false,
                    'error' => 'Failed'
                );

                $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    /**
     * @api {put} /product{product_id} Update Product
     * @apiVersion 1.0.0
     * @apiName UpdateProduct
     * @apiGroup Product
     * @apiDescription Update existing product
     *
     * @apiParam {integer} product_id <code>Required</code> Product ID
     * @apiParam {string} name Product name
     * @apiParam {string} model Product model
     * @apiParam {string} sku Produck SKU code
     * @apiParam {string} description Product description
     * @apiParam {float} price Base price
     * @apiParam {integer} quantity Quantity of stocks
     * @apiParam {integer} sort_order Sort order or sequence
     * @apiParam {integer} active Product status. 1 is active, 0 is inactive
     * @apiParam {mixed} category_id Category ID(s). For multiple values, add ":" as delimiter. Example: 2:3:5 etc
     * @apiParam {string} image Product image url. Must be accessible from internet/online
     *
     * @apiSuccessExample Success Response:
     *   {
     *    "status": true,
     *    "success": "Produk baru telah berhasil diperbarui",
     *    "result": {
     *        "product_id": "136",
     *        "name": "Test Produk From API",
     *        "model": "Test model",
     *        "sku": "TEST",
     *        "description": "",
     *        "price": "150000.00",
     *        "quantity": "100",
     *        "sort_order": "0",
     *        "active": "1"
     *    }
     *    }
     *
     */
    public function index_put($product_id = null)
    {
        if (!$product_id) {
            $this->response(array('status' => false, 'error' => 'ID product diperlukan'), REST_Controller::HTTP_BAD_REQUEST);
        }

        $response = array();

        $post = $this->put(null, true);
        $post['active'] = $this->put('active');
        $post['description'] = isset($put['description']) ? $post['description'] : '';

        if (isset($post['image'])) {
            if (!file_exists(DIR_IMAGE . 'data/products')) {
                @mkdir(DIR_IMAGE . 'data/products', 0777);
            }

            $filename = basename($post['image']);

            copy($post['image'], DIR_IMAGE . 'data/products/' . $filename);

            $post['image'] = 'data/products/' . $filename;
        }

        if (isset($post['category_id'])) {
            $categories = explode(':', $post['category_id']);

            foreach ($categories as $category_id) {
                $post['product_category'][] = (int)$category_id;
            }

            if (isset($categories[0])) {
                $post['category_id'] = $categories[0];
            }
        }

        $this->_edit_product($product_id, $this->put(null, true));

        if ($product = $this->product_model->get($product_id)) {
            $result = array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'model' => $product['model'],
                'sku' => $product['sku'],
                'description' => $product['description'],
                'price' => $product['price'],
                'quantity' => $product['quantity'],
                'sort_order' => $product['sort_order'],
                'active' => $product['active'],
            );

            $response = array(
                'status' => true,
                'success' => 'Produk telah berhasil diperbarui',
                'result' => $result
            );
        }

        $this->response($response, REST_Controller::HTTP_OK);
    }

    /**
     * @api {delete} /product/{product_id} Delete Product
     * @apiVersion 1.0.0
     * @apiName DeleteProduct
     * @apiGroup Product
     * @apiDescription Delete product
     *
     * @apiParam {integer} product_id product ID
     *
     * @apiSuccessExample Success Response:
     *   {
     *        "status": true,
     *        "success": "Berhasil menghapus produk"
     *    }
     *
     * @apiErrorExample Error Response:
     *   HTTP/1.1 400 Bad Request
     *   {
     *     "status": false,
     *     "error": "ID produk diperlukan"
     *   }
     *
     */
    public function index_delete($product_id = null)
    {
        if (!$product_id) {
            $this->response(array('status' => false, 'error' => 'ID product diperlukan'), REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $this->product_model->delete($product_id);
            $this->response(array('status' => true, 'success' => 'Berhasil menghapus produk'), REST_Controller::HTTP_OK);
        }
    }

    private function _edit_product($product_id, $data)
    {
        $this->product_model->update($product_id, $data);

        if (isset($data['category_id'])) {
            $this->db
                ->where('product_id', (int)$product_id)
                ->delete('product_category');
        }

        if (isset($data['product_category'])) $this->product_model->set_product_categories($product_id, $data['product_category']);
    }
}