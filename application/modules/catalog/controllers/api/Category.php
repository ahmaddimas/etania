<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends API_Controller
{
    /**
     * @api {get} /category/{category_id} Get Category
     * @apiVersion 1.0.0
     * @apiName GetCategory
     * @apiGroup Category
     * @apiDescription Get category data. If <code>category_id</code> is ommited, list of categories will be returned.
     * @apiExample {curl} cURL
     * curl
     * -H "Accept: application/json"
     * -H "Authorization: Bearer access-token-contoh-123" https://www.diskonabis.com/catalog/api/category
     *
     * @apiExample {php} PHP
     * $ch = curl_init();
     * curl_setopt($ch, CURLOPT_URL, 'https://www.diskonabis.com/catalog/api/category');
     * curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer access-token-contoh-123']);
     * $response = curl_exec($ch);
     * curl_close($ch);
     *
     * @apiParam {integer} limit Limit number of records returned. Maximum 1000 records
     * @apiParam {integer} offset Start position for fetching records
     * @apiParam {integer} order Sort order. Valid values are <code>name</code>, <code>sort_order</code>
     *
     * @apiSuccess {integer}    category_id Category ID
     * @apiSuccess {string} name Category name
     * @apiSuccess {integer} sort_order Sort order or sequence
     * @apiSuccess {string} path Category path
     *
     * @apiSuccessExample Success Response (Single):
     *   {
     *    "result": {
     *        "category_id": "8",
     *        "name": "SSD Storage",
     *        "sort_order": "1",
     *        "path": "komponen/hard-disk"
     *    }
     *    }
     *
     * @apiSuccessExample Success Response (Multiple):
     *   {
     *    "paging": {
     *        "offset": 0,
     *        "limit": 1000,
     *        "total_records": 8
     *    },
     *    "result": [
     *        {
     *            "category_id": "2",
     *            "path": "komponen",
     *            "name": "Komponen",
     *            "sort_order": "1"
     *        },
     *        {
     *            "category_id": "3",
     *            "path": "komponen/motherboard",
     *            "name": "Motherboard",
     *            "sort_order": "2"
     *        }
     *    ]
     *    }
     *
     * @apiErrorExample Error Response:
     *    HTTP/1.1 404 Not Found
     *   {
     *        "result": null
     *    }
     *
     */
    public function index_get($category_id = null)
    {
        $this->load->model('category_model');

        $response = array();

        if ($category_id) {
            $result = $this->db
                ->select("c.category_id, c.name, c.sort_order, (select group_concat(c1.slug order by level separator '/') from " . $this->db->dbprefix('category_path') . " cp left join " . $this->db->dbprefix('category') . " c1 on (cp.path_id = c1.category_id and cp.category_id != cp.path_id) where cp.category_id = c.category_id group by cp.category_id) as path", false)
                ->from('category c')
                ->where('c.category_id', (int)$category_id)
                ->get()
                ->row_array();

            $response = array(
                'result' => $result
            );
        } else {
            $total_records = $this->db->count_all_results('category');
            $limit = $this->get('limit') ? $this->get('limit') : 1000;
            $order = $this->get('order') ? $this->get('order') : 'name';
            $offset = $this->get('offset') ? $this->get('offset') : 0;

            $this->db->select('cp.category_id as category_id, group_concat(c1.slug order by cp.level separator "/") as path, c2.name, c.sort_order', false);
            $this->db->from('category_path cp');
            $this->db->join('category c', 'cp.path_id = c.category_id', 'left');
            $this->db->join('category c1', 'c.category_id = c1.category_id', 'left');
            $this->db->join('category c2', 'cp.category_id = c2.category_id', 'left');

            $this->db->group_by('cp.category_id');

            $sort_data = array(
                'name',
                'sort_order'
            );

            if ($order && isset($sort_data[$order])) {
                $this->db->order_by($sort_data[$order], 'asc');
            }

            if ($offset < 0) $offset = 0;
            if ($limit < 1 || $limit > 1000) $limit = 1000;

            $this->db->limit((int)$limit, (int)$offset);

            $result = $this->db->get()->result_array();

            $response = array(
                'paging' => array(
                    'offset' => $offset,
                    'limit' => $limit,
                    'total_records' => $total_records
                ),
                'result' => $result
            );
        }

        $code = $result ? REST_Controller::HTTP_OK : REST_Controller::HTTP_NOT_FOUND;

        $this->response($response, $code);
    }
}