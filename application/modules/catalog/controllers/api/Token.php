<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Token extends API_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get token
     *
     * @access public
     * @return void
     */
    public function index_post()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_data([
            'username' => $this->post('username'),
            'password' => $this->post('password'),
        ]);

        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == TRUE) {
            if ($admin = $this->admin_model->login($this->post('username'), $this->post('password'))) {

                $date = new DateTime();

                $token['admin_id'] = $admin['admin_id'];
                $token['email'] = $admin['email'];
                $token['iat'] = $date->getTimestamp();
                $token['exp'] = $date->getTimestamp() + $this->config->item('jwt_token_expire');

                $output_data['token'] = $this->jwt_encode($token);

                $this->response($output_data, REST_Controller::HTTP_OK);
            } else {
                $output_data[$this->config->item('rest_status_field_name')] = 'invalid_credentials';
                $output_data[$this->config->item('rest_message_field_name')] = 'Invalid username or password!';
                $this->response($output_data, REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $output_data[$this->config->item('rest_status_field_name')] = 'empty_fields';
            $output_data[$this->config->item('rest_message_field_name')] = $this->form_validation->error_array();

            $this->response($output_data, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Refresh token
     *
     * Refresh the token with new expirey time
     *
     * @access public
     * @return void
     */
    public function refresh_get()
    {
        try {
            $decoded = $this->jwt_decode($this->jwt_token());

            if ($this->admin_model->check_email($decoded['email']) == FALSE) {
                $output_data[$this->config->item('rest_status_field_name')] = 'invalid_user';
                $output_data[$this->config->item('rest_message_field_name')] = 'The token admin id is not exist in the system!';
                $this->response($output_data, REST_Controller::HTTP_UNAUTHORIZED);
            }

            $date = new DateTime();

            $token['admin_id'] = $decoded['admin_id'];
            $token['email'] = $decoded['email'];
            $token['iat'] = $date->getTimestamp();
            $token['exp'] = $date->getTimestamp() + $this->config->item('jwt_token_expire');

            $output_data['token'] = $this->jwt_encode($token);

            $this->response($output_data, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $output_data[$this->config->item('rest_status_field_name')] = 'invalid_token';
            $output_data[$this->config->item('rest_message_field_name')] = $e->getMessage();
            $this->response($output_data, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
}