<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Front_Controller
{
    private $limit = 20;

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('category_model');
        $this->load->model('product_model');
        $this->load->helper('format');
        $this->load->helper('form');
        $this->lang->load('product');
    }

    public function index()
    {
        $this->load->breadcrumb(lang('text_home'), site_url());

        $segments = $this->uri->segment_array();

        if (count($segments) == 2) {
            $this->detail($segments[2]);
        } else {
            $category = false;
            array_shift($segments);
            $product = array_pop($segments);
            $path = '';
            $path_ids = array();

            foreach ($segments as $slug) {
                if ($category = $this->category_model->get_category($slug)) {
                    $path .= $category['slug'] . '/';
                    $path_ids[] = $category['category_id'];
                    $this->load->breadcrumb($category['name'], site_url('category/' . $path));
                }
            }

            array_pop($path_ids);

            $path_id = implode('-', $path_ids);

            if ($category && $category['path_id'] == $path_id) {
                $this->detail($product, $path);
            } else {
                show_404();
            }
        }
    }

    /**
     * Detail product
     *
     * @access public
     * @param mixed $product
     * @param string $path (default: '')
     * @return void
     */
    public function detail($product, $path = '')
    {
        $data = $this->product_model->get_product($product);
        $pdwd = $this->config->item('image_product_detail_width');
        $pdhg = $this->config->item('image_product_detail_height');

        if ($data) {
            $data['images'] = array();
            $images = $this->product_model->get_product_images($data['product_id']);
            foreach ($images as $image) {
                $data['images'][] = array(
                    'popup' => $this->image->resize($image['image'], $pdwd, $pdhg),
                    'thumb' => $this->image->resize($image['image'], $pdwd / 2, $pdhg / 2),
                );
            }

            if (isset($data['image'])) {
                $data['popup'] = $this->image->resize($data['image'], $pdwd, $pdhg);
                $data['thumb'] = $this->image->resize($data['image'], $pdwd / 2, $pdhg / 2);
            }

            $this->load->library('currency');

            if ($data['discount']) {
                $data['discount'] = $this->currency->format($data['discount']);
            } else {
                $data['discount'] = false;
            }

            $data['href'] = 'product/' . $path . $data['slug'];

            $this->load->breadcrumb($data['name'], site_url($data['href']));

            $data['discounts'] = array();

            $discounts = $this->product_model->get_discounts($data['product_id']);

            foreach ($discounts as $discount) {
                $data['discounts'][] = array(
                    'quantity' => $discount['quantity'],
                    'price' => $this->currency->format($discount['price'])
                );
            }

            $data['options'] = $this->product_model->get_options($data['product_id']);
            $data['relates'] = array();

            $pwd = $this->config->item('image_product_width');
            $phg = $this->config->item('image_product_height');

            foreach ($this->product_model->get_relates($data['product_id']) as $relate) {
                $data['relates'][] = array(
                    'product_id' => $relate['product_id'],
                    'thumb' => $this->image->resize($relate['image'] ? $relate['image'] : 'no_image.jpg', $pwd, $phg),
                    'name' => $relate['name'],
                    'description' => $relate['description'],
                    'price' => $this->currency->format($relate['price']),
                    'discount' => (float)$relate['discount'] ? $this->currency->format($relate['discount']) : false,
                    'wholesaler' => ($relate['wholesaler'] > 1) ? true : false,
                    'discount_percent' => (float)$relate['discount'] ? ceil(($relate['price'] - $relate['discount']) / $relate['price'] * 100) . '%' : false,
                    'rating' => $relate['rating'],
                    'href' => site_url('product/' . $relate['slug']),
                    'quickview' => site_url('catalog/product/quickview/' . $relate['product_id']),
                );
            }

            $this->product_model->update_view($data['product_id']);

            $schema = array(
                '@context' => 'http://schema.org',
                '@type' => 'Product',
                'aggregateRating' => array(
                    '@type' => 'AggregateRating',
                    'ratingValue' => $data['rating'] ? $data['rating'] : '5',
                    'reviewCount' => $data['reviews'] ? $data['reviews'] : '1',
                ),
                'description' => $data['meta_description'],
                'name' => $data['name'],
                'image' => $data['thumb'],
                'offers' => array(
                    '@type' => 'Offer',
                    'availability' => 'http://schema.org/InStock',
                    'price' => $data['price'],
                    'priceCurrency' => 'IDR'
                )
            );

            $data['schema'] = '<script type="application/ld+json">';
            $data['schema'] .= json_encode($schema);
            $data['schema'] .= '</script>';

            $data['price'] = $this->currency->format($data['price']);

            $data['attribute_groups'] = $this->product_model->get_attributes($data['product_id']);

            if ($data['quantity'] > 0) {
                $data['stock_status'] = lang('text_instock');
            }

            $data['product_tabs'] = $this->product_model->get_product_tabs($data['product_id']);

            $this->load
                ->title($data['meta_title'], $this->config->item('site_name'))
                ->metadata('description', $data['meta_description'])
                ->metadata('keyword', $data['meta_keyword'])
                ->metadata('og:site_name', $this->config->item('site_name'), 'og')
                ->metadata('og:title', $data['meta_title'], 'og')
                ->metadata('og:description', $data['meta_description'], 'og')
                ->metadata('og:type', 'product', 'og')
                ->metadata('og:image', $data['thumb'], 'og')
                ->metadata('og:url', site_url('product/' . $data['slug']), 'og')
                ->metadata('twitter:card', 'product', 'meta')
                ->metadata('twitter:site', site_url(), 'meta')
                ->metadata('twitter:creator', $this->config->item('site_name'), 'meta')
                ->metadata('twitter:title', $data['meta_title'], 'meta')
                ->metadata('twitter:description', $data['meta_description'], 'meta')
                ->metadata('twitter:image', $data['thumb'], 'meta')
                ->js('/themes/desktop/assets/js/jquery.elevateZoom-3.0.3.min.js')
                ->view('product', $data);
        } else {
            show_404();
        }
    }

    /**
     * Review
     *
     * @access public
     * @return void
     */
    public function review()
    {
        check_ajax();

        $limit = 10;

        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        } else {
            $page = 1;
        }

        $start = ($page - 1) * $limit;

        $reviews = $this->db
            ->select('r.review_id, r.text, r.rating, r.date_added, c.name as author, c.image')
            ->join('customer c', 'c.customer_id = r.customer_id', 'left')
            ->from('review r')
            ->where('r.product_id', (int)$this->input->get('product_id'))
            ->where('r.active', 1)
            ->limit($limit, $start)
            ->order_by('date_added', 'desc')
            ->get()
            ->result_array();

        $data['reviews'] = array();

        foreach ($reviews as $review) {
            if ($review['image']) {
                $review['image'] = $this->image->resize($review['image'], 80, 80);
            } else {
                $review['image'] = $this->image->resize('photo.png', 80, 80);
            }

            $review['date_added'] = format_time_ago($review['date_added']);

            $data['reviews'][] = $review;
        }

        $num_rows = $this->db
            ->select('review_id')
            ->where('product_id', (int)$this->input->get('product_id'))
            ->count_all_results('review');

        if ($page < ceil($num_rows / $limit)) {
            $page = $page + 1;

            $data['next'] = site_url('product/review?product_id=' . $this->input->get('product_id') . '&page=' . $page);
        } else {
            $data['next'] = false;
        }


        return $this->load->layout(false)->view('product_review', $data);
    }

    public function quickview($product_id = null)
    {
        $data = $this->product_model->get_product($product_id);

        $pdwd = $this->config->item('image_product_detail_width');
        $pdhg = $this->config->item('image_product_detail_height');

        if ($data) {
            $data['images'] = array();
            $images = $this->product_model->get_product_images($data['product_id']);

            foreach ($images as $image) {
                $data['images'][] = array(
                    'popup' => $this->image->resize($image['image'], $pdwd, $pdhg),
                    'thumb' => $this->image->resize($image['image'], $pdwd / 2, $pdhg / 2),
                );
            }

            if ($data['image']) {
                $data['popup'] = $this->image->resize($data['image'], $pdwd, $pdhg);
                $data['thumb'] = $this->image->resize($data['image'], $pdwd / 2, $pdhg / 2);
            } else {
                $data['popup'] = $this->image->resize('no_image.jpg', $pdwd, $pdhg);
                $data['thumb'] = $this->image->resize('no_image.jpg', $pdwd / 2, $pdhg / 2);
            }

            $this->load->library('currency');

            if ($data['discount']) {
                $data['discount'] = $this->currency->format($data['discount']);
            } else {
                $data['discount'] = false;
            }

            $data['price'] = $this->currency->format($data['price']);
            $data['href'] = 'product/' . $data['slug'];
            $data['options'] = $this->product_model->get_options($data['product_id']);
            $data['discounts'] = array();

            $discounts = $this->product_model->get_discounts($data['product_id']);

            foreach ($discounts as $discount) {
                $data['discounts'][] = array(
                    'quantity' => $discount['quantity'],
                    'price' => $this->currency->format($discount['price'])
                );
            }

            $this->load->layout(false)->view('product_quickview', $data);
        } else {
            return;
        }
    }
}