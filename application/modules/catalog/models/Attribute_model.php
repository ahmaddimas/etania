<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attribute_model extends MY_Model
{
    /**
     * Add new attribute
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function add_attribute($data)
    {
        $this->db
            ->set(array('name' => $data['name'], 'sort_order' => (int)$data['sort_order']))
            ->insert('attribute_group');

        $attribute_group_id = $this->db->insert_id();

        if (isset($data['attribute'])) {
            $set = array();

            foreach ($data['attribute'] as $attribute) {
                $set[] = array(
                    'attribute_group_id' => (int)$attribute_group_id,
                    'name' => $attribute['name'],
                    'sort_order' => (int)$attribute['sort_order']
                );
            }

            if ($set) $this->db->insert_batch('attribute', $set);
        }
    }

    /**
     * Edit attribute
     *
     * @access public
     * @param int $attribute_group_id
     * @param array $data
     * @return void
     */
    public function edit_attribute($attribute_group_id, $data)
    {
        $this->db
            ->set(array('name' => $data['name'], 'sort_order' => (int)$data['sort_order']))
            ->where('attribute_group_id', (int)$attribute_group_id)
            ->update('attribute_group');

        $this->db->where('attribute_group_id', (int)$attribute_group_id)->delete('attribute');

        if (isset($data['attribute'])) {
            $set = array();

            foreach ($data['attribute'] as $attribute) {
                $attribute_id = $attribute['attribute_id'] ? $attribute['attribute_id'] : null;

                $set[] = array(
                    'attribute_id' => (int)$attribute_id,
                    'attribute_group_id' => (int)$attribute_group_id,
                    'name' => $attribute['name'],
                    'sort_order' => (int)$attribute['sort_order']
                );
            }

            if ($set) $this->db->insert_batch('attribute', $set);
        }
    }

    /**
     * Delete multiple attribute groups
     *
     * @access public
     * @param int $attribute_group_id
     * @return void
     */
    public function delete_attribute_groups($attribute_group_ids)
    {
        // delete all related attributes with this group
        $attribute_ids = array();

        $attributes = $this->db
            ->select('attribute_id')
            ->where_in('attribute_group_id', $attribute_group_ids)
            ->get('attribute')
            ->result_array();

        foreach ($attributes as $attribute) {
            $attribute_ids[] = (int)$attribute['attribute_id'];
        }

        $this->db
            ->where_in('attribute_id', $attribute_ids)
            ->delete('product_attribute');

        // delete all attribute groups include their attributes
        $this->db
            ->where_in('attribute_group_id', $attribute_group_ids)
            ->delete(array('attribute', 'attribute_group'));
    }

    /**
     * Delete attribute group individually
     *
     * @access public
     * @param int $attribute_group_id
     * @return void
     */
    public function delete_attribute_group($attribute_group_id)
    {
        // delete all related attributes with this group
        $attribute_ids = array();

        $attributes = $this->db
            ->select('attribute_id')
            ->where('attribute_group_id', (int)$attribute_group_id)
            ->get('attribute')
            ->result_array();

        foreach ($attributes as $attribute) {
            $attribute_ids[] = (int)$attribute['attribute_id'];
        }

        $this->db
            ->where_in('attribute_id', $attribute_ids)
            ->delete('product_attribute');

        // delete all attribute groups include their attributes
        $this->db
            ->where('attribute_group_id', $attribute_group_id)
            ->delete(array('attribute', 'attribute_group'));
    }

    /**
     * Get attribute group
     *
     * @access public
     * @param int $attribute_group_id
     * @return array
     */
    public function get_attribute_group($attribute_group_id)
    {
        return $this->db
            ->where('attribute_group_id', (int)$attribute_group_id)
            ->get('attribute_group')
            ->row_array();
    }

    /**
     * Get attribute groups
     *
     * @access public
     * @param array $data
     * @return array
     */
    public function get_attribute_groups($data = array())
    {
        if (!empty($data['attribute_name'])) {
            $this->db->like('name', $data['attribute_name'], 'after');
        }

        $sort_data = array(
            'name',
            'sort_order'
        );

        $sort = (isset($data['sort']) && in_array($data['sort'], $sort_data)) ? $data['sort'] : 'name';
        $order = (isset($data['order']) && ($data['order'] == 'desc')) ? 'desc' : 'asc';

        $this->db->order_by($sort, $order);

        if (isset($data['start']) or isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        return $this->db
            ->get('attribute_group')
            ->result_array();
    }

    /**
     * Get attribute
     *
     * @access public
     * @param int $attribute_id
     * @return array
     */
    public function get_attribute($attribute_id)
    {
        return $this->db
            ->select('a.*, ag.name as group')
            ->where('attribute_group ag', 'ag.attribute_group_id = a.attribute_group_id', 'left')
            ->where('a.attribute_id', (int)$attribute_id)
            ->get('attribute a')
            ->row_array();
    }

    /**
     * Get attributes
     *
     * @access public
     * @param array $data
     * @return array
     */
    public function get_attributes($data)
    {
        $this->db
            ->select('a.name, a.attribute_id, a.sort_order, ag.name as group_name')
            ->from('attribute a')
            ->join('attribute_group ag', 'ag.attribute_group_id = a.attribute_group_id', 'left');

        if (!empty($data['attribute_group_id'])) {
            $this->db->where('a.attribute_group_id', $data['attribute_group_id'], 'after');
        }

        if (!empty($data['filter_name'])) {
            $implode = array();

            $words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

            foreach ($words as $word) {
                $this->db->like('a.name', $word, 'both');
            }
        }

        $this->db->order_by('a.sort_order', 'asc');

        if (isset($data['start']) or isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        return $this->db->get()->result_array();
    }

    /**
     * Count all attribute groups
     *
     * @access public
     * @return int
     */
    public function count_attribute_groups()
    {
        return $this->db
            ->select('attribute_group_id')
            ->count_all_results('attribute_group');
    }
}