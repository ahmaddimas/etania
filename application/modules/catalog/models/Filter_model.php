<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filter_model extends MY_Model
{
    /**
     * Add new filter
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function add_filter($data)
    {
        $this->db
            ->set(array('name' => $data['name'], 'sort_order' => (int)$data['sort_order']))
            ->insert('filter_group');

        $filter_group_id = $this->db->insert_id();

        if (isset($data['filter'])) {
            $set = array();

            foreach ($data['filter'] as $filter) {
                $set[] = array(
                    'filter_group_id' => (int)$filter_group_id,
                    'name' => $filter['name'],
                    'sort_order' => (int)$filter['sort_order']
                );
            }

            $this->db->insert_batch('filter', $set);
        }
    }

    /**
     * Edit filter
     *
     * @access public
     * @param int $filter_group_id
     * @param array $data
     * @return void
     */
    public function edit_filter($filter_group_id, $data)
    {
        $this->db
            ->set(array('name' => $data['name'], 'sort_order' => (int)$data['sort_order']))
            ->where('filter_group_id', (int)$filter_group_id)
            ->update('filter_group');

        $this->db->where('filter_group_id', (int)$filter_group_id)->delete('filter');

        if (isset($data['filter'])) {
            $set = array();

            foreach ($data['filter'] as $filter) {
                $filter_id = $filter['filter_id'] ? $filter['filter_id'] : null;

                $set[] = array(
                    'filter_id' => (int)$filter_id,
                    'filter_group_id' => (int)$filter_group_id,
                    'name' => $filter['name'],
                    'sort_order' => (int)$filter['sort_order']
                );
            }

            if ($set) $this->db->insert_batch('filter', $set);
        }
    }

    /**
     * Delete filter group(s)
     *
     * @access public
     * @param int | array $filter_group_id
     * @return void
     */
    public function delete_filter_group($filter_group_id)
    {
        $filter_ids = array();

        $filters = $this->db->select('filter_id');

        if (is_array($filter_group_id)) {
            $filters = $this->db->where_in('filter_group_id', $filter_group_id);
        } else {
            $filters = $this->db->where('filter_group_id', (int)$filter_group_id);
        }

        $filters = $this->db->get('filter')->result_array();

        foreach ($filters as $filter) {
            $filter_ids[] = (int)$filter['filter_id'];
        }

        $this->db
            ->where_in('filter_id', $filter_ids)
            ->delete('product_filter');

        if (is_array($filter_group_id)) {
            $this->db->where_in('filter_group_id', $filter_group_id);
        } else {
            $this->db->where('filter_group_id', (int)$filter_group_id);
        }

        $this->db->delete(array('filter', 'filter_group'));
    }

    /**
     * Get filter group
     *
     * @access public
     * @param int $filter_group_id
     * @return array
     */
    public function get_filter_group($filter_group_id)
    {
        return $this->db
            ->where('filter_group_id', (int)$filter_group_id)
            ->get('filter_group')
            ->row_array();
    }

    /**
     * Get filter groups
     *
     * @access public
     * @param array $data
     * @return array
     */
    public function get_filter_groups($data = array())
    {
        if (!empty($data['filter_name'])) {
            $this->db->like('name', $data['filter_name'], 'after');
        }

        $sort_data = array(
            'name',
            'sort_order'
        );

        $sort = (isset($data['sort']) && in_array($data['sort'], $sort_data)) ? $data['sort'] : 'name';
        $order = (isset($data['order']) && ($data['order'] == 'desc')) ? 'desc' : 'asc';

        $this->db->order_by($sort, $order);

        if (isset($data['start']) or isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        return $this->db
            ->get('filter_group')
            ->result_array();
    }

    /**
     * Get filter
     *
     * @access public
     * @param int $filter_id
     * @return array
     */
    public function get_filter($filter_id)
    {
        return $this->db
            ->select('f.*, fg.name as group_name')
            ->join('filter_group fg', 'fg.filter_group_id = f.filter_group_id', 'left')
            ->where('f.filter_id', (int)$filter_id)
            ->get('filter f')
            ->row_array();
    }

    /**
     * Get filters
     *
     * @access public
     * @param array $data
     * @return array
     */
    public function get_filters($data)
    {
        $this->db
            ->select('f.name, f.filter_id, f.sort_order, fg.name as group_name')
            ->from('filter f')
            ->join('filter_group fg', 'fg.filter_group_id = f.filter_group_id', 'left');

        if (!empty($data['filter_group_id'])) {
            $this->db->where('f.filter_group_id', $data['filter_group_id'], 'after');
        }

        if (!empty($data['filter_name'])) {
            $this->db->like('f.name', $data['filter_name'], 'after');
        }

        $this->db->order_by('f.sort_order', 'asc');

        if (isset($data['start']) or isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        return $this->db->get()->result_array();
    }

    /**
     * Count all filter groups
     *
     * @access public
     * @return int
     */
    public function count_filter_groups()
    {
        return $this->db
            ->select('filter_group_id')
            ->count_all_results('filter_group');
    }

    public function get_filter_menus()
    {
        $filter_groups = array();

        foreach ($this->get_filter_groups() as $filter_group) {
            $filter_groups[] = array(
                'filter_group_id' => $filter_group['filter_group_id'],
                'name' => $filter_group['name'],
                'filters' => $this->db->where('filter_group_id', $filter_group['filter_group_id'])->get('filter')->result_array()
            );
        }

        return $filter_groups;
    }
}