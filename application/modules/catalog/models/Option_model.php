<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option_Model extends MY_Model
{
    /**
     * Create new option
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function create_option($data)
    {
        $this->db
            ->set(array(
                'type' => $data['type'],
                'name' => $data['name'],
                'sort_order' => (int)$data['sort_order']
            ))
            ->insert('option');

        $option_id = $this->db->insert_id();

        if (isset($data['option_value'])) {
            $set = array();

            foreach ($data['option_value'] as $option_value) {
                $set[] = array(
                    'option_id' => (int)$option_id,
                    'name' => $option_value['name'],
                    'sort_order' => (int)$option_value['sort_order']
                );
            }

            if ($set) $this->db->insert_batch('option_value', $set);
        }
    }

    /**
     * Edit option
     *
     * @access public
     * @param int $option_id
     * @param array $data
     * @return void
     */
    public function edit_option($option_id, $data)
    {
        $this->db
            ->set(array(
                'type' => $data['type'],
                'name' => $data['name'],
                'sort_order' => (int)$data['sort_order']
            ))
            ->where('option_id', (int)$option_id)
            ->update('option');

        $this->db->where('option_id', (int)$option_id)->delete('option_value');

        if (isset($data['option_value'])) {
            $set = array();

            foreach ($data['option_value'] as $option_value) {
                $option_value_id = $option_value['option_value_id'] ? $option_value['option_value_id'] : null;

                $set[] = array(
                    'option_value_id' => (int)$option_value_id,
                    'option_id' => (int)$option_id,
                    'name' => $option_value['name'],
                    'sort_order' => (int)$option_value['sort_order']
                );
            }

            if ($set) $this->db->insert_batch('option_value', $set);
        }
    }

    /**
     * Delete option
     *
     * @access public
     * @param int | array $option_group_id
     * @return void
     */
    public function delete_option($option_id)
    {
        if (is_array($option_id)) {
            $this->db->where_in('option_id', $option_id);
        } else {
            $this->db->where('option_id', (int)$option_id);
        }

        $this->db->delete(array('option', 'option_value'));
    }

    /**
     * Get option
     *
     * @access public
     * @param int $option_id
     * @return array
     */
    public function get_option($option_id)
    {
        return $this->db
            ->where('option_id', (int)$option_id)
            ->get('option')
            ->row_array();
    }

    /**
     * Get options
     *
     * @access public
     * @param array $data
     * @return array
     */
    public function get_options($data = array())
    {
        if (!empty($data['filter_name'])) {
            $this->db->like('name', $data['filter_name'], 'after');
        }

        $sort_data = array(
            'name',
            'sort_order'
        );

        $sort = (isset($data['sort']) && in_array($data['sort'], $sort_data)) ? $data['sort'] : 'name';
        $order = (isset($data['order']) && ($data['order'] == 'desc')) ? 'desc' : 'asc';

        $this->db->order_by($sort, $order);

        if (isset($data['start']) or isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        return $this->db->get('option')->result_array();
    }

    /**
     * Get option value
     *
     * @access public
     * @param int $option_value_id
     * @return array
     */
    public function get_option_value($option_value_id)
    {
        return $this->db
            ->where('option_value_id', (int)$option_value_id)
            ->get('option_value')
            ->row_array();
    }

    /**
     * Get option values by option id
     *
     * @access public
     * @param int $option_id
     * @return array
     */
    public function get_option_values($option_id)
    {
        return $this->db
            ->where('option_id', (int)$option_id)
            ->order_by('sort_order', 'asc')
            ->get('option_value')
            ->result_array();
    }

    /**
     * Count all option groups
     *
     * @access public
     * @return int
     */
    public function count_options()
    {
        return $this->db
            ->select('option_id')
            ->count_all_results('option');
    }
}