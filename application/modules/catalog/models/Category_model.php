<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add new category
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function create_category($data)
    {
        $this->load->library('seo');

        if (empty($data['meta_description'])) {
            $data['meta_description'] = $this->seo->meta_description($data['description']);
        }

        if (empty($data['meta_keyword'])) {
            $data['meta_keyword'] = $this->seo->meta_keyword($data['name']);
        }

        if (empty($data['meta_title'])) {
            $data['meta_title'] = $data['name'];
        }

        $this->db->set(array(
            'name' => $data['name'],
            'description' => $data['description'],
            'meta_description' => $data['meta_description'],
            'meta_keyword' => $data['meta_keyword'],
            'meta_title' => $data['meta_title'],
            'slug' => $this->validate_slug(strtolower(url_title($data['name']))),
            'image' => $data['image'],
            'menu_image' => $data['menu_image'],
            'parent_id' => (int)$data['parent_id'],
            'top' => !empty($data['top']) ? (int)$data['top'] : 0,
            'column' => (int)$data['column'],
            'sort_order' => (int)$data['sort_order'],
            'active' => (int)$data['active'],
            'date_added' => date('Y-m-d h:i:s', time())
        ))->insert('category');

        $category_id = $this->db->insert_id();

        $level = 0;
        $paths = array();

        foreach ($this->db
                     ->where('category_id', (int)$data['parent_id'])
                     ->order_by('level', 'asc')
                     ->get('category_path')
                     ->result_array() as $result) {
            $paths[] = array(
                'category_id' => (int)$category_id,
                'path_id' => (int)$result['path_id'],
                'level' => (int)$level
            );

            $level++;
        }

        if ($paths) $this->db->insert_batch('category_path', $paths);

        $this->db->set(array(
            'category_id' => (int)$category_id,
            'path_id' => (int)$category_id,
            'level' => (int)$level
        ))->insert('category_path');

        $category = $this->db
            ->select('name, slug')
            ->where('category_id', (int)$category_id)
            ->get('category')
            ->row_array();

        if ($category['slug'] == '') {
            $this->db
                ->where('category_id', (int)$category_id)
                ->set('slug', $this->validate_slug(strtolower(url_title($category['name']))))
                ->update('category');
        }
    }

    /**
     * Edit existing category
     *
     * @access public
     * @param int $category_id
     * @param array $data
     * @return void
     */
    public function edit_category($category_id, $data)
    {
        $this->load->library('seo');

        if (empty($data['meta_description'])) {
            $data['meta_description'] = $this->seo->meta_description($data['description']);
        }

        if (empty($data['meta_keyword'])) {
            $data['meta_keyword'] = $this->seo->meta_keyword($data['name']);
        }

        if (empty($data['meta_title'])) {
            $data['meta_title'] = $data['name'];
        }

        $this->db
            ->set(array(
                'name' => $data['name'],
                'description' => $data['description'],
                'meta_description' => $data['meta_description'],
                'meta_keyword' => $data['meta_keyword'],
                'meta_title' => $data['meta_title'],
                'slug' => $this->validate_slug(strtolower(url_title($data['name'])), $category_id),
                'image' => $data['image'],
                'menu_image' => $data['menu_image'],
                'parent_id' => (int)$data['parent_id'],
                'top' => !empty($data['top']) ? (int)$data['top'] : 0,
                'column' => (int)$data['column'],
                'sort_order' => (int)$data['sort_order'],
                'active' => (int)$data['active'],
                'date_modified' => date('Y-m-d h:i:s', time())
            ))
            ->where('category_id', (int)$category_id)
            ->update('category');

        $results = $this->db
            ->where('path_id', (int)$category_id)
            ->order_by('level', 'asc')
            ->get('category_path')
            ->result_array();

        if ($results) {
            foreach ($results as $category_path) {
                $this->db
                    ->where('category_id', (int)$category_path['category_id'])
                    ->where('level <', (int)$category_path['level'])
                    ->delete('category_path');

                $path = array();

                foreach ($this->db
                             ->where('category_id', (int)$data['parent_id'])
                             ->order_by('level', 'asc')
                             ->get('category_path')
                             ->result_array() as $result) {
                    $path[] = $result['path_id'];
                }

                foreach ($this->db
                             ->where('category_id', (int)$category_path['category_id'])
                             ->order_by('level', 'asc')
                             ->get('category_path')
                             ->result_array() as $result) {
                    $path[] = $result['path_id'];
                }

                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . $this->db->dbprefix('category_path') . "` SET 
						`category_id` = '" . (int)$category_path['category_id'] . "', 
						`path_id` = '" . (int)$path_id . "', 
						`level` = '" . (int)$level . "'");

                    $level++;
                }
            }
        } else {
            $this->db->where('category_id', (int)$category_id)->delete('category_path');

            $level = 0;
            $paths = array();

            foreach ($this->db
                         ->where('category_id', (int)$data['parent_id'])
                         ->order_by('level', 'asc')
                         ->get('category_path')
                         ->result_array() as $result) {
                $paths[] = array(
                    'category_id' => (int)$category_id,
                    'path_id' => (int)$result['path_id'],
                    'level' => (int)$level
                );

                $level++;
            }

            if ($paths) $this->db->insert_batch('category_path', $paths);

            $this->db->query("REPLACE INTO `" . $this->db->dbprefix('category_path') . "` SET 
				`category_id` = '" . (int)$category_id . "', 
				`path_id` = '" . (int)$category_id . "', 
				`level` = '" . (int)$level . "'");
        }
    }

    /**
     * Get categories
     *
     * @access public
     * @param array $data Parameters
     * @return array
     */
    public function get_categories($data = array())
    {
        $this->db->select('cp.category_id as category_id, group_concat(c1.slug order by cp.level separator "/") as path, group_concat(c1.name order by cp.level separator " &raquo; ") as name, c.parent_id, c.sort_order, c.active', false);
        $this->db->from('category_path cp');
        $this->db->join('category c', 'cp.path_id = c.category_id', 'left');
        $this->db->join('category c1', 'c.category_id = c1.category_id', 'left');
        $this->db->join('category c2', 'cp.category_id = c2.category_id', 'left');

        if (!empty($data['filter_name'])) {
            $this->db->like('c2.name', $data['filter_name'], 'after');
        }

        if (!empty($data['filter_description'])) {
            $this->db->or_like('c2.description', $data['filter_description'], 'after');
        }

        $this->db->group_by('cp.category_id');

        $sort_data = array(
            'name',
            'sort_order',
            'active'
        );

        $sort = (isset($data['sort']) && isset($data['sort'])) ? $data['sort'] : 'name';
        $order = (isset($data['order']) && ($data['order'] == 'desc')) ? 'desc' : 'asc';

        $this->db->order_by($sort, $order);

        if (isset($data['start']) or isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        return $this->db->get()->result_array();
    }

    public function get_subcategories($parent_id = 0, $limit = false)
    {
        $this->db
            ->select('category_id, name, slug, menu_image, top, sort_order, active')
            ->from('category')
            ->where('parent_id', (int)$parent_id)
            ->where('active', 1);

        if ($limit) {
            $this->db->limit($limit);
        }

        return $this->db->order_by('sort_order', 'asc')
            ->get()
            ->result_array();
    }


    public function get_subcategories_custom($parent_id = 0)
    {
        $this->db
            ->select('c2.name as name, cp.category_id as category_id, group_concat(c1.name order by cp.level separator " &raquo; ") as level, c.parent_id, c2.sort_order, c.active', false)
            ->from('category_path cp')
            ->join('category c', 'cp.path_id = c.category_id', 'left')
            ->join('category c1', 'c.category_id = c1.category_id', 'left')
            ->join('category c2', 'cp.category_id = c2.category_id', 'left')
            ->where('c.parent_id', (int)$parent_id)
            ->group_by('cp.category_id');

        return $this->db->order_by('sort_order', 'asc')
            ->get()
            ->result_array();
    }

    /**
     * Get category filters
     *
     * @access public
     * @param int $category_id
     * @return array
     */
    public function get_category_filters($category_id)
    {
        return $this->db
            ->select('f.name, f.filter_id, fg.name as group_name')
            ->from('category_filter cf')
            ->join('filter f', 'f.filter_id = cf.filter_id', 'left')
            ->join('filter_group fg', 'fg.filter_group_id = f.filter_group_id', 'left')
            ->where('cf.category_id', (int)$category_id)
            ->order_by('f.sort_order', 'asc')
            ->get()
            ->result_array();
    }

    /**
     * Count categories
     *
     * @access public
     * @return void
     */
    public function count_categories()
    {
        return $this->db
            ->select('category_id')
            ->count_all_results('category');
    }

    /**
     * Get category
     *
     * @access public
     * @param int $key
     * @return array
     */
    public function get_category($key)
    {
        $this->db
            ->select("distinct *, (select group_concat(c1.name order by level separator ' &raquo; ') from " . $this->db->dbprefix('category_path') . " cp left join " . $this->db->dbprefix('category') . " c1 on (cp.path_id = c1.category_id and cp.category_id != cp.path_id) where cp.category_id = c.category_id group by cp.category_id) as path, (select group_concat(c1.category_id order by level separator '-') from " . $this->db->dbprefix('category_path') . " cp left join " . $this->db->dbprefix('category') . " c1 on (cp.path_id = c1.category_id and cp.category_id != cp.path_id) where cp.category_id = c.category_id group by cp.category_id) as path_id", false)
            ->from('category c');

        if (is_numeric($key)) {
            $this->db->where('c.category_id', (int)$key);
        } else {
            $this->db->where('c.slug', $key);
        }

        return $this->db->get()->row_array();
    }

    /**
     * Repair categories
     *
     * @access public
     * @param int $parent_id
     * @return void
     */
    public function repair_categories($parent_id = 0)
    {
        foreach ($this->db
                     ->where('parent_id', (int)$parent_id)
                     ->get('category')
                     ->result_array() as $category) {
            $this->db->where('category_id', (int)$category['category_id'])->delete('category_path');

            $level = 0;
            $paths = array();

            foreach ($this->db
                         ->where('category_id', (int)$parent_id)
                         ->order_by('level', 'asc')
                         ->get('category_path')
                         ->result_array() as $result) {
                $paths[] = array(
                    'category_id' => (int)$category['category_id'],
                    'path_id' => (int)$result['path_id'],
                    'level' => (int)$level
                );

                $level++;
            }

            if ($paths) $this->db->insert_batch('category_path', $paths);

            $this->db->query("REPLACE INTO `" . $this->db->dbprefix('category_path') . "` SET 
				`category_id` = '" . (int)$category['category_id'] . "', 
				`path_id` = '" . (int)$category['category_id'] . "', 
				`level` = '" . (int)$level . "'");

            $this->repair_categories($category['category_id']);
        }
    }

    /**
     * Delete single category
     *
     * @access public
     * @param int $category_id
     * @return void
     */
    public function delete($category_id = null)
    {
        if (is_array($category_id)) {
            $this->db->where_in('category_id', (array)$category_id)->delete('category_path');

            foreach ($this->db
                         ->where_in('path_id', (array)$category_id)
                         ->get('category_path')
                         ->result_array() as $result) {
                $this->delete($result['category_id']);
            }

            $this->db->where_in('category_id', (array)$category_id);
        } else {
            $this->db->where('category_id', (int)$category_id)->delete('category_path');

            foreach ($this->db
                         ->where('path_id', (int)$category_id)
                         ->get('category_path')
                         ->result_array() as $result) {
                $this->delete($result['category_id']);
            }

            $this->db->where('category_id', (int)$category_id);
        }

        $this->db->delete(array(
            'category',
            'product_category'
        ));
    }

    /**
     * Count products inside category
     *
     * @access public
     * @param int $category_id
     * @return int
     */
    public function count_products($category_id)
    {
        return $this->db
            ->select('product_id')
            ->where('category_id', (int)$category_id)
            ->count_all_results('product_to_category');
    }

    /**
     * Validate slug
     *
     * @access public
     * @param string $slug
     * @param int $category_id
     * @param int $counter
     * @return string
     */
    public function validate_slug($slug = '', $category_id = null, $counter = null)
    {
        if ($category_id) {
            $this->db->where('category_id !=', $category_id);
        }

        $count = $this->db
            ->select('slug')
            ->from('category')
            ->where('slug', $slug . ($counter ? '-' . $counter : ''))
            ->count_all_results();

        if ($count > 0) {
            if (!$counter) {
                $counter = 1;
            } else {
                $counter++;
            }

            return $this->validate_slug($slug, $category_id, $counter);
        } else {
            return $slug . ($counter ? '-' . $counter : '');
        }
    }
}