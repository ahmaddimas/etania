<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Update view
     *
     * @access public
     * @param int $product_id
     * @return void
     */
    public function update_view($product_id)
    {
        $this->db
            ->where('product_id', (int)$product_id)
            ->set('viewed', 'viewed+1', false)
            ->update('product');
    }

    /**
     * Create product
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function create_product($data)
    {
        $data['slug'] = $this->validate_slug(strtolower(url_title($data['name'])));
        $data['meta_title'] = $data['name'];

        $this->load->library('seo');

        $data['meta_description'] = $this->seo->meta_description($data['description']);
        $data['meta_keyword'] = $this->seo->meta_keyword($data['name']);

        $product_id = $this->insert($data);

        if (isset($data['product_discount'])) $this->set_product_discounts($product_id, $data['product_discount']);
        if (isset($data['product_image'])) $this->set_product_images($product_id, $data['product_image']);
        if (isset($data['product_category'])) $this->set_product_categories($product_id, $data['product_category']);
        if (isset($data['product_related'])) $this->set_product_relates($product_id, $data['product_related']);
        if (isset($data['product_filter'])) $this->set_product_filters($product_id, $data['product_filter']);
        if (isset($data['product_option'])) $this->set_product_options($product_id, $data['product_option']);
        if (isset($data['product_attribute'])) $this->set_product_attributes($product_id, $data['product_attribute']);
        if (isset($data['product_digital'])) $this->set_product_digitals($product_id, $data['product_digital']);
        if (isset($data['product_tab'])) $this->set_product_tabs($product_id, $data['product_tab']);

        return $product_id;
    }

    /**
     * Edit product
     *
     * @access public
     * @param int $product_id
     * @param array $data
     * @return void
     */
    public function edit_product($product_id, $data)
    {
        $data['slug'] = $this->validate_slug(strtolower(url_title($data['name'])), $product_id);
        $data['meta_title'] = $data['name'];

        $this->load->library('seo');

        $data['meta_description'] = $this->seo->meta_description($data['description']);
        $data['meta_keyword'] = $this->seo->meta_keyword($data['name']);

        $this->update($product_id, $data);

        $this->db
            ->where('product_id', (int)$product_id)
            ->delete(array(
                'product_discount',
                'product_image',
                'product_category',
                'product_related',
                'product_filter',
                'product_option',
                'product_option_value',
                'product_attribute',
                'product_tab'
            ));

        if (isset($data['product_discount'])) $this->set_product_discounts($product_id, $data['product_discount']);
        if (isset($data['product_image'])) $this->set_product_images($product_id, $data['product_image']);
        if (isset($data['product_category'])) $this->set_product_categories($product_id, $data['product_category']);
        if (isset($data['product_related'])) $this->set_product_relates($product_id, $data['product_related']);
        if (isset($data['product_filter'])) $this->set_product_filters($product_id, $data['product_filter']);
        if (isset($data['product_option'])) $this->set_product_options($product_id, $data['product_option']);
        if (isset($data['product_attribute'])) $this->set_product_attributes($product_id, $data['product_attribute']);
        if ($data['type'] == 1) {
            $this->set_product_digitals($product_id, array(), $data['type']);
        } else {
            $this->db
                ->where('product_id', (int)$product_id)
                ->delete(array(
                    'product_digital'
                ));
            $this->set_product_digitals($product_id, array(), $data['type']);
        }
        if (isset($data['product_digital'])) $this->set_product_digitals($product_id, $data['product_digital'], $data['type']);
        if (isset($data['product_tab'])) $this->set_product_tabs($product_id, $data['product_tab']);
    }

    /**
     * Copy product
     *
     * @access public
     * @param int $product_id
     * @return void
     */
    public function copy_product($product_id)
    {
        $product = $this->get($product_id);

        if ($product) {
            $data = $product;

            $data['product_id'] = null;
            $data['slug'] = $this->validate_slug(strtolower(url_title($data['name'])));
            $data['sku'] = '';
            $data['viewed'] = 0;
            $data['active'] = 0;
            $data['product_discount'] = $this->get_product_discounts($product_id);
            $data['product_image'] = $this->get_product_images($product_id);
            $data['product_category'] = $this->get_product_categories($product_id);
            $data['product_option'] = $this->get_product_options($product_id);
            $data['product_related'] = $this->get_product_relates($product_id);
            $data['product_filter'] = $this->get_product_filters($product_id);
            $data['product_attribute'] = $this->get_product_attributes($product_id);
            $data['product_tab'] = $this->get_product_tabs($product_id);

            $this->create_product($data);
        }
    }

    /**
     * Get a product
     *
     * @access public
     * @param string $key Can be product id or slug
     * @return array
     */
    public function get_product($key)
    {
        $product = $this->db
            ->select("DISTINCT *, p.name AS name, p.slug, p.description, 
			(SELECT price FROM product_discount pd WHERE pd.product_id = p.product_id AND pd.quantity = '1' AND ((pd.date_start = '0000-00-00' OR pd.date_start < NOW()) AND (pd.date_end = '0000-00-00' OR pd.date_end > NOW())) ORDER BY pd.priority ASC, pd.price ASC LIMIT 1) AS discount,
			(SELECT ss.name FROM stock_status ss WHERE ss.stock_status_id = p.stock_status_id) AS stock_status,
			(SELECT wc.unit FROM weight_class wc WHERE p.weight_class_id = wc.weight_class_id) AS weight_class,
			(SELECT m.name FROM manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS manufacturer,
			(SELECT AVG(rating) AS total FROM review r1 WHERE r1.product_id = p.product_id AND r1.active = '1' GROUP BY r1.product_id) AS rating,
			(SELECT COUNT(*) AS total FROM review r2 WHERE r2.product_id = p.product_id AND r2.active = '1' GROUP BY r2.product_id) AS reviews, 
			(SELECT quantity FROM product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.quantity > '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.quantity DESC LIMIT 1) AS wholesaler, p.sort_order", false)
            ->from('product p')
            ->where((is_numeric($key)) ? array('p.product_id' => (int)$key) : array('p.slug' => $key))
            ->where('p.active', 1);

        $product = $this->db
            ->get()
            ->row_array();

        return $product;
    }

    /**
     * Get products
     *
     * @access public
     * @param array $data (default: array())
     * @return array
     */
    public function get_products($data = array())
    {
        $this->db->select('p.product_id, p.slug, (SELECT AVG(rating) AS total FROM review r1 WHERE r1.product_id = p.product_id AND r1.active = 1 GROUP BY r1.product_id) AS rating, (SELECT price FROM product_discount pd WHERE pd.product_id = p.product_id AND pd.quantity = 1 AND ((pd.date_start = \'0000-00-00\' OR pd.date_start < NOW()) AND (pd.date_end = \'0000-00-00\' OR pd.date_end > NOW())) ORDER BY pd.priority ASC, pd.price ASC LIMIT 1) AS discount');

        if (!empty($data['category_id'])) {
            if (!empty($data['sub_category'])) {
                $this->db->from('category_path cp');
                $this->db->join('product_category pc', 'pc.category_id = cp.category_id', 'left');
                $this->db->join('product p', 'p.product_id = pc.product_id', 'inner');
            } else {
                $this->db->from('product p');
                $this->db->join('product_category pc', 'p.product_id = pc.product_id', 'left');
            }
        } else {
            $this->db->from('product p');
        }

        if (!empty($data['filter'])) {
            $this->db->join('product_filter pf', 'p.product_id = pf.product_id', 'left');
        }

        if (!empty($data['category_id'])) {
            if (!empty($data['sub_category'])) {
                $this->db->where('cp.path_id', (int)$data['category_id']);
            } else {
                $this->db->where('(pc.category_id = ' . (int)$data['category_id'] . ' || p.category_id = ' . (int)$data['category_id'] . ')', null, false);
            }
        }

        if (!empty($data['filter'])) {
            $implode = array();

            $filters = $data['filter'];

            foreach ($filters as $filter_id) {
                $implode[] = (int)$filter_id;
            }

            $this->db->where_in('pf.filter_id', $implode);
        }

        if (!empty($data['name']) || !empty($data['tag'])) {
            if (!empty($data['name'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['name'])));

                foreach ($words as $word) {
                    $this->db->like('p.name', $word, 'both');
                }

                if (!empty($data['description'])) {
                    $this->db->or_like('p.description', $data['name'], 'both');
                }
            }
        }

        $this->db->where('p.active', 1);
        $this->db->group_by('p.product_id');

        $sort_data = array(
            'name' => 'p.name',
            'sku' => 'p.sku',
            'quantity' => 'p.quantity',
            'price' => 'p.price',
            'rating' => 'rating',
            'sort_order' => 'p.sort_order',
            'date_added' => 'p.date_added'
        );

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        if (isset($data['sort']) && isset($sort_data[$data['sort']])) {
            if ($sort_data[$data['sort']] == 'p.price') {
                $sort = '(CASE WHEN discount IS NOT NULL THEN discount ELSE p.price END)';
            } else {
                $sort = $sort_data[$data['sort']];
            }
        } else {
            $sort = 'p.name';
        }

        $order = (isset($data['order']) && ($data['order'] == 'desc')) ? 'desc' : 'asc';

        $this->db->order_by($sort, $order);

        $product_data = array();

        foreach ($this->db->get()->result_array() as $result) {
            $product_data[$result['product_id']] = $this->get_product($result['product_id']);
        }

        return $product_data;
    }

    /**
     * Count products
     *
     * @access public
     * @param array $data (default: array())
     * @return int
     */
    public function count_products($data = array())
    {
        $this->db->select('p.product_id');

        if (!empty($data['category_id'])) {
            if (!empty($data['sub_category'])) {
                $this->db->from('category_path cp');
                $this->db->join('product_category pc', 'pc.category_id = cp.category_id', 'left');
                $this->db->join('product p', 'p.product_id = pc.product_id', 'inner');
            } else {
                $this->db->from('product p');
                $this->db->join('product_category pc', 'p.product_id = pc.product_id', 'left');
            }
        } else {
            $this->db->from('product p');
        }

        if (!empty($data['filter'])) {
            $this->db->join('product_filter pf', 'p.product_id = pf.product_id', 'left');
        }

        if (!empty($data['category_id'])) {
            if (!empty($data['sub_category'])) {
                $this->db->where('cp.path_id', (int)$data['category_id']);
            } else {
                $this->db->where('(pc.category_id = ' . (int)$data['category_id'] . ' || p.category_id = ' . (int)$data['category_id'] . ')', null, false);
            }
        }

        if (!empty($data['filter'])) {
            $implode = array();

            $filters = $data['filter'];

            foreach ($filters as $filter_id) {
                $implode[] = (int)$filter_id;
            }

            $this->db->where_in('pf.filter_id', $implode);
        }

        if (!empty($data['name']) || !empty($data['tag'])) {
            if (!empty($data['name'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['name'])));

                foreach ($words as $word) {
                    $this->db->like('p.name', $word, 'both');
                }

                if (!empty($data['description'])) {
                    $this->db->or_like('p.description', $data['name'], 'both');
                }
            }
        }

        $this->db->where('p.active', 1);

        return $this->db->count_all_results();
    }

    /**
     * Get products
     *
     * @access public
     * @param array $data
     * @return array
     */
    public function get_products_autocomplete($data = array())
    {
        $this->db->from('product p');

        if (!empty($data['filter_name'])) {
            $implode = array();

            $words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

            foreach ($words as $word) {
                $this->db->like('p.name', $word, 'both');
            }

            if (!empty($data['description'])) {
                $this->db->or_like('p.description', $data['filter_name'], 'both');
            }
        }

        if (!empty($data['filter_price'])) {
            $this->db->or_like('p.price', $data['filter_price'], 'after');
        }

        if (isset($data['filter_active']) && !is_null($data['filter_active'])) {
            $this->db->where('p.active', $data['filter_active']);
        }

        $this->db->group_by('p.product_id');

        $sort_data = array(
            'name' => 'p.name',
            'price' => 'p.price',
            'active' => 'p.active',
            'sort_order' => 'p.sort_order'
        );

        $sort = (isset($data['sort']) && isset($sort_data[$data['sort']])) ? $sort_data[$data['sort']] : 'p.name';
        $order = (isset($data['order']) && ($data['order'] == 'desc')) ? 'desc' : 'asc';

        $this->db->order_by($sort, $order);

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        return $this->db->get()->result_array();
    }

    /**
     * Validate slug
     *
     * @access public
     * @param string $slug
     * @param int $product_id
     * @param int $counter
     * @return string
     */
    public function validate_slug($slug = '', $product_id = null, $counter = null)
    {
        if ($product_id) {
            $this->db->where('product_id !=', $product_id);
        }

        $count = $this->db
            ->select('slug')
            ->from('product')
            ->where('slug', $slug . ($counter ? '-' . $counter : ''))
            ->count_all_results();

        if ($count > 0) {
            if (!$counter) {
                $counter = 1;
            } else {
                $counter++;
            }

            return $this->validate_slug($slug, $product_id, $counter);
        } else {
            return $slug . ($counter ? '-' . $counter : '');
        }
    }

    /**
     * Set product discounts
     *
     * @access public
     * @param int $product_id
     * @param array $discounts
     * @return void
     */
    public function set_product_discounts($product_id, $discounts = array())
    {
        foreach ($discounts as $discount) {
            $this->db
                ->set(array(
                    'product_id' => (int)$product_id,
                    'quantity' => (int)$discount['quantity'],
                    'priority' => (int)$discount['priority'],
                    'price' => (float)$discount['price'],
                    'date_start' => ($discount['date_start'] != '') ? date('Y-m-d', strtotime($discount['date_start'])) : '0000-00-00',
                    'date_end' => ($discount['date_end'] != '') ? date('Y-m-d', strtotime($discount['date_end'])) : '0000-00-00'
                ))->insert('product_discount');
        }
    }

    /**
     * Set product images
     *
     * @access public
     * @param mixed $product_id
     * @param array $images
     * @return void
     */
    public function set_product_images($product_id, $images = array())
    {
        $product_image = array();
        $count = 1;

        foreach ($images as $image) {
            $product_image[] = array(
                'product_id' => (int)$product_id,
                'image' => html_entity_decode($image['image'], ENT_QUOTES, 'UTF-8'),
                'sort_order' => (empty($image['sort_order'])) ? $count : 0
            );

            $count++;
        }

        if ($product_image) {
            $this->db->insert_batch('product_image', $product_image);
        }
    }

    /**
     * Set product categories
     *
     * @access public
     * @param int $product_id
     * @param array $categories
     * @return void
     */
    public function set_product_categories($product_id, $categories = array())
    {
        foreach ($categories as $category_id) {
            $this->db
                ->set(array(
                    'product_id' => (int)$product_id,
                    'category_id' => (int)$category_id
                ))->insert('product_category');
        }
    }

    /**
     * Set product related
     *
     * @access public
     * @param int $product_id
     * @param array $categories
     * @return void
     */
    public function set_product_relates($product_id, $relates = array())
    {
        foreach ($relates as $related_id) {
            $this->db
                ->set(array(
                    'product_id' => (int)$product_id,
                    'related_id' => (int)$related_id
                ))->insert('product_related');
        }
    }

    /**
     * Set product related
     *
     * @access public
     * @param int $product_id
     * @param array $categories
     * @return void
     */
    public function set_product_filters($product_id, $filters = array())
    {
        foreach ($filters as $filter_id) {
            $this->db
                ->set(array(
                    'product_id' => (int)$product_id,
                    'filter_id' => (int)$filter_id
                ))->insert('product_filter');
        }
    }

    /**
     * Set product attributes
     *
     * @access public
     * @param int $product_id
     * @param array $attributes
     * @return void
     */
    public function set_product_attributes($product_id, $attributes = array())
    {
        foreach ($attributes as $attribute) {
            $this->db
                ->set(array(
                    'product_id' => (int)$product_id,
                    'attribute_id' => (int)$attribute['attribute_id'],
                    'text' => $attribute['text'],
                ))->insert('product_attribute');
        }
    }


    public function set_product_digitals($product_id, $digitals = array(), $type)
    {
        if ($type == 1) {
            $qty = count($digitals);
            $this->db
                ->set(array(
                    'quantity' => $qty,
                ))->where('product_id', $product_id)
                ->update('product');
        }
    }

    /**
     * Set product options
     *
     * @access public
     * @param int $product_id
     * @param array $options
     * @return void
     */
    public function set_product_options($product_id, $options = array())
    {
        foreach ($options as $option) {
            $this->db
                ->set(array(
                    'product_id' => (int)$product_id,
                    'option_id' => (int)$option['option_id'],
                    'option_value' => '',
                    'required' => (int)$option['required']
                ))->insert('product_option');

            $option_id = $this->db->insert_id();

            if (isset($option['product_option_value']) && count($option['product_option_value']) > 0) {
                foreach ($option['product_option_value'] as $option_value) {
                    $this->db
                        ->set(array(
                            'product_option_id' => (int)$option_id,
                            'product_id' => (int)$product_id,
                            'option_id' => (int)$option['option_id'],
                            'option_value_id' => (int)$option_value['option_value_id'],
                            'quantity' => isset($option_value['quantity']) ? (int)$option_value['quantity'] : 0,
                            'subtract' => isset($option_value['subtract']) ? (bool)$option_value['subtract'] : 0,
                            'price' => (float)$option_value['price'],
                            'points' => isset($option_value['points']) ? (int)$option_value['points'] : 0,
                            'weight' => isset($option_value['weight']) ? (float)$option_value['weight'] : 0,
                        ))->insert('product_option_value');
                }
            } else {
                $this->db
                    ->where('product_option_id', (int)$option_id)
                    ->delete('product_option');
            }
        }
    }

    /**
     * Set product tabs
     *
     * @access public
     * @param int $product_id
     * @param array $product_tabs (default: array())
     * @return void
     */
    public function set_product_tabs($product_id, $product_tabs = array())
    {
        foreach ($product_tabs as $product_tab) {
            $this->db
                ->set(array(
                    'product_tab_id' => $product_tab['product_tab_id'],
                    'product_id' => (int)$product_id,
                    'title' => $product_tab['title'],
                    'content' => $product_tab['content'],
                    'sort_order' => $product_tab['sort_order'],
                ))->insert('product_tab');
        }
    }

    /**
     * Get product discounts
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_product_discounts($product_id)
    {
        return $this->db
            ->where('product_id', (int)$product_id)
            ->order_by('quantity', 'asc')
            ->order_by('priority', 'asc')
            ->order_by('price', 'asc')
            ->get('product_discount')
            ->result_array();
    }

    /**
     * Get product images
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_product_images($product_id)
    {
        return $this->db
            ->where('product_id', (int)$product_id)
            ->get('product_image')
            ->result_array();
    }

    /**
     * Get product categories
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_product_categories($product_id)
    {
        $categories = array();

        foreach ($this->db
                     ->select('category_id')
                     ->where('product_id', (int)$product_id)
                     ->get('product_category')
                     ->result_array() as $result) {
            $categories[] = $result['category_id'];
        }

        return $categories;
    }

    /**
     * Get related products
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_product_relates($product_id)
    {
        $relates = array();

        foreach ($this->db
                     ->select('related_id')
                     ->where('product_id', (int)$product_id)
                     ->get('product_related')
                     ->result_array() as $result) {
            $relates[] = $result['related_id'];
        }

        return $relates;
    }

    /**
     * Get product filters
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_product_filters($product_id)
    {
        $filters = array();

        foreach ($this->db
                     ->select('filter_id')
                     ->where('product_id', (int)$product_id)
                     ->get('product_filter')
                     ->result_array() as $result) {
            $filters[] = $result['filter_id'];
        }

        return $filters;
    }

    /**
     * Get product attributes
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_product_attributes($product_id)
    {
        return $this->db
            ->select('pa.attribute_id, pa.text, pa.product_id, CONCAT_WS(" &raquo; ", ag.name, a.name) AS name')
            ->join('attribute a', 'a.attribute_id = pa.attribute_id', 'left')
            ->join('attribute_group ag', 'ag.attribute_group_id = a.attribute_group_id', 'left')
            ->where('product_id', (int)$product_id)
            ->group_by('pa.attribute_id')
            ->order_by('ag.attribute_group_id')
            ->get('product_attribute pa')
            ->result_array();
    }

    /**
     * Get product options
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_product_options($product_id)
    {
        $option_data = array();

        foreach ($this->db
                     ->join('option o', 'po.option_id = o.option_id', 'left')
                     ->where('po.product_id', (int)$product_id)
                     ->get('product_option po')
                     ->result_array() as $option) {
            $option_value_data = array();

            foreach ($this->db
                         ->where('product_option_id', (int)$option['product_option_id'])
                         ->get('product_option_value')
                         ->result_array() as $option_value) {
                $option_value_data[] = array(
                    'product_option_value_id' => $option_value['product_option_value_id'],
                    'option_value_id' => $option_value['option_value_id'],
                    'quantity' => $option_value['quantity'],
                    'subtract' => $option_value['subtract'],
                    'price' => $option_value['price'],
                    'points' => $option_value['points'],
                    'weight' => $option_value['weight']
                );
            }

            $option_data[] = array(
                'product_option_id' => $option['product_option_id'],
                'option_id' => $option['option_id'],
                'name' => $option['name'],
                'type' => $option['type'],
                'product_option_value' => $option_value_data,
                'option_value' => $option['option_value'],
                'required' => $option['required']
            );
        }

        return $option_data;
    }

    /**
     * Get product tabs
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_product_tabs($product_id)
    {
        return $this->db
            ->where('product_id', (int)$product_id)
            ->order_by('sort_order', 'asc')
            ->get('product_tab')
            ->result_array();
    }

    /**
     * Delete product
     *
     * @access public
     * @param int $product_id
     * @return void
     */
    public function delete($product_id = null)
    {
        if (is_array($product_id)) {
            $this->db->where_in('product_id', (array)$product_id);
        } else {
            $this->db->where('product_id', (int)$product_id);
        }

        $this->db->delete(array(
            'product',
            'product_discount',
            'product_image',
            'product_category',
            'product_related',
            'product_filter',
            'product_option',
            'product_option_value',
            'product_attribute',
            'review',
            'product_tab'
        ));

        $this->db
            ->where('related_id', (int)$product_id)
            ->delete('product_related');
    }

    /**
     * Get special products
     *
     * @access public
     * @param array $data
     * @return array
     */
    public function get_specials($data = array())
    {
        $this->db->select('p.product_id, p.name, p.slug, p.description, p.price, ps.price as discount, (SELECT pi.image FROM product_image pi WHERE pi.product_id = p.product_id LIMIT 1) as image, (SELECT AVG(rating) FROM review r1 WHERE r1.product_id = ps.product_id AND r1.active = 1 GROUP BY r1.product_id) AS rating, (SELECT quantity FROM product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.quantity > 1 AND ((pd2.date_start = \'0000-00-00\' OR pd2.date_start < NOW()) AND (pd2.date_end = \'0000-00-00\' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.quantity DESC LIMIT 1) AS wholesaler', false)
            ->from('product_discount ps')
            ->join('product p', 'ps.product_id = p.product_id', 'left')
            ->where('p.active', 1);

        $this->db
            ->where('ps.quantity', 1)
            ->where('(ps.date_start = \'0000-00-00\' || ps.date_start < NOW())', null, false)
            ->where('(ps.date_end = \'0000-00-00\' || ps.date_end > NOW())', null, false)
            ->group_by('ps.product_id');

        $sort_data = array(
            'p.name',
            'ps.price',
            'rating',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sort = $data['sort'];
        } else {
            $sort = 'p.sort_order';
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $order = 'desc';
        } else {
            $order = 'asc';
        }

        $this->db->order_by($sort, $order);

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $this->db->limit($data['limit'], $data['start']);
        }

        return $this->db->get()->result_array();
    }

    /**
     * Count special products
     *
     * @access public
     * @return array
     */
    public function count_specials()
    {
        return $this->db
            ->select('ps.product_discount_id')
            ->join('product p', 'ps.product_id = p.product_id', 'left')
            ->where('p.active', 1)
            ->where('(ps.date_start = \'0000-00-00\' || ps.date_start < NOW())', null, false)
            ->where('(ps.date_end = \'0000-00-00\' || ps.date_end > NOW())', null, false)
            ->group_by('ps.product_id')
            ->count_all_results('product_discount ps');
    }

    /**
     * Get discounts
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_discounts($product_id)
    {
        return $this->db
            ->where('product_id', (int)$product_id)
            ->where('quantity >', 1)
            ->where("((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW()))", null, false)
            ->order_by('quantity', 'asc')
            ->order_by('priority', 'asc')
            ->order_by('price', 'asc')
            ->get('product_discount')
            ->result_array();
    }

    /**
     * Get options
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_options($product_id)
    {
        $option_data = array();

        foreach ($this->db
                     ->join('option o', 'po.option_id = o.option_id', 'left')
                     ->where('po.product_id', (int)$product_id)
                     ->get('product_option po')
                     ->result_array() as $option) {
            $option_value_data = array();

            foreach ($this->db
                         ->select('pov.*, ov.name as name')
                         ->where('pov.product_option_id', (int)$option['product_option_id'])
                         ->join('option_value ov', 'ov.option_value_id = pov.option_value_id', 'left')
                         ->get('product_option_value pov')
                         ->result_array() as $option_value) {
                $option_value_data[] = array(
                    'product_option_value_id' => $option_value['product_option_value_id'],
                    'option_value_id' => $option_value['option_value_id'],
                    'name' => $option_value['name'],
                    'quantity' => $option_value['quantity'],
                    'subtract' => $option_value['subtract'],
                    'price' => $option_value['price'],
                    'points' => $option_value['points'],
                    'weight' => $option_value['weight']
                );
            }

            $option_data[] = array(
                'product_option_id' => $option['product_option_id'],
                'option_id' => $option['option_id'],
                'name' => $option['name'],
                'type' => $option['type'],
                'product_option_value' => $option_value_data,
                'option_value' => $option['option_value'],
                'required' => $option['required']
            );
        }

        return $option_data;
    }

    /**
     * Get filters
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_filters($product_id)
    {
        return $this->db
            ->select("f.filter_id, CONCAT_WS(' > ', fg.name, f.name) AS name", false)
            ->join('filter f', 'f.filter_id = pf.filter_id', 'left')
            ->join('filter_group fg', 'fg.filter_group_id = f.filter_group_id', 'left')
            ->where('pf.product_id', (int)$product_id)
            ->get('product_filter pf')
            ->result_array();
    }

    /**
     * Get attributes
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_attributes($product_id)
    {
        $attribute_groups = array();

        foreach ($this->db
                     ->join('attribute a', 'pa.attribute_id = a.attribute_id', 'left')
                     ->join('attribute_group ag', 'a.attribute_group_id = ag.attribute_group_id', 'left')
                     ->where('pa.product_id', (int)$product_id)
                     ->group_by('ag.attribute_group_id')
                     ->order_by('ag.sort_order', 'asc')
                     ->order_by('ag.name', 'asc')
                     ->get('product_attribute pa')
                     ->result_array() as $attribute_group) {
            $attributes = array();

            foreach ($this->db
                         ->select('a.attribute_id, a.name, pa.text')
                         ->join('attribute a', 'pa.attribute_id = a.attribute_id', 'left')
                         ->where('pa.product_id', (int)$product_id)
                         ->where('a.attribute_group_id', (int)$attribute_group['attribute_group_id'])
                         ->order_by('a.sort_order', 'asc')
                         ->order_by('a.name', 'asc')
                         ->get('product_attribute pa')
                         ->result_array() as $attribute) {
                $attributes[] = array(
                    'attribute_id' => $attribute['attribute_id'],
                    'name' => $attribute['name'],
                    'text' => $attribute['text']
                );
            }

            $attribute_groups[] = array(
                'attribute_group_id' => $attribute_group['attribute_group_id'],
                'name' => $attribute_group['name'],
                'attribute' => $attributes
            );
        }

        return $attribute_groups;
    }

    /**
     * Get relates
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    public function get_relates($product_id)
    {
        return $this->db
            ->select("DISTINCT *, p.name AS name, p.slug, p.description, 
			(SELECT price FROM product_discount pd WHERE pd.product_id = p.product_id AND pd.quantity = '1' AND ((pd.date_start = '0000-00-00' OR pd.date_start < NOW()) AND (pd.date_end = '0000-00-00' OR pd.date_end > NOW())) ORDER BY pd.priority ASC, pd.price ASC LIMIT 1) AS discount,
			(SELECT ss.name FROM stock_status ss WHERE ss.stock_status_id = p.stock_status_id) AS stock_status,
			(SELECT wc.unit FROM weight_class wc WHERE p.weight_class_id = wc.weight_class_id) AS weight_class,
			(SELECT AVG(rating) AS total FROM review r1 WHERE r1.product_id = p.product_id AND r1.active = '1' GROUP BY r1.product_id) AS rating,
			(SELECT COUNT(*) AS total FROM review r2 WHERE r2.product_id = p.product_id AND r2.active = '1' GROUP BY r2.product_id) AS reviews, 
			(SELECT quantity FROM product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.quantity > '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.quantity DESC LIMIT 1) AS wholesaler, p.sort_order", false)
            ->from('product_related pr')
            ->join('product p', 'p.product_id = pr.related_id', 'left')
            ->where('pr.product_id', $product_id)
            ->where('p.active', 1)
            ->get()
            ->result_array();
    }

    /**
     * Check SKU
     *
     * @access public
     * @param string $sku
     * @param int $product_id
     * @return bool
     */
    public function check_sku($sku, $product_id = null)
    {
        if ($product_id) {
            $this->db->where('product_id !=', $product_id);
        }

        return (bool)$this->db
            ->where('sku', $sku)
            ->count_all_results('product');
    }

    /**
     * Import
     *
     * @access public
     * @param array $products
     * @return void
     */
    public function import($products)
    {
        $current_products = $this->db->select('product_id')->get('product')->result_array();

        $product_ids = array();

        foreach ($current_products as $current_product) {
            $product_ids[] = $current_product['product_id'];
        }

        $old_products = array();
        $new_products = array();

        foreach ($products as $product) {
            $product['date_modified'] = date('Y-m-d H:i:s', time());

            if (in_array($product['product_id'], $product_ids)) {
                $old_products[] = $product;
            } else {
                $product['date_added'] = date('Y-m-d H:i:s', time());
                $new_products[] = $product;
            }
        }

        if ($old_products) {
            $this->db->update_batch('product', $old_products, 'product_id');
        }

        if ($new_products) {
            $this->db->insert_batch('product', $new_products);
        }
    }


    public function get_products_search($data = array())
    {
        $this->db->select('p.product_id, p.slug, (SELECT AVG(rating) AS total FROM review r1 WHERE r1.product_id = p.product_id AND r1.active = 1 GROUP BY r1.product_id) AS rating, (SELECT price FROM product_discount pd WHERE pd.product_id = p.product_id AND pd.quantity = 1 AND ((pd.date_start = \'0000-00-00\' OR pd.date_start < NOW()) AND (pd.date_end = \'0000-00-00\' OR pd.date_end > NOW())) ORDER BY pd.priority ASC, pd.price ASC LIMIT 1) AS discount');

        if (!empty($data['category_id'])) {
            if (!empty($data['sub_category'])) {
                $this->db->from('category_path cp');
                $this->db->join('product_category pc', 'pc.category_id = cp.category_id', 'left');
                $this->db->join('product p', 'p.product_id = pc.product_id', 'inner');
            } else {
                $this->db->from('product p');
                $this->db->join('product_category pc', 'p.product_id = pc.product_id', 'left');
            }
        } else {
            $this->db->from('product p');
        }

        if (!empty($data['filter'])) {
            $this->db->join('product_filter pf', 'p.product_id = pf.product_id', 'left');
        }

        if (!empty($data['search'])) {
            $search = $data['search'];
            $this->db->where("name LIKE '%$search%'");
        }

        if (!empty($data['category_id'])) {
            if (!empty($data['sub_category'])) {
                $this->db->where('cp.path_id', (int)$data['category_id']);
            } else {
                $this->db->where('(pc.category_id = ' . (int)$data['category_id'] . ' || p.category_id = ' . (int)$data['category_id'] . ')', null, false);
            }
        }

        if (!empty($data['filter'])) {
            $implode = array();

            $filters = $data['filter'];

            foreach ($filters as $filter_id) {
                $implode[] = (int)$filter_id;
            }

            $this->db->where_in('pf.filter_id', $implode);
        }

        if (!empty($data['name']) || !empty($data['tag'])) {
            if (!empty($data['name'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['name'])));

                foreach ($words as $word) {
                    $this->db->like('p.name', $word, 'both');
                }

                if (!empty($data['description'])) {
                    $this->db->or_like('p.description', $data['name'], 'both');
                }
            }
        }

        $this->db->where('p.active', 1);
        $this->db->group_by('p.product_id');

        $sort_data = array(
            'name' => 'p.name',
            'sku' => 'p.sku',
            'quantity' => 'p.quantity',
            'price' => 'p.price',
            'rating' => 'rating',
            'sort_order' => 'p.sort_order',
            'date_added' => 'p.date_added'
        );

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        if (isset($data['sort']) && isset($sort_data[$data['sort']])) {
            if ($sort_data[$data['sort']] == 'p.price') {
                $sort = '(CASE WHEN discount IS NOT NULL THEN discount ELSE p.price END)';
            } else {
                $sort = $sort_data[$data['sort']];
            }
        } else {
            $sort = 'p.name';
        }

        $order = (isset($data['order']) && ($data['order'] == 'desc')) ? 'desc' : 'asc';

        $this->db->order_by($sort, $order);

        $product_data = array();

        foreach ($this->db->get()->result_array() as $result) {
            $product_data[$result['product_id']] = $this->get_product($result['product_id']);
        }

        return $product_data;
    }
}