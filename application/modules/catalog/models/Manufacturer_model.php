<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturer_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Validate slug
     *
     * @access public
     * @param string $slug
     * @param int $manufacturer_id
     * @param int $counter
     * @return string
     */
    public function validate_slug($slug = '', $manufacturer_id = null, $counter = null)
    {
        if ($manufacturer_id) {
            $this->db->where('manufacturer_id !=', $manufacturer_id);
        }

        $count = $this->db
            ->select('slug')
            ->from('manufacturer')
            ->where('slug', $slug . ($counter ? '-' . $counter : ''))
            ->count_all_results();

        if ($count > 0) {
            if (!$counter) {
                $counter = 1;
            } else {
                $counter++;
            }

            return $this->validate_slug($slug, $manufacturer_id, $counter);
        } else {
            return $slug . ($counter ? '-' . $counter : '');
        }
    }
}