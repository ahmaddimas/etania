<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get review
     *
     * @access public
     * @param int $review_id
     * @return array
     */
    public function get_review($review_id)
    {
        return $this->db
            ->select('r.*, p.name as product, c.name as author')
            ->from('review r')
            ->join('product p', 'r.product_id = p.product_id', 'left')
            ->join('customer c', 'c.customer_id = r.customer_id', 'left')
            ->where('r.review_id', (int)$review_id)
            ->get()
            ->row_array();
    }
}