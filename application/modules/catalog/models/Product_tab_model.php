<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_tab_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
}