<div class="page-title">
    <div>
        <h1>Banner</h1>
        <p>Kelola banner</p>
    </div>
    <div class="btn-group">
        <a onclick="window.location = '<?= admin_url('banner/create') ?>';" class="btn btn-success"><i
                    class="fa fa-plus"></i> Buat Baru</a>
        <a onclick="refreshTable();" class="btn btn-info"><i class="fa fa-refresh"></i> Refresh</a>
        <a href="<?= current_url() ?>#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><i
                    class="fa fa-check-square-o"></i> <?= lang('button_action') ?> <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="<?= current_url() ?>#" onclick="bulk.active();"><?= lang('button_enable') ?></a></li>
            <li><a href="<?= current_url() ?>#" onclick="bulk.inactive();"><?= lang('button_disable') ?></a></li>
            <li><a href="<?= current_url() ?>#" onclick="bulk.delete();"><?= lang('button_delete') ?></a></li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div id="message"></div>
            <?php if ($success) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i> <?= $success ?></div>
            <?php } ?>
            <?php if ($error) { ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-ban"></i> <?= $error ?></div>
            <?php } ?>
            <div class="card-body">
                <?= form_open(admin_url('banner/delete'), 'id="bulk-action"') ?>
                <table class="table table-hover table-bordered" id="datatable" width="100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="select-all"></th>
                        <th>Name</th>
                        <th>Deskripsi</th>
                        <th>Total Foto</th>
                        <th>Status</th>
                        <th class="text-right"></th>
                    </tr>
                    </thead>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('banner')?>",
                "type": "POST",
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "banner_id"},
                {"data": "name"},
                {"data": "description"},
                {"data": "images"},
                {"data": "active"},
                {"orderable": false, "searchable": false, "data": "banner_id"},
            ],
            "sDom": '<"table-responsive" t>p',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<input type="checkbox" class="checkbox" name="banner_id[]" value="' + data.banner_id + '"/>');

                if (data.active === '1') {
                    $('td', row).eq(4).html('<span class="label label-success"><?=lang('text_enabled')?></span>');
                } else {
                    $('td', row).eq(4).html('<span class="label label-default"><?=lang('text_disabled')?></span>');
                }

                $('td', row).eq(5).html('<a href="<?=admin_url('banner/edit/\'+data.banner_id+\'')?>"><i class="fa fa-cog"></i> <?=lang('button_edit')?></a>').addClass('text-right');
            },
            "order": [[0, 'asc']]
        });

        $('#select-all').click(function () {
            var checkBoxes = $('.checkbox');
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });
    });

    var bulk = {
        'active': function () {
            action({
                text: "<?=lang('text_confirm_enable')?>",
                action: 'active',
            });
        },
        'inactive': function () {
            action({
                text: "<?=lang('text_confirm_disable')?>",
                action: 'inactive',
            });
        },
        'delete': function () {
            action({
                text: "<?=lang('text_confirm_delete')?>",
                action: 'delete',
            });
        },
    };

    function action(param) {
        swal({
            title: '<?=lang('text_warning')?>',
            text: param.text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('banner/\'+param.action+\'')?>',
                    type: 'post',
                    data: $('#bulk-action').serialize(),
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
        $('#select-all').prop('checked', false);
    }
</script>