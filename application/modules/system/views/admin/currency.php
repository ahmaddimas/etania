<section class="content-header">
    <h1><?= $template['title'] ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?= admin_url() ?>">Home</a></li>
        <li class="active"><?= $template['title'] ?></li>
    </ol>
</section>
<section class="content">
    <div id="message"></div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <a onclick="getForm(null, '/localisation/currencies/create');" class="btn btn-success"><i
                        class="fa fa-plus-circle"></i> Tambah</a>
            <a onclick="refreshTable();" class="btn btn-info"><i class="fa fa-refresh"></i> Refresh</a>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover table-bordered" id="datatable">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Kode</th>
                            <th>Status</th>
                            <th class="text-right"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="modal"></div>

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('localisation/currencies')?>",
                "type": "POST",
            },
            "columns": [
                {"data": "title"},
                {"data": "code"},
                {"data": "active"},
                {"orderable": false, "searchable": false, "data": "currency_id"},
            ],
            "sDom": 'lfr<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                html = '<div class="btn-group">';
                html += '<a onclick="getForm(' + data.currency_id + ', \'/localisation/currencies/edit\');" class="btn btn-default btn-xs">Edit</a>';
                html += '<a onclick="delRecord(' + data.currency_id + ');" class="btn btn-danger btn-xs">Hapus</a> ';
                html += '</div>';
                $('td', row).eq(3).addClass('text-right').html(html);
                if (data.active === '1') {
                    $('td', row).eq(2).html('<span class="label label-success">Aktif</span>');
                } else {
                    $('td', row).eq(2).html('<span class="label label-default">Non Aktif</span>');
                }
            },
            "order": [[0, 'asc']],
            "language": {
                "url": "<?=secure_url('assets/plugins/datatables/locales/Indonesian.json')?>",
            }
        });
    });

    function delRecord(currency_id) {
        if (confirm('Anda yakin ingin menghapus?')) {
            $.ajax({
                url: '<?=admin_url('localisation/currencies/delete')?>',
                type: 'post',
                data: 'currency_id=' + currency_id,
                dataType: 'json',
                success: function (json) {
                    if (json['success']) {
                        $('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                    } else if (json['error']) {
                        $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
                    } else if (json['redirect']) {
                        window.location = json['redirect'];
                    }

                    refreshTable();
                }
            });
        }
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }

    function getForm(currency_id, path) {
        $.ajax({
            url: $('base').attr('href') + path,
            data: 'currency_id=' + currency_id,
            dataType: 'json',
            success: function (json) {
                if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
                } else if (json['content']) {
                    $('#modal').html('<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['content'] + '</div>');
                    $('#form-modal').modal('show');
                    $('#form-modal').on('hidden.bs.modal', function (e) {
                        refreshTable();
                    });
                }
            }
        });
    }
</script>