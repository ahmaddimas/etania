<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $template['title'] ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="currency_id" value="<?= $currency_id ?>">
            <div class="form-group">
                <label class="control-label col-sm-4">Nama<br><span class="help">Nama mata uang</span></label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="title" value="<?= $title ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Kode<br><span
                            class="help">3 digit kode standar mata uang</span></label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="code" value="<?= $code ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Simbol<br><span class="help">Simbol mata uang</span></label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="symbol" value="<?= $symbol ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Nilai<br><span class="help">Nilai konversi terhadap mata uang default. Jika ini adalah default, isikan 1.000000</span></label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="value" value="<?= $value ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Status</label>
                <div class="col-sm-8">
                    <div class="checkbox">
                        <label>
                            <?php if ($active) { ?>
                                <input type="checkbox" name="active" value="1" checked="checked"> Aktif
                            <?php } else { ?>
                                <input type="checkbox" name="active" value="1"> Aktif
                            <?php } ?>
                        </label>
                    </div>
                </div>
            </div>

            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
            <button type="button" class="btn btn-success" id="submit"><i class="fa fa-check"></i> Simpan</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });
</script>