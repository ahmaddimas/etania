<div class="page-title">
    <div>
        <h1><?= $heading_title ?></h1>
    </div>
    <div class="btn-group">
        <a onclick="window.location = '<?= admin_url('system/payments') ?>';" class="btn btn-default"><i
                    class="fa fa-reply"></i> Batal</a>
        <button id="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <?= form_open($action, 'role="form" id="form" class="form-horizontal"') ?>
                <?= $form ?>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $this = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                $($this).attr('disabled', true);
                $($this).append('<span class="wait"> <i class="fa fa-refresh fa-spin"></i></span>');
            },
            complete: function () {
                $($this).attr('disabled', false);
                $('.wait').remove();
            },
            success: function (json) {
                $('.alert, .error').remove();
                $('.form-group').removeClass('has-error');
                if (json['redirect']) {
                    window.location = json['redirect'];
                } else if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json.error + '</div>');
                    $('html, body').animate({scrollTop: 0}, 'slow');
                } else if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').after('<span class="help-block error">' + json['errors'][i] + '</span>');
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                    }
                }
            }
        });
    });
</script>