<div class="page-title">
    <div>
        <h1>Template Halaman</h1>
    </div>
    <div class="btn-group">
        <button type="button" data-toggle="modal" data-target="#template" class="btn btn-success"><i
                    class="fa fa-plus"></i> Tambah
        </button>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</a>
    </div>
</div>
<section class="content">
    <div class="card">
        <div class="card-body">
            <div id="message"></div>
            <?php if ($success) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i> <?= $success ?></div>
            <?php } ?>
            <?php if ($error) { ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-ban"></i> <?= $error ?></div>
            <?php } ?>
            <h3>Template List</h3>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover table-bordered" width="100%" id="datatable">
                        <thead>
                        <tr>
                            <th style="width:25%">Nama</th>
                            <th>Picture</th>
                            <th style="width:25%">Description</th>
                            <th style="width:15%">Status</th>
                            <th style="width:15%">Aksi</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


<div class="modal fade" id="template" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="system/template/add" method="post" file="true" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Tambah Template</h4>
                </div>
                <div class="modal-body">
                    <input type="file" class="form-control" style="width:100%" placeholder="Addons zip file"
                           name="template" value="" required/>
                </div>
                <div class="modal-footer">
                    <span class="pull-left"><font
                                color="red">* Please enable zip archive module on PHP setting</font></span>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Batal
                    </button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Tambah template</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modal"></div>

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('system/template/desktop')?>",
                "type": "POST",
            },
            "columns": [
                {"data": "name"},
                {"data": "image"},
                {"data": "note"},
                {"data": "status"},
                {"data": "status"},
            ],
            "createdRow": function (row, data, index) {

                $('td', row).eq(1).html('<img src="<?=base_url('\'+data.image+\'')?>" title="" alt="" class="img-responsive">');


                if (data.status === '1') {
                    $('td', row).eq(3).html('<span class="label label-success">Active</span>');
                } else {
                    $('td', row).eq(3).html('<span class="label label-default">Not Active</span>');
                }


                if (data.status === '1') {
                    $('td', row).eq(4).html('<div class="btn-group btn-group-xs"><a class="btn btn-default btn-xs disabled"><i class="fa fa-cog"></i> Activate</a></div>');
                } else {
                    $('td', row).eq(4).html('<div class="btn-group btn-group-xs"><a href="<?=admin_url('system/template/setactive/\'+data.template_id+\'')?>" class="btn btn-success btn-xs"><i class="fa fa-cog"></i> Activate</a> <a onclick="deletetemplate(\'' + data.template_id + '\');" class="btn btn-default btn-xs"><i class="fa fa-trash"></i> Delete</a></div>');
                }

            },
            "order": [[0, 'asc']],
        });
    });


    function deletetemplate(template_id) {
        swal({
            title: '<?=lang('text_warning')?>',
            text: '<?=lang('text_confirm_delete')?>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: "<?=admin_url('system/template/delete')?>",
                    type: 'post',
                    data: 'template_id=' + template_id,
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }

                        refreshTable();
                    }
                });
            }
        });
    }


    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }
</script>