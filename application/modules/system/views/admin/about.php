<div class="page-title">
    <div>
        <h1>About</h1>
    </div>
    <div class="btn-group">
    </div>
</div>
<section class="content">
    <div class="card">
        <div class="card-body">
            <?php /*if ($success) {*/ ?><!--
			<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> <? /*=$success*/ ?></div>
			<?php /*}*/ ?>
			<?php /*if ($error) {*/ ?>
			<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> <? /*=$error*/ ?></div>
			--><?php /*}*/ ?>
            <div class="alert alert-dismissible hidden" id="message">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-check"></i> <span data-toggle="message"></span></div>
            <div id="message-wrapper"></div>
            <div class="box">
                <div class="box-body table-responsive" style="padding:15px">
                    <p>
                    <h4>E Commerce App</h4>
                    Sebuah apikasi toko online yang memungkinkan kita mengelola sendiri produk produk yang akan di jual
                    pada halaman utama aplikasi.
                    <legend></legend>
                    <br>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-1" data-toggle="tab" id="default">Petunjuk</a></li>
                        <li><a href="#tab-2" data-toggle="tab">System Update</a></li>
                    </ul>
                    <div class="tab-content" style="padding-top:20px;">
                        <div class="tab-pane active" id="tab-1">
                            <a target="_blank" href="<?= admin_url($version_manual_book) ?>">
                                <button class="btn btn-warning "><span class="fa fa-book"></span> Baca buku petunjuk
                                </button>
                            </a>
                            <button class="btn btn-danger"><span class="fa fa-info"></span> Laporkan bug</button>
                        </div>
                        <div class="tab-pane" id="tab-2">
                            Version <?= $version_no ?> build <?= date('Ymd', strtotime($version_date)) ?>
                            <br>
                            <br>
                            <button class="btn btn-success" role="check-update">
                                <i class="fa fa-refresh fa-spin hidden"></i>
                                Check Update
                            </button>
                            <br><br>
                            <table class="table table-stripped table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Type</th>
                                    <th>Current Version</th>
                                    <th>Update Version</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="list-update"></tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    E Commerce App
                    <br>
                    Copyright ©<?= date('Y') ?> E Commerce App. All rights reserved.
                    </p>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>


<script type="text/javascript">
    function submit() {

    }

    $(document).ready(function () {
        $('table').hide();

        $('button[role="check-update"]').click(function (event) {
            $('#list-update').empty();
            var e = $(this);
            e.prop('disabled', true).find('i.fa').removeClass('hidden');
            $.ajax({
                url: 'system/about/checkUpdate',
                type: 'POST',
                dataType: 'json',
                success: function (res) {
                    if (res.status) {
                        var rows = res.data.map((item) => {
                            let type = item.type === 'installer' ? 'system' : item.type;
                            return "<tr>" +
                                "<td>" + item.name + "</td>" +
                                "<td>" + item.desc + "</td>" +
                                "<td>" + type + "</td>" +
                                "<td>" + item.cur_version + "</td>" +
                                "<td>" + item.up_version + "</td>" +
                                "<td><button class='btn btn-info' data-id='" + item.code + "' data-type='" + item.type + "' role='update-module'>" +
                                "<i class='fa fa-refresh fa-spin hidden'></i>" +
                                "Update" +
                                "</button></td>" +
                                "</tr>";
                        });
                        $('#list-update').append(rows);
                        $('table').slideDown();
                    } else {
                        swal('Info', 'No update available\n Your system is up to date', 'info');
                    }
                },
                error: function (xhr, status, error) {
                    swal('Error', 'Some error occured, please try again later', 'error');
                },
                complete: function () {
                    e.prop('disabled', false).find('i.fa').addClass('hidden');
                }
            });
        });
        $(document).on('click', 'button[role="update-module"]', function (e) {
            let btn = $(this);
            let code = btn.attr('data-id');
            let type = btn.attr('data-type');
            btn.prop('disabled', true).find('i.fa').removeClass('hidden');
            swal({
                title: 'Attention',
                type: 'warning',
                text: 'Are you sure to update this module?',
                showCancelButton: true,
                confirmButtonText: 'Update',
                closeOnConfirm: false
            }, function (confirm) {
                if (confirm) {
                    swal({
                        title: 'Warning',
                        text: 'Please don\'t close or reload this window',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    $.ajax({
                        url: 'system/about/update',
                        type: 'POST',
                        data: {'code': code, 'type': type},
                        dataType: 'json',
                        success: function (res) {
                            let className = res.status ? 'success' : 'danger';
                            let msg = res.message;
                            const message = $('#message').clone().removeClass('hidden').addClass('alert-' + className);
                            message.children('[data-toggle="message"]').html('<b>' + code + '</b>, ' + msg);
                            $('#message-wrapper').append(message);

                            if (res.status) {
                                btn.parents('tr').remove();
                            }
                        },
                        error: function (xhr, status, error) {
                            swal('Error', 'Some error occured, please try again later', 'error');
                        },
                        complete: function () {
                            btn.prop('disabled', false).find('i.fa').addClass('hidden');
                        }
                    });
                }
            });
        });
    });
</script>