<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a onclick="getForm(null, '/system/length_class/create');" class="btn btn-success"><i
                    class="fa fa-plus-circle"></i> <?= lang('button_add') ?></a>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> <?= lang('button_refresh') ?>
        </a>
        <a onclick="delRecord();" class="btn btn-danger dropdown-toggle"><i
                    class="fa fa-trash"></i> <?= lang('button_delete') ?></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?= form_open(admin_url('system/length_class/delete'), 'id="bulk-action"') ?>
                <table class="table table-hover table-bordered" id="datatable" width="100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="select-all"></th>
                        <th>Nama</th>
                        <th>Satuan</th>
                        <th>Nilai</th>
                        <th class="text-right"></th>
                    </tr>
                    </thead>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="modal"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('system/length_class')?>",
                "type": "POST",
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "length_class_id"},
                {"data": "title"},
                {"data": "unit"},
                {"data": "value"},
                {"orderable": false, "searchable": false, "data": "length_class_id"},
            ],
            "sDom": 'lfr<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<input type="checkbox" class="checkbox" name="length_class_id[]" value="' + data.length_class_id + '"/>');
                $('td', row).eq(4).html('<a style="cursor:pointer;" onclick="getForm(' + data.length_class_id + ', \'system/length_class/edit\');"><i class="fa fa-cog"></i> <?=lang('button_edit')?></a>').addClass('text-right');
            },
            "order": [[4, 'asc']],
        });

        $('#select-all').click(function () {
            var checkBoxes = $('.checkbox');
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });
    });

    function delRecord() {
        swal({
            title: '<?=lang('text_warning')?>',
            text: '<?=lang('text_confirm_delete')?>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('system/length_class/delete')?>',
                    type: 'post',
                    data: $('#bulk-action').serialize(),
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
        $('#select-all').prop('checked', false);
    }

    function getForm(length_class_id, path) {
        $.ajax({
            url: $('base').attr('href') + path,
            data: 'length_class_id=' + length_class_id,
            dataType: 'json',
            success: function (json) {
                if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
                } else if (json['content']) {
                    $('#modal').html('<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['content'] + '</div>');
                    $('#form-modal').modal('show');
                    $('#form-modal').on('hidden.bs.modal', function (e) {
                        refreshTable();
                    });
                }
            }
        });
    }
</script>