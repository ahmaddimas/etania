<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $template['title'] ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="length_class_id" value="<?= $length_class_id ?>">
            <div class="form-group">
                <label class="control-label col-sm-4">Nama<br><span class="help">Nama satuan panjang</span></label>
                <div class=" col-sm-8">
                    <input type="text" class="form-control" name="title" value="<?= $title ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Satuan<br><span class="help">Kode satuan panjang</span></label>
                <div class=" col-sm-8">
                    <input type="text" class="form-control" name="unit" value="<?= $unit ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Nilai<br><span class="help">Nilai konversi terhadap satuan default. Jika ini adalah default, isikan 1.0000</span></label>
                <div class=" col-sm-8">
                    <input type="text" class="form-control" name="value" value="<?= $value ?>">
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
            <button type="button" class="btn btn-success" id="submit"><i class="fa fa-check"></i> Simpan</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });
</script>