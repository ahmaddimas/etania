<div class="page-title">
    <div>
        <h1>Modul Add Ons</h1>
    </div>
    <div class="btn-group">
        <button type="button" data-toggle="modal" data-target="#payment" class="btn btn-success"><i
                    class="fa fa-plus"></i> Tambah
        </button>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</a>
    </div>
</div>
<section class="content">
    <div class="card">
        <div class="card-body">
            <div id="message"></div>
            <?php if ($success) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i> <?= $success ?></div>
            <?php } ?>
            <?php if ($error) { ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-ban"></i> <?= $error ?></div>
            <?php } ?>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover table-bordered" width="100%" id="datatable">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Deskripsi</th>
                            <th>Versi</th>
                            <th>Status</th>
                            <th style="width:20%;"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?= admin_url('system/payments/add') ?>" method="post" file="true"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Tambah Payment</h4>
                </div>
                <div class="modal-body">
                    <input type="file" class="form-control" style="width:100%" placeholder="Payment zip file"
                           name="payment" value="" required/>
                </div>
                <div class="modal-footer">
                    <span class="pull-left"><font
                                color="red">* Please enable zip archive module on PHP setting</font></span>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Batal
                    </button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Tambah addons</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modal"></div>

<div id="modal"></div>

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('system/payments')?>",
                "type": "POST",
            },
            "columns": [
                {"data": "name"},
                {"data": "description"},
                {"data": "version"},
                {"data": "active"},
                {"orderable": false, "searchable": false, "data": "code"},
            ],
            "createdRow": function (row, data, index) {
                if (data.active === '1') {
                    $('td', row).eq(3).html('<span class="label label-success">Installed</span>');
                    $('td', row).eq(4).html('<div class="btn-group btn-group-xs"><a href="<?=admin_url('payment/\'+data.code+\'')?>" class="btn btn-default btn-xs"><i class="fa fa-cog"></i> Edit</a><a onclick="uninstall(\'' + data.code + '\');" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> Uninstall</a></div>').addClass('text-right');

                } else {
                    $('td', row).eq(4).html('<div class="btn-group btn-group-xs"><a onclick="install(\'' + data.code + '\');" class="btn btn-success btn-xs"><i class="fa fa-cog"></i> Install</a> <a onclick="deletepayment(\'' + data.code + '\');" class="btn btn-default btn-xs"><i class="fa fa-trash"></i> Delete</a></div>').addClass('text-right');
                    $('td', row).eq(3).html('<span class="label label-default">Not Installed</span>');
                }
            },
            "order": [[0, 'asc']],
        });
    });

    function install(code) {
        $.ajax({
            url: "<?=admin_url('system/payments/install')?>",
            type: 'post',
            data: 'code=' + code,
            dataType: 'json',
            success: function (json) {
                if (json['success']) {
                    $('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                } else if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
                } else if (json['redirect']) {
                    window.location = json['redirect'];
                }

                refreshTable();
            }
        });
    }

    function uninstall(code) {
        swal({
            title: '<?=lang('text_warning')?>',
            text: '<?=lang('text_confirm_disable')?>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: "<?=admin_url('system/payments/uninstall')?>",
                    type: 'post',
                    data: 'code=' + code,
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }

                        refreshTable();
                    }
                });
            }
        });
    }


    function deletepayment(code) {
        swal({
            title: '<?=lang('text_warning')?>',
            text: '<?=lang('text_confirm_delete')?>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: "<?=admin_url('system/payments/delete')?>",
                    type: 'post',
                    data: 'code=' + code,
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }

                        refreshTable();
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }
</script>