<div class="page-title">
    <div>
        <h1>Pengaturan</h1>
    </div>
    <div>
        <button id="submit" class="btn btn-success"><i class="fa fa-lg fa-check"></i> Simpan</button>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?= form_open($action, 'id="form" class="form-horizontal"') ?>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab" id="default">Instansi</a></li>
                    <li><a href="#tab-2" data-toggle="tab">Email</a></li>
                    <li><a href="#tab-3" data-toggle="tab">Opsi</a></li>
                    <li><a href="#tab-4" data-toggle="tab">SEO</a></li>
                    <li><a href="#tab-5" data-toggle="tab">Pengiriman</a></li>
                    <li><a href="#tab-6" data-toggle="tab">Social Login</a></li>
                    <li><a href="#tab-7" data-toggle="tab">Status Order</a></li>
                    <li><a href="#tab-8" data-toggle="tab">Homepage</a></li>
                    <li><a href="#tab-9" data-toggle="tab">Website</a></li>
                    <li id="delivery_tab" <?php if (isset($delivery_courier) && $delivery_courier == 1) {
                        echo "style=display:block";
                    } else {
                        echo "style=display:none";
                    } ?>><a href="#tab-10" data-toggle="tab">Delivery Cost</a></li>
                </ul>
                <div class="tab-content" style="padding-top:20px;">
                    <div class="tab-pane active" id="tab-1">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Nama Perusahaan</label>
                            <div class="col-sm-9">
                                <input type="text" name="company" value="<?= isset($company) ? $company : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Alamat</label>
                            <div class="col-sm-9">
                                <textarea name="address" rows="5"
                                          class="form-control"><?= isset($address) ? $address : '' ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">No. Telepon</label>
                            <div class="col-sm-9">
                                <input type="text" name="telephone" value="<?= isset($telephone) ? $telephone : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">No. Faks</label>
                            <div class="col-sm-9">
                                <input type="text" name="fax" value="<?= isset($fax) ? $fax : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Email</label>
                            <div class="col-sm-9">
                                <input type="text" name="email" value="<?= isset($email) ? $email : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Logo</label>
                            <div class="col-sm-3">
                                <img id="image-logo" src="<?= $thumb_logo ?>" class="img-thumbnail"
                                     data-placeholder="<?= $no_image ?>">
                                <div class="caption" style="margin-top:10px;">
                                    <button type="button" class="btn btn-default btn-block" id="upload-logo">Ubah Logo
                                    </button>
                                </div>
                                <input type="hidden" id="input-logo" name="logo" value="<?= $logo ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Favicon</label>
                            <div class="col-sm-3">
                                <img id="image-favicon" src="<?= $thumb_favicon ?>" class="img-thumbnail"
                                     data-placeholder="<?= $no_image ?>">
                                <div class="caption" style="margin-top:10px;">
                                    <button type="button" class="btn btn-default btn-block" id="upload-favicon">Ubah
                                        Favicon
                                    </button>
                                </div>
                                <input type="hidden" id="input-favicon" name="favicon" value="<?= $favicon ?>">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-2">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email Pengirim</label>
                            <div class="col-sm-4">
                                <input type="text" name="smtp_email"
                                       value="<?= isset($smtp_email) ? $smtp_email : '' ?>" class="form-control"
                                       placeholder="example: noreply@domain.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">SMTP Host</label>
                            <div class="col-sm-4">
                                <input type="text" name="smtp_host" value="<?= isset($smtp_host) ? $smtp_host : '' ?>"
                                       class="form-control" placeholder="example: ssl://smtp.domain.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">SMTP Port</label>
                            <div class="col-sm-4">
                                <input type="text" name="smtp_port" value="<?= isset($smtp_port) ? $smtp_port : '' ?>"
                                       class="form-control" placeholder="example: 465 atau 587">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">SMTP Username</label>
                            <div class="col-sm-4">
                                <input type="text" name="smtp_user" value="<?= isset($smtp_user) ? $smtp_user : '' ?>"
                                       class="form-control" placeholder="example: noreply@domain.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">SMTP Password</label>
                            <div class="col-sm-4">
                                <input type="password" name="smtp_pass"
                                       value="<?= isset($smtp_pass) ? $smtp_pass : '' ?>" class="form-control"
                                       placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-3">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Grup Pelanggan Default<br><span class="help">Ketika mendaftar, customer baru akan masuk ke grup pelanggan ini</span></label>
                            <div class="col-sm-3">
                                <select name="customer_group_id" class="form-control">
                                    <?php foreach ($customer_groups as $customer_group) { ?>
                                        <option value="<?= $customer_group['customer_group_id'] ?>"><?= $customer_group['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Satuan Berat<br><span class="help">Satuan berat default</span></label>
                            <div class="col-sm-3">
                                <select name="weight_class_id" class="form-control">
                                    <?php foreach ($weight_classes as $weight_class) { ?>
                                        <?php if ($weight_class['weight_class_id'] == $weight_class_id) { ?>
                                            <option value="<?= $weight_class['weight_class_id'] ?>"
                                                    selected="selected"><?= $weight_class['title'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $weight_class['weight_class_id'] ?>"><?= $weight_class['title'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Satuan Panjang<br><span class="help">Satuan panjang default</span></label>
                            <div class="col-sm-3">
                                <select name="length_class_id" class="form-control">
                                    <?php foreach ($length_classes as $length_class) { ?>
                                        <?php if ($length_class['length_class_id'] == $length_class_id) { ?>
                                            <option value="<?= $length_class['length_class_id'] ?>"
                                                    selected="selected"><?= $length_class['title'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $length_class['length_class_id'] ?>"><?= $length_class['title'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Script Sharing Button</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="8"
                                          name="sharing_button_code"><?= isset($sharing_button_code) ? $sharing_button_code : '' ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Script LiveChat</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="8"
                                          name="livechat_code"><?= isset($livechat_code) ? $livechat_code : '' ?></textarea>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong class="text-yellow">Social Media</strong>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Facebook Account<br><span class="help">Facebook user account</span></label>
                                    <div class="col-sm-3">
                                        <input type="text" name="facebook_user"
                                               value="<?= isset($facebook_user) ? $facebook_user : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Twitter Account<br><span class="help">Twitter user account</span></label>
                                    <div class="col-sm-3">
                                        <input type="text" name="twitter_user"
                                               value="<?= isset($twitter_user) ? $twitter_user : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Instagram Account<br><span class="help">Instagram user account</span></label>
                                    <div class="col-sm-3">
                                        <input type="text" name="instagram_user"
                                               value="<?= isset($instagram_user) ? $instagram_user : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Google+ Account<br><span class="help">Google+ user account</span></label>
                                    <div class="col-sm-3">
                                        <input type="text" name="gplus_user"
                                               value="<?= isset($gplus_user) ? $gplus_user : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Youtube Channel<br><span class="help">Youtube Channel account</span></label>
                                    <div class="col-sm-3">
                                        <input type="text" name="youtube_user"
                                               value="<?= isset($youtube_user) ? $youtube_user : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-4">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Nama Website</label>
                            <div class="col-sm-9">
                                <input type="text" name="site_name" value="<?= isset($site_name) ? $site_name : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">SEO Title</label>
                            <div class="col-sm-9">
                                <input type="text" name="seo_title" value="<?= isset($seo_title) ? $seo_title : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Meta Keywords</label>
                            <div class="col-sm-9">
                                <textarea name="meta_keywords" class="form-control"
                                          rows="5"><?= isset($meta_keywords) ? $meta_keywords : '' ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Meta Description</label>
                            <div class="col-sm-9">
                                <textarea name="meta_description" class="form-control"
                                          rows="5"><?= isset($meta_description) ? $meta_description : '' ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-5">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-city">Kunci API Pro</label>
                            <div class="col-sm-6">
                                <input type="text" name="rajaongkir_api_key"
                                       value="<?= isset($rajaongkir_api_key) ? $rajaongkir_api_key : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-province">Provinsi</label>
                            <div class="col-sm-6">
                                <select name="province_id" class="form-control">
                                    <?php foreach ($provinces as $province) { ?>
                                        <?php if ($province['province_id'] == $province_id) { ?>
                                            <option value="<?= $province['province_id'] ?>"
                                                    selected="selected"><?= $province['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $province['province_id'] ?>"><?= $province['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-city">Kota / Kabupaten</label>
                            <div class="col-sm-6">
                                <select name="city_id" class="form-control"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-subdistrict">Kecamatan</label>
                            <div class="col-sm-6">
                                <select name="subdistrict_id" class="form-control"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Satuan Berat<br><span class="help">Satuan berat pengiriman default</span></label>
                            <div class="col-sm-3">
                                <select name="rajaongkir_weight_class_id" class="form-control">
                                    <?php foreach ($weight_classes as $weight_class) { ?>
                                        <?php if ($weight_class['weight_class_id'] == $rajaongkir_weight_class_id) { ?>
                                            <option value="<?= $weight_class['weight_class_id'] ?>"
                                                    selected="selected"><?= $weight_class['title'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $weight_class['weight_class_id'] ?>"><?= $weight_class['title'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group">
                            <label class="control-label col-sm-3">Delivery<br><span class="help">Jasa pengiriman toko, setting biaya hanya untuk area kota yang sama dengan toko</span></label>
                            <div class="col-sm-9">
                                <div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" onchange="delivery()" name="delivery_courier"
                                                   id="delivery_courier"
                                                   value="1" <?php if (isset($delivery_courier) && $delivery_courier == 1) echo "checked=checked"; ?>>
                                            Company Delivery
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dukungan Kurir<br><span class="help">Pilih kurir-kurir yang dapat dipilih oleh pembeli</span></label>
                            <div class="col-sm-9">
                                <div>
                                    <?php foreach ($couriers as $courier_code => $courier) { ?>
                                        <div class="checkbox">
                                            <label>
                                                <?php if (in_array($courier_code, $rajaongkir_courier)) { ?>
                                                    <input type="checkbox" name="rajaongkir_courier[]"
                                                           value="<?php echo $courier_code; ?>" checked="checked"/>
                                                    <?php echo $courier; ?>
                                                <?php } else { ?>
                                                    <input type="checkbox" name="rajaongkir_courier[]"
                                                           value="<?php echo $courier_code; ?>"/>
                                                    <?php echo $courier; ?>
                                                <?php } ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong class="text-yellow">Facebook</strong>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Status</label>
                                    <div class="toggle lg col-sm-4">
                                        <label style="margin-top:5px;">
                                            <?php if (isset($oauth_facebook_status) && $oauth_facebook_status == 1) { ?>
                                                <input type="checkbox" name="oauth_facebook_status" value="1"
                                                       checked="checked">
                                            <?php } else { ?>
                                                <input type="checkbox" name="oauth_facebook_status" value="1">
                                            <?php } ?>
                                            <span class="button-indecator"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Application ID</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="facebook_app_id"
                                               value="<?= isset($facebook_app_id) ? $facebook_app_id : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Secret Key</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="facebook_app_secret"
                                               value="<?= isset($facebook_app_secret) ? $facebook_app_secret : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3">Authorized redirect URIs</label>
                                    <div class="col-sm-9">
                                        <div class="input-group" style="margin-top:10px">
                                            https://www.domain.com/customer/facebook_login/callback
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong class="text-yellow">Google</strong>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Status</label>
                                    <div class="toggle lg col-sm-4">
                                        <label style="margin-top:5px;">
                                            <?php if (isset($oauth_google_status) && $oauth_google_status == 1) { ?>
                                                <input type="checkbox" name="oauth_google_status" value="1"
                                                       checked="checked">
                                            <?php } else { ?>
                                                <input type="checkbox" name="oauth_google_status" value="1">
                                            <?php } ?>
                                            <span class="button-indecator"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Client ID</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="google_client_id"
                                               value="<?= isset($google_client_id) ? $google_client_id : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Client Secret</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="google_client_secret"
                                               value="<?= isset($google_client_secret) ? $google_client_secret : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Authorized redirect URIs</label>
                                    <div class="col-sm-9">
                                        <div class="input-group" style="margin-top:10px">
                                            https://www.domain.com/customer/google_login
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong class="text-yellow">Twitter</strong>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Status</label>
                                    <div class="toggle lg col-sm-4">
                                        <label style="margin-top:5px;">
                                            <?php if (isset($oauth_twitter_status) && $oauth_twitter_status == 1) { ?>
                                                <input type="checkbox" name="oauth_twitter_status" value="1"
                                                       checked="checked">
                                            <?php } else { ?>
                                                <input type="checkbox" name="oauth_twitter_status" value="1">
                                            <?php } ?>
                                            <span class="button-indecator"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Consumer Key (API Key)</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="twitter_consumer_key"
                                               value="<?= isset($twitter_consumer_key) ? $twitter_consumer_key : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Consumer Secret (API Secret)</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="twitter_consumer_secret"
                                               value="<?= isset($twitter_consumer_secret) ? $twitter_consumer_secret : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Authorized redirect URIs</label>
                                    <div class="col-sm-9">
                                        <div class="input-group" style="margin-top:10px">
                                            https://www.domain.com/customer/twitter_login
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong class="text-yellow">Instagram</strong>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Status</label>
                                    <div class="toggle lg col-sm-4">
                                        <label style="margin-top:5px;">
                                            <?php if (isset($oauth_instagram_status) && $oauth_instagram_status == 1) { ?>
                                                <input type="checkbox" name="oauth_instagram_status" value="1"
                                                       checked="checked">
                                            <?php } else { ?>
                                                <input type="checkbox" name="oauth_instagram_status" value="1">
                                            <?php } ?>
                                            <span class="button-indecator"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Client ID</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="instagram_client_id"
                                               value="<?= isset($instagram_client_id) ? $instagram_client_id : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Client Secret</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="instagram_client_secret"
                                               value="<?= isset($instagram_client_secret) ? $instagram_client_secret : '' ?>"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Authorized redirect URIs</label>
                                    <div class="col-sm-9">
                                        <div class="input-group" style="margin-top:10px">
                                            https://www.domain.com/customer/instagram_login
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-7">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status Order Default<br><span class="help">Default status order ketika baru dibuat</span></label>
                            <div class="col-sm-3">
                                <select name="order_status_id" class="form-control">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $order_status_id) { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"
                                                    selected="selected"><?= $order_status['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status Order Batal<br><span class="help">Status order jika dibatalkan</span></label>
                            <div class="col-sm-3">
                                <select name="order_cancel_status_id" class="form-control">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $order_cancel_status_id) { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"
                                                    selected="selected"><?= $order_status['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status Order Lunas<br><span class="help">Status order jika pembayaran lunas</span></label>
                            <div class="col-sm-3">
                                <select name="order_paid_status_id" class="form-control">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $order_paid_status_id) { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"
                                                    selected="selected"><?= $order_status['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status Order Proses<br><span class="help">Default status order sedang diproses</span></label>
                            <div class="col-sm-3">
                                <select name="order_process_status_id" class="form-control">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $order_process_status_id) { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"
                                                    selected="selected"><?= $order_status['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status Order Kirim<br><span class="help">Status order jika barang sudah dikirim</span></label>
                            <div class="col-sm-3">
                                <select name="order_delivered_status_id" class="form-control">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $order_delivered_status_id) { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"
                                                    selected="selected"><?= $order_status['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status Order Selesai<br><span class="help">Status order jika sudah dinyatakan selesai dan barang sudah diterima oleh pembeli</span></label>
                            <div class="col-sm-3">
                                <select name="order_complete_status_id" class="form-control">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $order_complete_status_id) { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"
                                                    selected="selected"><?= $order_status['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-8">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Slideshow</label>
                            <div class="col-sm-9">
                                <select name="slideshow_banner_id" class="form-control">
                                    <?php foreach ($banners as $banner) { ?>
                                        <?php if ($banner['banner_id'] == $slideshow_banner_id) { ?>
                                            <option value="<?= $banner['banner_id'] ?>"
                                                    selected="selected"><?= $banner['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $banner['banner_id'] ?>"><?= $banner['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Top Banner</label>
                            <div class="col-sm-9">
                                <select name="top_banner_id" class="form-control">
                                    <?php foreach ($banners as $banner) { ?>
                                        <?php if ($banner['banner_id'] == $top_banner_id) { ?>
                                            <option value="<?= $banner['banner_id'] ?>"
                                                    selected="selected"><?= $banner['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $banner['banner_id'] ?>"><?= $banner['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Middle Banner</label>
                            <div class="col-sm-9">
                                <select name="middle_banner_id" class="form-control">
                                    <?php foreach ($banners as $banner) { ?>
                                        <?php if ($banner['banner_id'] == $middle_banner_id) { ?>
                                            <option value="<?= $banner['banner_id'] ?>"
                                                    selected="selected"><?= $banner['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $banner['banner_id'] ?>"><?= $banner['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Footer Banner</label>
                            <div class="col-sm-9">
                                <select name="footer_banner_id" class="form-control">
                                    <?php foreach ($banners as $banner) { ?>
                                        <?php if ($banner['banner_id'] == $footer_banner_id) { ?>
                                            <option value="<?= $banner['banner_id'] ?>"
                                                    selected="selected"><?= $banner['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $banner['banner_id'] ?>"><?= $banner['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-9">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Server Url<br><span class="help">alamat untuk web server</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="web_server_url"
                                       value="<?= isset($web_server_url) ? $web_server_url : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">API SERVER<br><span class="help">API KEY untuk web server</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="api_key_server"
                                       value="<?= isset($api_key_server) ? $api_key_server : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Product Element Code<br><span class="help">Nomor produk website, silahkan hubungi administrator untuk lebih lanjut</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="web_product_code"
                                       value="<?= isset($web_product_code) ? $web_product_code : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                    </div>


                    <div class="tab-pane" id="tab-10">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Delivery Cost</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" style="text-align:left"></label>
                                    <label class="control-label col-sm-4" style="text-align:left">< 10 Kg</label>
                                    <label class="control-label col-sm-4" style="text-align:left">> 10 Kg</label>
                                </div>
                                <span id="delivery_cost_list"></span>
                            </div>
                        </div>
                    </div>

                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/js/ajaxupload.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    function delivery(val) {
        if ($('#delivery_courier').is(':checked')) {
            $('#delivery_tab').show();
        } else {
            $('#delivery_tab').hide();
        }
    }

    new AjaxUpload('#upload-logo', {
        action: "<?=admin_url('system/setting/upload');?>",
        name: 'userfile',
        autoSubmit: false,
        responseType: 'json',
        onChange: function (file, extension) {
            this.submit();
        },
        onSubmit: function (file, extension) {
            $('#upload-logo').append('<span class="wait"><i class="fa fa-refresh"></i></span>');
        },
        onComplete: function (file, json) {
            if (json.success) {
                $('.wait').remove();
                $('#image-logo').attr('src', json.thumb);
                $('#input-logo').val(json.image);
            }
            if (json.error) {
                alert(json.error);
            }

            $('.loading').remove();
        }
    });

    new AjaxUpload('#upload-favicon', {
        action: "<?=admin_url('system/setting/upload');?>",
        name: 'userfile',
        autoSubmit: false,
        responseType: 'json',
        onChange: function (file, extension) {
            this.submit();
        },
        onSubmit: function (file, extension) {
            $('#upload-favicon').append('<span class="wait"><i class="fa fa-refresh"></i></span>');
        },
        onComplete: function (file, json) {
            if (json.success) {
                $('.wait').remove();
                $('#image-favicon').attr('src', json.thumb);
                $('#input-favicon').val(json.image);
            }
            if (json.error) {
                alert(json.error);
            }

            $('.loading').remove();
        }
    });

    //new AjaxUpload('#upload-google-json', {
    //	action: "<?//=admin_url('system/setting/upload_google_json');?>//",
    //	name: 'userfile',
    //	autoSubmit: false,
    //	responseType: 'json',
    //	onChange: function(file, extension) {
    //		this.submit();
    //	},
    //	onSubmit: function(file, extension) {
    //		$('#upload-google-json').append('<span class="wait"><i class="fa fa-refresh"></i></span>');
    //	},
    //	onComplete: function(file, json) {
    //		if (json.success) {
    //			$('.wait').remove();
    //			$('#input-google-json').val(json.file);
    //		}
    //		if (json.error) {
    //			alert(json.error);
    //		}
    //
    //		$('.loading').remove();
    //	}
    //});

    $('#submit').bind('click', function () {
        $this = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                $($this).attr('disabled', true);
                $($this).append('<span class="wait"> <i class="fa fa-refresh fa-spin"></i></span>');
            },
            complete: function () {
                $($this).attr('disabled', false);
                $('.wait').remove();
            },
            success: function (json) {
                $('.error').remove();
                $('.form-group').removeClass('has-error');
                if (json['success']) {
                    $('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json.success + '</div>');
                    $('html, body').animate({scrollTop: 0}, 'slow');
                } else if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible error"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json.error + '</div>');
                    $('html, body').animate({scrollTop: 0}, 'slow');
                } else if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').after('<span class="help-block error">' + json['errors'][i] + '</span>');
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                    }
                }
            }
        });
    });

    $('select[name=\'province_id\']').on('change', function () {
        $.ajax({
            url: "<?=site_url('location/province')?>",
            data: 'province_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'province_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                html = '<option value="">-- Pilih --</option>';

                if (json['cities'] && json['cities'] != '') {
                    for (i = 0; i < json['cities'].length; i++) {
                        html += '<option value="' + json['cities'][i]['city_id'] + '"';

                        if (json['cities'][i]['city_id'] == '<?=$city_id?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['cities'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected">-- None --</option>';
                }

                $('select[name=\'city_id\']').html(html);
                $('select[name=\'city_id\']').trigger('change');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'city_id\']').on('change', function () {
        $.ajax({
            url: "<?=site_url('location/city')?>",
            data: 'city_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'city_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                html = '<option value="">-- Pilih --</option>';
                htmlcost = '';

                if (json['subdistricts'] && json['subdistricts'] != '') {
                    for (i = 0; i < json['subdistricts'].length; i++) {
                        html += '<option value="' + json['subdistricts'][i]['subdistrict_id'] + '"';

                        if (json['subdistricts'][i]['subdistrict_id'] == '<?=$subdistrict_id?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['subdistricts'][i]['name'] + '</option>';

                        if (json['subdistricts'][i]['delivery_cost'] == 0) {
                            deliverycost = '0&0';
                        } else {
                            deliverycost = json['subdistricts'][i]['delivery_cost'];
                        }

                        deliverycostsplit = deliverycost.split("&");

                        htmlcost += '<div class="form-group"><label class="control-label col-sm-3">' + json['subdistricts'][i]['name'] + '</label><div class="col-sm-4"><input name="delivery_cost_before[' + json['subdistricts'][i]['subdistrict_id'] + ']" class="form-control" value="' + deliverycostsplit[0] + '" style="width:50%; text-align: right" /></div><div class="col-sm-4"><input name="delivery_cost_after[' + json['subdistricts'][i]['subdistrict_id'] + ']" class="form-control" value="' + deliverycostsplit[1] + '" style="width:50%; text-align: right" /></div></div>';
                    }
                } else {
                    html += '<option value="0" selected="selected">-- None --</option>';
                }

                $('select[name=\'subdistrict_id\']').html(html);
                $('#delivery_cost_list').html(htmlcost);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'province_id\']').trigger('change');
</script>