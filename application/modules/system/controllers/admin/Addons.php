<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addons extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('datatables');
        $this->load->library('unzip');
    }

    /**
     * Index addon modules
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->load->model('addon_model');

        $nr = $this->db->get('addon')->num_rows();

        if ($nr > 0) {
            $this->addon_model->matches();
        }

        if ($this->input->post(null, true)) {
            $this->datatables
                ->select('code, name, description, version, active')
                ->from('addon')
                ->add_column('status', '$1', 'check_status(code)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->flashdata('success');
            $data['error'] = $this->session->flashdata('error');

            $this->load->view('admin/addon', $data);
        }
    }

    /**
     * Install addon module
     *
     * @access public
     * @return json
     */
    public function install()
    {
        check_ajax();

        $json = array();

        $code = $this->input->post('code');

        if (!$this->admin_auth->has_permission('create')) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('addon_model');

            $addon = $this->addon_model->get($code);

            if ($addon) {
                $model = $code . '_model';

                $this->load->model($code . '/' . $model);
                $this->{$model}->install();

                $this->addon_model->update($code, array('active' => 1));

                $json['success'] = 'Berhasil menginstall modul ' . $addon['name'];
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Uninstall addon module
     *
     * @access public
     * @return json
     */
    public function uninstall()
    {
        check_ajax();

        $json = array();

        $code = $this->input->post('code');

        if (!$this->admin_auth->has_permission('delete')) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('addon_model');
            $this->load->model('setting_model');

            $addon = $this->addon_model->get($code);

            if ($addon) {
                $model = $code . '_model';

                $this->load->model($code . '/' . $model);
                $this->{$model}->uninstall();

                $this->addon_model->update($code, array('active' => 0));
                $this->setting_model->delete_setting('addon_' . $code);

                $json['success'] = 'Berhasil menghapus modul ' . $addon['name'];
            }
        }

        $this->output->set_output(json_encode($json));
    }


    public function delete()
    {
        check_ajax();

        $json = array();

        $code = $this->input->post('code');

        if (!$this->admin_auth->has_permission('delete')) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('addon_model');
            $this->load->model('setting_model');

            $addon = $this->addon_model->get($code);

            $ins = 'addons/' . $code . '/' . $code . '.txt';
            $str = file_get_contents($ins);
            $string_awesome = eval($str);

            $this->db->where('code', $code)->delete('addon');

            $json['success'] = 'Addons ' . $addon['name'] . ' berhasil di hapus dari daftar';
        }

        $this->output->set_output(json_encode($json));
    }


    public function add()
    {

        $type = $_FILES['addons']['type'];
        $tmp = $_FILES['addons']['tmp_name'];
        $file = $_FILES['addons']['name'];
        $size = $_FILES['addons']['size'];

        if ($type <> "application/zip") {
            $this->session->set_flashdata('error', 'Harap masukan file zip');
            redirect('office/system/addons', 'refresh');
        }

        if ($size > 1000000) {
            $this->session->set_flashdata('error', 'File terlalu besar');
            redirect('office/system/addons', 'refresh');
        }

        move_uploaded_file($tmp, DIR_FILE . 'addons/' . $file);
        $this->unzip->extract(DIR_FILE . 'addons/' . $file, DIR_FILE . 'addons/');


        if (!file_exists(DIR_FILE . 'addons/v')) {
            $this->session->set_flashdata('error', 'File addons tidak di dukung, silahkan cek kembali zip file anda');
            redirect('office/system/addons', 'refresh');
        }


        include DIR_FILE . 'addons/v';
        $this->load->model('addon_model');
        $exist = $this->addon_model->is_added($detailaddons['code']);
        if ($exist == 1) {
            $this->session->set_flashdata('error', 'Addons sudah terinstall');
            redirect('office/system/addons', 'refresh');
        } else {
            //add to table addons list
            $ins = array(
                'code' => $detailaddons['code'],
                'name' => $detailaddons['name'],
                'description' => $detailaddons['description'],
                'version' => $detailaddons['version'],
                'active' => 0,
            );

            $code = $this->addon_model->insert($ins);
        }

        //move all files
        $ins = DIR_FILE . 'addons/install.txt';
        $str = file_get_contents($ins);
        $string_awesome = eval($str);

        $this->session->set_flashdata('success', 'Addons berhasil di tambahkan ke daftar');
        redirect('office/system/addons', 'refresh');


    }
}

function check_status($code)
{
    $ci =& get_instance();

    $addon_setting = $ci->config->item('addon_' . $code);
    return (!empty($addon_setting['active'])) ? 1 : 0;
}