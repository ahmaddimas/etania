<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weight_class extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');

        $this->form_validation->CI =& $this;
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('weight_class_id, title, unit, value')
                ->from('weight_class');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load
                ->title('Satuan Berat')
                ->view('admin/weight_class');
        }
    }

    /**
     * Create new weight class
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Satuan Berat');

        $this->form();
    }

    /**
     * Edit existing weight class
     *
     * @access public
     * @param int $weight_class_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Satuan Berat');

        $this->form($this->input->get('weight_class_id'));
    }

    /**
     * Load weight class form
     *
     * @access private
     * @param int $weight_class_id
     * @return void
     */
    private function form($weight_class_id = null)
    {
        $this->load->model('weight_class_model');

        $data['action'] = admin_url('system/weight_class/validate');
        $data['weight_class_id'] = null;
        $data['title'] = '';
        $data['unit'] = '';
        $data['value'] = 1;

        if ($weight_class = $this->weight_class_model->get($weight_class_id)) {
            $data['weight_class_id'] = (int)$weight_class['weight_class_id'];
            $data['title'] = $weight_class['title'];
            $data['unit'] = $weight_class['unit'];
            $data['value'] = (float)$weight_class['value'];
        }

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/weight_class_form', $data, true, true)
        )));
    }

    /**
     * Validate weight class form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $weight_class_id = $this->input->post('weight_class_id');

        $this->form_validation
            ->set_rules('title', 'Nama', 'trim|required|min_length[3]|min_length[32]')
            ->set_rules('unit', 'Satuan', 'trim|required|min_length[2]|min_length[4]')
            ->set_rules('value', 'Nilai', 'trim|required')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $data['title'] = $this->input->post('title');
            $data['unit'] = $this->input->post('unit');
            $data['value'] = $this->input->post('value');

            $this->load->model('weight_class_model');

            if ($weight_class_id) {
                $this->weight_class_model->update($weight_class_id, $data);

                $json['success'] = lang('success_update');
            } else {
                if ($this->weight_class_model->insert($data)) {
                    $json['success'] = lang('success_create');
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $weight_class_ids = $this->input->post('weight_class_id');

        if (!$weight_class_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('weight_class_model');
            $this->weight_class_model->delete($weight_class_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
} 