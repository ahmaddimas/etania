<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_status extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');

        $this->form_validation->CI =& $this;
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('stock_status_id, name')
                ->from('stock_status');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load
                ->title('Status Stok')
                ->view('admin/stock_status');
        }
    }

    /**
     * Create new stock status
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Status Stok');

        $this->form();
    }

    /**
     * Edit existing stock status
     *
     * @access public
     * @param int $stock_status_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Status Stok');

        $this->form($this->input->get('stock_status_id'));
    }

    /**
     * Load stock status form
     *
     * @access private
     * @param int $stock_status_id
     * @return void
     */
    private function form($stock_status_id = null)
    {
        $this->load->model('stock_status_model');

        $data['action'] = admin_url('system/stock_status/validate');
        $data['stock_status_id'] = null;
        $data['name'] = '';

        if ($stock_status = $this->stock_status_model->get($stock_status_id)) {
            $data['stock_status_id'] = (int)$stock_status['stock_status_id'];
            $data['name'] = $stock_status['name'];
        }

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/stock_status_form', $data, true, true)
        )));
    }

    /**
     * Validate stock status form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $stock_status_id = $this->input->post('stock_status_id');

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required|min_length[3]|max_length[32]')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $data['name'] = $this->input->post('name');

            $this->load->model('stock_status_model');

            if ($stock_status_id) {
                $this->stock_status_model->update($stock_status_id, $data);

                $json['success'] = lang('success_update');
            } else {
                if ($this->stock_status_model->insert($data)) {
                    $json['success'] = lang('success_create');
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $stock_status_ids = $this->input->post('stock_status_id');

        if (!$stock_status_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('stock_status_model');
            $this->stock_status_model->delete($stock_status_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
} 