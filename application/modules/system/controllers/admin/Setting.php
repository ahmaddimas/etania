<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->load->model('setting_model');
        $this->load->model('weight_class_model');
        $this->load->model('length_class_model');
        $this->load->model('order_status_model');
        $this->load->model('location/location_model');

        $data['action'] = admin_url('system/setting');
        $data['city_id'] = null;
        $data['province_id'] = null;
        $data['subdistrict_id'] = null;

        $settings = $this->setting_model->get_settings();

        if (isset($settings['config'])) {
            foreach ($settings['config'] as $key => $value) {
                $data[$key] = $value;
            }
        }

        $data['logo'] = isset($data['logo']) ? $data['logo'] : '';
        $data['favicon'] = isset($data['favicon']) ? $data['favicon'] : '';

        $this->load->library('image');

        $data['thumb_logo'] = $this->image->resize($data['logo'] !== '' ? $data['logo'] : 'no_image.jpg', 150, 150);
        $data['no_image'] = $this->image->resize('no_image.jpg', 150, 150);

        $data['thumb_favicon'] = $this->image->resize($data['favicon'] !== '' ? $data['favicon'] : 'no_image.jpg', 150, 150);
        $data['no_image'] = $this->image->resize('no_image.jpg', 150, 150);

        $this->load->library('rajaongkir');

        $data['rajaongkir_courier'] = $this->config->item('rajaongkir_courier') ? $this->config->item('rajaongkir_courier') : array();
        $data['couriers'] = $this->rajaongkir->get_couriers('domestic');
        $data['weight_classes'] = $this->weight_class_model->get_all();
        $data['length_classes'] = $this->length_class_model->get_all();
        $data['order_statuses'] = $this->order_status_model->get_all();

        $this->load->model('customer/customer_group_model');
        $data['customer_groups'] = $this->customer_group_model->get_all();

        $this->form_validation
            ->set_rules('company', 'Company Name', 'trim|required')
            ->set_rules('email', 'Email', 'trim|required|valid_email')
            ->set_error_delimiters('', '');

        if ($this->input->post(null, true)) {
            $json = array();

            if (!$this->admin_auth->has_permission('edit')) {
                $json['error'] = lang('admin_error_edit');
            }

            if ($this->form_validation->run() == false) {
                foreach (array('company', 'email') as $field) {
                    if (form_error($field) !== '') {
                        $json['errors'][$field] = form_error($field);
                    }
                }
            } else {
                if (!$json) {
                    $post = $this->input->post(null, false);

                    if ($this->input->post('oauth_facebook_status') == null) {
                        $post['oauth_facebook_status'] = 0;
                    }

                    if ($this->input->post('oauth_google_status') == null) {
                        $post['oauth_google_status'] = 0;
                    }

                    if ($this->input->post('oauth_twitter_status') == null) {
                        $post['oauth_twitter_status'] = 0;
                    }

                    if ($this->input->post('oauth_instagram_status') == null) {
                        $post['oauth_instagram_status'] = 0;
                    }

                    if ($this->input->post('delivery_courier') == null) {
                        $post['delivery_courier'] = 0;
                    }


                    if ($this->input->post('delivery_cost_before')) {
                        foreach ($this->input->post('delivery_cost_before') as $key => $val) {
                            $cost = $val . '&' . $this->input->post('delivery_cost_after')[$key];
                            $this->location_model->update_delivery_cost($key, $cost);
                        }
                    }


                    $this->setting_model->edit_setting('config', $post);
                    $json['success'] = 'Pengaturan berhasil diperbarui!';
                }
            }

            $this->output->set_output(json_encode($json));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401();
            }


            $data['provinces'] = $this->location_model->get_provinces();

            $this->load->model('banner/banner_model');

            $data['banners'] = $this->banner_model->get_all();

            $data['slideshow_banner_id'] = (int)$this->config->item('slideshow_banner_id');
            $carousel_categories = $this->config->item('carousel_categories');

            if (!is_array($carousel_categories)) {
                $carousel_categories = array(0);
            }

            $data['categories'] = $this->db->where_in('category_id', $carousel_categories)->get('category')->result_array();

            $this->load
                ->title('Pengaturan')
                ->view('admin/setting', $data);
        }
    }

    public function upload()
    {
        $json = array();

        $upload_path = DIR_IMAGE . 'data/logo';

        if (!file_exists($upload_path)) {
            @mkdir($upload_path, 0777);
        }

        $this->load->library('upload', array(
            'upload_path' => $upload_path,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size' => '3000',
            'overwrite' => true,
        ));

        if (!$this->upload->do_upload()) {
            $json['error'] = $this->upload->display_errors('', '');
        } else {
            $this->load->library('image');

            $upload_data = $this->upload->data();
            $image = substr($upload_data['full_path'], strlen(FCPATH . DIR_IMAGE));
            $thumb = $this->image->resize($image, 150, 150, true);

            $json['thumb'] = $thumb;
            $json['image'] = $image;
            $json['success'] = 'File berhasil diupload!';
        }

        $this->output->set_output(json_encode($json));
    }

    public function upload_google_json()
    {
        $json = array();

        $upload_path = FCPATH . 'application/config/json/';

        if (!file_exists($upload_path)) {
            @mkdir($upload_path, 0777);
        }

        $this->load->library('upload', array(
            'upload_path' => $upload_path,
            // belum fixed, kenapa json masih ditolak?
            'allowed_types' => 'json',
            'max_size' => '1000',
            'overwrite' => true,
        ));

        if (!$this->upload->do_upload()) {
            $json['error'] = $this->upload->display_errors('', '');
        } else {
            $upload_data = $this->upload->data();

            $json['file'] = substr($upload_data['full_path'], strlen($upload_path));
            $json['success'] = 'File berhasil diupload!';
        }

        $this->output->set_output(json_encode($json));
    }
} 