<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
        $this->load->library('unzip');
        $this->load->model('template_model');
        $this->load->model('templateconfig_model');
    }

    /**
     * Index addon modules
     *
     * @access public
     * @return void
     */
    public function index()
    {

        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $data['success'] = $this->session->flashdata('success');
        $data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/template', $data);

    }


    public function desktop()
    {
        $this->load->model('template_model');

        $this->datatables
            ->select('template_id, name, image, status, note')
            ->from('template');

        $this->output->set_output($this->datatables->generate('json'));

    }


    public function setactive($id)
    {

        $this->load->model('template_model');
        $this->load->model('templateconfig_model');
        $this->load->library('zip');


        $d = $this->template_model
            ->get($id);


        $this->deldir('themes/desktop');
        $zip = new ZipArchive;
        $zip->open($d['location_desktop']);
        $zip->extractTo('themes/desktop');
        $zip->close();

        $this->deldir('themes/mobile');
        $zip = new ZipArchive;
        $zip->open($d['location_mobile']);
        $zip->extractTo('themes/mobile');
        $zip->close();


        $updatedata = array(
            'status' => 0
        );

        $na = $this->template_model
            ->get_all();

        foreach ($na as $nna => $nn) {
            $update = $this->template_model
                ->update($nn['template_id'], $updatedata);
        }


        $updatedata2 = array(
            'status' => 1
        );
        $update2 = $this->template_model
            ->update($id, $updatedata2);

        $getbyid = array(
            'template_id' => $id
        );
        $na = $this->templateconfig_model
            ->get_by($getbyid);

        $this->setting_model->edit_setting_value('config', 'image_product_width', $na['pwd']);
        $this->setting_model->edit_setting_value('config', 'image_product_height', $na['phg']);
        $this->setting_model->edit_setting_value('config', 'image_detail_product_width', $na['pdwd']);
        $this->setting_model->edit_setting_value('config', 'image_detail_product_height', $na['pdhg']);
        $this->setting_model->edit_setting_value('config', 'image_compare_product_width', $na['pcwd']);
        $this->setting_model->edit_setting_value('config', 'image_compare_product_height', $na['pchg']);
        $this->setting_model->edit_setting_value('config', 'image_cart_product_width', $na['pcrwd']);
        $this->setting_model->edit_setting_value('config', 'image_cart_product_height', $na['pcrhg']);
        $this->setting_model->edit_setting_value('config', 'image_product_dod_width', $na['powd']);
        $this->setting_model->edit_setting_value('config', 'image_product_dod_height', $na['pohg']);


        $this->session->set_flashdata('success', 'Template telah di set');
        $this->index();
    }


    public function deldir($dirPath)
    {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);

        // print_r($files);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deldir($file);
            } else {
                unlink($file);
            }
        }
        // rmdir($dirPath);
    }


    public function add()
    {

        $type = $_FILES['template']['type'];
        $tmp = $_FILES['template']['tmp_name'];
        $file = $_FILES['template']['name'];
        $size = $_FILES['template']['size'];

//		print_r($_FILES);
//		exit;
        if ($type <> "application/zip") {
            $this->session->set_flashdata('error', 'Harap masukan file zip');
            redirect('office/system/template', 'refresh');
        }

        if ($size > 10000000) {
            $this->session->set_flashdata('error', 'File terlalu besar');
            redirect('office/system/template', 'refresh');
        }

        move_uploaded_file($tmp, DIR_FILE . 'template/' . $file);
        $this->unzip->extract(DIR_FILE . 'template/' . $file, DIR_FILE . 'template/');


        if (!file_exists(DIR_FILE . 'template/v')) {
            $this->session->set_flashdata('error', 'File template tidak di dukung, silahkan cek kembali zip file anda');
            $this->index();
        }


        include DIR_FILE . 'template/v';
        $this->load->model('template_model');
        $exist = $this->template_model->is_added($detailtemplate['code']);
        if ($exist == 1) {
            $this->session->set_flashdata('error', 'Template sudah terinstall');
            $this->index();
        } else {
            //add to table template list
            $ins = array(
                'code' => $detailtemplate['code'],
                'name' => $detailtemplate['name'],
                'location_desktop' => $detailtemplate['location_desktop'],
                'location_mobile' => $detailtemplate['location_mobile'],
                'image' => $detailtemplate['image'],
                'note' => $detailtemplate['note'],
                'status' => 0,
            );

            $code = $this->template_model->insert($ins);
        }

        include DIR_FILE . 'template/config';
        $getbyid = array(
            'code' => $detailtemplate['code']
        );
        $na = $this->template_model
            ->get_by($getbyid);

        $ins = array(
            'template_id' => $na['template_id'],
            'pwd' => $config['pwd'],
            'phg' => $config['phg'],
            'pdwd' => $config['pdwd'],
            'pdhg' => $config['pdhg'],
            'pcwd' => $config['pcwd'],
            'pchg' => $config['pchg'],
            'pcrwd' => $config['pcrwd'],
            'pcrhg' => $config['pcrhg'],
            'powd' => $config['powd'],
            'pohg' => $config['pohg'],
        );

        $ins = $this->templateconfig_model->insert($ins);


        //move all files
        $ins = DIR_FILE . 'template/install.txt';
        $str = file_get_contents($ins);
        $string_awesome = eval($str);

        $this->session->set_flashdata('success', 'Template berhasil di tambahkan ke daftar');
        $this->index();
    }


    public function delete()
    {
        check_ajax();

        $json = array();

        $template_id = $this->input->post('template_id');

        if (!$this->admin_auth->has_permission('delete')) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {

            $getbyid = array(
                'template_id' => $template_id
            );
            $na = $this->template_model
                ->get_by($getbyid);

            $ins = 'template/' . $na['code'] . '/' . $na['code'] . '.txt';
            $str = file_get_contents($ins);
            $string_awesome = eval($str);

            $this->db->where('template_id', $template_id)->delete('templateconfig');
            $this->db->where('template_id', $template_id)->delete('template');

            $json['success'] = 'Template ' . $na['name'] . ' berhasil di hapus dari daftar';
        }

        $this->output->set_output(json_encode($json));
    }


}