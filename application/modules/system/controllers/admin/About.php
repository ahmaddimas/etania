<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('datatables');
        $this->load->library('unzip');
        $this->load->model('version_model');
    }

    /**
     * Index addon modules
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $data['success'] = $this->session->flashdata('success');
        $data['error'] = $this->session->flashdata('error');
        $data['server_url'] = $this->config->item('web_server_url');
        $data['product_code'] = $this->config->item('web_product_code');

        $this->load->model('version_model');
        $version = $this->version_model->get(1);


        $data['version_no'] = $version['version_no'];
        $data['version_manual_book'] = $version['version_manual_book'];
        $data['version_date'] = $version['version_date'];


        $this->load->view('admin/about', $data);
    }

    public function checkUpdate()
    {
//        if (!$this->input->is_ajax_request()) {
//            show_404();
//        }

        $this->load->model('addon_model');
        $this->load->model('version_model');

        $version = $this->version_model->get(1);
        $url = $this->config->item('web_server_url') . '/api/v1/versions/getVersionList';
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'access_key=' . $this->config->item('api_key_server'));

        $response = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($response, TRUE);
        if (!$response['status'])
            return false;

        $addons = $this->addon_model->get_addons();
        $update = array(
            'status' => 0,
            'data' => array()
        );
        $temp = array();
        foreach ($response['data'] as $item) {
            if ($item['type'] == 'addons') {
                foreach ($addons as $addon) {
                    if (in_array($item['code'], $addon)) {
                        if ((double)$item['version_name'] > (double)$addon['version']) {
                            $update['data'][] = array(
                                'code' => $item['code'],
                                'name' => $addon['name'],
                                'desc' => $addon['description'],
                                'type' => $item['type'],
                                'cur_version' => $addon['version'],
                                'up_version' => $item['version_name']
                            );
//                            $this->updateAddon($item['code']);
                        }
                    }
                }
            } elseif ($item['type'] == 'installer') {
                if (!in_array($item['code'], $temp)) {
                    if ((double)$item['version_name'] > (double)$version['version_no']) {
                        $temp = array(
                            'code' => $item['code'],
                            'name' => $item['name'],
                            'desc' => $item['description'],
                            'type' => $item['type'],
                            'cur_version' => $version['version_no'],
                            'up_version' => $item['version_name']
                        );
                        $update['data'][] = $temp;
//                        $this->updateWebsite();
                    }
                }
            }
        }
        $update['status'] = count($update['data']) > 0 ? 1 : 0;
//        echo json_encode(array('status' => 1));
        echo json_encode($update);
    }

    public function update()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        $req_type = $this->input->post('type');
        if ($req_type == 'addons') {
            $this->updateAddon($this->input->post('code'));
        } else {
            $this->updateWebsite();
        }
        $status = empty($this->session->flashdata('success')) ? 'error' : 'success';
        $msg = $this->session->flashdata($status);
        echo json_encode(array('status' => $status === 'success' ? 1 : 0, 'message' => $msg));
    }

    private function updateAddon($code)
    {
        $file_url = $this->config->item('web_server_url') . '/api/v1/versions/downloadWithApiKey?access_key=' . $this->config->item('api_key_server') . '&code=' . $code;
        $filepath = DIR_FILE . 'addons/' . $code . '.zip';

        copy($file_url, $filepath);
        $type = mime_content_type($filepath);

        if ($type <> "application/zip") {
            $this->session->set_flashdata('error', 'Harap masukan file zip');
            echo json_encode(array('status' => 1));
            return;
        }

        $this->unzip->extract($filepath, DIR_FILE . 'addons/');


        if (!file_exists(DIR_FILE . 'addons/v')) {
            $this->session->set_flashdata('error', 'File addons tidak di dukung, silahkan cek kembali zip file anda');
            echo json_encode(array('status' => 1));
            return;
        }


        include DIR_FILE . 'addons/v';
        $this->load->model('addon_model');
        //add to table addons list
        $ins = array(
            'version' => $detailaddons['version']
        );

        $code = $this->addon_model->update($detailaddons['code'], $ins);

        //move all files
        $ins = DIR_FILE . 'addons/install.txt';
        $str = file_get_contents($ins);
        $string_awesome = eval($str);

        $this->session->set_flashdata('success', 'Addons berhasil di tambahkan ke daftar');
//        redirect('office/system/addons', 'refresh');


    }

    private function updateWebsite()
    {
        $file_url = $this->config->item('web_server_url') . '/api/v1/versions/downloadWithApiKey?access_key=' . $this->config->item('api_key_server') . '&code=' . $this->config->item('web_product_code');
        $filepath = DIR_FILE . 'update/update build ' . date('Ymd') . '.zip';

        copy($file_url, $filepath);
        $type = mime_content_type($filepath);

        if ($type <> "application/zip") {
            $this->session->set_flashdata('error', 'File tidak didukung, silahkan hubungi administrator');
            echo json_encode(array('status' => 1));
            return;
        }

        $this->unzip->extract($filepath, DIR_FILE . 'update/');


        if (!file_exists(DIR_FILE . 'update/v')) {
            $this->session->set_flashdata('error', 'File addons tidak di dukung, silahkan cek kembali zip file anda');
            echo json_encode(array('status' => 1));
            return;
        }


        $ver = DIR_FILE . 'update/version';
        $verhs = file_get_contents($ver);
        $this->load->model('version_model');
        $version = $this->version_model->get(1);


        if (password_verify($verhs, $version['version_code_hash']) == "") {
            $this->session->set_flashdata('error', 'Update tidak dapat di proses, silahkan cek kembali urutan update anda');
            return;
        }


        ini_set('max_execution_time', 0);
        $servername = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;

        $filename = DIR_FILE . 'update/update.sql';
        $templine = '';
        $lines = file($filename);

        error_reporting(0);
        $mysqli = new mysqli($servername, $username, $password, $dbname);
        if ($mysqli->connect_errno) {
            printf("<center><h3 style=margin-top:100px>Koneksi database gagal, silahkan cek detail setting database anda : %s\n", $mysqli->connect_error . '</h3>
				    	<br>
				    	<a href=' . admin_url('system/about') . '>Kembali</a></center>
				    	');
            exit();
        }
        foreach ($lines as $line) {
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;

            $templine .= $line;
            if (substr(trim($line), -1, 1) == ';') {
                $show = $mysqli->query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . $mysqli->error . '<br /><br />');
                $templine = '';
            }
        }
        $mysqli->close();

        $ins = DIR_FILE . 'update/update.txt';
        $str = file_get_contents($ins);
        $string_awesome = eval($str);


        $build = DIR_FILE . 'update/build';
        $buildr = file_get_contents($build);

        $v = DIR_FILE . 'update/v';
        $vr = file_get_contents($v);


        $verup = array(
            'version_no' => $vr,
            'version_code_hash' => password_hash('Version ' . $vr, PASSWORD_DEFAULT),
            'version_date' => $buildr,
        );

        $update2 = $this->version_model
            ->update(1, $verup);


        $this->session->set_flashdata('success', 'Update terinstall');
        // redirect('office/system/about', 'refresh');

    }


}
	
