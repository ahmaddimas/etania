<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_status extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');

        $this->form_validation->CI =& $this;
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('order_status_id, name')
                ->from('order_status');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load
                ->title('Status Order')
                ->view('admin/order_status');
        }
    }

    /**
     * Create new order status
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Status Order');

        $this->form();
    }

    /**
     * Edit existing order status
     *
     * @access public
     * @param int $order_status_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Status Order');

        $this->form($this->input->get('order_status_id'));
    }

    /**
     * Load order status form
     *
     * @access private
     * @param int $order_status_id
     * @return void
     */
    private function form($order_status_id = null)
    {
        $this->load->model('order_status_model');

        $data['action'] = admin_url('system/order_status/validate');
        $data['order_status_id'] = null;
        $data['name'] = '';

        if ($order_status = $this->order_status_model->get($order_status_id)) {
            $data['order_status_id'] = (int)$order_status['order_status_id'];
            $data['name'] = $order_status['name'];
        }

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/order_status_form', $data, true, true)
        )));
    }

    /**
     * Validate order status form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $order_status_id = $this->input->post('order_status_id');

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required|min_length[3]|max_length[32]')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $data['name'] = $this->input->post('name');

            $this->load->model('order_status_model');

            if ($order_status_id) {
                $this->order_status_model->update($order_status_id, $data);

                $json['success'] = lang('success_update');
            } else {
                if ($this->order_status_model->insert($data)) {
                    $json['success'] = lang('success_create');
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $order_status_ids = $this->input->post('order_status_id');

        if (!$order_status_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('order_status_model');
            $this->order_status_model->delete($order_status_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
} 