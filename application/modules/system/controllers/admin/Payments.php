<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('datatables');
        $this->load->library('unzip');
    }

    /**
     * Index payment modules
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->load->model('payment_model');

        $this->payment_model->matches();

        if ($this->input->post(null, true)) {
            $this->datatables
                ->select('code, name, description, version, active')
                ->from('payment');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->flashdata('success');
            $data['error'] = $this->session->flashdata('error');

            $this->load->view('admin/payment', $data);
        }
    }

    /**
     * Install payment module
     *
     * @access public
     * @return json
     */
    public function install()
    {
        check_ajax();

        $json = array();

        $code = $this->input->post('code');

        if (!$this->admin_auth->has_permission('create')) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('payment_model');

            $payment = $this->payment_model->get($code);

            if ($payment) {
                $model = $code . '_model';

                $this->load->model('payment/' . $model);
                $this->{$model}->install();

                $this->payment_model->update($code, array('active' => 1));

                $json['success'] = 'Berhasil menginstall sistem pembayaran ' . $payment['name'];
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Uninstall payment module
     *
     * @access public
     * @return json
     */
    public function uninstall()
    {
        check_ajax();

        $json = array();

        $code = $this->input->post('code');

        if (!$this->admin_auth->has_permission('delete')) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('payment_model');
            $this->load->model('Account_model');

            $payment = $this->payment_model->get($code);

            if ($payment) {
                $model = $code . '_model';

                $this->load->model('payment/' . $model);
                $this->{$model}->uninstall();

                $this->payment_model->update($code, array('active' => 0));
                $this->setting_model->delete_setting('payment_' . $code);

                $json['success'] = 'Berhasil menghapus sistem pembayaran ' . $payment['name'];
            }
        }

        $this->output->set_output(json_encode($json));
    }


    public function delete()
    {
        check_ajax();

        $json = array();

        $code = $this->input->post('code');

        if (!$this->admin_auth->has_permission('delete')) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('payment_model');
            $this->load->model('Account_model');

            $payment = $this->payment_model->get($code);

            $ins = APPPATH . 'modules/payment/' . $code . '.txt';
            $str = file_get_contents($ins);
            $string_awesome = eval($str);

            $this->db->where('code', $code)->delete('payment');

            $json['success'] = 'Payment ' . $payment['name'] . ' berhasil di hapus dari daftar';
        }

        $this->output->set_output(json_encode($json));
    }


    public function add()
    {

        $type = $_FILES['payment']['type'];
        $tmp = $_FILES['payment']['tmp_name'];
        $file = $_FILES['payment']['name'];
        $size = $_FILES['payment']['size'];

        if ($type <> "application/zip") {
            $this->session->set_flashdata('error', 'Harap masukan file zip');
            redirect('office/system/payments', 'refresh');
        }

        if ($size > 1000000) {
            $this->session->set_flashdata('error', 'File terlalu besar');
            redirect('office/system/payments', 'refresh');
        }

        move_uploaded_file($tmp, DIR_FILE . 'payment/' . $file);
        $this->unzip->extract(DIR_FILE . 'payment/' . $file, DIR_FILE . 'payment/');


        if (!file_exists(DIR_FILE . 'payment/vp')) {
            $this->session->set_flashdata('error', 'File payment tidak di dukung, silahkan cek kembali zip file anda');
            redirect('office/system/payments', 'refresh');
        }


        include DIR_FILE . 'payment/vp';
        $this->load->model('payment_model');
        $exist = $this->payment_model->is_added($detailpayment['code']);
        if ($exist == 1) {
            $this->session->set_flashdata('error', 'Payment sudah terinstall');
            redirect('office/system/payments', 'refresh');
        } else {
            //add to table payment list
            $ins = array(
                'code' => $detailpayment['code'],
                'name' => $detailpayment['name'],
                'description' => $detailpayment['description'],
                'version' => $detailpayment['version'],
                'active' => 0,
            );

            $code = $this->payment_model->insert($ins);
        }

        //move all files
        $ins = DIR_FILE . 'payment/install.txt';
        $str = file_get_contents($ins);
        $string_awesome = eval($str);

        $this->session->set_flashdata('success', 'Payment berhasil di tambahkan ke daftar');
        redirect('office/system/payments', 'refresh');


    }
}