<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Length_class extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');

        $this->form_validation->CI =& $this;
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('length_class_id, title, unit, value')
                ->from('length_class');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load
                ->title('Satuan Panjang')
                ->view('admin/length_class');
        }
    }

    /**
     * Create new length class
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Satuan Panjang');

        $this->form();
    }

    /**
     * Edit existing length class
     *
     * @access public
     * @param int $length_class_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Satuan Panjang');

        $this->form($this->input->get('length_class_id'));
    }

    /**
     * Load length class form
     *
     * @access private
     * @param int $length_class_id
     * @return void
     */
    private function form($length_class_id = null)
    {
        $this->load->model('length_class_model');

        $data['action'] = admin_url('system/length_class/validate');
        $data['length_class_id'] = null;
        $data['title'] = '';
        $data['unit'] = '';
        $data['value'] = 1;

        if ($length_class = $this->length_class_model->get($length_class_id)) {
            $data['length_class_id'] = (int)$length_class['length_class_id'];
            $data['title'] = $length_class['title'];
            $data['unit'] = $length_class['unit'];
            $data['value'] = (float)$length_class['value'];
        }

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/length_class_form', $data, true, true)
        )));
    }

    /**
     * Validate length class form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $length_class_id = $this->input->post('length_class_id');

        $this->form_validation
            ->set_rules('title', 'Nama', 'trim|required|min_length[3]|max_length[32]')
            ->set_rules('unit', 'Satuan', 'trim|required|min_length[2]|max_length[4]')
            ->set_rules('value', 'Nilai', 'trim|required')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $data['title'] = $this->input->post('title');
            $data['unit'] = $this->input->post('unit');
            $data['value'] = $this->input->post('value');

            $this->load->model('length_class_model');

            if ($length_class_id) {
                $this->length_class_model->update($length_class_id, $data);

                $json['success'] = lang('success_update');
            } else {
                if ($this->length_class_model->insert($data)) {
                    $json['success'] = lang('success_create');
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $length_class_ids = $this->input->post('length_class_id');

        if (!$length_class_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('length_class_model');
            $this->length_class_model->delete($length_class_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
} 