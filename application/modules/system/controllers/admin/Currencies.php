<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Currencies extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');

        $this->form_validation->CI =& $this;
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('*')
                ->from('currency');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->template
                ->title('Satuan Kuantitas')
                ->view('admin/currency');
        }
    }

    /**
     * Create new currency
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->template->title('Tambah Mata Uang');

        $this->form();
    }

    /**
     * Edit existing currency
     *
     * @access public
     * @param int $currency_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->template->title('Edit Mata Uang');

        $this->form($this->input->get('currency_id'));
    }

    /**
     * Load currency form
     *
     * @access private
     * @param int $currency_id
     * @return void
     */
    private function form($currency_id = null)
    {

        $this->load->model('localisation/currency_model');

        $data['action'] = admin_url('localisation/currencies/validate');
        $data['currency_id'] = null;
        $data['title'] = '';
        $data['code'] = '';
        $data['symbol'] = '';
        $data['value'] = 1;
        $data['active'] = null;

        if ($currency = $this->currency_model->get($currency_id)) {
            $data['currency_id'] = (int)$currency['currency_id'];
            $data['title'] = $currency['title'];
            $data['code'] = $currency['code'];
            $data['symbol'] = $currency['symbol'];
            $data['value'] = $currency['value'];
            $data['active'] = (bool)$currency['active'];
        }

        $this->output->set_output(json_encode(array(
            'content' => $this->template->set_layout(null)->view('admin/currency_form', $data, true, true)
        )));
    }

    /**
     * Validate currency form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $currency_id = $this->input->post('currency_id');

        $this->form_validation
            ->set_rules('title', 'Nama', 'trim|required')
            ->set_rules('code', 'Kode', 'trim|required|max_length[3]')
            ->set_rules('symbol', 'Simbol', 'trim|required')
            ->set_rules('value', 'Value', 'trim|required')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach (array('name', 'code', 'symbol', 'value') as $field) {
                if (form_error($field) !== '') {
                    $json['error'][$field] = form_error($field);
                }
            }
        } else {
            $data['currency_id'] = (int)$this->input->post('currency_id');
            $data['title'] = $this->input->post('title');
            $data['code'] = $this->input->post('code');
            $data['symbol'] = $this->input->post('symbol');
            $data['value'] = $this->input->post('value');
            $data['active'] = (bool)$this->input->post('active');

            $this->load->model('localisation/currency_model');

            if ($currency_id) {
                $this->currency_model->update($currency_id, $data);

                $json['success'] = 'Data Mata Uang telah berhasil diperbarui.';
            } else {
                if ($this->currency_model->insert($data)) {
                    $json['success'] = 'Data Mata Uang baru telah ditambahkan.';
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        $currency_id = $this->input->post('currency_id');

        if (!$this->auth->has_permission()) {
            $json['error'] = lang('auth_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('localisation/currency_model');

            $this->currency_model->delete($currency_id);

            $json['success'] = 'Berhasil menghapus Mata Uang';
        }

        $this->output->set_output(json_encode($json));
    }
} 