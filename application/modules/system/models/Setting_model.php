<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get settings
     *
     * @access public
     * @return void
     */
    public function get_settings()
    {
        $settings = $this->cache->get('settings');

        if (!$settings) {
            $settings = array();

            $results = $this->db
                ->get('setting')
                ->result_array();

            foreach ($results as $result) {
                $settings[$result['group']][$result['key']] = (!$result['serialized']) ? $result['value'] : unserialize($result['value']);
            }

            $this->cache->save('settings', $settings);
        }

        return $settings;
    }

    /**
     * Get setting
     *
     * @access public
     * @param string $group
     * @param string $key
     * @return void
     */
    public function get_setting($group = '', $key = '')
    {
        $settings = $this->cache->get('settings');

        if ($settings && isset($settings[$group][$key])) {
            return $settings[$group][$key];
        } else {
            if ($setting = $this->db
                ->select('value, serialized')
                ->where('group', $group)
                ->where('key', $key)
                ->get('setting')
                ->row_array()) {
                return (!$setting['serialized']) ? $setting['value'] : unserialize($setting['value']);
            }
        }

        return false;
    }

    /**
     * Edit setting
     *
     * @access public
     * @param string $group
     * @param array $data
     * @return void
     */
    public function edit_setting($group = '', $data = array())
    {
        $settings = array();

        foreach ($data as $key => $value) {
            $settings[] = array(
                'group' => $group,
                'key' => $key,
                'value' => is_array($value) ? serialize($value) : $value,
                'serialized' => is_array($value) ? 1 : 0
            );

            $this->db
                ->where('group', $group)
                ->where('key', $key)
                ->delete('setting');
        }

        if ($settings) {

            $this->db->insert_batch('setting', $settings);

            $this->cache->delete('settings');
        }
    }

    /**
     * Delete setting
     *
     * @access public
     * @param string $group
     * @return void
     */
    public function delete_setting($group = '')
    {
        $this->db
            ->where('group', $group)
            ->delete('setting');

        if ($this->db->affected_rows()) {
            $this->cache->delete('settings');
        }
    }

    /**
     * Edit setting value
     *
     * @access public
     * @param string $group
     * @param string $key
     * @param string $value
     * @return int | bool
     */
    public function edit_setting_value($group = '', $key = '', $value = '')
    {
        $return = false;

        $setting = $this->db
            ->select('value, serialized')
            ->where('group', $group)
            ->where('key', $key)
            ->get('setting')
            ->row_array();

        if ($setting) {
            $this->db
                ->where('group', $group)
                ->where('key', $key)
                ->set('value', is_array($value) ? serialize($value) : $value)
                ->set('serialized', is_array($value) ? 1 : 0)
                ->update('setting');

            if ($this->db->affected_rows()) {
                $return = true;
            }
        } else {
            $this->db
                ->set(array(
                    'group' => $group,
                    'key' => $key,
                    'value' => is_array($value) ? serialize($value) : $value,
                    'serialized' => is_array($value) ? 1 : 0,
                ))->insert('setting');

            $return = $this->db->insert_id();
        }

        $this->cache->delete('settings');

        return $return;
    }
}