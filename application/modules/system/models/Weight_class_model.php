<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weight_class_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
}