<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends MY_Model
{
    /**
     * Primary key
     *
     * @var string
     * @access protected
     */
    protected $primary_key = 'code';

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Match payment package files with database
     *
     * @access public
     * @return void
     */
    public function matches()
    {
        $payments = array();
        $old = array();
        $new = array();
        $codes = array();
        $files = glob(APPPATH . 'modules/payment/controllers/admin/*');

        foreach ($this->get_all() as $payment) {
            $payments[$payment['code']] = $payment;
        }

        if ($files) {
            foreach ($files as $file) {
                $code = strtolower(basename($file, '.php'));
                $conf = APPPATH . 'modules/payment/config/' . $code . '.php';

                if (file_exists($conf)) {
                    require_once($conf);
                    if (isset($payments[$code])) {
                        $old[] = array(
                            'code' => $code,
                            'name' => $config['manifest']['name'],
                            'description' => $config['manifest']['description'],
                            'version' => $config['manifest']['version'],
                        );
                    } else {
                        $new[] = array(
                            'code' => $code,
                            'name' => $config['manifest']['name'],
                            'description' => $config['manifest']['description'],
                            'version' => $config['manifest']['version']
                        );
                    }

                    $codes[] = $code;
                }
            }

            $this->db->where_not_in('code', $codes)->delete('payment');

            if ($old) $this->db->update_batch('payment', $old, 'code');
            if ($new) $this->db->insert_batch('payment', $new);
        } else {
            $this->db->empty_table('payment');
        }
    }

    /**
     * Is installed?
     *
     * @access public
     * @param string $code
     * @return bool
     */
    public function is_installed($code)
    {
        return (bool)$this->db
            ->where('code', $code)
            ->where('active', 1)
            ->count_all_results('payment');
    }


    public function is_added($code)
    {
        return (bool)$this->db
            ->where('code', $code)
            ->count_all_results('addon');
    }
}