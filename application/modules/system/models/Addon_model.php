<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Addon_model extends MY_Model
{
    /**
     * Primary key
     *
     * @var string
     * @access protected
     */
    protected $primary_key = 'code';

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Match module package files with database
     *
     * @access public
     * @return void
     */
    public function matches()
    {
        $addons = array();
        $old = array();
        $new = array();
        $codes = array();
        $files = glob(FCPATH . 'addons/*');

        foreach ($this->get_all() as $module) {
            $addons[$module['code']] = $module;
        }

        if ($files) {
            foreach ($files as $file) {
                $code = basename($file);
                $conf = $file . '/config/manifest.php';
                if (file_exists($conf)) {
                    require_once($conf);
                    if (isset($addons[$code])) {
                        $old[] = array(
                            'code' => $code,
                            'name' => $config['name'],
                            'description' => $config['description'],
                            'version' => $config['version'],
                        );
                    } else {
                        $new[] = array(
                            'code' => $code,
                            'name' => $config['name'],
                            'description' => $config['description'],
                            'version' => $config['version']
                        );
                    }

                    $codes[] = $code;
                }
            }

            $this->db->where_not_in('code', $codes)->delete('addon');

            if ($old) $this->db->update_batch('addon', $old, 'code');
            if ($new) $this->db->insert_batch('addon', $new);
        } else {
            $this->db->empty_table('addon');
        }
    }

    /**
     * Is installed?
     *
     * @access public
     * @param string $code
     * @return bool
     */
    public function is_installed($code)
    {
        return (bool)$this->db
            ->where('code', $code)
            ->where('active', 1)
            ->count_all_results('addon');
    }

    public function is_added($code)
    {
        return (bool)$this->db
            ->where('code', $code)
            ->count_all_results('addon');
    }

    public function get_addons()
    {
        return $this->db
            ->where('active', 1)
            ->get('addon')
            ->result_array();
    }
}