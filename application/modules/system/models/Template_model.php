<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function is_added($code)
    {
        return (bool)$this->db
            ->where('code', $code)
            ->count_all_results('template');
    }
}