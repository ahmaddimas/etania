<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['system/addons'] = 'Modul Add Ons';
$lang['system/length_class'] = 'Satuan Panjang';
$lang['system/order_status'] = 'Status Order';
$lang['system/payments'] = 'Modul Pembayaran';
$lang['system/setting'] = 'Pengaturan';
$lang['system/stock_status'] = 'Status Stok';
$lang['system/weight_class'] = 'Satuan Berat';