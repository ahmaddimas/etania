<section class="sidebar">
    <div class="user-panel">
        <div class="pull-left image"><img src="<?= $this->admin_auth->image() ?>" alt="<?= $this->admin_auth->name() ?>"
                                          class="img-circle"></div>
        <div class="pull-left info">
            <p><?= $this->admin_auth->name() ?></p>
            <p class="designation"><?= $this->admin_auth->group() ?></p>
        </div>
    </div>
    <ul class="sidebar-menu">
        <li><a href="<?= admin_url() ?>"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
        <li class="treeview">
            <a href="#"><i class="fa fa-tags"></i><span>Katalog</span><i class="fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="<?= admin_url('catalog/product') ?>">Produk</a></li>
                <li><a href="<?= admin_url('catalog/category') ?>">Kategori Produk</a></li>
                <li><a href="<?= admin_url('catalog/filter') ?>">Filter Produk</a></li>
                <li><a href="<?= admin_url('catalog/option') ?>">Opsi Produk</a></li>
                <li><a href="<?= admin_url('catalog/attribute') ?>">Atribut Produk</a></li>
                <li><a href="<?= admin_url('catalog/manufacturer') ?>">Brand / Manufaktur</a></li>
                <li><a href="<?= admin_url('catalog/review') ?>">Ulasan Produk</a></li>
            </ul>
        </li>
        <?php if ($addons) { ?>
            <li class="treeview"><a href="#"><i class="fa fa-puzzle-piece"></i><span>Extensi</span><i
                            class="fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <?php foreach ($addons as $addon) { ?>
                        <?php if ($addon['menus']) { ?>
                            <li class="treeview"><a href="#"><?= $addon['title'] ?></span><i
                                            class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <?php foreach ($addon['menus'] as $menu) { ?>
                                        <li><a href="<?= $menu['url'] ?>"><?= $menu['title'] ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } else { ?>
                            <li><a href="<?= $addon['url'] ?>"><?= $addon['title'] ?></a></li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>
        <li><a href="<?= admin_url('order') ?>"><i class="fa fa-shopping-basket"></i><span>Pesanan</span></a></li>
        <li class="treeview">
            <a href="#"><i class="fa fa-thumb-tack"></i><span>Konten</span><i class="fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="<?= admin_url('page') ?>">Halaman</a></li>
                <li><a href="<?= admin_url('banner') ?>">Banner</a></li>
            </ul>
        </li>


        <li class="treeview">
            <a href="#"><i class="fa fa-bars"></i><span>Menu</span><i class="fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="<?= admin_url('menu') ?>">Daftar Menu</a></li>
                <li><a href="<?= admin_url('menu/socialmedia') ?>">Social Media</a></li>
            </ul>
        </li>


        <li class="treeview">
            <a href="#"><i class="fa fa-user"></i><span>Pelanggan</span><i class="fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="<?= admin_url('customer') ?>">Pelanggan</a></li>
                <li><a href="<?= admin_url('customer/group') ?>">Grup Pelanggan</a></li>
                <li><a href="<?= admin_url('customer/email') ?>">Email</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#"><i class="fa fa-envelope"></i><span>Newsletter</span><i class="fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="<?= admin_url('newsletter/create') ?>">Create Newsletter</a></li>
                <li><a href="<?= admin_url('newsletter/member') ?>">Subscriber List</a></li>
                <li><a href="<?= admin_url('newsletter/group') ?>">Group Filter</a></li>
                <li><a href="<?= admin_url('newsletter/template') ?>">Template</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#"><i class="fa fa-key"></i><span>Administrator</span><i class="fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="<?= admin_url('admin') ?>">Admin</a></li>
                <li><a href="<?= admin_url('admin/group') ?>">Grup Admin</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#"><i class="fa fa-cog"></i><span>Sistem</span><i class="fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="<?= admin_url('system/setting') ?>">Pengaturan</a></li>
                <li><a href="<?= admin_url('system/payments') ?>">Metode Pembayaran</a></li>
                <li><a href="<?= admin_url('system/addons') ?>">Add-Ons</a></li>
                <li><a href="<?= admin_url('system/order_status') ?>">Status Order</a></li>
                <li><a href="<?= admin_url('system/stock_status') ?>">Status Stok</a></li>
                <li><a href="<?= admin_url('system/weight_class') ?>">Satuan Berat</a></li>
                <li><a href="<?= admin_url('system/length_class') ?>">Satuan Panjang</a></li>
                <li><a href="<?= admin_url('system/template') ?>">Template Halaman</a></li>
                <li><a href="<?= admin_url('system/about') ?>">About</a></li>
            </ul>
        </li>
        <li><a href="<?= admin_url('logout') ?>"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
    </ul>
</section>