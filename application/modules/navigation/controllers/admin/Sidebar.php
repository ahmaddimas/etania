<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebar extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $data = array();

        $this->load->model('system/addon_model');

        $data['addons'] = array();

        foreach ($this->addon_model->get_addons() as $addon) {
            $setting = $this->config->item('addon_' . $addon['code']);
            if ($setting['active']) {
                $module = $addon['code'];
                $this->load->model($module . '/' . $module . '_model');

                $data['addons'][] = array(
                    'title' => $addon['name'],
                    'url' => admin_url('offers'),
                    'icon' => 'fa fa-database',
                    'menus' => $this->{$module . '_model'}->get_menus()
                );
            }
        }

        return $this->load->layout(null)->view('admin/sidebar', $data, true);
    }
}