<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_method extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('shopping_cart');
        $this->load->library('form_validation');
        $this->load->library('currency');
        $this->load->helper('form');
        $this->load->helper('language');
        $this->load->model('customer/address_model');
        $this->load->model('location/location_model');
        $this->lang->load('checkout/checkout');

        $this->form_validation->CI =& $this;
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        check_ajax();

        if ($this->input->post()) {
            $this->lang->load('checkout');

            $json = array();

            if (!$this->shopping_cart->has_products() || !$this->shopping_cart->has_stock()) {
                $json['redirect'] = site_url('cart');
            }

            $products = $this->shopping_cart->get_products();

            foreach ($products as $product) {
                $product_total = 0;

                foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $product['product_id']) {
                        $product_total += $product_2['quantity'];
                    }
                }

                if ($product['minimum'] > $product_total) {
                    $json['redirect'] = site_url('cart');
                    break;
                }
            }

            $payment_methods = $this->session->userdata('payment_methods');

            if (!$json) {
                if (!$this->input->post('payment_method')) {
                    $json['error']['warning'] = $this->lang->line('error_payment');
                } elseif (!isset($payment_methods[$this->input->post('payment_method')])) {
                    $json['error']['warning'] = $this->lang->line('error_payment');
                }

                if (!$json) {
                    $this->session->set_userdata('payment_method', $payment_methods[$this->input->post('payment_method')]);
                }
            }

            $this->output->set_output(json_encode($json));
        } else {
            $this->lang->load('checkout');
            $this->load->model('customer/address_model');

            $payment_address = array();
            if ($this->customer->is_logged() and $this->session->userdata('shipping_address_id')) {
                $payment_address = $this->address_model->get_address($this->session->userdata('shipping_address_id'));
            } elseif ($this->session->userdata('guest')) {
                $guest = $this->session->userdata('guest');
                $payment_address = isset($guest['payment']) ? $guest['payment'] : array();
            }

            $this->load->library('total');
            $this->load->helper('array');

            list($total_data, $total) = $this->total->get_totals();

            $total_data = reorder($total_data, 'sort_order', 'asc');

            $method_data = array();

            $this->load->model('system/payment_model');

            $results = $this->payment_model->get_all_by(array('active' => 1));

            foreach ($results as $result) {

                if ($this->config->item('active', 'payment_' . $result['code'])) {
                    $model = strtolower($result['code']) . '_model';

                    $this->load->model('payment/' . $model);

                    $method = $this->{$model}->get_method($payment_address, $total);


                    if ($method) {
                        if (count($method) < 3) {
                            foreach ($method as $key => $val) {
                                $method_data[$val['code']] = $val;
                            }
                        } else {
                            $method_data[$result['code']] = $method;
                        }
                    }
                }
            }


            $sort_order = array();

            foreach ($method_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $method_data);

            $this->session->set_userdata('payment_methods', $method_data);

            $data['text_payment_method'] = $this->lang->line('text_payment_method');
            $data['text_comment'] = $this->lang->line('text_comment');

            if (!$this->session->userdata('payment_methods')) {
                $data['error_warning'] = sprintf($this->lang->line('error_no_payment'), site_url('contact'));
            } else {
                $data['error_warning'] = '';
            }

            if ($this->session->userdata('payment_methods')) {
                $data['payment_methods'] = $this->session->userdata('payment_methods');
            } else {
                $data['payment_methods'] = array();
            }

            $payment_method = $this->session->userdata('payment_method');

            if (isset($payment_method['code'])) {
                $data['code'] = $payment_method['code'];
            } else {
                $data['code'] = '';
            }

            $data['action'] = site_url('checkout/payment_method');

            return $this->load->layout(false)->view('payment_method', $data);
        }
    }
}