<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping_address extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('shopping_cart');
        $this->load->library('form_validation');
        $this->load->library('currency');
        $this->load->helper('form');
        $this->load->helper('language');
        $this->load->model('customer/address_model');
        $this->load->model('location/location_model');
        $this->lang->load('checkout/checkout');

        $this->form_validation->CI =& $this;
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        check_ajax();

        if ($this->input->post()) {
            $json = array();

            if (!$this->customer->is_logged()) {
                $json['redirect'] = site_url('checkout');
            }

            if (!$this->shopping_cart->has_products() || (!$this->shopping_cart->has_stock() && $this->config->item('stock_checkout'))) {
                $json['redirect'] = site_url('cart');
            }

            $products = $this->shopping_cart->get_products();

            foreach ($products as $product) {
                $product_total = 0;

                foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $product['product_id']) {
                        $product_total += $product_2['quantity'];
                    }
                }

                if ($product['minimum'] > $product_total) {
                    $json['redirect'] = site_url('cart');
                    break;
                }
            }

            if (!$json) {
                if ($this->input->post('shipping_address') && $this->input->post('shipping_address') == 'existing') {
                    if (!$this->input->post('address_id')) {
                        $json['error']['warning'] = lang('error_address');
                    } elseif (!$this->address_model->check_address($this->input->post('address_id'), $this->customer->customer_id())) {
                        $json['error']['warning'] = lang('error_address');
                    } else {
                        $address_info = $this->address_model->get_address($this->input->post('address_id'));
                    }

                    if (!$json) {
                        $this->session->set_userdata('shipping_address_id', $this->input->post('address_id'));

                        if ($address_info) {
                            $this->session->set_userdata('shipping_province_id', $address_info['province_id']);
                            $this->session->set_userdata('shipping_city_id', $address_info['city_id']);
                            $this->session->set_userdata('shipping_subdistrict_id', $address_info['subdistrict_id']);
                        } else {
                            $this->session->unset_userdata('shipping_province_id');
                            $this->session->unset_userdata('shipping_city_id');
                            $this->session->unset_userdata('shipping_subdistrict_id');
                        }

                        $this->session->unset_userdata('shipping_method');
                        $this->session->unset_userdata('shipping_methods');
                    }
                } else {
                    $this->form_validation
                        ->set_rules('name', 'lang:entry_name', 'trim|required|min_length[3]')
                        ->set_rules('address', 'lang:entry_address', 'trim|required|min_length[3]|max_length[128]')
                        ->set_error_delimiters('', '');

                    if ($this->form_validation->run() == false) {
                        $errors = validation_errors();

                        foreach (array('name', 'address') as $field) {
                            if (form_error($field) != '') $json['error'][$field] = form_error($field);
                        }
                    } else {
                        $post['customer_id'] = (int)$this->customer->customer_id();
                        $post['name'] = $this->input->post('name');
                        $post['address'] = $this->input->post('address');
                        $post['postcode'] = $this->input->post('postcode');
                        $post['province_id'] = (int)$this->input->post('province_id');
                        $post['city_id'] = (int)$this->input->post('city_id');
                        $post['subdistrict_id'] = (int)$this->input->post('subdistrict_id');

                        $this->session->set_userdata('shipping_address_id', $this->address_model->insert($post));
                        $this->session->set_userdata('shipping_province_id', $this->input->post('province_id'));
                        $this->session->set_userdata('shipping_city_id', $this->input->post('city_id'));
                        $this->session->set_userdata('shipping_subdistrict_id', $this->input->post('subdistrict_id'));

                        $this->session->unset_userdata('shipping_method');
                        $this->session->unset_userdata('shipping_methods');
                    }
                }
            }

            $this->output->set_output(json_encode($json));
        } else {
            $data['action'] = site_url('checkout/shipping_address');
            $data['address_id'] = $this->session->userdata('shipping_address_id') ? $this->session->userdata('shipping_address_id') : 0;
            $data['addresses'] = $this->address_model->get_addresses($this->customer->customer_id());
            $data['provinces'] = $this->location_model->get_provinces();
            $data['province_id'] = $this->session->userdata('shipping_province_id') ? $this->session->userdata('shipping_province_id') : '';
            $data['city_id'] = $this->session->userdata('shipping_city_id') ? $this->session->userdata('shipping_city_id') : '';
            $data['subdistrict_id'] = $this->session->userdata('shipping_subdistrict_id') ? $this->session->userdata('shipping_subdistrict_id') : '';

            return $this->load->layout(false)->view('shipping_address', $data);
        }
    }
}