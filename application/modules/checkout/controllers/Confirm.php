<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Confirm extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('shopping_cart');
        $this->load->library('form_validation');
        $this->load->library('currency');
        $this->load->helper('form');
        $this->load->helper('language');
        $this->load->model('customer/address_model');
        $this->load->model('location/location_model');
        $this->lang->load('checkout/checkout');

        $this->form_validation->CI =& $this;
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        check_ajax();

        $this->load->model('customer_model');
        $this->load->model('address_model');

        $redirect = '';

        if ($this->shopping_cart->has_shipping()) {
            $shipping_address = $this->address_model->get_address($this->session->userdata('shipping_address_id'));

            if (empty($shipping_address)) {
                $redirect = site_url('checkout');
            }

            if (!$this->session->userdata('shipping_method')) {
                $redirect = site_url('checkout');
            }
        } else {
            $this->session->unset_userdata('shipping_method');
            $this->session->unset_userdata('shipping_methods');
        }

        if (!$this->session->userdata('payment_method')) {
            $redirect = site_url('checkout');
        }

        if (!$this->shopping_cart->has_products()) {
            $redirect = site_url('cart');
        }

        $products = $this->shopping_cart->get_products();
        $product_names = array();

        foreach ($products as $product) {
            $product_names[] = $product['name'];

            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($this->config->item('stock_checkout') && $product['minimum'] < $product_total) {
                $redirect = site_url('cart');
                break;
            }
        }

        if (!$redirect) {
            $this->load->library('total');
            $this->load->helper('array');

            list($total_data, $total) = $this->total->get_totals();

            $total_data = reorder($total_data, 'sort_order', 'asc');

            $customer = $this->customer_model->get($this->customer->customer_id());

            $order = array();

            $address = $this->address_model->get_address($this->session->userdata('shipping_address_id'));
            $shipping_method = $this->session->userdata('shipping_method');

            $order['invoice_prefix'] = $this->config->item('invoice_prefix');
            $order['customer_id'] = $this->customer->customer_id();
            $order['name'] = $this->customer->name();
            $order['email'] = $this->customer->email();
            $order['telephone'] = $customer['telephone'];
            $order['address'] = $address['address'];
            $order['postcode'] = $address['postcode'];
            $order['province'] = $address['province'];
            $order['province_id'] = $address['province_id'];
            $order['city'] = $address['city'];
            $order['city_id'] = $address['city_id'];
            $order['subdistrict'] = $address['subdistrict'];
            $order['subdistrict_id'] = $address['subdistrict_id'];
            $order['user_agent'] = $this->input->user_agent();
            $order['total'] = $total;
            $order['currency_id'] = $this->currency->get_id();
            $order['currency_value'] = $this->currency->get_value($this->currency->get_code());
            $order['ip'] = $this->input->ip_address();

            $payment_method = $this->session->userdata('payment_method');

            $order['payment_method'] = isset($payment_method['title']) ? $payment_method['title'] : '';
            $order['payment_code'] = isset($payment_method['code']) ? $payment_method['code'] : '';

            $shipping_method = $this->session->userdata('shipping_method');

            $order['shipping_method'] = isset($shipping_method['title']) ? $shipping_method['title'] : '';
            $order['shipping_code'] = isset($shipping_method['code']) ? $shipping_method['code'] : '';
            $order['comment'] = $this->session->userdata('comment');

            $product_data = array();

            foreach ($this->shopping_cart->get_products() as $product) {
                $product_data[] = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'sku' => $product['sku'],
                    'quantity' => $product['quantity'],
                    'price' => $product['price'],
                    'total' => $product['total'],
                    'option' => $product['option']
                );
            }

            $this->load->model('order/order_model');

            $order_id = $this->session->userdata('order_id');

            if ($order_id) {
                $this->order_model->update_order($order_id, $order, $product_data, $total_data);
            } else {
                $order_id = $this->order_model->create_order($order, $product_data, $total_data);
                $this->session->set_userdata('order_id', $order_id);
            }

            $not_digital = $this->shopping_cart->get_product_digital();
            if ($not_digital == 0) {
                $this->load->model('digital/order_digital_model');
                $digital_order_id = $this->order_digital_model->cekorder($order_id);
                if ($digital_order_id == 0) {
                    $this->order_digital_model->create($order_id);
                }
            }


            if (file_exists(APPPATH . 'modules/payment/controllers/' . ucfirst($payment_method['code']) . '.php')) {
                $data['payment'] = Modules::run('payment/' . $payment_method['code'] . '/index');
            } else {
                if (file_exists(APPPATH . 'modules/payment/controllers/' . ucfirst($payment_method['type']) . '.php')) {
                    $data['payment'] = Modules::run('payment/' . $payment_method['type'] . '/index');
                } else {
                    $data['payment'] = 'Tidak ada metode pembayaran yang dipilih!';
                }
            }


            $data['products'] = $product_data;
            $data['totals'] = $total_data;
            $data['comment'] = $order['comment'];
        } else {
            $data['redirect'] = $redirect;
        }

        return $this->load
            ->module('checkout')
            ->layout(false)
            ->view('confirm', $data);
    }
}