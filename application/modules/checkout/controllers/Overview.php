<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Overview extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('shopping_cart');
        $this->load->library('form_validation');
        $this->load->library('currency');
        $this->load->helper('form');
        $this->load->helper('language');
        $this->load->model('customer/address_model');
        $this->load->model('location/location_model');
        $this->lang->load('checkout/checkout');

        $this->form_validation->CI =& $this;
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        check_ajax();

        $this->load->library('image');
        $this->load->library('currency');
        $this->load->library('total');
        $this->load->helper('array');
        $this->load->model('category_model');

        list($total_data, $total) = $this->total->get_totals();

        $total_data = reorder($total_data, 'sort_order', 'asc');

        $data['products'] = array();

        $pcwd = $this->config->item('image_product_compare_width');
        $pchg = $this->config->item('image_product_compare_height');
        foreach ($this->shopping_cart->get_products() as $product) {
            if ($product['image']) {
                $image = $this->image->resize($product['image'], $pcwd, $pchg);
            } else {
                $image = '';
            }

            $path = '';

            if ($category = $this->category_model->get_category($product['category_id'])) {
                if ($category['path_id']) {
                    $category_ids = explode('-', $category['path_id']);
                    foreach ($category_ids as $category_id) {
                        if ($category_path = $this->category_model->get_category($category_id)) {
                            $path .= $category_path['slug'] . '/';
                        }
                    }
                }

                $path .= $category['slug'] . '/';
            }

            $data['products'][] = array(
                'key' => $product['key'],
                'thumb' => $image,
                'name' => $product['name'],
                'quantity' => $product['quantity'],
                'price' => $this->currency->format($product['price']),
                'total' => $this->currency->format($product['price'] * $product['quantity']),
                'href' => site_url('product/' . $path . $product['slug'])
            );
        }

        $data['totals'] = array();

        foreach ($total_data as $result) {
            $data['totals'][] = array(
                'title' => $result['title'],
                'text' => $this->currency->format($result['value']),
            );
        }

        $this->load->model('address_model');

        if ($this->customer->is_logged() && $this->session->userdata('shipping_address_id')) {
            $shipping_address = $this->address_model->get_address($this->session->userdata('shipping_address_id'));
        } elseif ($this->session->userdata('guest')) {
            $guest = $this->session->userdata('guest');
            $shipping_address = $guest['shipping'];
        }

        if (!empty($shipping_address)) {
            $data['shipping_address'] = $shipping_address['address'] . ', ' . $shipping_address['subdistrict'] . ', ' . $shipping_address['city'] . ' - ' . $shipping_address['province'] . ' ' . $shipping_address['postcode'];
        } else {
            $data['shipping_address'] = '';
        }

        echo $this->load->layout(false)->view('overview', $data);
    }
}