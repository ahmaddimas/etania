<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Success extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('shopping_cart');
        $this->lang->load('checkout/checkout');
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->session->userdata('order_id')) {
            $this->shopping_cart->clear();

            $this->session->unset_userdata('shipping_method');
            $this->session->unset_userdata('shipping_methods');
            $this->session->unset_userdata('payment_method');
            $this->session->unset_userdata('payment_methods');
            $this->session->unset_userdata('comment');
            $this->session->unset_userdata('order_id');
            $this->session->unset_userdata('coupon');
            $this->session->unset_userdata('totals');

            $data['heading_title'] = lang('heading_title');

            if ($this->customer->is_logged()) {
                $data['text_message'] = sprintf(lang('text_customer'), site_url('customer'), site_url('customer/order'), site_url('customer/download'), site_url('contact'));
            } else {
                $data['text_message'] = sprintf(lang('text_guest'), site_url('contact'));
            }


            // echo "send mail";

            $data['continue'] = site_url();

            $this->load
                ->title('Pesanan Berhasil')
                ->breadcrumb('Home', site_url())
                ->breadcrumb('Checkout', site_url('checkout'))
                ->breadcrumb('Pesanan Berhasil', site_url('checkout/success'))
                ->view('success', $data);
        } else {
            show_404();
        }
    }
}