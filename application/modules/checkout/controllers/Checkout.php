<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('shopping_cart');
        $this->load->library('form_validation');
        $this->load->library('currency');
        $this->load->helper('form');
        $this->load->helper('language');
        $this->load->model('customer/address_model');
        $this->load->model('location/location_model');
        $this->lang->load('checkout/checkout');

        $this->form_validation->CI =& $this;
    }

    /**
     * Checkout page
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if (!$this->customer->is_logged()) {
            $this->session->set_userdata('redirect', 'checkout');
            redirect('customer/login');
        }

        if (!$this->shopping_cart->has_products()) {
            redirect('checkout/cart');
        }

        if (!$this->shopping_cart->has_stock()) {
            $this->session->set_flashdata('error', 'Maaf, produk yang ingin Anda beli saat ini sedang kosong!');
            redirect('checkout/cart');
        }

        $products = $this->shopping_cart->get_products();

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                redirect('checkout/cart');
            }
        }

        $data['heading_title'] = lang('heading_title');
        $data['logged'] = $this->customer->is_logged();

        $this->load
            ->title(lang('heading_title'))
            ->breadcrumb(lang('text_home'), site_url())
            ->breadcrumb(lang('heading_title'), site_url('checkout'))
            ->view('checkout', $data);
    }
}