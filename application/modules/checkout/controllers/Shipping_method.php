<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping_method extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('shopping_cart');
        $this->load->library('form_validation');
        $this->load->library('currency');
        $this->load->helper('form');
        $this->load->helper('language');
        $this->load->model('customer/address_model');
        $this->load->model('location/location_model');
        $this->lang->load('checkout/checkout');

        $this->form_validation->CI =& $this;
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        check_ajax();


        $not_digital = $this->shopping_cart->get_product_digital();

        if ($this->input->post()) {
            $json = array();

            $this->load->model('address_model');

            if ($this->customer->is_logged() && $this->session->userdata('shipping_address_id')) {
                $shipping_address = $this->address_model->get_address($this->session->userdata('shipping_address_id'));
            } elseif ($this->session->userdata('guest')) {
                $guest = $this->session->userdata('guest');
                $shipping_address = $guest['shipping'];
            }

            if (empty($shipping_address)) {
                $json['redirect'] = site_url('checkout');
            }

            if (!$json) {
                if (!$this->input->post('shipping_method')) {
                    $json['error']['warning'] = $this->lang->line('error_shipping');
                } else {
                    $shipping = explode('.', $this->input->post('shipping_method'));
                    $shipping_methods = $this->session->userdata('shipping_methods');

                    if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($shipping_methods[$shipping[0]]['quote'][$shipping[1]])) {
                        $json['error']['warning'] = $this->lang->line('error_shipping');
                    }
                }

                if (!$json) {
                    $shipping = explode('.', $this->input->post('shipping_method'));
                    $shipping_methods = $this->session->userdata('shipping_methods');

                    $this->session->set_userdata('shipping_method', $shipping_methods[$shipping[0]]['quote'][$shipping[1]]);
                    $this->session->set_userdata('comment', strip_tags($this->input->post('comment')));
                }
            }

            $this->output->set_output(json_encode($json));
        } else {
            if ($this->customer->is_logged() && $this->session->userdata('shipping_address_id')) {
                $shipping_address = $this->address_model->get_address($this->session->userdata('shipping_address_id'));
            } elseif ($this->session->userdata('guest')) {
                $guest = $this->session->userdata('guest');
                $shipping_address = $guest['shipping'];
            }

            if (!empty($shipping_address)) {
                $quote_data = array();

                if ($not_digital == 1) {
                    $quote = $this->rajaongkir->get_quote($shipping_address);

                    if ($quote) {
                        $quote_data[$quote['code']] = array(
                            'title' => $quote['title'],
                            'quote' => $quote['quote'],
                            'error' => $quote['error']
                        );
                    }
                } else {

                    $quote['free'] = array(
                        'code' => 'rajaongkir.free',
                        'title' => 'No Courier (Digital Product)',
                        'cost' => '0',
                        'text' => 'Rp 0',
                    );


                    $quote_data['rajaongkir'] = array(
                        'title' => 'Metode Pengiriman',
                        'quote' => $quote,
                        'error' => ''
                    );
                }

                $this->session->set_userdata('shipping_methods', $quote_data);

                $data['shipping_methods'] = $quote_data;
            }

            $data['text_shipping_method'] = $this->lang->line('text_shipping_method');
            $data['text_comment'] = $this->lang->line('text_comment');

            if (!$this->session->userdata('shipping_methods')) {
                $data['error_warning'] = sprintf($this->lang->line('error_no_shipping'), site_url('contact'));
            } else {
                $data['error_warning'] = '';
            }

            if ($this->session->userdata('shipping_methods')) {
                $data['shipping_methods'] = $this->session->userdata('shipping_methods');
            } else {
                $data['shipping_methods'] = array();
            }

            if ($this->session->userdata('shipping_method')) {
                $shipping = $this->session->userdata('shipping_method');
                $data['code'] = $shipping['code'];
            } else {
                $data['code'] = '';
            }

            if ($this->session->userdata('comment')) {
                $data['comment'] = $this->session->userdata('comment');
            } else {
                $data['comment'] = '';
            }

            $data['action'] = site_url('checkout/shipping_method');

            return $this->load->layout(false)->view('shipping_method', $data);
        }
    }
}