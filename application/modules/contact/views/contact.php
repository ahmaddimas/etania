<div style="background-color:#fff;">
    <div class="breadcrumb full-width">
        <div class="background">
            <div class="pattern">
                <div class="container">
                    <div class="clearfix">
                        <h1 id="title-page"><?= $template['title'] ?></h1>
                        <ul>
                            <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                                <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-content full-width inner-page">
        <div class="background">
            <div class="pattern">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-offset-3">
                            <?= form_open($action, 'id="contact-form" class="contact-form" style="padding-top:30px;"') ?>
                            <div class="form-group">
                                <input type="text" name="name" class="form-control"
                                       placeholder="<?= lang('entry_name') ?>">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control"
                                       placeholder="<?= lang('entry_email') ?>">
                            </div>
                            <div class="form-group">
                                <input type="text" name="telephone" class="form-control"
                                       placeholder="<?= lang('entry_telephone') ?>">
                            </div>
                            <div class="form-group">
                                <input type="text" name="subject" class="form-control"
                                       placeholder="<?= lang('entry_subject') ?>">
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="message" style="height:auto;" class="form-control"
                                          rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6" id="captcha"></div>
                                    <div class="col-md-6 text-right">
                                        <button type="button" class="btn btn-warning btn-sm" id="reset-captcha"><i
                                                    class="fa fa-refresh" title="Reload Image"></i></button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" name="captcha" placeholder="<?= lang('entry_captcha') ?>"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <a id="submit" class="btn btn-primary btn-lg"><?= lang('button_submit') ?></a>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        var btn = $(this);
        $.ajax({
            url: $('#contact-form').attr('action'),
            data: $('#contact-form').serialize(),
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=site_url()?>";
                }
            }
        });
    });

    $(document).delegate('#reset-captcha', 'click', function (e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            url: "<?=site_url('captcha')?>",
            dataType: 'html',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (html) {
                $('#captcha').html(html);
            }
        });
    });

    $('#reset-captcha').trigger('click');
</script>