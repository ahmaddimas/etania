<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Hubungi kami';
$lang['text_location'] = 'Lokasi kami';
$lang['text_store'] = 'Toko kami';
$lang['text_contact'] = 'Form Kontak';
$lang['text_address'] = 'Alamat';
$lang['text_telephone'] = 'Telepon';
$lang['text_fax'] = 'Fax';
$lang['text_open'] = 'Jam Buka';
$lang['text_comment'] = 'Komentar';
$lang['text_success'] = 'Pertanyaan Anda telah berhasil dikirim';
$lang['entry_name'] = 'Nama Anda';
$lang['entry_email'] = 'Alamat E-Mail';
$lang['entry_enquiry'] = 'Pertanyaan';
$lang['entry_subject'] = 'Perihal';
$lang['entry_telephone'] = 'No. Telepon';
$lang['entry_captcha'] = 'Masukkan kode gambar di atas';
$lang['email_subject'] = 'Pertanyaan %s';
$lang['error_captcha'] = 'Kode verifikasi tidak sesuai dengan gambar!';
