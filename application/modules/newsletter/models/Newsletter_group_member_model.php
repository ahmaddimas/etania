<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_group_member_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getwherenot($newsletter_group_id)
    {
        $res = $this->db
            ->select('n.newsletter_id', false)
            ->join('newsletter n', 'n.newsletter_id = ngm.newsletter_id', 'left')
            ->where('ngm.newsletter_group_id', $newsletter_group_id)
            ->from('newsletter_group_member ngm')
            ->group_by('n.newsletter_id')
            ->get()
            ->result_array();

        $data = array(0 => 0);
        for ($i = 0; $i < (count($res)); $i++) {
            $data[] = $res[$i]['newsletter_id'];
        }


        $res2 = $this->db
            ->select('newsletter_id', false)
            ->where_not_in('newsletter_id', $data)
            ->from('newsletter')
            ->get()
            ->result_array();

        $data2 = array();
        for ($i2 = 0; $i2 < (count($res2)); $i2++) {
            $data2[] = $res2[$i2]['newsletter_id'];
        }

        return $data2;
    }


    public function deletegroup($newsletter_group_id)
    {

        if (is_array($newsletter_group_id)) {
            $this->db->where_in('newsletter_group_id', (array)$newsletter_group_id);
        } else {
            $this->db->where('newsletter_group_id', $newsletter_group_id);
        }

        $this->db->delete('newsletter_group_member');
    }

}