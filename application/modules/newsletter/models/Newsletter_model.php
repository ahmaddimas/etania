<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Check email
     *
     * @access public
     * @param string $email
     * @param int $newsletter_id
     * @return bool
     */
    public function check_email($email, $newsletter_id = null)
    {
        if ($newsletter_id) {
            $this->db->where('newsletter_id !=', (int)$newsletter_id);
        }

        return (bool)$this->db
            ->select('newsletter_id')
            ->where('email', strtolower($email))
            ->count_all_results('newsletter');
    }


    /**
     * Check telephone
     *
     * @access public
     * @param string $telephone
     * @param int $newsletter_id
     * @return bool
     */
    public function check_telephone($telephone, $newsletter_id = null)
    {
        if ($newsletter_id) {
            $this->db->where('newsletter_id !=', (int)$newsletter_id);
        }

        return (bool)$this->db
            ->select('newsletter_id')
            ->where('telephone', (string)$telephone)
            ->count_all_results('newsletter');
    }


    public function import($members)
    {
        $current_products = $this->db->select('newsletter_id')->get('newsletter')->result_array();

        $product_ids = array();

        foreach ($current_products as $current_product) {
            $product_ids[] = $current_product['newsletter_id'];
        }

        $old_member = array();
        $new_member = array();

        foreach ($members as $member) {
            $member['date_modified'] = date('Y-m-d H:i:s', time());

            if (in_array($member['newsletter_id'], $product_ids)) {
                $old_member[] = $member;
            } else {
                $member['date_added'] = date('Y-m-d H:i:s', time());
                $new_member[] = $member;
            }
        }

        if ($old_member) {
            $this->db->update_batch('newsletter', $old_member, 'newsletter_id');
        }

        if ($new_member) {
            $this->db->insert_batch('newsletter', $new_member);
        }
    }


    public function getgroup($newsletter_group_id)
    {
        return $this->db
            ->select('n.*', false)
            ->where('ng.newsletter_group_id', $newsletter_group_id)
            ->join('newsletter n', 'n.newsletter_id = ngm.newsletter_id', 'left')
            ->join('newsletter_group ng', 'ng.newsletter_group_id = ngm.newsletter_group_id', 'left')
            ->from('newsletter_group_member ngm')
            ->group_by('n.newsletter_id')
            ->get()
            ->result_array();
    }


    public function get_newsletter_autocomplete($data = array())
    {
        $this->db->from('newsletter');

        if (!empty($data['name'])) {
            $this->db->like('name', $data['name'], 'both');
            $this->db->or_like('newsletter_id', (int)$data['name'], 'both');
        }

        $this->db->group_by('newsletter_id');

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) $data['start'] = 0;
            if ($data['limit'] < 1) $data['limit'] = 20;

            $this->db->limit((int)$data['limit'], (int)$data['start']);
        }

        return $this->db->get()->result_array();
    }

}