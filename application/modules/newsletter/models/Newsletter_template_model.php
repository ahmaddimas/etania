<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_template_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function getactive()
    {
        $template = $this->db
            ->where('active', 1)
            ->get('newsletter_template')
            ->row_array();

        if (count($template) == 0) {
            $template = array(
                'template' => "Tidak ada template yang aktif"
            );
        }

        return $template;
    }


    public function updatetemplate($content)
    {
        $this->db
            ->where('active', 1)
            ->set('content', $content)
            ->update('newsletter_template');
    }


    public function setactive($newsletter_template_id)
    {
        $this->db
            ->where('newsletter_template_id', $newsletter_template_id)
            ->set('active', 1)
            ->update('newsletter_template');

        $this->db
            ->where('newsletter_template_id <>', $newsletter_template_id)
            ->set('active', 0)
            ->update('newsletter_template');
    }

}