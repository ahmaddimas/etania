<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a onclick="getForm(null, '/newsletter/group/create');" class="btn btn-success"><i
                    class="fa fa-plus-circle"></i> <?= lang('button_add') ?></a>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> <?= lang('button_refresh') ?>
        </a>
        <a onclick="delRecord();" class="btn btn-danger dropdown-toggle"><i
                    class="fa fa-trash"></i> <?= lang('button_delete') ?></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?= form_open(admin_url('newsletter/group/delete'), 'id="form-checks" target="_blank"') ?>
                <table class="table table-hover table-bordered" id="datatable" width="100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="select-all"></th>
                        <th>Name</th>
                        <th>Deskripsi</th>
                        <th class="text-right"></th>
                    </tr>
                    </thead>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="modal"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('newsletter/group')?>",
                "type": "POST",
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "newsletter_group_id"},
                {"data": "name"},
                {"data": "description"},
                {"orderable": false, "searchable": false, "data": "newsletter_group_id"},
            ],
            "sDom": 'lfr<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<input type="checkbox" class="checkbox" name="newsletter_group_id[]" value="' + data.newsletter_group_id + '"/>');
                $('td', row).eq(3).html('<a class="btn btn-sm btn-primary" style="cursor:pointer;" onclick="getForm(' + data.newsletter_group_id + ', \'newsletter/group/edit\');"><i class="fa fa-edit"></i> <?=lang('button_edit')?></a> <a class="btn btn-warning" style="cursor:pointer;" href="newsletter/group/detail/' + data.newsletter_group_id + '"><i class="fa fa-cog"></i> Manage</a> <a onclick="delRecord(' + data.newsletter_group_id + ');" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> Hapus</a>').addClass('text-right');

            },
            "order": [[3, 'asc']],
        });

        $('#select-all').click(function () {
            var checkBoxes = $('.checkbox');
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });
    });

    function delRecord(newsletter_group_id) {
        if (newsletter_group_id) {
            data = 'newsletter_group_id=' + newsletter_group_id;
        } else {
            if ($('.checkbox:checked').length === 0) {
                alert('Tidak ada data yang dipilih!');
                return false;
            }
            data = $('#form-checks').serialize();
        }

        swal({
            title: '<?=lang('text_warning')?>',
            text: '<?=lang('text_confirm_delete')?>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('newsletter/group/delete')?>',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
        $('#select-all').prop('checked', false);
    }

    function getForm(newsletter_group_id, path) {
        $.ajax({
            url: $('base').attr('href') + path,
            data: 'newsletter_group_id=' + newsletter_group_id,
            dataType: 'json',
            success: function (json) {
                if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
                } else if (json['content']) {
                    $('#modal').html('<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['content'] + '</div>');
                    $('#form-modal').modal('show');
                    $('#form-modal').on('hidden.bs.modal', function (e) {
                        refreshTable();
                    });
                }
            }
        });
    }
</script>