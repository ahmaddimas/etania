<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('newsletter/group/add/' . $newsletter_group_id) ?>" class="btn btn-success"><i
                    class="fa fa-plus-circle"></i> Tambah member</a>
        <a href="<?= admin_url('newsletter/group') ?>" class="btn btn-warning"><i
                    class="fa fa-arrow-left"></i> <?= lang('button_back') ?></a>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> <?= lang('button_refresh') ?>
        </a>
        <a onclick="delRecord();" class="btn btn-danger dropdown-toggle"><i
                    class="fa fa-trash"></i> <?= lang('button_delete') ?></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?= form_open(admin_url('newsletter/group/delete'), 'id="form-checks" target="_blank"') ?>
                <table class="table table-hover table-bordered" id="datatable" width="100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="select-all"></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Gender</th>
                        <th>City</th>
                        <th class="text-right"></th>
                    </tr>
                    </thead>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="modal"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('newsletter/group/detail/' . $newsletter_group_id)?>",
                "type": "POST",
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "newsletter_group_member_id"},
                {"data": "name"},
                {"data": "email"},
                {"data": "gender"},
                {"data": "city"},
                {"orderable": false, "searchable": false, "data": "newsletter_group_member_id"},
            ],
            "sDom": 'lfr<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<input type="checkbox" class="checkbox" name="newsletter_group_member_id[]" value="' + data.newsletter_group_member_id + '"/>');
                $('td', row).eq(5).html('<a class="btn btn-danger" style="cursor:pointer;" onclick="delRecord(' + data.newsletter_group_member_id + ');"><i class="fa fa-trash"></i> Hapus</a>').addClass('text-right');


                if (data.gender == "M") {
                    $('td', row).eq(3).html('Laki laki');
                } else if (data.gender == "F") {
                    $('td', row).eq(3).html('Prempuan');
                } else {
                    $('td', row).eq(3).html('Undefined');
                }

                if (data.city) {
                    $('td', row).eq(4).html(data.city);
                } else {
                    $('td', row).eq(4).html('undefined');
                }
            },
            "order": [[1, 'asc']],
        });

        $('#select-all').click(function () {
            var checkBoxes = $('.checkbox');
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });
    });

    function delRecord(newsletter_group_member_id) {
        if (newsletter_group_member_id) {
            data = 'newsletter_group_member_id=' + newsletter_group_member_id;
        } else {
            if ($('.checkbox:checked').length === 0) {
                alert('Tidak ada data yang dipilih!');
                return false;
            }
            data = $('#form-checks').serialize();
        }
        swal({
            title: '<?=lang('text_warning')?>',
            text: '<?=lang('text_confirm_delete')?>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '<?=lang('text_yes')?>',
            cancelButtonText: '<?=lang('text_no')?>',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('newsletter/group/deletemember/' . $newsletter_group_id)?>',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal('<?=lang('text_success')?>', json['success'], 'success');
                            refreshTable();
                        } else if (json['error']) {
                            swal('<?=lang('text_error')?>', json['error'], 'error');
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
        $('#select-all').prop('checked', false);
    }

</script>