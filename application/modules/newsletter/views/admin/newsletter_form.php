<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('newsletter/member') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>
        <a id="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?= form_open($action, 'id="form" class="form-horizontal" role="form"') ?>
                <input type="hidden" name="newsletter_id" value="<?= $newsletter_id ?>">
                <div id="message"></div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Title</label>
                    <div class="col-sm-9">
                        <input type="text" name="title" value="<?= $title ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" value="<?= $name ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Alamat</label>
                    <div class="col-sm-9">
                        <textarea type="text" name="address" class="form-control"><?= $address ?></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" name="email" value="<?= $email ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Telephone</label>
                    <div class="col-sm-9">
                        <input type="text" name="telephone" value="<?= $telephone ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                        <label><input type="radio" name="gender"
                                      value="M" <?php if ($gender == "M") echo "checked=checked"; ?> /> Laki
                            laki</label><br>
                        <label><input type="radio" name="gender"
                                      value="F" <?php if ($gender == "F") echo "checked=checked"; ?> />
                            Perempuan</label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3">Provinsi</label>
                    <div class="col-sm-9">
                        <select name="province_id" class="form-control">
                            <?php foreach ($provinces as $province) { ?>
                                <?php if ($province['province_id'] == $province_id) { ?>
                                    <option value="<?= $province['province_id'] ?>"
                                            selected="selected"><?= $province['name'] ?></option>
                                <?php } else { ?>
                                    <option value="<?= $province['province_id'] ?>"><?= $province['name'] ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Kota/Kabupaten</label>
                    <div class="col-sm-9">
                        <select name="city_id" class="form-control"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Kecamatan</label>
                    <div class="col-sm-9">
                        <select name="subdistrict_id" class="form-control"></select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Status</label>
                    <div class="toggle lg col-sm-9">
                        <label style="margin-top:5px;">
                            <?php if ($active) { ?>
                                <input type="checkbox" name="active" value="1" checked="checked">
                            <?php } else { ?>
                                <input type="checkbox" name="active" value="1">
                            <?php } ?>
                            <span class="button-indecator"></span>
                        </label>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').parent().addClass('has-error');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('newsletter/member')?>";
                }
            }
        });
    });

    $('select[name=\'province_id\']').on('change', function () {
        $.ajax({
            url: "<?=site_url('location/province')?>",
            data: 'province_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'province_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                html = '<option value="">-- Pilih --</option>';

                if (json['cities'] && json['cities'] != '') {
                    for (i = 0; i < json['cities'].length; i++) {
                        html += '<option value="' + json['cities'][i]['city_id'] + '"';

                        if (json['cities'][i]['city_id'] == '<?php echo $city_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['cities'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected">-- None --</option>';
                }

                $('select[name=\'city_id\']').html(html);
                $('select[name=\'city_id\']').trigger('change');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'city_id\']').on('change', function () {
        $.ajax({
            url: "<?=site_url('location/city')?>",
            data: 'city_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'city_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                html = '<option value="">-- Pilih --</option>';

                if (json['subdistricts'] && json['subdistricts'] != '') {
                    for (i = 0; i < json['subdistricts'].length; i++) {
                        html += '<option value="' + json['subdistricts'][i]['subdistrict_id'] + '"';

                        if (json['subdistricts'][i]['subdistrict_id'] == '<?php echo $subdistrict_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['subdistricts'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected">-- None --</option>';
                }

                $('select[name=\'subdistrict_id\']').html(html);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'province_id\']').trigger('change');
</script>