<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('page') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>
        <a id="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Kirim</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?= form_open($action, 'id="form" class="form-horizontal" role="form"') ?>
                <div id="message"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kepada</label>
                    <div class="col-sm-6">
                        <select name="to" class="form-control">
                            <option value="newsletter">Subcriber Newsletter Active</option>
                            <option value="subcriber_all">Semua Subcriber</option>
                            <option value="subcriber_group">Grup Subcriber</option>
                            <option value="subcriber">Pilih Subcriber</option>
                        </select>
                    </div>
                </div>
                <div id="subcriber">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Subcriber</label>
                        <div class="col-sm-10">
                            <input type="text" name="newsletter" value="" class="form-control" autocomplete="off"
                                   placeholder="Ketikkan nama Subcriber...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <div class="controls"
                                 style="border: 1px solid #ddd; height: 200px; background: #fff; overflow-y: scroll;">
                                <table class="table table-striped" width="100%">
                                    <tbody id="newsletter-list"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="subcriber_group">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Grup Subcriber</label>
                        <div class="col-sm-6">
                            <select name="newsletter_group_id" class="form-control">
                                <?php foreach ($subcriber_groups as $subcriber_group) { ?>
                                    <option value="<?= $subcriber_group['newsletter_group_id'] ?>"><?= $subcriber_group['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Judul</label>
                    <div class="col-sm-10">
                        <input type="text" name="subject" value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Konten</label>
                    <div class="col-sm-10">
                        <textarea name="body" id="body"
                                  class="form-control summernote"><?= $template['content'] ?></textarea>
                        <a id="savetemplate" class="btn btn-primary"><i class="fa fa-paper-save"></i> Simpan Template
                            Konten</a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Preview</label>
                    <div class="col-sm-10">
                        <div style="border:1px dashed #000; padding:10px; margin:10px"><?= $template['content'] ?></div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('select[name=\'to\']').on('change', function () {
        $('#subcriber, #subcriber_group').hide();
        $('#' + $(this).val()).show();
    });

    $('#submit').bind('click', function () {
        $btn = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $btn.button('loading');
            },
            complete: function () {
                $btn.button('reset');
            },
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\'], select[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\'], select[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').parent().addClass('has-error');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + json['success'] + '</div>');
                }
            }
        });
    });


    $('#savetemplate').bind('click', function () {
        content = $('#body').val();

        $btn = $(this);
        $.ajax({
            url: "<?=admin_url('newsletter/create/savetemplate')?>",
            data: 'content=' + content,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $btn.button('loading');
            },
            complete: function () {
                $btn.button('reset');
            },
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\'], select[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\'], select[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').parent().addClass('has-error');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + json['success'] + '</div>');
                    location.reload();
                }
            }
        });
    });

    $('input[name=\'newsletter\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('newsletter/member/autocomplete')?>",
                data: 'name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    newsletters = [];
                    map = {};
                    $.each(json, function (i, newsletter) {
                        map[newsletter.name] = newsletter;
                        newsletters.push(newsletter.name);
                    });
                    process(newsletters);
                }
            });
        },
        updater: function (item) {
            $('#newsletter-list' + map[item].newsletter_id).remove();
            $('#newsletter-list').append('<tr id="newsletter-list' + map[item].newsletter_id + '"><td>' + item + '<input type="hidden" name="newsletter[]" value="' + map[item].newsletter_id + '" /></td><td class="text-right"><a class="btn btn-danger btn-xs btn-flat" onclick="$(\'#newsletter-list' + map[item].newsletter_id + '\').remove();"><i class="fa fa-remove "></i></a></td></tr>');
        },
        minLength: 1
    });

    $('select[name=\'to\']').trigger('change');
</script>