<div class="page-title">
    <div>
        <h1>Tes</h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('newsletter/template') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>
        <a id="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?= form_open($action, 'id="form" class="form-horizontal" role="form"') ?>
                <input type="hidden" name="newsletter_template_id" value="<?= $newsletter_template_id ?>">
                <div id="message"></div>


                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" value="<?= $name ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Deskripsi</label>
                    <div class="col-sm-10">
                        <textarea type="text" name="description" class="form-control"><?= $description ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Konten</label>
                    <div class="col-sm-10">
                        <textarea name="content" id="body" class="form-control summernote"><?= $content ?></textarea>
                    </div>
                </div>

                <?php if ($newsletter_template_id <> "") { ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Preview</label>
                        <div class="col-sm-10">
                            <div style="border:1px dashed #000; padding:10px; margin:10px"><?= $content ?></div>
                        </div>
                    </div>
                <?php } ?>


                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').parent().addClass('has-error');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('newsletter/template')?>";
                }
            }
        });
    });
</script>