<div class="page-title">
    <div>
        <h1>Data Member</h1>
    </div>
    <div class="btn-group">
        <a onclick="addRecord();" class="btn btn-primary"><i class="fa fa-plus"></i> Tambahkan</a>
        <a href="<?= admin_url('newsletter/group/detail/' . $newsletter_group_id) ?>" class="btn btn-warning"><i
                    class="fa fa-arrow-left"></i> <?= lang('button_back') ?></a>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?php if ($success) { ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                        </button><?= $success ?></div>
                <?php } ?>
                <?php if ($error) { ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                        </button><?= $error ?></div>
                <?php } ?>
                <?= form_open(admin_url('newsletter/member/shipping'), 'id="form-checks" target="_blank"') ?>
                <table class="table table-hover table-bordered" id="datatable" width="100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="select-all"></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Gender</th>
                        <th>Telephone</th>
                        <th>City</th>
                        <th class="text-right" style="min-width:15%;"></th>
                    </tr>
                    </thead>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/js/ajaxupload.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('newsletter/group/add/' . $newsletter_group_id)?>",
                "type": "POST",
            },
            "columns": [
                {"orderable": false, "data": "newsletter_id"},
                {"data": "name"},
                {"data": "email"},
                {"data": "gender"},
                {"data": "telephone"},
                {"data": "city"},
                {"orderable": false, "data": "newsletter_id"},
            ],
            "sDom": 'lfr<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<input type="checkbox" class="checkbox" name="newsletter_id[]" value="' + data.newsletter_id + '"/>');
                if (data.city) {
                    $('td', row).eq(5).html(data.city);
                } else {
                    $('td', row).eq(5).html('undefined');
                }

                if (data.gender == "M") {
                    $('td', row).eq(3).html('Laki laki');
                } else if (data.gender == "F") {
                    $('td', row).eq(3).html('Prempuan');
                } else {
                    $('td', row).eq(3).html('Undefined');
                }

                html = '<div class="btn-group btn-group-xs">';
                html += '<a onclick="addRecord(' + data.newsletter_id + ');" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Tambahkan</a>';

                $('td', row).eq(6).addClass('text-right').html(html);
            },
            "order": [[1, 'desc']],
        });

        $('#select-all').click(function () {
            var checkBoxes = $('.checkbox');
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });
    });

    function addRecord(newsletter_id) {
        if (newsletter_id) {
            data = 'newsletter_id=' + newsletter_id;
        } else {
            if ($('.checkbox:checked').length === 0) {
                alert('Tidak ada data yang dipilih!');
                return false;
            }
            data = $('#form-checks').serialize();
        }


        $.ajax({
            url: '<?=admin_url('newsletter/group/addmember/' . $newsletter_group_id)?>',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (json) {
                if (json['success']) {
                    swal("Ditambahakan!", json['success'], "success");
                } else if (json['error']) {
                    swal("Error!", json['error'], "error");
                } else if (json['redirect']) {
                    window.location = json['redirect'];
                }
                refreshTable();
            }
        });

    }

    function actionSuccess(json) {
        if (json['success']) {
            $('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
        } else if (json['error']) {
            $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
        } else if (json['redirect']) {
            window.location = json['redirect'];
        }
        refreshTable();
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
        $('#select-all').prop('checked', false);
    }

</script>