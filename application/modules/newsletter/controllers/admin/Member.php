<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('currency');
        $this->load->library('form_validation');

        $this->load->helper('form');
        $this->load->helper('language');

        $this->load->model('newsletter_model');
        $this->load->model('order/order_model');
        $this->load->model('system/order_status_model');

        $this->form_validation->CI =& $this;
    }

    /**
     * Orders list
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('n.newsletter_id, n.email,  n.gender, n.telephone, n.name, l.name as city', false)
                ->where('l.type <>', 'Provinsi')
                ->where('l.type <>', 'Kecamatan')
                ->join('location l', 'l.city_id = n.city_id', 'left')
                ->from('newsletter n');


            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');

            $this->load
                ->title('Data Subscriber')
                ->view('admin/newsletter', $data);
        }
    }


    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        $newsletter_id = $this->input->post('newsletter_id');

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->newsletter_model->delete($newsletter_id);
            $json['success'] = 'Berhasil menghapus Subscriber!';
        }

        $this->output->set_output(json_encode($json));
    }


    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->title('Tambah Subscriber');

        $this->form();
    }

    public function edit($newsletter_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->title('Edit Subscriber');

        $this->form($newsletter_id);
    }

    public function form($newsletter_id = null)
    {
        $this->load->model('location/location_model');

        if ($newsletter_id) {
            foreach ($this->newsletter_model->get($newsletter_id) as $key => $value) {
                $data[$key] = $value;
            }
        } else {
            foreach ($this->newsletter_model->list_fields() as $field) {
                $data[$field] = '';
            }
        }

        $data['active'] = 1;
        $data['provinces'] = $this->location_model->get_provinces();
        $data['action'] = admin_url('newsletter/member/validate');

        $this->load->view('admin/newsletter_form', $data);
    }

    public function validate()
    {
        check_ajax();

        $json = array();

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required')
            ->set_rules('telephone', 'No. Telephone', 'trim|required|callback__check_telephone')
            ->set_rules('email', 'Email', 'trim|required|valid_email|callback__check_email')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['errors'][$field] = $error;
            }
        } else {
            $newsletter_id = $this->input->post('newsletter_id');
            $data = $this->input->post(null, true);
            $data['active'] = (bool)$this->input->post('active');

            if ($newsletter_id) {
                $data['date_added'] = date('Y-m-d H:i:s');
                $this->newsletter_model->update($newsletter_id, $data);
                $success = 'Data Subscriber berhasil diperbarui';
            } else {
                $data['date_modified'] = date('Y-m-d H:i:s');
                $newsletter_id = $this->newsletter_model->insert($data);
                $success = 'Berhasil menambahkan Subscriber baru';
            }

            $json['success'] = $success;
        }

        $this->output->set_output(json_encode($json));
    }


    public function _check_email($email)
    {
        if ($this->newsletter_model->check_email($email, $this->input->post('newsletter_id'))) {
            $this->form_validation->set_message('_check_email', 'Email ini tidak tersedia!');
            return false;
        }

        return true;
    }

    public function _check_telephone($telephone)
    {
        if ($this->newsletter_model->check_telephone($telephone, $this->input->post('newsletter_id'))) {
            $this->form_validation->set_message('_check_telephone', 'No. telepon ini sudah ada yang menggunakan!');
            return false;
        }

        return true;
    }


    public function xls_export()
    {
        if (!$this->admin_auth->has_permission('index')) {
            show_401($this->uri->uri_string());
        }

        $this->load->helper('format');

        $results = $this->db
            ->order_by('name', 'asc')
            ->get('newsletter')
            ->result_array();

        $format = $this->input->get('format');

        $this->load->library('PHPExcel');
        $this->load->library('weight');
        $this->load->library('length');

        require_once(APPPATH . 'libraries/PHPExcel.php');
        require_once(APPPATH . 'libraries/PHPExcel/IOFactory.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Title');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Nama');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Jenis Kelamin');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'Tanggal Lahir');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'ID Kota');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'Email');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'Telephone');

        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);

        $row = 2;

        foreach ($results as $result) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $result['newsletter_id']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $result['title']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $result['name']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $result['gender']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $result['birthday']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $result['city_id']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $result['email']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $result['telephone']);

            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="subscriber-export-' . date('dmYHis') . '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save('php://output');
    }

    /**
     * Import data from XLS
     *
     * @access public
     * @return void
     */
    public function xls_import()
    {
        $json = array();

        if (!$this->admin_auth->has_permission('edit')) {
            $json['error'] = lang('admin_error_edit');
        }

        if (!isset($json['error'])) {
            $cache_path = APPPATH . 'cache/xls/';
            $xls_path = FCPATH . $cache_path;

            if (!file_exists($cache_path)) {
                @mkdir($cache_path, 0777);
            }

            $config['upload_path'] = $cache_path;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size'] = '2048';
            $config['overwrite'] = true;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $json['error'] = $this->upload->display_errors('', '');
            } else {
                $file_data = $this->upload->data();

                if (file_exists($file_data['file_path'] . $file_data['raw_name'] . $file_data['file_ext'])) {
                    $this->load->library('PHPExcel');

                    require_once(APPPATH . 'libraries/PHPExcel.php');
                    require_once(APPPATH . 'libraries/PHPExcel/IOFactory.php');

                    $objPHPExcel = PHPExcel_IOFactory::load($file_data['file_path'] . $file_data['raw_name'] . $file_data['file_ext']);

                    $products = array();

                    foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
                        $highestRow = $worksheet->getHighestRow();

                        for ($row = 2; $row <= $highestRow; $row++) {
                            $products[] = array(
                                'newsletter_id' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
                                'title' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
                                'name' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                                'gender' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                                'birthday' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                                'city_id' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
                                'email' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                                'telephone' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                            );
                        }
                    }

                    $this->newsletter_model->import($products);

                    unlink($file_data['file_path'] . $file_data['raw_name'] . $file_data['file_ext']);

                    $json['success'] = 'Proses import telah berhasil';
                } else {
                    system('rm -rf ' . escapeshellarg($xls_path), $return);

                    $json['error'] = 'Proses import gagal!';
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }


    public function autocomplete()
    {
        $json = array();

        if ($this->input->get('name')) {
            $params = array(
                'name' => $this->input->get('name'),
                'start' => 0,
                'limit' => 20
            );

            foreach ($this->newsletter_model->get_newsletter_autocomplete($params) as $newsletter) {
                $json[$newsletter['newsletter_id']] = array(
                    'newsletter_id' => (int)$newsletter['newsletter_id'],
                    'name' => $newsletter['newsletter_id'] . ' - ' . strip_tags(html_entity_decode($newsletter['name'], ENT_QUOTES, 'UTF-8')),
                    'email' => $newsletter['email'],
                    'telephone' => $newsletter['telephone']
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->output->set_output(json_encode($json));
    }
}