<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('currency');
        $this->load->library('form_validation');

        $this->load->helper('form');
        $this->load->helper('language');

        $this->load->model('order/order_model');
        $this->load->model('system/order_status_model');
        $this->load->model('newsletter_template_model');

        $this->form_validation->CI =& $this;
    }

    /**
     * Orders list
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('*', false)
                ->from('newsletter_template');


            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');

            $this->load
                ->title('Newsletter Template')
                ->view('admin/template', $data);
        }
    }


    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        $newsletter_template_id = $this->input->post('newsletter_template_id');

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->newsletter_template_model->delete($newsletter_template_id);
            $json['success'] = 'Berhasil menghapus template!';
        }

        $this->output->set_output(json_encode($json));
    }


    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->title('Tambah template');

        $this->form();
    }

    public function edit($newsletter_template_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->title('Edit template');

        $this->form($newsletter_template_id);
    }

    public function form($newsletter_template_id = null)
    {

        if ($newsletter_template_id) {
            foreach ($this->newsletter_template_model->get($newsletter_template_id) as $key => $value) {
                $data[$key] = $value;
            }
        } else {
            foreach ($this->newsletter_template_model->list_fields() as $field) {
                $data[$field] = '';
            }
        }

        $data['action'] = admin_url('newsletter/template/validate');


        $this->load->view('admin/template_form', $data);
    }

    public function validate()
    {
        check_ajax();

        $json = array();

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['errors'][$field] = $error;
            }
        } else {
            $newsletter_template_id = $this->input->post('newsletter_template_id');
            $data = $this->input->post(null, true);

            if ($newsletter_template_id) {
                $this->newsletter_template_model->update($newsletter_template_id, $data);
                $success = 'Template berhasil diperbarui';
            } else {
                $newsletter_template_id = $this->newsletter_template_model->insert($data);
                $success = 'Berhasil menambahkan template baru';
            }

            $json['success'] = $success;
        }

        $this->output->set_output(json_encode($json));
    }


    public function setactive($newsletter_template_id)
    {
        $this->newsletter_template_model->setactive($newsletter_template_id);

        $this->session->set_flashdata('success', 'Template telah di set');
        $this->index();
    }

}