<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Create extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('currency');
        $this->load->library('form_validation');

        $this->load->helper('form');
        $this->load->helper('language');

        $this->load->model('newsletter_model');
        $this->load->model('newsletter_group_model');
        $this->load->model('newsletter_group_member_model');
        $this->load->model('newsletter_template_model');


        $this->form_validation->CI =& $this;
    }


    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $json = array();

            $this->form_validation->set_rules('subject', 'Judul', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('body', 'Konten', 'trim|required|min_length[64]');

            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->get_errors() as $field => $error) {
                    $json['errors'][$field] = $error;
                }
            } else {
                $post = $this->input->post(null, true);


                $customers = array();

                switch ($post['to']) {
                    case 'newsletter':
                        $newsletters = $this->newsletter_model->get_all_by(array('active' => 1));
                        break;

                    case 'subcriber_all':
                        $newsletters = $this->newsletter_model->get_all();
                        break;

                    case 'subcriber_group':
                        $newsletters = $this->newsletter_model->getgroup($this->input->post('newsletter_group_id'));
                        break;

                    case 'subcriber':
                        $post['newsletter'] = $this->input->post('newsletter') ? $this->input->post('newsletter') : array();
                        foreach ($post['newsletter'] as $newsletter_id) {
                            $newsletters[] = $this->newsletter_model->get($newsletter_id);
                        }
                        break;
                }

                if ($newsletters) {
                    $this->load->library('email');
                    $this->email->initialize();
                    $this->email->from($this->config->item('smtp_email'), $this->config->item('company'));

                    $to = array();

                    foreach ($newsletters as $newsletter) {
                        $to = $newsletter['email'];
                    }

                    $this->email->to($to);

                    $this->email->subject(html_entity_decode($this->input->post('subject'), ENT_QUOTES, 'UTF-8'));
                    $this->email->message(html_entity_decode($this->input->post('body'), ENT_QUOTES, 'UTF-8'));

                    if ($this->email->send()) {
                        $success = 'Email berhasil dikirim!';
                        $this->session->set_flashdata('success', $success);
                        $json['success'] = $success;
                    } else {
                        exit($this->email->print_debugger());
                    }
                } else {
                    $json['errors']['to'] = 'Tidak ada data subscriber yang terpilih untuk kategori penerima yang ditentukan!';
                }
            }

            $this->output->set_output(json_encode($json));
        } else {
            $data['action'] = admin_url('newsletter/create');

            $data['subcriber_groups'] = $this->newsletter_group_model->get_all();
            $data['template'] = $this->newsletter_template_model->getactive();


            $this->load
                ->title('Buat Newsletter')
                ->js('/assets/js/bootstrap-typeahead.min.js')
                ->view('admin/create', $data);
        }
    }


    public function savetemplate()
    {
        $post = $this->input->post('content');

        $this->newsletter_template_model->updatetemplate($post);

        $success = 'Template berhasil di simpan';
        $this->session->set_flashdata('success', $success);
        $json['success'] = $success;

        $this->output->set_output(json_encode($json));
    }

}