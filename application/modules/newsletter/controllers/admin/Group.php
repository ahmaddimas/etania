<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('newsletter_model');
        $this->load->model('newsletter_group_model');
        $this->load->model('newsletter_group_member_model');

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('format');
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('newsletter_group_id, name, description', false)
                ->from('newsletter_group');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');

            $this->load
                ->title('Group Filter')
                ->view('admin/group', $data);
        }
    }

    /**
     * Create new category
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_create'))));
        }

        $this->load->title('Tambah Group Filter');

        $this->form();
    }

    /**
     * Edit existing category
     *
     * @access public
     * @param int $newsletter_group_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('auth_error_edit'))));
        }

        $this->load->title('Edit Group Filter');

        $this->form($this->input->get('newsletter_group_id'));
    }

    /**
     * Load category form
     *
     * @access private
     * @param int $newsletter_group_id
     * @return void
     */
    private function form($newsletter_group_id = null)
    {
        $data['action'] = admin_url('newsletter/group/validate');
        $data['newsletter_group_id'] = null;
        $data['name'] = '';
        $data['description'] = '';

        if ($newsletter_group = $this->newsletter_group_model->get($newsletter_group_id)) {
            $data['newsletter_group_id'] = (int)$newsletter_group['newsletter_group_id'];
            $data['name'] = $newsletter_group['name'];
            $data['description'] = $newsletter_group['description'];
        }

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/group_form', $data, true, true)
        )));
    }

    /**
     * Validate category form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $newsletter_group_id = $this->input->post('newsletter_group_id');

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $post = $this->input->post();

            if ($newsletter_group_id) {
                $this->newsletter_group_model->update($newsletter_group_id, $post);

                $json['success'] = lang('success_update');
            } else {
                if ($this->newsletter_group_model->insert($post)) {
                    $json['success'] = lang('success_create');
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $newsletter_group_id = $this->input->post('newsletter_group_id');

        if (!$newsletter_group_id) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {

            $this->newsletter_group_member_model->deletegroup($newsletter_group_id);
            $this->newsletter_group_model->delete($newsletter_group_id);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }


    public function detail($newsletter_group_id)
    {
        $ng = $this->newsletter_group_model->get($newsletter_group_id);
        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('ngm.newsletter_group_member_id, n.email,  n.gender, n.telephone, n.name, l.name as city', false)
                ->where('ng.newsletter_group_id', $newsletter_group_id)
                ->where('l.type <>', 'Provinsi')
                ->where('l.type <>', 'Kecamatan')
                ->join('newsletter n', 'n.newsletter_id = ngm.newsletter_id', 'left')
                ->join('location l', 'l.city_id = n.city_id', 'left')
                ->join('newsletter_group ng', 'ng.newsletter_group_id = ngm.newsletter_group_id', 'left')
                ->from('newsletter_group_member ngm');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');
            $data['newsletter_group_id'] = $newsletter_group_id;

            $this->load
                ->title('Member group ' . $ng['name'])
                ->view('admin/groupdetail', $data);
        }

    }


    public function add($newsletter_group_id)
    {
        $ng = $this->newsletter_group_model->get($newsletter_group_id);
        $ngmc = $this->newsletter_group_member_model->get_by('newsletter_group_id', $newsletter_group_id);
        $newsletter_id = $this->newsletter_group_member_model->getwherenot($newsletter_group_id);

        if (count($newsletter_id) == 0) {
            $newsletter_id = 0;
        }

        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            if ($ngmc == 0) {
                $this->datatables
                    ->select('ngm.newsletter_group_member_id, n.newsletter_id, n.gender, n.email, n.telephone, n.name, l.name as city', false)
                    ->where('l.type <>', 'Provinsi')
                    ->where('l.type <>', 'Kecamatan')
                    ->join('location l', 'l.city_id = n.city_id', 'left')
                    ->join('newsletter_group_member ngm', 'ngm.newsletter_id = n.newsletter_id', 'left')
                    ->from('newsletter n');
            } else {
                $this->datatables
                    ->select('ngm.newsletter_group_member_id, n.newsletter_id, n.gender, n.email, n.telephone, n.name, l.name as city', false)
                    ->where_in('n.newsletter_id', $newsletter_id)
                    ->where('l.type <>', 'Provinsi')
                    ->where('l.type <>', 'Kecamatan')
                    ->join('location l', 'l.city_id = n.city_id', 'left')
                    ->join('newsletter_group_member ngm', 'ngm.newsletter_id = n.newsletter_id', 'left')
                    ->from('newsletter n');
            }

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');
            $data['newsletter_group_id'] = $newsletter_group_id;

            $this->load
                ->title('Member group ' . $ng['name'])
                ->view('admin/groupadd', $data);
        }

    }


    public function addmember($newsletter_group_id)
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $newsletter_id = $this->input->post('newsletter_id');

        if (!$newsletter_id) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {

            $data = array();
            if (is_array($newsletter_id)) {
                foreach ($newsletter_id as $key => $val) {
                    $data['newsletter_group_id'] = $newsletter_group_id;
                    $data['newsletter_id'] = $val;

                    $this->newsletter_group_member_model->insert($data);
                }
            } else {
                $data['newsletter_group_id'] = $newsletter_group_id;
                $data['newsletter_id'] = $newsletter_id;

                $this->newsletter_group_member_model->insert($data);
            }

            $json['success'] = "Berhasil ditambahkan";
        }

        $this->output->set_output(json_encode($json));
    }


    public function deletemember($newsletter_group_id)
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $newsletter_group_member_id = $this->input->post('newsletter_group_member_id');

        if (!$newsletter_group_member_id) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {

            $this->newsletter_group_member_model->delete($newsletter_group_member_id);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
} 