<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Midtrans_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get method data
     *
     * @access public
     * @param array $address
     * @param float $total
     * @return array
     */
    public function get_method($address = array(), $total)
    {
        $this->load->library('currency');
        $this->load->library('shopping_cart');
        $shipping_method = $this->session->userdata('shipping_method');


        $payment_min = $this->config->item('total', 'payment_midtrans');
        $fees = explode('&', $this->config->item('fee', 'payment_midtrans'));

        $totals = $this->shopping_cart->get_total() + $shipping_method['cost'];
        $fee = ($totals * $fees[0] / 100) + $fees[1];

        if ($payment_min > 0 && $payment_min > $total) {
            $status = false;
        } else {
            $status = true;
        }


        $method_data = array();

        if ($status) {
            $method_data = array(
                'code' => 'midtrans',
                'title' => $this->config->item('display_name', 'payment_midtrans'),
                'logo' => DIR_IMAGE . $this->config->item('logo', 'payment_midtrans'),
                'instruction' => $this->config->item('instruction', 'payment_midtrans'),
                'sort_order' => $this->config->item('sort_order', 'payment_midtrans'),
                'fee' => $fees[0] . '%+' . $this->currency->format($fees[1]),
                'fee_nominal' => $fee,
                'note' => $this->config->item('note', 'payment_midtrans'),
            );
        }


        return $method_data;
    }

    /**
     * Install
     *
     * @access public
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * Uninstall
     *
     * @access public
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }
}