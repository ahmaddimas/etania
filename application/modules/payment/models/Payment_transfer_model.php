<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_transfer_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get method data
     *
     * @access public
     * @param array $address
     * @param float $total
     * @return array
     */
    public function get_method($address = array(), $total)
    {
        $payment_transfer = $this->db
            ->select('payment_transfer_id, name, logo, display_name, bank_name, rekening_number, rekening_name, instruction, success_status_id, total, sort_order, active')
            ->where('active', 1)
            ->order_by('sort_order')
            ->get('payment_transfer')
            ->result_array();

        $method_data = array();
        foreach ($payment_transfer as $py) {
            if ($py['total'] > 0 && $py['total'] > $total) {
                $status = false;
            } else {
                $status = true;
            }


            if ($status) {
                $method_data[] = array(
                    'payment_transfer_id' => $py['payment_transfer_id'],
                    'type' => 'payment_transfer',
                    'code' => $py['name'] . ' ' . $py['payment_transfer_id'],
                    'bank_name' => $py['bank_name'],
                    'title' => $py['display_name'],
                    'logo' => DIR_IMAGE . $py['logo'],
                    'rekening_number' => $py['rekening_number'],
                    'rekening_name' => $py['rekening_name'],
                    'instruction' => $py['instruction'],
                    'sort_order' => $py['sort_order'],
                    'fee' => 0
                );
            }
        }

        return $method_data;
    }


    /**
     * Instruction
     *
     * @access public
     * @return bool
     */
    public function Instruction($payment_transfer_id)
    {
        $payment_transfer = $this->get_by('payment_transfer_id', $payment_transfer_id);


        return $payment_transfer['instruction'];
    }


    /**
     * Status_order
     *
     * @access public
     * @return bool
     */
    public function Status_order($payment_transfer_id)
    {
        $payment_transfer = $this->get_by('payment_transfer_id', $payment_transfer_id);


        return $payment_transfer['success_status_id'];
    }


    /**
     * Install
     *
     * @access public
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * Uninstall
     *
     * @access public
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }
}