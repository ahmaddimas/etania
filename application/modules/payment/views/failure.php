<div class="body-content">
    <div class="container">
        <div class="terms-conditions-page">
            <div class="row">
                <div class="col-md-12 terms-conditions">
                    <h2 class="heading-title">Mohon maaf</h2>
                    <div>
                        <p>Untuk saat ini kami belum dapat memproses pembayaran anda. Silakan hubungi customer service
                            kami</p>
                        <p>Terimakasih</p>
                        <hr>
                        <button class="btn btn-primary" onclick="window.location = '<?= site_url() ?>'">Kembali ke
                            depan
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-top:30px;" class="clearfix"></div>
    </div>
</div>