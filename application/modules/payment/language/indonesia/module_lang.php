<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['payment/bca'] = 'Transfer Bank BCA';
$lang['payment/mandiri'] = 'Transfer Bank Mandiri';
$lang['payment/midtrans'] = 'Midtrans';
