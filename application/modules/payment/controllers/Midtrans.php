<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Midtrans extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('shopping_cart');
        $this->load->library('midtrans_lib');

        Midtrans_lib::config(array(
            'server_key' => $this->config->item('server_key', 'payment_midtrans'),
            'production' => (bool)$this->config->item('production', 'payment_midtrans'),
        ));
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $data['action'] = site_url('payment/midtrans/confirm');
        $data['client_key'] = $this->config->item('client_key', 'payment_midtrans');

        return $this->load->layout(null)->view('midtrans', $data, true);
    }

    /**
     * Get token
     *
     * @access public
     * @return void
     */
    public function token()
    {
        $this->load->model('order/order_model');

        if ($order = $this->order_model->get_order($this->session->userdata('order_id'))) {
            $order_products = $this->order_model->get_order_products($order['order_id']);
            $order_totals = $this->order_model->get_order_totals($order['order_id']);
            $item_details = array();

            $this->load->helper('format');

            foreach ($order_products as $order_product) {
                $item_details[] = array(
                    'id' => (int)$order_product['product_id'],
                    'price' => (float)$order_product['price'],
                    'quantity' => (int)$order_product['quantity'],
                    'name' => cut_text($order_product['name'], 16, ''),
                );
            }

            foreach ($order_totals as $order_total) {
                if ($order_total['code'] != 'total' && $order_total['code'] != 'subtotal') {
                    $item_details[] = array(
                        'id' => $order_total['code'],
                        'price' => (float)$order_total['value'],
                        'quantity' => 1,
                        'name' => $order_total['title'],
                    );
                } elseif ($order_total['code'] == 'total') {
                    $transaction_details = array(
                        'order_id' => $order['order_id'],
                        'gross_amount' => (float)$order_total['value']
                    );
                }
            }

            $this->load->model('customer/address_model');

            $address = $this->address_model->get_address($this->session->userdata('shipping_address_id'));

            if ($address) {
                $customer = $this->customer_model->get($this->customer->customer_id());
                $name = explode(' ', $customer['name']);

                $billing_address = array(
                    'first_name' => isset($name[0]) ? $name[0] : '',
                    'last_name' => isset($name[1]) ? $name[1] : '',
                    'address' => $address['address'],
                    'city' => $address['city'],
                    'postal_code' => $address['postcode'],
                    'phone' => $customer['telephone'],
                    'country_code' => 'IDN'
                );

                $shipping_address = array(
                    'first_name' => isset($name[0]) ? $name[0] : '',
                    'last_name' => isset($name[1]) ? $name[1] : '',
                    'address' => $address['address'],
                    'city' => $address['city'],
                    'postal_code' => $address['postcode'],
                    'phone' => $customer['telephone'],
                    'country_code' => 'IDN'
                );

                $customer_details = array(
                    'first_name' => isset($name[0]) ? $name[0] : '',
                    'last_name' => isset($name[1]) ? $name[1] : '',
                    'email' => $customer['email'],
                    'phone' => $customer['telephone'],
                    'billing_address' => $billing_address,
                    'shipping_address' => $shipping_address
                );

                $transaction = array(
                    'transaction_details' => $transaction_details,
                    'customer_details' => $customer_details,
                    'item_details' => $item_details,
                    //'enabled_payments' => array('credit_card')
                );

                $snapToken = Midtrans_lib::getSnapToken($transaction);

                $this->output->set_output($snapToken);
            }
        }
    }

    /**
     * Confirm
     *
     * @access public
     * @return void
     */
    public function confirm()
    {
        if ($this->input->post('result_data')) {
            $response = json_decode($this->input->post('result_data'));
        } elseif ($this->input->post('response')) {
            $response = json_decode($this->input->post('response'));
        } elseif ($this->input->get('id')) {
            $response = Midtrans_lib::status($this->input->get('id'));
        } else {
            $response = false;
        }

        if ($response) {
            $data = array();

            $this->load->model('order/order_model');

            $transaction_status = $response->transaction_status;
            $payment_type = $response->payment_type;
            $channel = array('bank_transfer', 'gopay', 'bca_klikbca', 'echannel', 'cstore', 'xl_tunai');

            if ($payment_type == 'bca_klikpay') {
                if ($response->transaction_status == 'settlement') {
                    $data['data'] = array(
                        'payment_type' => 'bca_klikpay',
                        'payment_method' => 'BCA KlikPay',
                        'payment_status' => 'Berhasil'
                    );

                    $this->order_model->confirm(
                        $response->order_id,
                        $this->config->item('success_status_id', 'payment_midtrans'),
                        'Pembayaran menggunakan BCA KlikPay telah berhasil',
                        true
                    );

                    $this->sendfile($response->order_id);

                    redirect('checkout/success');
                } else {
                    redirect('payment/midtrans/failure');
                }
            } elseif ($transaction_status == 'capture' || $transaction_status == 'settlement') {
                $this->order_model->confirm(
                    $response->order_id,
                    $this->config->item('success_status_id', 'payment_midtrans'),
                    'Pembayaran menggunakan Kartu Kredit telah berhasil',
                    true
                );

                $this->sendfile($response->order_id);

                redirect('checkout/success');
            } elseif ($transaction_status == 'deny') {
                redirect('payment/midtrans/failure');
            } elseif ($transaction_status == 'pending' && in_array($payment_type, $channel)) {
                // $check = Midtrans_lib::status($response->order_id);
                switch ($payment_type) {
                    case 'bank_transfer':
                        if ($response->transaction_status == 'settlement') {
                            $this->order_model->confirm(
                                $response->order_id,
                                $this->config->item('process_status_id', 'payment_midtrans'),
                                'Pembayaran melalui Transfer Bank sedang diproses',
                                true
                            );

                            $this->sendfile($response->order_id);
                            redirect('checkout/success');
                        }

                        if (isset($response->va_numbers[0]->bank)) {
                            $data['data'] = array(
                                'payment_type' => $payment_type,
                                'payment_method' => strtoupper($response->va_numbers[0]->bank == 'bca') . ' Virtual Whatsappaccount',
                                'instruction' => $response->pdf_url,
                                'payment_code' => $response->va_numbers[0]->va_number,
                            );
                        } else {
                            $data['data'] = array(
                                'payment_type' => $payment_type,
                                'payment_method' => 'Permata Virtual Whatsappaccount',
                                'instruction' => $response->pdf_url,
                                'payment_code' => $response->permata_va_number,
                            );
                        }

                        $this->order_model->confirm(
                            $response->order_id,
                            $this->config->item('pending_status_id', 'payment_midtrans'),
                            'Pembayaran menggunakan Transfer Bank menunggu transfer',
                            true
                        );
                        break;

                    case 'echannel':
                        if ($response->transaction_status == 'settlement') {
                            $this->order_model->confirm(
                                $response->order_id,
                                $this->config->item('process_status_id', 'payment_midtrans'),
                                'Pembayaran menggunakan Mandiri Bill Payment berhasil',
                                true
                            );

                            $this->sendfile($response->order_id);


                            redirect('checkout/success');
                        }

                        $data['data'] = array(
                            'payment_type' => $payment_type,
                            'payment_method' => 'Mandiri Bill Payment',
                            'instruction' => $response->pdf_url,
                            'company_code' => $response->biller_code,
                            'payment_code' => $response->bill_key,
                        );

                        $this->order_model->confirm(
                            $response->order_id,
                            $this->config->item('pending_status_id', 'payment_midtrans'),
                            'Pembayaran menggunakan Mandiri Bill Payment pending',
                            true
                        );
                        break;

                    case 'cstore':
                        if ($response->transaction_status == 'settlement') {
                            $this->order_model->confirm(
                                $response->order_id,
                                $this->config->item('success_status_id', 'payment_midtrans'),
                                'Pembayaran melalui Indomaret berhasil',
                                true
                            );

                            $this->sendfile($response->order_id);


                            redirect('checkout/success');
                        }

                        $data['data'] = array(
                            'payment_type' => $payment_type,
                            'payment_method' => 'Indomaret',
                            'store' => $response->store,
                            'payment_code' => $response->payment_code,
                            //'expire' => $response->indomaret_expire_time
                        );

                        $this->order_model->confirm(
                            $response->order_id,
                            $this->config->item('pending_status_id', 'payment_midtrans'),
                            'Pembayaran melalui Indomaret pending',
                            true
                        );
                        break;

                    case 'xl_tunai':
                        if ($response->transaction_status == 'settlement') {
                            $this->order_model->confirm(
                                $response->order_id,
                                $this->config->item('success_status_id', 'payment_midtrans'),
                                'Pembayaran menggunakan XL Tunai berhasil',
                                true
                            );
                            $this->sendfile($response->order_id);

                            redirect('checkout/success');
                        } else {
                            $instruction = '<p><strong>Silahkan melakukan pembayaran melalui XL Tunai</strong></p>';
                            $instruction .= '<p>Jika sudah melakukan pembayaran, harap tunggun proses verifikasi pembayaran dari kami</p>';


                            $data['data'] = array(
                                'payment_type' => $payment_type,
                                'payment_method' => 'XL Tunai',
                                'instruction' => $instruction,
                                'expire' => $response->transaction_time
                            );

                            $this->order_model->confirm(
                                $response->order_id,
                                $this->config->item('pending_status_id', 'payment_midtrans'),
                                'Pembayaran menggunakan XL Tunai pending',
                                true
                            );
                        }
                        break;


                    case 'gopay':
                        $instruction = '<p><strong>Silahkan melakukan pembayaran melalui aplikasi GoPay</strong></p>';
                        $instruction .= '<p>Jika sudah melakukan pembayaran, harap tunggun proses verifikasi pembayaran dari kami</p>';


                        $data['data'] = array(
                            'payment_type' => $payment_type,
                            'payment_method' => 'GoPay',
                            'instruction' => $instruction,
                            'expire' => $response->transaction_time
                        );


                        $this->order_model->confirm(
                            $response->order_id,
                            $this->config->item('pending_status_id', 'payment_midtrans'),
                            'Pembayaran menggunakan GoPay pending',
                            true
                        );
                        break;


                    case 'bca_klikbca':
                        $instruction = '<p><strong>Silahkan melakukan pembayaran melalui BCA Klick BCA</strong></p>';
                        $instruction .= '<p>Jika sudah melakukan pembayaran, harap tunggun proses verifikasi pembayaran dari kami</p>';


                        $data['data'] = array(
                            'payment_type' => $payment_type,
                            'payment_method' => 'BCA Klik BCA',
                            'instruction' => $instruction,
                            'expire' => $response->transaction_time
                        );


                        $this->order_model->confirm(
                            $response->order_id,
                            $this->config->item('pending_status_id', 'payment_midtrans'),
                            'Pembayaran menggunakan BCA Klik BCA pending',
                            true
                        );
                        break;
                }

                $this->shopping_cart->clear();

                $this->session->unset_userdata('shipping_method');
                $this->session->unset_userdata('shipping_methods');
                $this->session->unset_userdata('payment_method');
                $this->session->unset_userdata('payment_methods');
                $this->session->unset_userdata('comment');
                $this->session->unset_userdata('order_id');
                $this->session->unset_userdata('coupon');
                $this->session->unset_userdata('totals');

                $this->load->title('Menunggu Pembayaran');
                $this->load->view('midtrans_instruction', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    /**
     * Accept notification
     *
     * @access public
     * @return void
     */
    public function notification()
    {
// 		if ($this->input->post('result_data')) {
// 			$result = json_decode($this->input->post('result_data'));
// 		} elseif ($this->input->post('response')) {
// 			$result = json_decode($this->input->post('response'));
// 		} else {
// 			$result = false;
// 		}

        $result = json_decode(json_decode(file_get_contents('php://input')), true);


        if ($result) {
// 			$notif = Midtrans_lib::status($result['order_id']);

            $transaction = $result['transaction_status'];
            $type = $result['payment_type'];
            $order_id = (int)$result['order_id'];
            $fraud = $result['fraud_status'];

            // tahap berhasil tapi belum diproses oleh bank
            if ($transaction == 'capture') {
                if ($type == 'credit_card') {
                    if ($fraud == 'challenge') {
                        $order_status_id = $this->config->item('order_status_id');
                        $comment = 'Status pembayaran : CHALLENGED BY FDS';
                    } elseif ($fraud == 'accept') {
                        $order_status_id = $this->config->item('order_status_id');
                        $comment = 'Status pembayaran : ACCEPT BY FDS';
                    }
                }

                // tahap berhasil dan sudah diproses/verifikasi bank
            } else if ($transaction == 'settlement') {
                $order_status_id = $this->config->item('order_paid_status_id');
                $comment = 'Status pembayaran : LUNAS';
                // tahap menunggu proses
            } else if ($transaction == 'pending') {
                $order_status_id = $this->config->item('order_status_id');
                $comment = 'Status pembayaran : PENDING';
                // tahap ditolak
            } else if ($transaction == 'deny') {
                $order_status_id = $this->config->item('order_cancel_status_id');
                $comment = 'Status pembayaran : DITOLAK';
                // deadline pembayaran telah lewat
            } else if ($transaction == 'expire') {
                $order_status_id = $this->config->item('order_status_id');
                $comment = 'Status pembayaran : KADALUARSA';
            } else {
                $order_status_id = false;
            }

            if ($order_status_id) {
                $this->load->model('order/order_history_model');

                $order_history = array(
                    'order_id' => (int)$order_id,
                    'order_status_id' => (int)$order_status_id,
                    'notify' => 1,
                    'comment' => $comment
                );

                $this->order_history_model->insert($order_history);

                $update['order_status_id'] = $order_status_id;

                // jika diset lunas, maka set tanggal pelunasan
                if ($order_status_id == $this->config->item('order_paid_status_id')) {
                    $update['date_paid'] = date('Y-m-d H:i:s', time());
                }

                // jika diset cancel, maka status pembayaran lunas akan dicabut
                if ($order_status_id == $this->config->item('order_cancel_status_id')) {
                    $update['date_paid'] = '0000-00-00 00:00:00';
                }

                $this->load->model('order/order_model');
                $this->order_model->update($order_id, $update);
            }

            $this->output->set_output('Notification received');
        } else {
            $this->output->set_output('Not found result data!');
        }
    }

    /**
     * Failure notification
     *
     * @access public
     * @return void
     */
    public function failure()
    {
        $this->shopping_cart->clear();

        $this->session->unset_userdata('shipping_method');
        $this->session->unset_userdata('shipping_methods');
        $this->session->unset_userdata('payment_method');
        $this->session->unset_userdata('payment_methods');
        $this->session->unset_userdata('comment');
        $this->session->unset_userdata('order_id');
        $this->session->unset_userdata('coupon');
        $this->session->unset_userdata('totals');

        $this->load->view('failure');
    }


    public function midtrans_instruction()
    {

        $instruction = '<p><strong>Silahkan selesaikan proses pembayaran</strong></p>';
        $instruction .= '<p>Jika sudah melakukan pembayaran, harap tunggun proses verifikasi pembayaran dari kami</p>';


        $data['data'] = array(
            'payment_type' => 'unfinished',
            'payment_method' => 'Pembayaran Belum Selesai',
            'instruction' => $instruction,
        );


        $this->shopping_cart->clear();

        $this->session->unset_userdata('shipping_method');
        $this->session->unset_userdata('shipping_methods');
        $this->session->unset_userdata('payment_method');
        $this->session->unset_userdata('payment_methods');
        $this->session->unset_userdata('comment');
        $this->session->unset_userdata('order_id');
        $this->session->unset_userdata('coupon');
        $this->session->unset_userdata('totals');


        $this->load->title('Menunggu Pembayaran');
        $this->load->view('midtrans_instruction', $data);
    }


    public function sendfile($order_id)
    {
        $addon_digital = $this->config->item('addon_digital');
        if (!empty($addon_digital['active'])) {
            $this->load->module('digital');
            $this->digital->sendfile($order_id);
        }
    }


}