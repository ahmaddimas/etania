<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_transfer extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_transfer_model');
    }

    /**
     * Index
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $payment_method = $this->session->userdata('payment_method');

        $data['continue'] = site_url('checkout/success');
        $data['instruction'] = $this->payment_transfer_model->Instruction($payment_method['payment_transfer_id']);

        $this->load->helper('form');

        return $this->load->layout(null)->view('payment_transfer', $data, true);
    }

    /**
     * Confirm
     *
     * @access public
     * @return void
     */
    public function confirm()
    {
        $payment_method = $this->session->userdata('payment_method');
        $this->load->model('order/order_model');

        $json['success'] = true;

        if ((bool)$this->input->post('confirm')) {
            $comment = $this->payment_transfer_model->Instruction($payment_method['payment_transfer_id']);
            $status = $this->payment_transfer_model->Status_order($payment_method['payment_transfer_id']);

            if ($this->order_model->confirm($this->session->userdata('order_id'), $status, $comment, true)) {
                $json['success'] = true;
            }
        } else {
            $json['error'] = true;
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Confirmation
     *
     * @access public
     * @return void
     */
    public function confirmation()
    {
        return true;
    }
}