<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_transfer extends Admin_Controller
{
    private $manifest = array();

    /**
     * Construction
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->config('payment/payment_transfer');

        $this->manifest = $this->config->item('manifest');
        $this->load->library('currency');

        $this->load->library('form_validation');
        $this->load->helper('form');
    }

    /**
     * Module setting
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->load->helper('form');
        $this->load->model('system/setting_model');
        $this->load->model('system/order_status_model');

        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('payment_transfer_id, name, logo, display_name, bank_name, rekening_number, rekening_name, instruction, success_status_id, total, sort_order, active')
                ->from('payment_transfer');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');
            $data['message'] = $this->session->userdata('message');

            $data['action'] = admin_url('payment/payment_transfer/validate');
            $data['heading_title'] = $this->manifest['name'];

            $this->load
                ->js('/assets/js/bootstrap-typeahead.min.js')
                ->view('admin/payment_transfer', $data);
        }
    }

    /**
     * Create new category
     *
     * @access public
     * @return void
     */
    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->title('Tambah Bank');

        $this->form();
    }

    /**
     * Edit existing category
     *
     * @access public
     * @param int $payment_transfer_id
     * @return void
     */
    public function edit($payment_transfer_id)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->title('Edit Bank');

        $this->form($payment_transfer_id);
    }

    /**
     * Load category form
     *
     * @access private
     * @param int $payment_transfer_id
     * @return void
     */
    private function form($payment_transfer_id = null)
    {
        $this->load->model('payment_transfer_model');
        $this->load->model('system/order_status_model');

        $data['action'] = admin_url('payment/payment_transfer/validate');
        $data['order_statuses'] = $this->order_status_model->get_all();
        $data['payment_transfer_id'] = null;
        $data['name'] = '';
        $data['display_name'] = '';
        $data['rekening_number'] = '';
        $data['rekening_name'] = '';
        $data['bank_name'] = '';
        $data['logo'] = '';
        $data['instruction'] = '';
        $data['success_status_id'] = '';
        $data['total'] = '';
        $data['active'] = '';
        $data['sort_order'] = 0;


        if ($payment_transfer = $this->payment_transfer_model->get($payment_transfer_id)) {
            foreach ($payment_transfer as $key => $value) {
                $data[$key] = $value;
            }
        }

        $data['logo'] = isset($data['logo']) ? $data['logo'] : '';

        $this->load->library('image');

        $data['thumb_logo'] = $this->image->resize($data['logo'] !== '' ? $data['logo'] : 'no_image.jpg', 150, 150);
        $data['no_image'] = $this->image->resize('no_image.jpg', 150, 150);


        $this->load
            ->view('admin/payment_transfer_form', $data);
    }

    /**
     * Validate category form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $this->load->model('payment_transfer_model');
        $payment_transfer_id = $this->input->post('payment_transfer_id');


        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required|min_length[3]|max_length[32]')
            ->set_rules('display_name', 'Nama Display', 'trim|required|min_length[3]|max_length[255]')
            ->set_rules('rekening_number', 'Nomor Rekening', 'trim|required|min_length[3]|max_length[255]')
            ->set_rules('rekening_name', 'Nama Rekening', 'trim|required|min_length[3]|max_length[255]')
            ->set_rules('bank_name', 'Nama Bank', 'trim|required|min_length[3]|max_length[255]')
            ->set_rules('logo', 'Logo', 'trim|required|min_length[3]|max_length[255]')
            ->set_rules('instruction', 'Instruksi', 'trim|required|min_length[3]|max_length[255]')
            ->set_rules('total', 'Minimal Order', 'trim|required|min_length[3]|max_length[255]')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            if ($payment_transfer_id) {
                $post = $this->input->post(null, false);
                $post['active'] = (bool)$this->input->post('active');
                $this->payment_transfer_model->update($payment_transfer_id, $post);

                $json['success'] = 'Payment Transfer Bank berhasil diperbarui.';
                $this->session->set_flashdata('success', 'Payment Transfer Bank berhasil diperbarui.');
            } else {
                $payment_transfer_id = $this->payment_transfer_model->insert($this->input->post(null, true));

                $json['success'] = 'Payment Transfer Bank telah berhasil ditambahkan.';
                $this->session->set_flashdata('success', 'Payment Transfer Bank telah berhasil ditambahkan.');
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        $payment_transfer_ids = $this->input->post('payment_transfer_id');

        if (!$payment_transfer_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->load->model('payment_transfer_model');
            $this->payment_transfer_model->delete($payment_transfer_ids);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
}