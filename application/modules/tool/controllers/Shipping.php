<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('order/order_model');
    }

    /**
     * Tracking shipment status
     *
     * @access public
     * @return html
     */
    public function track()
    {
        $order_id = $this->input->get('order_id');

        $order = $this->order_model->get($order_id);

        if ($order && $order['waybill'] != '') {
            $waybill = $order['waybill'];
            $shipping_code = $order['shipping_code'];
        } else {
            $waybill = false;
            $shipping_code = false;
        }

        $html = '<div class="modal-dialog modal-lg">';
        $html .= '<div class="modal-content">';
        $html .= '<div class="modal-header">';
        $html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        $html .= '<h4 class="modal-title">Status Pengiriman #' . $order_id . '</h4>';
        $html .= '</div>';
        $html .= '<div class="modal-body" style="height: 300px; overflow-y : auto;">';
        $html .= '<table class="table table-bordered">';
        $html .= '<tbody>';

        $manifests = array();

        $this->load->library('rajaongkir');

        if ($waybill && $shipping_code) {
            $sc = explode('.', $shipping_code);
            if (isset($sc[1])) {
                $ssc = explode('_', $sc[1]);
                if (isset($ssc[0])) {
                    $result = $this->rajaongkir->waybill($waybill, $ssc[0]);

                    if (isset($result['manifest']) && is_array($result['manifest'])) {
                        foreach ($result['manifest'] as $manifest) {
                            $html .= '<tr>';
                            $html .= '<td>' . date('d/m/Y H:i', strtotime($manifest['manifest_date'] . ' ' . $manifest['manifest_time'])) . '</td>';
                            $html .= '<td>' . $manifest['manifest_description'] . '</td>';
                            $html .= '</tr>';
                        }
                    } else {
                        $html .= '<tr><td>Nomor resi pengiriman tidak ditemukan atau tidak valid!</td></tr>';
                    }
                }
            }
        } else {
            $html .= '<tr><td>Tidak ada metode pengiriman</td></tr>';
        }

        $html .= '<tbody>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= '<div class="modal-footer">';
        $html .= '<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Tutup</button>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';

        $this->output->set_output($html);
    }


    public function changecourier()
    {

        $this->load->library('admin/admin_auth');
        $order_id = $this->input->get('order_id');

        $this->load->library('rajaongkir');

        $order = $this->order_model->get($order_id);
        $code = $order['shipping_code'];

        $shipping_address['city_id'] = $order['city_id'];
        $shipping_address['subdistrict_id'] = $order['subdistrict_id'];
        $shipping_methods = array();

        $quotes = $this->rajaongkir->get_quote_courier_only($shipping_address);

        if ($quotes) {
            $shipping_methods[$quotes['code']] = array(
                'title' => $quotes['title'],
                'quote' => $quotes['quote'],
                'error' => $quotes['error']
            );
        }


        if ($this->input->get('shipping_method')) {

            $code = $this->input->get('shipping_method');
            $comment = $this->input->get('comment');
            $order_status_id = $order['order_status_id'];
            $notify = $this->input->get('notify');

            foreach ($shipping_methods as $shipping_method) {
                foreach ($shipping_method['quote'] as $quote) {
                    if ($quote['code'] == $code) {
                        $method = $quote['title'];
                    }
                }
            }

            $update = array(
                'shipping_code' => $code,
                'shipping_method' => $method
            );

            $order = $this->order_model->update($order_id, $update);


            $this->load->model('order/order_history_model');
            $this->load->model('order/order_total_model');

            $order_history = array(
                'order_id' => (int)$order_id,
                'order_status_id' => (int)$order_status_id,
                'admin_id' => (int)$this->admin_auth->admin_id(),
                'notify' => (int)$notify,
                'comment' => $comment
            );

            $id = array(
                'order_id' => $order_id,
                'code' => 'shipping'
            );
            $updatetotal = array(
                'title' => $method
            );

            $waybill = null;

            $this->order_model->update($order_id, $update);
            $this->order_total_model->updatetotal($id, $updatetotal);
            $this->order_history_model->insert($order_history);

            if ($notify) {
                $this->sendmail($order_id, $waybill, $comment);
            }

            $this->session->set_flashdata('success', 'Metode pengiriman berhasil di ubah');
            redirect('office/order/detail/' . $order_id, 'refresh');
        }


        $html = '<div class="modal-dialog modal-md">';
        $html .= '<div class="modal-content">';
        $html .= '<form action="' . site_url('tool/shipping/changecourier') . '" type="post" id="form-sm" class="form-horizontal">';
        $html .= '<div class="modal-header">';
        $html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        $html .= '<h4 class="modal-title">Ganti metode pengiriman #' . $order_id . '</h4>';
        $html .= '</div>';
        $html .= '<div class="modal-body" style="overflow-y : auto;">';

        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-md-5 pull-left">Pilih metode pengiriman:</label>';
        $html .= '<div class="col-md-7">';
        $html .= '<input type="hidden" name="order_id" value="' . $order_id . '" />';
        foreach ($shipping_methods as $shipping_method) {
            if (!$shipping_method['error']) {
                $html .= '<select id="shipping_method" name="shipping_method" class="form-control unicase-form-control shipping-method">';
                foreach ($shipping_method['quote'] as $quote) {
                    if ($quote['code'] == $code) {
                        $html .= '<option value="' . $quote['code'] . '" rel="' . $quote['text'] . '" selected="selected">' . $quote['title'] . ' - ' . $quote['text'] . '</option>';
                    } else {
                        $html .= '<option value="' . $quote['code'] . '" rel="' . $quote['text'] . '">' . $quote['title'] . ' - ' . $quote['text'] . '</option>';
                    }
                }
                $html .= '</select>';
            } else {
                $html .= '<div class="alert alert-danger">' . $shipping_method['error'] . '</div>';
            }
        }
        $html .= '</div>';
        $html .= '</div>';

        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-5">Catatan</label>';
        $html .= '<div class="col-sm-7">';
        $html .= '<textarea name="comment" class="form-control" rows="8"></textarea>';
        $html .= '</div>';
        $html .= '</div>';

        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-5">Notifikasi</label>';
        $html .= '<div class="col-sm-7">';
        $html .= '<div class="checkbox"><label><input type="checkbox" name="notify" value="1"> Notifikasi ke pembeli dan penjual</label></div>';
        $html .= '</div>';
        $html .= '</div>';

        $html .= '</div>';
        $html .= '<div class="modal-footer">';
        $html .= '<span class="pull-left"><font color="red">Biaya kirim tidak berubah</font></span>';
        $html .= '<button type="submit" class="btn btn-success" ><i class="fa fa-save"></i> Simpan</button>';
        $html .= '<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Tutup</button>';
        $html .= '</div>';
        $html .= '</form>';
        $html .= '</div>';
        $html .= '</div>';


        $this->output->set_output($html);
    }


    public function sendmail($order_id, $waybill = null, $comment = null)
    {
        $this->load->library('currency');
        $this->load->library('form_validation');

        $this->load->helper('form');
        $this->load->helper('language');

        $this->load->model('order/order_model');
        $this->load->model('system/order_status_model');


        $order_info = $this->order_model->get_order($order_id);


        $data['logo'] = $this->image->resize($this->config->item('logo'), 200);
        $data['order_id'] = $order_id;
        $data['invoice'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
        $data['name'] = $order_info['name'];
        $data['telephone'] = $order_info['telephone'];
        $data['email'] = $order_info['email'];
        $data['date_added'] = date('d/m/Y', strtotime($order_info['date_added']));
        $data['status'] = $order_info['status'];
        $data['ip_address'] = $order_info['ip'];
        $data['payment_method'] = $order_info['payment_method'];
        $data['total'] = $this->currency->format($order_info['total']);


        if ($waybill) {
            $data['waybill'] = $waybill;
        } else {
            $data['waybill'] = $waybill;
        }

        if ($comment) {
            $data['comment'] = $comment;
        } else {
            $data['comment'] = $comment;
        }

        $this->load->helper('format');


        $this->load->library('email');

        $this->email->initialize();
        $this->email->from($this->config->item('smtp_email'), $this->config->item('company'));
        $this->email->to($order_info['email']);
        $this->email->subject('Pemberitahuan Update Order');
        $this->email->message($this->load->layout(false)->view('admin/order_update', $data, true));
        $this->email->send();

        // admin notification
        $this->email->initialize();
        $this->email->from($this->config->item('smtp_email'), $this->config->item('company'));
        $this->email->to($this->config->item('email'));
        $this->email->subject('Pemberitahuan Update Order');
        $this->email->message($this->load->layout(false)->view('admin/order_update', $data, true));
        $this->email->send();

    }
}