<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Pengolah Gambar';
$lang['text_uploaded'] = 'Berhasil: File berhasil diupload!';
$lang['text_directory'] = 'Berhasil: Folder baru telah dibuat!';
$lang['text_delete'] = 'Berhasil: File atau folder telah dihapus!';
$lang['text_confirm'] = 'Apakah anda yakin?';
$lang['entry_search'] = 'Cari...';
$lang['entry_folder'] = 'Nama Folder';
$lang['button_parent'] = 'Induk';
$lang['button_folder'] = 'Folder Baru';
$lang['button_search'] = 'Cari';
$lang['button_delete'] = 'Hapus';
$lang['button_refresh'] = 'Refresh';
$lang['button_upload'] = 'Upload';
$lang['error_permission'] = 'Perhatian: Akses ditolak!';
$lang['error_filename'] = 'Perhatian: Nama file harus antara 3 sampai 255!';
$lang['error_folder'] = 'Perhatian: Nama folder harus antara 3 sampai 255!';
$lang['error_exists'] = 'Perhatian: File atau folder sudah ada!';
$lang['error_directory'] = 'Perhatian: Folder tidak ditemukan!';
$lang['error_delete'] = 'Perhatian: Anda tidak dapat menghapus folder!';

