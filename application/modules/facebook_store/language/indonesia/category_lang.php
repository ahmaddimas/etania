<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_refine'] = 'Perbaiki Pencarian';
$lang['text_product'] = 'Produk';
$lang['text_error'] = 'Kategori tidak ditemukan!';
$lang['text_empty'] = 'Tidak ada produk yang sesuai dengan kriteria yang diminta.';
$lang['text_compare'] = 'Komparasi Produk (%s)';
$lang['text_sort'] = 'Urut Berdasarkan:';
$lang['text_default'] = 'normal';
$lang['text_name_asc'] = 'Nama (A - Z)';
$lang['text_name_desc'] = 'Nama (Z - A)';
$lang['text_price_asc'] = 'Harga (Rendah &gt; Tinggi)';
$lang['text_price_desc'] = 'Harga (Tinggi &gt; Rendah)';
$lang['text_rating_asc'] = 'Rating (Terendah)';
$lang['text_rating_desc'] = 'Rating (Tertinggi)';
$lang['text_model_asc'] = 'Model (A - Z)';
$lang['text_model_desc'] = 'Model (Z - A)';
$lang['text_limit'] = 'Tampilkan:';