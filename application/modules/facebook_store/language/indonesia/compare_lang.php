<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['heading_title'] = 'Komparasi Produk';

$lang['text_product'] = 'Detil Produk';
$lang['text_name'] = 'Produk';
$lang['text_image'] = 'Foto';
$lang['text_price'] = 'Harga';
$lang['text_model'] = 'Model';
$lang['text_manufacturer'] = 'Merek';
$lang['text_availability'] = 'Ketersediaan';
$lang['text_instock'] = 'Tersedia';
$lang['text_rating'] = 'Peringkat';
$lang['text_reviews'] = 'Dengan %s ulasan';
$lang['text_summary'] = 'Ringkasan';
$lang['text_weight'] = 'Berat';
$lang['text_dimension'] = 'Dimensi';
$lang['text_compare'] = 'Komparasi Produk (%s)';
$lang['text_empty'] = 'Belum ada produk satu pun yang anda bandingkan.';

$lang['success_add'] = '<b>Berhasil</b> : Anda menambahkan <a href="%s">%s</a> ke daftar <a href="%s">komparasi produk</a>!';
$lang['success_remove'] = '<b>Berhasil</b> : Daftar komparasi produk telah diperbarui!';