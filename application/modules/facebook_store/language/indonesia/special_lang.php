<?php
// Heading
$lang['heading_title'] = 'Penawaran khusus';

// Text
$lang['text_empty'] = 'Tidak ada ada penawaran khusus produk.';
$lang['text_quantity'] = 'Jumlah:';
$lang['text_manufacturer'] = 'Merek:';
$lang['text_model'] = 'Kode Produk:';
$lang['text_points'] = 'Poin Reward:';
$lang['text_price'] = 'Harga:';
$lang['text_tax'] = 'Sebelum Pajak:';
$lang['text_compare'] = 'Bandingkan Produk (%s)';
$lang['text_sort'] = 'Urutkan:';
$lang['text_default'] = 'Standar';
$lang['text_name_asc'] = 'Nama (A - Z)';
$lang['text_name_desc'] = 'Nama (Z - A)';
$lang['text_price_asc'] = 'Harga (Rendah &gt; Tinggi)';
$lang['text_price_desc'] = 'Harga (Tinggi &gt; Rendah)';
$lang['text_rating_asc'] = 'Nilai (terendah)';
$lang['text_rating_desc'] = 'Nilai (tertinggi)';
$lang['text_model_asc'] = 'Model (A - Z)';
$lang['text_model_desc'] = 'Model (Z - A)';
$lang['text_limit'] = 'Tampilkan:';