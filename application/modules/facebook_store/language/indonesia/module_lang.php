<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['catalog/product'] = 'Produk';
$lang['catalog/category'] = 'Kategori Produk';
$lang['catalog/filter'] = 'Filter Produk';
$lang['catalog/attribute'] = 'Atribut Produk';
$lang['catalog/review'] = 'Ulasan Produk';
$lang['catalog/option'] = 'Opsi Produk';