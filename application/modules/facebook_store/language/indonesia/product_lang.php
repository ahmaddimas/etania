<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_search'] = 'cari';
$lang['text_brand'] = 'Merek';
$lang['text_manufacturer'] = 'Merek';
$lang['text_model'] = 'Kode Produk';
$lang['text_reward'] = 'Poin Reward';
$lang['text_points'] = 'Harga dalam poin reward';
$lang['text_stock'] = 'Status Stok';
$lang['text_instock'] = 'Tersedia';
$lang['text_tax'] = 'Sebelum Pajak';
$lang['text_discount'] = 'Harga Grosir';
$lang['text_option'] = 'Pilihan yang Tersedia';
$lang['text_minimum'] = 'Jumlah minimum dari produk ini adalah %s';
$lang['text_reviews'] = '%s ulasan';
$lang['text_write'] = 'Tulis ulasan';
$lang['text_login'] = 'Silakan <a href="%s"> login</a> atau <a href="%s"> mendaftar</a> untuk memberikan ulasan';
$lang['text_no_reviews'] = 'Belum ada ulasan untuk produk ini.';
$lang['text_note'] = '<span class="text-danger"> Catatan:</span> HTML tidak diterjemahkan!';
$lang['text_success'] = 'Terima kasih, ulasan anda telah dikirim dan menunggu persetujuan dari webmaster.';
$lang['text_related'] = 'Produk yang mungkin Anda suka';
$lang['text_tags'] = 'Tags/penandaan';
$lang['text_error'] = 'Produk tidak ditemukan!';

$lang['entry_qty'] = 'Jumlah';
$lang['entry_name'] = 'Nama Anda';
$lang['entry_review'] = 'Ulasan Anda';
$lang['entry_rating'] = 'Peringkat';
$lang['entry_good'] = 'Baik';
$lang['entry_bad'] = 'Buruk';
$lang['entry_captcha'] = 'Masukkan kode dalam kotak di bawah ini';

$lang['tab_description'] = 'Deskripsi';
$lang['tab_attribute'] = 'Spesifikasi';
$lang['tab_review'] = 'Ulasan (%s)';

$lang['error_name'] = 'Peringatan: Nama untuk ulasan harus antara 3 sampai 25 karakter!';
$lang['error_text'] = 'Peringatan: Teks ulasan harus antara 25 sampai 1000 karakter!';
$lang['error_rating'] = 'Peringatan: Harap beri nilai untuk ulasan!';
$lang['error_captcha'] = 'Peringatan: Kode verifikasi tidak sesuai dengan gambar!';