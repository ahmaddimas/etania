<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Terimakasih telah berbelanja di <?= $this->config->item('company') ?></h4>
            <div class="alert alert-success"><?= $text ?></div>
            <div class="row">
                <div class="col-md-7">
                    <div class="media">
                        <a class="pull-left" href="<?= $href ?>"><img class="media-object" src="<?= $image ?>"
                                                                      alt="<?= $name ?>"></a>
                        <div class="media-body">
                            <h4 class="media-heading"><?= $name ?></h4>
                            <p><?= cut_text($description, 100) ?></p>
                            <p><strong>Harga : <?= format_money($price) ?></strong></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <hr style="margin:2px 0;">
                    <div style="background-color:#efefef; padding: 10px 20px;"><?= $shopping_total ?></div>
                    <hr style="margin:2px 0;">
                </div>
            </div>
        </div>
        <div class="modal-footer" style="text-transform:uppercase;">
            <a href="<?= site_url('facebook_store') ?>" class="btn btn-default" data-dismiss="modal"><i
                        class="fa fa-chevron-left"></i> Belanja Lagi</a>
            <a href="<?= site_url('facebook_store/cart') ?>" class="btn btn-default" id="submit">Proses Pembayaran <i
                        class="fa fa-chevron-right"></i></a>
        </div>
    </div>
</div>