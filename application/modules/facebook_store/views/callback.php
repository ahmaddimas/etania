<div class="breadcrumb full-width">
    <div class="background-breadcrumb"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="title-page"><?= lang('heading_title') ?></h1>
                            <ul>
                                <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                                    <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background-content"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 center-column content-with-background" id="content">
                                Facebook Store berhasil dipasang di halaman Facebook anda. Kami telah menambahkan tab
                                baru berjudul "Toko" pada halaman Facebook anda
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>