<div class="row" id="product-wrapper">
    <?php foreach ($latest as $product) { ?>
        <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12" style="padding:0;">
            <div class="product-thumb" style="margin-left:15px;">
                <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>"
                                                                                  alt="<?php echo $product['name']; ?>"
                                                                                  title="<?php echo $product['name']; ?>"
                                                                                  class="img-responsive"/></a></div>
                <div>
                    <div class="caption">
                        <p><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        <p>
                            <?php if ($product['rating']) { ?>
                        <div class="rating">
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ($product['rating'] < $i) { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } else { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } ?>
                            <?php } ?>
                        </div>
                        <?php } ?>
                        <?php if ($product['price']) { ?>
                            <p class="price">
                                <?php if (!$product['discount']) { ?>
                                    <span class="price-new"><?php echo $product['price']; ?></span>
                                <?php } else { ?>
                                    <span class="price-new"><?php echo $product['discount']; ?></span><br><span
                                            class="price-old"><?php echo $product['price']; ?></span>
                                <?php } ?>
                            </p>
                        <?php } ?>
                        <?php if ($product['discount_percent']) { ?>
                            <span class="label label-success">Diskon <?= $product['discount_percent'] ?></span>
                        <?php } ?>
                        <?php if ($product['wholesaler']) { ?>
                            <span class="label label-danger" style="margin:0;">Wholesale</span>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>