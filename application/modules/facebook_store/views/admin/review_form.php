<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $template['title'] ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="review_id" value="<?= $review_id ?>">
            <div class="form-group">
                <label class="control-label col-sm-4">Penulis (Pelanggan)</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="author" value="<?= $author ?>">
                    <input type="hidden" name="customer_id" value="<?= $customer_id ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Produk</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="product" value="<?= $product ?>">
                    <input type="hidden" name="product_id" value="<?= $product_id ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Ulasan</label>
                <div class="col-sm-8">
                    <textarea name="text" class="form-control" rows="5"><?= $text ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Rating</label>
                <div class="col-sm-8">
                    <?php $stars = ''; ?>
                    <?php $radios = ''; ?>
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <?php $checked = $rating == $i ? 'checked="checked"' : ''; ?>
                        <?php $stars .= '<i class="fa fa-star" style="color:gold;"></i> '; ?>
                        <?php $radios .= '<label class="radio-inline"><input type="radio" name="rating" value="' . $i . '" ' . $checked . '> ' . $stars . '</label>'; ?>
                    <?php } ?>
                    <?= $radios ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Status</label>
                <div class="col-sm-4">
                    <select name="active" class="form-control">
                        <?php if ($active) { ?>
                            <option value="1" selected="selected">Aktif</option>
                            <option value="0">Nonaktif</option>
                        <?php } else { ?>
                            <option value="1">Aktif</option>
                            <option value="0" selected="selected">Nonaktif</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
            <button type="button" class="btn btn-default" id="submit"><i class="fa fa-check"></i> Simpan</button>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/plugins/bootstrap-typeahead.min.js') ?>"></script>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });

    $('input[name=\'author\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('customer/autocomplete')?>",
                data: 'name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    customers = [];
                    map = {};
                    $.each(json, function (i, customer) {
                        map[customer.name] = customer;
                        customers.push(customer.name);
                    });
                    process(customers);
                }
            });
        },
        updater: function (item) {
            $('input[name=\'author\']').val(map[item].name);
            $('input[name=\'customer_id\']').val(map[item].customer_id);

            return map[item].name;
        },
        minLength: 1
    });

    $('input[name=\'product\']').typeahead({
        source: function (query, process) {
            $.ajax({
                url: "<?=admin_url('catalog/product/auto_complete')?>",
                data: 'filter_name=' + query,
                type: 'get',
                dataType: 'json',
                success: function (json) {
                    products = [];
                    map = {};
                    $.each(json, function (i, product) {
                        map[product.name] = product;
                        products.push(product.name);
                    });
                    process(products);
                }
            });
        },
        updater: function (item) {
            $('input[name=\'product\']').val(map[item].name);
            $('input[name=\'product_id\']').val(map[item].product_id);

            return map[item].name;
        },
        minLength: 1
    });
</script>