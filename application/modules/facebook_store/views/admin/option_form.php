<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <a onclick="$('#form').submit();" class="btn btn-success"><i class="fa fa-check"></i> <?= lang('button_save') ?>
        </a>
        <a href="<?= $cancel ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?= lang('button_cancel') ?></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?php if ($error) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                        </button><?= $error ?></div>
                <?php } ?>
                <?= form_open($action, 'id="form" class="form-horizontal"') ?>
                <input type="hidden" name="option_id" value="<?= $option_id ?>">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Opsi</label>
                    <div class="col-sm-6">
                        <input type="text" name="name" value="<?= $name ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tipe</label>
                    <div class="col-sm-3">
                        <select name="type" class="form-control">
                            <?php foreach ($types as $key => $option_type) { ?>
                                <?php if ($key == $type) { ?>
                                    <option value="<?= $key ?>" selected><?= $option_type ?></option>
                                <?php } else { ?>
                                    <option value="<?= $key ?>"><?= $option_type ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Urutan</label>
                    <div class="col-sm-2">
                        <input type="text" name="sort_order" value="<?= $sort_order ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Pilihan</label>
                    <div class="col-sm-10">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Nama Pilihan</th>
                                <th>Urutan</th>
                                <th class="text-right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $option_value_row = 0; ?>
                            <?php foreach ($option_values as $option_value) { ?>
                                <tr id="option-value-row<?= $option_value_row ?>">
                                    <td><input type="hidden"
                                               name="option_value[<?php echo $option_value_row; ?>][option_value_id]"
                                               value="<?= $option_value['option_value_id'] ?>"/><input type="text"
                                                                                                       name="option_value[<?php echo $option_value_row; ?>][name]"
                                                                                                       value="<?= $option_value['name'] ?>"
                                                                                                       class="form-control"/>
                                    </td>
                                    <td class="text-right"><input type="text"
                                                                  name="option_value[<?php echo $option_value_row; ?>][sort_order]"
                                                                  value="<?= $option_value['sort_order'] ?>"
                                                                  class="form-control"/></td>
                                    <td class="text-right"><a class="btn btn-small btn-danger"
                                                              onclick="$('#option-value-row<?php echo $option_value_row; ?>').remove();"
                                                              class="button"><i class="fa fa-remove"></i></a></td>
                                </tr>
                                <?php $option_value_row++; ?>
                            <?php } ?>
                            <tr id="option-value">
                                <td colspan="2"></td>
                                <td class="text-right"><a class="btn btn-success" onclick="addOptionValue();"><i
                                                class="fa fa-plus"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var option_value_row = <?php echo $option_value_row; ?>;

    function addOptionValue() {
        html = '<tr id="option-value-row' + option_value_row + '">';
        html += '	<td><input type="hidden" name="option_value[' + option_value_row + '][option_value_id]" value="" />';
        html += '		<input type="text" name="option_value[' + option_value_row + '][name]" value="" class="form-control" />';
        html += '	</td>';
        html += '	<td class="text-right"><input type="text" name="option_value[' + option_value_row + '][sort_order]" value="" class="form-control" /></td>';
        html += '	<td class="text-right"><a class="btn btn-small btn-danger" onclick="$(\'#option-value-row' + option_value_row + '\').remove();" class="button"><i class="fa fa-remove"></i></a></td>';
        html += '</tr>';

        $('#option-value').before(html);

        option_value_row++;
    }
</script>