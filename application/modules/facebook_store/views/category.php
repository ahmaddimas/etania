<div class="page-header"><h4><?= $heading_title ?></h4><?= $description ?></div>
<div>
    <div>
        <?php if ($products) { ?>
            <div class="row">
                <?php if ($categories) { ?>
                    <div class="col-sm-2 text-left">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                Sub Kategori <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <?php foreach ($categories as $category) { ?>
                                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-sm-2 text-right">
                    <label class="control-label" style="margin-top:8px;" for="input-sort">Urutkan</label>
                </div>
                <div class="col-sm-8 text-right">
                    <select id="input-sort" class="form-control" onchange="location = this.value;">
                        <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                <option value="<?php echo $sorts['href']; ?>"
                                        selected="selected"><?php echo $sorts['text']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row" id="product-wrapper">
                <?php foreach ($products as $product) { ?>
                    <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12" style="padding:0;">
                        <div class="product-thumb" style="margin-left:15px;">
                            <div class="image"><a href="<?php echo $product['href']; ?>"><img
                                            src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                            title="<?php echo $product['name']; ?>" class="img-responsive"/></a></div>
                            <div>
                                <div class="caption">
                                    <p><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    <p>
                                        <?php if ($product['rating']) { ?>
                                    <div class="rating">
                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                            <?php if ($product['rating'] < $i) { ?>
                                                <span class="fa fa-stack"><i
                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                            <?php } else { ?>
                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <?php if ($product['price']) { ?>
                                        <p class="price">
                                            <?php if (!$product['discount']) { ?>
                                                <span class="price-new"><?php echo $product['price']; ?></span>
                                            <?php } else { ?>
                                                <span class="price-new"><?php echo $product['discount']; ?></span><br>
                                                <span class="price-old"><?php echo $product['price']; ?></span>
                                            <?php } ?>
                                        </p>
                                    <?php } ?>
                                    <?php if ($product['discount_percent']) { ?>
                                        <span class="label label-success">Diskon <?= $product['discount_percent'] ?></span>
                                    <?php } ?>
                                    <?php if ($product['wholesaler']) { ?>
                                        <span class="label label-danger" style="margin:0;">Grosir</span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-sm-12"><?= $pagination ?></div>
            </div>
        <?php } ?>
        <?php if (!$categories && !$products) { ?>
            <p>Tidak ada produk dalam kategori ini</p>
            <div class="buttons">
                <div class="pull-right"><a href="<?= site_url('facebook_store') ?>" class="btn btn-default">Lanjut</a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>