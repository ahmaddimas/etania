<div id="cart-content-ajax">
    <?php if ($products) { ?>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4>Keranjang Belanja</h4>
        <table class="table">
            <?php foreach ($products as $key => $product) { ?>
                <tr id="item-<?= $key ?>">
                    <td style="vertical-align:middle;padding-left:0px;"><a href="<?= $product['href'] ?>"><img
                                    src="<?= $product['thumb'] ?>"></a></td>
                    <td style="vertical-align:middle;"><strong><?= $product['name'] ?></strong><br>
                        <small><?= $product['quantity'] ?> &times; <?= $product['price'] ?></small>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <a href="<?= site_url('checkout/cart') ?>" class="btn btn-warning btn-block">Keranjang Belanja</a>
        <a href="<?= site_url('checkout') ?>" class="btn btn-default btn-block">Checkout</a>
    <?php } else { ?>
        <div id="cart-content-ajax" style="text-align:center;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-shopping-basket fa-5x text-muted"></i>
            <p>Keranjang belanja kosong!</p>
        </div>
    <?php } ?>
</div>
<div id="cart-count-ajax"><?= count($products) ?></div>
				