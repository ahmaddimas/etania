<div class="cart-page">
    <div class="page-header"><h4><?= $template['title'] ?></h4></div>
    <?php if ($error) { ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?= $error ?>
        </div>
    <?php } ?>
    <?php if ($products) { ?>
        <?= form_open($action, 'id="form-cart"') ?>
        <div class="table-responsive">
            <table class="table cart-info">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center"><?= lang('column_image') ?></th>
                    <th><?= lang('column_name') ?></th>
                    <th class="text-center" width="15%"><?= lang('column_quantity') ?></th>
                    <th class="text-right"><?= lang('column_price') ?></th>
                    <th class="text-right"><?= lang('column_total') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($products as $key => $product) { ?>
                    <tr>
                        <td class="text-center"><a style="cursor:pointer;" onclick="cart.remove('<?= $key ?>');"
                                                   title="Keluarkan"><i class="fa fa-trash-o fa-2x"></i></a></td>
                        <td class="text-center">
                            <a href="<?= $product['href'] ?>"><img src="<?= $product['thumb'] ?>"
                                                                   alt="<?= $product['name'] ?>"
                                                                   title="<?= $product['name'] ?>"/></a>
                            <div class="visible-xs"><a href="<?= $product['href'] ?>"><?= $product['name'] ?>
                                    <div>
                        </td>
                        <td>
                            <a href="<?= $product['href'] ?>"><?= $product['name'] ?></a>
                            <?php if (!$product['stock']) { ?>
                                <br><span class="error">***</span>
                            <?php } ?>
                            <br/>
                            <?php if ($product['option']) { ?>
                                <?php foreach ($product['option'] as $option) { ?>
                                    <small><?= $option['name'] ?> : <?= $option['option_value'] ?></small>
                                <?php } ?>
                            <?php } ?>
                        </td>
                        <td class="text-center">
                            <div class="input-group">
								<span class="input-group-btn">
									<a class="btn btn-default button-quantity">-</a>
								</span>
                                <input type="hidden" value="<?= $key ?>">
                                <input style="line-height:22px;" type="text" class="form-control text-center"
                                       value="<?= $product['quantity'] ?>" style="text-align:center;">
                                <span class="input-group-btn">
									<a class="btn btn-default button-quantity">+</a>
								</span>
                            </div>
                        </td>
                        <td class="text-right"><?= $product['price'] ?></td>
                        <td class="text-right"><?= $product['total'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <?php foreach ($totals as $total) { ?>
                    <tr>
                        <th class="text-right" colspan="5"><strong><?= $total['title'] ?></strong></th>
                        <th class="text-right"><?= $total['text'] ?></th>
                    </tr>
                <?php } ?>
                </tfoot>
            </table>
        </div>
        </form>
        <div class="form-actions">
            <div class="pull-left"><a href="<?= site_url('facebook_store') ?>"
                                      class="btn btn-default"><?= lang('button_continue') ?></a></div>
            <div class="pull-right"><a href="<?= site_url('checkout') ?>" target="_blank"
                                       class="btn btn-default"><?= lang('button_checkout') ?></a></div>
        </div>
    <?php } else { ?>
        <p><?= lang('error_product') ?></p>
        <div class="form-actions">
            <div><a href="<?= site_url('facebook_store') ?>" class="btn btn-default"><?= lang('button_continue') ?></a>
            </div>
        </div>
    <?php } ?>
</div>
<script type="text/javascript">
    $('.button-quantity').on('click', function () {
        var btn = $(this);
        var oldVal = btn.closest('.input-group').find('input[type=\'text\']').val();
        if (btn.text() == '+') {
            var newVal = parseFloat(oldVal) + 1;
        } else {
            if (oldVal > 1) {
                var newVal = parseFloat(oldVal) - 1;
            } else {
                var newVal = 1;
            }
        }

        btn.closest('.input-group').find('input[type=\'text\']').val(newVal);
        productKey = btn.closest('.input-group').find('input[type=\'hidden\']').val();
        cart.update(productKey, newVal);
    });
</script>