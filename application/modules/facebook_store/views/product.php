<style>
    .product-info .price-new {
        font-size: 50px;
        line-height: 50px;
        color: #ffcc00;
        font-weight: 600;
        display: inline-block;
        vertical-align: top
    }

    .product-info .price-old {
        color: #ccc;
        font-weight: 400;
        text-decoration: line-through;
        padding-left: 15px;
        font-size: 20px;
        display: inline-block;
        vertical-align: top;
        line-height: 20px
    }
</style>
<div class="product-page" style="padding-bottom:30px;">
    <div class="page-header"><h4><?= $name ?></h4></div>
    <div class="row">
        <div class="col-sm-6">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <?php $key = 1; ?>
                    <?php foreach ($images as $image) { ?>
                        <li data-target="#carousel-example-generic" data-slide-to="<?= $key ?>"></li>
                        <?php $key++; ?>
                    <?php } ?>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="<?= $popup ?>" alt="<?= $name ?>">
                        <div class="carousel-caption"></div>
                    </div>
                    <?php if ($images) { ?>
                        <?php foreach ($images as $image) { ?>
                            <div class="item">
                                <img src="<?= $image['popup'] ?>" alt="<?= $name ?>">
                                <div class="carousel-caption"></div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"><span
                            class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"><span
                            class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
        <div class="col-sm-6 product-info">
            <ul class="list-group">
                <li class="list-group-item">
                    <span>Rating</span>
                    <span class="pull-right">
					<?php $star = ''; ?>
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                            <?php if ($i <= $rating) { ?>
                                <?php $star .= '<i class="fa fa-star" style="color:gold;"></i>'; ?>
                            <?php } else { ?>
                                <?php $star .= '<i class="fa fa-star" style="color:grey;"></i>'; ?>
                            <?php } ?>
                        <?php } ?>
                        <?= $star ?>
					(<?= sprintf(lang('text_reviews'), (int)$reviews) ?>)
					</span>
                </li>
                <li class="list-group-item">
                    <span><?= lang('text_model') ?></span>
                    <span class="pull-right"><?= $model ?></span>
                </li>
                <li class="list-group-item">
                    <span><?= lang('text_manufacturer') ?></span>
                    <span class="pull-right"><?= $manufacturer ?></span>
                </li>
                <li class="list-group-item">
                    <span><?= lang('text_stock') ?></span>
                    <span class="pull-right"><?= $stock_status ?></span>
                </li>
            </ul>
            <div class="price">
                <?php if ($discount) { ?>
                    <span class="price-old"><?= $price ?></span><br><span class="price-new"><?= $discount ?></span>
                <?php } else { ?>
                    <span class="price-new"><?= $price ?></span>
                <?php } ?>
            </div>
            <?php if ($discounts) { ?>
                <div class="options">
                    <h3><?= lang('text_discount') ?></h3>
                    <table class="table table-bordered">
                        <?php foreach ($discounts as $discount) { ?>
                            <tr>
                                <td>&ge; <?= $discount['quantity'] ?></td>
                                <td><?= $discount['price'] ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            <?php } ?>
            <?= form_open(site_url('facebook_store/cart/add'), 'id="form-cart"') ?>
            <div id="product">
                <?php if ($options) { ?>
                <div class="options">
                    <h2><?= lang('text_option') ?></h2>
                    <?php foreach ($options

                                   as $option) { ?>
                <?php if ($option['type'] == 'select') { ?>
                <?php if ($option['required']) { ?>
                    <div class="form-group required">
                        <?php } else { ?>
                        <div class="form-group">
                            <?php } ?>
                            <label class="control-label" for="input-option<?= $option['product_option_id'] ?>">
                                <?= $option['name'] ?>
                            </label>
                            <select name="option[<?= $option['product_option_id'] ?>]"
                                    id="input-option<?= $option['product_option_id'] ?>" class="form-control">
                                <option value=""><?= lang('text_select') ?></option>
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <option value="<?= $option_value['product_option_value_id'] ?>"><?= $option_value['name'] ?>
                                        <?php if ($option_value['price'] > 0) { ?>
                                            (<?= $option_value['price'] ?>)
                                        <?php } ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <div class="cart">
                        <input type="hidden" name="product_id" value="<?= $product_id ?>"/>
                        <div style="padding-top:30px;">
                            <div class="input-group input-group-lg">
								<span class="input-group-btn">
									<a class="btn btn-default button-quantity">-</a>
								</span>
                                <input name="quantity" type="text" class="form-control" value="1"
                                       style="text-align:center;" readonly="readonly">
                                <span class="input-group-btn">
									<a class="btn btn-default button-quantity">+</a>
									<button type="button" value="<?= lang('button_cart') ?>" id="button-cart"
                                            rel="<?= $product_id ?>" data-loading-text="<?= lang('text_loading') ?>"
                                            class="btn btn-primary"><i
                                                class="fa fa-shopping-basket"></i> <?= lang('button_cart') ?></button>
								</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="options">
                    <?= $this->config->item('sharing_button_code') ?>
                </div>
                </form>
            </div>
        </div>
        <div class="row" style="padding-top:30px;">
            <div class="col-sm-12">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-description" data-toggle="tab"><?= lang('tab_description') ?></a>
                    </li>
                    <li><a href="#tab-attribute" data-toggle="tab"><?= lang('tab_attribute') ?></a></li>
                    <?php foreach ($product_tabs as $key => $product_tab) { ?>
                        <li><a href="#tab-custom-<?= $key ?>" data-toggle="tab"><?= $product_tab['title'] ?></a></li>
                    <?php } ?>
                    <li><a href="#tab-review" data-toggle="tab"><?= sprintf(lang('tab_review'), (int)$reviews) ?></a>
                    </li>
                </ul>
                <div class="tab-content" style="padding-top:20px;">
                    <div class="tab-pane active" id="tab-description">
                        <?= $description ?>
                    </div>
                    <div class="tab-pane" id="tab-attribute">
                        <?php if ($attribute_groups) { ?>
                            <table class="table table-bordered">
                                <?php foreach ($attribute_groups as $attribute_group) { ?>
                                    <thead>
                                    <tr>
                                        <th colspan="2" style="background:#f6f6f6;"><?= $attribute_group['name'] ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                        <tr>
                                            <td><?= $attribute['name'] ?></td>
                                            <td><?= $attribute['text'] ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                <?php } ?>
                            </table>
                        <?php } ?>
                    </div>
                    <div class="tab-pane" id="tab-pane"></div>
                    <?php foreach ($product_tabs as $key => $product_tab) { ?>
                        <div id="tab-custom-<?= $key ?>" class="tab-pane"><?= $product_tab['content'] ?></div>
                    <?php } ?>
                    <div class="tab-pane" id="tab-review"></div>
                </div>
            </div>
        </div>
        <?php if ($relates) { ?>
            <div class="page-header"><h4>Produk yang mungkin anda suka</h4></div>
            <div class="row" id="product-wrapper">
                <?php foreach ($relates as $product) { ?>
                    <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12" style="padding:0;">
                        <div class="product-thumb" style="margin-left:15px;">
                            <div class="image"><a href="<?php echo $product['href']; ?>"><img
                                            src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                            title="<?php echo $product['name']; ?>" class="img-responsive"/></a></div>
                            <div>
                                <div class="caption">
                                    <p><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    <p>
                                        <?php if ($product['rating']) { ?>
                                    <div class="rating">
                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                            <?php if ($product['rating'] < $i) { ?>
                                                <span class="fa fa-stack"><i
                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                            <?php } else { ?>
                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <?php if ($product['price']) { ?>
                                        <p class="price">
                                            <?php if (!$product['discount']) { ?>
                                                <span class="price-new"><?php echo $product['price']; ?></span>
                                            <?php } else { ?>
                                                <span class="price-new"><?php echo $product['discount']; ?></span><br>
                                                <span class="price-old"><?php echo $product['price']; ?></span>
                                            <?php } ?>
                                        </p>
                                    <?php } ?>
                                    <?php if ($product['discount_percent']) { ?>
                                        <span class="label label-success">Diskon <?= $product['discount_percent'] ?></span>
                                    <?php } ?>
                                    <?php if ($product['wholesaler']) { ?>
                                        <span class="label label-danger" style="margin:0;">Grosir</span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
    <div class="modal fade" id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="comment-modal-label"
         aria-hidden="true"></div>
    <script src="<?= base_url('themes/desktop/assets/js/jscroll.min.js') ?>"></script>
    <script type="text/javascript">
        $.ajax({
            url: "<?=site_url('facebook_store/product/review')?>",
            type: 'get',
            data: 'product_id=<?=$product_id?>',
            dataType: 'html',
            success: function (html) {
                $('#tab-review').html('<div class="scroll">' + html + '</div>');
                $('.scroll').jscroll({
                    autoTrigger: false,
                    loadingHtml: '<i class="fa fa-refresh fa-spin"></i> Loading...',
                    padding: 20,
                    nextSelector: 'a:last',
                    contentSelector: ''
                });
            }
        });

        $('#button-cart').on('click', function () {
            $.ajax({
                url: $('#form-cart').attr('action'),
                type: 'post',
                data: $('#form-cart').serialize(),
                dataType: 'json',
                success: function (json) {
                    $('.form-group').removeClass('has-error');
                    $('.text-danger').remove();
                    if (json['redirect']) {
                        location = json['redirect'];
                    }
                    if (json['success']) {
                        $('body').append('<div class="modal fade" id="cart-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['success'] + '</div>');
                        $('#cart-modal').modal('show');
                        $('#cart-modal').on('hidden.bs.modal', function (e) {
                            $('#cart-modal').remove();
                        });
                        $('#cart-block .cart-content').load('checkout/cart/info #cart-content-ajax');
                        $('#cart-block .cart-count').html(json['total']);
                    } else if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            $('#input-option' + i).closest('.form-group').addClass('has-error');
                            $('#input-option' + i).after('<small class="text-danger"><i>' + json['error']['option'][i] + '</i></small>');
                        }
                    }
                }
            });
        });

        $('.button-quantity').on('click', function () {
            var btn = $(this);
            var oldVal = $('input[name=\'quantity\']').val();
            if (btn.text() == '+') {
                var newVal = parseFloat(oldVal) + 1;
            } else {
                if (oldVal > 1) {
                    var newVal = parseFloat(oldVal) - 1;
                } else {
                    var newVal = 1;
                }
            }

            $('input[name=\'quantity\']').val(newVal);
        });
    </script>