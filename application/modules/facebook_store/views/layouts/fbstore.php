<!DOCTYPE html>
<!--[if IE 7]>
<html lang="en" class="ie7 responsive"> <![endif]-->
<!--[if IE 8]>
<html lang="en" class="ie8 responsive"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 responsive"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="responsive">
<!--<![endif]-->

<head>
    <title><?= $template['title'] ?></title>
    <base href="<?= base_url() ?>"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= $template['metadata'] ?>
    <link href="<?= $_favicon ?>" rel="icon"/>
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('themes/desktop/assets/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">
    <style>body {
            padding-top: 50px;
        }

        .product-page .list-group-item.active, .product-page .list-group-item.active:hover, .product-page .list-group-item.active:focus {
            background-color: #f5f5f5;
            color: #333;
            border-color: #ddd
        }

        .product-page .list-group-item.active .list-group-item-text, .product-page .list-group-item.active:hover .list-group-item-text, .product-page .list-group-item.active:focus .list-group-item-text {
            color: #333
        }

        #content {
            min-height: 600px
        }

        .product-thumb {
            border: 1px solid #ddd;
            margin-bottom: 15px;
            overflow: auto;
            border-radius: 4px;
            -moz-border-radius: 4px;
            background-color: #fff;
        }

        .product-thumb .image {
            text-align: center;
            background-color: #fff;
        }

        .product-thumb .image a {
            display: block
        }

        .product-thumb .image a:hover {
            opacity: .8
        }

        .product-thumb .image img {
            margin-left: auto;
            margin-right: auto
        }

        .product-grid .product-thumb .image {
            float: none
        }

        @media (min-width: 767px) {
            .product-list .product-thumb .image {
                float: left;
                padding: 0 15px
            }
        }

        .product-thumb h4 {
            font-weight: 700
        }

        .product-thumb .caption {
            padding: 0 20px;
            min-height: 120px
        }

        .product-thumb .caption p a {
            color: #333
        }

        .product-thumb .caption p a:hover {
            color: #f26039;
            text-decoration: none
        }

        .product-list .product-thumb .caption {
            margin-left: 230px
        }

        @media (max-width: 1200px) {
            .product-grid .product-thumb .caption {
                min-height: 120px;
                padding: 0 10px
            }
        }

        @media (max-width: 767px) {
            .product-list .product-thumb .caption {
                min-height: 0;
                margin-left: 0;
                padding: 0 10px
            }

            .product-grid .product-thumb .caption {
                min-height: 0
            }
        }

        .product-thumb .rating {
            padding-bottom: 10px
        }

        .rating .fa-stack {
            font-size: 8px
        }

        .rating .fa-star-o {
            color: #999;
            font-size: 15px
        }

        .rating .fa-star {
            color: #FC0;
            font-size: 15px
        }

        .rating .fa-star + .fa-star-o {
            color: #E69500
        }

        h2.price {
            margin: 0
        }

        .product-thumb .price {
            color: #444
        }

        .product-thumb .price-new {
            font-weight: 600;
            color: #f26039
        }

        .product-thumb .price-old {
            color: #999;
            text-decoration: line-through;
            margin-left: 0
        }

        .product-thumb .price-tax {
            color: #999;
            font-size: 12px;
            display: block
        }

        .product-thumb .button-group {
            border-top: 1px solid #ddd;
            background-color: #eee;
            overflow: auto
        }

        .product-list .product-thumb .button-group {
            border-left: 1px solid #ddd
        }

        @media (max-width: 768px) {
            .product-list .product-thumb .button-group {
                border-left: none
            }
        }

        .product-thumb .button-group button {
            width: 60%;
            border: none;
            display: inline-block;
            float: left;
            background-color: #eee;
            color: #888;
            line-height: 38px;
            font-weight: 700;
            text-align: center;
            text-transform: uppercase
        }

        .product-thumb .button-group button + button {
            width: 20%;
            border-left: 1px solid #ddd
        }

        .product-thumb .button-group button:hover {
            color: #444;
            background-color: #ddd;
            text-decoration: none;
            cursor: pointer
        }

        @media (max-width: 1200px) {
            .product-thumb .button-group button, .product-thumb .button-group button + button {
                width: 33.33%
            }
        }

        @media (max-width: 767px) {
            .product-thumb .button-group button, .product-thumb .button-group button + button {
                width: 33.33%
            }
        }

        .thumbnails {
            overflow: auto;
            clear: both;
            list-style: none;
            padding: 0;
            margin: 0
        }

        .thumbnails > li {
            margin-left: 20px
        }

        .thumbnails {
            margin-left: -20px
        }

        .thumbnails > img {
            width: 100%
        }

        .image-additional a {
            margin-bottom: 20px;
            padding: 5px;
            display: block;
            border: 1px solid #ddd
        }

        .image-additional {
            max-width: 78px
        }

        .thumbnails .image-additional {
            float: left;
            margin-left: 20px
        }

        @media (min-width: 1200px) {
            #content .col-lg-2:nth-child(6n+1), #content .col-lg-2:nth-child(6n+1), #content .col-lg-3:nth-child(4n+1), #content .col-lg-4:nth-child(3n+1), #content .col-lg-6:nth-child(2n+1) {
                clear: left
            }
        }

        @media (min-width: 992px) and (max-width: 1199px) {
            #content .col-md-2:nth-child(6n+1), #content .col-md-2:nth-child(6n+1), #content .col-md-3:nth-child(4n+1), #content .col-md-4:nth-child(3n+1), #content .col-md-6:nth-child(2n+1) {
                clear: left
            }
        }

        @media (min-width: 768px) and (max-width: 991px) {
            #content .col-sm-2:nth-child(6n+1), #content .col-sm-2:nth-child(6n+1), #content .col-sm-3:nth-child(4n+1), #content .col-sm-4:nth-child(3n+1), #content .col-sm-6:nth-child(2n+1) {
                clear: left
            }
        }

        #product-wrapper {
            padding-right: 15px;
            padding-top: 15px;
            background-color: #fff /*#E9EBEE*/;
        }

        table.cart-info tr th {
            background-color: #efefef;
        }

        .dropdown-submenu {
            position: relative;
        }

        .dropdown-submenu > .dropdown-menu {
            top: 0;
            left: 100%;
            margin-top: -6px;
            margin-left: -1px;
            -webkit-border-radius: 0 6px 6px 6px;
            -moz-border-radius: 0 6px 6px;
            border-radius: 0 6px 6px 6px;
        }

        .dropdown-submenu:hover > .dropdown-menu {
            display: block;
        }

        .dropdown-submenu > a:after {
            display: block;
            content: " ";
            float: right;
            width: 0;
            height: 0;
            border-color: transparent;
            border-style: solid;
            border-width: 5px 0 5px 5px;
            border-left-color: #ccc;
            margin-top: 5px;
            margin-right: -10px;
        }

        .dropdown-submenu:hover > a:after {
            border-left-color: #fff;
        }

        .dropdown-submenu.pull-left {
            float: none;
        }

        .dropdown-submenu.pull-left > .dropdown-menu {
            left: -100%;
            margin-left: 10px;
            -webkit-border-radius: 6px 0 6px 6px;
            -moz-border-radius: 6px 0 6px 6px;
            border-radius: 6px 0 6px 6px;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script>
        var storeUrl = $('base').attr('href');

        function getUrlPath() {
            query = String(document.location).split('?');

            if (query[0]) {
                value = query[0].slice(storeUrl.length);
                if (value.length > 0) {
                    return urlPath = value;
                } else {
                    return urlPath = '';
                }
            }
        }

        var cart = {
            'add': function (product_id, quantity) {
                $.ajax({
                    url: 'facebook_store/cart/add',
                    type: 'post',
                    data: 'product_id=' + product_id + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1),
                    dataType: 'json',
                    success: function (json) {
                        if (json['redirect']) {
                            location = json['redirect'];
                        }

                        if (json['success']) {
                            $('body').append('<div class="modal fade" id="cart-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['success'] + '</div>');
                            $('#cart-modal').modal('show');
                            $('#cart-modal').on('hidden.bs.modal', function (e) {
                                $('#cart-modal').remove();
                            });

                            $('#cart-block .cart-content').load('facebook_store/cart/info #cart-content-ajax');
                            $('#cart-block .cart-count').html(json['total']);
                        }
                    }
                });
            },
            'update': function (key, quantity) {
                $.ajax({
                    url: 'facebook_store/cart/edit',
                    type: 'post',
                    data: 'key=' + key + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1),
                    dataType: 'json',
                    success: function (json) {
                        if (getUrlPath() == 'facebook_store/cart' || getUrlPath() == 'checkout') {
                            location = 'facebook_store/cart';
                        } else {
                            $('#cart-block .cart-content').load('facebook_store/cart/info #cart-content-ajax');
                            $('#cart-block .cart-count').html(json['total']);
                        }
                    }
                });
            },
            'remove': function (key) {
                $.ajax({
                    url: 'facebook_store/cart/remove',
                    type: 'post',
                    data: 'key=' + key,
                    dataType: 'json',
                    success: function (json) {
                        if (getUrlPath() == 'facebook_store/cart' || getUrlPath() == 'checkout') {
                            location = 'facebook_store/cart';
                        } else {
                            $('#cart-block .cart-content').load('facebook_store/cart/info #cart-content-ajax');
                            $('#cart-block .cart-count').html(json['total']);
                        }
                    }
                });
            }
        }
    </script>
</head>
<body>
<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation" style="background-color:#fff;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?= site_url('facebook_store') ?>">Beranda</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pilih Kategori <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <?php foreach ($category_menu as $category) { ?>
                            <?php if ($category['children']) { ?>
                                <li class="dropdown-submenu">
                                    <a href="<?= $category['href'] ?>" onclick="window.location = $(this).attr('href');"
                                       class="dropdown-toggle" data-toggle="dropdown"><?= $category['name'] ?></a>
                                    <ul class="dropdown-menu">
                                        <?php foreach ($category['children'] as $children) { ?>
                                            <?php if ($children['children']) { ?>
                                                <li class="dropdown-submenu">
                                                    <a href="<?= $children['href'] ?>"
                                                       onclick="window.location = $(this).attr('href');"
                                                       class="dropdown-toggle"
                                                       data-toggle="dropdown"><?= $children['name'] ?></a>
                                                    <ul class="dropdown-menu">
                                                        <?php foreach ($children['children'] as $subchildren) { ?>
                                                            <li>
                                                                <a href="<?= $subchildren['href'] ?>"><?= $subchildren['name'] ?></a>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php } else { ?>
                                                <li><a href="<?= $children['href'] ?>"><?= $children['name'] ?></a></li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } else { ?>
                                <li><a href="<?= $category['href'] ?>"><?= $category['name'] ?></a></li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Halaman <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <?php foreach ($page_menu as $page) { ?>
                            <li>
                                <a href="<?= site_url('facebook_store/page/' . $page['slug']) ?>"><?= $page['title'] ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
            <div class="navbar-right">
                <a href="<?= site_url('facebook_store/cart') ?>" class="btn btn-default navbar-btn"><i
                            class="fa fa-shopping-bag"></i> Keranjang Belanja</a>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <?= $template['body'] ?>
</div>
</body>