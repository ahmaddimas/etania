<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'modules/facebook_store/core/FBS_Controller.php';

class Cart extends FBS_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('shopping_cart');
        $this->load->library('currency');
        $this->load->library('encrypt');
        $this->load->library('image');
        $this->load->helper('form');
        $this->lang->load('checkout/cart');
        $this->load->model('catalog/category_model');
    }

    /**
     * Shopping cart page
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post('quantity') && is_array($this->input->post('quantity'))) {
            foreach ($this->input->post('quantity') as $key => $value) {
                $this->shopping_cart->update($key, $value);
            }

            $this->_reset();

            $this->session->set_flashdata('success', lang('text_remove'));

            redirect('facebook_store/cart');
        }

        if ($this->session->flashdata('error')) {
            $data['error'] = $this->session->flashdata('error');
        } elseif (!$this->shopping_cart->has_stock()) {
            $data['error'] = lang('error_stock');
        } else {
            $data['error'] = false;
        }

        $data['success'] = $this->session->flashdata('success');
        $data['action'] = site_url('facebook_store/cart');
        $data['weight'] = $this->weight->format($this->shopping_cart->get_weight(), $this->config->item('weight_class_id'));

        $data['products'] = array();

        $products = $this->shopping_cart->get_products();

        foreach ($products as $key => $product) {
            $product_total = 0;
            $subtotal = 0;
            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $data['error'] = sprintf(lang('error_minimum'), $product['name'], $product['minimum']);
            }

            if ($product['image']) {
                $image = $this->image->resize($product['image'], 150);
            } else {
                $image = '';
            }

            $path = '';

            if ($category = $this->category_model->get_category($product['category_id'])) {
                if ($category['path_id']) {
                    $category_ids = explode('-', $category['path_id']);
                    foreach ($category_ids as $category_id) {
                        if ($category_path = $this->category_model->get_category($category_id)) {
                            $path .= $category_path['slug'] . '/';
                        }
                    }
                }

                $path .= $category['slug'] . '/';
            }

            $product['href'] = site_url('facebook_store/product/' . $path . $product['slug']);
            $product['thumb'] = $image;
            $product['price'] = $this->currency->format($product['price']);
            $product['total'] = $this->currency->format($product['total']);
            $product['remove'] = site_url('facebook_store/cart') . '?remove=' . $product['key'];

            $data['products'][$key] = $product;
        }

        $data['next'] = $this->input->post('next') ? $this->input->post('next') : '';

        if ($this->input->post('coupon')) {
            $data['coupon'] = $this->input->post('coupon');
        } elseif ($this->session->userdata('coupon')) {
            $data['coupon'] = $this->session->userdata('coupon');
        } else {
            $data['coupon'] = '';
        }

        $this->load->model('location/location_model');

        $this->load->library('total');
        $this->load->helper('array');

        list($total_data, $total) = $this->total->get_totals();

        $total_data = reorder($total_data, 'sort_order', 'asc');

        $data['totals'] = $total_data;
        $data['continue'] = site_url();
        $data['checkout'] = site_url('facebook_store/cart');

        $this->load
            ->title(lang('heading_title'))
            ->view('cart', $data);
    }

    /**
     * Add product to shopping cart
     *
     * @access public
     * @return void
     */
    public function add()
    {
        $json = array();

        $product_id = $this->input->post('product_id') ? $this->input->post('product_id') : 0;
        $option = $this->input->post('option') != '' ? array_filter($this->input->post('option')) : '';

        $this->load->model('catalog/product_model');

        if ($product = $this->product_model->get_product($product_id)) {
            foreach ($this->product_model->get_options($product_id) as $product_option) {
                if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                    $json['error']['option'][$product_option['product_option_id']] = sprintf(lang('error_required'), $product_option['name']);
                }
            }

            $path = '';

            if ($category = $this->category_model->get_category($product['category_id'])) {
                if ($category['path_id']) {
                    $category_ids = explode('-', $category['path_id']);
                    foreach ($category_ids as $category_id) {
                        if ($category_path = $this->category_model->get_category($category_id)) {
                            $path .= $category_path['slug'] . '/';
                        }
                    }
                }

                $path .= $category['slug'] . '/';
            }

            $quantity = $this->input->post('quantity') ? $this->input->post('quantity') : 1;

            if (!$json) {
                if ($option == '') {
                    $option = array();
                }

                $this->shopping_cart->add($product_id, $quantity, $option);


                $this->load->helper('format');

                if ($product['image']) {
                    $image = $product['image'];
                } else {
                    $image = 'no_image.jpg';
                }

                $image = $this->image->resize($image, 100, 100);

                $data = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'href' => site_url('product/' . $path . $product['slug']),
                    'image' => $image,
                    'price' => $product['discount'] ? $product['discount'] : $product['price'],
                    'shopping_total' => sprintf(lang('text_shopping_total'), $this->shopping_cart->count_products(), $this->currency->format($this->shopping_cart->get_total())),
                    'description' => $product['description'],
                    'text' => sprintf(lang('text_success'), site_url('facebook_store/product/' . $path . $product['slug']), $product['name'], site_url('facebook_store/cart'))
                );

                $json['success'] = $this->load->layout(false)->view('cart_success', $data, true);
                $json['total'] = $this->shopping_cart->count_products();

                $this->session->unset_userdata('shipping_method');
                $this->session->unset_userdata('shipping_methods');
                $this->session->unset_userdata('payment_method');
                $this->session->unset_userdata('payment_methods');
            } else {
                if ($option == '') {
                    $json['redirect'] = 'product/' . $path . $product['slug'];
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    public function edit()
    {
        $json = array();

        $this->shopping_cart->update($this->input->post('key'), $this->input->post('quantity'));

        $this->session->unset_userdata('shipping_method');
        $this->session->unset_userdata('shipping_methods');
        $this->session->unset_userdata('payment_method');
        $this->session->unset_userdata('payment_methods');

        $json['total'] = $this->shopping_cart->count_products();
        $json['success'] = true;

        $this->output->set_output(json_encode($json));
    }

    public function info()
    {
        $this->lang->load('cart');
        $this->load->library('image');
        $this->load->library('currency');
        $this->load->library('total');
        $this->load->helper('array');

        $data['products'] = array();
        $products = $this->shopping_cart->get_products();

        foreach ($products as $product) {
            if ($product['image']) {
                $image = $this->image->resize($product['image'], 80, 80);
            } else {
                $image = '';
            }

            $path = '';

            if ($category = $this->category_model->get_category($product['category_id'])) {
                if ($category['path_id']) {
                    $category_ids = explode('-', $category['path_id']);
                    foreach ($category_ids as $category_id) {
                        if ($category_path = $this->category_model->get_category($category_id)) {
                            $path .= $category_path['slug'] . '/';
                        }
                    }
                }

                $path .= $category['slug'] . '/';
            }

            $data['products'][] = array(
                'key' => $product['key'],
                'thumb' => $image,
                'name' => $product['name'],
                'quantity' => $product['quantity'],
                'price' => $this->currency->format($product['price']),
                'total' => $this->currency->format($product['price'] * $product['quantity']),
                'href' => site_url('product/' . $path . $product['slug']),
                'option' => $product['option']
            );
        }

        list($total_data, $total) = $this->total->get_totals();

        $total_data = reorder($total_data, 'sort_order', 'asc');

        $data['totals'] = array();

        foreach ($total_data as $result) {
            $data['totals'][] = array(
                'title' => $result['title'],
                'text' => $this->currency->format($result['value']),
            );
        }

        $data['total'] = $this->shopping_cart->count_products();
        $data['cart'] = site_url('facebook_store/cart');
        $data['checkout'] = site_url('facebook_store');

        echo $this->load->layout(false)->view('cart_info', $data);
    }

    /**
     * Remove item via ajax
     *
     * @access public
     * @return void
     */
    public function remove()
    {
        $json = array();

        if ($this->input->post('key')) {
            $this->shopping_cart->remove($this->input->post('key'));

            $this->session->set_flashdata('success', lang('text_remove'));

            $this->_reset();

            $json['total'] = $this->shopping_cart->count_products();
            $json['success'] = true;

            $this->output->set_output(json_encode($json));
        }
    }

    private function _reset()
    {
        $this->session->unset_userdata('shipping_method');
        $this->session->unset_userdata('shipping_methods');
        $this->session->unset_userdata('payment_method');
        $this->session->unset_userdata('payment_methods');
    }
}