<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('review_model');
    }

    /**
     * Index of reviews
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('r.review_id, s.name as product, r.rating, r.active, r.date_added, c.name as author')
                ->from('review r')
                ->join('product s', 's.product_id = r.product_id', 'left')
                ->join('customer c', 'c.customer_id = r.customer_id', 'left')
                ->edit_column('date_added', '$1', 'format_date(date_added, true)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->set_userdata('success');
            $data['error'] = $this->session->set_userdata('error');
            $data['message'] = $this->session->set_userdata('message');

            $this->load
                ->title('Ulasan')
                ->view('admin/review', $data);
        }
    }

    /**
     * Create new review
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('admin_error_create'))));
        }

        $this->load->title('Tambah Ulasan');

        $this->form();
    }

    /**
     * Edit
     *
     * @access public
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('admin_error_edit'))));
        }

        $this->load->title('Edit Ulasan');

        $this->form($this->input->get('review_id'));
    }

    /**
     * Load review form
     *
     * @access private
     * @param int $review_id
     * @return void
     */
    private function form($review_id = null)
    {

        $this->load->model('review_model');

        $data['review_id'] = null;
        $data['product_id'] = null;
        $data['product'] = '';
        $data['customer_id'] = '';
        $data['author'] = '';
        $data['text'] = '';
        $data['rating'] = 1;
        $data['active'] = 0;

        if ($review = $this->review_model->get_review($review_id)) {
            $data = $review;
        }

        $data['action'] = admin_url('catalog/review/validate');

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(false)->view('admin/review_form', $data, true, true)
        )));
    }

    /**
     * Validate review form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $review_id = $this->input->post('review_id');

        $this->form_validation
            ->set_rules('author', 'Pelanggan', 'trim|required')
            ->set_rules('customer_id', 'Pelanggan', 'trim|required')
            ->set_rules('text', 'Isi Ulasan', 'trim|required')
            ->set_rules('rating', 'Rating', 'trim|required')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $this->load->model('review_model');

            $post = $this->input->post(null, true);
            $post['rating'] = (int)$post['rating'];

            if ($review_id) {
                $this->review_model->update($review_id, $post);

                $json['success'] = 'Ulasan telah berhasil diperbarui';
            } else {
                if ($this->review_model->insert($post)) {
                    $json['success'] = 'Ulasan baru berhasil dibuat.';
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    public function delete()
    {
        check_ajax();

        $json = array();

        $review_ids = $this->input->post('review_id') ? $this->input->post('review_id') : array();

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        if (!$review_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            $this->review_model->delete($review_ids);

            $json['success'] = 'Ulasan telah dihapus!';
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Set to active
     *
     * @access public
     * @return json
     */
    public function active()
    {
        check_ajax();

        $json = array();

        $review_ids = $this->input->post('review_id');

        if (!$this->admin_auth->has_permission('edit')) {
            $json['error'] = lang('admin_error_edit');
        }

        if (!$review_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            if (is_array($review_ids)) {
                foreach ($review_ids as $review_id) {
                    $this->review_model->update($review_id, array('active' => 1));
                }
            } else {
                $this->review_model->update($review_id, array('active' => 1));
            }

            $json['success'] = lang('success_disable');
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Set to inactive
     *
     * @access public
     * @return json
     */
    public function inactive()
    {
        check_ajax();

        $json = array();

        $review_ids = $this->input->post('review_id');

        if (!$this->admin_auth->has_permission('edit')) {
            $json['error'] = lang('admin_error_edit');
        }

        if (!$review_ids) {
            $json['error'] = lang('error_selected');
        }

        if (empty($json['error'])) {
            if (is_array($review_ids)) {
                foreach ($review_ids as $review_id) {
                    $this->review_model->update($review_id, array('active' => 0));
                }
            } else {
                $this->review_model->update($review_id, array('active' => 0));
            }

            $json['success'] = lang('success_disable');
        }

        $this->output->set_output(json_encode($json));
    }
}