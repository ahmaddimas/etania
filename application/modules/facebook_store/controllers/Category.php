<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'modules/facebook_store/core/FBS_Controller.php';

class Category extends FBS_Controller
{
    private $limit = 12;

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('currency');
        $this->load->model('catalog/category_model');
        $this->load->model('catalog/product_model');
        $this->lang->load('catalog/category');
    }

    /**
     * Get all categories
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $segments = $this->uri->segment_array();

        if (count($segments) < 2) {
            show_404();
        } else {
            $this->load->breadcrumb(lang('text_home'), site_url());

            $category = false;
            $slugs = $segments;
            array_shift($segments);
            $slug = array_pop($segments);
            $path = '';
            $path_ids = array();

            foreach ($slugs as $slug) {
                if ($category = $this->category_model->get_category($slug)) {
                    $path .= $category['slug'] . '/';
                    $path_ids[] = $category['category_id'];
                    $this->load->breadcrumb($category['name'], site_url('facebook_store/category/' . $path));
                }
            }

            array_pop($path_ids);

            $path_id = implode('-', $path_ids);

            if ($category && $category['path_id'] == $path_id) {
                $products = array();
                $categories = array();

                $this->load->library('pagination');

                $sort = ($this->input->get('sort')) ? $this->input->get('sort') : 'sort_order';
                $order = ($this->input->get('order')) ? $this->input->get('order') : 'asc';
                $page = ($this->input->get('page')) ? $this->input->get('page') : 1;
                $filters = ($this->input->get('filter')) ? $this->input->get('filter') : array();

                $params = array(
                    'category_id' => $category['category_id'],
                    'sub_category' => true,
                    'filter' => $filters,
                    'sort' => $sort,
                    'order' => $order,
                    'start' => ($page - 1) * $this->limit,
                    'limit' => $this->limit
                );

                $filter_query = '';

                foreach ($filters as $filter_id) {
                    $filter_query .= '&filter[]=' . $filter_id;
                }

                $sorts = array();

                $sorts[] = array(
                    'text' => 'Default',
                    'value' => 'sort_order-asc',
                    'href' => site_url('facebook_store/category/' . $path . '?sort=sort_order&order=asc' . $filter_query)
                );

                $sorts[] = array(
                    'text' => lang('text_name_asc'),
                    'value' => 'name-asc',
                    'href' => site_url('facebook_store/category/' . $path . '?sort=name&order=asc' . $filter_query)
                );

                $sorts[] = array(
                    'text' => lang('text_name_desc'),
                    'value' => 'name-desc',
                    'href' => site_url('facebook_store/category/' . $path . '?sort=name&order=desc' . $filter_query)
                );

                $sorts[] = array(
                    'text' => lang('text_price_asc'),
                    'value' => 'price-asc',
                    'href' => site_url('facebook_store/category/' . $path . '?sort=price&order=asc' . $filter_query)
                );

                $sorts[] = array(
                    'text' => lang('text_price_desc'),
                    'value' => 'price-desc',
                    'href' => site_url('facebook_store/category/' . $path . '?sort=price&order=desc' . $filter_query)
                );

                $sorts[] = array(
                    'text' => lang('text_rating_asc'),
                    'value' => 'rating-desc',
                    'href' => site_url('facebook_store/category/' . $path . '?sort=rating&order=desc' . $filter_query)
                );

                $sorts[] = array(
                    'text' => lang('text_rating_desc'),
                    'value' => 'rating-asc',
                    'href' => site_url('facebook_store/category/' . $path . '?sort=rating&order=asc' . $filter_query)
                );

                foreach ($this->product_model->get_products($params) as $product) {
                    $href = ($path != '') ? 'facebook_store/product/' . $path . $product['slug'] : 'facebook_store/product/' . $product['slug'];

                    $products[] = array(
                        'product_id' => $product['product_id'],
                        'thumb' => $this->image->resize($product['image'] ? $product['image'] : 'no_image.jpg', 250, 250),
                        'name' => $product['name'],
                        'description' => $product['description'],
                        'price' => $this->currency->format($product['price']),
                        'discount' => (float)$product['discount'] ? $this->currency->format($product['discount']) : false,
                        'wholesaler' => ($product['wholesaler'] > 1) ? true : false,
                        'discount_percent' => (float)$product['discount'] ? ceil(($product['price'] - $product['discount']) / $product['price'] * 100) . '%' : false,
                        'rating' => $product['rating'],
                        'reviews' => (int)$product['reviews'],
                        'href' => site_url($href)
                    );
                }

                $query = '';
                $query .= '?sort=' . $sort;
                $query .= '&order=' . $order;
                $query .= $filter_query;

                $config = array(
                    'base_url' => site_url('facebook_store/category/' . $path) . $query,
                    'total_rows' => $this->product_model->count_products($params),
                    'per_page' => $this->limit,
                    'query_string_segment' => 'page',
                    'page_query_string' => true,
                    'full_tag_open' => '<ul class="pagination">',
                    'full_tag_close' => '</ul>',
                    'num_tag_open' => '<li>',
                    'num_tag_close' => '</li>',
                    'cur_tag_open' => '<li class="active"><a>',
                    'cur_tag_close' => '</a></li>',
                    'first_tag_open' => '<li>',
                    'first_tag_close' => '</li>',
                    'last_tag_open' => '<li>',
                    'last_tag_close' => '</li>',
                    'prev_tag_open' => '<li>',
                    'prev_tag_close' => '</li>',
                    'next_tag_open' => '<li>',
                    'next_tag_close' => '</li>',
                    'use_page_numbers' => true
                );

                $this->pagination->initialize($config);

                foreach ($this->category_model->get_subcategories($category['category_id']) as $sub_category) {
                    $categories[] = array(
                        'name' => $sub_category['name'],
                        'href' => site_url('facebook_store/category/' . $path . $sub_category['slug'])
                    );
                }

                $data['name'] = $category['name'];
                $data['description'] = html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8');
                $data['image'] = $this->image->resize($category['image'] ? $category['image'] : 'no_image.jpg', 300);
                $data['products'] = $products;
                $data['categories'] = $categories;
                $data['sorts'] = $sorts;
                $data['order'] = $order;
                $data['sort'] = $sort;
                $data['page'] = $page;
                $data['heading_title'] = $category['name'];
                $data['pagination'] = $this->pagination->create_links();
                $data['filters'] = $filters;

                $this->load->model('catalog/filter_model');
                $data['filter_groups'] = $this->filter_model->get_filter_menus();

                $data['compare'] = sprintf(lang('text_compare'), count($this->session->userdata('compare')));

                $this->load->helper('format');

                if ($this->input->is_ajax_request()) {
                    $this->output->set_output($this->load->layout(null)->view('category', $data, true));
                } else {
                    $this->load
                        ->title($category['meta_title'], $this->config->item('site_name'))
                        ->view('category', $data);
                }
            } else {
                show_404();
            }
        }
    }
}