<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'modules/facebook_store/core/FBS_Controller.php';

class Page extends FBS_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('page/page_model');
    }

    public function index($slug = false)
    {
        if ($page = $this->page_model->get_page($slug)) {
            foreach ($page as $key => $value) {
                $data[$key] = $value;
            }

            $data['breadcrumbs'][] = array(
                'text' => 'Home',
                'href' => site_url()
            );

            $data['breadcrumbs'][] = array(
                'text' => $data['title'],
                'href' => site_url('page/' . $data['slug'])
            );

            $this->load
                ->title($page['title'])
                ->metadata('description', $page['meta_description'])
                ->metadata('keywords', $page['meta_keyword'])
                ->view('page', $data);
        } else {
            show_404();
        }
    }

    /**
     * Page missing
     *
     * @access public
     * @return void
     */
    public function missing()
    {
        $this->output->set_status_header('404');

        $this->load
            ->title('Page Not Found')
            ->view('page_missing');
    }
}