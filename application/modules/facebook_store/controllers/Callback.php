<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Callback extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Callback
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->setting_model->edit_setting_value('config', 'fbstore', 1);
        $this->load->view('callback');
    }
}