<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'modules/facebook_store/core/FBS_Controller.php';

class Facebook_store extends FBS_Controller
{
    private $limit = 12;

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('currency');
        $this->load->model('catalog/category_model');
        $this->load->model('catalog/product_model');
        $this->load->model('banner/banner_model');
        $this->lang->load('catalog/category');
    }

    public function index()
    {
        $data = array();

        $data['categories'] = array();

        foreach ($this->category_model->get_subcategories(0) as $category) {
            if ($category['top']) {
                $data['categories'][] = array(
                    'name' => $category['name'],
                    'href' => site_url('category/' . $category['slug']),
                    'menu_image' => $category['menu_image']
                );
            }
        }

        $data['banners'] = array();
        $data['top_banners'] = array();
        $data['footer_banners'] = array();

        $banners = $this->banner_model->get_images($this->config->item('slideshow_banner_id'));

        foreach ($banners as $banner) {
            if ($banner['active']) {
                if ($banner['image']) {
                    $image = $banner['image'];
                } else {
                    $image = 'no_image.jpg';
                }

                $data['banners'][] = array(
                    'image' => $this->image->resize($banner['image'], 1440),
                    'title' => $banner['title'],
                    'subtitle' => $banner['subtitle'],
                    'link' => $banner['link'],
                    'link_title' => $banner['link_title'],
                );
            }
        }

        $top_banners = $this->banner_model->get_images($this->config->item('top_banner_id'));

        foreach ($top_banners as $banner) {
            if ($banner['active']) {
                if ($banner['image']) {
                    $image = $banner['image'];
                } else {
                    $image = 'no_image.jpg';
                }

                $data['top_banners'][] = array(
                    'image' => $this->image->resize($banner['image'], 555, 180),
                    'title' => $banner['title'],
                    'subtitle' => $banner['subtitle'],
                    'link' => $banner['link'],
                    'link_title' => $banner['link_title'],
                );
            }
        }

        $footer_banners = $this->banner_model->get_images($this->config->item('footer_banner_id'));

        foreach ($footer_banners as $banner) {
            if ($banner['active']) {
                if ($banner['image']) {
                    $image = $banner['image'];
                } else {
                    $image = 'no_image.jpg';
                }

                $data['footer_banners'][] = array(
                    'image' => $this->image->resize($banner['image'], 555, 180),
                    'title' => $banner['title'],
                    'subtitle' => $banner['subtitle'],
                    'link' => $banner['link'],
                    'link_title' => $banner['link_title'],
                );
            }
        }

        $data['latest'] = $this->_latest();

        $this->load
            ->title($this->config->item('seo_title'), $this->config->item('site_name'))
            ->view('home', $data);
    }

    private function _latest()
    {
        $products = array();

        $params = array(
            'start' => 0,
            'limit' => 12,
            'order' => 'date_added',
            'sort' => 'desc'
        );

        foreach ($this->product_model->get_products($params) as $product) {
            $path = 'facebook_store/product/';

            if ($category = $this->category_model->get_category($product['category_id'])) {
                $path_ids = explode('-', $category['path_id']);

                foreach ($path_ids as $path_id) {
                    if ($subcategory = $this->category_model->get_category($path_id)) {
                        $path .= $subcategory['slug'] . '/';
                    }
                }

                $path .= $category['slug'] . '/';
            }

            $path .= $product['slug'];

            $products[] = array(
                'product_id' => $product['product_id'],
                'thumb' => $this->image->resize($product['image'] ? $product['image'] : 'no_image.jpg', 300, 300),
                'name' => (strlen($product['name']) <= 48) ? $product['name'] : substr(strip_tags(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')), 0, 48) . '...',
                'description' => substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '...',
                'price' => $this->currency->format($product['price']),
                'discount' => (float)$product['discount'] ? $this->currency->format($product['discount']) : false,
                'wholesaler' => ($product['wholesaler'] > 1) ? true : false,
                'discount_percent' => (float)$product['discount'] ? ceil(($product['price'] - $product['discount']) / $product['price'] * 100) . '%' : false,
                'rating' => $product['rating'],
                'href' => site_url($path),
            );
        }

        return $products;
    }
}