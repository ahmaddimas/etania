<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FBS_Controller extends MY_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('shopping_cart');

        $this->load->vars('category_menu', $this->_get_categories());
        $this->load->layout('fbstore');
        $this->lang->load('front');

        $this->load->model('page/page_model');
        $this->load->vars('page_menu', $this->page_model->get_page_menus());
    }

    /**
     * Get category menu
     *
     * @access public
     * @return void
     */
    public function _get_categories()
    {
        $this->load->model('catalog/category_model');

        $data['categories'] = array();

        foreach ($this->category_model->get_subcategories(0, 12) as $category) {
            $children = array();

            foreach ($this->category_model->get_subcategories($category['category_id'], 8) as $subcategory) {
                $children2 = array();

                foreach ($this->category_model->get_subcategories($subcategory['category_id'], 8) as $subcategory2) {
                    $children2[] = array(
                        'name' => $subcategory2['name'],
                        'href' => site_url('facebook_store/category/' . $category['slug'] . '/' . $subcategory['slug'] . '/' . $subcategory2['slug']),
                        'children' => array()
                    );
                }

                $children[] = array(
                    'name' => $subcategory['name'],
                    'name' => $subcategory['name'],
                    'href' => site_url('facebook_store/category/' . $category['slug'] . '/' . $subcategory['slug']),
                    'children' => $children2
                );
            }

            $data['categories'][] = array(
                'name' => $category['name'],
                'href' => site_url('facebook_store/category/' . $category['slug']),
                'children' => $children,
                'menu_image' => $category['menu_image']
            );
        }

        return $data['categories'];
    }
}