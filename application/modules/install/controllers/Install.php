<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends Install_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('format');
        $this->load->helper('form');
        $this->load->library('session');

    }

    public function index()
    {
        $installed = $this->config->item('system_installed');
        if ($installed == 1) {
            redirect('/');
        }

        $this->load
            ->title('Welcome')
            ->view('install');
    }


    public function database()
    {
        $installed = $this->config->item('system_installed');
        if ($installed == 1) {
            redirect('/');
        }

        $data['database_name'] = $this->config->item('database_name');
        $data['database_username'] = $this->config->item('database_username');
        $data['database_password'] = $this->config->item('database_password');

        $this->load
            ->title('Install the Database')
            ->view('database', array('data' => $data));
    }


    public function company()
    {
        $installed = $this->config->item('system_installed');
        if ($installed == 1) {
            redirect('/');
        }

        $servername = "localhost";


        if ($this->input->post(null, true)) {
            ini_set('max_execution_time', 0);
            $username = $this->input->post('dbusername');
            $password = $this->input->post('dbpassword');
            $dbname = $this->input->post('dbname');


            error_reporting(0);
            $mysqli = new mysqli($servername, $username, $password, $dbname);
            if ($mysqli->connect_errno) {
                printf("<center><h3 style=margin-top:100px>Koneksi database gagal, silahkan cek detail setting database anda : %s\n", $mysqli->connect_error . '</h3>
						    	<br>
						    	<a href=install/database>Kembali</a></center>
						    	');
                exit();
            }


            $filename = 'db/table.sql';
            $templine = '';
            $lines = file($filename);

            foreach ($lines as $line) {
                if (substr($line, 0, 2) == '--' || $line == '')
                    continue;

                $templine .= $line;
                if (substr(trim($line), -1, 1) == ';') {
                    $show = $mysqli->query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . $mysqli->error . '<br /><br />');
                    $templine = '';
                }
            }

            $filename2 = 'db/tabledata.sql';
            $templine2 = '';
            $lines2 = file($filename2);

            foreach ($lines2 as $line2) {
                if (substr($line2, 0, 2) == '--' || $line2 == '')
                    continue;

                $templine2 .= $line2;
                if (substr(trim($line2), -1, 1) == ';') {
                    $show = $mysqli->query($templine2) or print('Error performing query \'<strong>' . $templine2 . '\': ' . $mysqli->error . '<br /><br />');
                    $templine2 = '';
                }
            }


            $addconfigdb = "INSERT INTO `setting` (`setting_id`, `group`, `key`, `value`, `serialized`) VALUES 
									(1044, 'config', 'database_name', '" . $dbname . "', 0),
									(1045, 'config', 'database_username', '" . $username . "', 0),
									(1046, 'config', 'database_password', '" . $password . "', 0);";
            $mysqli->query($addconfigdb);
            $mysqli->close();


            $file = file_get_contents('db/database.php', true);

            $newfile = str_replace("#host", $servername, $file);
            $newfile1 = str_replace("#dbname", $dbname, $newfile);
            $newfile2 = str_replace("#username", $username, $newfile1);
            $newfilefix = str_replace("#password", $password, $newfile2);
            $file = 'application/config/database.php';
            $myfile = fopen($file, "w") or die("Unable to open file!");
            fwrite($myfile, $newfilefix);
            fclose($myfile);
        }

        $data['company'] = $this->config->item('company');
        $data['address'] = $this->config->item('address');
        $data['telephone'] = $this->config->item('telephone');
        $data['email'] = $this->config->item('email');

        $this->load
            ->title('Company Detail')
            ->view('company', array('data' => $data));
    }


    public function email()
    {
        $installed = $this->config->item('system_installed');
        if ($installed == 1) {
            redirect('/');
        }

        if ($this->input->post(null, true)) {
            $this->setting_model->edit_setting_value('config', 'company', $this->input->post('company'));
            $this->setting_model->edit_setting_value('config', 'address', $this->input->post('address'));
            $this->setting_model->edit_setting_value('config', 'telephone', $this->input->post('telephone'));
            $this->setting_model->edit_setting_value('config', 'email', $this->input->post('email'));
        }


        $data['smtp_email'] = $this->config->item('smtp_email');
        $data['smtp_host'] = $this->config->item('smtp_host');
        $data['smtp_port'] = $this->config->item('smtp_port');
        $data['smtp_user'] = $this->config->item('smtp_user');
        $data['smtp_pass'] = $this->config->item('smtp_pass');


        $this->load
            ->title('Company Detail')
            ->view('email', array('data' => $data));
    }


    public function settup()
    {
        $installed = $this->config->item('system_installed');
        if ($installed == 1) {
            redirect('/');
        }

        if ($this->input->post(null, true)) {
            $this->setting_model->edit_setting_value('config', 'smtp_email', $this->input->post('smtp_email'));
            $this->setting_model->edit_setting_value('config', 'smtp_host', $this->input->post('smtp_host'));
            $this->setting_model->edit_setting_value('config', 'smtp_port', $this->input->post('smtp_port'));
            $this->setting_model->edit_setting_value('config', 'smtp_user', $this->input->post('smtp_user'));
            $this->setting_model->edit_setting_value('config', 'smtp_pass', $this->input->post('smtp_pass'));
        }


        $data['site_name'] = $this->config->item('site_name');
        $data['seo_title'] = $this->config->item('seo_title');
        $data['meta_keywords'] = $this->config->item('meta_keywords');
        $data['meta_description'] = $this->config->item('meta_description');

        $this->load
            ->title('Website Settup')
            ->view('settup', array('data' => $data));
    }

    public function courier()
    {
        $installed = $this->config->item('system_installed');
        if ($installed == 1) {
            redirect('/');
        }

        if ($this->input->post(null, true)) {
            $this->setting_model->edit_setting_value('config', 'site_name', $this->input->post('site_name'));
            $this->setting_model->edit_setting_value('config', 'seo_title', $this->input->post('seo_title'));
            $this->setting_model->edit_setting_value('config', 'meta_keywords', $this->input->post('meta_keywords'));
            $this->setting_model->edit_setting_value('config', 'meta_description', $this->input->post('meta_description'));
        }


        $this->load->model('location/location_model');
        $data['provinces'] = $this->location_model->get_provinces();
        $data['province_id'] = $this->config->item('province_id');
        $data['city_id'] = $this->config->item('city_id');
        $data['subdistrict_id'] = $this->config->item('subdistrict_id');
        $data['rajaongkir_api_key'] = $this->config->item('rajaongkir_api_key');


        $this->load
            ->title('Courier Settup')
            ->view('courier', array('data' => $data));
    }

    public function administrator()
    {
        $installed = $this->config->item('system_installed');
        if ($installed == 1) {
            redirect('/');
        }

        if ($this->input->post(null, true)) {
            $this->setting_model->edit_setting_value('config', 'province_id', $this->input->post('province_id'));
            $this->setting_model->edit_setting_value('config', 'city_id', $this->input->post('city_id'));
            $this->setting_model->edit_setting_value('config', 'subdistrict_id', $this->input->post('subdistrict_id'));
            $this->setting_model->edit_setting_value('config', 'rajaongkir_api_key', $this->input->post('rajaongkir_api_key'));
        }


        $this->load->model('admin/admin_model');
        if ($admin = $this->admin_model->get(-1)) {
            $data['name'] = $admin['name'];
            $data['username'] = $admin['username'];
            $data['email'] = $admin['email'];
            $data['password'] = "";
        } else {
            $data['name'] = "";
            $data['username'] = "";
            $data['email'] = "";
            $data['password'] = "";
        }

        $this->load
            ->title('Administrator Settup')
            ->view('administrator', array('data' => $data));
    }


    public function finish()
    {

        $installed = $this->config->item('system_installed');
        if ($installed == 1) {
            redirect('/');
        }

        if ($this->input->post(null, true)) {
            $this->load->model('admin/admin_model');

            $data['admin_id'] = -1;
            $data['name'] = $this->input->post('name');
            $data['username'] = $this->input->post('username');
            $data['email'] = strtolower($this->input->post('email'));
            $data['password'] = $this->input->post('password');
            $data['admin_group_id'] = 0;
            $data['active'] = 1;

            if ($admin = $this->admin_model->get(-1)) {
                $this->admin_model->update_admin(-1, $data);
            } else {
                $data['photo'] = 'admin.png';
                $this->admin_model->create_admin($data);
            }
        }

        $this->setting_model->edit_setting_value('config', 'system_installed', 1);


        $this->load->view('finish');

    }
}