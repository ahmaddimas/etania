<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= base_url('assets/css/admin.min.css') ?>" rel="stylesheet">
    <title><?= $this->config->item('company') ?></title>
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
    script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
    -->
</head>
<body>
<section class="login-content">
    <div class="login-box">
        <?php if ($message) { ?>
        <div class="login-form">
            <h3 class="login-head">Email Sent</h3>
            <div class="text-center"><i class="fa fa-check fa-5x text-success"></i></div>
            <p><?= $message ?></p>
            <div class="form-group mt-20 text-center">
                <p class="semibold-text mb-0"><a href="<?= admin_url('login') ?>">Go to login page</a></p>
            </div>
            </form>
            <?php } else { ?>
                <?= form_open(admin_url('reset'), 'id="form-reset" class="login-form"') ?>
                <div class="logo text-center"><img src="<?= $_logo ?>"></div>
                <div class="form-group">
                    <label class="control-label">Email</label>
                    <input type="text" name="email" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-9" id="captcha"></div>
                        <div class="col-xs-2 text-right"><a class="btn btn-warning btn-sm" id="reset-captcha"><i
                                        class="fa fa-refresh" title="Reload image"></i></a></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="text" name="captcha" placeholder="Ketik susunan kode di atas"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group btn-container">
                    <button class="btn btn-primary btn-block" id="button-reset">Reset <i class="fa fa-unlock fa-lg"></i>
                    </button>
                </div>
                <div class="form-group mt-20">
                    <p class="semibold-text mb-0"><a href="<?= admin_url('login') ?>"><i
                                    class="fa fa-angle-left fa-fw"></i> Kembali login</a></p>
                </div>
                </form>
            <?php } ?>
        </div>
</section>
<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/js/essential-plugins.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/pace.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/bootstrap-notify.min.js') ?>"></script>
<script src="<?= base_url('assets/js/admin.js') ?>"></script>
<script type="text/javascript">
    $(document).delegate('#button-reset', 'click', function (e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            url: $('#form-reset').attr('action'),
            data: $('#form-reset').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                if (json['warning']) {
                    $.notify({
                        title: "Warning : ",
                        message: json['warning'],
                        icon: 'fa fa-ban'
                    }, {
                        type: "danger"
                    });
                } else if (json['redirect']) {
                    window.location = json['redirect'];
                }
            }
        });
    });

    $(document).delegate('#reset-captcha', 'click', function (e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            url: "<?=site_url('captcha')?>",
            dataType: 'html',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (html) {
                $('#captcha').html(html);
            }
        });
    });

    $('#reset-captcha').trigger('click');
</script>
</body>
</html>