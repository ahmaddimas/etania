<div class="page-title">
    <div>
        <h1>Dashboard</h1>
        <p>Dashboard &amp; control panel</p>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="card">
            <h3 class="card-title">Penjualan</h3>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tampilkan</label>
                            <div class="col-sm-9">
                                <select name="sales_range" class="form-control">
                                    <option value="week" selected="selected">Minggu Ini</option>
                                    <option value="month">Bulan Ini</option>
                                    <option value="year">Tahun Ini</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="area-legend"></div>
                    </div>
                </div>
                <div class="chart" id="sale">
                    <canvas id="lineChart" class="embed-responsive-item"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
                <h4>Pelanggan</h4>
                <p><span class="badge"><?= $count_customers ?></span></p>
            </div>
        </div>
        <div class="widget-small primary coloured-icon"><i class="icon fa fa-shopping-basket fa-3x"></i>
            <div class="info">
                <h4>Order</h4>
                <p><span class="badge"><?= $count_orders ?></span></p>
            </div>
        </div>
        <div class="widget-small primary coloured-icon"><i class="icon fa fa-shopping-basket fa-3x"></i>
            <div class="info">
                <h4>Order Pending</h4>
                <p><span class="badge"><?= $count_pending_orders ?></span></p>
            </div>
        </div>
        <div class="widget-small primary coloured-icon"><i class="icon fa fa-shopping-basket fa-3x"></i>
            <div class="info">
                <h4>Order Selesai</h4>
                <p><span class="badge"><?= $count_paid_orders ?></span></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <h3 class="card-title">Order</h3>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tampilkan</label>
                            <div class="col-sm-9">
                                <select name="orders_range" class="form-control">
                                    <option value="week" selected="selected">Minggu Ini</option>
                                    <option value="month">Bulan Ini</option>
                                    <option value="year">Tahun Ini</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="bar-legend"></div>
                    </div>
                </div>
                <div class="chart" id="order">
                    <canvas id="barChart" class="embed-responsive-item"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= base_url('assets/plugins/chart.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        getSalesChart();
        getOrdersChart();
    });

    $('select[name=\'sales_range\']').bind('change', function () {
        getSalesChart();
    });

    $('select[name=\'orders_range\']').bind('change', function () {
        getOrdersChart();
    });

    var salesData = {};
    var ordersData = {};

    function getSalesChart() {
        if (typeof salesChart === 'object') {
            salesChart.destroy();
        }

        $.ajax({
            url: "<?=admin_url('dashboard/chart_sales')?>",
            data: 'range=' + $('select[name=\'sales_range\'] option:selected').val(),
            dataType: 'json',
            success: function (d) {
                salesData = {
                    labels: d.labels,
                    datasets: d.datasets,
                };

                salesChart = new Chart($("#lineChart").get(0).getContext("2d")).Line(salesData, {
                    showScale: true,
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: false,
                    bezierCurve: true,
                    bezierCurveTension: 0.3,
                    pointDot: false,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: false,
                    responsive: true,
                    maintainAspectRatio: true,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li style=\"list-style:none; display:inline; float:left; padding:0 10px;\"><span style=\"color:<%=datasets[i].fillColor%>\" class=\"fa fa-circle\"></span> <%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                });

                $('#area-legend').html(salesChart.generateLegend());
            }
        });
    }

    function getOrdersChart() {
        if (typeof ordersChart === 'object') {
            ordersChart.destroy();
        }

        $.ajax({
            url: "<?=admin_url('dashboard/chart_orders')?>",
            data: 'range=' + $('select[name=\'orders_range\'] option:selected').val(),
            dataType: 'json',
            success: function (d) {
                ordersData = {
                    labels: d.labels,
                    datasets: d.datasets,
                };

                ordersChart = new Chart($("#barChart").get(0).getContext("2d")).Bar(ordersData, {
                    showScale: true,
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: false,
                    bezierCurve: true,
                    bezierCurveTension: 0.3,
                    pointDot: false,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: false,
                    responsive: true,
                    maintainAspectRatio: true,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li style=\"list-style:none; display:inline; float:left; padding:0 10px;\"><span style=\"color:<%=datasets[i].fillColor%>\" class=\"fa fa-circle\"></span> <%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                });

                $('#bar-legend').html(ordersChart.generateLegend());
            }
        });
    }
</script>