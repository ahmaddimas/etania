<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $heading_title ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="admin_group_id" value="<?= $admin_group_id ?>">
            <div class="form-group">
                <label class="control-label col-sm-3">Nama Grup</label>
                <div class=" col-sm-9">
                    <input type="text" class="form-control" name="name" value="<?= $name ?>">
                </div>
            </div>
            <div class="form-group" id="permission">
                <label class="control-label col-sm-3">Hak Akses</label>
                <div class=" col-sm-9">
                    <div>
                        <table class="table table-fixed" style="border:2px solid #ccc;">
                            <thead>
                            <tr>
                                <th style="width:38.9%;">Modul</th>
                                <th style="width:14.7%;" class="text-center"><a id="select-all-index"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top" title="Hak Baca"><i
                                                class="fa fa-eye"></i></a></th>
                                <th style="width:14.6%;" class="text-center"><a id="select-all-create"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top" title="Hak Tambah"><i
                                                class="fa fa-plus"></i></a></th>
                                <th style="width:14.5%;" class="text-center"><a id="select-all-edit"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Hak Modifikasi"><i
                                                class="fa fa-pencil"></i></a></th>
                                <th style="width:14.6%;" class="text-center"><a id="select-all-delete"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Hak Hapus"><i
                                                class="fa fa-trash"></i></a></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($permissions as $module => $paths) { ?>
                                <tr>
                                    <th style="width:100%;" colspan="6"><?= ucfirst($module) ?></th>
                                </tr>
                                <?php foreach ($paths as $path) { ?>
                                    <tr>
                                        <td style="width:40%;"><?= lang($path) ?></td>
                                        <td style="width:15%;" class="text-center">
                                            <?php if (isset($permission[$path]) && in_array('index', $permission[$path])) { ?>
                                                <input type="checkbox" name="permission[<?= $path ?>][index]"
                                                       class="check-index" value="1" checked="checked"/>
                                            <?php } else { ?>
                                                <input type="checkbox" name="permission[<?= $path ?>][index]"
                                                       class="check-index" value="1"/>
                                            <?php } ?>
                                        </td>
                                        <td style="width:15%;" class="text-center">
                                            <?php if (isset($permission[$path]) && in_array('create', $permission[$path])) { ?>
                                                <input type="checkbox" name="permission[<?= $path ?>][create]"
                                                       class="check-create" value="1" checked="checked"/>
                                            <?php } else { ?>
                                                <input type="checkbox" name="permission[<?= $path ?>][create]"
                                                       class="check-create" value="1"/>
                                            <?php } ?>
                                        </td>
                                        <td style="width:15%;" class="text-center">
                                            <?php if (isset($permission[$path]) && in_array('edit', $permission[$path])) { ?>
                                                <input type="checkbox" name="permission[<?= $path ?>][edit]"
                                                       class="check-edit" value="1" checked="checked"/>
                                            <?php } else { ?>
                                                <input type="checkbox" name="permission[<?= $path ?>][edit]"
                                                       class="check-edit" value="1"/>
                                            <?php } ?>
                                        </td>
                                        <td style="width:15%;" class="text-center">
                                            <?php if (isset($permission[$path]) && in_array('delete', $permission[$path])) { ?>
                                                <input type="checkbox" name="permission[<?= $path ?>][delete]"
                                                       class="check-delete" value="1" checked="checked"/>
                                            <?php } else { ?>
                                                <input type="checkbox" name="permission[<?= $path ?>][delete]"
                                                       class="check-delete" value="1"/>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
            <button type="button" class="btn btn-success" id="submit"><i class="fa fa-check"></i> Simpan</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('a').tooltip();
        var checks = ['index', 'create', 'edit', 'delete'];
        for (i in checks) {
            toggleCheck(checks[i]);
        }
    });

    function toggleCheck(x) {
        $('#select-all-' + x).click(function () {
            var checkBoxes = $('.check-' + x);
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });
    }

    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });
</script>