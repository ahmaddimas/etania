<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?= $heading_title ?></h4>
        </div>
        <div class="modal-body">
            <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
            <input type="hidden" name="admin_id" value="<?= $admin_id ?>">
            <div class="form-group">
                <label class="control-label col-sm-3">Nama</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="name" value="<?= $name ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Username</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="username" value="<?= $username ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Email</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="email" value="<?= $email ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Grup Admin</label>
                <div class="col-sm-9">
                    <?php if ($admin_id >= 0) { ?>
                        <select name="admin_group_id" class="form-control">
                            <?php foreach ($admin_groups as $admin_group) { ?>
                                <?php if ($admin_group['admin_group_id'] == $admin_group_id) { ?>
                                    <option value="<?= $admin_group['admin_group_id'] ?>"
                                            selected="selected"><?= $admin_group['name'] ?></option>
                                <?php } else { ?>
                                    <option value="<?= $admin_group['admin_group_id'] ?>"><?= $admin_group['name'] ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    <?php } else { ?>
                        <input type="text" class="form-control" value="Top Administrator" disabled="disabled">
                    <?php } ?>
                </div>
            </div>
            <?php if ($admin_id) { ?>
                <div class="alert alert-info">Biarka password kosong jika tidak ingin mengganti password</div>
            <?php } ?>
            <div class="form-group">
                <label class="control-label col-sm-3">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="password" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Ulangi Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="confirm" value="">
                </div>
            </div>
            <?php if ($admin_id >= 0) { ?>
                <div class="form-group">
                    <label class="control-label col-sm-3">Status</label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label>
                                <?php if ($active) { ?>
                                    <input type="checkbox" name="active" value="1" checked="checked"> Non Aktif
                                <?php } else { ?>
                                    <input type="checkbox" name="active" value="1"> Aktif
                                <?php } ?>
                            </label>
                        </div>
                    </div>
                </div>
            <?php } ?>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
            <button type="button" class="btn btn-success" id="submit"><i class="fa fa-check"></i> Simpan</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#form-modal').modal('hide');
                    parent.$('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                }
            }
        });
    });
</script>