<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= base_url('assets/css/main.css') ?>" rel="stylesheet">
    <link href="<?= $_favicon ?>" rel="shortcut icon">
    <title>Match Making</title>
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
    script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
    -->
</head>
<body>
<section class="material-half-bg">
    <div class="cover"></div>
</section>
<section class="login-content">
    <div class="logo"><h1>Match Making</h1></div>
    <div class="login-box">
        <?= form_open($action, 'class="login-form" id="form-reset"') ?>
        <h3 class="login-head">Change Password</h3>
        <p>Please enter your new password</p>
        <div class="form-group">
            <label class="control-label">Password</label>
            <input type="password" name="password" placeholder="New Password" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">Confirm Password</label>
            <input type="password" name="confirm" placeholder="Re-type new password" class="form-control">
        </div>
        <div class="form-group btn-container">
            <button class="btn btn-primary btn-block" id="button-reset">Submit <i class="fa fa-unlock fa-lg"></i>
            </button>
        </div>
        </form>
    </div>
</section>
<script src="<?= base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>
<script src="<?= base_url('assets/js/essential-plugins.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/js/plugins/pace.min.js') ?>"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script src="<?= base_url('assets/js/plugins/bootstrap-notify.min.js') ?>"></script>
<script type="text/javascript">
    $(document).delegate('#button-reset', 'click', function (e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            url: $('#form-reset').attr('action'),
            data: $('#form-reset').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                if (json['warning']) {
                    $.notify({
                        title: "Warning : ",
                        message: json['warning'],
                        icon: 'fa fa-ban'
                    }, {
                        type: "danger"
                    });
                } else if (json['redirect']) {
                    window.location = json['redirect'];
                }
            }
        });
    });
</script>
</body>
</html>