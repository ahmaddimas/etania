<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_auth
{
    private $ci;
    private $admin_id = null;
    private $name = '';
    private $group = '';
    private $email = '';
    private $image = null;
    private $permissions = array();

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->model('admin/admin_model');
        $this->ci->load->library('encrypt');
        $this->ci->load->helper('cookie');

        if ($admin = $this->ci->admin_model->get_admin($this->ci->session->userdata('admin_id'))) {
            $this->admin_id = (int)$admin['admin_id'];
            $this->name = $admin['name'];
            $this->group = $admin['group_name'];
            $this->email = $admin['email'];
            $this->image = $admin['image'];
            $this->permissions = unserialize($admin['permission']);
        } elseif ($admin = get_cookie('admin')) {
            $data = json_decode($this->ci->encrypt->decode($admin), true);

            $this->login($data['identity'], $data['password']);
        } else {
            $this->logout();
        }
    }

    /**
     * Do login
     *
     * @access public
     * @param string $string
     * @param string $password
     * @return void
     */
    public function login($string, $password, $remember = false)
    {
        if ($admin = $this->ci->admin_model->login($string, $password)) {
            $this->ci->session->set_userdata('admin_id', $admin['admin_id']);

            $this->ci->admin_model->update_last_login($admin['admin_id']);

            $this->admin_id = (int)$admin['admin_id'];
            $this->name = $admin['name'];
            $this->email = $admin['email'];
            $this->image = $admin['image'];

            if ($remember) {
                $data['identity'] = $admin['email'];
                $data['password'] = $password;

                set_cookie(array(
                    'name' => 'admin',
                    'value' => $this->ci->encrypt->encode(json_encode($data)),
                    'expire' => strtotime('+6 months'),
                ));
            }

            return true;
        }

        return false;
    }

    /**
     * Logout
     *
     * @access public
     * @return void
     */
    public function logout()
    {
        $this->ci->session->unset_userdata('admin_id');

        if (get_cookie('admin')) {
            delete_cookie('admin');
        }

        $this->admin_id = null;
        $this->name = '';
        $this->group = '';
        $this->email = '';
        $this->image = null;
        $this->permissions = array();
    }

    /**
     * Is admin logged in?
     *
     * @access public
     * @return bool
     */
    public function is_logged($admin_redirect = false)
    {
        if ($admin_redirect) {
            $this->ci->session->set_flashdata('admin_redirect', $admin_redirect);
        }

        return (bool)$this->admin_id;
    }

    /**
     * Get current admin id
     *
     * @access public
     * @return int
     */
    public function admin_id()
    {
        return (int)$this->admin_id;
    }

    /**
     * Get current admin name
     *
     * @access public
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * Get current admin email
     *
     * @access public
     * @return string
     */
    public function email()
    {
        return $this->email;
    }

    /**
     * Get admin group name
     *
     * @access public
     * @return void
     */
    public function group()
    {
        if ($this->admin_id < 0) {
            return 'Top Administrator';
        }

        return $this->group;
    }

    public function image()
    {
        if ($this->image == '') {
            $image = 'user.png';
        } else {
            $image = $this->image;
        }

        return $this->ci->image->resize($image, 50, 50);
    }

    /**
     * Check permission access
     *
     * @access public
     * @param $action
     * @param $permission
     * @return bool
     */
    public function has_permission($action = null, $permission = false)
    {
        if ($this->admin_id < 0) {
            return true;
        }

        if (!$action) {
            $action = $this->ci->router->fetch_method();
        }

        if (!$permission) {
            $permission = $this->ci->router->fetch_module() . '/' . strtolower($this->ci->router->fetch_class());
        }

        if (isset($this->permissions[$permission])) {
            if (in_array($action, $this->permissions[$permission])) {
                return true;
            }
        }

        return false;
    }
}