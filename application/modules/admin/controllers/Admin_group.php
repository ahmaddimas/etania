<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_group extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('admin_group_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    /**
     * Index admin groups data
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('ag.admin_group_id, ag.name, (SELECT COUNT(*) AS total FROM admin a WHERE a.admin_group_id = ag.admin_group_id) AS admins')
                ->from('admin_group ag');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load
                ->title('Grup Admin')
                ->view('admin/admin_group');
        }
    }

    /**
     * Create new admin group
     *
     * @access public
     * @return void
     */
    public function create()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('admin_error_create'))));
        }

        $this->load->vars(array('heading_title' => 'Add Admin Group'));

        $this->form();
    }

    /**
     * Edit existing admin group
     *
     * @access public
     * @param int $admin_group_id
     * @return void
     */
    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('admin_error_edit'))));
        }

        $this->load->vars(array('heading_title' => 'Edit Admin Group'));

        $this->form($this->input->get('admin_group_id'));
    }

    /**
     * Load admin group form
     *
     * @access private
     * @param int $admin_group_id
     * @return void
     */
    private function form($admin_group_id = null)
    {
        $data['action'] = admin_url('admin/group/validate');
        $data['admin_group_id'] = null;
        $data['name'] = '';

        $ignore = array(
            'customer/admin_address',
            'navigation/sidebar',
            'system/currencies'
        );

        $files = glob(APPPATH . '/modules/*/controllers/{admin/*,Admin,Admin_*}.php', GLOB_BRACE);

        foreach ($files as $file) {
            $file_path = str_replace(array('controllers/admin/', 'controllers/'), '', substr($file, strlen(APPPATH . 'modules/')));
            $xxx = explode('/', dirname($file_path));
            $module = end($xxx);
            $path = strtolower($module . '/' . basename($file_path, '.php'));

            if (!in_array($path, $ignore)) {
                $this->load->helper('language');
                $this->lang->load($module . '/module');
                $data['permissions'][$module][] = $path;
            }
        }

        $files = glob(FCPATH . '/addons/*/controllers/{admin/*,Admin,Admin_*}.php', GLOB_BRACE);

        foreach ($files as $file) {
            $file_path = str_replace(array('controllers/admin/', 'controllers/'), '', substr($file, strlen(FCPATH . 'addons/')));
            $xxx = explode('/', dirname($file_path));
            $module = end($xxx);
            $path = strtolower($module . '/' . basename($file_path, '.php'));

            if (!in_array($path, $ignore)) {
                $this->load->helper('language');
                $this->lang->load($module . '/module');
                $data['permissions'][$module][] = $path;
            }
        }

        if ($user_group = $this->admin_group_model->get($admin_group_id)) {
            $data['admin_group_id'] = (int)$user_group['admin_group_id'];
            $data['name'] = $user_group['name'];
            $data['permission'] = unserialize($user_group['permission']);
        }

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(null)->view('admin/admin_group_form', $data, true)
        )));
    }

    /**
     * Validate admin group form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $json = array();

        $admin_group_id = $this->input->post('admin_group_id');

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required|min_length[3]')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            $json['error']['name'] = form_error('name');
        } else {
            $data['admin_group_id'] = (int)$admin_group_id;
            $data['name'] = $this->input->post('name');
            $data['permission'] = array();

            if ($permission = $this->input->post('permission')) {
                foreach ($permission as $path => $action) {
                    $data['permission'][$path] = array_keys($action);
                }
            }

            $data['permission'] = serialize($data['permission']);

            if ($admin_group_id) {
                $this->admin_group_model->update($admin_group_id, $data);
                $json['success'] = lang('success_update');
            } else {
                if ($this->admin_group_model->insert($data)) {
                    $json['success'] = lang('success_create');
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Delete admin group
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        $json = array();

        $this->load->model('admin_model');

        $admin_group_ids = $this->input->post('admin_group_id') ? $this->input->post('admin_group_id') : array();

        if (!$admin_group_ids) {
            $json['error'] = lang('error_selected');
        }

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        } else {
            foreach ($admin_group_ids as $admin_group_id) {
                if ($this->admin_model->check_admin_group($admin_group_id)) {
                    $json['error'] = 'Gagal menghapus grup admin karena masih digunakan oleh satu atau lebih admin!';
                }
            }
        }

        if (empty($json['error'])) {
            $this->admin_group_model->delete($admin_group_id);
            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
} 