<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array();

        $data['count_customers'] = $this->db->where('active', 1)->count_all_results('customer');

        $data['count_orders'] = $this->db
            ->select('COUNT(DISTINCT(order_id)) as total', false)
            ->from('order')
            ->where('SUBSTRING_INDEX(date_added,"-",1) = "' . date('Y', time()) . '"', null, false)
            ->get()->row()->total;

        $data['count_pending_orders'] = $this->db
            ->select('COUNT(DISTINCT(order_id)) as total', false)
            ->from('order')
            ->where('SUBSTRING_INDEX(date_added,"-",1) = "' . date('Y', time()) . '"', null, false)
            ->where('order_status_id', (int)$this->config->item('order_status_id'))
            ->get()->row()->total;

        $data['count_paid_orders'] = $this->db
            ->select('COUNT(DISTINCT(order_id)) as total', false)
            ->from('order')
            ->where('SUBSTRING_INDEX(date_added,"-",1) = "' . date('Y', time()) . '"', null, false)
            ->where('order_status_id', (int)$this->config->item('order_complete_status_id'))
            ->get()->row()->total;

        $this->load
            ->title('Dashboard')
            ->js('/assets/plugins/chart.js')
            ->view('admin/dashboard', $data);
    }

    public function chart_sales()
    {
        $this->load->model('order/order_model');

        $json = array();

        if ($this->input->get('range')) {
            $range = $this->input->get('range');
        } else {
            $range = 'week';
        }

        $params = $this->set_params($range);

        if (!$params) {
            return false;
        }

        $sale = $this->order_model->get_chart_sales($params);

        foreach ($params['labels'] as $key => $label) {
            if (isset($sale[$key])) {
                $sales[$key] = $sale[$key];
            } else {
                $sales[$key] = 0;
            }
        }

        $json['labels'] = array_values($params['labels']);
        $json['datasets'] = array(
            array(
                'label' => 'Sales (Rp)',
                'fillColor' => 'rgba(255, 190, 10, 1)',
                'strokeColor' => 'rgba(189, 138, 0, 1)',
                'pointColor' => 'rgba(189, 138, 0, 1)',
                'pointStrokeColor' => '#fff',
                'pointHighlightFill' => '#fff',
                'pointHighlightStroke' => 'rgba(189, 138, 0, 1)',
                'data' => array_values($sales)
            )
        );

        $this->output->set_output(json_encode($json));
    }

    protected function set_params($range)
    {
        switch ($range) {
            case 'week':
                $params['group'] = 'day';
                $params['start'] = date('Y-m-d H:i:s', strtotime('monday this week'));
                $params['end'] = date('Y-m-d H:i:s', strtotime('sunday this week') + 86340);

                $iStart = (int)date('d', strtotime($params['start']));
                $iEnd = (int)date('d', strtotime($params['end']));
                $days = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');

                $j = 0;

                for ($i = $iStart; $i <= $iEnd; $i++) {
                    $params['labels'][$i] = (string)$days[$j];
                    $j++;
                }

                return $params;
                break;

            case 'month':
                $params['group'] = 'day';
                $params['start'] = date('Y-m-d H:i:s', strtotime('first day of this month'));
                $params['end'] = date('Y-m-d H:i:s', strtotime('last day of this month'));

                $iStart = (int)date('d', strtotime($params['start']));
                $iEnd = (int)date('d', strtotime($params['end']));

                for ($i = $iStart; $i <= $iEnd; $i++) {
                    $params['labels'][$i] = (string)$i;
                }

                return $params;
                break;

            case 'year':
                $params['group'] = 'month';
                $params['start'] = date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, date('Y', time())));
                $params['end'] = date('Y-m-d H:i:s', mktime(23, 59, 0, 12, 31, date('Y', time())));

                $iStart = (int)date('m', strtotime($params['start']));
                $iEnd = (int)date('m', strtotime($params['end']));
                $months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Des');

                $j = 0;

                for ($i = $iStart; $i <= $iEnd; $i++) {
                    $params['labels'][$i] = (string)$months[$j];
                    $j++;
                }

                return $params;
                break;

            default:
                return false;
                break;
        }
    }

    public function chart_orders()
    {
        $this->load->model('order/order_model');

        $json = array();

        if ($this->input->get('range')) {
            $range = $this->input->get('range');
        } else {
            $range = 'week';
        }

        $params = $this->set_params($range);

        if (!$params) {
            return false;
        }

        // pending
        $pending = $this->order_model->get_chart_orders(array(
            'order_status_id' => $this->config->item('order_status_id'),
        ), $params);

        // complete
        $completed = $this->order_model->get_chart_orders(array(
            'order_status_id' => $this->config->item('order_complete_status_id'),
        ), $params);

        foreach ($params['labels'] as $key => $label) {
            if (isset($pending[$key])) {
                $delivers[$key] = $pending[$key];
            } else {
                $delivers[$key] = 0;
            }
        }

        foreach ($params['labels'] as $key => $label) {
            if (isset($completed[$key])) {
                $completes[$key] = $completed[$key];
            } else {
                $completes[$key] = 0;
            }
        }

        $json['labels'] = array_values($params['labels']);
        $json['datasets'] = array(
            array(
                'label' => 'Pending',
                'fillColor' => 'rgba(255, 15, 15, 1)',
                'strokeColor' => 'rgba(173, 0, 0, 1)',
                'pointColor' => 'rgba(173, 0, 0, 1)',
                'pointStrokeColor' => '#fff',
                'pointHighlightFill' => '#fff',
                'pointHighlightStroke' => 'rgba(173, 0, 0, 1)',
                'data' => array_values($delivers)
            ),
            array(
                'label' => 'Completed',
                'fillColor' => 'rgba(26, 255, 0, 0.81)',
                'strokeColor' => 'rgba(14, 138, 0, 0.81)',
                'pointColor' => 'rgba(14, 138, 0, 0.81)',
                'pointStrokeColor' => '#fff',
                'pointHighlightFill' => '#fff',
                'pointHighlightStroke' => 'rgba(14, 138, 0, 0.81)',
                'data' => array_values($completes)
            )
        );

        $this->output->set_output(json_encode($json));
    }
}