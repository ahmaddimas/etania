<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('admin_model');

        $this->form_validation->CI =& $this;
    }

    /**
     * Request form
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $json = array();

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->form_validation
                ->set_rules('email', 'Email', 'trim|required|valid_email|callback__check_email')
                ->set_rules('captcha', 'Captcha', 'trim|required|callback__check_captcha')
                ->set_error_delimiters('', '');

            if ($this->form_validation->run() == true) {
                if ($code = $this->admin_model->request_reset_password($this->input->post('email'))) {
                    $this->load->library('email');
                    $this->email->initialize();

                    $admin = $this->db->where('code_forgotten', $code)->get('admin')->row_array();

                    $data['name'] = $admin ? $admin['name'] : 'Guys';
                    $data['title'] = 'Password Recovery';
                    $data['continue'] = admin_url('reset/change_password?code=' . $code);
                    $data['cancel'] = admin_url('reset/cancel?code=' . $code);

                    $this->email->from($this->config->item('smtp_email'), $this->config->item('company'));
                    $this->email->to($this->input->post('email'));
                    $this->email->subject($data['title']);
                    $this->email->message($this->load->view('admin/reset_password_email', $data, true));

                    $this->session->set_flashdata('message', 'Please check your mailbox. We have sent an email with instructions on how to reset your password.');

                    if ($this->email->send()) {
                        $this->session->set_flashdata('message', 'Please check your mailbox. We have sent an email with instructions on how to reset your password.');
                    } else {
                        $this->session->set_flashdata('message', 'Failed when sending email. Please contact your system administrator!');
                    }
                } else {
                    $this->session->set_flashdata('message', 'Failed. Please contact your system administrator!');
                }

                $json['redirect'] = admin_url('reset');
            } else {
                $json['warning'] = validation_errors();
            }

            $this->output->set_output(json_encode($json));
        } else {
            if ($this->session->flashdata('message')) {
                $data['message'] = $this->session->flashdata('message');
            } else {
                $data['message'] = false;
            }

            $this->load->layout(false)->view('admin/reset', $data);
        }
    }

    /**
     * Check email callback
     *
     * @access public
     * @param string $email
     * @return bool
     */
    public function _check_email($email)
    {
        if (!$this->admin_model->check_email($email)) {
            $this->form_validation->set_message('_check_email', 'Email not found!');
            return false;
        }

        return true;
    }

    public function _check_captcha($word)
    {
        $captcha = $this->session->userdata('captcha');

        if (isset($captcha['word'])) {
            if ($captcha['word'] == $word) {
                return true;
            } else {
                $this->form_validation->set_message('_check_captcha', 'No matches captcha!');
                return false;
            }
        } else {
            $this->form_validation->set_message('_check_captcha', 'No matches captcha!');
            return false;
        }
    }

    /**
     * Change reset password
     *
     * @access public
     * @return void
     */
    public function change_password()
    {
        $code = $this->input->get('code');

        if (is_null($code)) {
            show_404();
        }

        $json = array();

        $admin = $this->db->where('code_forgotten', $code)->get('admin')->row_array();

        if ($admin) {
            $this->form_validation
                ->set_rules('password', 'Password', 'trim|required|min_length[8]')
                ->set_rules('confirm', 'Password Confirmation', 'trim|required|matches[password]')
                ->set_error_delimiters('', '');

            if ($this->form_validation->run() == true) {
                $new_password = $this->input->post('password');
                $old_password = $this->admin_model->reset_password($code);

                $this->admin_model->change_password($admin['admin_id'], $old_password, $new_password);

                if ($this->admin_auth->login($admin['email'], $new_password)) {
                    $redirect = admin_url();
                } else {
                    $redirect = admin_url('login');
                }

                if ($this->input->is_ajax_request()) {
                    $json['redirect'] = $redirect;
                } else {
                    redirect($redirect);
                }
            } else {
                $error = validation_errors();

                if ($this->input->is_ajax_request()) {
                    $json['warning'] = $error;
                } else {
                    $data['action'] = admin_url('reset/change_password?code=' . $code);
                    $data['warning'] = $error;

                    $this->load->layout(false)->view('admin/change_password', $data);
                }
            }
        } else {
            if ($this->input->is_ajax_request()) {
                $json['error'] = 'Bad request!';
            } else {
                show_404();
            }
        }

        if ($this->input->is_ajax_request()) {
            $this->output->set_output(json_encode($json));
        }
    }

    /**
     * Cancel reset password
     *
     * @access public
     * @param string $code
     * @return bool
     */
    public function cancel()
    {
        $code = $this->input->get('code');

        $this->admin_model->cancel_reset_password($code);

        $this->session->set_flashdata('message', 'Password reset request has been canceled!');

        redirect(admin_url('login'));
    }
}