<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Logout class
 *
 * @author    Adi Setiawan
 * @link    mas.adisetiawan@gmail.com
 */
class Logout extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Administrator logout
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->admin_auth->logout();
        $this->session->set_flashdata('message', 'Anda telah berhasil keluar!');
        redirect(PATH_ADMIN . '/login', 'refresh');
    }
}