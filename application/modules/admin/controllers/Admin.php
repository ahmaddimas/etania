<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller
{
    private $admin_id;
    private $username;
    private $email;

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('admin_model');
        $this->load->model('admin_group_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->CI =& $this;
    }

    /**
     * Index admins data
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('admin.admin_id, admin.name, admin.email, admin.active, (case when(admin.admin_id < 0) then "Top Administrator" else admin_group.name end) as group_name')
                ->from('admin')
                ->join('admin_group', 'admin_group.admin_group_id = admin.admin_group_id', 'left');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load
                ->title('Admin')
                ->view('admin/admin');
        }
    }

    /**
     * Create new admin
     *
     * @access public
     * @return void
     */
    public function create()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('admin_error_create'))));
        }

        $this->load->vars(array('heading_title' => 'Tambah Admin'));

        $this->form();
    }

    /**
     * Edit existing admin
     *
     * @access public
     * @param int $admin_id
     * @return void
     */
    public function edit()
    {
        check_ajax();

        if (!$this->admin_auth->has_permission()) {
            return $this->output->set_output(json_encode(array('error' => lang('admin_error_edit'))));
        }

        $this->load->vars(array('heading_title' => 'Edit Admin'));

        $this->form($this->input->get('admin_id'));
    }

    /**
     * Load admin form
     *
     * @access private
     * @param int $admin_id
     * @return void
     */
    private function form($admin_id = null)
    {
        $data['action'] = admin_url('admin/validate');
        $data['admin_id'] = null;
        $data['name'] = '';
        $data['username'] = '';
        $data['email'] = '';
        $data['admin_group_id'] = null;
        $data['active'] = null;
        $data['admin_groups'] = $this->admin_group_model->get_all();

        if ($admin = $this->admin_model->get($admin_id)) {
            $data['admin_id'] = (int)$admin['admin_id'];
            $data['name'] = $admin['name'];
            $data['username'] = $admin['username'];
            $data['email'] = $admin['email'];
            $data['admin_group_id'] = (int)$admin['admin_group_id'];
            $data['active'] = (bool)$admin['active'];
        }

        $this->output->set_output(json_encode(array(
            'content' => $this->load->layout(false)->view('admin/admin_form', $data, true)
        )));
    }

    /**
     * Validate admin form
     *
     * @access public
     * @return json
     */
    public function validate()
    {
        check_ajax();

        $json = array();

        $admin_id = $this->input->post('admin_id');

        if ($admin = $this->admin_model->get($admin_id)) {
            $this->admin_id = (int)$admin['admin_id'];
            $this->username = $admin['username'];
            $this->email = $admin['email'];
        }

        if ($this->input->post('password') !== '' || !$admin_id) {
            $this->form_validation
                ->set_rules('password', 'Password', 'trim|required|min_length[6]')
                ->set_rules('confirm', 'Konfirmasi Password', 'trim|required|matches[password]');
        }

        $this->form_validation
            ->set_rules('name', 'Nama', 'trim|required')
            ->set_rules('username', 'Username', 'trim|required|callback__check_username')
            ->set_rules('email', 'Email', 'trim|required|valid_email|callback__check_email')
            ->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            if ($this->input->post('password') !== '' || !$admin_id) {
                if (form_error('password') !== '') {
                    $json['error']['password'] = form_error('password');
                }

                if (form_error('confirm') !== '' || !$admin_id) {
                    $json['error']['confirm'] = form_error('confirm');
                }
            }

            foreach (array('name', 'username', 'email') as $field) {
                if (form_error($field) !== '') {
                    $json['error'][$field] = form_error($field);
                }
            }
        } else {
            $data['admin_id'] = (int)$admin_id;
            $data['name'] = $this->input->post('name');
            $data['username'] = $this->input->post('username');
            $data['email'] = strtolower($this->input->post('email'));
            $data['password'] = $this->input->post('password');
            $data['admin_group_id'] = (int)$this->input->post('admin_group_id');

            if ($admin_id !== $this->admin_auth->admin_id() && $admin_id >= 0) {
                $data['active'] = (bool)$this->input->post('active');
            }

            if ($admin_id) {
                if ($this->admin_model->update_admin($admin_id, $data)) {
                    $json['success'] = lang('success_update');
                }
            } else {
                $data['photo'] = 'admin.png';

                if ($this->admin_model->create_admin($data)) {
                    $json['success'] = lang('success_create');
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Callback check username
     *
     * @access public
     * @param string $username
     * @return bool
     */
    public function _check_username($username)
    {
        if ($this->admin_model->check_username($username) && $username !== $this->username) {
            $this->form_validation->set_message('_check_username', 'Username tidak tersedia!');
            return false;
        }

        return true;
    }

    /**
     * Callback check username
     *
     * @access public
     * @param string $username
     * @return bool
     */
    public function _check_email($email)
    {
        if ($this->admin_model->check_email($email) && $email !== $this->email) {
            $this->form_validation->set_message('_check_email', 'Email ini sudah digunakan!');
            return false;
        }

        return true;
    }

    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        $admin_ids = $this->input->post('admin_id') ? $this->input->post('admin_id') : array();

        if (!$admin_ids) {
            $json['error'] = lang('error_selected');
        }

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        } else {
            foreach ($admin_ids as $admin_id) {
                if ($admin_id < 0) {
                    $json['error'] = 'Anda tidak dapat menghapus Top Administrator!';
                    break;
                } elseif ($admin_id == $this->admin_auth->admin_id()) {
                    $json['error'] = 'Anda tidak dapat menghapus akun anda sendiri!';
                    break;
                }
            }
        }

        if (empty($json['error'])) {
            foreach ($admin_ids as $admin_id) {
                $this->admin_model->delete($admin_id);
                $json['success'] = lang('success_delete');
            }
        }

        $this->output->set_output(json_encode($json));
    }
} 