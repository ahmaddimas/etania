<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Front_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('page_model');
    }

    public function index($slug = false)
    {
        if ($page = $this->page_model->get_page($slug)) {
            foreach ($page as $key => $value) {
                $data[$key] = $value;
            }

            $data['breadcrumbs'][] = array(
                'text' => 'Home',
                'href' => site_url()
            );

            $data['breadcrumbs'][] = array(
                'text' => $data['title'],
                'href' => site_url('page/' . $data['slug'])
            );

            $this->load
                ->title($page['title'])
                ->metadata('description', $page['meta_description'])
                ->metadata('keywords', $page['meta_keyword'])
                ->view('page', $data);
        } else {
            show_404();
        }
    }

    public function home()
    {
        $data = array();
        $this->load->model('catalog/category_model');

        $data['categories'] = array();

        foreach ($this->category_model->get_subcategories(0) as $category) {
            if ($category['top']) {
                $data['categories'][] = array(
                    'name' => $category['name'],
                    'href' => site_url('category/' . $category['slug']),
                    'menu_image' => $category['menu_image']
                );
            }
        }

        $this->load->model('banner/banner_model');

        $data['banners'] = array();
        $data['top_banners'] = array();
        $data['footer_banners'] = array();
        $data['header_banners'] = array();

        $banners = $this->banner_model->get_images($this->config->item('slideshow_banner_id'));

        foreach ($banners as $banner) {
            if ($banner['active']) {
                if ($banner['image']) {
                    $image = $banner['image'];
                } else {
                    $image = 'no_image.jpg';
                }

                $data['banners'][] = array(
                    'image' => $this->image->resize($banner['image'], 1440),
                    'title' => $banner['title'],
                    'subtitle' => $banner['subtitle'],
                    'link' => $banner['link'],
                    'link_title' => $banner['link_title'],
                );
            }
        }

        $top_banners = $this->banner_model->get_images($this->config->item('top_banner_id'));

        foreach ($top_banners as $banner) {
            if ($banner['active']) {
                if ($banner['image']) {
                    $image = $banner['image'];
                } else {
                    $image = 'no_image.jpg';
                }

                $data['top_banners'][] = array(
                    'image' => $this->image->resize($banner['image'], 555, 180),
                    'title' => $banner['title'],
                    'subtitle' => $banner['subtitle'],
                    'link' => $banner['link'],
                    'link_title' => $banner['link_title'],
                );
            }
        }

        $footer_banners = $this->banner_model->get_images($this->config->item('footer_banner_id'));

        foreach ($footer_banners as $banner) {
            if ($banner['active']) {
                if ($banner['image']) {
                    $image = $banner['image'];
                } else {
                    $image = 'no_image.jpg';
                }

                $data['footer_banners'][] = array(
                    'image' => $this->image->resize($banner['image'], 555, 180),
                    'title' => $banner['title'],
                    'subtitle' => $banner['subtitle'],
                    'link' => $banner['link'],
                    'link_title' => $banner['link_title'],
                );
            }
        }


        $header_banners_active = $this->banner_model->get($this->config->item('header_banner_id'));
        $header_banners = $this->banner_model->get_images($this->config->item('header_banner_id'));

        if ($header_banners_active['active']) {
            foreach ($header_banners as $banner) {
                if ($banner['active']) {
                    if ($banner['image']) {
                        $image = $banner['image'];
                    } else {
                        $image = 'no_image.jpg';
                    }

                    $data['header_banners'][] = array(
                        'image' => $this->image->resize($banner['image'], 130, 30),
                        'title' => $banner['title'],
                        'subtitle' => $banner['subtitle'],
                        'link' => $banner['link'],
                        'link_title' => $banner['link_title'],
                    );
                }
            }
        }

        $data['latest'] = $this->_latest();

        //addon freeshipping logo
        $data['free_shipping'] = $this->config->item('addon_freeshipping');

        $this->load
            ->title($this->config->item('seo_title'), $this->config->item('site_name'))
            ->metadata('description', $this->config->item('meta_description'))
            ->metadata('keyword', $this->config->item('meta_keywords'))
            ->view('home', $data);
    }

    private function _latest()
    {
        $this->load->library('currency');
        $this->load->model('catalog/product_model');
        $this->load->model('catalog/category_model');

        $products = array();

        $params = array(
            'start' => 0,
            'limit' => 8,
            'order' => 'desc',
            'sort' => 'date_added'
        );

        $pwd = $this->config->item('image_product_width');
        $phg = $this->config->item('image_product_height');

        //addons free shipping
        $freeshipping = 0;
        //

        foreach ($this->product_model->get_products($params) as $product) {

            //addon freeshipping
            $addon_offers = $this->config->item('addon_freeshipping');
            if (!empty($addon_offers['active'])) {
                $this->load->model('freeshipping/product_free_shipping_model');
                $ct = $this->product_free_shipping_model->cek_category($product['category_id']);
                $pr = $this->product_free_shipping_model->cek_product($product['product_id']);

                if (count($ct) <> 0 OR count($pr) <> 0) {
                    $freeshipping = 1;
                } else {
                    $freeshipping = 0;
                }
            }
            //


            $path = 'product/';

            if ($category = $this->category_model->get_category($product['category_id'])) {
                $path_ids = explode('-', $category['path_id']);

                foreach ($path_ids as $path_id) {
                    if ($subcategory = $this->category_model->get_category($path_id)) {
                        $path .= $subcategory['slug'] . '/';
                    }
                }

                $path .= $category['slug'] . '/';
            }

            $path .= $product['slug'];

            $products[] = array(
                'product_id' => $product['product_id'],
                'thumb' => $this->image->resize($product['image'] ? $product['image'] : 'no_image.jpg', $pwd, $phg),
                'name' => (strlen($product['name']) <= 48) ? $product['name'] : substr(strip_tags(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')), 0, ($this->is_mobile ? 28 : 48)) . '...',
                'description' => substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '...',
                'price' => $this->currency->format($product['price']),
                'discount' => (float)$product['discount'] ? $this->currency->format($product['discount']) : false,
                'wholesaler' => ($product['wholesaler'] > 1) ? true : false,
                'discount_percent' => (float)$product['discount'] ? ceil(($product['price'] - $product['discount']) / $product['price'] * 100) . '%' : false,
                'rating' => $product['rating'],
                'href' => site_url($path),
                'quickview' => site_url('catalog/product/quickview/' . $product['product_id']),
                'free_shipping' => $freeshipping
            );
        }

        return $products;
    }

    /**
     * Page missing
     *
     * @access public
     * @return void
     */
    public function missing()
    {
        $this->output->set_status_header('404');

        $this->load
            ->title('Page Not Found')
            ->view('page_missing');
    }
}