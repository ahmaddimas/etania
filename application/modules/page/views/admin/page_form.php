<div class="page-title">
    <div>
        <h1><?= $heading_title ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('page') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>
        <a id="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?= form_open($action, 'id="form" class="form-horizontal" role="form"') ?>
                <input type="hidden" name="page_id" value="<?= $page_id ?>">
                <div id="message"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Judul Halaman</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" value="<?= $title ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Konten</label>
                    <div class="col-sm-10">
                        <textarea name="content" id="content" class="form-control summernote"><?= $content ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Meta Deskripsi</label>
                    <div class="col-sm-10">
                        <textarea name="meta_description" class="form-control"><?= $meta_description ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Meta Keywords</label>
                    <div class="col-sm-10">
                        <input type="text" name="meta_keyword" value="<?= $meta_keyword ?>" class="form-control"
                               placeholder="Pisahkan setiap phrase dengan tanda koma(,)">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Urutan</label>
                    <div class="col-sm-2">
                        <input type="text" name="sort_order" value="<?= $sort_order ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="toggle lg col-sm-4">
                        <label style="margin-top:5px;">
                            <?php if ($active) { ?>
                                <input type="checkbox" name="active" value="1" checked="checked">
                            <?php } else { ?>
                                <input type="checkbox" name="active" value="1">
                            <?php } ?>
                            <span class="button-indecator"></span>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kolom Footer</label>
                    <div class="col-sm-2">
                        <select class="form-control" name="footer_column">
                            <?php for ($i = 1; $i <= 4; $i++) { ?>
                                <?php if ($i == $footer_column) { ?>
                                    <option value="<?= $i ?>" selected="selected"><?= $i ?></option>
                                <?php } else { ?>
                                    <option value="<?= $i ?>"><?= $i ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').parent().addClass('has-error');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('page')?>";
                }
            }
        });
    });
</script>