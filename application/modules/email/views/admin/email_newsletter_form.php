<div class="page-title">
    <div>
        <h1><?= $heading_title ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('email/newsletter') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>
        <a id="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?= form_open($action, 'id="form" class="form-horizontal" role="form"') ?>
                <input type="hidden" name="email_id" value="<?= $email_id ?>">
                <div id="message"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Template</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" value="<?= $name ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Body</label>
                    <div class="col-sm-10">
                        <textarea name="body" id="body" class="form-control"><?= $body ?></textarea>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').parent().addClass('has-error');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('email/newsletter')?>";
                }
            }
        });
    });
</script>