<div class="page-title">
    <div>
        <h1>Template Newsletter</h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('email/newsletter/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <table class="table table-hover table-bordered" id="datatable" width="100%">
                    <thead>
                    <tr>
                        <th>Judul</th>
                        <th>Tanggal Update</th>
                        <th class="text-right"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('email/newsletter')?>",
                "type": "POST",
            },
            "columns": [
                {"data": "name"},
                {"data": "date_modified"},
                {"orderable": false, "searchable": false, "data": "email_id"},
            ],
            "createdRow": function (row, data, index) {
                html = '<div class="btn-group btn-group-xs">';
                html += '<a href="<?=admin_url('email/newsletter/edit')?>/' + data.email_id + '" class="btn btn-primary btn-xs"><i class="fa fa-cog"></i> Edit</a>';
                html += '<a onclick="delRecord(' + data.email_id + ');" class="btn btn-warning btn-xs"><i class="fa fa-minus-circle"></i> Hapus</a> ';
                html += '</div>';
                $('td', row).eq(2).addClass('text-right').html(html);
            },
            "order": [[2, 'desc']],
        });
    });

    function delRecord(email_id) {
        swal({
            title: "Apakah anda yakin?",
            text: "Data yang sudah dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('email/newsletter/delete')?>',
                    type: 'post',
                    data: 'email_id=' + email_id,
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal("Terhapus!", json['success'], "success");
                        } else if (json['error']) {
                            swal("Error!", json['error'], "error");
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                        refreshTable();
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }
</script>