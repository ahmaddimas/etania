<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_notification extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Notification email template
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('email_id, name, date_modified')
                ->where('type', 'notication')
                ->from('email')
                ->edit_column('date_modified', '$1', 'format_date(date_modified, true)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load->view('admin/email_notication');
        }
    }

    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars('heading_title', 'Tambah Halaman');

        $this->form();
    }

    public function edit($email_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars('heading_title', 'Edit Halaman');

        $this->form($email_id);
    }

    public function form($email_id = null)
    {
        $this->load->helper('form');


        $this->load->model('email_model');

        $data['action'] = admin_url('email/validate');
        $data['email_id'] = null;
        $data['title'] = '';
        $data['content'] = '';
        $data['description'] = '';
        $data['meta_description'] = '';
        $data['meta_keyword'] = '';
        $data['sort_order'] = 0;
        $data['footer_column'] = 0;
        $data['active'] = 0;

        if ($email = $this->email_model->get($email_id)) {
            foreach ($email as $key => $val) {
                $data[$key] = $val;
            }
        }

        $this->load->view('admin/email_form', $data);
    }

    public function validate()
    {
        $this->load->library('form_validation');
        $this->load->model('email_model');

        $this->form_validation
            ->set_rules('title', 'Judul Halaman', 'trim|required|min_length[3]')
            ->set_rules('content', 'Konten', 'trim|required|min_length[3]')
            ->set_error_delimiters('', '');

        $json = array();

        if ($this->form_validation->run() === false) {
            foreach (array('title', 'content') as $field) {
                if (form_error($field) != '') {
                    $json['error'][$field] = form_error($field);
                }
            }
        } else {
            $email_id = $this->input->post('email_id');

            $post['title'] = $this->input->post('title');
            $post['content'] = $this->input->post('content');
            $post['meta_description'] = $this->input->post('meta_description');
            $post['meta_keyword'] = $this->input->post('meta_keyword');
            $post['sort_order'] = (int)$this->input->post('sort_order');
            $post['footer_column'] = (int)$this->input->post('footer_column');
            $post['active'] = (bool)$this->input->post('active');

            if ((bool)$email_id) {
                $this->email_model->edit($email_id, $post);
                $json['success'] = 'Data Halaman telah berhasil diperbarui.';
            } else {
                $this->email_model->create($post);
                $json['success'] = 'Data Halaman telah berhasil ditambahkan.';
            }
        }

        $this->output->set_output(json_encode($json));
    }

    public function delete()
    {
        check_ajax();

        $json = array();

        $email_id = $this->input->post('email_id');

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('email_model');

            $this->email_model->delete($email_id);

            $json['success'] = 'Halaman telah terhapus!';
        }

        $this->output->set_output(json_encode($json));
    }
} 