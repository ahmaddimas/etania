<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_newsletter extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Newsletter email template
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');
            $this->load->helper('format');

            $this->datatables
                ->select('email_id, name, date_modified')
                ->where('type', 'newsletter')
                ->from('email')
                ->edit_column('date_modified', '$1', 'format_date(date_modified, true)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load->view('admin/email_newsletter');
        }
    }

    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars('heading_title', 'Tambah Template');

        $this->form();
    }

    public function edit($email_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars('heading_title', 'Edit Template');

        $this->form($email_id);
    }

    public function form($email_id = null)
    {
        $this->load->helper('form');
        $this->load->model('email_model');

        $data['action'] = admin_url('email/validate');
        $data['email_id'] = null;
        $data['name'] = '';
        $data['body'] = '';

        if ($email = $this->email_model->get($email_id)) {
            foreach ($email as $key => $val) {
                $data[$key] = $val;
            }
        }

        $this->load->view('admin/email_newsletter_form', $data);
    }

    public function validate()
    {
        $this->load->library('form_validation');
        $this->load->model('email_model');

        $this->form_validation
            ->set_rules('name', 'Judul Template', 'trim|required|min_length[3]')
            ->set_rules('body', 'Body', 'trim|required|min_length[3]')
            ->set_error_delimiters('', '');

        $json = array();

        if ($this->form_validation->run() === false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['error'][$field] = $error;
            }
        } else {
            $email_id = $this->input->post('email_id');

            $post['name'] = $this->input->post('name');
            $post['body'] = $this->input->post('body');
            $post['type'] = $this->input->post('type');

            if ((bool)$email_id) {
                $this->email_model->edit($email_id, $post);
                $json['success'] = lang('success_update');
            } else {
                $this->email_model->create($post);
                $json['success'] = lang('success_create');
            }
        }

        $this->output->set_output(json_encode($json));
    }

    public function delete()
    {
        check_ajax();

        $json = array();

        $email_id = $this->input->post('email_id');

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('email_model');

            $this->email_model->delete($email_id);

            $json['success'] = lang('success_delete');
        }

        $this->output->set_output(json_encode($json));
    }
} 