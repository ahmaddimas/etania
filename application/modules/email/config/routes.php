<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['email/admin/newsletter(:any)?'] = 'admin_newsletter$1';
$route['email/admin/notification(:any)?'] = 'admin_notofication$1';