<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');

        $this->load->helper('form');
        $this->load->helper('language');

        $this->load->model('menu_model');
    }

    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('menu_id, title, sort_order, type, active, slug, page_id, icon')
                ->where('sub_id', 0)
                ->from('menu');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load->view('admin/menu');
        }
    }

    public function create()
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars('heading_title', 'Tambah Menu');

        $this->form();
    }

    public function edit($page_id = null)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars('heading_title', 'Edit Menu');

        $this->form($page_id);
    }

    public function form($menu_id = null)
    {
        $this->load->helper('form');


        $this->load->model('menu_model');

        $data['action'] = admin_url('menu/validate');
        $data['menu_id'] = null;
        $data['sub_id'] = 0;
        $data['icon'] = 'fa-chevron-right';
        $data['title'] = '';
        $data['type'] = '';
        $data['page_id'] = '';
        $data['slug'] = '';
        $data['sort_order'] = 0;
        $data['active'] = 0;

        if ($page = $this->menu_model->get($menu_id)) {
            foreach ($page as $key => $val) {
                $data[$key] = $val;
            }
        }

        $this->load->view('admin/menu_form', $data);
    }

    public function validate()
    {
        $this->load->library('form_validation');
        $this->load->model('menu_model');

        $this->form_validation
            ->set_rules('title', 'Menu', 'trim|required|min_length[3]')
            ->set_rules('icon', 'Icon', 'trim|required|min_length[3]')
            ->set_rules('type', 'Type', 'required')
            ->set_error_delimiters('', '');

        $json = array();

        if ($this->form_validation->run() === false) {
            foreach (array('title', 'icon', 'type') as $field) {
                if (form_error($field) != '') {
                    $json['error'][$field] = form_error($field);
                }
            }
        } else {
            $menu_id = $this->input->post('menu_id');

            $post['icon'] = $this->input->post('icon');
            $post['title'] = $this->input->post('title');
            $post['type'] = $this->input->post('type');
            $post['sort_order'] = (int)$this->input->post('sort_order');
            $post['active'] = (bool)$this->input->post('active');

            if ($this->input->post('type') == 0) {
                $post['page_id'] = $this->input->post('page');
                $pgs = $this->db
                    ->select('slug')
                    ->where('page_id', $this->input->post('page'))
                    ->where('active', 1)
                    ->get('page')
                    ->row();

                $post['slug'] = 'page/' . $pgs->slug;
            } elseif ($this->input->post('type') == 1) {
                $post['slug'] = "";
            } elseif ($this->input->post('type') == 2) {
                $post['slug'] = "customer/login";
            } elseif ($this->input->post('type') == 3) {
                $post['slug'] = "customer/register";
            } elseif ($this->input->post('type') == 6) {
                $post['slug'] = $this->input->post('slug');
            } else {
                $post['slug'] = "#";
            }

            if ((bool)$menu_id) {
                $this->menu_model->edit($menu_id, $post);
                $json['success'] = 'Data Menu telah berhasil diperbarui.';
            } else {
                $this->menu_model->create($post);
                $json['success'] = 'Data Menu telah berhasil ditambahkan.';
            }
        }

        $this->output->set_output(json_encode($json));
    }

    public function delete()
    {
        check_ajax();

        $json = array();

        $menu_id = $this->input->post('menu_id');

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('menu_model');

            $this->menu_model->delete($menu_id);

            $json['success'] = 'Menu telah terhapus!';
        }

        $this->output->set_output(json_encode($json));
    }


    public function editcategory()
    {
        $this->load->vars('heading_title', 'Edit category list');

        $category = $this->db
            ->select('category_id , name, menu_active')
            ->where('parent_id', 0)
            ->get('category')
            ->result_array();
        $this->load->vars('category', $category);

        $data['action'] = admin_url('menu/posteditcategory');
        $this->load->view('admin/editcategory', $data);
    }


    public function posteditcategory()
    {
        $post = $this->input->post('id');


        $this->db->update("category", array('menu_active' => 0));


        foreach ($post as $key => $val) {
            $this->db->where("category_id", $val)->update("category", array('menu_active' => 1));
        }


        $json['success'] = 'Data Category list telah di update.';
        $this->output->set_output(json_encode($json));
    }


    public function submenu($id)
    {
        $this->load->vars('id', $id);


        if ($this->input->post(null, true)) {
            $this->load->library('datatables');

            $this->datatables
                ->select('menu_id, title, sort_order, type, active, slug, page_id, icon')
                ->where('sub_id', $id)
                ->from('menu');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $this->load->view('admin/submenu');
        }
    }

    public function createsubmenu($id)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars('heading_title', 'Tambah Sub Menu');
        $this->load->vars('id', $id);

        $this->formsubmenu();
    }

    public function editsubmenu($page_id = null, $id)
    {
        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }

        $this->load->vars('heading_title', 'Edit Menu');
        $this->load->vars('id', $id);

        $this->formsubmenu($page_id);
    }

    public function formsubmenu($menu_id = null)
    {
        $this->load->helper('form');


        $this->load->model('menu_model');

        $data['action'] = admin_url('menu/validatesubmenu');
        $data['menu_id'] = null;
        $data['sub_id'] = 0;
        $data['icon'] = 'fa-chevron-right';
        $data['title'] = '';
        $data['type'] = '';
        $data['page_id'] = '';
        $data['slug'] = '';
        $data['sort_order'] = 0;
        $data['active'] = 0;

        if ($page = $this->menu_model->get($menu_id)) {
            foreach ($page as $key => $val) {
                $data[$key] = $val;
            }
        }

        $this->load->view('admin/submenu_form', $data);
    }

    public function validatesubmenu()
    {
        $this->load->library('form_validation');
        $this->load->model('menu_model');

        $this->form_validation
            ->set_rules('title', 'Menu', 'trim|required|min_length[3]')
            ->set_rules('icon', 'Icon', 'trim|required|min_length[3]')
            ->set_rules('type', 'Type', 'required')
            ->set_error_delimiters('', '');

        $json = array();

        if ($this->form_validation->run() === false) {
            foreach (array('title', 'icon', 'type') as $field) {
                if (form_error($field) != '') {
                    $json['error'][$field] = form_error($field);
                }
            }
        } else {
            $menu_id = $this->input->post('menu_id');

            $post['icon'] = $this->input->post('icon');
            $post['sub_id'] = $this->input->post('sub_id');
            $post['title'] = $this->input->post('title');
            $post['type'] = $this->input->post('type');
            $post['sort_order'] = (int)$this->input->post('sort_order');
            $post['active'] = (bool)$this->input->post('active');

            if ($this->input->post('type') == 0) {
                $post['page_id'] = $this->input->post('page');
                $pgs = $this->db
                    ->select('slug')
                    ->where('page_id', $this->input->post('page'))
                    ->where('active', 1)
                    ->get('page')
                    ->row();

                $post['slug'] = 'page/' . $pgs->slug;
            } elseif ($this->input->post('type') == 1) {
                $post['slug'] = "";
            } elseif ($this->input->post('type') == 2) {
                $post['slug'] = "customer/login";
            } elseif ($this->input->post('type') == 3) {
                $post['slug'] = "customer/register";
            } elseif ($this->input->post('type') == 6) {
                $post['slug'] = $this->input->post('slug');
            } else {
                $post['slug'] = "#";
            }

            if ((bool)$menu_id) {
                $this->menu_model->edit($menu_id, $post);
                $json['success'] = 'Data Menu telah berhasil diperbarui.';
            } else {
                $this->menu_model->create($post);
                $json['success'] = 'Data Menu telah berhasil ditambahkan.';
            }
        }

        $this->output->set_output(json_encode($json));
    }

    public function deletesubmenu()
    {
        check_ajax();

        $json = array();

        $menu_id = $this->input->post('menu_id');

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->load->model('menu_model');

            $this->menu_model->delete($menu_id);

            $json['success'] = 'Menu telah terhapus!';
        }

        $this->output->set_output(json_encode($json));
    }


    public function socialmedia()
    {

        if (!$this->admin_auth->has_permission()) {
            show_401($this->uri->uri_string());
        }


        if ($this->input->post(null, true)) {


            $this->setting_model->edit_setting_value('config', 'facebook_user', $this->input->post('facebook_user'));
            $this->setting_model->edit_setting_value('config', 'twitter_user', $this->input->post('twitter_user'));
            $this->setting_model->edit_setting_value('config', 'instagram_user', $this->input->post('instagram_user'));
            $this->setting_model->edit_setting_value('config', 'gplus_user', $this->input->post('gplus_user'));
            $this->setting_model->edit_setting_value('config', 'youtube_user', $this->input->post('youtube_user'));


            $json['success'] = 'Social media tersimpan!';
            $this->output->set_output(json_encode($json));
        } else {
            $data['facebook_user'] = $this->config->item('facebook_user');
            $data['twitter_user'] = $this->config->item('twitter_user');
            $data['instagram_user'] = $this->config->item('instagram_user');
            $data['gplus_user'] = $this->config->item('gplus_user');
            $data['youtube_user'] = $this->config->item('youtube_user');

            $data['action'] = admin_url('menu/socialmedia');
            $this->load->view('admin/socialmedia', $data);
        }


    }


} 