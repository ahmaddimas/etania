<div class="page-title">
    <div>
        <h1>Social Media</h1>
    </div>
    <div class="btn-group">
        <a id="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?= form_open($action, 'id="form" class="form-horizontal" role="form"') ?>
                <div class="form-group">
                    <label class="control-label col-sm-3">Facebook Account<br><span
                                class="help">Facebook user account</span></label>
                    <div class="col-sm-8">
                        <input type="text" name="facebook_user"
                               value="<?= isset($facebook_user) ? $facebook_user : '' ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Twitter Account<br><span
                                class="help">Twitter user account</span></label>
                    <div class="col-sm-8">
                        <input type="text" name="twitter_user" value="<?= isset($twitter_user) ? $twitter_user : '' ?>"
                               class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Instagram Account<br><span
                                class="help">Instagram user account</span></label>
                    <div class="col-sm-8">
                        <input type="text" name="instagram_user"
                               value="<?= isset($instagram_user) ? $instagram_user : '' ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Google+ Account<br><span
                                class="help">Google+ user account</span></label>
                    <div class="col-sm-8">
                        <input type="text" name="gplus_user" value="<?= isset($gplus_user) ? $gplus_user : '' ?>"
                               class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Youtube Channel<br><span
                                class="help">Youtube Channel account</span></label>
                    <div class="col-sm-8">
                        <input type="text" name="youtube_user" value="<?= isset($youtube_user) ? $youtube_user : '' ?>"
                               class="form-control">
                    </div>
                </div>

                <i><font color="red">Field yang kosong otomatis tidak muncul dalam footer menu</font></i>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                        $('select[name=\'' + i + '\']').parent().addClass('has-error');
                        $('select[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').parent().addClass('has-error');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('menu/socialmedia')?>";
                }
            }
        });
    });
</script>