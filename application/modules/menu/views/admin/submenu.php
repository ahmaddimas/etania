<div class="page-title">
    <div>
        <h1>Sub Menu</h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('menu') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>
        <a href="<?= admin_url('menu/createsubmenu/') ?><?= $id ?>" class="btn btn-primary"><i class="fa fa-plus"></i>
            Tambah</a>
        <a onclick="refreshTable();" class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <table class="table table-hover table-bordered" id="datatable" width="100%">
                    <thead>
                    <tr>
                        <th style="width:7%">Urutan</th>
                        <th>Menu</th>
                        <th>Type</th>
                        <th style="width:7%">Active</th>
                        <th style="width:15%" class="text-right"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('menu/submenu/')?><?=$id?>",
                "type": "POST",
            },
            "columns": [
                {"data": "sort_order"},
                {"data": "title"},
                {"data": "type"},
                {"data": "active"},
                {"orderable": false, "searchable": false, "data": "menu_id"},
            ],
            "createdRow": function (row, data, index) {
                if (data.type == 0) {
                    $('td', row).eq(2).html('Page');
                    $('td', row).eq(1).html('<a href="<?=site_url('\'+data.slug+\'')?>" target="_blank">' + data.title + '</a>');
                } else if (data.type == 1) {
                    $('td', row).eq(2).html('Halaman Utama');
                    $('td', row).eq(1).html('<a href="<?=site_url('\'+data.slug+\'')?>" target="_blank">' + data.title + '</a>');
                } else if (data.type == 2) {
                    $('td', row).eq(2).html('Login');
                    $('td', row).eq(1).html('<a href="<?=site_url('\'+data.slug+\'')?>" target="_blank">' + data.title + '</a>');
                } else if (data.type == 3) {
                    $('td', row).eq(2).html('Register');
                    $('td', row).eq(1).html('<a href="<?=site_url('\'+data.slug+\'')?>" target="_blank">' + data.title + '</a>');
                } else if (data.type == 4) {
                    $('td', row).eq(2).html('Category <div class="btn-group btn-group-xs pull-right"><a href="<?=admin_url('menu/editcategory')?>" class="btn btn-primary btn-xs"><i class="fa fa-cog"></i> Edit category list</a></div>');
                    $('td', row).eq(1).html('<a href="<?=site_url('\'+data.slug+\'')?>" target="_blank">' + data.title + '</a>');
                } else if (data.type == 5) {
                    $('td', row).eq(2).html('Menu Bertingkat <div class="btn-group btn-group-xs pull-right"><a href="<?=admin_url('menu/submenu')?>/' + data.menu_id + '" class="btn btn-primary btn-xs"><i class="fa fa-cog"></i> Edit sub menu</a></div>');
                    $('td', row).eq(1).html('<a href="<?=site_url('\'+data.slug+\'')?>" target="_blank">' + data.title + '</a>');
                } else {
                    $('td', row).eq(2).html('Link');
                    $('td', row).eq(1).html('<a href="' + data.slug + '" target="_blank">' + data.title + '</a>');
                }

                html = '<div class="btn-group btn-group-xs">';
                html += '<a href="<?=admin_url('menu/editsubmenu')?>/' + data.menu_id + '/<?=$id?>" class="btn btn-primary btn-xs"><i class="fa fa-cog"></i> Edit</a>';
                html += '<a onclick="delRecord(' + data.menu_id + ');" class="btn btn-warning btn-xs"><i class="fa fa-minus-circle"></i> Hapus</a> ';
                html += '</div>';

                $('td', row).eq(4).addClass('text-right').html(html);

                if (data.active === '1') {
                    $('td', row).eq(3).html('<span class="label label-success">Yes</span>');
                } else {
                    $('td', row).eq(3).html('<span class="label label-default">No</span>');
                }
            },
            "order": [[1, 'asc']],
        });
    });

    function delRecord(menu_id) {
        swal({
            title: "Apakah anda yakin?",
            text: "Data yang sudah dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('menu/deletesubmenu')?>',
                    type: 'post',
                    data: 'menu_id=' + menu_id,
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal("Terhapus!", json['success'], "success");
                        } else if (json['error']) {
                            swal("Error!", json['error'], "error");
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                        refreshTable();
                    }
                });
            }
        });
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }
</script>