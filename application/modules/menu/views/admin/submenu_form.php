<div class="page-title">
    <div>
        <h1><?= $heading_title ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('menu/submenu/') ?><?= $id ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>
        <a id="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?= form_open($action, 'id="form" class="form-horizontal" role="form"') ?>
                <input type="hidden" name="menu_id" value="<?= $menu_id ?>">
                <input type="hidden" name="sub_id" value="<?= $id ?>">
                <div id="message"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Menu</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" value="<?= $title ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group" style="display:none">
                    <label class="col-sm-2 control-label">Icon</label>
                    <div class="col-sm-10">
                        <input type="text" name="icon" value="<?= $icon ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Urutan</label>
                    <div class="col-sm-2">
                        <input type="text" name="sort_order" value="<?= $sort_order ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Type</label>
                    <div class="toggle lg col-sm-8">
                        <label style="margin-top:5px;">
                            <select name="type" class="form-control" style="width:100%" id="type-list"
                                    onchange="checkpage()" required>
                                <option value="">Silahkan pilih menu tipe . . .</option>
                                <option value="0" <?php if ($type == 0) echo "selected"; ?>>Page</option>
                                <option value="1" <?php if ($type == 1) echo "selected"; ?>>Halaman Utama</option>
                                <option value="2" <?php if ($type == 2) echo "selected"; ?>>Login</option>
                                <option value="3" <?php if ($type == 3) echo "selected"; ?>>Register</option>
                                <option value="6" <?php if ($type == 6) echo "selected"; ?>>URL link</option>
                            </select>
                        </label>
                    </div>
                </div>

                <?php
                $pgs = $this->db
                    ->select('page_id , title')
                    ->where('active', 1)
                    ->order_by('sort_order', 'asc')
                    ->get('page')
                    ->result_array();
                ?>

                <div class="form-group" id="page-list" style="<?php if ($type <> 0) echo "display:none"; ?>">
                    <label class="col-sm-2 control-label">Page</label>
                    <div class="toggle lg col-sm-8">
                        <label style="margin-top:5px;">
                            <select name="page" class="form-control" style="width:100%">
                                <option value="">Silahkan pilih page . . .</option>
                                <?php foreach ($pgs as $ps) { ?>
                                    <option value="<?= $ps['page_id'] ?>" <?php if ($page_id == $ps['page_id']) echo "selected"; ?>><?= $ps['title'] ?></option>
                                <?php } ?>
                            </select>
                        </label>
                    </div>
                </div>


                <div class="form-group" id="url-link" style="<?php if ($type <> 6) echo "display:none"; ?>">
                    <label class="col-sm-2 control-label">URL link</label>
                    <div class="toggle lg col-sm-8">
                        <label style="margin-top:5px;">
                            <input type="text" name="slug" value="<?= $slug ?>" class="form-control">
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="toggle lg col-sm-4">
                        <label style="margin-top:5px;">
                            <?php if ($active) { ?>
                                <input type="checkbox" name="active" value="1" checked="checked">
                            <?php } else { ?>
                                <input type="checkbox" name="active" value="1">
                            <?php } ?>
                            <span class="button-indecator"></span>
                        </label>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                        $('select[name=\'' + i + '\']').parent().addClass('has-error');
                        $('select[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').parent().addClass('has-error');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('menu/submenu/')?><?=$id?>";
                }
            }
        });
    });


    function checkpage() {
        type = $('#type-list').val();
        if (type == 0) {
            $('#page-list').show();
            $('#url-link').hide();
        } else if (type == 6) {
            $('#url-link').show();
            $('#page-list').hide();
        } else {
            $('#page-list').hide();
            $('#url-link').hide();
        }
    }
</script>