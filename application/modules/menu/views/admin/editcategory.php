<div class="page-title">
    <div>
        <h1><?= $heading_title ?></h1>
    </div>
    <div class="btn-group">
        <a href="<?= admin_url('menu') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>
        <a id="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body treeview">
                <?= form_open($action, 'id="form" class="form-horizontal" role="form"') ?>
                <legend>Silahkan checklist menu category yang akan di tampilkan</legend>
                <div class="treeview">
                    <ul>
                        <?php foreach ($category as $parrent) { ?>
                            <?php $child = $this->db
                                ->select('category_id , name, menu_active')
                                ->where('parent_id', $parrent['category_id'])
                                ->get('category')
                                ->result_array();
                            ?>
                            <?php if (count($child) <> 0) { ?>
                                <li>
                                    <label><input type="checkbox" name="id[]" value="<?= $parrent['category_id'] ?>"
                                                  id="parrent" <?php if ($parrent['menu_active'] == 1) echo "checked" ?> /><?= $parrent['name'] ?>
                                    </label>
                                    <ul>
                                        <?php foreach ($child as $cld) { ?>
                                            <?php $grandchild = $this->db
                                                ->select('category_id , name, menu_active')
                                                ->where('parent_id', $cld['category_id'])
                                                ->get('category')
                                                ->result_array();
                                            ?>
                                            <?php if (count($grandchild) <> 0) { ?>
                                                <li>
                                                    <label><input type="checkbox" name="id[]"
                                                                  value="<?= $cld['category_id'] ?>"
                                                                  id="child" <?php if ($cld['menu_active'] == 1) echo "checked" ?> /><?= $cld['name'] ?>
                                                    </label>
                                                    <ul>
                                                        <?php foreach ($grandchild as $grcld) { ?>
                                                            <li><input type="checkbox" name="id[]"
                                                                       value="<?= $grcld['category_id'] ?>"
                                                                       id="grandchild" <?php if ($grcld['menu_active'] == 1) echo "checked" ?> /><?= $grcld['name'] ?></label>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php } else { ?>
                                                <li><label><input type="checkbox" name="id[]"
                                                                  value="<?= $cld['category_id'] ?>"
                                                                  id="child" <?php if ($cld['menu_active'] == 1) echo "checked" ?> /><?= $cld['name'] ?>
                                                    </label></li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } else { ?>
                                <li><label><input type="checkbox" name="id[]" value="<?= $parrent['category_id'] ?>"
                                                  id="parrent" <?php if ($parrent['menu_active'] == 1) echo "checked" ?> /><?= $parrent['name'] ?>
                                    </label></li>
                            <?php } ?>

                        <?php } ?>
                    </ul>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                        $('select[name=\'' + i + '\']').parent().addClass('has-error');
                        $('select[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                        $('textarea[name=\'' + i + '\']').parent().addClass('has-error');
                        $('textarea[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('menu')?>";
                }
            }
        });
    });


    function checkpage() {
        type = $('#type-list').val();
        if (type == 0) {
            $('#page-list').show();
        } else {
            $('#page-list').hide();
        }

    }
</script>
<style type="text/css" media="screen">
    .treeview {
        margin: 10px;
    }

    .treeview ul {
        list-style: none;
    }

    .treeview ul li {
        list-style: none;
    }

    .treeview span {
        font-weight: 600;
    }

    .treeview input {
        margin-right: 10px;
    }
</style>

