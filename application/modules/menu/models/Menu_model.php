<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Create new page
     *
     * @access public
     * @param array $data
     * @return int | bool
     */
    public function create($data = array())
    {
        return $this->insert($data);
    }

    /**
     * Update existing page
     *
     * @access public
     * @param array $data
     * @return int | bool
     */
    public function edit($menu_id, $data = array())
    {
        return $this->update($menu_id, $data);
    }

    /**
     * Delete page
     *
     * @access public
     * @param int $menu_id
     * @return void
     */
    public function delete($menu_id = null)
    {
        $this->db
            ->where('menu_id', $menu_id)
            ->delete('menu');
    }


}