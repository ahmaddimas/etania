<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('currency');
        $this->load->library('form_validation');

        $this->load->helper('form');
        $this->load->helper('language');

        $this->load->model('order/order_model');
        $this->load->model('system/order_status_model');
    }

    /**
     * Orders list
     *
     * @access public
     * @return void
     */
    public function index()
    {
        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('o.order_id, o.date_added, o.name, o.total, os.name as status, o.order_status_id', false)
                ->join('order_status os', 'os.order_status_id = o.order_status_id', 'left')
                ->from('order o')
                ->edit_column('date_added', '$1 WIB', 'format_date(date_added, true)')
                ->edit_column('total', '$1', 'format_money(total)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            if (!$this->admin_auth->has_permission()) {
                show_401($this->uri->uri_string());
            }

            $data['success'] = $this->session->userdata('success');
            $data['error'] = $this->session->userdata('error');

            $this->load
                ->title('Data Order')
                ->view('admin/order', $data);
        }
    }


    /**
     * Delete
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        check_ajax();

        $json = array();

        $order_id = $this->input->post('order_id');

        if (!$this->admin_auth->has_permission()) {
            $json['error'] = lang('admin_error_delete');
        }

        if (empty($json['error'])) {
            $this->order_model->delete($order_id);
            $json['success'] = 'Berhasil menghapus pesanan!';
        }

        $this->output->set_output(json_encode($json));
    }

    /**
     * Detail
     *
     * @access public
     * @param int $order_id
     * @return void
     */
    public function detail($order_id = null)
    {
        $this->load->helper('format');

        if ($order = $this->order_model->get_order($order_id)) {
            foreach ($order as $key => $value) {
                $data[$key] = $value;
            }

            $order_status = $this->order_status_model->get($data['order_status_id']);

            $data['order_statuses'] = $this->order_status_model->get_all();
            $data['total'] = $this->currency->format($data['total']);
            $data['status'] = $order_status ? $order_status['name'] : '';
            $data['date_added'] = date('d/m/Y', strtotime($order['date_added']));
            $data['products'] = array();

            $key = 1;

            foreach ($this->order_model->get_order_products($order_id) as $product) {
                $product['price'] = format_money($product['price']);
                $product['total'] = format_money($product['total']);

                $data['products'][$key] = $product;

                $key++;
            }

            $data['totals'] = $this->order_model->get_order_totals($order_id);

            if ($this->input->get('ref')) {
                $data['back'] = admin_url($this->input->get('ref'));
            } else {
                $data['back'] = admin_url('sale/shipping');
            }

            $address = array(
                'name' => $data['name'],
                'address' => $data['address'],
                'telephone' => $data['telephone'],
                'postcode' => $data['postcode'],
                'subdistrict' => $data['subdistrict'],
                'city' => $data['city'],
                'province' => $data['province']
            );

            $data['shipping_address'] = format_address($address);
            $data['action'] = admin_url('order/add_history');

            $data['success'] = $this->session->flashdata('success');
            $data['error'] = $this->session->flashdata('error');

            $this->load
                ->title('Detil Order #' . $order_id)
                ->view('admin/order_detail', $data);
        } else {
            show_404();
        }
    }

    /**
     * Fetch order hitory
     *
     * @access public
     * @return json
     */
    public function history()
    {
        if ($this->input->post(null, true)) {
            $this->load->helper('format');
            $this->load->library('datatables');

            $this->datatables
                ->select('oh.*, oh.date_added as date_added, os.name as status, CONCAT_WS(" - ", a.name, ag.name) as updater', false)
                ->join('order_status os', 'os.order_status_id = oh.order_status_id', 'left')
                ->join('admin a', 'a.admin_id = oh.admin_id', 'left')
                ->join('admin_group ag', 'ag.admin_group_id = a.admin_group_id', 'left')
                ->where('oh.order_id', $this->input->post('order_id'))
                ->from('order_history oh')
                ->edit_column('date_added', '$1', 'format_date(date_added, true)')
                ->edit_column('total', '$1', 'format_money(total)');

            $this->output->set_output($this->datatables->generate('json'));
        } else {
            show_404();
        }
    }

    /**
     * Add history
     *
     * @access public
     * @return void
     */
    public function add_history()
    {
        $json = array();

        $keys = array(
            'order_id',
            'order_status_id',
            'notify',
            'override',
            'comment',
            'waybill'
        );

        foreach ($keys as $key) {
            if ($this->input->post($key)) {
                $$key = $this->input->post($key);
            } else {
                $$key = '';
            }
        }

        $this->load->model('order_model');

        if ($this->order_model->get($order_id)) {
            $this->load->model('order_history_model');

            $order_history = array(
                'order_id' => (int)$order_id,
                'order_status_id' => (int)$order_status_id,
                'admin_id' => (int)$this->admin_auth->admin_id(),
                'notify' => (int)$notify,
                'comment' => $comment
            );

            $update['order_status_id'] = $order_status_id;

            if ($waybill) {
                $update['waybill'] = $waybill;
            }

            // jika diset lunas, maka set tanggal pelunasan
            if ($order_status_id == $this->config->item('order_paid_status_id')) {
                $update['date_paid'] = date('Y-m-d H:i:s', time());
            }

            // jika diset cancel, maka status pembayaran lunas akan dicabut
            if ($order_status_id == $this->config->item('order_cancel_status_id')) {
                $update['date_paid'] = '0000-00-00 00:00:00';
            }

            $this->order_model->update($order_id, $update);
            $this->order_history_model->insert($order_history);

            if ($notify) {
                $this->sendmail($order_id, $waybill, $comment);
            }

            $json['success'] = 'Status pesanan telah berhasil diperbarui';
        } else {
            $json['error'] = 'Pesanan tidak ditemukan';
        }

        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($json));
    }


    public function sendmail($order_id, $waybill = null, $comment = null)
    {
        $this->load->model('order_model');


        $order_info = $this->order_model->get_order($order_id);


        $data['logo'] = $this->image->resize($this->config->item('logo'), 200);
        $data['order_id'] = $order_id;
        $data['invoice'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
        $data['name'] = $order_info['name'];
        $data['telephone'] = $order_info['telephone'];
        $data['email'] = $order_info['email'];
        $data['date_added'] = date('d/m/Y', strtotime($order_info['date_added']));
        $data['status'] = $order_info['status'];
        $data['ip_address'] = $order_info['ip'];
        $data['payment_method'] = $order_info['payment_method'];
        $data['total'] = $this->currency->format($order_info['total']);


        if ($waybill) {
            $data['waybill'] = $waybill;
        } else {
            $data['waybill'] = $waybill;
        }

        if ($comment) {
            $data['comment'] = $comment;
        } else {
            $data['comment'] = $comment;
        }

        $this->load->helper('format');


        $this->load->library('email');

        $this->email->initialize();
        $this->email->from($this->config->item('smtp_email'), $this->config->item('company'));
        $this->email->to($order_info['email']);
        $this->email->subject('Pemberitahuan Update Order');
        $this->email->message($this->load->layout(false)->view('admin/order_update', $data, true));
        $this->email->send();

        // admin notification
        $this->email->initialize();
        $this->email->from($this->config->item('smtp_email'), $this->config->item('company'));
        $this->email->to($this->config->item('email'));
        $this->email->subject('Pemberitahuan Update Order');
        $this->email->message($this->load->layout(false)->view('admin/order_update', $data, true));
        $this->email->send();

    }

    /**
     * Print or export docs to pdf with spesific type
     *
     * @access public
     * @param string $type Invoice, delivery note
     * @param int $order_id
     * @param bool $stream
     * @return void
     */
    public function eprint($type = false, $order_id = null, $stream = true)
    {
        if (!in_array($type, array('invoice', 'delivery'))) {
            show_404();
        }

        $this->load->helper('format');
        $this->load->library('currency');
        $this->load->library('pdf');

        if ($order = $this->order_model->get_order($order_id)) {
            foreach ($order as $key => $value) {
                $data[$key] = $value;
            }

            $data['date_added'] = date('d/m/Y', strtotime($order['date_added']));
            $data['products'] = array();

            $key = 1;

            foreach ($this->order_model->get_order_products($order_id) as $product) {
                $product['price'] = format_money($product['price']);
                $product['total'] = format_money($product['total']);

                $data['products'][$key] = $product;

                $key++;
            }

            $data['totals'] = $this->order_model->get_order_totals($order_id);

            $shipping_address = array(
                'name' => $data['name'],
                'address' => $data['address'],
                'telephone' => $data['telephone'],
                'postcode' => $data['postcode'],
                'subdistrict' => $data['subdistrict'],
                'city' => $data['city'],
                'province' => $data['province']
            );

            $data['shipping_address'] = format_address($shipping_address);
        } else {
            show_404();
        }

        $this->load->library('image');

        $data['logo'] = str_replace(base_url(), FCPATH, $this->image->resize($this->config->item('logo'), 120));

        switch ($type) {
            case 'invoice':
                $filename = APPPATH . 'cache/docs/invoice-' . $order_id . '.pdf';
                if (!$stream) {
                    $this->pdf->pdf_create($this->load->layout(false)->view('admin/order_invoice_pdf', $data, true), $filename, false);
                    return file_exists($filename) ? $filename : false;
                } else {
                    $this->pdf->pdf_create($this->load->layout(false)->view('admin/order_invoice_pdf', $data, true), 'invoice-' . $order_id, true);
                }
                break;

            case 'delivery':
                $filename = APPPATH . 'cache/docs/delivery-' . $order_id . '.pdf';
                if (!$stream) {
                    $this->pdf->pdf_create($this->load->layout(false)->view('admin/order_delivery_pdf', $data, true), $filename, false);
                    return file_exists($filename) ? $filename : false;
                } else {
                    $this->pdf->pdf_create($this->load->layout(false)->view('admin/order_delivery_pdf', $data, true), 'delivery-' . $order_id, true);
                }
                break;

            default:
                show_404();
                break;
        }
    }

    /**
     * Print shipping note
     *
     * @access public
     * @return void
     */
    public function shipping()
    {
        $this->load->helper('format');

        $data['orders'] = array();

        $orders = array();

        if ($this->input->post('order_id')) {
            $orders = $this->input->post('order_id');
        } elseif ($this->input->get('order_id')) {
            $orders[] = $this->input->get('order_id');
        }

        foreach ($orders as $order_id) {
            if ($order = $this->order_model->get_order($order_id)) {
                $order_status = $this->order_status_model->get($order['order_status_id']);

                $order['order_statuses'] = $this->order_status_model->get_all();
                $order['total'] = $this->currency->format($order['total']);
                $order['status'] = $order_status ? $order_status['name'] : '';
                $order['date_added'] = date('d/m/Y', strtotime($order['date_added']));
                $order['products'] = array();

                $key = 1;

                foreach ($this->order_model->get_order_products($order_id) as $product) {
                    $product['price'] = format_money($product['price']);
                    $product['total'] = format_money($product['total']);

                    $order['products'][$key] = $product;

                    $key++;
                }

                $order['totals'] = $this->order_model->get_order_totals($order_id);

                $address = array(
                    'name' => $order['name'],
                    'address' => $order['address'],
                    'telephone' => $order['telephone'],
                    'postcode' => $order['postcode'],
                    'subdistrict' => $order['subdistrict'],
                    'city' => $order['city'],
                    'province' => $order['province']
                );

                $order['shipping_address'] = format_address($address);

                $data['orders'][] = $order;
            }
        }

        $data['logo'] = str_replace(base_url(), FCPATH, $this->image->resize($this->config->item('logo'), 120));

        $this->load->library('pdf');

        $filename = APPPATH . 'cache/docs/shipping-note-' . time() . '.pdf';
        $stream = true;

        if (!$stream) {
            $this->pdf->render_shipping_note($this->load->layout(false)->view('admin/order_shipping_note', $data, true), $filename, false);
            return file_exists($filename) ? $filename : false;
        } else {
            $this->pdf->render_shipping_note($this->load->layout(false)->view('admin/order_shipping_note', $data, true), 'shipping-note-' . $order_id, true);
        }
    }


    public function shippinglabel($order_id)
    {
        $this->load->helper('format');

        $data['orders'] = array();


        $order = $order_id;

        if ($order = $this->order_model->get_order($order_id)) {
            $order_status = $this->order_status_model->get($order['order_status_id']);

            $order['order_statuses'] = $this->order_status_model->get_all();
            $order['total'] = $this->currency->format($order['total']);
            $order['status'] = $order_status ? $order_status['name'] : '';
            $order['date_added'] = date('d/m/Y', strtotime($order['date_added']));
            $order['products'] = array();

            $key = 1;

            foreach ($this->order_model->get_order_products($order_id) as $product) {
                $product['price'] = format_money($product['price']);
                $product['total'] = format_money($product['total']);

                $order['products'][$key] = $product;

                $key++;
            }

            $order['totals'] = $this->order_model->get_order_totals($order_id);

            $address = array(
                'name' => $order['name'],
                'address' => $order['address'],
                'telephone' => $order['telephone'],
                'postcode' => $order['postcode'],
                'subdistrict' => $order['subdistrict'],
                'city' => $order['city'],
                'province' => $order['province']
            );

            $order['address'] = $address;
            $order['shipping_address'] = format_address($address);

            $data['orders'][] = $order;
        }

        $data['logo'] = $this->image->resize($this->config->item('logo'), 120);


        $this->load->layout(false)->view('admin/order_shipping_note_print', $data);
    }
}