<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_total_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function updatetotal($id, $update)
    {

        $this->db->where($id);
        $this->db->set($this->set_data($update));
        $this->db->update($this->table);
    }
}