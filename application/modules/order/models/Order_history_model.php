<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_history_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
}