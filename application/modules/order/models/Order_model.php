<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends MY_Model
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Create order
     *
     * @access public
     * @param array $order
     * @param array $products
     * @param array $totals
     * @return int
     */
    public function create_order($order, $products, $totals)
    {
        $order_id = $this->insert($order);

        if ($order_id) {
            $this->set_order_products($order_id, $products);
            $this->set_order_totals($order_id, $totals);
            $this->set_invoice($order_id);
        }

        return $order_id;
    }

    /**
     * Update order
     *
     * @access public
     * @param int $order_id
     * @param array $order
     * @param array $products
     * @param float $totals
     * @return int
     */
    public function update_order($order_id, $order, $products, $totals)
    {
        $this->update($order_id, $order);

        $this->db
            ->where('order_id', $order_id)
            ->delete(array('order_product', 'order_total', 'order_option'));

        $this->set_order_products($order_id, $products);
        $this->set_order_totals($order_id, $totals);

        return $order_id;
    }

    /**
     * Set order products
     *
     * @access public
     * @param array $products
     * @return void
     */
    public function set_order_products($order_id, $products)
    {
        $order_product = array();

        foreach ($products as $product) {
            if (isset($product['option'])) {
                $options = $product['option'];
                unset($product['option']);
            } else {
                $options = array();
            }

            $product['order_id'] = (int)$order_id;

            $order_product = $this->set_data($product, 'order_product');
            $this->db->insert('order_product', $order_product);
            $order_product_id = $this->db->insert_id();

            if ($order_product_id && $options) {
                $order_options = array();

                foreach ($options as $option) {
                    $order_options[] = array(
                        'order_id' => $order_id,
                        'order_product_id' => $order_product_id,
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value_id' => $option['product_option_value_id'],
                        'name' => $option['name'],
                        'value' => $option['option_value'],
                        'type' => $option['type'],
                    );
                }

                if ($order_options) {
                    $this->db->insert_batch('order_option', $order_options);
                }
            }
        }
    }

    /**
     * Set order totals
     *
     * @access public
     * @param array $totals
     * @return void
     */
    public function set_order_totals($order_id, $totals)
    {
        $order_total = array();

        foreach ($totals as $total) {
            $total['order_id'] = (int)$order_id;
            $order_total[] = $this->set_data($total, 'order_total');
        }

        if ($order_total) {
            $this->db->insert_batch('order_total', $order_total);
        }
    }

    /**
     * Set invoice number
     *
     * @access public
     * @param int $order_id
     * @return void
     */
    public function set_invoice($order_id)
    {
        $months = array(
            '01' => 'I',
            '02' => 'II',
            '03' => 'III',
            '04' => 'IV',
            '05' => 'V',
            '06' => 'VI',
            '07' => 'VII',
            '08' => 'VIII',
            '09' => 'IX',
            '10' => 'X',
            '11' => 'XI',
            '12' => 'XII',
        );

        $year = date('Y', time());
        $month = date('m', time());

        $this->db
            ->where('order_id', (int)$order_id)
            ->set('invoice_prefix', 'INV/' . $year . '/' . $months[$month] . '/')
            ->set('invoice_no', $order_id)
            ->update('order');
    }

    /**
     * Get order
     *
     * @access public
     * @param int $order_id
     * @return array
     */
    public function get_order($order_id)
    {
        return $this->db
            ->select('o.*, CONCAT(o.invoice_prefix, o.invoice_no) as invoice, os.name as status', false)
            ->join('order_status os', 'os.order_status_id = o.order_status_id', 'left')
            ->where('o.order_id', (int)$order_id)
            ->get('order o')
            ->row_array();
    }

    /**
     * Get order products
     *
     * @access public
     * @param int $order_id
     * @return array
     */
    public function get_order_products($order_id)
    {
        $results = array();

        $order_products = $this->db
            ->where('order_id', (int)$order_id)
            ->get('order_product')
            ->result_array();

        foreach ($order_products as $order_product) {
            $order_product['option'] = $this->db
                ->select('oo.*, pov.subtract')
                ->join('product_option_value pov', 'pov.product_option_value_id = oo.product_option_value_id', 'left')
                ->where('oo.order_id', (int)$order_id)
                ->where('oo.order_product_id', (int)$order_product['order_product_id'])
                ->get('order_option oo')
                ->result_array();

            $results[] = $order_product;
        }

        return $results;
    }

    public function get_product_options($order_id, $product_id)
    {
        return $this->db
            ->where('order_id', (int)$order_id)
            ->where('product_id', (int)$product_id)
            ->get('order_option')
            ->result_array();
    }

    /**
     * Get order totals
     *
     * @access public
     * @param int $order_id
     * @return array
     */
    public function get_order_totals($order_id)
    {
        return $this->db
            ->where('order_id', (int)$order_id)
            ->order_by('sort_order', 'asc')
            ->get('order_total')
            ->result_array();
    }

    /**
     * Order confirmation
     *
     * @access public
     * @param int $order_id
     * @param int $order_status_id
     * @param string $comment
     * @param bool $notifys
     * @return void
     */
    public function confirm($order_id, $order_status_id, $comment = '', $notify = false)
    {
        $order_info = $this->get_order($order_id);

        if ($order_info && !$order_info['order_status_id']) {
            $this->db
                ->where('order_id', (int)$order_id)
                ->set(array(
                    'order_status_id' => (int)$order_status_id,
                    'date_modified' => date('Y-m-d H:i:s', time())
                ))->update('order');

            $this->load->model('order/order_history_model');

            $order_history = array(
                'order_id' => (int)$order_id,
                'order_status_id' => (int)$order_status_id,
                'notify' => (int)$notify,
                'comment' => $comment
            );

            $this->order_history_model->insert($order_history);

            $this->load->library('currency');

            $data['products'] = array();

            foreach ($this->get_order_products($order_id) as $order_product) {
                $this->db
                    ->where('product_id', (int)$order_product['product_id'])
                    ->where('quantity >', 0)
                    ->set('quantity', '(quantity - ' . (int)$order_product['quantity'] . ')', false)
                    ->update('product');

                $data['products'][] = array(
                    'name' => $order_product['name'],
                    'quantity' => $order_product['quantity'],
                    'price' => $this->currency->format($order_product['price']),
                    'total' => $this->currency->format($order_product['total']),
                    'option' => $order_product['option']
                );

                if ($order_product['option']) {
                    foreach ($order_product['option'] as $order_option) {
                        if ($order_option['subtract']) {
                            $this->db
                                ->where('product_option_value_id', (int)$order_option['product_option_value_id'])
                                ->where('quantity >', 0)
                                ->set('quantity', '(quantity - ' . (int)$order_product['quantity'] . ')', false)
                                ->update('product_option_value');
                        }
                    }
                }
            }

            $this->load->library('total');
            $this->total->confirm($order_id);

            if ($notify) {
                $this->load->library('image');

                $data['logo'] = $this->image->resize($this->config->item('logo'), 200);
                $data['order_id'] = $order_id;
                $data['invoice'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                $data['name'] = $order_info['name'];
                $data['telephone'] = $order_info['telephone'];
                $data['email'] = $order_info['email'];
                $data['date_added'] = date('d/m/Y', strtotime($order_info['date_added']));
                $data['status'] = $order_info['status'];
                $data['comment'] = nl2br($comment);
                $data['ip_address'] = $order_info['ip'];


                if ($order_info['payment_code'] <> "midtrans") {
                    $pid = explode(" ", $order_info['payment_code']);
                    $payment_transfer_id = $pid[(count($pid) - 1)];
                    $this->load->model('payment/payment_transfer_model');
                    $method = $this->payment_transfer_model->get($payment_transfer_id);
                    $data['payment_instruction'] = $method['instruction'];
                } else {
                    $data['payment_instruction'] = false;
                }

                $this->load->helper('format');

                $address['name'] = $order_info['name'];
                $address['address'] = $order_info['address'];
                $address['telephone'] = $order_info['telephone'];
                $address['postcode'] = $order_info['postcode'];
                $address['subdistrict'] = $order_info['subdistrict'];
                $address['city'] = $order_info['city'];
                $address['province'] = $order_info['province'];

                $data['address'] = format_address($address);

                $data['payment_method'] = $order_info['payment_method'];

                $data['totals'] = $this->get_order_totals($order_id);


                $this->load->library('email');
                $pdf = $this->eprint($order_id);

                $this->email->initialize();
                $this->email->from($this->config->item('smtp_email'), $this->config->item('company'));
                $this->email->to($order_info['email']);
                $this->email->attach($pdf);
                $this->email->subject('Pemberitahuan Order');
                $this->email->message($this->load->layout(false)->view('emails/order', $data, true));
                $this->email->send();

                // admin notification
                $this->email->initialize();
                $this->email->from($this->config->item('smtp_email'), $this->config->item('company'));
                $this->email->to($this->config->item('email'));
                $this->email->subject('Pemberitahuan Order');
                $this->email->message($this->load->layout(false)->view('emails/order', $data, true));
                $this->email->send();
            }
        }
    }

    /**
     * Delete order
     *
     * @access public
     * @param int $order_id
     * @return void
     */
    public function delete($order_id = null)
    {
        $this->db
            ->where('order_id', $order_id)
            ->delete(array(
                'order',
                'order_total',
                'order_product',
                'order_history',
            ));
    }

    /**
     * Get cart of sales
     *
     * @access public
     * @param array $params
     * @return array
     */
    public function get_chart_sales($params = array())
    {
        $results = $this->db
            ->select('SUM(total) as total, DATE_FORMAT(date_added, "%d") as day, DATE_FORMAT(date_added, "%m") as month', false)
            ->where('date_added >= ', $params['start'])
            ->where('date_added <= ', $params['end'])
            ->where('order_status_id', (int)$this->config->item('order_complete_status_id'))
            ->order_by($params['group'], 'asc')
            ->group_by($params['group'])
            ->get('order')
            ->result_array();

        $sales = array();

        foreach ($results as $result) {
            $sales[(int)$result[$params['group']]] = $result['total'];
        }

        return $sales;
    }

    /**
     * Get chart of orders
     *
     * @access public
     * @param array $data
     * @param array $params
     * @return array
     */
    public function get_chart_orders($data = array(), $params = array())
    {
        $results = $this->db
            ->select('COUNT(order_id) as total, DATE_FORMAT(date_added, "%d") as day, DATE_FORMAT(date_added, "%m") as month', false)
            ->from('order')
            ->where('date_added >= ', $params['start'])
            ->where('date_added <= ', $params['end']);

        if (isset($data['order_status_id'])) {
            $results = $this->db->where('order_status_id', $data['order_status_id']);
        }

        $results = $this->db
            ->order_by($params['group'], 'asc')
            ->group_by($params['group'])
            ->get()
            ->result_array();

        $orders = array();

        foreach ($results as $result) {
            $orders[(int)$result[$params['group']]] = $result['total'];
        }

        return $orders;
    }


    public function eprint($order_id = null)
    {
        $this->load->helper('format');
        $this->load->library('currency');
        $this->load->library('pdf');

        if ($order = $this->order_model->get_order($order_id)) {
            foreach ($order as $key => $value) {
                $data[$key] = $value;
            }

            $data['date_added'] = date('d/m/Y', strtotime($order['date_added']));
            $data['products'] = array();

            $key = 1;

            foreach ($this->order_model->get_order_products($order_id) as $product) {
                $product['price'] = format_money($product['price']);
                $product['total'] = format_money($product['total']);

                $data['products'][$key] = $product;

                $key++;
            }

            $data['totals'] = $this->order_model->get_order_totals($order_id);

            $shipping_address = array(
                'name' => $data['name'],
                'address' => $data['address'],
                'telephone' => $data['telephone'],
                'postcode' => $data['postcode'],
                'subdistrict' => $data['subdistrict'],
                'city' => $data['city'],
                'province' => $data['province']
            );

            $data['shipping_address'] = format_address($shipping_address);
        } else {
            show_404();
        }

        $this->load->library('image');

        $data['logo'] = str_replace(base_url(), FCPATH, $this->image->resize($this->config->item('logo'), 120));

        $filename = APPPATH . 'cache/invoice-' . $order_id . '.pdf';
        $this->pdf->pdf_create($this->load->layout(false)->view('../modules/order/views/admin/order_invoice_pdf', $data, true), $filename, false);
        return $filename;
    }
}