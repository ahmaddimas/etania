<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
        <p><?= $date_added ?></p>
    </div>
    <div class="btn-group">
        <button onclick="window.history.back();" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</button>
        <button id="track" class="btn btn-info"><i class="fa fa-truck"></i> Cek Pengiriman</button>
        <a href="<?= admin_url('order/eprint/invoice/' . $order_id) ?>" target="_blank" class="btn btn-warning"><i
                    class="fa fa-print"></i> Cetak Invoice</a>
    </div>
</div>
<section class="content">
    <div class="card">
        <div class="card-body">
            <div id="notification"></div>
            <div class="row">
                <div class="col-sm-6">
                    <dl class="dl-horizontal">
                        <dt>No. Invoice :</dt>
                        <dd><?= $invoice ?></dd>
                        <dt>Tanggal :</dt>
                        <dd><?= $date_added ?></dd>
                        <dt>Status :</dt>
                        <dd id="order-status"><?= $status ?></dd>
                        <dt>Nama :</dt>
                        <dd><?= $name ?></dd>
                        <dt>Email :</dt>
                        <dd><?= $email ?></dd>
                        <dt>No. Telepon :</dt>
                        <dd><?= $telephone ?></dd>
                        <dt>Total :</dt>
                        <dd><?= $total ?></dd>
                    </dl>
                </div>
                <div class="col-sm-6">
                    <dl class="dl-horizontal">
                        <dt>Alamat Pengiriman :</dt>
                        <dd><?= $shipping_address ?></dd>
                        <dt>Metode Pembayaran :</dt>
                        <dd><?= $payment_method ?></dd>
                        <dt>Metode Pengiriman :</dt>
                        <dd><?= $shipping_method ?></dd>
                        <dt>Catatan :</dt>
                        <dd><?= $comment ?></dd>
                        <dt>No. Resi :</dt>
                        <dd><?= $waybill ?></dd>
                    </dl>
                </div>
            </div>
            <legend><h4>Item Produk</h4></legend>
            <div class="table-responsive">
                <table class="table table-bordered" width="100%">
                    <thead>
                    <tr>
                        <th>Produk</th>
                        <th class="text-right">Qty</th>
                        <th class="text-right">Harga</th>
                        <th class="text-right">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($products as $product) { ?>
                        <tr>
                            <td>
                                <?= $product['name'] ?>
                                <?php if ($product['option']) { ?>
                                    <p>
                                        <?php foreach ($product['option'] as $option) { ?>
                                            <small><i><?= $option['name'] ?>: <?= $option['value'] ?></i></small><br>
                                        <?php } ?>
                                    </p>
                                <?php } ?>
                            </td>
                            <td class="text-right"><?= $product['quantity'] ?></td>
                            <td class="text-right"><?= $product['price'] ?></td>
                            <td class="text-right"><?= $product['total'] ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <?php foreach ($totals as $total) { ?>
                        <tr>
                            <th colspan="3" class="text-right"><?= $total['title'] ?></th>
                            <th class="text-right"><?= $total['text'] ?></th>
                        </tr>
                    <?php } ?>
                    </tfoot>
                </table>
            </div>
            <legend><h4>Histori Order</h4></legend>
            <table class="table table-bordered" id="datatable" width="100%">
                <thead>
                <tr>
                    <th style="min-width:15%;">Tanggal</th>
                    <th>Status</th>
                    <th>Dibuat Oleh</th>
                    <th>Catatan</th>
                    <th>Notified</th>
                </tr>
                </thead>
            </table>
            <legend><h4>Update Status</h4></legend>
            <?= form_open($action, 'class="form-horizontal" id="form-status"') ?>
            <input type="hidden" name="order_id" value="<?= $order_id ?>">
            <div class="form-group">
                <label class="control-label col-sm-3">Status Pesanan</label>
                <div class="col-sm-4">
                    <select name="order_status_id" class="form-control">
                        <?php foreach ($order_statuses as $order_status) { ?>
                            <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Resi Pengiriman</label>
                <div class="col-sm-6">
                    <input name="waybill" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Catatan</label>
                <div class="col-sm-9">
                    <textarea name="comment" class="form-control" rows="8"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Notifikasi</label>
                <div class="col-sm-9">
                    <div class="checkbox"><label><input type="checkbox" name="notify" value="1"> Notifikasi ke pembeli
                            dan penjual</label></div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3"></label>
                <div class="col-sm-9">
                    <a class="btn btn-primary" id="button-status">Update Status</a>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>
<div id="modal"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('order/history')?>",
                "type": "POST",
                "data": function (d) {
                    d.order_id = "<?=$order_id?>";
                }
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "date_added"},
                {"orderable": false, "searchable": false, "data": "status"},
                {"orderable": false, "searchable": false, "data": "updater"},
                {"orderable": false, "searchable": false, "data": "comment"},
                {"orderable": false, "searchable": false, "data": "notify"},
            ],
            "createdRow": function (row, data, index) {
                if (data.notify == '1') {
                    $('td', row).eq(4).html('<i class="fa fa-check" style="color:green;"></i>');
                } else {
                    $('td', row).eq(4).html('');
                }
            },
            "sDom": '<"table-responsive" t>p',
            "order": [[0, 'desc']],
        });
    });

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }

    $('#button-status').on('click', function () {
        var btn = $(this);
        $.ajax({
            url: $('#form-status').attr('action'),
            data: $('#form-status').serialize(),
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('html, body').animate({scrollTop: 0}, 'slow');
                if (json['success']) {
                    $('#notification').html('<div class="alert alert-success">' + json['success'] + '</div>');
                    $('#order-status').html($('select[name=\'order_status_id\'] option:selected').text());
                    refreshTable();
                }

                if (json['error']) {
                    $('#notification').html('<div class="alert alert-danger">' + json['error'] + '</div>');
                    refreshTable();
                }
            }
        });
    });

    $('#track').on('click', function () {
        var btn = $(this);

        $.ajax({
            url: "<?=site_url('tool/shipping/track')?>",
            data: 'order_id=<?=$order_id?>',
            dataType: 'html',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (html) {
                $('#modal').html('<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">' + html + '</div>');
                $('#form-modal').modal('show');
                $('#form-modal').on('hidden.bs.modal', function (e) {
                    refreshTable();
                });
            }
        });
    });
</script>