<div class="page-title">
    <div>
        <h1>Pesanan</h1>
    </div>
    <div class="btn-group">
        <a onclick="refreshTable();" class="btn btn-info"><i class="fa fa-refresh"></i> Refresh</a>
        <a onclick="$('#form-checks').submit();" class="btn btn-warning"><i class="fa fa-refresh"></i> Print Pengiriman</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id="message"></div>
                <?php if ($success) { ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                        </button><?= $success ?></div>
                <?php } ?>
                <?php if ($error) { ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                        </button><?= $error ?></div>
                <?php } ?>
                <?= form_open(admin_url('order/shipping'), 'id="form-checks" target="_blank"') ?>
                <table class="table table-hover table-bordered" id="datatable" width="100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="select-all"></th>
                        <th>ID Pesanan</th>
                        <th>Tanggal</th>
                        <th>Pelanggan</th>
                        <th class="text-right">Total</th>
                        <th>Status</th>
                        <th class="text-right" style="min-width:15%;"></th>
                    </tr>
                    </thead>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=admin_url('order')?>",
                "type": "POST",
            },
            "columns": [
                {"orderable": false, "data": "order_id"},
                {"data": "order_id"},
                {"data": "date_added"},
                {"data": "name"},
                {"data": "total"},
                {"data": "status"},
                {"orderable": false, "data": "order_id"},
            ],
            "sDom": 'lfr<"table-responsive" t>ip',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<input type="checkbox" class="checkbox" name="order_id[]" value="' + data.order_id + '"/>');
                $('td', row).eq(1).html('<a href="<?=admin_url('order/detail/\'+data.order_id+\'')?>" data-toggle="tooltip" title="Lihat detil">' + data.order_id + '</a>');
                $('td', row).eq(3).addClass('text-right');

                html = '<div class="btn-group btn-group-xs">';
                html += '<a href="<?=admin_url('order/detail/\'+data.order_id+\'')?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detil</a>';
                html += ' <a onclick="delRecord(' + data.order_id + ');" class="btn btn-warning btn-xs"><i class="fa fa-minus-circle"></i> Hapus</a></div>';

                $('td', row).eq(6).addClass('text-right').html(html);
            },
            "order": [[6, 'desc']],
        });

        $('#select-all').click(function () {
            var checkBoxes = $('.checkbox');
            checkBoxes.prop('checked', !checkBoxes.prop('checked'));
        });
    });

    function delRecord(order_id) {
        if (order_id) {
            data = 'order_id=' + order_id;
        } else {
            if ($('.check:checked').length === 0) {
                alert('Tidak ada order yang dipilih!');
                return false;
            }
            data = $('#form-checks').serialize();
        }

        swal({
            title: "Apakah anda yakin?",
            text: "Data yang sudah dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?=admin_url('order/delete')?>',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    success: function (json) {
                        if (json['success']) {
                            swal("Terhapus!", json['success'], "success");
                        } else if (json['error']) {
                            swal("Error!", json['error'], "error");
                        } else if (json['redirect']) {
                            window.location = json['redirect'];
                        }
                        refreshTable();
                    }
                });
            }
        });
    }

    function actionSuccess(json) {
        if (json['success']) {
            $('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
        } else if (json['error']) {
            $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
        } else if (json['redirect']) {
            window.location = json['redirect'];
        }
        refreshTable();
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }
</script>