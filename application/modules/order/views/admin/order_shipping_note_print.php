<!DOCTYPE html>
<html style="margin:3px;">
<head>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style>
        @page {
            margin: 3px;
        }

        body {
            margin: 3px;
            font-family: Arial;
            font-size: 12px;
            color: #000000;
        }

        .location {
            width: 100%;
        }

        .user {
            width: 100%;
            border: 1px solid #DDDDDD;
            padding: 5px;
        }

        .user th {
            text-align: left;
            font-size: 14px;
            font-weight: bold;
        }

        hr {
            border-top: 1px dashed #ccc;
            border-bottom: none;
            margin: 15px 0;
        }

        .header {
            text-align: left;
            font-size: 14px;
            font-weight: bold;
        }

        .product {
            border-collapse: collapse;
            width: 100%;
            border-top: 1px solid #DDDDDD;
            border-left: 1px solid #DDDDDD;
        }

        .product th {
            font-size: 12px;
            border-right: 1px solid #DDDDDD;
            border-bottom: 1px solid #DDDDDD;
            background-color: #EFEFEF;
            font-weight: bold;
            text-align: left;
            padding: 7px;
            color: #222222;
        }

        .product td {
            font-size: 12px;
            border-right: 1px solid #DDDDDD;
            border-bottom: 1px solid #DDDDDD;
            text-align: left;
            padding: 7px;
            vertical-align: top;
        }

        .left {
            text-align: left
        }

        .right {
            text-align: right
        }

        .clear {
            clear: both;
        }

        .center {
            text-align: center;
        }

        p {
            float: left;
            max-width: 45%;
            margin: 2em 5% 2em 0;
        }

        th.head {
            text-transform: uppercase;
            font-size: 18px;
        }

        .box {
            width: 49.8%;
            height: auto;
        }

        .box-body {
            border: 1px dashed #ddd;
            padding: 5px;
        }

        .logo {
            width: 150px;
            height: auto;
        }
    </style>
</head>
<body>
<div id="divslip">
    <a style="color:green;text-decoration:none;" id="cetakslip" href="javascript:window.print()"
       onclick="disapear()"><span class="fa fa-print"></span> Cetak </a>
</div>
<?php foreach (array_chunk($orders, ceil(count($orders) / 2)) as $order_list) { ?>
    <div id="slip" class="center clear">
        <?php foreach ($order_list as $order) { ?>
            <div class="box">
                <div class="box-body">
                    <table width="100%">
                        <tr>
                            <th class="left"><img class="logo" src="<?= $logo ?>"/></th>
                            <th class="head">Note #<?php echo $order['order_id']; ?></th>
                        </tr>
                    </table>
                    <table class="product">
                        <tr>
                            <th>Pengirim</th>
                            <th>Penerima</th>
                        </tr>
                        <tr>
                            <td>
                                <address>
                                    <strong><?= $this->config->item('site_name') ?></strong><br/>
                                    <?= $this->config->item('address') ?>
                                </address>
                                No. Telepon: <?= $this->config->item('telephone') ?><br/>
                                Email: <?= $this->config->item('email') ?><br/>
                                Website: <a href="<?= site_url() ?>"><?= site_url() ?></a>
                            </td>
                            <td><?php echo $order['shipping_address']; ?><br/><br/>
                                No. Telepon: <?php echo $order['telephone']; ?>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">Detil Pesanan</th>
                        </tr>
                        <tr>
                            <td>
                                <b>Tanggal:</b> <?php echo $order['date_added']; ?><br/>
                                <?php if ($order['invoice']) { ?>
                                    <b>No. Invoice:</b> <?php echo $order['invoice']; ?><br/>
                                <?php } ?>
                                <b>ID Pesanan:</b> <?php echo $order['order_id']; ?>
                            </td>
                            <td>
                                <?php if ($order['shipping_method']) { ?>
                                    <b>Metode Pengiriman:</b> <?php echo $order['shipping_method']; ?><br/>
                                <?php } ?>
                            </td>
                        </tr>
                    </table>
                    <table class="product">
                        <thead>
                        <tr>
                            <th class="left">Produk</th>
                            <th class="left">SKU</th>
                            <th class="left">Qty</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($order['products'] as $product) { ?>
                            <tr>
                                <td class="left"><?php echo $product['name']; ?>
                                    <?php foreach ($product['option'] as $option) { ?>
                                        <br/>&nbsp;
                                        <small> - <?php echo $option['name']; ?>
                                            : <?php echo $option['value']; ?></small>
                                    <?php } ?>
                                </td>
                                <td><?php echo $product['sku']; ?></td>
                                <td class="left"><?php echo $product['quantity']; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>
</body>

<script type="text/javascript">
    function disapear() {
        document.getElementById('divslip').style.display = "none";
        setTimeout(
            function () {
                document.getElementById('divslip').style.display = 'block';
            }, 25000);
    }
</script>
</html>