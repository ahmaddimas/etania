<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= $this->config->item('company') ?></title>
</head>
<body style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; color: #000000;">
<table width="100%" style="margin-bottom:20px;">
    <tbody>
    <tr>
        <td style="border-bottom: 1px solid #ddd; text-align:center; padding:5px;">
            <a><img src="<?= $logo ?>" alt="<?= $this->config->item('company') ?>" style="border: none;"/></a>
        </td>
    </tr>
    </tbody>
</table>
<p style="margin-top: 0px; margin-bottom: 20px;">Hai <strong><?= $name ?></strong>,</p>
<p style="margin-top: 0px; margin-bottom: 20px;">Terimakasih telah berbelanja di <?= $this->config->item('company') ?>.
    Berikut ini adalah status terbaru dari pesanan Anda:</p>
<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
    <tr>
        <td style="font-size: 14px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;"
            colspan="2">Detail Pesanan
        </td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="font-size: 14px; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><b>Invoice
                :</b> <?= $invoice ?><br/>
            <b>Tanggal:</b> <?= $date_added ?><br/>
            <b>Total Order:</b> <?= $total ?><br/>
            <b>Metode Pembayaran:</b> <?= $payment_method ?>
        </td>
        <td style="font-size: 14px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
            <b>Email :</b> <?= $email ?><br/>
            <b>Telepon: </b> <?= $telephone ?><br/>
            <b>IP:</b> <?= $ip_address ?><br/></td>
    </tr>
    </tbody>
</table>
<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
    <tr>
        <td style="font-size: 14px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">
            Status Pesanan
        </td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="font-size: 14px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?= $status ?></td>
    </tr>
    </tbody>
</table>

<?php if ($waybill) { ?>
    <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
        <thead>
        <tr>
            <td style="font-size: 14px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">
                No resi pengiriman
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td style="font-size: 14px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?= $waybill ?></td>
        </tr>
        </tbody>
    </table>
<?php } ?>

<?php if ($comment) { ?>
    <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
        <thead>
        <tr>
            <td style="font-size: 14px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">
                Keterangan
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td style="font-size: 14px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?= $comment ?></td>
        </tr>
        </tbody>
    </table>
<?php } ?>

<p style="margin-top: 0px; margin-bottom: 20px;">Apabila ada pertanyaan, silakan menghubungi kami melalui
    email <?= $this->config->item('email') ?>, atau melalui telepon di nomor <?= $this->config->item('telephone') ?></p>
<p style="margin-top: 0px; margin-bottom: 20px;"><em>– <?= $this->config->item('company') ?></em</p>
<table width="100%">
    <tr>
        <td style="border-top:1px solid #ddd; font-size:12px; color:#666;" align="center">
            <p>Copyright &copy;2016 <a href="<?= site_url() ?>"
                                       style="text-decoration:none; color:#666;"><?= $this->config->item('company') ?></a>.
                All Rights Reserved.</p>
        </td>
    </tr>
</table>
</body>
</html>