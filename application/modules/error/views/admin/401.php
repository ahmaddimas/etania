<div class="page-error" style="min-height:100%;">
    <h1><i class="fa fa-ban fa-5x" style="color:#bbb;"></i></h1>
    <h2>Akses ditolak!</h2>
    <p>Anda tidak diijinkan untuk mengakses halaman ini!</p>
    <p><a href="<?= admin_url() ?>">Kembali ke dashboard</a></p>
</div>