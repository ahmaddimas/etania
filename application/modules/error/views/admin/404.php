<div class="page-error" style="min-height:100%;">
    <h1 style="font-size:172px; font-family:'Arial Black';">404</h1>
    <p>Halaman tidak tidak ditemukan atau tidak tersedia!</p>
    <p><a href="<?= admin_url() ?>">Kembali ke dashboard</a></p>
</div>