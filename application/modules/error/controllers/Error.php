<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Error class
 *
 * @extends MY_Controller
 */
class Error extends MY_Controller
{
    /**
     * _error
     *
     * @var mixed
     * @access private
     */
    private $_error;

    /**
     * Contructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        switch ($this->uri->segment(1)) {
            case PATH_ADMIN :
                $this->_error = new Admin_Error;
                break;
            default :
                $this->_error = new Front_Error;
                break;
        }
    }

    /**
     * index function.
     *
     * @access public
     * @return void
     */
    public function index()
    {
        show_404($this->uri->uri_string());
    }

    /**
     * _404 function.
     *
     * @access public
     * @param string $page (default: '')
     * @param bool $log_error (default: false)
     * @return void
     */
    public function _404($page = '', $log_error = false)
    {
        if ($log_error) log_message('error', '404 Page Not Found --> ' . $page);

        $this->_error->view(404);
    }

    /**
     * _401 function.
     *
     * @access public
     * @param string $page (default: '')
     * @param bool $log_error (default: false)
     * @return void
     */
    public function _401($page = '', $log_error = false)
    {
        if ($log_error) log_message('error', '401 Unauthorized --> ' . $page . ' [USERID :' . $this->auth->user_id() . ']');

        $this->_error->view(401);
    }
}

/**
 * Admin error class.
 *
 * @extends Admin_Controller
 */
class Admin_Error extends Admin_Controller
{
    public function view($code = '')
    {
        $this->output->set_status_header($code);

        if ($this->admin_auth->is_logged()) {
            $output = $this->load
                ->title('Error ' . $code)
                ->view('admin/' . $code, array(), true);
        } else {
            $output = $this->load
                ->title('Error ' . $code)
                ->layout('default')
                ->desktop()
                ->view($code, array(), true);
        }

        exit($output);
    }
}

/**
 * Public error class.
 *
 * @extends Front_Controller
 */
class Front_Error extends Front_Controller
{
    public function view($code = '')
    {
        $this->output->set_status_header($code);

        exit($this->load
            ->title('Error ' . $code)
            ->view($code, array(), true));
    }
}
