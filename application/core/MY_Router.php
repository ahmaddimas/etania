<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'third_party/MX/Router.php';

class MY_Router extends MX_Router
{
    protected function _parse_routes()
    {
        $uri = implode('/', $this->uri->segments);

        $http_verb = isset($_SERVER['REQUEST_METHOD']) ? strtolower($_SERVER['REQUEST_METHOD']) : 'cli';

        foreach ($this->routes as $key => $val) {
            if (is_array($val)) {
                $val = array_change_key_case($val, CASE_LOWER);

                if (isset($val[$http_verb])) {
                    $val = $val[$http_verb];
                } else {
                    continue;
                }
            }

            $key = str_replace(':any', '.+', str_replace(':num', '[0-9]+', $key));

            if (preg_match('#^' . $key . '$#', $uri, $matches)) {
                if (!is_string($val) && is_callable($val)) {
                    array_shift($matches);

                    $val = call_user_func_array($val, $matches);
                } elseif (strpos($val, '$') !== FALSE && strpos($key, '(') !== FALSE) {
                    $val = preg_replace('#^' . $key . '$#', $val, $uri);
                }

                $this->_set_request(explode('/', $val));
                return;
            }
        }

        $this->_set_request(array_values($this->uri->segments));
    }
}