<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Exceptions Class
 *
 * @package        CodeIgniter
 * @subpackage    Libraries
 * @category    Exceptions
 * @author        ExpressionEngine Dev Team
 * @link        http://codeigniter.com/user_guide/libraries/exceptions.html
 */
class MY_Exceptions extends CI_Exceptions
{
    /**
     * 404 Page Not Found Handler
     *
     * @access    private
     * @param    string    the page
     * @return    string
     */
    function show_404($page = '', $log_error = true)
    {
        Modules::run('error/_404', $page, $log_error);
    }

    /**
     * 401 Unauthorized Page Handler
     *
     * @access public
     * @param string $role
     * @return void
     */
    function show_401($page = '', $log_error = true)
    {
        Modules::run('error/_401', $page, $log_error);
    }

    /**
     * Under maintenance
     *
     * @access public
     * @param string $role
     * @return void
     */
    function under_maintenance()
    {
        Modules::run('common/maintenance/index');
    }
}