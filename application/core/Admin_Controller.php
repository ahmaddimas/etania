<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends MY_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $installed = $this->config->item('system_installed');
        if ($installed == 0) {
            redirect('install');
        }

        $this->lang->load('admin');
        $this->load->library('admin/admin_auth');
        $this->load->layout('admin');

        $this->_check_access();

        // global variables
        $this->load->vars(array(
            '_name' => $this->admin_auth->name(),
            '_image' => $this->admin_auth->image()
        ));
    }

    /**
     * Check access permission
     *
     * @access private
     * @return void
     */
    private function _check_access()
    {
        $ignored_pages = array(PATH_ADMIN . '/login', PATH_ADMIN . '/logout', PATH_ADMIN . '/reset');
        $current_page = $this->uri->segment(1, '') . '/' . $this->uri->segment(2, 'index');

        if (in_array($current_page, $ignored_pages)) {
            if ($this->admin_auth->is_logged() && $current_page != PATH_ADMIN . '/logout') {
                redirect(PATH_ADMIN);
            }

            return true;
        }

        if (!$this->admin_auth->is_logged() && $current_page != PATH_ADMIN . '/login') {
            $this->session->set_userdata('admin_redirect', $this->uri->uri_string());
            redirect(PATH_ADMIN . '/login');
        }

        return true;
    }
}