<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class API_Controller extends REST_Controller
{
    protected $admin;

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('admin/admin_model');

        if ($this->jwt_token()) {
            $data = $this->jwt_decode($this->jwt_token());

            $this->admin = $this->admin_model->get_admin($data['admin_id']);
        }
    }
}