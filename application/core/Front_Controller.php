<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_Controller extends MY_Controller
{
    protected $is_mobile = false;

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $installed = $this->config->item('system_installed');
        if ($installed == 0) {
            redirect('install');
        }

        $this->load->library('asset');
        $this->load->library('shopping_cart');
        $this->load->library('customer/customer');

        $this->_check_access();

        $this->load->vars(array(
            '_name' => $this->customer->name(),
            '_image' => $this->customer->image(),
            '_is_logged' => $this->customer->is_logged(),
            '_categories' => $this->_get_categories(),
            '_get_menu' => $this->_get_menu(),
            '_get_header_banner' => $this->_get_header_banner(),
        ));

        $this->load->vars('category_menu', $this->_get_categories());
        $this->load->vars('menu', $this->_get_menu());

        $settings = $this->setting_model->get_settings();
        if (isset($settings['addon_whatsapp'])) {
            $this->load->library('whatsapp/whatsapp');
            $this->whatsapp->install($this);
        }

        $this->load->desktop();
        $this->load->model('page/page_model');
        $this->load->vars('page_menu', $this->page_model->get_page_menus());
        $this->load->vars('header_banners', $this->_get_header_banner());


        if ($this->agent->is_mobile()) {
            $this->load->mobile();
            Asset::set_path('mobile');
            $this->is_mobile = true;
        }

        $this->lang->load('front');
    }

    /**
     * Check access permission
     *
     * @access private
     * @return void
     */
    private function _check_access()
    {
        if ($this->uri->segment(1) == PATH_USER) {
            $ignored_pages = array(PATH_USER . '/login', PATH_USER . '/register', PATH_USER . '/facebook_login', PATH_USER . '/google_login', PATH_USER . '/logout', PATH_USER . '/reset');
            $current_page = $this->uri->segment(1, '') . '/' . $this->uri->segment(2, 'index');

            if (in_array($current_page, $ignored_pages)) {
                if ($this->customer->is_logged() && $current_page != PATH_USER . '/logout') {
                    redirect(PATH_USER);
                }

                return true;
            }

            if (!$this->customer->is_logged() && $current_page != 'user/login') {
                $this->session->set_userdata('redirect', $this->uri->uri_string());
                redirect(PATH_USER . '/login');
            }

            return true;
        } else {
            $this->load->layout('default');
        }
    }

    /**
     * Get category menu
     *
     * @access public
     * @return void
     */
    public function _get_categories()
    {
        $this->load->model('catalog/category_model');

        $data['categories'] = array();

        foreach ($this->category_model->get_subcategories(0, 24) as $category) {
            $children = array();

            foreach ($this->category_model->get_subcategories($category['category_id'], 24) as $subcategory) {
                $children2 = array();

                foreach ($this->category_model->get_subcategories($subcategory['category_id'], 24) as $subcategory2) {
                    $children2[] = array(
                        'name' => $subcategory2['name'],
                        'slug' => $category['slug'],
                        'href' => site_url('category/' . $category['slug'] . '/' . $subcategory['slug'] . '/' . $subcategory2['slug']),
                        'children' => array()
                    );
                }

                $children[] = array(
                    'name' => $subcategory['name'],
                    'slug' => $category['slug'],
                    'href' => site_url('category/' . $category['slug'] . '/' . $subcategory['slug']),
                    'children' => $children2
                );
            }

            $data['categories'][] = array(
                'name' => $category['name'],
                'slug' => $category['slug'],
                'href' => site_url('category/' . $category['slug']),
                'children' => $children,
                'menu_image' => $category['menu_image']
            );
        }


        if ($this->config->item('addon_offers')) {
            $datar['categories'][] = array(
                'name' => 'Special Offer',
                'menu_image' => 'fa-caret-right',
                'slug' => 'specials',
                'href' => site_url('offers/specials'),
                'children' => array()
            );
            $data['categories'] = array_merge($data['categories'], $datar['categories']);
        }

        return $data['categories'];
    }


    public function _get_menu()
    {
        $menu = $this->db->select('menu_id, title, sort_order, type, active, slug, page_id, icon')
            ->where('sub_id', 0)
            ->order_by('sort_order', 'asc')
            ->get('menu')
            ->result_array();

        $data['menu'] = array();
        foreach ($menu as $mn) {
            $menuschild = array();
            $menuchild = $this->db->select('menu_id, title, sort_order, type, active, slug, page_id, icon')
                ->where('sub_id', $mn['menu_id'])
                ->order_by('sort_order', 'asc')
                ->get('menu')
                ->result_array();
            foreach ($menuchild as $mnc) {
                $menuschild[] = array(
                    'title' => $mnc['title'],
                    'type' => $mnc['type'],
                    'icon' => $mnc['icon'],
                    'href' => $mnc['slug'],
                );
            }

            $data['menu'][] = array(
                'title' => $mn['title'],
                'type' => $mn['type'],
                'icon' => $mn['icon'],
                'href' => $mn['slug'],
                'children' => $menuschild,
            );
        }

        return $data['menu'];
    }


    public function _get_header_banner()
    {

        $this->load->model('banner/banner_model');
        $data['header_banners'] = array();

        $header_banners_active = $this->banner_model->get($this->config->item('header_banner_id'));
        if ($header_banners_active['active']) {

            $header_banners = $this->banner_model->get_images($this->config->item('header_banner_id'));
            foreach ($header_banners as $banner) {
                if ($banner['active']) {
                    if ($banner['image']) {
                        $image = $banner['image'];
                    } else {
                        $image = 'no_image.jpg';
                    }

                    $data['header_banners'][] = array(
                        'image' => $this->image->resize($banner['image'], 130, 30),
                        'title' => $banner['title'],
                        'subtitle' => $banner['subtitle'],
                        'link' => $banner['link'],
                        'link_title' => $banner['link_title'],
                    );
                }
            }
        }

        return $data['header_banners'];
    }
}