<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Install_Controller extends MY_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('asset');
        $this->load->install();
        Asset::set_path('install');

    }

}