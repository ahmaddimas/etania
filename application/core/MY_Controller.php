<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");

class MY_Controller extends MX_Controller
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->_set_config();
    }

    /**
     * Set config from database
     *
     * @access private
     * @return void
     */
    private function _set_config()
    {
        $this->load->model('system/setting_model');

        foreach ($this->setting_model->get_settings() as $group => $setting) {
            foreach ($setting as $key => $value) {
                if ($group == 'config' && !$this->config->item($key)) {
                    $this->config->set_item($key, $value);
                } else {
                    $this->config->set_item($group, $setting);
                }
            }
        }

        $this->load->library('image');

        $this->load->vars(array(
            '_logo' => $this->image->resize($this->config->item('logo'), 200),
            '_favicon' => $this->image->resize($this->config->item('favicon'), 32, 32),
            '_copyright' => '&copy;' . date('Y', time()) . ' <a href="' . site_url() . '">' . $this->config->item('company') . '</a>'
        ));
    }
}