<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title><?= $template['title'] ?></title>
    <link href="<?= $_favicon ?>" rel="icon"/>
    <base href="<?= admin_url() ?>"/>
    <link href="<?= base_url('assets/css/admin.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/plugins/summernote/summernote.css') ?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/essential-plugins.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/summernote/summernote.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/bootstrap-datepicker.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/bootstrap-datepicker.id.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/pace.min.js') ?>"></script>

    <?= $template['css'] ?>
    <?= $template['js'] ?>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper">
    <header class="main-header hidden-print"><a href="<?= admin_url() ?>" class="logo"><img src="<?= $_logo ?>"
                                                                                            style="height:28px;"></a>
        <nav class="navbar navbar-static-top">
            <a href="#" data-toggle="offcanvas" class="sidebar-toggle"></a>
            <div class="navbar-custom-menu">
                <ul class="top-nav">
                    <li class="dropdown"><a href="<?= admin_url() ?>" data-toggle="dropdown" role="button"
                                            aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i
                                    class="fa fa-user fa-lg"></i></a>
                        <ul class="dropdown-menu settings-menu">
                            <li><a href="<?= admin_url('settings') ?>"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                            <li><a href="<?= admin_url('profile') ?>"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                            <li><a href="<?= admin_url('logout') ?>"><i class="fa fa-sign-out fa-lg"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar hidden-print">
        <?= Modules::run('navigation/admin/sidebar/index') ?>
    </aside>
    <div class="content-wrapper">
        <?= $template['body'] ?>
    </div>
</div>

<script src="<?= base_url('assets/plugins/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/dataTables.bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/sweetalert.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/admin.js') ?>"></script>
</body>
</html>