<div class="page-error">
    <h1 style="font-size:172px; font-family:'Arial Black';">401</h1>
    <p>Access denied! Please contact your system administrator</p>
    <p><a href="javascript:window.history.back();">Go back to previous page</a></p>
</div>