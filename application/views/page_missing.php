<div class="body-content outer-top-bd">
    <div class="container">
        <div class="x-page inner-bottom-sm">
            <div class="row">
                <div class="col-md-12 x-text text-center">
                    <h1>404</h1>
                    <p>Maaf, halaman yang anda cari tidak ditemukan.</p>
                    <a href="<?= site_url() ?>"><i class="fa fa-home"></i> Kembali ke halaman depan</a>
                </div>
            </div>
        </div>
    </div>
</div>