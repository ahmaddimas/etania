-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2017 at 10:07 AM
-- Server version: 5.6.16
-- PHP Version: 5.6.25

SET
SQL_MODE
=
"NO_AUTO_VALUE_ON_ZERO";
SET
time_zone
=
"+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `medansoft-2`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon`
--

CREATE TABLE IF NOT EXISTS `addon`
(
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `description` text,
  `version` varchar
(
  32
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `code`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address`
(
  `address_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `address` varchar
(
  128
) NOT NULL DEFAULT '',
  `telephone` varchar
(
  32
) NOT NULL DEFAULT '',
  `postcode` varchar
(
  10
) NOT NULL DEFAULT '',
  `country_id` int
(
  11
) NOT NULL DEFAULT '0',
  `province_id` int
(
  11
) NOT NULL DEFAULT '0',
  `city_id` int
(
  11
) NOT NULL DEFAULT '0',
  `subdistrict_id` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `address_id`
),
  KEY `customer_id`
(
  `customer_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Tabel alamat' AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin`
(
  `admin_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `admin_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `image` text,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `email` varchar
(
  255
) NOT NULL DEFAULT '',
  `username` varchar
(
  32
) NOT NULL DEFAULT '',
  `password` varchar
(
  255
) NOT NULL DEFAULT '',
  `salt` varchar
(
  9
) NOT NULL DEFAULT '',
  `code_activation` varchar
(
  40
) NOT NULL DEFAULT '',
  `code_forgotten` varchar
(
  40
) NOT NULL DEFAULT '',
  `ip` varchar
(
  64
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `admin_id`
),
  KEY `email`
(
  `email`,
  `username`,
  `active`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=2;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_group_id`, `image`, `name`, `email`, `username`, `password`, `salt`,
                     `code_activation`, `code_forgotten`, `ip`, `active`, `date_added`, `date_modified`, `last_login`)
VALUES
(-1, 0, 'admin/image--1.png', 'Administrator', 'admin@medansoft.com', 'admin',
 '5df933e0d0a3a9d30591d99447ed1e8c85a03f6c', 'd78e0b8a9', '', '', '127.0.0.1', 1, '2017-10-15 12:12:57',
 '2017-10-30 20:25:10', '2017-11-27 10:03:06');

-- --------------------------------------------------------

--
-- Table structure for table `admin_group`
--

CREATE TABLE IF NOT EXISTS `admin_group`
(
  `admin_group_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `permission` text,
  PRIMARY KEY
(
  `admin_group_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel Grup Pengguna' AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

CREATE TABLE IF NOT EXISTS `attribute`
(
  `attribute_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  64
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL,
  PRIMARY KEY
(
  `attribute_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_group`
--

CREATE TABLE IF NOT EXISTS `attribute_group`
(
  `attribute_group_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  64
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL,
  PRIMARY KEY
(
  `attribute_group_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner`
(
  `banner_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `description` text,
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `banner_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `banner_image`
--

CREATE TABLE IF NOT EXISTS `banner_image`
(
  `banner_image_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `banner_id` int
(
  11
) NOT NULL DEFAULT '0',
  `title` varchar
(
  64
) NOT NULL DEFAULT '',
  `subtitle` varchar
(
  255
) NOT NULL DEFAULT '',
  `link` varchar
(
  255
) NOT NULL DEFAULT '',
  `link_title` varchar
(
  255
) NOT NULL DEFAULT '',
  `image` varchar
(
  255
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `banner_image_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category`
(
  `category_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `description` text,
  `meta_description` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_keyword` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_title` varchar
(
  255
) NOT NULL DEFAULT '',
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `image` varchar
(
  255
) NOT NULL DEFAULT '',
  `menu_image` varchar
(
  255
) NOT NULL DEFAULT '',
  `parent_id` int
(
  11
) NOT NULL DEFAULT '0',
  `top` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `column` int
(
  3
) NOT NULL DEFAULT '0',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `category_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `category_path`
--

CREATE TABLE IF NOT EXISTS `category_path`
(
  `category_id` int
(
  11
) NOT NULL DEFAULT '0',
  `path_id` int
(
  11
) NOT NULL DEFAULT '0',
  `level` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `category_id`,
  `path_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency`
(
  `currency_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `title` varchar
(
  32
) NOT NULL DEFAULT '',
  `code` varchar
(
  3
) NOT NULL DEFAULT '',
  `symbol` varchar
(
  12
) NOT NULL DEFAULT '',
  `value` float
(
  15,
  8
) NOT NULL DEFAULT '0.00000000',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `currency_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=9;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`currency_id`, `title`, `code`, `symbol`, `value`, `active`, `date_modified`)
VALUES
  (8, 'Rupiah', 'IDR', 'Rp', 1.00000000, 1, '2017-04-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer`
(
  `customer_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `customer_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `bank_code` varchar
(
  5
) NOT NULL DEFAULT '',
  `bank_name` varchar
(
  255
) NOT NULL DEFAULT '',
  `bank_account_name` varchar
(
  64
) NOT NULL DEFAULT '',
  `bank_account_number` varchar
(
  64
) NOT NULL DEFAULT '',
  `address_id` int
(
  11
) NOT NULL DEFAULT '0',
  `type` enum
(
  'b',
  's'
) NOT NULL DEFAULT 'b',
  `image` text,
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `email` varchar
(
  255
) NOT NULL DEFAULT '',
  `gender` enum
(
  'm',
  'f'
) NOT NULL DEFAULT 'm',
  `telephone` varchar
(
  32
) NOT NULL DEFAULT '',
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `username` varchar
(
  32
) NOT NULL DEFAULT '',
  `password` varchar
(
  255
) NOT NULL DEFAULT '',
  `salt` varchar
(
  9
) NOT NULL DEFAULT '',
  `code_activation` varchar
(
  40
) NOT NULL DEFAULT '',
  `code_forgotten` varchar
(
  40
) NOT NULL DEFAULT '',
  `ip` varchar
(
  64
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `verified` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `newsletter` tinyint
(
  1
) NOT NULL DEFAULT '1',
  PRIMARY KEY
(
  `customer_id`
),
  KEY `email`
(
  `email`,
  `username`,
  `active`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_group`
--

CREATE TABLE IF NOT EXISTS `customer_group`
(
  `customer_group_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  32
) NOT NULL DEFAULT '',
  `description` text,
  `approval` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `customer_group_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

--
-- Dumping data for table `customer_group`
--

INSERT INTO `customer_group` (`customer_group_id`, `name`, `description`, `approval`)
VALUES
  (3, 'Default', NULL, 1),
  (4, 'Reseller', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_history`
--

CREATE TABLE IF NOT EXISTS `customer_history`
(
  `customer_history_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `comment` text,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `customer_history_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_transaction`
--

CREATE TABLE IF NOT EXISTS `customer_transaction`
(
  `customer_transaction_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `reference` varchar
(
  32
) NOT NULL DEFAULT '',
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `description` text,
  `amount` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_paid` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_method` varchar
(
  32
) NOT NULL DEFAULT '',
  `payment_code` varchar
(
  32
) NOT NULL DEFAULT '',
  `approved` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `customer_transaction_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE IF NOT EXISTS `filter`
(
  `filter_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `filter_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  PRIMARY KEY
(
  `filter_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `filter_group`
--

CREATE TABLE IF NOT EXISTS `filter_group`
(
  `filter_group_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  PRIMARY KEY
(
  `filter_group_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `length_class`
--

CREATE TABLE IF NOT EXISTS `length_class`
(
  `length_class_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `title` varchar
(
  32
) NOT NULL DEFAULT '',
  `unit` varchar
(
  4
) NOT NULL DEFAULT '',
  `value` decimal
(
  15,
  4
) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY
(
  `length_class_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

--
-- Dumping data for table `length_class`
--

INSERT INTO `length_class` (`length_class_id`, `title`, `unit`, `value`)
VALUES
  (1, 'Milimeter', 'mm', '1.0000'),
  (2, 'Centimeter', 'cm', '0.1000');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location`
(
  `location_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `province_id` int
(
  11
) NOT NULL DEFAULT '0',
  `city_id` int
(
  11
) NOT NULL DEFAULT '0',
  `subdistrict_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  128
) NOT NULL DEFAULT '',
  `type` varchar
(
  32
) NOT NULL DEFAULT '',
  `postcode` varchar
(
  8
) NOT NULL DEFAULT '',
  PRIMARY KEY
(
  `location_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=291;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `province_id`, `city_id`, `subdistrict_id`, `name`, `type`, `postcode`)
VALUES
  (1, 1, 0, 0, 'Bali', 'Provinsi', ''),
  (2, 2, 0, 0, 'Bangka Belitung', 'Provinsi', ''),
  (3, 3, 0, 0, 'Banten', 'Provinsi', ''),
  (4, 4, 0, 0, 'Bengkulu', 'Provinsi', ''),
  (5, 5, 0, 0, 'DI Yogyakarta', 'Provinsi', ''),
  (6, 6, 0, 0, 'DKI Jakarta', 'Provinsi', ''),
  (7, 7, 0, 0, 'Gorontalo', 'Provinsi', ''),
  (8, 8, 0, 0, 'Jambi', 'Provinsi', ''),
  (9, 9, 0, 0, 'Jawa Barat', 'Provinsi', ''),
  (10, 10, 0, 0, 'Jawa Tengah', 'Provinsi', ''),
  (11, 11, 0, 0, 'Jawa Timur', 'Provinsi', ''),
  (12, 12, 0, 0, 'Kalimantan Barat', 'Provinsi', ''),
  (13, 13, 0, 0, 'Kalimantan Selatan', 'Provinsi', ''),
  (14, 14, 0, 0, 'Kalimantan Tengah', 'Provinsi', ''),
  (15, 15, 0, 0, 'Kalimantan Timur', 'Provinsi', ''),
  (16, 16, 0, 0, 'Kalimantan Utara', 'Provinsi', ''),
  (17, 17, 0, 0, 'Kepulauan Riau', 'Provinsi', ''),
  (18, 18, 0, 0, 'Lampung', 'Provinsi', ''),
  (19, 19, 0, 0, 'Maluku', 'Provinsi', ''),
  (20, 20, 0, 0, 'Maluku Utara', 'Provinsi', ''),
  (21, 21, 0, 0, 'Nanggroe Aceh Darussalam (NAD)', 'Provinsi', ''),
  (22, 22, 0, 0, 'Nusa Tenggara Barat (NTB)', 'Provinsi', ''),
  (23, 23, 0, 0, 'Nusa Tenggara Timur (NTT)', 'Provinsi', ''),
  (24, 24, 0, 0, 'Papua', 'Provinsi', ''),
  (25, 25, 0, 0, 'Papua Barat', 'Provinsi', ''),
  (26, 26, 0, 0, 'Riau', 'Provinsi', ''),
  (27, 27, 0, 0, 'Sulawesi Barat', 'Provinsi', ''),
  (28, 28, 0, 0, 'Sulawesi Selatan', 'Provinsi', ''),
  (29, 29, 0, 0, 'Sulawesi Tengah', 'Provinsi', ''),
  (30, 30, 0, 0, 'Sulawesi Tenggara', 'Provinsi', ''),
  (31, 31, 0, 0, 'Sulawesi Utara', 'Provinsi', ''),
  (32, 32, 0, 0, 'Sumatera Barat', 'Provinsi', ''),
  (33, 33, 0, 0, 'Sumatera Selatan', 'Provinsi', ''),
  (34, 34, 0, 0, 'Sumatera Utara', 'Provinsi', ''),
  (35, 6, 151, 0, 'Jakarta Barat', 'Kota', '11220'),
  (36, 6, 152, 0, 'Jakarta Pusat', 'Kota', '10540'),
  (37, 6, 153, 0, 'Jakarta Selatan', 'Kota', '12230'),
  (38, 6, 154, 0, 'Jakarta Timur', 'Kota', '13330'),
  (39, 6, 155, 0, 'Jakarta Utara', 'Kota', '14140'),
  (40, 6, 189, 0, 'Kepulauan Seribu', 'Kabupaten', '14550'),
  (41, 6, 152, 2095, 'Cempaka Putih', 'Kecamatan', ''),
  (42, 6, 152, 2096, 'Gambir', 'Kecamatan', ''),
  (43, 6, 152, 2097, 'Johar Baru', 'Kecamatan', ''),
  (44, 6, 152, 2098, 'Kemayoran', 'Kecamatan', ''),
  (45, 6, 152, 2099, 'Menteng', 'Kecamatan', ''),
  (46, 6, 152, 2100, 'Sawah Besar', 'Kecamatan', ''),
  (47, 6, 152, 2101, 'Senen', 'Kecamatan', ''),
  (48, 6, 152, 2102, 'Tanah Abang', 'Kecamatan', ''),
  (49, 11, 31, 0, 'Bangkalan', 'Kabupaten', '69118'),
  (50, 11, 42, 0, 'Banyuwangi', 'Kabupaten', '68416'),
  (51, 11, 51, 0, 'Batu', 'Kota', '65311'),
  (52, 11, 74, 0, 'Blitar', 'Kabupaten', '66171'),
  (53, 11, 75, 0, 'Blitar', 'Kota', '66124'),
  (54, 11, 80, 0, 'Bojonegoro', 'Kabupaten', '62119'),
  (55, 11, 86, 0, 'Bondowoso', 'Kabupaten', '68219'),
  (56, 11, 133, 0, 'Gresik', 'Kabupaten', '61115'),
  (57, 11, 160, 0, 'Jember', 'Kabupaten', '68113'),
  (58, 11, 164, 0, 'Jombang', 'Kabupaten', '61415'),
  (59, 11, 178, 0, 'Kediri', 'Kabupaten', '64184'),
  (60, 11, 179, 0, 'Kediri', 'Kota', '64125'),
  (61, 11, 222, 0, 'Lamongan', 'Kabupaten', '64125'),
  (62, 11, 243, 0, 'Lumajang', 'Kabupaten', '67319'),
  (63, 11, 247, 0, 'Madiun', 'Kabupaten', '63153'),
  (64, 11, 248, 0, 'Madiun', 'Kota', '63122'),
  (65, 11, 251, 0, 'Magetan', 'Kabupaten', '63314'),
  (66, 11, 255, 0, 'Malang', 'Kabupaten', '65163'),
  (67, 11, 256, 0, 'Malang', 'Kota', '65112'),
  (68, 11, 289, 0, 'Mojokerto', 'Kabupaten', '61382'),
  (69, 11, 290, 0, 'Mojokerto', 'Kota', '61316'),
  (70, 11, 305, 0, 'Nganjuk', 'Kabupaten', '64414'),
  (71, 11, 306, 0, 'Ngawi', 'Kabupaten', '63219'),
  (72, 11, 317, 0, 'Pacitan', 'Kabupaten', '63512'),
  (73, 11, 330, 0, 'Pamekasan', 'Kabupaten', '69319'),
  (74, 11, 342, 0, 'Pasuruan', 'Kabupaten', '67153'),
  (75, 11, 343, 0, 'Pasuruan', 'Kota', '67118'),
  (76, 11, 363, 0, 'Ponorogo', 'Kabupaten', '63411'),
  (77, 11, 369, 0, 'Probolinggo', 'Kabupaten', '67282'),
  (78, 11, 370, 0, 'Probolinggo', 'Kota', '67215'),
  (79, 11, 390, 0, 'Sampang', 'Kabupaten', '69219'),
  (80, 11, 409, 0, 'Sidoarjo', 'Kabupaten', '61219'),
  (81, 11, 418, 0, 'Situbondo', 'Kabupaten', '68316'),
  (82, 11, 441, 0, 'Sumenep', 'Kabupaten', '69413'),
  (83, 11, 444, 0, 'Surabaya', 'Kota', '60119'),
  (84, 11, 487, 0, 'Trenggalek', 'Kabupaten', '66312'),
  (85, 11, 489, 0, 'Tuban', 'Kabupaten', '62319'),
  (86, 11, 492, 0, 'Tulungagung', 'Kabupaten', '66212'),
  (87, 11, 51, 708, 'Batu', 'Kecamatan', ''),
  (88, 11, 51, 709, 'Bumiaji', 'Kecamatan', ''),
  (89, 11, 51, 710, 'Junrejo', 'Kecamatan', ''),
  (90, 2, 27, 0, 'Bangka', 'Kabupaten', '33212'),
  (91, 2, 28, 0, 'Bangka Barat', 'Kabupaten', '33315'),
  (92, 2, 29, 0, 'Bangka Selatan', 'Kabupaten', '33719'),
  (93, 2, 30, 0, 'Bangka Tengah', 'Kabupaten', '33613'),
  (94, 2, 56, 0, 'Belitung', 'Kabupaten', '33419'),
  (95, 2, 57, 0, 'Belitung Timur', 'Kabupaten', '33519'),
  (96, 2, 334, 0, 'Pangkal Pinang', 'Kota', '33115'),
  (97, 2, 27, 426, 'Bakam', 'Kecamatan', ''),
  (98, 2, 27, 427, 'Belinyu', 'Kecamatan', ''),
  (99, 2, 27, 428, 'Mendo Barat', 'Kecamatan', ''),
  (100, 2, 27, 429, 'Merawang', 'Kecamatan', ''),
  (101, 2, 27, 430, 'Pemali', 'Kecamatan', ''),
  (102, 2, 27, 431, 'Puding Besar', 'Kecamatan', ''),
  (103, 2, 27, 432, 'Riau Silip', 'Kecamatan', ''),
  (104, 2, 27, 433, 'Sungai Liat', 'Kecamatan', ''),
  (105, 33, 40, 0, 'Banyuasin', 'Kabupaten', '30911'),
  (106, 33, 121, 0, 'Empat Lawang', 'Kabupaten', '31811'),
  (107, 33, 220, 0, 'Lahat', 'Kabupaten', '31419'),
  (108, 33, 242, 0, 'Lubuk Linggau', 'Kota', '31614'),
  (109, 33, 292, 0, 'Muara Enim', 'Kabupaten', '31315'),
  (110, 33, 297, 0, 'Musi Banyuasin', 'Kabupaten', '30719'),
  (111, 33, 298, 0, 'Musi Rawas', 'Kabupaten', '31661'),
  (112, 33, 312, 0, 'Ogan Ilir', 'Kabupaten', '30811'),
  (113, 33, 313, 0, 'Ogan Komering Ilir', 'Kabupaten', '30618'),
  (114, 33, 314, 0, 'Ogan Komering Ulu', 'Kabupaten', '32112'),
  (115, 33, 315, 0, 'Ogan Komering Ulu Selatan', 'Kabupaten', '32211'),
  (116, 33, 316, 0, 'Ogan Komering Ulu Timur', 'Kabupaten', '32312'),
  (117, 33, 324, 0, 'Pagar Alam', 'Kota', '31512'),
  (118, 33, 327, 0, 'Palembang', 'Kota', '31512'),
  (119, 33, 367, 0, 'Prabumulih', 'Kota', '31121'),
  (120, 32, 12, 0, 'Agam', 'Kabupaten', '26411'),
  (121, 32, 93, 0, 'Bukittinggi', 'Kota', '26115'),
  (122, 32, 116, 0, 'Dharmasraya', 'Kabupaten', '27612'),
  (123, 32, 186, 0, 'Kepulauan Mentawai', 'Kabupaten', '25771'),
  (124, 32, 236, 0, 'Lima Puluh Koto/Kota', 'Kabupaten', '26671'),
  (125, 32, 318, 0, 'Padang', 'Kota', '25112'),
  (126, 32, 321, 0, 'Padang Panjang', 'Kota', '27122'),
  (127, 32, 322, 0, 'Padang Pariaman', 'Kabupaten', '25583'),
  (128, 32, 337, 0, 'Pariaman', 'Kota', '25511'),
  (129, 32, 339, 0, 'Pasaman', 'Kabupaten', '26318'),
  (130, 32, 340, 0, 'Pasaman Barat', 'Kabupaten', '26511'),
  (131, 32, 345, 0, 'Payakumbuh', 'Kota', '26213'),
  (132, 32, 357, 0, 'Pesisir Selatan', 'Kabupaten', '25611'),
  (133, 32, 394, 0, 'Sawah Lunto', 'Kota', '27416'),
  (134, 32, 411, 0, 'Sijunjung (Sawah Lunto Sijunjung)', 'Kabupaten', '27511'),
  (135, 32, 420, 0, 'Solok', 'Kabupaten', '27365'),
  (136, 32, 421, 0, 'Solok', 'Kota', '27315'),
  (137, 32, 422, 0, 'Solok Selatan', 'Kabupaten', '27779'),
  (138, 32, 453, 0, 'Tanah Datar', 'Kabupaten', '27211'),
  (139, 34, 15, 0, 'Asahan', 'Kabupaten', '21214'),
  (140, 34, 52, 0, 'Batu Bara', 'Kabupaten', '21655'),
  (141, 34, 70, 0, 'Binjai', 'Kota', '20712'),
  (142, 34, 110, 0, 'Dairi', 'Kabupaten', '22211'),
  (143, 34, 112, 0, 'Deli Serdang', 'Kabupaten', '20511'),
  (144, 34, 137, 0, 'Gunungsitoli', 'Kota', '22813'),
  (145, 34, 146, 0, 'Humbang Hasundutan', 'Kabupaten', '22457'),
  (146, 34, 173, 0, 'Karo', 'Kabupaten', '22119'),
  (147, 34, 217, 0, 'Labuhan Batu', 'Kabupaten', '21412'),
  (148, 34, 218, 0, 'Labuhan Batu Selatan', 'Kabupaten', '21511'),
  (149, 34, 219, 0, 'Labuhan Batu Utara', 'Kabupaten', '21711'),
  (150, 34, 229, 0, 'Langkat', 'Kabupaten', '20811'),
  (151, 34, 268, 0, 'Mandailing Natal', 'Kabupaten', '22916'),
  (152, 34, 278, 0, 'Medan', 'Kota', '20228'),
  (153, 34, 307, 0, 'Nias', 'Kabupaten', '22876'),
  (154, 34, 308, 0, 'Nias Barat', 'Kabupaten', '22895'),
  (155, 34, 309, 0, 'Nias Selatan', 'Kabupaten', '22865'),
  (156, 34, 310, 0, 'Nias Utara', 'Kabupaten', '22856'),
  (157, 34, 319, 0, 'Padang Lawas', 'Kabupaten', '22763'),
  (158, 34, 320, 0, 'Padang Lawas Utara', 'Kabupaten', '22753'),
  (159, 34, 323, 0, 'Padang Sidempuan', 'Kota', '22727'),
  (160, 34, 325, 0, 'Pakpak Bharat', 'Kabupaten', '22272'),
  (161, 34, 353, 0, 'Pematang Siantar', 'Kota', '21126'),
  (162, 34, 389, 0, 'Samosir', 'Kabupaten', '22392'),
  (163, 34, 404, 0, 'Serdang Bedagai', 'Kabupaten', '20915'),
  (164, 34, 407, 0, 'Sibolga', 'Kota', '22522'),
  (165, 34, 413, 0, 'Simalungun', 'Kabupaten', '21162'),
  (166, 34, 459, 0, 'Tanjung Balai', 'Kota', '21321'),
  (167, 34, 463, 0, 'Tapanuli Selatan', 'Kabupaten', '22742'),
  (168, 34, 464, 0, 'Tapanuli Tengah', 'Kabupaten', '22611'),
  (169, 34, 465, 0, 'Tapanuli Utara', 'Kabupaten', '22414'),
  (170, 34, 470, 0, 'Tebing Tinggi', 'Kota', '20632'),
  (171, 34, 481, 0, 'Toba Samosir', 'Kabupaten', '22316'),
  (172, 34, 278, 3906, 'Medan Amplas', 'Kecamatan', ''),
  (173, 34, 278, 3907, 'Medan Area', 'Kecamatan', ''),
  (174, 34, 278, 3908, 'Medan Barat', 'Kecamatan', ''),
  (175, 34, 278, 3909, 'Medan Baru', 'Kecamatan', ''),
  (176, 34, 278, 3910, 'Medan Belawan Kota', 'Kecamatan', ''),
  (177, 34, 278, 3911, 'Medan Deli', 'Kecamatan', ''),
  (178, 34, 278, 3912, 'Medan Denai', 'Kecamatan', ''),
  (179, 34, 278, 3913, 'Medan Helvetia', 'Kecamatan', ''),
  (180, 34, 278, 3914, 'Medan Johor', 'Kecamatan', ''),
  (181, 34, 278, 3915, 'Medan Kota', 'Kecamatan', ''),
  (182, 34, 278, 3916, 'Medan Labuhan', 'Kecamatan', ''),
  (183, 34, 278, 3917, 'Medan Maimun', 'Kecamatan', ''),
  (184, 34, 278, 3918, 'Medan Marelan', 'Kecamatan', ''),
  (185, 34, 278, 3919, 'Medan Perjuangan', 'Kecamatan', ''),
  (186, 34, 278, 3920, 'Medan Petisah', 'Kecamatan', ''),
  (187, 34, 278, 3921, 'Medan Polonia', 'Kecamatan', ''),
  (188, 34, 278, 3922, 'Medan Selayang', 'Kecamatan', ''),
  (189, 34, 278, 3923, 'Medan Sunggal', 'Kecamatan', ''),
  (190, 34, 278, 3924, 'Medan Tembung', 'Kecamatan', ''),
  (191, 34, 278, 3925, 'Medan Timur', 'Kecamatan', ''),
  (192, 34, 278, 3926, 'Medan Tuntungan', 'Kecamatan', ''),
  (193, 1, 17, 0, 'Badung', 'Kabupaten', '80351'),
  (194, 1, 32, 0, 'Bangli', 'Kabupaten', '80619'),
  (195, 1, 94, 0, 'Buleleng', 'Kabupaten', '81111'),
  (196, 1, 114, 0, 'Denpasar', 'Kota', '80227'),
  (197, 1, 128, 0, 'Gianyar', 'Kabupaten', '80519'),
  (198, 1, 161, 0, 'Jembrana', 'Kabupaten', '82251'),
  (199, 1, 170, 0, 'Karangasem', 'Kabupaten', '80819'),
  (200, 1, 197, 0, 'Klungkung', 'Kabupaten', '80719'),
  (201, 1, 447, 0, 'Tabanan', 'Kabupaten', '82119'),
  (202, 1, 32, 472, 'Bangli', 'Kecamatan', ''),
  (203, 1, 32, 473, 'Kintamani', 'Kecamatan', ''),
  (204, 1, 32, 474, 'Susut', 'Kecamatan', ''),
  (205, 1, 32, 475, 'Tembuku', 'Kecamatan', ''),
  (206, 11, 178, 2497, 'Badas', 'Kecamatan', ''),
  (207, 11, 178, 2498, 'Banyakan', 'Kecamatan', ''),
  (208, 11, 178, 2499, 'Gampengrejo', 'Kecamatan', ''),
  (209, 11, 178, 2500, 'Grogol', 'Kecamatan', ''),
  (210, 11, 178, 2501, 'Gurah', 'Kecamatan', ''),
  (211, 11, 178, 2502, 'Kandangan', 'Kecamatan', ''),
  (212, 11, 178, 2503, 'Kandat', 'Kecamatan', ''),
  (213, 11, 178, 2504, 'Kayen Kidul', 'Kecamatan', ''),
  (214, 11, 178, 2505, 'Kepung', 'Kecamatan', ''),
  (215, 11, 178, 2506, 'Kras', 'Kecamatan', ''),
  (216, 11, 178, 2507, 'Kunjang', 'Kecamatan', ''),
  (217, 11, 178, 2508, 'Mojo', 'Kecamatan', ''),
  (218, 11, 178, 2509, 'Ngadiluwih', 'Kecamatan', ''),
  (219, 11, 178, 2510, 'Ngancar', 'Kecamatan', ''),
  (220, 11, 178, 2511, 'Ngasem', 'Kecamatan', ''),
  (221, 11, 178, 2512, 'Pagu', 'Kecamatan', ''),
  (222, 11, 178, 2513, 'Papar', 'Kecamatan', ''),
  (223, 11, 178, 2514, 'Pare', 'Kecamatan', ''),
  (224, 11, 178, 2515, 'Plemahan', 'Kecamatan', ''),
  (225, 11, 178, 2516, 'Plosoklaten', 'Kecamatan', ''),
  (226, 11, 178, 2517, 'Puncu', 'Kecamatan', ''),
  (227, 11, 178, 2518, 'Purwoasri', 'Kecamatan', ''),
  (228, 11, 178, 2519, 'Ringinrejo', 'Kecamatan', ''),
  (229, 11, 178, 2520, 'Semen', 'Kecamatan', ''),
  (230, 11, 178, 2521, 'Tarokan', 'Kecamatan', ''),
  (231, 11, 178, 2522, 'Wates', 'Kecamatan', ''),
  (232, 1, 94, 1279, 'Banjar', 'Kecamatan', ''),
  (233, 1, 94, 1280, 'Buleleng', 'Kecamatan', ''),
  (234, 1, 94, 1281, 'Busungbiu', 'Kecamatan', ''),
  (235, 1, 94, 1282, 'Gerokgak', 'Kecamatan', ''),
  (236, 1, 94, 1283, 'Kubutambahan', 'Kecamatan', ''),
  (237, 1, 94, 1284, 'Sawan', 'Kecamatan', ''),
  (238, 1, 94, 1285, 'Seririt', 'Kecamatan', ''),
  (239, 1, 94, 1286, 'Sukasada', 'Kecamatan', ''),
  (240, 1, 94, 1287, 'Tejakula', 'Kecamatan', ''),
  (241, 11, 256, 3634, 'Blimbing', 'Kecamatan', ''),
  (242, 11, 256, 3635, 'Kedungkandang', 'Kecamatan', ''),
  (243, 11, 256, 3636, 'Klojen', 'Kecamatan', ''),
  (244, 11, 256, 3637, 'Lowokwaru', 'Kecamatan', ''),
  (245, 11, 256, 3638, 'Sukun', 'Kecamatan', ''),
  (246, 30, 53, 0, 'Bau-Bau', 'Kota', '93719'),
  (247, 30, 85, 0, 'Bombana', 'Kabupaten', '93771'),
  (248, 30, 101, 0, 'Buton', 'Kabupaten', '93754'),
  (249, 30, 102, 0, 'Buton Utara', 'Kabupaten', '93745'),
  (250, 30, 182, 0, 'Kendari', 'Kota', '93126'),
  (251, 30, 198, 0, 'Kolaka', 'Kabupaten', '93511'),
  (252, 30, 199, 0, 'Kolaka Utara', 'Kabupaten', '93911'),
  (253, 30, 200, 0, 'Konawe', 'Kabupaten', '93411'),
  (254, 30, 201, 0, 'Konawe Selatan', 'Kabupaten', '93811'),
  (255, 30, 202, 0, 'Konawe Utara', 'Kabupaten', '93311'),
  (256, 30, 295, 0, 'Muna', 'Kabupaten', '93611'),
  (257, 30, 494, 0, 'Wakatobi', 'Kabupaten', '93791'),
  (258, 30, 295, 4157, 'Barangka', 'Kecamatan', ''),
  (259, 30, 295, 4158, 'Batalaiwaru (Batalaiworu)', 'Kecamatan', ''),
  (260, 30, 295, 4159, 'Batukara', 'Kecamatan', ''),
  (261, 30, 295, 4160, 'Bone (Bone Tondo)', 'Kecamatan', ''),
  (262, 30, 295, 4161, 'Duruka', 'Kecamatan', ''),
  (263, 30, 295, 4162, 'Kabangka', 'Kecamatan', ''),
  (264, 30, 295, 4163, 'Kabawo', 'Kecamatan', ''),
  (265, 30, 295, 4164, 'Katobu', 'Kecamatan', ''),
  (266, 30, 295, 4165, 'Kontu Kowuna', 'Kecamatan', ''),
  (267, 30, 295, 4166, 'Kontunaga', 'Kecamatan', ''),
  (268, 30, 295, 4167, 'Kusambi', 'Kecamatan', ''),
  (269, 30, 295, 4168, 'Lasalepa', 'Kecamatan', ''),
  (270, 30, 295, 4169, 'Lawa', 'Kecamatan', ''),
  (271, 30, 295, 4170, 'Lohia', 'Kecamatan', ''),
  (272, 30, 295, 4171, 'Maginti', 'Kecamatan', ''),
  (273, 30, 295, 4172, 'Maligano', 'Kecamatan', ''),
  (274, 30, 295, 4173, 'Marobo', 'Kecamatan', ''),
  (275, 30, 295, 4174, 'Napabalano', 'Kecamatan', ''),
  (276, 30, 295, 4175, 'Napano Kusambi', 'Kecamatan', ''),
  (277, 30, 295, 4176, 'Parigi', 'Kecamatan', ''),
  (278, 30, 295, 4177, 'Pasi Kolaga', 'Kecamatan', ''),
  (279, 30, 295, 4178, 'Pasir Putih', 'Kecamatan', ''),
  (280, 30, 295, 4179, 'Sawerigadi (Sawerigading/Sewergadi)', 'Kecamatan', ''),
  (281, 30, 295, 4180, 'Tiworo Kepulauan', 'Kecamatan', ''),
  (282, 30, 295, 4181, 'Tiworo Selatan', 'Kecamatan', ''),
  (283, 30, 295, 4182, 'Tiworo Tengah', 'Kecamatan', ''),
  (284, 30, 295, 4183, 'Tiworo Utara', 'Kecamatan', ''),
  (285, 30, 295, 4184, 'Tongkuno', 'Kecamatan', ''),
  (286, 30, 295, 4185, 'Tongkuno Selatan', 'Kecamatan', ''),
  (287, 30, 295, 4186, 'Towea', 'Kecamatan', ''),
  (288, 30, 295, 4187, 'Wa Daga', 'Kecamatan', ''),
  (289, 30, 295, 4188, 'Wakorumba Selatan', 'Kecamatan', ''),
  (290, 30, 295, 4189, 'Watopute', 'Kecamatan', '');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer`
(
  `manufacturer_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  64
) NOT NULL,
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `image` varchar
(
  255
) DEFAULT NULL,
  `sort_order` int
(
  3
) NOT NULL,
  PRIMARY KEY
(
  `manufacturer_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

CREATE TABLE IF NOT EXISTS `option`
(
  `option_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `type` varchar
(
  32
) NOT NULL,
  `name` varchar
(
  128
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL,
  PRIMARY KEY
(
  `option_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `option_value`
--

CREATE TABLE IF NOT EXISTS `option_value`
(
  `option_value_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `option_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  128
) NOT NULL,
  `image` varchar
(
  255
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL,
  PRIMARY KEY
(
  `option_value_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order`
(
  `order_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `invoice_prefix` varchar
(
  32
) NOT NULL DEFAULT '',
  `invoice_no` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `email` varchar
(
  255
) NOT NULL DEFAULT '',
  `telephone` varchar
(
  32
) NOT NULL DEFAULT '',
  `address` varchar
(
  128
) NOT NULL DEFAULT '',
  `postcode` varchar
(
  10
) NOT NULL DEFAULT '',
  `province_id` int
(
  11
) NOT NULL DEFAULT '0',
  `province` varchar
(
  128
) NOT NULL DEFAULT '',
  `city_id` int
(
  11
) NOT NULL DEFAULT '0',
  `city` varchar
(
  128
) NOT NULL DEFAULT '',
  `subdistrict_id` int
(
  11
) NOT NULL DEFAULT '0',
  `subdistrict` varchar
(
  128
) NOT NULL DEFAULT '',
  `user_agent` varchar
(
  255
) NOT NULL DEFAULT '',
  `payment_method` varchar
(
  128
) NOT NULL DEFAULT '',
  `payment_code` varchar
(
  128
) NOT NULL DEFAULT '',
  `shipping_method` varchar
(
  128
) NOT NULL DEFAULT '',
  `shipping_code` varchar
(
  128
) NOT NULL DEFAULT '',
  `waybill` varchar
(
  64
) NOT NULL DEFAULT '',
  `currency_id` int
(
  11
) NOT NULL DEFAULT '0',
  `currency_value` decimal
(
  15,
  8
) NOT NULL DEFAULT '0.00000000',
  `comment` text,
  `total` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `order_status_id` int
(
  11
) NOT NULL DEFAULT '0',
  `ip` varchar
(
  40
) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_paid` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `order_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

CREATE TABLE IF NOT EXISTS `order_history`
(
  `order_history_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `order_status_id` int
(
  5
) NOT NULL DEFAULT '0',
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `admin_id` int
(
  11
) NOT NULL DEFAULT '0',
  `notify` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `comment` text,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `order_history_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `order_option`
--

CREATE TABLE IF NOT EXISTS `order_option`
(
  `order_option_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `order_id` int
(
  11
) NOT NULL,
  `order_product_id` int
(
  11
) NOT NULL,
  `product_option_id` int
(
  11
) NOT NULL,
  `product_option_value_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  255
) NOT NULL,
  `value` text NOT NULL,
  `type` varchar
(
  32
) NOT NULL,
  PRIMARY KEY
(
  `order_option_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE IF NOT EXISTS `order_product`
(
  `order_product_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `sku` varchar
(
  64
) NOT NULL DEFAULT '',
  `quantity` int
(
  4
) NOT NULL DEFAULT '0',
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `total` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  PRIMARY KEY
(
  `order_product_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE IF NOT EXISTS `order_status`
(
  `order_status_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  32
) NOT NULL DEFAULT '',
  `group` varchar
(
  8
) NOT NULL DEFAULT 'payment',
  PRIMARY KEY
(
  `order_status_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=8;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`order_status_id`, `name`, `group`)
VALUES
  (1, 'Pending', 'payment'),
  (2, 'Belum Lunas', 'payment'),
  (3, 'Lunas', 'payment'),
  (4, 'Diproses', 'payment'),
  (5, 'Batal', 'payment'),
  (6, 'Dikirim', 'payment'),
  (7, 'Selesai', 'payment');

-- --------------------------------------------------------

--
-- Table structure for table `order_total`
--

CREATE TABLE IF NOT EXISTS `order_total`
(
  `order_total_id` int
(
  10
) NOT NULL AUTO_INCREMENT,
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `title` varchar
(
  255
) NOT NULL DEFAULT '',
  `text` varchar
(
  255
) NOT NULL DEFAULT '',
  `value` decimal
(
  15,
  4
) NOT NULL DEFAULT '0.0000',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `order_total_id`
),
  KEY `idx_orders_total_orders_id`
(
  `order_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page`
(
  `page_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `title` varchar
(
  64
) NOT NULL DEFAULT '',
  `content` text,
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_description` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_keyword` varchar
(
  255
) NOT NULL DEFAULT '',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `new_window` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `footer_column` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '1',
  PRIMARY KEY
(
  `page_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment`
(
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `description` text,
  `version` varchar
(
  32
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `code`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product`
(
  `product_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `model` varchar
(
  128
) NOT NULL DEFAULT '',
  `sku` varchar
(
  64
) NOT NULL DEFAULT '',
  `description` text,
  `meta_description` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_keyword` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_title` varchar
(
  255
) NOT NULL DEFAULT '',
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `tag` text,
  `image` varchar
(
  255
) DEFAULT NULL,
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `category_id` int
(
  11
) NOT NULL DEFAULT '0',
  `manufacturer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `points` int
(
  11
) NOT NULL DEFAULT '0',
  `weight` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `weight_class_id` int
(
  11
) NOT NULL DEFAULT '0',
  `length` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `width` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `height` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `length_class_id` int
(
  11
) NOT NULL DEFAULT '0',
  `quantity` int
(
  11
) NOT NULL DEFAULT '0',
  `unit_class_id` int
(
  11
) NOT NULL DEFAULT '0',
  `stock_status_id` int
(
  11
) NOT NULL DEFAULT '0',
  `minimum` int
(
  11
) NOT NULL DEFAULT '1',
  `shipping` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int
(
  5
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `product_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute`
--

CREATE TABLE IF NOT EXISTS `product_attribute`
(
  `product_id` int
(
  11
) NOT NULL,
  `attribute_id` int
(
  11
) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY
(
  `product_id`,
  `attribute_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category`
(
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `category_id` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `product_id`,
  `category_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_discount`
--

CREATE TABLE IF NOT EXISTS `product_discount`
(
  `product_discount_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `quantity` int
(
  4
) NOT NULL DEFAULT '0',
  `priority` int
(
  5
) NOT NULL DEFAULT '1',
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY
(
  `product_discount_id`
),
  KEY `product_id`
(
  `product_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `product_filter`
--

CREATE TABLE IF NOT EXISTS `product_filter`
(
  `product_id` int
(
  11
) NOT NULL,
  `filter_id` int
(
  11
) NOT NULL,
  PRIMARY KEY
(
  `product_id`,
  `filter_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE IF NOT EXISTS `product_image`
(
  `product_image_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `image` varchar
(
  255
) NOT NULL DEFAULT '',
  `alt` varchar
(
  64
) NOT NULL DEFAULT '',
  `title` varchar
(
  64
) NOT NULL DEFAULT '',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `product_image_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `product_option`
--

CREATE TABLE IF NOT EXISTS `product_option`
(
  `product_option_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_id` int
(
  11
) NOT NULL,
  `option_id` int
(
  11
) NOT NULL,
  `option_value` text NOT NULL,
  `required` tinyint
(
  1
) NOT NULL,
  PRIMARY KEY
(
  `product_option_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `product_option_value`
--

CREATE TABLE IF NOT EXISTS `product_option_value`
(
  `product_option_value_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_option_id` int
(
  11
) NOT NULL,
  `product_id` int
(
  11
) NOT NULL,
  `option_id` int
(
  11
) NOT NULL,
  `option_value_id` int
(
  11
) NOT NULL,
  `quantity` int
(
  3
) NOT NULL,
  `subtract` tinyint
(
  1
) NOT NULL,
  `price` decimal
(
  15,
  2
) NOT NULL,
  `points` int
(
  8
) NOT NULL,
  `weight` decimal
(
  15,
  2
) NOT NULL,
  PRIMARY KEY
(
  `product_option_value_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `product_related`
--

CREATE TABLE IF NOT EXISTS `product_related`
(
  `product_id` int
(
  11
) NOT NULL,
  `related_id` int
(
  11
) NOT NULL,
  `type` varchar
(
  9
) NOT NULL DEFAULT '',
  PRIMARY KEY
(
  `product_id`,
  `related_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_tab`
--

CREATE TABLE IF NOT EXISTS `product_tab`
(
  `product_tab_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `title` varchar
(
  128
) NOT NULL DEFAULT '',
  `content` text,
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `product_tab_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review`
(
  `review_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `author` varchar
(
  128
) NOT NULL DEFAULT '',
  `text` text,
  `rating` int
(
  1
) NOT NULL DEFAULT '0',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `review_id`
),
  KEY `product_id`
(
  `product_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE IF NOT EXISTS `search`
(
  `search_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `search` varchar
(
  255
) COLLATE utf8_bin NOT NULL DEFAULT '',
  `phase` int
(
  1
) NOT NULL DEFAULT '0',
  `results` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `ip` varchar
(
  15
) COLLATE utf8_bin NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `search_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE =utf8_bin AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session`
(
  `id` varchar
(
  128
) NOT NULL,
  `ip_address` varchar
(
  45
) NOT NULL,
  `timestamp` int
(
  10
) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp`
(
  `timestamp`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `ip_address`, `timestamp`, `data`)
VALUES
('005rke16hk9h3j0fdv1hijpic0d8l44b', '127.0.0.1', 1511773604,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313737333336363b61646d696e5f72656469726563747c733a31323a226f66666963652f6f72646572223b61646d696e5f69647c733a323a222d31223b);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting`
(
  `setting_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `group` varchar
(
  32
) NOT NULL,
  `key` varchar
(
  64
) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint
(
  1
) NOT NULL,
  PRIMARY KEY
(
  `setting_id`
),
  KEY `group`
(
  `group`
),
  KEY `key`
(
  `key`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_status`
--

CREATE TABLE IF NOT EXISTS `stock_status`
(
  `stock_status_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  32
) NOT NULL DEFAULT '',
  PRIMARY KEY
(
  `stock_status_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

--
-- Dumping data for table `stock_status`
--

INSERT INTO `stock_status` (`stock_status_id`, `name`)
VALUES
  (1, 'Tersedia'),
  (2, 'Habis');

-- --------------------------------------------------------

--
-- Table structure for table `weight_class`
--

CREATE TABLE IF NOT EXISTS `weight_class`
(
  `weight_class_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `title` varchar
(
  32
) NOT NULL DEFAULT '',
  `unit` varchar
(
  4
) NOT NULL DEFAULT '',
  `value` decimal
(
  15,
  4
) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY
(
  `weight_class_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

--
-- Dumping data for table `weight_class`
--

INSERT INTO `weight_class` (`weight_class_id`, `title`, `unit`, `value`)
VALUES
  (3, 'Gram', 'gr', '1.0000'),
  (4, 'Kilogram', 'kg', '0.0000');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
