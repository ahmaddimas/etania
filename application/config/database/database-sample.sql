-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 21, 2017 at 06:47 PM
-- Server version: 5.6.16
-- PHP Version: 5.6.25

SET
SQL_MODE
=
"NO_AUTO_VALUE_ON_ZERO";
SET
time_zone
=
"+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `medansoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon`
--

CREATE TABLE IF NOT EXISTS `addon`
(
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `description` text,
  `version` varchar
(
  32
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `code`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `addon`
--

INSERT INTO `addon` (`code`, `name`, `description`, `version`, `active`)
VALUES
('custom', 'Rakitan', 'Sistem order kustom untuk PC rakitan', '1.0.0', 1),
('offers', 'Offers', 'Modul penawaran spesial', '1.0.0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address`
(
  `address_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `address` varchar
(
  128
) NOT NULL DEFAULT '',
  `telephone` varchar
(
  32
) NOT NULL DEFAULT '',
  `postcode` varchar
(
  10
) NOT NULL DEFAULT '',
  `country_id` int
(
  11
) NOT NULL DEFAULT '0',
  `province_id` int
(
  11
) NOT NULL DEFAULT '0',
  `city_id` int
(
  11
) NOT NULL DEFAULT '0',
  `subdistrict_id` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `address_id`
),
  KEY `customer_id`
(
  `customer_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Tabel alamat' AUTO_INCREMENT=13;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `customer_id`, `name`, `address`, `telephone`, `postcode`, `country_id`,
                       `province_id`, `city_id`, `subdistrict_id`)
VALUES
  (11, 2, 'Kantor', 'Jl. Cengkeh No. 55', '', '65141', 0, 11, 256, 3637),
  (12, 2, 'Adi Setiawan', 'Jl. Pahlawan No. 123', '', '123456', 0, 11, 256, 3637);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin`
(
  `admin_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `admin_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `image` text,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `email` varchar
(
  255
) NOT NULL DEFAULT '',
  `username` varchar
(
  32
) NOT NULL DEFAULT '',
  `password` varchar
(
  255
) NOT NULL DEFAULT '',
  `salt` varchar
(
  9
) NOT NULL DEFAULT '',
  `code_activation` varchar
(
  40
) NOT NULL DEFAULT '',
  `code_forgotten` varchar
(
  40
) NOT NULL DEFAULT '',
  `ip` varchar
(
  64
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `admin_id`
),
  KEY `email`
(
  `email`,
  `username`,
  `active`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=2;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_group_id`, `image`, `name`, `email`, `username`, `password`, `salt`,
                     `code_activation`, `code_forgotten`, `ip`, `active`, `date_added`, `date_modified`, `last_login`)
VALUES
(-1, 0, 'admin/image--1.png', 'Administrator', 'admin@medansoft.com', 'admin',
 '5df933e0d0a3a9d30591d99447ed1e8c85a03f6c', 'd78e0b8a9', '', '', '127.0.0.1', 1, '2017-10-15 12:12:57',
 '2017-10-30 20:25:10', '2017-11-21 18:00:31'),
(1, 2, NULL, 'Andre', 'andre@medansoft.com', 'andre', 'cb64c825aac488443e0e63cd997a94ffd05dba3d', '16fd6f820', '', '',
 '', 1, '0000-00-00 00:00:00', '2017-10-30 20:24:18', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_group`
--

CREATE TABLE IF NOT EXISTS `admin_group`
(
  `admin_group_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `permission` text,
  PRIMARY KEY
(
  `admin_group_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel Grup Pengguna' AUTO_INCREMENT=3;

--
-- Dumping data for table `admin_group`
--

INSERT INTO `admin_group` (`admin_group_id`, `name`, `permission`)
VALUES
(2, 'Administrator',
 'a:29:{s:17:"catalog/attribute";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:16:"catalog/category";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:14:"catalog/filter";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:14:"catalog/option";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:15:"catalog/product";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:14:"catalog/review";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:11:"payment/bca";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:15:"payment/mandiri";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:16:"payment/midtrans";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:13:"system/addons";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:19:"system/length_class";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:19:"system/order_status";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:15:"system/payments";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:14:"system/setting";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:19:"system/stock_status";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:19:"system/weight_class";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:11:"admin/admin";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:17:"admin/admin_group";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:12:"banner/admin";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:14:"customer/admin";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:20:"customer/admin_group";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:11:"order/admin";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:10:"page/admin";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:12:"custom/admin";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:21:"custom/admin_category";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:17:"custom/admin_part";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:12:"offers/admin";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:21:"offers/admin_category";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}s:21:"offers/admin_discount";a:4:{i:0;s:5:"index";i:1;s:6:"create";i:2;s:4:"edit";i:3;s:6:"delete";}}');

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

CREATE TABLE IF NOT EXISTS `attribute`
(
  `attribute_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  64
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL,
  PRIMARY KEY
(
  `attribute_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=13;

--
-- Dumping data for table `attribute`
--

INSERT INTO `attribute` (`attribute_id`, `attribute_group_id`, `name`, `sort_order`)
VALUES
  (12, 3, 'RAM', 3),
  (11, 3, 'Storage', 2),
  (10, 3, 'Prosesor', 1);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_group`
--

CREATE TABLE IF NOT EXISTS `attribute_group`
(
  `attribute_group_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  64
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL,
  PRIMARY KEY
(
  `attribute_group_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=4;

--
-- Dumping data for table `attribute_group`
--

INSERT INTO `attribute_group` (`attribute_group_id`, `name`, `sort_order`)
VALUES
  (3, 'Spesifikasi Teknis', 1);

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner`
(
  `banner_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `description` text,
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `banner_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=7;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `name`, `description`, `active`)
VALUES
  (3, 'Slideshow', 'Slideshow', 1),
  (4, 'Banner', 'Top Banner', 1),
  (5, 'Middle Banner', 'Middle Banner', 1),
  (6, 'Footer Banner', 'Footer Banner', 1);

-- --------------------------------------------------------

--
-- Table structure for table `banner_image`
--

CREATE TABLE IF NOT EXISTS `banner_image`
(
  `banner_image_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `banner_id` int
(
  11
) NOT NULL DEFAULT '0',
  `title` varchar
(
  64
) NOT NULL DEFAULT '',
  `subtitle` varchar
(
  255
) NOT NULL DEFAULT '',
  `link` varchar
(
  255
) NOT NULL DEFAULT '',
  `link_title` varchar
(
  255
) NOT NULL DEFAULT '',
  `image` varchar
(
  255
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `banner_image_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=39;

--
-- Dumping data for table `banner_image`
--

INSERT INTO `banner_image` (`banner_image_id`, `banner_id`, `title`, `subtitle`, `link`, `link_title`, `image`,
                            `active`, `sort_order`)
VALUES
  (27, 4, 'Banner 1', '', '', '', 'data/banners/banner-01.png', 1, 1),
  (28, 4, '', '', '', '', 'data/banners/banner-01.png', 1, 1),
  (29, 5, '', '', '', '', 'data/banners/banner-06.png', 0, 0),
  (33, 6, '', '', '', '', 'data/banners/banner-01.png', 1, 0),
  (32, 6, '', '', '', '', 'data/banners/banner-01.png', 1, 0),
  (38, 3, '', '', '', '', 'data/banners/F_20170927135526TQMZX2.JPG', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category`
(
  `category_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `description` text,
  `meta_description` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_keyword` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_title` varchar
(
  255
) NOT NULL DEFAULT '',
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `image` varchar
(
  255
) NOT NULL DEFAULT '',
  `menu_image` varchar
(
  255
) NOT NULL DEFAULT '',
  `parent_id` int
(
  11
) NOT NULL DEFAULT '0',
  `top` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `column` int
(
  3
) NOT NULL DEFAULT '0',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `category_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=10;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `description`, `meta_description`, `meta_keyword`, `meta_title`, `slug`,
                        `image`, `menu_image`, `parent_id`, `top`, `column`, `sort_order`, `active`, `date_added`,
                        `date_modified`)
VALUES
(2, 'Komponen', '<p>Kami menjual berbagai macam komponen komputer desktop<br></p>',
 'Kami menjual berbagai macam komponen komputer desktop', 'komponen, komputer', 'Komponen Komputer', 'komponen', '',
 'fa-caret-right', 0, 1, 0, 0, 1, '2017-10-17 07:15:06', '2017-11-03 11:54:19'),
(3, 'Motherboard', '', '0', 'motherboard', 'Motherboard', 'motherboard', '', 'fa-caret-right', 2, 0, 0, 0, 1,
 '2017-11-17 02:39:29', '0000-00-00 00:00:00'),
(4, 'Motherboard Intel', '', '0', 'motherboard, intel', 'Motherboard Intel', 'motherboard-intel', '', 'fa-caret-right',
 3, 0, 0, 0, 1, '2017-11-17 02:40:21', '0000-00-00 00:00:00'),
(5, 'Motherboard AMD', '', '0', 'motherboard, amd', 'Motherboard AMD', 'motherboard-amd', '', 'fa-caret-right', 3, 0, 0,
 0, 1, '2017-11-17 02:41:55', '0000-00-00 00:00:00'),
(6, 'Hard Disk', '', '0', 'hard, disk', 'Hard Disk', 'hard-disk', '', 'fa-caret-right', 2, 0, 0, 0, 1,
 '2017-11-17 02:49:53', '2017-11-17 02:50:24'),
(7, 'HDD SATA', '', '0', 'hdd, sata', 'HDD SATA', 'hdd-sata', '', 'fa-caret-right', 6, 0, 0, 0, 1,
 '2017-11-17 02:51:18', '0000-00-00 00:00:00'),
(8, 'SSD Storage', '', '0', 'ssd, storage', 'SSD Storage', 'ssd-storage', '', 'fa-caret-right', 6, 0, 0, 0, 1,
 '2017-11-17 02:51:42', '0000-00-00 00:00:00'),
(9, 'Monitor', '', '0', 'monitor', 'Monitor', 'monitor', '', 'fa-caret-right', 0, 1, 0, 0, 1, '2017-11-17 02:56:44',
 '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `category_path`
--

CREATE TABLE IF NOT EXISTS `category_path`
(
  `category_id` int
(
  11
) NOT NULL DEFAULT '0',
  `path_id` int
(
  11
) NOT NULL DEFAULT '0',
  `level` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `category_id`,
  `path_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_path`
--

INSERT INTO `category_path` (`category_id`, `path_id`, `level`)
VALUES
  (2, 2, 0),
  (3, 2, 0),
  (3, 3, 1),
  (4, 2, 0),
  (4, 3, 1),
  (4, 4, 2),
  (5, 2, 0),
  (5, 3, 1),
  (5, 5, 2),
  (6, 2, 0),
  (6, 6, 1),
  (7, 2, 0),
  (7, 6, 1),
  (7, 7, 2),
  (8, 2, 0),
  (8, 6, 1),
  (8, 8, 2),
  (9, 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency`
(
  `currency_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `title` varchar
(
  32
) NOT NULL DEFAULT '',
  `code` varchar
(
  3
) NOT NULL DEFAULT '',
  `symbol` varchar
(
  12
) NOT NULL DEFAULT '',
  `value` float
(
  15,
  8
) NOT NULL DEFAULT '0.00000000',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `currency_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=9;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`currency_id`, `title`, `code`, `symbol`, `value`, `active`, `date_modified`)
VALUES
  (8, 'Rupiah', 'IDR', 'Rp', 1.00000000, 1, '2017-04-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer`
(
  `customer_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `customer_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `bank_code` varchar
(
  5
) NOT NULL DEFAULT '',
  `bank_name` varchar
(
  255
) NOT NULL DEFAULT '',
  `bank_account_name` varchar
(
  64
) NOT NULL DEFAULT '',
  `bank_account_number` varchar
(
  64
) NOT NULL DEFAULT '',
  `address_id` int
(
  11
) NOT NULL DEFAULT '0',
  `type` enum
(
  'b',
  's'
) NOT NULL DEFAULT 'b',
  `image` text,
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  `email` varchar
(
  255
) NOT NULL DEFAULT '',
  `gender` enum
(
  'm',
  'f'
) NOT NULL DEFAULT 'm',
  `telephone` varchar
(
  32
) NOT NULL DEFAULT '',
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `username` varchar
(
  32
) NOT NULL DEFAULT '',
  `password` varchar
(
  255
) NOT NULL DEFAULT '',
  `salt` varchar
(
  9
) NOT NULL DEFAULT '',
  `code_activation` varchar
(
  40
) NOT NULL DEFAULT '',
  `code_forgotten` varchar
(
  40
) NOT NULL DEFAULT '',
  `ip` varchar
(
  64
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `verified` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `customer_id`
),
  KEY `email`
(
  `email`,
  `username`,
  `active`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_group_id`, `bank_code`, `bank_name`, `bank_account_name`,
                        `bank_account_number`, `address_id`, `type`, `image`, `name`, `email`, `gender`, `telephone`,
                        `dob`, `username`, `password`, `salt`, `code_activation`, `code_forgotten`, `ip`, `active`,
                        `verified`, `date_added`, `date_modified`, `last_login`)
VALUES
(2, 3, '', '', '12121212', '1332423412341234', 11, 'b', 'customer/a5c4331dc1e7ac05df57b4e65a90d9a4.png', 'Adi Setia',
 'mas.adisetiawan@gmail.com', 'm', '1234567890', '0000-00-00', 'adise0589', '104554681a483ffef05c1067a0300848a033327f',
 '953bf0cbf', '', '', '127.0.0.1', 1, 0, '2017-10-29 22:59:25', '2017-11-21 16:56:41', '2017-11-21 17:48:08');

-- --------------------------------------------------------

--
-- Table structure for table `customer_group`
--

CREATE TABLE IF NOT EXISTS `customer_group`
(
  `customer_group_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  32
) NOT NULL DEFAULT '',
  `description` text,
  `approval` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `customer_group_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=4;

--
-- Dumping data for table `customer_group`
--

INSERT INTO `customer_group` (`customer_group_id`, `name`, `description`, `approval`)
VALUES
  (3, 'Reseller', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer_history`
--

CREATE TABLE IF NOT EXISTS `customer_history`
(
  `customer_history_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `comment` text,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `customer_history_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_transaction`
--

CREATE TABLE IF NOT EXISTS `customer_transaction`
(
  `customer_transaction_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `reference` varchar
(
  32
) NOT NULL DEFAULT '',
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `description` text,
  `amount` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_paid` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_method` varchar
(
  32
) NOT NULL DEFAULT '',
  `payment_code` varchar
(
  32
) NOT NULL DEFAULT '',
  `approved` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `customer_transaction_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=4;

-- --------------------------------------------------------

--
-- Table structure for table `custom_category`
--

CREATE TABLE IF NOT EXISTS `custom_category`
(
  `custom_category_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `required` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `custom_category_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

--
-- Dumping data for table `custom_category`
--

INSERT INTO `custom_category` (`custom_category_id`, `name`, `required`, `sort_order`)
VALUES
  (1, 'Prosesor', 1, 1),
  (2, 'Motherboard', 1, 2),
  (3, 'Storage', 1, 3),
  (4, 'Memory', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `custom_part`
--

CREATE TABLE IF NOT EXISTS `custom_part`
(
  `custom_part_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `custom_category_id` int
(
  11
) NOT NULL DEFAULT '0',
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `custom_part_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

--
-- Dumping data for table `custom_part`
--

INSERT INTO `custom_part` (`custom_part_id`, `custom_category_id`, `product_id`, `price`, `active`)
VALUES
  (1, 1, 63, '950000.00', 1),
  (2, 2, 64, '1050000.00', 1),
  (3, 4, 99, '560000.00', 1),
  (4, 3, 100, '480000.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `custom_part_pair`
--

CREATE TABLE IF NOT EXISTS `custom_part_pair`
(
  `product_id` int
(
  11
) NOT NULL,
  `pair_id` int
(
  11
) NOT NULL,
  PRIMARY KEY
(
  `product_id`,
  `pair_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `custom_part_pair`
--

INSERT INTO `custom_part_pair` (`product_id`, `pair_id`)
VALUES
  (63, 64);

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE IF NOT EXISTS `filter`
(
  `filter_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `filter_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  PRIMARY KEY
(
  `filter_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=47;

--
-- Dumping data for table `filter`
--

INSERT INTO `filter` (`filter_id`, `filter_group_id`, `sort_order`, `name`)
VALUES
  (45, 11, 5, '8 GB'),
  (46, 11, 6, '16 GB'),
  (44, 11, 4, '4 GB'),
  (43, 11, 3, '2 GB'),
  (42, 11, 2, '1 GB'),
  (41, 11, 1, '512 MB'),
  (40, 10, 4, '14.0"'),
  (39, 10, 3, '13.0"'),
  (38, 10, 2, '11.6"'),
  (37, 10, 1, '10.2"');

-- --------------------------------------------------------

--
-- Table structure for table `filter_group`
--

CREATE TABLE IF NOT EXISTS `filter_group`
(
  `filter_group_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `name` varchar
(
  64
) NOT NULL DEFAULT '',
  PRIMARY KEY
(
  `filter_group_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=12;

--
-- Dumping data for table `filter_group`
--

INSERT INTO `filter_group` (`filter_group_id`, `sort_order`, `name`)
VALUES
  (11, 2, 'Memory'),
  (10, 1, 'Display');

-- --------------------------------------------------------

--
-- Table structure for table `length_class`
--

CREATE TABLE IF NOT EXISTS `length_class`
(
  `length_class_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `title` varchar
(
  32
) NOT NULL DEFAULT '',
  `unit` varchar
(
  4
) NOT NULL DEFAULT '',
  `value` decimal
(
  15,
  4
) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY
(
  `length_class_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=4;

--
-- Dumping data for table `length_class`
--

INSERT INTO `length_class` (`length_class_id`, `title`, `unit`, `value`)
VALUES
  (1, 'Milimeter', 'mm', '1.0000'),
  (2, 'Centimeter', 'cm', '0.1000');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location`
(
  `location_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `province_id` int
(
  11
) NOT NULL DEFAULT '0',
  `city_id` int
(
  11
) NOT NULL DEFAULT '0',
  `subdistrict_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  128
) NOT NULL DEFAULT '',
  `type` varchar
(
  32
) NOT NULL DEFAULT '',
  `postcode` varchar
(
  8
) NOT NULL DEFAULT '',
  PRIMARY KEY
(
  `location_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=291;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `province_id`, `city_id`, `subdistrict_id`, `name`, `type`, `postcode`)
VALUES
  (1, 1, 0, 0, 'Bali', 'Provinsi', ''),
  (2, 2, 0, 0, 'Bangka Belitung', 'Provinsi', ''),
  (3, 3, 0, 0, 'Banten', 'Provinsi', ''),
  (4, 4, 0, 0, 'Bengkulu', 'Provinsi', ''),
  (5, 5, 0, 0, 'DI Yogyakarta', 'Provinsi', ''),
  (6, 6, 0, 0, 'DKI Jakarta', 'Provinsi', ''),
  (7, 7, 0, 0, 'Gorontalo', 'Provinsi', ''),
  (8, 8, 0, 0, 'Jambi', 'Provinsi', ''),
  (9, 9, 0, 0, 'Jawa Barat', 'Provinsi', ''),
  (10, 10, 0, 0, 'Jawa Tengah', 'Provinsi', ''),
  (11, 11, 0, 0, 'Jawa Timur', 'Provinsi', ''),
  (12, 12, 0, 0, 'Kalimantan Barat', 'Provinsi', ''),
  (13, 13, 0, 0, 'Kalimantan Selatan', 'Provinsi', ''),
  (14, 14, 0, 0, 'Kalimantan Tengah', 'Provinsi', ''),
  (15, 15, 0, 0, 'Kalimantan Timur', 'Provinsi', ''),
  (16, 16, 0, 0, 'Kalimantan Utara', 'Provinsi', ''),
  (17, 17, 0, 0, 'Kepulauan Riau', 'Provinsi', ''),
  (18, 18, 0, 0, 'Lampung', 'Provinsi', ''),
  (19, 19, 0, 0, 'Maluku', 'Provinsi', ''),
  (20, 20, 0, 0, 'Maluku Utara', 'Provinsi', ''),
  (21, 21, 0, 0, 'Nanggroe Aceh Darussalam (NAD)', 'Provinsi', ''),
  (22, 22, 0, 0, 'Nusa Tenggara Barat (NTB)', 'Provinsi', ''),
  (23, 23, 0, 0, 'Nusa Tenggara Timur (NTT)', 'Provinsi', ''),
  (24, 24, 0, 0, 'Papua', 'Provinsi', ''),
  (25, 25, 0, 0, 'Papua Barat', 'Provinsi', ''),
  (26, 26, 0, 0, 'Riau', 'Provinsi', ''),
  (27, 27, 0, 0, 'Sulawesi Barat', 'Provinsi', ''),
  (28, 28, 0, 0, 'Sulawesi Selatan', 'Provinsi', ''),
  (29, 29, 0, 0, 'Sulawesi Tengah', 'Provinsi', ''),
  (30, 30, 0, 0, 'Sulawesi Tenggara', 'Provinsi', ''),
  (31, 31, 0, 0, 'Sulawesi Utara', 'Provinsi', ''),
  (32, 32, 0, 0, 'Sumatera Barat', 'Provinsi', ''),
  (33, 33, 0, 0, 'Sumatera Selatan', 'Provinsi', ''),
  (34, 34, 0, 0, 'Sumatera Utara', 'Provinsi', ''),
  (35, 6, 151, 0, 'Jakarta Barat', 'Kota', '11220'),
  (36, 6, 152, 0, 'Jakarta Pusat', 'Kota', '10540'),
  (37, 6, 153, 0, 'Jakarta Selatan', 'Kota', '12230'),
  (38, 6, 154, 0, 'Jakarta Timur', 'Kota', '13330'),
  (39, 6, 155, 0, 'Jakarta Utara', 'Kota', '14140'),
  (40, 6, 189, 0, 'Kepulauan Seribu', 'Kabupaten', '14550'),
  (41, 6, 152, 2095, 'Cempaka Putih', 'Kecamatan', ''),
  (42, 6, 152, 2096, 'Gambir', 'Kecamatan', ''),
  (43, 6, 152, 2097, 'Johar Baru', 'Kecamatan', ''),
  (44, 6, 152, 2098, 'Kemayoran', 'Kecamatan', ''),
  (45, 6, 152, 2099, 'Menteng', 'Kecamatan', ''),
  (46, 6, 152, 2100, 'Sawah Besar', 'Kecamatan', ''),
  (47, 6, 152, 2101, 'Senen', 'Kecamatan', ''),
  (48, 6, 152, 2102, 'Tanah Abang', 'Kecamatan', ''),
  (49, 11, 31, 0, 'Bangkalan', 'Kabupaten', '69118'),
  (50, 11, 42, 0, 'Banyuwangi', 'Kabupaten', '68416'),
  (51, 11, 51, 0, 'Batu', 'Kota', '65311'),
  (52, 11, 74, 0, 'Blitar', 'Kabupaten', '66171'),
  (53, 11, 75, 0, 'Blitar', 'Kota', '66124'),
  (54, 11, 80, 0, 'Bojonegoro', 'Kabupaten', '62119'),
  (55, 11, 86, 0, 'Bondowoso', 'Kabupaten', '68219'),
  (56, 11, 133, 0, 'Gresik', 'Kabupaten', '61115'),
  (57, 11, 160, 0, 'Jember', 'Kabupaten', '68113'),
  (58, 11, 164, 0, 'Jombang', 'Kabupaten', '61415'),
  (59, 11, 178, 0, 'Kediri', 'Kabupaten', '64184'),
  (60, 11, 179, 0, 'Kediri', 'Kota', '64125'),
  (61, 11, 222, 0, 'Lamongan', 'Kabupaten', '64125'),
  (62, 11, 243, 0, 'Lumajang', 'Kabupaten', '67319'),
  (63, 11, 247, 0, 'Madiun', 'Kabupaten', '63153'),
  (64, 11, 248, 0, 'Madiun', 'Kota', '63122'),
  (65, 11, 251, 0, 'Magetan', 'Kabupaten', '63314'),
  (66, 11, 255, 0, 'Malang', 'Kabupaten', '65163'),
  (67, 11, 256, 0, 'Malang', 'Kota', '65112'),
  (68, 11, 289, 0, 'Mojokerto', 'Kabupaten', '61382'),
  (69, 11, 290, 0, 'Mojokerto', 'Kota', '61316'),
  (70, 11, 305, 0, 'Nganjuk', 'Kabupaten', '64414'),
  (71, 11, 306, 0, 'Ngawi', 'Kabupaten', '63219'),
  (72, 11, 317, 0, 'Pacitan', 'Kabupaten', '63512'),
  (73, 11, 330, 0, 'Pamekasan', 'Kabupaten', '69319'),
  (74, 11, 342, 0, 'Pasuruan', 'Kabupaten', '67153'),
  (75, 11, 343, 0, 'Pasuruan', 'Kota', '67118'),
  (76, 11, 363, 0, 'Ponorogo', 'Kabupaten', '63411'),
  (77, 11, 369, 0, 'Probolinggo', 'Kabupaten', '67282'),
  (78, 11, 370, 0, 'Probolinggo', 'Kota', '67215'),
  (79, 11, 390, 0, 'Sampang', 'Kabupaten', '69219'),
  (80, 11, 409, 0, 'Sidoarjo', 'Kabupaten', '61219'),
  (81, 11, 418, 0, 'Situbondo', 'Kabupaten', '68316'),
  (82, 11, 441, 0, 'Sumenep', 'Kabupaten', '69413'),
  (83, 11, 444, 0, 'Surabaya', 'Kota', '60119'),
  (84, 11, 487, 0, 'Trenggalek', 'Kabupaten', '66312'),
  (85, 11, 489, 0, 'Tuban', 'Kabupaten', '62319'),
  (86, 11, 492, 0, 'Tulungagung', 'Kabupaten', '66212'),
  (87, 11, 51, 708, 'Batu', 'Kecamatan', ''),
  (88, 11, 51, 709, 'Bumiaji', 'Kecamatan', ''),
  (89, 11, 51, 710, 'Junrejo', 'Kecamatan', ''),
  (90, 2, 27, 0, 'Bangka', 'Kabupaten', '33212'),
  (91, 2, 28, 0, 'Bangka Barat', 'Kabupaten', '33315'),
  (92, 2, 29, 0, 'Bangka Selatan', 'Kabupaten', '33719'),
  (93, 2, 30, 0, 'Bangka Tengah', 'Kabupaten', '33613'),
  (94, 2, 56, 0, 'Belitung', 'Kabupaten', '33419'),
  (95, 2, 57, 0, 'Belitung Timur', 'Kabupaten', '33519'),
  (96, 2, 334, 0, 'Pangkal Pinang', 'Kota', '33115'),
  (97, 2, 27, 426, 'Bakam', 'Kecamatan', ''),
  (98, 2, 27, 427, 'Belinyu', 'Kecamatan', ''),
  (99, 2, 27, 428, 'Mendo Barat', 'Kecamatan', ''),
  (100, 2, 27, 429, 'Merawang', 'Kecamatan', ''),
  (101, 2, 27, 430, 'Pemali', 'Kecamatan', ''),
  (102, 2, 27, 431, 'Puding Besar', 'Kecamatan', ''),
  (103, 2, 27, 432, 'Riau Silip', 'Kecamatan', ''),
  (104, 2, 27, 433, 'Sungai Liat', 'Kecamatan', ''),
  (105, 33, 40, 0, 'Banyuasin', 'Kabupaten', '30911'),
  (106, 33, 121, 0, 'Empat Lawang', 'Kabupaten', '31811'),
  (107, 33, 220, 0, 'Lahat', 'Kabupaten', '31419'),
  (108, 33, 242, 0, 'Lubuk Linggau', 'Kota', '31614'),
  (109, 33, 292, 0, 'Muara Enim', 'Kabupaten', '31315'),
  (110, 33, 297, 0, 'Musi Banyuasin', 'Kabupaten', '30719'),
  (111, 33, 298, 0, 'Musi Rawas', 'Kabupaten', '31661'),
  (112, 33, 312, 0, 'Ogan Ilir', 'Kabupaten', '30811'),
  (113, 33, 313, 0, 'Ogan Komering Ilir', 'Kabupaten', '30618'),
  (114, 33, 314, 0, 'Ogan Komering Ulu', 'Kabupaten', '32112'),
  (115, 33, 315, 0, 'Ogan Komering Ulu Selatan', 'Kabupaten', '32211'),
  (116, 33, 316, 0, 'Ogan Komering Ulu Timur', 'Kabupaten', '32312'),
  (117, 33, 324, 0, 'Pagar Alam', 'Kota', '31512'),
  (118, 33, 327, 0, 'Palembang', 'Kota', '31512'),
  (119, 33, 367, 0, 'Prabumulih', 'Kota', '31121'),
  (120, 32, 12, 0, 'Agam', 'Kabupaten', '26411'),
  (121, 32, 93, 0, 'Bukittinggi', 'Kota', '26115'),
  (122, 32, 116, 0, 'Dharmasraya', 'Kabupaten', '27612'),
  (123, 32, 186, 0, 'Kepulauan Mentawai', 'Kabupaten', '25771'),
  (124, 32, 236, 0, 'Lima Puluh Koto/Kota', 'Kabupaten', '26671'),
  (125, 32, 318, 0, 'Padang', 'Kota', '25112'),
  (126, 32, 321, 0, 'Padang Panjang', 'Kota', '27122'),
  (127, 32, 322, 0, 'Padang Pariaman', 'Kabupaten', '25583'),
  (128, 32, 337, 0, 'Pariaman', 'Kota', '25511'),
  (129, 32, 339, 0, 'Pasaman', 'Kabupaten', '26318'),
  (130, 32, 340, 0, 'Pasaman Barat', 'Kabupaten', '26511'),
  (131, 32, 345, 0, 'Payakumbuh', 'Kota', '26213'),
  (132, 32, 357, 0, 'Pesisir Selatan', 'Kabupaten', '25611'),
  (133, 32, 394, 0, 'Sawah Lunto', 'Kota', '27416'),
  (134, 32, 411, 0, 'Sijunjung (Sawah Lunto Sijunjung)', 'Kabupaten', '27511'),
  (135, 32, 420, 0, 'Solok', 'Kabupaten', '27365'),
  (136, 32, 421, 0, 'Solok', 'Kota', '27315'),
  (137, 32, 422, 0, 'Solok Selatan', 'Kabupaten', '27779'),
  (138, 32, 453, 0, 'Tanah Datar', 'Kabupaten', '27211'),
  (139, 34, 15, 0, 'Asahan', 'Kabupaten', '21214'),
  (140, 34, 52, 0, 'Batu Bara', 'Kabupaten', '21655'),
  (141, 34, 70, 0, 'Binjai', 'Kota', '20712'),
  (142, 34, 110, 0, 'Dairi', 'Kabupaten', '22211'),
  (143, 34, 112, 0, 'Deli Serdang', 'Kabupaten', '20511'),
  (144, 34, 137, 0, 'Gunungsitoli', 'Kota', '22813'),
  (145, 34, 146, 0, 'Humbang Hasundutan', 'Kabupaten', '22457'),
  (146, 34, 173, 0, 'Karo', 'Kabupaten', '22119'),
  (147, 34, 217, 0, 'Labuhan Batu', 'Kabupaten', '21412'),
  (148, 34, 218, 0, 'Labuhan Batu Selatan', 'Kabupaten', '21511'),
  (149, 34, 219, 0, 'Labuhan Batu Utara', 'Kabupaten', '21711'),
  (150, 34, 229, 0, 'Langkat', 'Kabupaten', '20811'),
  (151, 34, 268, 0, 'Mandailing Natal', 'Kabupaten', '22916'),
  (152, 34, 278, 0, 'Medan', 'Kota', '20228'),
  (153, 34, 307, 0, 'Nias', 'Kabupaten', '22876'),
  (154, 34, 308, 0, 'Nias Barat', 'Kabupaten', '22895'),
  (155, 34, 309, 0, 'Nias Selatan', 'Kabupaten', '22865'),
  (156, 34, 310, 0, 'Nias Utara', 'Kabupaten', '22856'),
  (157, 34, 319, 0, 'Padang Lawas', 'Kabupaten', '22763'),
  (158, 34, 320, 0, 'Padang Lawas Utara', 'Kabupaten', '22753'),
  (159, 34, 323, 0, 'Padang Sidempuan', 'Kota', '22727'),
  (160, 34, 325, 0, 'Pakpak Bharat', 'Kabupaten', '22272'),
  (161, 34, 353, 0, 'Pematang Siantar', 'Kota', '21126'),
  (162, 34, 389, 0, 'Samosir', 'Kabupaten', '22392'),
  (163, 34, 404, 0, 'Serdang Bedagai', 'Kabupaten', '20915'),
  (164, 34, 407, 0, 'Sibolga', 'Kota', '22522'),
  (165, 34, 413, 0, 'Simalungun', 'Kabupaten', '21162'),
  (166, 34, 459, 0, 'Tanjung Balai', 'Kota', '21321'),
  (167, 34, 463, 0, 'Tapanuli Selatan', 'Kabupaten', '22742'),
  (168, 34, 464, 0, 'Tapanuli Tengah', 'Kabupaten', '22611'),
  (169, 34, 465, 0, 'Tapanuli Utara', 'Kabupaten', '22414'),
  (170, 34, 470, 0, 'Tebing Tinggi', 'Kota', '20632'),
  (171, 34, 481, 0, 'Toba Samosir', 'Kabupaten', '22316'),
  (172, 34, 278, 3906, 'Medan Amplas', 'Kecamatan', ''),
  (173, 34, 278, 3907, 'Medan Area', 'Kecamatan', ''),
  (174, 34, 278, 3908, 'Medan Barat', 'Kecamatan', ''),
  (175, 34, 278, 3909, 'Medan Baru', 'Kecamatan', ''),
  (176, 34, 278, 3910, 'Medan Belawan Kota', 'Kecamatan', ''),
  (177, 34, 278, 3911, 'Medan Deli', 'Kecamatan', ''),
  (178, 34, 278, 3912, 'Medan Denai', 'Kecamatan', ''),
  (179, 34, 278, 3913, 'Medan Helvetia', 'Kecamatan', ''),
  (180, 34, 278, 3914, 'Medan Johor', 'Kecamatan', ''),
  (181, 34, 278, 3915, 'Medan Kota', 'Kecamatan', ''),
  (182, 34, 278, 3916, 'Medan Labuhan', 'Kecamatan', ''),
  (183, 34, 278, 3917, 'Medan Maimun', 'Kecamatan', ''),
  (184, 34, 278, 3918, 'Medan Marelan', 'Kecamatan', ''),
  (185, 34, 278, 3919, 'Medan Perjuangan', 'Kecamatan', ''),
  (186, 34, 278, 3920, 'Medan Petisah', 'Kecamatan', ''),
  (187, 34, 278, 3921, 'Medan Polonia', 'Kecamatan', ''),
  (188, 34, 278, 3922, 'Medan Selayang', 'Kecamatan', ''),
  (189, 34, 278, 3923, 'Medan Sunggal', 'Kecamatan', ''),
  (190, 34, 278, 3924, 'Medan Tembung', 'Kecamatan', ''),
  (191, 34, 278, 3925, 'Medan Timur', 'Kecamatan', ''),
  (192, 34, 278, 3926, 'Medan Tuntungan', 'Kecamatan', ''),
  (193, 1, 17, 0, 'Badung', 'Kabupaten', '80351'),
  (194, 1, 32, 0, 'Bangli', 'Kabupaten', '80619'),
  (195, 1, 94, 0, 'Buleleng', 'Kabupaten', '81111'),
  (196, 1, 114, 0, 'Denpasar', 'Kota', '80227'),
  (197, 1, 128, 0, 'Gianyar', 'Kabupaten', '80519'),
  (198, 1, 161, 0, 'Jembrana', 'Kabupaten', '82251'),
  (199, 1, 170, 0, 'Karangasem', 'Kabupaten', '80819'),
  (200, 1, 197, 0, 'Klungkung', 'Kabupaten', '80719'),
  (201, 1, 447, 0, 'Tabanan', 'Kabupaten', '82119'),
  (202, 1, 32, 472, 'Bangli', 'Kecamatan', ''),
  (203, 1, 32, 473, 'Kintamani', 'Kecamatan', ''),
  (204, 1, 32, 474, 'Susut', 'Kecamatan', ''),
  (205, 1, 32, 475, 'Tembuku', 'Kecamatan', ''),
  (206, 11, 178, 2497, 'Badas', 'Kecamatan', ''),
  (207, 11, 178, 2498, 'Banyakan', 'Kecamatan', ''),
  (208, 11, 178, 2499, 'Gampengrejo', 'Kecamatan', ''),
  (209, 11, 178, 2500, 'Grogol', 'Kecamatan', ''),
  (210, 11, 178, 2501, 'Gurah', 'Kecamatan', ''),
  (211, 11, 178, 2502, 'Kandangan', 'Kecamatan', ''),
  (212, 11, 178, 2503, 'Kandat', 'Kecamatan', ''),
  (213, 11, 178, 2504, 'Kayen Kidul', 'Kecamatan', ''),
  (214, 11, 178, 2505, 'Kepung', 'Kecamatan', ''),
  (215, 11, 178, 2506, 'Kras', 'Kecamatan', ''),
  (216, 11, 178, 2507, 'Kunjang', 'Kecamatan', ''),
  (217, 11, 178, 2508, 'Mojo', 'Kecamatan', ''),
  (218, 11, 178, 2509, 'Ngadiluwih', 'Kecamatan', ''),
  (219, 11, 178, 2510, 'Ngancar', 'Kecamatan', ''),
  (220, 11, 178, 2511, 'Ngasem', 'Kecamatan', ''),
  (221, 11, 178, 2512, 'Pagu', 'Kecamatan', ''),
  (222, 11, 178, 2513, 'Papar', 'Kecamatan', ''),
  (223, 11, 178, 2514, 'Pare', 'Kecamatan', ''),
  (224, 11, 178, 2515, 'Plemahan', 'Kecamatan', ''),
  (225, 11, 178, 2516, 'Plosoklaten', 'Kecamatan', ''),
  (226, 11, 178, 2517, 'Puncu', 'Kecamatan', ''),
  (227, 11, 178, 2518, 'Purwoasri', 'Kecamatan', ''),
  (228, 11, 178, 2519, 'Ringinrejo', 'Kecamatan', ''),
  (229, 11, 178, 2520, 'Semen', 'Kecamatan', ''),
  (230, 11, 178, 2521, 'Tarokan', 'Kecamatan', ''),
  (231, 11, 178, 2522, 'Wates', 'Kecamatan', ''),
  (232, 1, 94, 1279, 'Banjar', 'Kecamatan', ''),
  (233, 1, 94, 1280, 'Buleleng', 'Kecamatan', ''),
  (234, 1, 94, 1281, 'Busungbiu', 'Kecamatan', ''),
  (235, 1, 94, 1282, 'Gerokgak', 'Kecamatan', ''),
  (236, 1, 94, 1283, 'Kubutambahan', 'Kecamatan', ''),
  (237, 1, 94, 1284, 'Sawan', 'Kecamatan', ''),
  (238, 1, 94, 1285, 'Seririt', 'Kecamatan', ''),
  (239, 1, 94, 1286, 'Sukasada', 'Kecamatan', ''),
  (240, 1, 94, 1287, 'Tejakula', 'Kecamatan', ''),
  (241, 11, 256, 3634, 'Blimbing', 'Kecamatan', ''),
  (242, 11, 256, 3635, 'Kedungkandang', 'Kecamatan', ''),
  (243, 11, 256, 3636, 'Klojen', 'Kecamatan', ''),
  (244, 11, 256, 3637, 'Lowokwaru', 'Kecamatan', ''),
  (245, 11, 256, 3638, 'Sukun', 'Kecamatan', ''),
  (246, 30, 53, 0, 'Bau-Bau', 'Kota', '93719'),
  (247, 30, 85, 0, 'Bombana', 'Kabupaten', '93771'),
  (248, 30, 101, 0, 'Buton', 'Kabupaten', '93754'),
  (249, 30, 102, 0, 'Buton Utara', 'Kabupaten', '93745'),
  (250, 30, 182, 0, 'Kendari', 'Kota', '93126'),
  (251, 30, 198, 0, 'Kolaka', 'Kabupaten', '93511'),
  (252, 30, 199, 0, 'Kolaka Utara', 'Kabupaten', '93911'),
  (253, 30, 200, 0, 'Konawe', 'Kabupaten', '93411'),
  (254, 30, 201, 0, 'Konawe Selatan', 'Kabupaten', '93811'),
  (255, 30, 202, 0, 'Konawe Utara', 'Kabupaten', '93311'),
  (256, 30, 295, 0, 'Muna', 'Kabupaten', '93611'),
  (257, 30, 494, 0, 'Wakatobi', 'Kabupaten', '93791'),
  (258, 30, 295, 4157, 'Barangka', 'Kecamatan', ''),
  (259, 30, 295, 4158, 'Batalaiwaru (Batalaiworu)', 'Kecamatan', ''),
  (260, 30, 295, 4159, 'Batukara', 'Kecamatan', ''),
  (261, 30, 295, 4160, 'Bone (Bone Tondo)', 'Kecamatan', ''),
  (262, 30, 295, 4161, 'Duruka', 'Kecamatan', ''),
  (263, 30, 295, 4162, 'Kabangka', 'Kecamatan', ''),
  (264, 30, 295, 4163, 'Kabawo', 'Kecamatan', ''),
  (265, 30, 295, 4164, 'Katobu', 'Kecamatan', ''),
  (266, 30, 295, 4165, 'Kontu Kowuna', 'Kecamatan', ''),
  (267, 30, 295, 4166, 'Kontunaga', 'Kecamatan', ''),
  (268, 30, 295, 4167, 'Kusambi', 'Kecamatan', ''),
  (269, 30, 295, 4168, 'Lasalepa', 'Kecamatan', ''),
  (270, 30, 295, 4169, 'Lawa', 'Kecamatan', ''),
  (271, 30, 295, 4170, 'Lohia', 'Kecamatan', ''),
  (272, 30, 295, 4171, 'Maginti', 'Kecamatan', ''),
  (273, 30, 295, 4172, 'Maligano', 'Kecamatan', ''),
  (274, 30, 295, 4173, 'Marobo', 'Kecamatan', ''),
  (275, 30, 295, 4174, 'Napabalano', 'Kecamatan', ''),
  (276, 30, 295, 4175, 'Napano Kusambi', 'Kecamatan', ''),
  (277, 30, 295, 4176, 'Parigi', 'Kecamatan', ''),
  (278, 30, 295, 4177, 'Pasi Kolaga', 'Kecamatan', ''),
  (279, 30, 295, 4178, 'Pasir Putih', 'Kecamatan', ''),
  (280, 30, 295, 4179, 'Sawerigadi (Sawerigading/Sewergadi)', 'Kecamatan', ''),
  (281, 30, 295, 4180, 'Tiworo Kepulauan', 'Kecamatan', ''),
  (282, 30, 295, 4181, 'Tiworo Selatan', 'Kecamatan', ''),
  (283, 30, 295, 4182, 'Tiworo Tengah', 'Kecamatan', ''),
  (284, 30, 295, 4183, 'Tiworo Utara', 'Kecamatan', ''),
  (285, 30, 295, 4184, 'Tongkuno', 'Kecamatan', ''),
  (286, 30, 295, 4185, 'Tongkuno Selatan', 'Kecamatan', ''),
  (287, 30, 295, 4186, 'Towea', 'Kecamatan', ''),
  (288, 30, 295, 4187, 'Wa Daga', 'Kecamatan', ''),
  (289, 30, 295, 4188, 'Wakorumba Selatan', 'Kecamatan', ''),
  (290, 30, 295, 4189, 'Watopute', 'Kecamatan', '');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer`
(
  `manufacturer_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  64
) NOT NULL,
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `image` varchar
(
  255
) DEFAULT NULL,
  `sort_order` int
(
  3
) NOT NULL,
  PRIMARY KEY
(
  `manufacturer_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `offers_category`
--

CREATE TABLE IF NOT EXISTS `offers_category`
(
  `offers_category_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `category_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `offers_category_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

--
-- Dumping data for table `offers_category`
--

INSERT INTO `offers_category` (`offers_category_id`, `category_id`, `name`, `sort_order`)
VALUES
  (1, 2, 'Komponen', 1),
  (2, 3, 'Motherboard', 2);

-- --------------------------------------------------------

--
-- Table structure for table `offers_discount`
--

CREATE TABLE IF NOT EXISTS `offers_discount`
(
  `offers_discount_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `priority` int
(
  5
) NOT NULL DEFAULT '1',
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `date_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `offers_discount_id`
),
  KEY `product_id`
(
  `product_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

--
-- Dumping data for table `offers_discount`
--

INSERT INTO `offers_discount` (`offers_discount_id`, `product_id`, `priority`, `price`, `date_start`, `date_end`)
VALUES
  (1, 63, 2, '450000.00', '2017-11-19 15:30:00', '2017-11-19 23:55:00'),
  (2, 64, 1, '250000.00', '2017-11-19 08:21:00', '2017-11-30 15:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

CREATE TABLE IF NOT EXISTS `option`
(
  `option_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `type` varchar
(
  32
) NOT NULL,
  `name` varchar
(
  128
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL,
  PRIMARY KEY
(
  `option_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=7;

--
-- Dumping data for table `option`
--

INSERT INTO `option` (`option_id`, `type`, `name`, `sort_order`)
VALUES
  (5, 'select', 'Warna', 1),
  (6, 'radio', 'Operating System', 2);

-- --------------------------------------------------------

--
-- Table structure for table `option_value`
--

CREATE TABLE IF NOT EXISTS `option_value`
(
  `option_value_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `option_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  128
) NOT NULL,
  `image` varchar
(
  255
) NOT NULL,
  `sort_order` int
(
  3
) NOT NULL,
  PRIMARY KEY
(
  `option_value_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=12;

--
-- Dumping data for table `option_value`
--

INSERT INTO `option_value` (`option_value_id`, `option_id`, `name`, `image`, `sort_order`)
VALUES
  (9, 6, 'Windows 7', '', 1),
  (8, 5, 'Hijau', '', 3),
  (7, 5, 'Kuning', '', 2),
  (6, 5, 'Merah', '', 1),
  (10, 6, 'Windows 8', '', 2),
  (11, 6, 'Windows 10', '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order`
(
  `order_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `invoice_prefix` varchar
(
  32
) NOT NULL DEFAULT '',
  `invoice_no` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `email` varchar
(
  255
) NOT NULL DEFAULT '',
  `telephone` varchar
(
  32
) NOT NULL DEFAULT '',
  `address` varchar
(
  128
) NOT NULL DEFAULT '',
  `postcode` varchar
(
  10
) NOT NULL DEFAULT '',
  `province_id` int
(
  11
) NOT NULL DEFAULT '0',
  `province` varchar
(
  128
) NOT NULL DEFAULT '',
  `city_id` int
(
  11
) NOT NULL DEFAULT '0',
  `city` varchar
(
  128
) NOT NULL DEFAULT '',
  `subdistrict_id` int
(
  11
) NOT NULL DEFAULT '0',
  `subdistrict` varchar
(
  128
) NOT NULL DEFAULT '',
  `user_agent` varchar
(
  255
) NOT NULL DEFAULT '',
  `payment_method` varchar
(
  128
) NOT NULL DEFAULT '',
  `payment_code` varchar
(
  128
) NOT NULL DEFAULT '',
  `shipping_method` varchar
(
  128
) NOT NULL DEFAULT '',
  `shipping_code` varchar
(
  128
) NOT NULL DEFAULT '',
  `waybill` varchar
(
  64
) NOT NULL DEFAULT '',
  `currency_id` int
(
  11
) NOT NULL DEFAULT '0',
  `currency_value` decimal
(
  15,
  8
) NOT NULL DEFAULT '0.00000000',
  `comment` text,
  `total` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `order_status_id` int
(
  11
) NOT NULL DEFAULT '0',
  `ip` varchar
(
  40
) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_paid` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `order_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=26;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `invoice_prefix`, `invoice_no`, `customer_id`, `name`, `email`, `telephone`, `address`,
                     `postcode`, `province_id`, `province`, `city_id`, `city`, `subdistrict_id`, `subdistrict`,
                     `user_agent`, `payment_method`, `payment_code`, `shipping_method`, `shipping_code`, `waybill`,
                     `currency_id`, `currency_value`, `comment`, `total`, `order_status_id`, `ip`, `date_added`,
                     `date_modified`, `date_paid`)
VALUES
(6, 'INV/2017/XI/', 6, 2, '', '', '', '', '', 0, '', 0, '', 0, '',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', NULL, '1425000.00', 1, '127.0.0.1',
 '2017-11-15 18:20:48', '2017-11-15 18:43:17', '0000-00-00 00:00:00'),
(7, 'INV/2017/XI/', 7, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Cengkeh No. 55', '65141', 11,
 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer Mandiri', 'mandiri',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', NULL, '1425000.00', 1, '127.0.0.1',
 '2017-11-15 18:51:12', '2017-11-15 18:51:14', '0000-00-00 00:00:00'),
(8, 'INV/2017/XI/', 8, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Cengkeh No. 55', '65141', 11,
 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '1234567890', 8, '1.00000000', NULL, '1425000.00', 1,
 '127.0.0.1', '2017-11-16 05:29:50', '2017-11-16 06:50:16', '0000-00-00 00:00:00'),
(9, 'INV/2017/XI/', 9, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Cengkeh No. 55', '65141', 11,
 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - REG', 'rajaongkir.jne_reg', '', 8, '1.00000000', NULL, '4316000.00', 1, '127.0.0.1',
 '2017-11-16 09:11:16', '2017-11-16 09:39:17', '0000-00-00 00:00:00'),
(10, 'INV/2017/XI/', 10, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Cengkeh No. 55', '65141', 11,
 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', NULL, '1425000.00', 1, '127.0.0.1',
 '2017-11-17 05:02:59', '2017-11-17 05:03:03', '0000-00-00 00:00:00'),
(11, 'INV/2017/XI/', 11, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Cengkeh No. 55', '65141', 11,
 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', NULL, '1425000.00', 1, '127.0.0.1',
 '2017-11-17 08:15:55', '2017-11-17 08:16:55', '0000-00-00 00:00:00'),
(12, 'INV/2017/XI/', 12, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Cengkeh No. 55', '65141', 11,
 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', NULL, '1425000.00', 1, '127.0.0.1',
 '2017-11-17 08:29:35', '2017-11-17 08:29:38', '0000-00-00 00:00:00'),
(13, 'INV/2017/XI/', 13, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456',
 11, 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', NULL, '2765000.00', 1, '127.0.0.1',
 '2017-11-18 17:30:49', '2017-11-18 17:31:02', '0000-00-00 00:00:00'),
(14, 'INV/2017/XI/', 14, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456',
 11, 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer Mandiri', 'mandiri',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', NULL, '2765000.00', 1, '127.0.0.1',
 '2017-11-18 17:36:21', '2017-11-18 17:36:52', '0000-00-00 00:00:00'),
(15, 'INV/2017/XI/', 15, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456',
 11, 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', NULL, '2765000.00', 1, '127.0.0.1',
 '2017-11-18 17:45:45', '2017-11-18 17:45:48', '0000-00-00 00:00:00'),
(16, 'INV/2017/XI/', 16, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456',
 11, 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', 'Pembelian PC Rakitan', '2765000.00',
 1, '127.0.0.1', '2017-11-18 17:52:51', '2017-11-18 17:54:55', '0000-00-00 00:00:00'),
(17, 'INV/2017/XI/', 17, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456',
 11, 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer Mandiri', 'mandiri',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', '', '1035000.00', 1, '127.0.0.1',
 '2017-11-18 17:57:29', '2017-11-18 18:00:04', '0000-00-00 00:00:00'),
(18, 'INV/2017/XI/', 18, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456',
 11, 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', 'Pembelian PC Rakitan', '2765000.00',
 3, '127.0.0.1', '2017-11-18 18:07:49', '2017-11-18 18:14:43', '0000-00-00 00:00:00'),
(19, '', 0, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456', 11,
 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', '', '875000.00', 0, '127.0.0.1',
 '2017-11-18 18:24:28', '2017-11-18 18:24:28', '0000-00-00 00:00:00'),
(20, '', 0, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456', 11,
 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', '', '875000.00', 0, '127.0.0.1',
 '2017-11-18 18:25:32', '2017-11-18 18:25:32', '0000-00-00 00:00:00'),
(21, 'INV/2017/XI/', 21, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456',
 11, 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', '', '875000.00', 1, '127.0.0.1',
 '2017-11-18 18:26:36', '2017-11-18 18:29:12', '0000-00-00 00:00:00'),
(22, 'INV/2017/XI/', 22, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456',
 11, 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', '', '875000.00', 1, '127.0.0.1',
 '2017-11-18 18:42:41', '2017-11-18 18:43:57', '0000-00-00 00:00:00'),
(23, 'INV/2017/XI/', 23, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456',
 11, 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', '', '875000.00', 1, '127.0.0.1',
 '2017-11-18 18:46:22', '2017-11-18 18:46:57', '0000-00-00 00:00:00'),
(24, 'INV/2017/XI/', 24, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Pahlawan No. 123', '123456',
 11, 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:48.0) Gecko/20100101 Firefox/48.0', 'Transfer BCA', 'bca',
 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '410120030938817', 8, '1.00000000', '', '875000.00', 7,
 '127.0.0.1', '2017-11-18 18:48:00', '2017-11-20 20:30:06', '0000-00-00 00:00:00'),
(25, 'INV/2017/XI/', 25, 2, 'Adi Setia', 'mas.adisetiawan@gmail.com', '1234567890', 'Jl. Cengkeh No. 55', '65141', 11,
 'Jawa Timur', 256, 'Kota Malang', 3637, 'Lowokwaru',
 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.23 Mobile Safari/537.36',
 'Transfer Mandiri', 'mandiri', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'rajaongkir.jne_oke', '', 8, '1.00000000', '',
 '875000.00', 1, '127.0.0.1', '2017-11-21 15:20:22', '2017-11-21 15:44:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

CREATE TABLE IF NOT EXISTS `order_history`
(
  `order_history_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `order_status_id` int
(
  5
) NOT NULL DEFAULT '0',
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `admin_id` int
(
  11
) NOT NULL DEFAULT '0',
  `notify` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `comment` text,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `order_history_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=51;

--
-- Dumping data for table `order_history`
--

INSERT INTO `order_history` (`order_history_id`, `order_status_id`, `order_id`, `admin_id`, `notify`, `comment`,
                             `date_added`, `date_modified`)
VALUES
  (11, 1, 7, 0, 1, NULL, '2017-11-15 18:51:14', '2017-11-15 18:51:14'),
  (12, 1, 8, 0, 1, NULL, '2017-11-16 05:29:54', '2017-11-16 05:29:54'),
  (13, 1, 8, -1, 0, '', '2017-11-16 06:34:32', '2017-11-16 06:34:32'),
  (14, 6, 8, -1, 1, '', '2017-11-16 06:34:53', '2017-11-16 06:34:53'),
  (15, 1, 8, -1, 0, '', '2017-11-16 06:50:16', '2017-11-16 06:50:16'),
  (16, 1, 9, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-16 09:39:17', '2017-11-16 09:39:17'),
  (17, 1, 10, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-17 05:03:03', '2017-11-17 05:03:03'),
  (18, 1, 11, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-17 08:16:00', '2017-11-17 08:16:00'),
  (19, 1, 12, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-17 08:29:38', '2017-11-17 08:29:38'),
  (20, 1, 13, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-18 17:31:02', '2017-11-18 17:31:02'),
  (21, 1, 14, 0, 1,
   '<p><img style="width: 199.812px; height: 105px;" src="http://medansoft.dev/storage/images/data/logo/Logo_Bank_Mandiri.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank Mandiri berikut :</p><p>Nomor : 1234567890</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Malang<br></p>',
   '2017-11-18 17:36:52', '2017-11-18 17:36:52'),
  (22, 1, 15, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-18 17:45:48', '2017-11-18 17:45:48'),
  (23, 1, 16, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-18 17:54:55', '2017-11-18 17:54:55'),
  (24, 1, 17, 0, 1,
   '<p><img style="width: 199.812px; height: 105px;" src="http://medansoft.dev/storage/images/data/logo/Logo_Bank_Mandiri.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank Mandiri berikut :</p><p>Nomor : 1234567890</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Malang<br></p>',
   '2017-11-18 18:00:04', '2017-11-18 18:00:04'),
  (25, 1, 18, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-18 18:13:17', '2017-11-18 18:13:17'),
  (26, 3, 18, -1, 1, 'Pembayaran diterima', '2017-11-18 18:14:43', '2017-11-18 18:14:43'),
  (27, 1, 21, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-18 18:29:12', '2017-11-18 18:29:12'),
  (28, 1, 22, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-18 18:43:57', '2017-11-18 18:43:57'),
  (29, 1, 23, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-18 18:46:27', '2017-11-18 18:46:27'),
  (30, 1, 24, 0, 1,
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   '2017-11-18 18:48:06', '2017-11-18 18:48:06'),
  (31, 6, 24, -1, 1, '', '2017-11-19 08:59:22', '2017-11-19 08:59:22'),
  (32, 6, 24, -1, 1, 'Update contoh nomor resi JNE', '2017-11-19 09:03:44', '2017-11-19 09:03:44'),
  (33, 6, 24, -1, 1, '', '2017-11-19 09:05:45', '2017-11-19 09:05:45'),
  (34, 1, 0, 0, 1, 'Barang bagus dan cepat sampai tujuan', '2017-11-20 20:03:35', '2017-11-20 20:03:35'),
  (35, 1, 0, 0, 1, 'Sip barang sudah diterima', '2017-11-20 20:03:55', '2017-11-20 20:03:55'),
  (36, 7, 0, 0, 1, 'Mantap barang sudah diterima....', '2017-11-20 20:04:43', '2017-11-20 20:04:43'),
  (37, 7, 0, 0, 1, 'Mantap barang udah diterima', '2017-11-20 20:05:31', '2017-11-20 20:05:31'),
  (38, 7, 0, 0, 1, 'Bagus barang udah diterima', '2017-11-20 20:07:32', '2017-11-20 20:07:32'),
  (39, 7, 0, 0, 1, 'Bagus barang udah diterima', '2017-11-20 20:08:40', '2017-11-20 20:08:40'),
  (40, 7, 0, 0, 1, 'Bagus barang udah diterima', '2017-11-20 20:09:04', '2017-11-20 20:09:04'),
  (41, 7, 0, 0, 1, 'Bagus barang udah diterima', '2017-11-20 20:09:17', '2017-11-20 20:09:17'),
  (42, 7, 0, 0, 1, 'Bagus barang udah diterima', '2017-11-20 20:09:46', '2017-11-20 20:09:46'),
  (43, 7, 0, 0, 1, 'Bagus barang udah diterima', '2017-11-20 20:10:20', '2017-11-20 20:10:20'),
  (44, 7, 0, 0, 1, 'Bagus barang udah diterima', '2017-11-20 20:10:48', '2017-11-20 20:10:48'),
  (45, 7, 0, 0, 1, 'Bagus barang udah diterima', '2017-11-20 20:11:00', '2017-11-20 20:11:00'),
  (46, 7, 0, 0, 1, 'Bagus barang udah diterima', '2017-11-20 20:12:32', '2017-11-20 20:12:32'),
  (47, 7, 24, 0, 1, 'Mantap barang sudah diterima dalam keadaan baik', '2017-11-20 20:13:43', '2017-11-20 20:13:43'),
  (48, 6, 24, -1, 0, '', '2017-11-20 20:19:41', '2017-11-20 20:19:41'),
  (49, 7, 24, 0, 1, 'Lumayan cepet pengirimannya', '2017-11-20 20:30:06', '2017-11-20 20:30:06'),
  (50, 1, 25, 0, 1,
   '<p><img style="width: 199.812px; height: 105px;" src="http://medansoft.dev/storage/images/data/logo/Logo_Bank_Mandiri.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank Mandiri berikut :</p><p>Nomor : 1234567890</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Malang<br></p>',
   '2017-11-21 15:44:39', '2017-11-21 15:44:39');

-- --------------------------------------------------------

--
-- Table structure for table `order_option`
--

CREATE TABLE IF NOT EXISTS `order_option`
(
  `order_option_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `order_id` int
(
  11
) NOT NULL,
  `order_product_id` int
(
  11
) NOT NULL,
  `product_option_id` int
(
  11
) NOT NULL,
  `product_option_value_id` int
(
  11
) NOT NULL,
  `name` varchar
(
  255
) NOT NULL,
  `value` text NOT NULL,
  `type` varchar
(
  32
) NOT NULL,
  PRIMARY KEY
(
  `order_option_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=23;

--
-- Dumping data for table `order_option`
--

INSERT INTO `order_option` (`order_option_id`, `order_id`, `order_product_id`, `product_option_id`,
                            `product_option_value_id`, `name`, `value`, `type`)
VALUES
  (3, 21, 66, 53, 122, 'Warna', 'Hijau', 'select'),
  (5, 22, 68, 53, 121, 'Warna', 'Kuning', 'select'),
  (7, 23, 70, 53, 121, 'Warna', 'Kuning', 'select'),
  (8, 24, 71, 53, 120, 'Warna', 'Merah', 'select'),
  (22, 25, 85, 53, 122, 'Warna', 'Hijau', 'select');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE IF NOT EXISTS `order_product`
(
  `order_product_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `sku` varchar
(
  64
) NOT NULL DEFAULT '',
  `quantity` int
(
  4
) NOT NULL DEFAULT '0',
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `total` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  PRIMARY KEY
(
  `order_product_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=86;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`order_product_id`, `order_id`, `product_id`, `name`, `sku`, `quantity`, `price`, `total`)
VALUES
(9, 6, 63, 'Prosesor Intel  Core i7', '123456789', 1, '1400000.00', '1400000.00'),
(10, 7, 63, 'Prosesor Intel  Core i7', '123456789', 1, '1400000.00', '1400000.00'),
(11, 8, 63, 'Prosesor Intel  Core i7', '123456789', 1, '1400000.00', '1400000.00'),
(18, 9, 63, 'Prosesor Intel  Core i7', '123456789', 3, '1400000.00', '4200000.00'),
(19, 10, 63, 'Prosesor Intel  Core i7', '123456789', 1, '1400000.00', '1400000.00'),
(21, 11, 63, 'Prosesor Intel  Core i7', '123456789', 1, '1400000.00', '1400000.00'),
(22, 12, 63, 'Prosesor Intel  Core i7', '123456789', 1, '1400000.00', '1400000.00'),
(23, 13, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(24, 13, 64, 'Motherboard Asus', '12345678', 1, '800000.00', '800000.00'),
(25, 13, 100, 'HDD 500GB 5400', '212312445', 1, '480000.00', '480000.00'),
(26, 13, 99, 'RAM 1 GB Visipro DDR3', '122324235', 1, '560000.00', '560000.00'),
(33, 14, 100, 'HDD 500GB 5400', '212312445', 1, '480000.00', '480000.00'),
(34, 14, 99, 'RAM 1 GB Visipro DDR3', '122324235', 1, '560000.00', '560000.00'),
(32, 14, 64, 'Motherboard Asus', '12345678', 1, '800000.00', '800000.00'),
(31, 14, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(35, 15, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(36, 15, 64, 'Motherboard Asus', '12345678', 1, '800000.00', '800000.00'),
(37, 15, 100, 'HDD 500GB 5400', '212312445', 1, '480000.00', '480000.00'),
(38, 15, 99, 'RAM 1 GB Visipro DDR3', '122324235', 1, '560000.00', '560000.00'),
(39, 16, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(40, 16, 64, 'Motherboard Asus', '12345678', 1, '800000.00', '800000.00'),
(41, 16, 100, 'HDD 500GB 5400', '212312445', 1, '480000.00', '480000.00'),
(42, 16, 99, 'RAM 1 GB Visipro DDR3', '122324235', 1, '560000.00', '560000.00'),
(45, 17, 100, 'HDD 500GB 5400', '212312445', 2, '480000.00', '960000.00'),
(61, 18, 99, 'RAM 1 GB Visipro DDR3', '122324235', 1, '560000.00', '560000.00'),
(62, 19, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(59, 18, 64, 'Motherboard Asus', '12345678', 1, '800000.00', '800000.00'),
(60, 18, 100, 'HDD 500GB 5400', '212312445', 1, '480000.00', '480000.00'),
(58, 18, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(63, 20, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(66, 21, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(68, 22, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(70, 23, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(71, 24, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00'),
(85, 25, 63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', '123456789', 1, '850000.00',
 '850000.00');

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE IF NOT EXISTS `order_status`
(
  `order_status_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  32
) NOT NULL DEFAULT '',
  `group` varchar
(
  8
) NOT NULL DEFAULT 'payment',
  PRIMARY KEY
(
  `order_status_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=8;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`order_status_id`, `name`, `group`)
VALUES
  (1, 'Pending', 'payment'),
  (2, 'Belum Lunas', 'payment'),
  (3, 'Lunas', 'payment'),
  (4, 'Diproses', 'payment'),
  (5, 'Batal', 'payment'),
  (6, 'Dikirim', 'payment'),
  (7, 'Selesai', 'payment');

-- --------------------------------------------------------

--
-- Table structure for table `order_total`
--

CREATE TABLE IF NOT EXISTS `order_total`
(
  `order_total_id` int
(
  10
) NOT NULL AUTO_INCREMENT,
  `order_id` int
(
  11
) NOT NULL DEFAULT '0',
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `title` varchar
(
  255
) NOT NULL DEFAULT '',
  `text` varchar
(
  255
) NOT NULL DEFAULT '',
  `value` decimal
(
  15,
  4
) NOT NULL DEFAULT '0.0000',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `order_total_id`
),
  KEY `idx_orders_total_orders_id`
(
  `order_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=172;

--
-- Dumping data for table `order_total`
--

INSERT INTO `order_total` (`order_total_id`, `order_id`, `code`, `title`, `text`, `value`, `sort_order`)
VALUES
  (30, 6, 'total', 'Total', 'Rp 1.425.000', '1425000.0000', 8),
  (28, 6, 'subtotal', 'Sub Total', 'Rp 1.400.000', '1400000.0000', 1),
  (29, 6, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6),
  (31, 7, 'subtotal', 'Sub Total', 'Rp 1.400.000', '1400000.0000', 1),
  (32, 7, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6),
  (33, 7, 'total', 'Total', 'Rp 1.425.000', '1425000.0000', 8),
  (34, 8, 'subtotal', 'Sub Total', 'Rp 1.400.000', '1400000.0000', 1),
  (35, 8, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6),
  (36, 8, 'total', 'Total', 'Rp 1.425.000', '1425000.0000', 8),
  (57, 9, 'total', 'Total', 'Rp 4.316.000', '4316000.0000', 8),
  (55, 9, 'subtotal', 'Sub Total', 'Rp 4.200.000', '4200000.0000', 1),
  (56, 9, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - REG', 'Rp 116.000', '116000.0000', 6),
  (58, 10, 'subtotal', 'Sub Total', 'Rp 1.400.000', '1400000.0000', 1),
  (59, 10, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6),
  (60, 10, 'total', 'Total', 'Rp 1.425.000', '1425000.0000', 8),
  (65, 11, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6),
  (64, 11, 'subtotal', 'Sub Total', 'Rp 1.400.000', '1400000.0000', 1),
  (66, 11, 'total', 'Total', 'Rp 1.425.000', '1425000.0000', 8),
  (67, 12, 'subtotal', 'Sub Total', 'Rp 1.400.000', '1400000.0000', 1),
  (68, 12, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6),
  (69, 12, 'total', 'Total', 'Rp 1.425.000', '1425000.0000', 8),
  (70, 13, 'subtotal', 'Sub Total', 'Rp 2.690.000', '2690000.0000', 1),
  (71, 13, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 75.000', '75000.0000', 6),
  (72, 13, 'total', 'Total', 'Rp 2.765.000', '2765000.0000', 8),
  (77, 14, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 75.000', '75000.0000', 6),
  (76, 14, 'subtotal', 'Sub Total', 'Rp 2.690.000', '2690000.0000', 1),
  (78, 14, 'total', 'Total', 'Rp 2.765.000', '2765000.0000', 8),
  (79, 15, 'subtotal', 'Sub Total', 'Rp 2.690.000', '2690000.0000', 1),
  (80, 15, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 75.000', '75000.0000', 6),
  (81, 15, 'total', 'Total', 'Rp 2.765.000', '2765000.0000', 8),
  (82, 16, 'subtotal', 'Sub Total', 'Rp 2.690.000', '2690000.0000', 1),
  (83, 16, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 75.000', '75000.0000', 6),
  (84, 16, 'total', 'Total', 'Rp 2.765.000', '2765000.0000', 8),
  (93, 17, 'total', 'Total', 'Rp 1.035.000', '1035000.0000', 8),
  (92, 17, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 75.000', '75000.0000', 6),
  (91, 17, 'subtotal', 'Sub Total', 'Rp 960.000', '960000.0000', 1),
  (105, 18, 'total', 'Total', 'Rp 2.765.000', '2765000.0000', 8),
  (103, 18, 'subtotal', 'Sub Total', 'Rp 2.690.000', '2690000.0000', 1),
  (104, 18, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 75.000', '75000.0000', 6),
  (114, 21, 'total', 'Total', 'Rp 875.000', '875000.0000', 8),
  (113, 21, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6),
  (112, 21, 'subtotal', 'Sub Total', 'Rp 850.000', '850000.0000', 1),
  (119, 22, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6),
  (118, 22, 'subtotal', 'Sub Total', 'Rp 850.000', '850000.0000', 1),
  (120, 22, 'total', 'Total', 'Rp 875.000', '875000.0000', 8),
  (125, 23, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6),
  (124, 23, 'subtotal', 'Sub Total', 'Rp 850.000', '850000.0000', 1),
  (126, 23, 'total', 'Total', 'Rp 875.000', '875000.0000', 8),
  (127, 24, 'subtotal', 'Sub Total', 'Rp 850.000', '850000.0000', 1),
  (128, 24, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6),
  (129, 24, 'total', 'Total', 'Rp 875.000', '875000.0000', 8),
  (171, 25, 'total', 'Total', 'Rp 875.000', '875000.0000', 8),
  (169, 25, 'subtotal', 'Sub Total', 'Rp 850.000', '850000.0000', 1),
  (170, 25, 'shipping', 'Jalur Nugraha Ekakurir (JNE) - OKE', 'Rp 25.000', '25000.0000', 6);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page`
(
  `page_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `title` varchar
(
  64
) NOT NULL DEFAULT '',
  `content` text,
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_description` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_keyword` varchar
(
  255
) NOT NULL DEFAULT '',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  `new_window` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `footer_column` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '1',
  PRIMARY KEY
(
  `page_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=7;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `title`, `content`, `slug`, `meta_description`, `meta_keyword`, `sort_order`,
                    `new_window`, `date_added`, `footer_column`, `date_modified`, `active`)
VALUES
(1, 'Tentang Kami',
 '<p align="justify">Sit an solum sonet erroribus. Et quando apeirian pertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua sit, ut vel ipsum graeci. At cum convenire similique, posse ubique insolens id sea, facer graece pro ea. In summo ullamcorper cum, ut oblique recusabo concludaturque per, sed soluta prompta percipitur ex. Ipsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam detraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae duo no.<br><br>Purto tation ea est, an utinam sententiae per. Eos eu fuisset cotidieque, meis numquam commune per an. Quo lorem atqui possit cu, et latine labores mea. Et vix aeterno facilis, ei natum omnes timeam pro. Quas malorum laboramus qui no, quas integre impedit ne duo. Eu ius graeco petentium, tollit invenire temporibus pri an, commune officiis intellegam ut eos. Mea quis suavitate at, offendit reformidans no has. Ne cum discere molestiae neglegentur. No has ubique vocent, id delicata recteque accommodare eam.<br><br>Vim ne admodum ancillae laboramus, audiam omnesque molestie duo in. An vis eleifend voluptatibus. Graecis ocurreret accusamus eos ad, recteque pericula comprehensam id mea, ad quot mnesarchum voluptatum quo. Mei amet oblique reprehendunt cu. At decore sanctus periculis pri. Soleat adolescens vix at. Persecuti voluptaria ad qui, vel id habemus vulputate delicatissimi, ius veniam meliore recteque ne. Ad soleat tempor prodesset nec, in scaevola tacimates pertinacia mel. Deserunt theophrastus usu an, nam vitae adolescens eu. Congue aperiri copiosae est ut, mea errem eloquentiam cu.<br><br>At impedit principes mea. Pro verear animal percipitur ut, id sonet mollis delicata vix. Pri in quis everti consequuntur, no utinam deseruisse eos. Sed agam movet semper in, ex elit tota aeque vel. Te quot quando nam. Legendos gubergren id cum. Purto duis conceptam ius ei. Pri at paulo patrioque consetetur, nec atqui doming eu. Est eu forensibus concludaturque, an mei vidit essent vituperatoribus. Usu no tota elitr, scripta eleifend dissentias ei has. Assum sonet dignissim ex qui, deleniti urbanitas qui no. Cum probo legere inimicus et, brute noster eum ut. Vix nihil electram at.<br><br>Vide mediocrem voluptatibus ne sit, inani eligendi facilisi nec an, cu eam simul primis definitiones. Ius cu diam clita, et lucilius omittantur mel. Omnes feugiat vivendum ut has, in qui ipsum latine apeirian. Ad fabellas explicari forensibus vis, eu usu nulla dolorum. Id doming consetetur posidonium nam, quas dictas ius ad, at vel nusquam delectus disputando. Possim sanctus electram et nec, eruditi oportere usu an. Cu pro invidunt appellantur, error repudiare cum ne. Eligendi electram et quo.<br><br>Erat putant in sed, ei quot graeci cum, an mundi labores accommodare nec. Pro at aeque accommodare, iusto aperiam has et. Dolor voluptatibus cum ad, his justo primis accusata cu, ne putant accusamus mel. Per partem vituperata at, at meliore similique mea. Ei erat constituam eam, nam et diceret dolorem principes, pro eu illum ignota detraxit. Novum possit volutpat usu no, vis fierent eleifend an, ea exerci argumentum vim.<br></p>',
 'tentang-kami', '', '', 1, 0, '2017-11-08 11:03:36', 1, '2017-11-08 11:14:46', 1),
(2, 'Ketentuan Pengiriman',
 '<p align="justify">Sit an solum sonet erroribus. Et quando apeirian pertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua sit, ut vel ipsum graeci. At cum convenire similique, posse ubique insolens id sea, facer graece pro ea. In summo ullamcorper cum, ut oblique recusabo concludaturque per, sed soluta prompta percipitur ex. Ipsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam detraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae duo no.<br><br>Purto tation ea est, an utinam sententiae per. Eos eu fuisset cotidieque, meis numquam commune per an. Quo lorem atqui possit cu, et latine labores mea. Et vix aeterno facilis, ei natum omnes timeam pro. Quas malorum laboramus qui no, quas integre impedit ne duo. Eu ius graeco petentium, tollit invenire temporibus pri an, commune officiis intellegam ut eos. Mea quis suavitate at, offendit reformidans no has. Ne cum discere molestiae neglegentur. No has ubique vocent, id delicata recteque accommodare eam.<br><br>Vim ne admodum ancillae laboramus, audiam omnesque molestie duo in. An vis eleifend voluptatibus. Graecis ocurreret accusamus eos ad, recteque pericula comprehensam id mea, ad quot mnesarchum voluptatum quo. Mei amet oblique reprehendunt cu. At decore sanctus periculis pri. Soleat adolescens vix at. Persecuti voluptaria ad qui, vel id habemus vulputate delicatissimi, ius veniam meliore recteque ne. Ad soleat tempor prodesset nec, in scaevola tacimates pertinacia mel. Deserunt theophrastus usu an, nam vitae adolescens eu. Congue aperiri copiosae est ut, mea errem eloquentiam cu.<br><br>At impedit principes mea. Pro verear animal percipitur ut, id sonet mollis delicata vix. Pri in quis everti consequuntur, no utinam deseruisse eos. Sed agam movet semper in, ex elit tota aeque vel. Te quot quando nam. Legendos gubergren id cum. Purto duis conceptam ius ei. Pri at paulo patrioque consetetur, nec atqui doming eu. Est eu forensibus concludaturque, an mei vidit essent vituperatoribus. Usu no tota elitr, scripta eleifend dissentias ei has. Assum sonet dignissim ex qui, deleniti urbanitas qui no. Cum probo legere inimicus et, brute noster eum ut. Vix nihil electram at.<br><br>Vide mediocrem voluptatibus ne sit, inani eligendi facilisi nec an, cu eam simul primis definitiones. Ius cu diam clita, et lucilius omittantur mel. Omnes feugiat vivendum ut has, in qui ipsum latine apeirian. Ad fabellas explicari forensibus vis, eu usu nulla dolorum. Id doming consetetur posidonium nam, quas dictas ius ad, at vel nusquam delectus disputando. Possim sanctus electram et nec, eruditi oportere usu an. Cu pro invidunt appellantur, error repudiare cum ne. Eligendi electram et quo.<br><br>Erat putant in sed, ei quot graeci cum, an mundi labores accommodare nec. Pro at aeque accommodare, iusto aperiam has et. Dolor voluptatibus cum ad, his justo primis accusata cu, ne putant accusamus mel. Per partem vituperata at, at meliore similique mea. Ei erat constituam eam, nam et diceret dolorem principes, pro eu illum ignota detraxit. Novum possit volutpat usu no, vis fierent eleifend an, ea exerci argumentum vim.<br></p>',
 'ketentuan-pengiriman', '', '', 2, 0, '2017-11-08 11:04:05', 1, '2017-11-08 11:32:07', 1),
(3, 'Kebijakan Privasi',
 '<p align="justify">Sit an solum sonet erroribus. Et quando apeirian pertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua sit, ut vel ipsum graeci. At cum convenire similique, posse ubique insolens id sea, facer graece pro ea. In summo ullamcorper cum, ut oblique recusabo concludaturque per, sed soluta prompta percipitur ex. Ipsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam detraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae duo no.<br><br>Purto tation ea est, an utinam sententiae per. Eos eu fuisset cotidieque, meis numquam commune per an. Quo lorem atqui possit cu, et latine labores mea. Et vix aeterno facilis, ei natum omnes timeam pro. Quas malorum laboramus qui no, quas integre impedit ne duo. Eu ius graeco petentium, tollit invenire temporibus pri an, commune officiis intellegam ut eos. Mea quis suavitate at, offendit reformidans no has. Ne cum discere molestiae neglegentur. No has ubique vocent, id delicata recteque accommodare eam.<br><br>Vim ne admodum ancillae laboramus, audiam omnesque molestie duo in. An vis eleifend voluptatibus. Graecis ocurreret accusamus eos ad, recteque pericula comprehensam id mea, ad quot mnesarchum voluptatum quo. Mei amet oblique reprehendunt cu. At decore sanctus periculis pri. Soleat adolescens vix at. Persecuti voluptaria ad qui, vel id habemus vulputate delicatissimi, ius veniam meliore recteque ne. Ad soleat tempor prodesset nec, in scaevola tacimates pertinacia mel. Deserunt theophrastus usu an, nam vitae adolescens eu. Congue aperiri copiosae est ut, mea errem eloquentiam cu.<br><br>At impedit principes mea. Pro verear animal percipitur ut, id sonet mollis delicata vix. Pri in quis everti consequuntur, no utinam deseruisse eos. Sed agam movet semper in, ex elit tota aeque vel. Te quot quando nam. Legendos gubergren id cum. Purto duis conceptam ius ei. Pri at paulo patrioque consetetur, nec atqui doming eu. Est eu forensibus concludaturque, an mei vidit essent vituperatoribus. Usu no tota elitr, scripta eleifend dissentias ei has. Assum sonet dignissim ex qui, deleniti urbanitas qui no. Cum probo legere inimicus et, brute noster eum ut. Vix nihil electram at.<br><br>Vide mediocrem voluptatibus ne sit, inani eligendi facilisi nec an, cu eam simul primis definitiones. Ius cu diam clita, et lucilius omittantur mel. Omnes feugiat vivendum ut has, in qui ipsum latine apeirian. Ad fabellas explicari forensibus vis, eu usu nulla dolorum. Id doming consetetur posidonium nam, quas dictas ius ad, at vel nusquam delectus disputando. Possim sanctus electram et nec, eruditi oportere usu an. Cu pro invidunt appellantur, error repudiare cum ne. Eligendi electram et quo.<br><br>Erat putant in sed, ei quot graeci cum, an mundi labores accommodare nec. Pro at aeque accommodare, iusto aperiam has et. Dolor voluptatibus cum ad, his justo primis accusata cu, ne putant accusamus mel. Per partem vituperata at, at meliore similique mea. Ei erat constituam eam, nam et diceret dolorem principes, pro eu illum ignota detraxit. Novum possit volutpat usu no, vis fierent eleifend an, ea exerci argumentum vim.<br></p>',
 'kebijakan-privasi', '', '', 3, 0, '2017-11-08 11:04:50', 1, '2017-11-08 11:15:55', 1),
(4, 'Syarat & Ketentuan',
 '<p align="justify">Sit an solum sonet erroribus. Et quando apeirian pertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua sit, ut vel ipsum graeci. At cum convenire similique, posse ubique insolens id sea, facer graece pro ea. In summo ullamcorper cum, ut oblique recusabo concludaturque per, sed soluta prompta percipitur ex. Ipsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam detraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae duo no.<br><br>Purto tation ea est, an utinam sententiae per. Eos eu fuisset cotidieque, meis numquam commune per an. Quo lorem atqui possit cu, et latine labores mea. Et vix aeterno facilis, ei natum omnes timeam pro. Quas malorum laboramus qui no, quas integre impedit ne duo. Eu ius graeco petentium, tollit invenire temporibus pri an, commune officiis intellegam ut eos. Mea quis suavitate at, offendit reformidans no has. Ne cum discere molestiae neglegentur. No has ubique vocent, id delicata recteque accommodare eam.<br><br>Vim ne admodum ancillae laboramus, audiam omnesque molestie duo in. An vis eleifend voluptatibus. Graecis ocurreret accusamus eos ad, recteque pericula comprehensam id mea, ad quot mnesarchum voluptatum quo. Mei amet oblique reprehendunt cu. At decore sanctus periculis pri. Soleat adolescens vix at. Persecuti voluptaria ad qui, vel id habemus vulputate delicatissimi, ius veniam meliore recteque ne. Ad soleat tempor prodesset nec, in scaevola tacimates pertinacia mel. Deserunt theophrastus usu an, nam vitae adolescens eu. Congue aperiri copiosae est ut, mea errem eloquentiam cu.<br><br>At impedit principes mea. Pro verear animal percipitur ut, id sonet mollis delicata vix. Pri in quis everti consequuntur, no utinam deseruisse eos. Sed agam movet semper in, ex elit tota aeque vel. Te quot quando nam. Legendos gubergren id cum. Purto duis conceptam ius ei. Pri at paulo patrioque consetetur, nec atqui doming eu. Est eu forensibus concludaturque, an mei vidit essent vituperatoribus. Usu no tota elitr, scripta eleifend dissentias ei has. Assum sonet dignissim ex qui, deleniti urbanitas qui no. Cum probo legere inimicus et, brute noster eum ut. Vix nihil electram at.<br><br>Vide mediocrem voluptatibus ne sit, inani eligendi facilisi nec an, cu eam simul primis definitiones. Ius cu diam clita, et lucilius omittantur mel. Omnes feugiat vivendum ut has, in qui ipsum latine apeirian. Ad fabellas explicari forensibus vis, eu usu nulla dolorum. Id doming consetetur posidonium nam, quas dictas ius ad, at vel nusquam delectus disputando. Possim sanctus electram et nec, eruditi oportere usu an. Cu pro invidunt appellantur, error repudiare cum ne. Eligendi electram et quo.<br><br>Erat putant in sed, ei quot graeci cum, an mundi labores accommodare nec. Pro at aeque accommodare, iusto aperiam has et. Dolor voluptatibus cum ad, his justo primis accusata cu, ne putant accusamus mel. Per partem vituperata at, at meliore similique mea. Ei erat constituam eam, nam et diceret dolorem principes, pro eu illum ignota detraxit. Novum possit volutpat usu no, vis fierent eleifend an, ea exerci argumentum vim.<br></p>',
 'syarat-dan-ketentuan', '', '', 4, 0, '2017-11-08 11:05:13', 1, '2017-11-08 11:15:48', 1),
(5, 'Cara Belanja',
 '<p align="justify">Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br><br>Purto tation ea est, an utinam sententiae per. Eos eu \r\nfuisset cotidieque, meis numquam commune per an. Quo lorem atqui possit \r\ncu, et latine labores mea. Et vix aeterno facilis, ei natum omnes timeam\r\n pro. Quas malorum laboramus qui no, quas integre impedit ne duo. Eu ius\r\n graeco petentium, tollit invenire temporibus pri an, commune officiis \r\nintellegam ut eos. Mea quis suavitate at, offendit reformidans no has. \r\nNe cum discere molestiae neglegentur. No has ubique vocent, id delicata \r\nrecteque accommodare eam.<br><br>Vim ne admodum ancillae laboramus, \r\naudiam omnesque molestie duo in. An vis eleifend voluptatibus. Graecis \r\nocurreret accusamus eos ad, recteque pericula comprehensam id mea, ad \r\nquot mnesarchum voluptatum quo. Mei amet oblique reprehendunt cu. At \r\ndecore sanctus periculis pri. Soleat adolescens vix at. Persecuti \r\nvoluptaria ad qui, vel id habemus vulputate delicatissimi, ius veniam \r\nmeliore recteque ne. Ad soleat tempor prodesset nec, in scaevola \r\ntacimates pertinacia mel. Deserunt theophrastus usu an, nam vitae \r\nadolescens eu. Congue aperiri copiosae est ut, mea errem eloquentiam cu.<br><br>At\r\n impedit principes mea. Pro verear animal percipitur ut, id sonet mollis\r\n delicata vix. Pri in quis everti consequuntur, no utinam deseruisse \r\neos. Sed agam movet semper in, ex elit tota aeque vel. Te quot quando \r\nnam. Legendos gubergren id cum. Purto duis conceptam ius ei. Pri at \r\npaulo patrioque consetetur, nec atqui doming eu. Est eu forensibus \r\nconcludaturque, an mei vidit essent vituperatoribus. Usu no tota elitr, \r\nscripta eleifend dissentias ei has. Assum sonet dignissim ex qui, \r\ndeleniti urbanitas qui no. Cum probo legere inimicus et, brute noster \r\neum ut. Vix nihil electram at.<br><br>Vide mediocrem voluptatibus ne \r\nsit, inani eligendi facilisi nec an, cu eam simul primis definitiones. \r\nIus cu diam clita, et lucilius omittantur mel. Omnes feugiat vivendum ut\r\n has, in qui ipsum latine apeirian. Ad fabellas explicari forensibus \r\nvis, eu usu nulla dolorum. Id doming consetetur posidonium nam, quas \r\ndictas ius ad, at vel nusquam delectus disputando. Possim sanctus \r\nelectram et nec, eruditi oportere usu an. Cu pro invidunt appellantur, \r\nerror repudiare cum ne. Eligendi electram et quo.<br><br>Erat putant in \r\nsed, ei quot graeci cum, an mundi labores accommodare nec. Pro at aeque \r\naccommodare, iusto aperiam has et. Dolor voluptatibus cum ad, his justo \r\nprimis accusata cu, ne putant accusamus mel. Per partem vituperata at, \r\nat meliore similique mea. Ei erat constituam eam, nam et diceret dolorem\r\n principes, pro eu illum ignota detraxit. Novum possit volutpat usu no, \r\nvis fierent eleifend an, ea exerci argumentum vim.<br></p><p><br></p>',
 'cara-belanja', '', '', 1, 0, '2017-11-08 11:31:49', 2, '2017-11-18 14:57:03', 1),
(6, 'Cara Pembayaran',
 '<p align="justify">Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br><br>Purto tation ea est, an utinam sententiae per. Eos eu \r\nfuisset cotidieque, meis numquam commune per an. Quo lorem atqui possit \r\ncu, et latine labores mea. Et vix aeterno facilis, ei natum omnes timeam\r\n pro. Quas malorum laboramus qui no, quas integre impedit ne duo. Eu ius\r\n graeco petentium, tollit invenire temporibus pri an, commune officiis \r\nintellegam ut eos. Mea quis suavitate at, offendit reformidans no has. \r\nNe cum discere molestiae neglegentur. No has ubique vocent, id delicata \r\nrecteque accommodare eam.<br><br>Vim ne admodum ancillae laboramus, \r\naudiam omnesque molestie duo in. An vis eleifend voluptatibus. Graecis \r\nocurreret accusamus eos ad, recteque pericula comprehensam id mea, ad \r\nquot mnesarchum voluptatum quo. Mei amet oblique reprehendunt cu. At \r\ndecore sanctus periculis pri. Soleat adolescens vix at. Persecuti \r\nvoluptaria ad qui, vel id habemus vulputate delicatissimi, ius veniam \r\nmeliore recteque ne. Ad soleat tempor prodesset nec, in scaevola \r\ntacimates pertinacia mel. Deserunt theophrastus usu an, nam vitae \r\nadolescens eu. Congue aperiri copiosae est ut, mea errem eloquentiam cu.<br><br>At\r\n impedit principes mea. Pro verear animal percipitur ut, id sonet mollis\r\n delicata vix. Pri in quis everti consequuntur, no utinam deseruisse \r\neos. Sed agam movet semper in, ex elit tota aeque vel. Te quot quando \r\nnam. Legendos gubergren id cum. Purto duis conceptam ius ei. Pri at \r\npaulo patrioque consetetur, nec atqui doming eu. Est eu forensibus \r\nconcludaturque, an mei vidit essent vituperatoribus. Usu no tota elitr, \r\nscripta eleifend dissentias ei has. Assum sonet dignissim ex qui, \r\ndeleniti urbanitas qui no. Cum probo legere inimicus et, brute noster \r\neum ut. Vix nihil electram at.<br><br>Vide mediocrem voluptatibus ne \r\nsit, inani eligendi facilisi nec an, cu eam simul primis definitiones. \r\nIus cu diam clita, et lucilius omittantur mel. Omnes feugiat vivendum ut\r\n has, in qui ipsum latine apeirian. Ad fabellas explicari forensibus \r\nvis, eu usu nulla dolorum. Id doming consetetur posidonium nam, quas \r\ndictas ius ad, at vel nusquam delectus disputando. Possim sanctus \r\nelectram et nec, eruditi oportere usu an. Cu pro invidunt appellantur, \r\nerror repudiare cum ne. Eligendi electram et quo.<br><br>Erat putant in \r\nsed, ei quot graeci cum, an mundi labores accommodare nec. Pro at aeque \r\naccommodare, iusto aperiam has et. Dolor voluptatibus cum ad, his justo \r\nprimis accusata cu, ne putant accusamus mel. Per partem vituperata at, \r\nat meliore similique mea. Ei erat constituam eam, nam et diceret dolorem\r\n principes, pro eu illum ignota detraxit. Novum possit volutpat usu no, \r\nvis fierent eleifend an, ea exerci argumentum vim.<br></p><p><br></p>',
 'cara-pembayaran', '', '', 0, 0, '2017-11-08 11:33:39', 2, '2017-11-18 14:57:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment`
(
  `code` varchar
(
  32
) NOT NULL DEFAULT '',
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `description` text,
  `version` varchar
(
  32
) NOT NULL DEFAULT '',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `code`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`code`, `name`, `description`, `version`, `active`)
VALUES
  ('midtrans', 'Midtrans', 'Metode pembayaran Midtrans SNAP', '1.1.0', 1),
  ('mandiri', 'Transfer Mandiri', 'Metode pembayaran transfer bank Mandiri', '1.1.0', 1),
  ('bca', 'Transfer BCA', 'Metode pembayaran transfer bank BCA', '1.1.0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product`
(
  `product_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  255
) NOT NULL DEFAULT '',
  `model` varchar
(
  128
) NOT NULL DEFAULT '',
  `sku` varchar
(
  64
) NOT NULL DEFAULT '',
  `description` text,
  `meta_description` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_keyword` varchar
(
  255
) NOT NULL DEFAULT '',
  `meta_title` varchar
(
  255
) NOT NULL DEFAULT '',
  `slug` varchar
(
  255
) NOT NULL DEFAULT '',
  `tag` text,
  `image` varchar
(
  255
) DEFAULT NULL,
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `category_id` int
(
  11
) NOT NULL DEFAULT '0',
  `manufacturer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `points` int
(
  11
) NOT NULL DEFAULT '0',
  `weight` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `weight_class_id` int
(
  11
) NOT NULL DEFAULT '0',
  `length` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `width` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `height` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `length_class_id` int
(
  11
) NOT NULL DEFAULT '0',
  `quantity` int
(
  11
) NOT NULL DEFAULT '0',
  `unit_class_id` int
(
  11
) NOT NULL DEFAULT '0',
  `stock_status_id` int
(
  11
) NOT NULL DEFAULT '0',
  `minimum` int
(
  11
) NOT NULL DEFAULT '1',
  `shipping` tinyint
(
  1
) NOT NULL DEFAULT '1',
  `sort_order` int
(
  11
) NOT NULL DEFAULT '0',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int
(
  5
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `product_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=135;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `name`, `model`, `sku`, `description`, `meta_description`, `meta_keyword`,
                       `meta_title`, `slug`, `tag`, `image`, `price`, `category_id`, `manufacturer_id`, `points`,
                       `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `quantity`,
                       `unit_class_id`, `stock_status_id`, `minimum`, `shipping`, `sort_order`, `active`, `date_added`,
                       `date_modified`, `viewed`)
VALUES
(63, 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace', 'D945', '123456789',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7, d12345, 24ghz, unboxed, garansi, 2, tahun, replace',
 'Prosesor Intel  Core i7 D12345 2.4Ghz Unboxed Garansi 2 Tahun Replace',
 'prosesor-intel-core-i7-d12345-24ghz-unboxed-garansi-2-tahun-replace', NULL, 'data/products/CP550IN_96142.jpg',
 '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 2, 0, 1, 1, 1, 0, 1, '2017-11-04 11:51:31',
 '2017-11-21 18:01:14', 175),
(64, 'Motherboard Asus ABC', 'M5-A78l', '12345678',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus, abc', 'Motherboard Asus ABC', 'motherboard-asus-abc', NULL,
 'data/products/874193_BB_00_FB_EPS_1000.jpg', '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 95, 0, 1, 1,
 1, 0, 1, '2017-11-04 13:53:20', '2017-11-20 16:36:43', 113),
(112, 'Prosesor Intel  Core i7', 'D945', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7', 'Prosesor Intel  Core i7', 'prosesor-intel-core-i7-5', NULL,
 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 12, 0, 1, 1, 1, 0,
 1, '2017-11-04 11:51:31', '2017-11-17 19:22:57', 2),
(111, 'Motherboard Asus', 'M5-A78l', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus', 'Motherboard Asus', 'motherboard-asus-5', NULL, 'data/products/874193_BB_00_FB_EPS_1000.jpg',
 '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 0, 0, 1, 1, 1, 0, 1, '2017-11-04 13:53:20',
 '2017-11-17 19:22:57', 14),
(110, 'Motherboard Asus', 'M5-A78l', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus', 'Motherboard Asus', 'motherboard-asus-4', NULL, 'data/products/874193_BB_00_FB_EPS_1000.jpg',
 '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 0, 0, 1, 1, 1, 0, 1, '2017-11-04 13:53:20',
 '2017-11-17 19:22:57', 9),
(105, 'Prosesor Intel  Core i7', 'D945', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7', 'Prosesor Intel  Core i7', 'prosesor-intel-core-i7-2', NULL,
 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 12, 0, 1, 1, 1, 0,
 1, '2017-11-04 11:51:31', '2017-11-17 19:22:57', 5),
(106, 'Motherboard Asus', 'M5-A78l', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus', 'Motherboard Asus', 'motherboard-asus-2', NULL, 'data/products/874193_BB_00_FB_EPS_1000.jpg',
 '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 0, 0, 1, 1, 1, 0, 1, '2017-11-04 13:53:20',
 '2017-11-17 19:22:57', 0),
(107, 'Motherboard Asus', 'M5-A78l', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus', 'Motherboard Asus', 'motherboard-asus-3', NULL, 'data/products/874193_BB_00_FB_EPS_1000.jpg',
 '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 0, 0, 1, 1, 1, 0, 1, '2017-11-04 13:53:20',
 '2017-11-17 19:22:57', 1),
(108, 'Prosesor Intel  Core i7', 'D945', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7', 'Prosesor Intel  Core i7', 'prosesor-intel-core-i7-3', NULL,
 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 12, 0, 1, 1, 1, 0,
 1, '2017-11-04 11:51:31', '2017-11-17 19:22:57', 4),
(109, 'Prosesor Intel  Core i7', 'D945', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7', 'Prosesor Intel  Core i7', 'prosesor-intel-core-i7-4', NULL,
 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 12, 0, 1, 1, 1, 0,
 1, '2017-11-04 11:51:31', '2017-11-17 19:22:57', 0),
(113, 'Prosesor Intel  Core i7', 'D945', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7', 'Prosesor Intel  Core i7', 'prosesor-intel-core-i7-6', NULL,
 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 12, 0, 1, 1, 1, 0,
 1, '2017-11-04 11:51:31', '2017-11-17 19:22:57', 1),
(100, 'HDD 500GB 5400', 'D945', '212312445',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'hdd, 500gb, 5400', 'HDD 500GB 5400', 'hdd-500gb-5400', NULL, 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0,
 '1200.00', 3, '0.00', '0.00', '0.00', 1, 5, 0, 1, 1, 1, 0, 1, '2017-11-04 11:51:31', '2017-11-18 17:12:06', 9),
(99, 'RAM 1 GB Visipro DDR3', 'M5-A78l', '122324235',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'ram, 1, gb, visipro, ddr3', 'RAM 1 GB Visipro DDR3', 'ram-1-gb-visipro-ddr3', NULL,
 'data/products/874193_BB_00_FB_EPS_1000.jpg', '850000.00', 2, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 195, 0, 1, 1,
 1, 0, 1, '2017-11-04 13:53:20', '2017-11-18 17:27:19', 1),
(114, 'Motherboard Asus', 'M5-A78l', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus', 'Motherboard Asus', 'motherboard-asus-6', NULL, 'data/products/874193_BB_00_FB_EPS_1000.jpg',
 '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 0, 0, 1, 1, 1, 0, 1, '2017-11-04 13:53:20',
 '2017-11-17 19:22:57', 1),
(115, 'Motherboard Asus', 'M5-A78l', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus', 'Motherboard Asus', 'motherboard-asus-7', NULL, 'data/products/874193_BB_00_FB_EPS_1000.jpg',
 '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 0, 0, 1, 1, 1, 0, 1, '2017-11-04 13:53:20',
 '2017-11-17 19:22:26', 7),
(116, 'Prosesor Intel  Core i7', 'D945', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7', 'Prosesor Intel  Core i7', 'prosesor-intel-core-i7-7', NULL,
 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 12, 0, 1, 1, 1, 0,
 1, '2017-11-04 11:51:31', '2017-11-17 19:22:26', 0),
(117, 'Prosesor Intel  Core i7', 'D945', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7', 'Prosesor Intel  Core i7', 'prosesor-intel-core-i7-8', NULL,
 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 12, 0, 1, 1, 1, 0,
 1, '2017-11-04 11:51:31', '2017-11-17 19:22:26', 2),
(118, 'Motherboard Asus', 'M5-A78l', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus', 'Motherboard Asus', 'motherboard-asus-8', NULL, 'data/products/874193_BB_00_FB_EPS_1000.jpg',
 '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 0, 0, 1, 1, 1, 0, 1, '2017-11-04 13:53:20',
 '2017-11-17 19:22:26', 1),
(119, 'Motherboard Asus', 'M5-A78l', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus', 'Motherboard Asus', 'motherboard-asus-9', NULL, 'data/products/874193_BB_00_FB_EPS_1000.jpg',
 '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 0, 0, 1, 1, 1, 0, 1, '2017-11-04 13:53:20',
 '2017-11-17 19:22:26', 2),
(120, 'Prosesor Intel  Core i7', 'D945', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7', 'Prosesor Intel  Core i7', 'prosesor-intel-core-i7-9', NULL,
 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 12, 0, 1, 1, 1, 0,
 1, '2017-11-04 11:51:31', '2017-11-17 19:22:26', 0),
(121, 'Prosesor Intel  Core i7', 'D945', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7', 'Prosesor Intel  Core i7', 'prosesor-intel-core-i7-10', NULL,
 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 12, 0, 1, 1, 1, 0,
 1, '2017-11-04 11:51:31', '2017-11-17 19:22:26', 4),
(122, 'Motherboard Asus', 'M5-A78l', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus', 'Motherboard Asus', 'motherboard-asus-10', NULL, 'data/products/874193_BB_00_FB_EPS_1000.jpg',
 '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 0, 0, 1, 1, 1, 0, 1, '2017-11-04 13:53:20',
 '2017-11-17 19:22:26', 0),
(123, 'Motherboard Asus', 'M5-A78l', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'motherboard, asus', 'Motherboard Asus', 'motherboard-asus-11', NULL, 'data/products/874193_BB_00_FB_EPS_1000.jpg',
 '850000.00', 3, 0, 0, '0.00', 3, '0.00', '0.00', '0.00', 1, 0, 0, 1, 1, 1, 0, 1, '2017-11-04 13:53:20',
 '2017-11-17 19:22:26', 0),
(124, 'Prosesor Intel  Core i7', 'D945', '',
 '<p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.</p><p>Sit an solum sonet erroribus. Et quando apeirian \r\npertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua \r\nsit, ut vel ipsum graeci. At cum convenire similique, posse ubique \r\ninsolens id sea, facer graece pro ea. In summo ullamcorper cum, ut \r\noblique recusabo concludaturque per, sed soluta prompta percipitur ex. \r\nIpsum mucius omnesque his ut, ex eum falli nobis oportere. Diam utinam \r\ndetraxit eum te, impedit consectetuer reprehendunt ius cu, movet causae \r\nduo no.<br></p><p><br></p>',
 'Sit an solum sonet erroribus Et quando apeirian pertinax mea et natum iudico populo nam No nostrum eligendi perpetua sit ut vel ipsum graeci At cum convenire si',
 'prosesor, intel, core, i7', 'Prosesor Intel  Core i7', 'prosesor-intel-core-i7-11', NULL,
 'data/products/CP550IN_96142.jpg', '1400000.00', 2, 0, 0, '1200.00', 3, '0.00', '0.00', '0.00', 1, 12, 0, 1, 1, 1, 0,
 1, '2017-11-04 11:51:31', '2017-11-17 19:22:26', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute`
--

CREATE TABLE IF NOT EXISTS `product_attribute`
(
  `product_id` int
(
  11
) NOT NULL,
  `attribute_id` int
(
  11
) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY
(
  `product_id`,
  `attribute_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_attribute`
--

INSERT INTO `product_attribute` (`product_id`, `attribute_id`, `text`)
VALUES
  (63, 12, '4 GB DDR3'),
  (63, 11, '500 GB'),
  (63, 10, 'Intel Core i7'),
  (64, 12, '2GB DDR3'),
  (64, 10, 'Intel Core i3'),
  (64, 11, '250 GB'),
  (99, 11, '250 GB'),
  (100, 11, '500 GB'),
  (100, 10, 'Intel Core i7'),
  (99, 12, '2GB DDR3'),
  (100, 12, '4 GB DDR3'),
  (107, 10, 'Intel Core i3'),
  (106, 12, '2GB DDR3'),
  (106, 11, '250 GB'),
  (106, 10, 'Intel Core i3'),
  (105, 12, '4 GB DDR3'),
  (105, 11, '500 GB'),
  (105, 10, 'Intel Core i7'),
  (99, 10, 'Intel Core i3'),
  (107, 11, '250 GB'),
  (107, 12, '2GB DDR3'),
  (108, 10, 'Intel Core i7'),
  (108, 11, '500 GB'),
  (108, 12, '4 GB DDR3'),
  (109, 10, 'Intel Core i7'),
  (109, 11, '500 GB'),
  (109, 12, '4 GB DDR3'),
  (110, 10, 'Intel Core i3'),
  (110, 11, '250 GB'),
  (110, 12, '2GB DDR3'),
  (111, 10, 'Intel Core i3'),
  (111, 11, '250 GB'),
  (111, 12, '2GB DDR3'),
  (112, 10, 'Intel Core i7'),
  (112, 11, '500 GB'),
  (112, 12, '4 GB DDR3'),
  (113, 10, 'Intel Core i7'),
  (113, 11, '500 GB'),
  (113, 12, '4 GB DDR3'),
  (114, 10, 'Intel Core i3'),
  (114, 11, '250 GB'),
  (114, 12, '2GB DDR3'),
  (115, 10, 'Intel Core i3'),
  (115, 11, '250 GB'),
  (115, 12, '2GB DDR3'),
  (116, 10, 'Intel Core i7'),
  (116, 11, '500 GB'),
  (116, 12, '4 GB DDR3'),
  (117, 10, 'Intel Core i7'),
  (117, 11, '500 GB'),
  (117, 12, '4 GB DDR3'),
  (118, 10, 'Intel Core i3'),
  (118, 11, '250 GB'),
  (118, 12, '2GB DDR3'),
  (119, 10, 'Intel Core i3'),
  (119, 11, '250 GB'),
  (119, 12, '2GB DDR3'),
  (120, 10, 'Intel Core i7'),
  (120, 11, '500 GB'),
  (120, 12, '4 GB DDR3'),
  (121, 10, 'Intel Core i7'),
  (121, 11, '500 GB'),
  (121, 12, '4 GB DDR3'),
  (122, 10, 'Intel Core i3'),
  (122, 11, '250 GB'),
  (122, 12, '2GB DDR3'),
  (123, 10, 'Intel Core i3'),
  (123, 11, '250 GB'),
  (123, 12, '2GB DDR3'),
  (124, 10, 'Intel Core i7'),
  (124, 11, '500 GB'),
  (124, 12, '4 GB DDR3');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category`
(
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `category_id` int
(
  11
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `product_id`,
  `category_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_discount`
--

CREATE TABLE IF NOT EXISTS `product_discount`
(
  `product_discount_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_group_id` int
(
  11
) NOT NULL DEFAULT '0',
  `quantity` int
(
  4
) NOT NULL DEFAULT '0',
  `priority` int
(
  5
) NOT NULL DEFAULT '1',
  `price` decimal
(
  15,
  2
) NOT NULL DEFAULT '0.00',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY
(
  `product_discount_id`
),
  KEY `product_id`
(
  `product_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=157;

--
-- Dumping data for table `product_discount`
--

INSERT INTO `product_discount` (`product_discount_id`, `product_id`, `customer_group_id`, `quantity`, `priority`,
                                `price`, `date_start`, `date_end`)
VALUES
  (156, 63, 0, 1, 1, '850000.00', '2017-11-16', '2017-11-30'),
  (132, 108, 0, 1, 1, '850000.00', '2017-11-16', '2017-11-30'),
  (131, 107, 0, 1, 1, '800000.00', '2017-11-17', '2017-11-25'),
  (153, 64, 0, 1, 1, '800000.00', '2017-11-17', '2017-11-25'),
  (133, 109, 0, 1, 1, '850000.00', '2017-11-16', '2017-11-30'),
  (134, 110, 0, 1, 1, '800000.00', '2017-11-17', '2017-11-25'),
  (135, 115, 0, 1, 1, '800000.00', '2017-11-17', '2017-11-25'),
  (136, 116, 0, 1, 1, '850000.00', '2017-11-16', '2017-11-30'),
  (137, 117, 0, 1, 1, '850000.00', '2017-11-16', '2017-11-30'),
  (138, 118, 0, 1, 1, '800000.00', '2017-11-17', '2017-11-25'),
  (139, 123, 0, 1, 1, '800000.00', '2017-11-17', '2017-11-25'),
  (140, 124, 0, 1, 1, '850000.00', '2017-11-16', '2017-11-30'),
  (155, 64, 0, 10, 2, '500000.00', '0000-00-00', '0000-00-00'),
  (154, 64, 0, 5, 2, '750000.00', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `product_filter`
--

CREATE TABLE IF NOT EXISTS `product_filter`
(
  `product_id` int
(
  11
) NOT NULL,
  `filter_id` int
(
  11
) NOT NULL,
  PRIMARY KEY
(
  `product_id`,
  `filter_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_filter`
--

INSERT INTO `product_filter` (`product_id`, `filter_id`)
VALUES
  (63, 37),
  (63, 46),
  (64, 40),
  (64, 42);

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE IF NOT EXISTS `product_image`
(
  `product_image_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `image` varchar
(
  255
) NOT NULL DEFAULT '',
  `alt` varchar
(
  64
) NOT NULL DEFAULT '',
  `title` varchar
(
  64
) NOT NULL DEFAULT '',
  `sort_order` int
(
  3
) NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `product_image_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=322;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`product_image_id`, `product_id`, `image`, `alt`, `title`, `sort_order`)
VALUES
  (321, 63, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 2),
  (320, 63, 'data/products/CP550IN_96142.jpg', '', '', 1),
  (278, 105, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 0),
  (279, 105, 'data/products/CP550IN_96142.jpg', '', '', 0),
  (319, 100, 'data/products/CP550IN_96142.jpg', '', '', 2),
  (318, 100, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 1),
  (280, 108, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 0),
  (281, 108, 'data/products/CP550IN_96142.jpg', '', '', 0),
  (282, 109, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 1),
  (283, 109, 'data/products/CP550IN_96142.jpg', '', '', 2),
  (284, 112, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 1),
  (285, 112, 'data/products/CP550IN_96142.jpg', '', '', 2),
  (286, 113, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 0),
  (287, 113, 'data/products/CP550IN_96142.jpg', '', '', 0),
  (288, 116, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 0),
  (289, 116, 'data/products/CP550IN_96142.jpg', '', '', 0),
  (290, 117, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 1),
  (291, 117, 'data/products/CP550IN_96142.jpg', '', '', 2),
  (292, 120, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 1),
  (293, 120, 'data/products/CP550IN_96142.jpg', '', '', 2),
  (294, 121, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 0),
  (295, 121, 'data/products/CP550IN_96142.jpg', '', '', 0),
  (296, 124, 'data/products/874193_BB_00_FB_EPS_1000.jpg', '', '', 0),
  (297, 124, 'data/products/CP550IN_96142.jpg', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_option`
--

CREATE TABLE IF NOT EXISTS `product_option`
(
  `product_option_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_id` int
(
  11
) NOT NULL,
  `option_id` int
(
  11
) NOT NULL,
  `option_value` text NOT NULL,
  `required` tinyint
(
  1
) NOT NULL,
  PRIMARY KEY
(
  `product_option_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=55;

--
-- Dumping data for table `product_option`
--

INSERT INTO `product_option` (`product_option_id`, `product_id`, `option_id`, `option_value`, `required`)
VALUES
  (54, 63, 5, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_option_value`
--

CREATE TABLE IF NOT EXISTS `product_option_value`
(
  `product_option_value_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_option_id` int
(
  11
) NOT NULL,
  `product_id` int
(
  11
) NOT NULL,
  `option_id` int
(
  11
) NOT NULL,
  `option_value_id` int
(
  11
) NOT NULL,
  `quantity` int
(
  3
) NOT NULL,
  `subtract` tinyint
(
  1
) NOT NULL,
  `price` decimal
(
  15,
  2
) NOT NULL,
  `points` int
(
  8
) NOT NULL,
  `weight` decimal
(
  15,
  2
) NOT NULL,
  PRIMARY KEY
(
  `product_option_value_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=126;

--
-- Dumping data for table `product_option_value`
--

INSERT INTO `product_option_value` (`product_option_value_id`, `product_option_id`, `product_id`, `option_id`,
                                    `option_value_id`, `quantity`, `subtract`, `price`, `points`, `weight`)
VALUES
  (125, 54, 63, 5, 6, 9, 1, '0.00', 0, '0.00'),
  (124, 54, 63, 5, 7, 20, 1, '0.00', 0, '0.00'),
  (123, 54, 63, 5, 8, 29, 1, '0.00', 0, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `product_related`
--

CREATE TABLE IF NOT EXISTS `product_related`
(
  `product_id` int
(
  11
) NOT NULL,
  `related_id` int
(
  11
) NOT NULL,
  `type` varchar
(
  9
) NOT NULL DEFAULT '',
  PRIMARY KEY
(
  `product_id`,
  `related_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_related`
--

INSERT INTO `product_related` (`product_id`, `related_id`, `type`)
VALUES
  (63, 64, ''),
  (63, 99, ''),
  (63, 124, '');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review`
(
  `review_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `product_id` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `author` varchar
(
  128
) NOT NULL DEFAULT '',
  `text` text,
  `rating` int
(
  1
) NOT NULL DEFAULT '0',
  `active` tinyint
(
  1
) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY
(
  `review_id`
),
  KEY `product_id`
(
  `product_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=9;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`review_id`, `product_id`, `customer_id`, `author`, `text`, `rating`, `active`, `date_added`)
VALUES
(6, 63, 2, '2 - Adi Setia',
 'Test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan test ulasan',
 3, 1, '2017-11-20 19:32:34'),
(5, 63, 2, '2 - Adi Setia', 'Sangat bagus nih produk', 4, 1, '2017-11-20 16:55:03'),
(7, 63, 2, 'Adi Setia', 'Mantap barang sudah diterima dalam keadaan baik', 5, 1, '2017-11-20 20:13:43'),
(8, 63, 2, 'Adi Setia', 'Lumayan cepet pengirimannya', 3, 0, '2017-11-20 20:30:06');

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE IF NOT EXISTS `search`
(
  `search_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `search` varchar
(
  255
) COLLATE utf8_bin NOT NULL DEFAULT '',
  `phase` int
(
  1
) NOT NULL DEFAULT '0',
  `results` int
(
  11
) NOT NULL DEFAULT '0',
  `customer_id` int
(
  11
) NOT NULL DEFAULT '0',
  `ip` varchar
(
  15
) COLLATE utf8_bin NOT NULL DEFAULT '0',
  PRIMARY KEY
(
  `search_id`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE =utf8_bin AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session`
(
  `id` varchar
(
  128
) NOT NULL,
  `ip_address` varchar
(
  45
) NOT NULL,
  `timestamp` int
(
  10
) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp`
(
  `timestamp`
)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `ip_address`, `timestamp`, `data`)
VALUES
('eu4bq24pqnjtdeamcpqcnnn8vhsaqe2u', '127.0.0.1', 1511268971,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313236383639333b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d),
('bs1lbrkt0vv7lvpt2hmtah1p2m93clh0', '127.0.0.1', 1511269245,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313236383939353b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d),
('78uch6odiarjcmjjno5n7cigljii853g', '127.0.0.1', 1511269608,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313236393331313b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d),
('sosp3rh67le7r4qencr98ie0mje7mail', '127.0.0.1', 1511269970,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313236393731363b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d),
('96smmqekeoc2i0vp55gtqd67ke7vgl2h', '127.0.0.1', 1511270356,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237303036383b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d),
('m7ocql0b33vkkuj14ug9cjaqngj22mrf', '127.0.0.1', 1511270310,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237303330393b63757272656e63797c733a333a22494452223b61646d696e5f72656469726563747c733a32323a226f66666963652f6f66666572732f646973636f756e74223b61646d696e5f69647c733a323a222d31223b),
('k36aj9ojh54v2jb01hh0srcnge08mm84', '127.0.0.1', 1511270560,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237303337333b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d),
('suk00db654c3sj0o0tu1sbsah0ibk4fn', '127.0.0.1', 1511272847,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237323630373b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d),
('1e924u3c2rjgb20mpcn85j8ups34p76o', '127.0.0.1', 1511273276,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237323937383b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d),
('hv187g19bf0cvcj7782q0igj46gdk94s', '127.0.0.1', 1511273061,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237333034303b63757272656e63797c733a333a22494452223b61646d696e5f72656469726563747c733a32323a226f66666963652f6f66666572732f646973636f756e74223b61646d696e5f69647c733a323a222d31223b),
('e2639b204htefgn4hen8m7ugfg35a0hl', '127.0.0.1', 1511273557,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237333238303b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a303a7b7d733a353a226572726f72223b733a34393a22476167616c2073616174206d656e67616d62696c20696e666f726d617369206d65746f64652070656e676972696d616e2e223b7d7d),
('bjbu90enqpk76bvm4j5ch805k5o4d091', '127.0.0.1', 1511273809,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237333538333b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a303a7b7d733a353a226572726f72223b733a34393a22476167616c2073616174206d656e67616d62696c20696e666f726d617369206d65746f64652070656e676972696d616e2e223b7d7d),
('mf1pvaj8bmf10h5i49q8vmurh77pqadd', '127.0.0.1', 1511274202,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237333932353b63757272656e63797c733a333a22494452223b636172747c613a313a7b733a33313a2236333a59546f784f6e74704f6a557a4f334d364d7a6f694d544979496a7439223b693a313b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636f6d6d656e747c733a303a22223b7061796d656e745f6d6574686f64737c613a323a7b733a333a22626361223b613a333a7b733a343a22636f6465223b733a333a22626361223b733a353a227469746c65223b733a31323a225472616e7366657220424341223b733a31303a22736f72745f6f72646572223b693a313b7d733a373a226d616e64697269223b613a333a7b733a343a22636f6465223b733a373a226d616e64697269223b733a353a227469746c65223b733a31363a225472616e73666572204d616e64697269223b733a31303a22736f72745f6f72646572223b693a313b7d7d7061796d656e745f6d6574686f647c613a333a7b733a343a22636f6465223b733a333a22626361223b733a353a227469746c65223b733a31323a225472616e7366657220424341223b733a31303a22736f72745f6f72646572223b693a313b7d6f726465725f69647c693a32353b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a383a7b733a373a226a6e655f6f6b65223b613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f6f6b65223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d204f4b45223b733a343a22636f7374223b693a32353030303b733a343a2274657874223b733a393a2252702032352e303030223b7d733a373a226a6e655f726567223b613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f726567223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d20524547223b733a343a22636f7374223b693a32393030303b733a343a2274657874223b733a393a2252702032392e303030223b7d733a373a226a6e655f796573223b613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f796573223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d20594553223b733a343a22636f7374223b693a33333030303b733a343a2274657874223b733a393a2252702033332e303030223b7d733a32323a22706f735f70616b65745f6b696c61745f6b6875737573223b613a343a7b733a343a22636f6465223b733a33333a2272616a616f6e676b69722e706f735f70616b65745f6b696c61745f6b6875737573223b733a353a227469746c65223b733a34303a22504f5320496e646f6e657369612028504f5329202d2050616b6574204b696c6174204b6875737573223b733a343a22636f7374223b693a32393030303b733a343a2274657874223b733a393a2252702032392e303030223b7d733a32373a22706f735f657870726573735f6e6578745f6461795f626172616e67223b613a343a7b733a343a22636f6465223b733a33383a2272616a616f6e676b69722e706f735f657870726573735f6e6578745f6461795f626172616e67223b733a353a227469746c65223b733a34353a22504f5320496e646f6e657369612028504f5329202d2045787072657373204e6578742044617920426172616e67223b733a343a22636f7374223b693a33343530303b733a343a2274657874223b733a393a2252702033342e353030223b7d733a383a2274696b695f726567223b613a343a7b733a343a22636f6465223b733a31393a2272616a616f6e676b69722e74696b695f726567223b733a353a227469746c65223b733a33363a2243697472612056616e205469746970616e204b696c6174202854494b4929202d20524547223b733a343a22636f7374223b693a33343530303b733a343a2274657874223b733a393a2252702033342e353030223b7d733a383a2274696b695f65636f223b613a343a7b733a343a22636f6465223b733a31393a2272616a616f6e676b69722e74696b695f65636f223b733a353a227469746c65223b733a33363a2243697472612056616e205469746970616e204b696c6174202854494b4929202d2045434f223b733a343a22636f7374223b693a33333530303b733a343a2274657874223b733a393a2252702033332e353030223b7d733a383a2274696b695f6f6e73223b613a343a7b733a343a22636f6465223b733a31393a2272616a616f6e676b69722e74696b695f6f6e73223b733a353a227469746c65223b733a33363a2243697472612056616e205469746970616e204b696c6174202854494b4929202d204f4e53223b733a343a22636f7374223b693a33393030303b733a343a2274657874223b733a393a2252702033392e303030223b7d7d733a353a226572726f72223b623a303b7d7d7368697070696e675f6d6574686f647c613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f6f6b65223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d204f4b45223b733a343a22636f7374223b693a32353030303b733a343a2274657874223b733a393a2252702032352e303030223b7d),
('afogr3313e2cl2dtedlljejhaohov0l3', '127.0.0.1', 1511274562,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237343331353b63757272656e63797c733a333a22494452223b636172747c613a313a7b733a33313a2236333a59546f784f6e74704f6a557a4f334d364d7a6f694d544979496a7439223b693a313b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636f6d6d656e747c733a303a22223b7061796d656e745f6d6574686f64737c613a323a7b733a333a22626361223b613a333a7b733a343a22636f6465223b733a333a22626361223b733a353a227469746c65223b733a31323a225472616e7366657220424341223b733a31303a22736f72745f6f72646572223b693a313b7d733a373a226d616e64697269223b613a333a7b733a343a22636f6465223b733a373a226d616e64697269223b733a353a227469746c65223b733a31363a225472616e73666572204d616e64697269223b733a31303a22736f72745f6f72646572223b693a313b7d7d7061796d656e745f6d6574686f647c613a333a7b733a343a22636f6465223b733a333a22626361223b733a353a227469746c65223b733a31323a225472616e7366657220424341223b733a31303a22736f72745f6f72646572223b693a313b7d6f726465725f69647c693a32353b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a383a7b733a373a226a6e655f6f6b65223b613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f6f6b65223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d204f4b45223b733a343a22636f7374223b693a32353030303b733a343a2274657874223b733a393a2252702032352e303030223b7d733a373a226a6e655f726567223b613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f726567223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d20524547223b733a343a22636f7374223b693a32393030303b733a343a2274657874223b733a393a2252702032392e303030223b7d733a373a226a6e655f796573223b613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f796573223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d20594553223b733a343a22636f7374223b693a33333030303b733a343a2274657874223b733a393a2252702033332e303030223b7d733a32323a22706f735f70616b65745f6b696c61745f6b6875737573223b613a343a7b733a343a22636f6465223b733a33333a2272616a616f6e676b69722e706f735f70616b65745f6b696c61745f6b6875737573223b733a353a227469746c65223b733a34303a22504f5320496e646f6e657369612028504f5329202d2050616b6574204b696c6174204b6875737573223b733a343a22636f7374223b693a32393030303b733a343a2274657874223b733a393a2252702032392e303030223b7d733a32373a22706f735f657870726573735f6e6578745f6461795f626172616e67223b613a343a7b733a343a22636f6465223b733a33383a2272616a616f6e676b69722e706f735f657870726573735f6e6578745f6461795f626172616e67223b733a353a227469746c65223b733a34353a22504f5320496e646f6e657369612028504f5329202d2045787072657373204e6578742044617920426172616e67223b733a343a22636f7374223b693a33343530303b733a343a2274657874223b733a393a2252702033342e353030223b7d733a383a2274696b695f726567223b613a343a7b733a343a22636f6465223b733a31393a2272616a616f6e676b69722e74696b695f726567223b733a353a227469746c65223b733a33363a2243697472612056616e205469746970616e204b696c6174202854494b4929202d20524547223b733a343a22636f7374223b693a33343530303b733a343a2274657874223b733a393a2252702033342e353030223b7d733a383a2274696b695f65636f223b613a343a7b733a343a22636f6465223b733a31393a2272616a616f6e676b69722e74696b695f65636f223b733a353a227469746c65223b733a33363a2243697472612056616e205469746970616e204b696c6174202854494b4929202d2045434f223b733a343a22636f7374223b693a33333530303b733a343a2274657874223b733a393a2252702033332e353030223b7d733a383a2274696b695f6f6e73223b613a343a7b733a343a22636f6465223b733a31393a2272616a616f6e676b69722e74696b695f6f6e73223b733a353a227469746c65223b733a33363a2243697472612056616e205469746970616e204b696c6174202854494b4929202d204f4e53223b733a343a22636f7374223b693a33393030303b733a343a2274657874223b733a393a2252702033392e303030223b7d7d733a353a226572726f72223b623a303b7d7d7368697070696e675f6d6574686f647c613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f6f6b65223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d204f4b45223b733a343a22636f7374223b693a32353030303b733a343a2274657874223b733a393a2252702032352e303030223b7d),
('7qhtue68kn06gs274l5kqs1navu5bf69', '127.0.0.1', 1511274980,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237343739383b63757272656e63797c733a333a22494452223b636172747c613a313a7b733a33313a2236333a59546f784f6e74704f6a557a4f334d364d7a6f694d544979496a7439223b693a313b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636f6d6d656e747c733a303a22223b7061796d656e745f6d6574686f64737c613a323a7b733a333a22626361223b613a333a7b733a343a22636f6465223b733a333a22626361223b733a353a227469746c65223b733a31323a225472616e7366657220424341223b733a31303a22736f72745f6f72646572223b693a313b7d733a373a226d616e64697269223b613a333a7b733a343a22636f6465223b733a373a226d616e64697269223b733a353a227469746c65223b733a31363a225472616e73666572204d616e64697269223b733a31303a22736f72745f6f72646572223b693a313b7d7d7061796d656e745f6d6574686f647c613a333a7b733a343a22636f6465223b733a333a22626361223b733a353a227469746c65223b733a31323a225472616e7366657220424341223b733a31303a22736f72745f6f72646572223b693a313b7d6f726465725f69647c693a32353b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a383a7b733a373a226a6e655f6f6b65223b613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f6f6b65223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d204f4b45223b733a343a22636f7374223b693a32353030303b733a343a2274657874223b733a393a2252702032352e303030223b7d733a373a226a6e655f726567223b613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f726567223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d20524547223b733a343a22636f7374223b693a32393030303b733a343a2274657874223b733a393a2252702032392e303030223b7d733a373a226a6e655f796573223b613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f796573223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d20594553223b733a343a22636f7374223b693a33333030303b733a343a2274657874223b733a393a2252702033332e303030223b7d733a32323a22706f735f70616b65745f6b696c61745f6b6875737573223b613a343a7b733a343a22636f6465223b733a33333a2272616a616f6e676b69722e706f735f70616b65745f6b696c61745f6b6875737573223b733a353a227469746c65223b733a34303a22504f5320496e646f6e657369612028504f5329202d2050616b6574204b696c6174204b6875737573223b733a343a22636f7374223b693a32393030303b733a343a2274657874223b733a393a2252702032392e303030223b7d733a32373a22706f735f657870726573735f6e6578745f6461795f626172616e67223b613a343a7b733a343a22636f6465223b733a33383a2272616a616f6e676b69722e706f735f657870726573735f6e6578745f6461795f626172616e67223b733a353a227469746c65223b733a34353a22504f5320496e646f6e657369612028504f5329202d2045787072657373204e6578742044617920426172616e67223b733a343a22636f7374223b693a33343530303b733a343a2274657874223b733a393a2252702033342e353030223b7d733a383a2274696b695f726567223b613a343a7b733a343a22636f6465223b733a31393a2272616a616f6e676b69722e74696b695f726567223b733a353a227469746c65223b733a33363a2243697472612056616e205469746970616e204b696c6174202854494b4929202d20524547223b733a343a22636f7374223b693a33343530303b733a343a2274657874223b733a393a2252702033342e353030223b7d733a383a2274696b695f65636f223b613a343a7b733a343a22636f6465223b733a31393a2272616a616f6e676b69722e74696b695f65636f223b733a353a227469746c65223b733a33363a2243697472612056616e205469746970616e204b696c6174202854494b4929202d2045434f223b733a343a22636f7374223b693a33333530303b733a343a2274657874223b733a393a2252702033332e353030223b7d733a383a2274696b695f6f6e73223b613a343a7b733a343a22636f6465223b733a31393a2272616a616f6e676b69722e74696b695f6f6e73223b733a353a227469746c65223b733a33363a2243697472612056616e205469746970616e204b696c6174202854494b4929202d204f4e53223b733a343a22636f7374223b693a33393030303b733a343a2274657874223b733a393a2252702033392e303030223b7d7d733a353a226572726f72223b623a303b7d7d7368697070696e675f6d6574686f647c613a343a7b733a343a22636f6465223b733a31383a2272616a616f6e676b69722e6a6e655f6f6b65223b733a353a227469746c65223b733a33343a224a616c7572204e75677261686120456b616b7572697220284a4e4529202d204f4b45223b733a343a22636f7374223b693a32353030303b733a343a2274657874223b733a393a2252702032352e303030223b7d),
('rba4r0th8i91e8thpq1i4nfk3pgkju5g', '127.0.0.1', 1511275493,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237353230393b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('i5s8d08kpfabggu2su8bfu626h34mnjt', '127.0.0.1', 1511275812,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237353532303b63757272656e63797c733a333a22494452223b636172747c613a313a7b733a33313a2236333a59546f784f6e74704f6a557a4f334d364d7a6f694d544978496a7439223b693a323b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('hep5dhs0lfb4jelmngmrohq63900qg3j', '127.0.0.1', 1511276144,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237353835343b63757272656e63797c733a333a22494452223b636172747c613a323a7b733a33313a2236333a59546f784f6e74704f6a557a4f334d364d7a6f694d544978496a7439223b693a323b693a3131383b693a313b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('sa6en0o4aiqknoimvc7a676dbcu2tkh6', '127.0.0.1', 1511276459,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237363136363b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('0ao4nsll7mromhfvktteuuf69iok1jlh', '127.0.0.1', 1511276779,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237363439393b63757272656e63797c733a333a22494452223b636172747c613a323a7b693a3130383b693a313b693a3131383b693a313b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('6smp5gb4aqe88j1ag2igcutd4f5lm539', '127.0.0.1', 1511277100,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237363830333b63757272656e63797c733a333a22494452223b636172747c613a323a7b693a3130383b693a313b693a3131383b693a313b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('v3psdr8iud429f1fejmhu83degbiud3p', '127.0.0.1', 1511277432,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237373132363b63757272656e63797c733a333a22494452223b636172747c613a323a7b693a3130383b693a323b693a3131383b693a313b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('7fv0c8c7kt2c9rhqhlosrrc0t2ua0tjb', '127.0.0.1', 1511277720,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237373433323b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('5a970tb3dqc2v86q3qppgk5n36l5na7g', '127.0.0.1', 1511277918,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237373735343b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('cp6so1j50a5k7ukscfuelia3v3qvp4o3', '127.0.0.1', 1511278348,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237383036373b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('atkam1k9aono118cvemor18mjv28d0h4', '127.0.0.1', 1511278780,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237383439323b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('tvv7qcr4odrs1nja4v3tfk5a9oh8jios', '127.0.0.1', 1511279072,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237383831353b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('ojuvormtt28tod7nh79mtd5iluf5hivu', '127.0.0.1', 1511279395,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237393133383b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('1ccpbfqfu6gudd82dmjjocuiosg2s83m', '127.0.0.1', 1511279740,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237393434333b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('su2hjrba6stbdrotaqlmc9t7quoo6i2s', '127.0.0.1', 1511280033,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313237393734363b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('u8e9626kdljcmfq4l0k70b858ftbijjq', '127.0.0.1', 1511280359,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238303037373b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b),
('vf7nd6u81q7vsof73vcfcn9l34gc04q5', '127.0.0.1', 1511280777,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238303430323b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a22446b5158536a223b733a343a2274696d65223b643a313531313238303437362e373839353632393b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238303437362e373839362e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d),
('ods4r5jrg6kfakrrtaq831uapfo0kobu', '127.0.0.1', 1511281084,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238303830313b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a22734572564231223b733a343a2274696d65223b643a313531313238313038342e3339393838393b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238313038342e333939392e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d),
('o7q815a316osc4f5tcu8qrputg1ol2r0', '127.0.0.1', 1511281289,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238313131343b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a2261784c4b4669223b733a343a2274696d65223b643a313531313238313237392e383833373733313b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238313237392e383833382e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d),
('pul2tr97dl9eckjuj2gcojgs0b7fonlc', '127.0.0.1', 1511281947,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238313638353b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a2261784c4b4669223b733a343a2274696d65223b643a313531313238313237392e383833373733313b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238313237392e383833382e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d),
('124mm3ah03seruaalmhugrb4s7bllpo7', '127.0.0.1', 1511282132,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238313939333b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a226e4d65623142223b733a343a2274696d65223b643a313531313238323132372e3938373131333b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238323132372e393837312e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d),
('qh3gau0bq78lgmievgprjug3hkkavjnc', '127.0.0.1', 1511282381,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238323333353b63757272656e63797c733a333a22494452223b636172747c613a303a7b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a226e4d65623142223b733a343a2274696d65223b643a313531313238323132372e3938373131333b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238323132372e393837312e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d),
('ur6fftfjdv4q392tosr90nc9vof7dflq', '127.0.0.1', 1511282902,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238323638303b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a226e4d65623142223b733a343a2274696d65223b643a313531313238323132372e3938373131333b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238323132372e393837312e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a303a7b7d733a353a226572726f72223b733a34393a22476167616c2073616174206d656e67616d62696c20696e666f726d617369206d65746f64652070656e676972696d616e2e223b7d7d),
('f28por4paii0ue70vsrgnjlr558381u2', '127.0.0.1', 1511283016,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238333031363b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a226e4d65623142223b733a343a2274696d65223b643a313531313238323132372e3938373131333b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238323132372e393837312e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a303a7b7d733a353a226572726f72223b733a34393a22476167616c2073616174206d656e67616d62696c20696e666f726d617369206d65746f64652070656e676972696d616e2e223b7d7d),
('rjlkaf5f9ekbon66eqp31b4slehsa3kv', '127.0.0.1', 1511283736,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238333438363b63757272656e63797c733a333a22494452223b61646d696e5f72656469726563747c733a32323a226f66666963652f636174616c6f672f70726f64756374223b61646d696e5f69647c733a323a222d31223b),
('9ivglotif2bmgbpnvnbce2gdlj19pfhv', '127.0.0.1', 1511283812,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238333830303b63757272656e63797c733a333a22494452223b61646d696e5f72656469726563747c733a32323a226f66666963652f636174616c6f672f70726f64756374223b61646d696e5f69647c733a323a222d31223b),
('qjpon46itt2ifa0m4pc250edo2cge48h', '127.0.0.1', 1511284443,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238343135303b63757272656e63797c733a333a22494452223b61646d696e5f72656469726563747c733a32323a226f66666963652f636174616c6f672f70726f64756374223b61646d696e5f69647c733a323a222d31223b),
('7prio4ppbu4kkeennajrq532u12rojt1', '127.0.0.1', 1511284869,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238343539363b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a226e4d65623142223b733a343a2274696d65223b643a313531313238323132372e3938373131333b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238323132372e393837312e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a303a7b7d733a353a226572726f72223b733a34393a22476167616c2073616174206d656e67616d62696c20696e666f726d617369206d65746f64652070656e676972696d616e2e223b7d7d),
('q9hh5h9ash00nk7321unudtqu9osb2fu', '127.0.0.1', 1511285261,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238343937303b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a226e4d65623142223b733a343a2274696d65223b643a313531313238323132372e3938373131333b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238323132372e393837312e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a303a7b7d733a353a226572726f72223b733a34393a22476167616c2073616174206d656e67616d62696c20696e666f726d617369206d65746f64652070656e676972696d616e2e223b7d7d),
('fl2vr1bpeo4rjukgbt1c6hfehi18reeh', '127.0.0.1', 1511285546,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238353330313b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a226e4d65623142223b733a343a2274696d65223b643a313531313238323132372e3938373131333b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238323132372e393837312e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a303a7b7d733a353a226572726f72223b733a34393a22476167616c2073616174206d656e67616d62696c20696e666f726d617369206d65746f64652070656e676972696d616e2e223b7d7d),
('igcg7egcgpssjsktpc3kthd1udbaa259', '127.0.0.1', 1511285410,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238353430393b63757272656e63797c733a333a22494452223b61646d696e5f72656469726563747c733a32323a226f66666963652f636174616c6f672f70726f64756374223b61646d696e5f69647c733a323a222d31223b),
('0edc29hfqbmqo16c4i48qsje6ibj8k11', '127.0.0.1', 1511285916,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238353636353b63757272656e63797c733a333a22494452223b636172747c613a313a7b693a36343b693a313b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a226e4d65623142223b733a343a2274696d65223b643a313531313238323132372e3938373131333b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238323132372e393837312e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d637573746f6d65725f69647c733a313a2232223b7368697070696e675f6d6574686f64737c613a313a7b733a31303a2272616a616f6e676b6972223b613a333a7b733a353a227469746c65223b733a31373a224d65746f64652050656e676972696d616e223b733a353a2271756f7465223b613a303a7b7d733a353a226572726f72223b733a34393a22476167616c2073616174206d656e67616d62696c20696e666f726d617369206d65746f64652070656e676972696d616e2e223b7d7d),
('4phqittdmkjul1shem2prcldbec83d7p', '127.0.0.1', 1511286189,
 0x5f5f63695f6c6173745f726567656e65726174657c693a313531313238353936373b63757272656e63797c733a333a22494452223b636172747c613a323a7b693a36343b693a313b733a33313a2236333a59546f784f6e74704f6a55304f334d364d7a6f694d544930496a7439223b693a323b7d7368697070696e675f616464726573735f69647c733a323a223131223b7368697070696e675f70726f76696e63655f69647c733a323a223131223b7368697070696e675f636974795f69647c733a333a22323536223b7368697070696e675f73756264697374726963745f69647c733a343a2233363337223b636170746368617c613a333a7b733a343a22776f7264223b733a363a226e4d65623142223b733a343a2274696d65223b643a313531313238323132372e3938373131333b733a353a22696d616765223b733a3136333a223c696d67207372633d22687474703a2f2f6d6564616e736f66742e6465762f73746f726167652f636170746368612f313531313238323132372e393837312e6a7067222077696474683d2232303022206865696768743d22333022207374796c653d22626f726465723a303b2077696474683a313030253b6865696768743a6175746f3b6d617267696e2d626f74746f6d3a313070783b2220616c743d222022202f3e223b7d637573746f6d65725f69647c733a313a2232223b);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting`
(
  `setting_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `group` varchar
(
  32
) NOT NULL,
  `key` varchar
(
  64
) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint
(
  1
) NOT NULL,
  PRIMARY KEY
(
  `setting_id`
),
  KEY `group`
(
  `group`
),
  KEY `key`
(
  `key`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=2352;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`setting_id`, `group`, `key`, `value`, `serialized`)
VALUES
  (2224, 'payment_midtrans', 'expired_status_id', '1', 0),
  (2223, 'payment_midtrans', 'deny_status_id', '1', 0),
  (2222, 'payment_midtrans', 'pending_status_id', '1', 0),
  (2221, 'payment_midtrans', 'challenge_status_id', '1', 0),
  (2219, 'payment_midtrans', 'sort_order', '0', 0),
  (2220, 'payment_midtrans', 'success_status_id', '1', 0),
  (2218, 'payment_midtrans', 'total', '0', 0),
  (2176, 'payment_mandiri', 'sort_order', '0', 0),
  (2177, 'payment_mandiri', 'active', '1', 0),
  (2178, 'payment_mandiri', 'success_status_id', '1', 0),
  (2179, 'payment_mandiri', 'instruction',
   '<p><img style="width: 199.812px; height: 105px;" src="http://medansoft.dev/storage/images/data/logo/Logo_Bank_Mandiri.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank Mandiri berikut :</p><p>Nomor : 1234567890</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Malang<br></p>',
   0),
  (2351, 'config', 'footer_banner_id', '6', 0),
  (2350, 'config', 'middle_banner_id', '5', 0),
  (2349, 'config', 'top_banner_id', '4', 0),
  (2348, 'config', 'slideshow_banner_id', '3', 0),
  (2347, 'config', 'order_complete_status_id', '7', 0),
  (2346, 'config', 'order_delivered_status_id', '6', 0),
  (2345, 'config', 'order_process_status_id', '4', 0),
  (2192, 'addon_custom', 'active', '1', 0),
  (2191, 'addon_custom', 'subtitle', 'Simulasi PC Rakitan', 0),
  (2190, 'addon_custom', 'title', 'PC Rakitan', 0),
  (2344, 'config', 'order_paid_status_id', '3', 0),
  (2343, 'config', 'order_cancel_status_id', '5', 0),
  (2342, 'config', 'order_status_id', '1', 0),
  (2341, 'config', 'google_json', 'google.json', 0),
  (2340, 'config', 'facebook_app_secret', '189af7741e29617703e966aa7ec300c8', 0),
  (2339, 'config', 'facebook_app_id', '1728849037379483', 0),
  (2338, 'config', 'rajaongkir_courier', 'a:3:{i:0;s:3:"jne";i:1;s:3:"pos";i:2;s:4:"tiki";}', 1),
  (2337, 'config', 'rajaongkir_weight_class_id', '3', 0),
  (2336, 'config', 'subdistrict_id', '3915', 0),
  (2335, 'config', 'city_id', '278', 0),
  (2334, 'config', 'province_id', '34', 0),
  (2333, 'config', 'rajaongkir_api_key', '2e8529d097023ed1aa080f759cd8a3d0', 0),
  (2330, 'config', 'seo_title',
   'Sit an solum sonet erroribus. Et quando apeirian pertinax mea, et natum iudico populo nam. No nostrum eligendi perpetua sit, ut vel ipsum graeci. At cum convenire similique, posse ubique insolens id sea, facer graece pro ea',
   0),
  (2332, 'config', 'meta_description', '', 0),
  (2331, 'config', 'meta_keywords', '', 0),
  (2329, 'config', 'site_name', 'MedanSoft', 0),
  (2328, 'config', 'youtube_user', '', 0),
  (2327, 'config', 'gplus_user', '', 0),
  (2326, 'config', 'instagram_user', '', 0),
  (2325, 'config', 'twitter_user', '', 0),
  (2324, 'config', 'facebook_user', '', 0),
  (2323, 'config', 'length_class_id', '1', 0),
  (2322, 'config', 'weight_class_id', '3', 0),
  (2321, 'config', 'smtp_pass', 'thisisthepassword', 0),
  (2320, 'config', 'smtp_user', '', 0),
  (2319, 'config', 'smtp_port', '', 0),
  (2318, 'config', 'smtp_host', '', 0),
  (2317, 'config', 'smtp_email', '', 0),
  (2316, 'config', 'logo', 'data/logo/logo-dark.png', 0),
  (2314, 'config', 'fax', '', 0),
  (2315, 'config', 'email', 'info@medansoft.com', 0),
  (2313, 'config', 'telephone', '+62 21 55555', 0),
  (2217, 'payment_midtrans', 'server_key', 'qwertyuioplkjhgfdsazxcvbnm', 0),
  (2216, 'payment_midtrans', 'client_key', '1234567890987654321', 0),
  (2215, 'payment_midtrans', 'display_name', 'Kartu Kredit', 0),
  (2172, 'payment_bca', 'success_status_id', '1', 0),
  (2171, 'payment_bca', 'active', '1', 0),
  (2170, 'payment_bca', 'sort_order', '0', 0),
  (2169, 'payment_bca', 'total', '0', 0),
  (2168, 'payment_bca', 'display_name', 'Transfer BCA', 0),
  (2173, 'payment_bca', 'instruction',
   '<p><img style="width: 199.839px; height: 62px;" src="http://medansoft.dev/storage/images/data/logo/bca.png"><br></p><p>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA berikut :</p><p>Nomor : 0987654321</p><p>Atas Nama : Budi Waseso</p><p>Cabang : Surabaya<br></p>',
   0),
  (2174, 'payment_mandiri', 'display_name', 'Transfer Mandiri', 0),
  (2175, 'payment_mandiri', 'total', '0', 0),
  (2312, 'config', 'address', 'Jl. Merdeka No. 17\r\nMedan Kota - Medan\r\nSumatera Utara\r\n', 0),
  (2311, 'config', 'company', 'PT. MedanSoft', 0),
  (2228, 'addon_offers', 'active', '1', 0),
  (2226, 'addon_offers', 'title', 'Flash Deals', 0),
  (2227, 'addon_offers', 'subtitle', 'Penawaran spesial di hari ini', 0),
  (2225, 'payment_midtrans', 'cancel_status_id', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stock_status`
--

CREATE TABLE IF NOT EXISTS `stock_status`
(
  `stock_status_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `name` varchar
(
  32
) NOT NULL DEFAULT '',
  PRIMARY KEY
(
  `stock_status_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=4;

--
-- Dumping data for table `stock_status`
--

INSERT INTO `stock_status` (`stock_status_id`, `name`)
VALUES
  (1, 'Tersedia'),
  (2, 'Habis');

-- --------------------------------------------------------

--
-- Table structure for table `weight_class`
--

CREATE TABLE IF NOT EXISTS `weight_class`
(
  `weight_class_id` int
(
  11
) NOT NULL AUTO_INCREMENT,
  `title` varchar
(
  32
) NOT NULL DEFAULT '',
  `unit` varchar
(
  4
) NOT NULL DEFAULT '',
  `value` decimal
(
  15,
  4
) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY
(
  `weight_class_id`
)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

--
-- Dumping data for table `weight_class`
--

INSERT INTO `weight_class` (`weight_class_id`, `title`, `unit`, `value`)
VALUES
  (3, 'Gram', 'gr', '1.0000'),
  (4, 'Kilogram', 'kg', '0.0000');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
