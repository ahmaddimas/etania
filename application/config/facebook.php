<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['facebook']['token_name'] = 'facebook_token';
$config['facebook']['app_id'] = '';
$config['facebook']['app_secret'] = '';
$config['facebook']['redirect_url'] = '';
$config['facebook']['permissions'] = array('email');
