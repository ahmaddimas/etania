<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf
{
    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        require_once(APPPATH . 'third_party/dompdf/dompdf_config.inc.php');

        spl_autoload_register('DOMPDF_autoload');
    }

    /**
     * Create
     *
     * @access public
     * @param string $html
     * @param string $filename
     * @param bool $stream (default: TRUE)
     * @return void
     */
    public function pdf_create($html, $filename, $stream = TRUE)
    {
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'portrait');
        $dompdf->render();

        $canvas = $dompdf->get_canvas();
        $font = Font_Metrics::get_font('helvetica');
        $canvas->page_text(34, 18, 'Halaman : {PAGE_NUM} dari {PAGE_COUNT}', $font, 6, array(0, 0, 0));

        if ($stream) {
            $dompdf->stream($filename . '.pdf', array('Attachment' => 0));
        } else {
            $ci =& get_instance();
            $ci->load->helper('file');

            write_file($filename, $dompdf->output());
        }
    }

    /**
     * Get output
     *
     * @access public
     * @param string $html
     * @return void
     */
    public function get_output($html)
    {
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'portrait');
        $dompdf->render();

        $canvas = $dompdf->get_canvas();
        $font = Font_Metrics::get_font('helvetica');
        $canvas->page_text(40, 18, 'Halaman : {PAGE_NUM} dari {PAGE_COUNT}', $font, 6, array(0, 0, 0));

        return $dompdf->output();
    }

    /**
     * Render shipping note
     *
     * @access public
     * @param string $html
     * @param string $filename
     * @param bool $stream (default: TRUE)
     * @return void
     */
    public function render_shipping_note($html, $filename, $stream = TRUE)
    {
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'landscape');
        $dompdf->render();

        if ($stream) {
            $dompdf->stream($filename . '.pdf', array('Attachment' => 0));
        } else {
            $ci =& get_instance();
            $ci->load->helper('file');

            write_file($filename, $dompdf->output());
        }
    }
}