<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{
    public $CI;

    /**
     * Get array of errors
     *
     * @access public
     * @return array
     */
    public function get_errors()
    {
        $errors = array();

        foreach ($this->_field_data as $field => $data) {
            $error = $this->error($field);

            if ($error) {
                $errors[$field] = $error;
            }
        }

        return $errors;
    }
}