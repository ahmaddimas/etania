<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping_cart
{
    /**
     * CI Instance
     *
     * @var object
     * @access private
     */
    private $ci;

    /**
     * Items data
     *
     * @var array
     * @access private
     */
    private $data = array();

    /**
     * Shopping cart content
     *
     * @var array
     * @access private
     */
    private $content = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->ci =& get_instance();

        $this->content = $this->ci->session->userdata('cart');

        if (!$this->content || !is_array($this->content)) {
            $this->content = array();
        }

        $this->ci->load->library('weight');
    }

    /**
     * Get product
     *
     * @access public
     * @param int $product_id
     * @return array
     */
    private function _get_product($product_id)
    {
        return $this->ci->db
            ->from('product')
            ->where('product_id', (int)$product_id)
            ->where('active', 1)
            ->get()
            ->row_array();
    }

    /**
     * Get product discount
     *
     * @access private
     * @param int $product_id
     * @param int $discount_quantity
     * @return array
     */
    private function _get_product_discount($product_id, $discount_quantity)
    {
        return $this->ci->db
            ->select('price')
            ->where('product_id', (int)$product_id)
            ->where('quantity >', 1)
            ->where('quantity <=', (int)$discount_quantity)
            ->where("(date_start = '0000-00-00' || date_start < NOW())", null, false)
            ->where("(date_end = '0000-00-00' || date_end > NOW())", null, false)
            ->order_by('quantity', 'desc')
            ->order_by('priority', 'asc')
            ->order_by('price', 'asc')
            ->limit(1)
            ->get('product_discount')
            ->row_array();
    }

    /**
     * Get produc special
     *
     * @access private
     * @param int $product_id
     * @return array
     */
    private function _get_product_special($product_id)
    {
        return $this->ci->db
            ->select('price')
            ->where('product_id', (int)$product_id)
            ->where('quantity', 1)
            ->where("(date_start = '0000-00-00' || date_start < NOW())", null, false)
            ->where("(date_end = '0000-00-00' || date_end > NOW())", null, false)
            ->order_by('priority', 'asc')
            ->order_by('price', 'asc')
            ->limit(1)
            ->get('product_discount')
            ->row_array();
    }

    /**
     * Get product option
     *
     * @access private
     * @param int $product_option_id
     * @param int $product_id
     * @return array
     */
    private function _get_product_option($product_option_id, $product_id)
    {
        return $this->ci->db
            ->select('po.product_option_id, po.option_id, o.name, o.type')
            ->from('product_option po')
            ->join('option o', 'po.option_id = o.option_id', 'left')
            ->where('po.product_option_id', (int)$product_option_id)
            ->where('po.product_id', (int)$product_id)
            ->get()
            ->row_array();
    }

    /**
     * Get product option value
     *
     * @access public
     * @param int $product_option_value_id
     * @param int $product_option_id
     * @return array
     */
    private function _get_product_option_value($product_option_value_id, $product_option_id)
    {
        return $this->ci->db
            ->select('pov.option_value_id, ov.name, pov.quantity, pov.subtract, pov.price, pov.points, pov.weight')
            ->from('product_option_value pov')
            ->join('option_value ov', 'pov.option_value_id = ov.option_value_id', 'left')
            ->where('pov.product_option_value_id', (int)$product_option_value_id)
            ->where('pov.product_option_id', (int)$product_option_id)
            ->get()
            ->row_array();
    }


    public function get_product($product_id)
    {
        return $this->ci->db
            ->from('product')
            ->where('product_id', (int)$product_id)
            ->where('active', 1)
            ->get()
            ->row_array();
    }

    /**
     * Get product discount
     *
     * @access private
     * @param int $product_id
     * @param int $discount_quantity
     * @return array
     */
    public function get_product_discount($product_id, $discount_quantity)
    {
        return $this->ci->db
            ->select('price')
            ->where('product_id', (int)$product_id)
            ->where('quantity >', 1)
            ->where('quantity <=', (int)$discount_quantity)
            ->where("(date_start = '0000-00-00' || date_start < NOW())", null, false)
            ->where("(date_end = '0000-00-00' || date_end > NOW())", null, false)
            ->order_by('quantity', 'desc')
            ->order_by('priority', 'asc')
            ->order_by('price', 'asc')
            ->limit(1)
            ->get('product_discount')
            ->row_array();
    }

    /**
     * Get produc special
     *
     * @access private
     * @param int $product_id
     * @return array
     */
    public function get_product_special($product_id)
    {
        return $this->ci->db
            ->select('price')
            ->where('product_id', (int)$product_id)
            ->where('quantity', 1)
            ->where("(date_start = '0000-00-00' || date_start < NOW())", null, false)
            ->where("(date_end = '0000-00-00' || date_end > NOW())", null, false)
            ->order_by('priority', 'asc')
            ->order_by('price', 'asc')
            ->limit(1)
            ->get('product_discount')
            ->row_array();
    }

    /**
     * Get product option
     *
     * @access private
     * @param int $product_option_id
     * @param int $product_id
     * @return array
     */
    public function get_product_option($product_option_id, $product_id)
    {
        return $this->ci->db
            ->select('po.product_option_id, po.option_id, o.name, o.type')
            ->from('product_option po')
            ->join('option o', 'po.option_id = o.option_id', 'left')
            ->where('po.product_option_id', (int)$product_option_id)
            ->where('po.product_id', (int)$product_id)
            ->get()
            ->row_array();
    }

    /**
     * Get product option value
     *
     * @access public
     * @param int $product_option_value_id
     * @param int $product_option_id
     * @return array
     */
    public function get_product_option_value($product_option_value_id, $product_option_id)
    {
        return $this->ci->db
            ->select('pov.option_value_id, ov.name, pov.quantity, pov.subtract, pov.price, pov.points, pov.weight')
            ->from('product_option_value pov')
            ->join('option_value ov', 'pov.option_value_id = ov.option_value_id', 'left')
            ->where('pov.product_option_value_id', (int)$product_option_value_id)
            ->where('pov.product_option_id', (int)$product_option_id)
            ->get()
            ->row_array();
    }

    /**
     * Get products data
     *
     * @access public
     * @return void
     */
    public function get_products()
    {
        if (!$this->data) {
            foreach ($this->content as $key => $quantity) {
                $product = explode(':', $key);
                $product_id = $product[0];
                $stock = true;
                $options = !empty($product[1]) ? unserialize(base64_decode(strtr($product[1], '-_', '+/'))) : array();

                if ($product = $this->_get_product($product_id)) {
                    $option_price = 0;
                    $option_points = 0;
                    $option_weight = 0;

                    $option_data = array();

                    foreach ($options as $product_option_id => $option_value) {
                        if ($option_query = $this->_get_product_option($product_option_id, $product_id)) {
                            if ($option_query['type'] == 'select' || $option_query['type'] == 'radio') {
                                if ($option_value_query = $this->_get_product_option_value($option_value, $product_option_id)) {
                                    $option_price += $option_value_query['price'];
                                    $option_points += $option_value_query['points'];
                                    $option_weight += $option_value_query['weight'];

                                    if ($option_value_query['subtract'] && (!$option_value_query['quantity'] || ($option_value_query['quantity'] < $quantity))) {
                                        $stock = false;
                                    }

                                    $option_data[] = array(
                                        'product_option_id' => $product_option_id,
                                        'product_option_value_id' => $option_value,
                                        'option_id' => $option_query['option_id'],
                                        'option_value_id' => $option_value_query['option_value_id'],
                                        'name' => $option_query['name'],
                                        'option_value' => $option_value_query['name'],
                                        'type' => $option_query['type'],
                                        'quantity' => $option_value_query['quantity'],
                                        'subtract' => $option_value_query['subtract'],
                                        'price' => $option_value_query['price'],
                                        'points' => $option_value_query['points'],
                                        'weight' => $option_value_query['weight']
                                    );
                                }
                            } elseif ($option_query['type'] == 'checkbox' && is_array($option_value)) {
                                foreach ($option_value as $product_option_value_id) {
                                    if ($option_value_query = $this->_get_product_option_value($product_option_value_id, $product_option_id)) {
                                        $option_price += $option_value_query['price'];
                                        $option_points += $option_value_query['points'];
                                        $option_weight += $option_value_query['weight'];

                                        if ($option_value_query['subtract'] && (!$option_value_query['quantity'] || ($option_value_query['quantity'] < $quantity))) {
                                            $stock = false;
                                        }

                                        $option_data[] = array(
                                            'product_option_id' => $product_option_id,
                                            'product_option_value_id' => $product_option_value_id,
                                            'option_id' => $option_query['option_id'],
                                            'option_value_id' => $option_value_query['option_value_id'],
                                            'name' => $option_query['name'],
                                            'option_value' => $option_value_query['name'],
                                            'type' => $option_query['type'],
                                            'quantity' => $option_value_query['quantity'],
                                            'subtract' => $option_value_query['subtract'],
                                            'price' => $option_value_query['price'],
                                            'points' => $option_value_query['points'],
                                            'weight' => $option_value_query['weight']
                                        );
                                    }
                                }
                            }
                        }
                    }

                    $price = $product['price'];
                    $discount_quantity = 0;

                    foreach ($this->content as $product_id_2 => $quantity_2) {
                        if ($product_id_2 == $product_id) {
                            $discount_quantity += $quantity_2;
                        }
                    }

                    // check custom part price
                    // see offers module at addons/custom/

                    $addon_custom = $this->ci->config->item('addon_custom');

                    if (!empty($addon_custom['active'])) {
                        $this->ci->load->model('custom/custom_part_model');

                        if ($custom = $this->ci->custom_part_model->get_price($product_id)) {
                            $price = $custom['price'];
                        }
                    }

                    // check special price

                    if ($product_special = $this->_get_product_special($product_id)) {
                        $price = $product_special['price'];
                    }

                    // check discount price

                    if ($product_discount = $this->_get_product_discount($product_id, $discount_quantity)) {
                        $price = $product_discount['price'];
                    }

                    // check flash offers price
                    // see offers module at addons/offers/

                    $addon_offers = $this->ci->config->item('addon_offers');

                    if (!empty($addon_offers['active'])) {
                        $this->ci->load->model('offers/offers_discount_model');

                        if ($offers_discount = $this->ci->offers_discount_model->get_price($product_id)) {
                            $price = $offers_discount['price'];
                        }
                    }


                    // check next price
                    // see next price module at addons/nextprice/

                    $addon_nextprice = $this->ci->config->item('addon_nextprice');

                    if (!empty($addon_nextprice['active'])) {
                        $this->ci->load->model('nextprice/nextprice_step_model');

                        if ($nextprice = $this->ci->nextprice_step_model->get_price($product_id)) {
                            $price = $nextprice['price'];
                        }
                    }

                    if (!$product['quantity'] || ($product['quantity'] < $quantity)) {
                        $stock = false;
                    }

                    $this->data[$key] = array(
                        'key' => $key,
                        'product_id' => $product['product_id'],
                        'type' => $product['type'],
                        'name' => $product['name'],
                        'model' => $product['model'],
                        'sku' => $product['sku'],
                        'image' => $product['image'],
                        'option' => $option_data,
                        'quantity' => $quantity,
                        'minimum' => $product['minimum'],
                        'stock' => $stock,
                        'price' => ($price + $option_price),
                        'total' => ($price + $option_price) * $quantity,
                        'weight' => $product['weight'] * $quantity,
                        'weight_class_id' => $product['weight_class_id'],
                        'category_id' => $product['category_id'],
                        'slug' => $product['slug'],
                        'shipping' => $product['shipping'],
                    );
                } else {
                    $this->remove($key);
                }
            }
        }

        return $this->data;
    }

    /**
     * Add new item into shopping cart
     *
     * @access public
     * @param int $product_id
     * @param int $quantity
     * @return void
     */

    public function add($product_id, $qty = 1, $option = array())
    {
        $p = count($this->get_products());
        $isdigital = $this->cek_product_digital($product_id);
        $digital = $this->get_product_digital();
        if ($isdigital == $digital OR $p == 0) {
            $key = ($option) ? (int)$product_id . ':' . strtr(rtrim(base64_encode(serialize($option)), '='), '+/', '-_') : (int)$product_id;

            if ((int)$qty && ((int)$qty > 0)) {
                if (!isset($this->content[$key])) {
                    $this->content[$key] = (int)$qty;
                } else {
                    $this->content[$key] += (int)$qty;
                }

                $this->ci->session->set_userdata('cart', $this->content);
            }

            $this->data = array();
        }
    }

    /**
     * Update an item from shopping cart
     *
     * @access public
     * @param string $key
     * @param int $quantity
     * @return void
     */
    public function update($key, $quantity)
    {


        $p = count($this->get_products());
        $isdigital = $this->cek_product_digital($key);
        $digital = $this->get_product_digital();
        if ($isdigital == $digital OR $p == 0) {
            if ((int)$quantity && ((int)$quantity > 0)) {
                $this->content[$key] = (int)$quantity;

                $this->ci->session->set_userdata('cart', $this->content);
            } else {
                $this->remove($key);
            }

            $this->data = array();
        }
    }


    /**
     * Remove an item from shopping cart
     *
     * @access public
     * @param string $key
     * @return void
     */
    public function remove($key)
    {
        if (isset($this->content[$key])) {
            unset($this->content[$key]);

            $this->ci->session->set_userdata('cart', $this->content);
        }

        $this->data = array();
    }

    /**
     * Clear shopping cart
     *
     * @access public
     * @return void
     */
    public function clear()
    {
        $this->content = array();
        $this->ci->session->set_userdata('cart', array());

        $this->data = array();
    }

    /**
     * Get weight
     *
     * @access public
     * @return void
     */
    public function get_weight()
    {
        $weight = 0;

        foreach ($this->get_products() as $product) {
            $weight += $this->ci->weight->convert($product['weight'], $product['weight_class_id'], $this->ci->config->item('weight_class_id'));
        }

        return $weight;
    }

    /**
     * Get sub total
     *
     * @access public
     * @return float
     */
    public function get_sub_total()
    {
        $total = 0;

        foreach ($this->get_products() as $product) {
            $total += $product['total'];
        }

        return $total;
    }

    /**
     * Get total
     *
     * @access public
     * @return float
     */
    public function get_total()
    {
        $total = 0;

        foreach ($this->get_products() as $product) {
            $total += $product['price'] * $product['quantity'];
        }

        return $total;
    }

    /**
     * Count products
     *
     * @access public
     * @return int
     */
    public function count_products()
    {
        $product_total = 0;

        foreach ($this->get_products() as $product) {
            $product_total += $product['quantity'];
        }

        return $product_total;
    }


    public function count_product_id($product_id)
    {
        $product_total = 0;

        foreach ($this->get_products() as $product) {
            if ($product['product_id'] == $product_id) {
                $product_total += $product['quantity'];
            }
        }

        return $product_total;
    }

    /**
     * Is has product?
     *
     * @access public
     * @return int
     */
    public function has_products()
    {
        return (bool)count($this->get_products());
    }

    /**
     * Is has stock?
     *
     * @access public
     * @return int
     */
    public function has_stock()
    {
        $stock = true;

        foreach ($this->get_products() as $product) {
            if (!$product['stock']) {
                $stock = false;
                break;
            }
        }

        return $stock;
    }

    /**
     * Is shippable product(s)?
     *
     * @access public
     * @return int
     */
    public function has_shipping()
    {
        $shipping = false;

        foreach ($this->get_products() as $product) {
            if ((bool)$product['shipping']) {
                $shipping = true;
                break;
            }
        }

        return $shipping;
    }


    public function get_product_digital()
    {
        $not_digital = 0;
        foreach ($this->get_products() as $product) {
            if ((bool)$product['type'] == 0) {
                $not_digital = 1;
            }
        }

        return $not_digital;
    }


    public function cek_product_digital($product_id)
    {

        $p = $this->_get_product($product_id);

        $not_digital = 0;

        if ((bool)$p['type'] == 0) {
            $not_digital = 1;
        }

        return $not_digital;
    }


    public function get_product_free_shipping()
    {
        $addon_freeshipping = $this->ci->config->item('addon_freeshipping');
        if (!empty($addon_freeshipping['active'])) {
            $this->ci->load->model('freeshipping/product_free_shipping_model');

            $not_freeshipping = 0;
            foreach ($this->get_products() as $product) {
                $ct = $this->ci->product_free_shipping_model->cek_category($product['category_id']);
                $pr = $this->ci->product_free_shipping_model->cek_product($product['product_id']);

                if ((count($ct) == 0) AND (count($pr) == 0)) {
                    $not_freeshipping = 1;
                }
            }
        } else {
            $not_freeshipping = 1;
        }

        return $not_freeshipping;
    }


    public function cek_product_free_shipping($product_id)
    {
        $addon_freeshipping = $this->ci->config->item('addon_freeshipping');
        if (!empty($addon_freeshipping['active'])) {
            $this->ci->load->model('freeshipping/product_free_shipping_model');

            $p = $this->_get_product($product_id);

            $not_freeshipping = 0;

            $ct = $this->ci->product_free_shipping_model->cek_category($p['category_id']);
            $pr = $this->ci->product_free_shipping_model->cek_product($p['product_id']);

            if ((count($ct) == 0) AND (count($pr) == 0)) {
                $not_freeshipping = 1;
            }
        } else {
            $not_freeshipping = 1;
        }

        return $not_freeshipping;
    }


}