<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seo
{
    /**
     * CI Instance
     *
     * @var object
     * @access private
     */
    private $ci;

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->ci =& get_instance();
    }

    /**
     * Generate meta keywords
     *
     * @access public
     * @param string $string
     * @return string
     */
    public function meta_keyword($string)
    {
        $string = strtolower($string);
        $string = preg_replace('/<[^>]*>/', ' ', $string);
        $string = str_replace("\r", '', $string);
        $string = str_replace("\n", ' ', $string);
        $string = str_replace("\t", ' ', $string);
        $string = trim(preg_replace('/ {2,}/', ' ', $string));
        $string = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $string);
        $string = preg_replace("#[[:punct:]]#", "", $string);
        $string = preg_replace("[^-\w\d\s\.=$'€%]", '', $string);
        $string = str_replace(' ', ', ', $string);
        $string = str_replace(' ,', '', $string);
        $string = trim($string, ", ");
        $string = trim($string, " ,");
        $string = trim($string, ",");
        $string = trim($string);

        return $string;
    }

    /**
     * Generate meta description
     *
     * @access public
     * @param string $string
     * @return string
     */
    public function meta_description($string)
    {
        $string = preg_replace('/<[^>]*>/', ' ', $string);
        $string = str_replace("\r", '', $string);
        $string = str_replace("\n", ' ', $string);
        $string = str_replace("\t", ' ', $string);
        $string = str_replace("&nbsp;&nbsp;&nbsp;", ' ', $string);
        $string = str_replace("&nbsp;&nbsp;", ' ', $string);
        $string = str_replace("&nbsp; &nbsp;", ' ', $string);
        $string = trim(preg_replace('/ {2,}/', ' ', $string));
        $string = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $string);
        $string = preg_replace("#[[:punct:]]#", "", $string);
        $string = trim($string, ", ");
        $string = trim($string, " ,");
        $string = trim($string, ",");
        $string = str_replace("  ", ' ', $string);
        $string = trim($string);
        $string = substr($string, 0, 160);

        return $string;
    }
}