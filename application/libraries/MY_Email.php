<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Email extends CI_Email
{
    /**
     * Initialize preferences
     *
     * @access    public
     * @param    array
     * @return    void
     */
    public function initialize(array $config = array())
    {
        if (count($config) == 0) {
            $ci =& get_instance();

            $config['smtp_host'] = $ci->config->item('smtp_host');
            $config['smtp_port'] = $ci->config->item('smtp_port');
            $config['smtp_user'] = $ci->config->item('smtp_user');
            $config['smtp_pass'] = $ci->config->item('smtp_pass');

            if ($config['smtp_host'] == '' || $config['smtp_port'] || $config['smtp_user'] || $config['smtp_pass'] == '') {
                $config['protocol'] = 'mail';
            } else {
                $config['protocol'] = 'smtp';
            }

            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
            $config['newline'] = "\r\n";
        }

        foreach ($config as $key => $val) {
            if (isset($this->$key)) {
                $method = 'set_' . $key;

                if (method_exists($this, $method)) {
                    $this->$method($val);
                } else {
                    $this->$key = $val;
                }
            }
        }

        $this->clear();

        $this->_smtp_auth = ($this->smtp_user == '' AND $this->smtp_pass == '') ? FALSE : TRUE;
        $this->_safe_mode = ((boolean)@ini_get("safe_mode") === FALSE) ? FALSE : TRUE;

        return $this;
    }
}