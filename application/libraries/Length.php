<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Length
{
    private $ci;

    /**
     * length classes data
     *
     * @var array
     * @access private
     */
    private $lengths = array();

    /**
     * Constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->model('system/length_class_model');

        foreach ($this->ci->length_class_model->get_all() as $length) {
            $this->lengths[$length['length_class_id']] = $length;
        }
    }

    /**
     * Convert length class to another class
     *
     * @access public
     * @param int $value length value
     * @param int $from length id as source
     * @param int $to length id as destination
     * @return number
     */
    public function convert($value = 0, $from = null, $to = null)
    {
        if ($from == $to) return $value;

        $from = (isset($this->lengths[$from])) ? $this->lengths[$from]['value'] : 0;
        $to = (isset($this->lengths[$to])) ? $this->lengths[$to]['value'] : 0;

        if ($from != 0 && $to != 0) {
            return $value * ($to / $from);
        }

        return $value;
    }

    /**
     * Get length unit
     *
     * @access public
     * @param int $id length id
     * @return string length unit
     */
    public function get_unit($id = null)
    {
        return (isset($this->lengths[$id])) ? $this->lengths[$id]['unit'] : '';
    }

    /**
     * Format length class
     *
     * @access public
     * @param int $value length value
     * @param int $id length id
     * @param string $decimal_point Decimal separtor
     * @param string $thousand_point Thousand separator
     * @return string
     */
    public function format($value = 0, $id = null, $decimal_point = ',', $thousand_point = '.')
    {
        return (isset($this->lengths[$id]))
            ? number_format($value, 2, $decimal_point, $thousand_point) . $this->lengths[$id]['unit']
            : number_format($value, 2, $decimal_point, $thousand_point);
    }
}