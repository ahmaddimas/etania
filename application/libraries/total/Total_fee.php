<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Total_fee
{
    protected $sort_order = 7;

    public function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->library('currency');
    }

    /**
     * Get total
     *
     * @access public
     * @param array &$total_data
     * @param float &$total
     * @return void
     */
    public function get(&$total_data, &$total)
    {
        $payment_method = $this->ci->session->userdata('payment_method');

        if ($payment_method) {
            if ($payment_method['fee']) {
                $total_data[] = array(
                    'code' => 'fee',
                    'title' => $payment_method['note'],
                    'text' => $this->ci->currency->format($payment_method['fee_nominal']),
                    'value' => $payment_method['fee_nominal'],
                    'sort_order' => $this->sort_order
                );

                $total += $payment_method['fee_nominal'];
            }
        }
    }
}