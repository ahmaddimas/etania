<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Total_shipping
{
    protected $sort_order = 6;

    public function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->library('currency');
    }

    /**
     * Get total
     *
     * @access public
     * @param array &$total_data
     * @param float &$total
     * @return void
     */
    public function get(&$total_data, &$total)
    {
        $shipping_method = $this->ci->session->userdata('shipping_method');

        if ($shipping_method) {
            $total_data[] = array(
                'code' => 'shipping',
                'title' => $shipping_method['title'],
                'text' => $this->ci->currency->format($shipping_method['cost']),
                'value' => $shipping_method['cost'],
                'sort_order' => $this->sort_order
            );

            $total += $shipping_method['cost'];
        }
    }
}