<div class="page-title">
    <div>
        <h1><?= $template['title'] ?></h1>
    </div>
    <div class="btn-group">
        <button onclick="window.location = '<?= admin_url('payment/payment_transfer') ?>';" class="btn btn-default"><i
                    class="fa fa-reply"></i> <?= lang('button_cancel') ?></button>
        <button id="submit" class="btn btn-success"><i class="fa fa-check"></i> <?= lang('button_save') ?></button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-title-w-btn">

            </div>
            <div class="card-body">
                <?= form_open($action, 'id="form" role="form" class="form-horizontal"') ?>
                <input type="hidden" name="payment_transfer_id" value="<?= $payment_transfer_id ?>">
                <div class="form-group">
                    <label class="control-label col-sm-3">Nama</label>
                    <div class=" col-sm-9">
                        <input type="text" class="form-control" name="name" value="<?= $name ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3">Display Name</label>
                    <div class=" col-sm-9">
                        <input type="text" class="form-control" name="display_name" value="<?= $display_name ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3">Nama Bank</label>
                    <div class=" col-sm-9">
                        <input type="text" class="form-control" name="bank_name" value="<?= $bank_name ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3">Logo Bank</label>
                    <div class=" col-sm-3">
                        <img id="image-logo" src="<?= $thumb_logo ?>" class="img-thumbnail"
                             data-placeholder="<?= $no_image ?>">
                        <br><span><font color="red">Recomended size 400px x 200px</font></span>
                        <div class="caption" style="margin-top:10px;">
                            <button type="button" class="btn btn-default btn-block" id="upload-logo">Ubah Logo</button>
                        </div>
                        <input type="hidden" id="input-logo" name="logo" value="<?= $logo ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3">Nama Rekening</label>
                    <div class=" col-sm-9">
                        <input type="text" class="form-control" name="rekening_name" value="<?= $rekening_name ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3">No Rekening</label>
                    <div class=" col-sm-9">
                        <input type="text" class="form-control" name="rekening_number" value="<?= $rekening_number ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Minimal Belanja</label>
                    <div class="col-sm-3">
                        <input type="text" name="total" value="<?= $total ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Urutan</label>
                    <div class="col-sm-3">
                        <input type="text" name="sort_order" value="<?= $sort_order ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Aktif</label>
                    <div class="toggle lg col-sm-4">
                        <label style="margin-top:5px;">
                            <?php if ($active) { ?>
                                <input type="checkbox" name="active" value="1" checked="checked">
                            <?php } else { ?>
                                <input type="checkbox" name="active" value="1">
                            <?php } ?>
                            <span class="button-indecator"></span>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Order</label>
                    <div class="col-sm-4">
                        <select name="success_status_id" class="form-control">
                            <?php foreach ($order_statuses as $order_status) { ?>
                                <?php if ($order_status['order_status_id'] == $order_status_id) { ?>
                                    <option value="<?= $order_status['order_status_id'] ?>"
                                            selected="selected"><?= $order_status['name'] ?></option>
                                <?php } else { ?>
                                    <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Instruksi</label>
                    <div class="col-sm-9">
                        <textarea class="form-control summernote" name="instruction"><?= $instruction ?></textarea>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
</div>


<script src="<?= base_url('assets/js/ajaxupload.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    new AjaxUpload('#upload-logo', {
        action: "<?=admin_url('system/setting/upload');?>",
        name: 'userfile',
        autoSubmit: false,
        responseType: 'json',
        onChange: function (file, extension) {
            this.submit();
        },
        onSubmit: function (file, extension) {
            $('#upload-logo').append('<span class="wait"><i class="fa fa-refresh"></i></span>');
        },
        onComplete: function (file, json) {
            if (json.success) {
                $('.wait').remove();
                $('#image-logo').attr('src', json.thumb);
                $('#input-logo').val(json.image);
            }
            if (json.error) {
                alert(json.error);
            }

            $('.loading').remove();
        }
    });

    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['error'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('payment/payment_transfer')?>";
                }
            }
        });
    });
</script>