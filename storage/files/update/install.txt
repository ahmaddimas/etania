rename('storage/files/payment/midtrans.txt', 'application/modules/payment/midtrans.txt');

rename('storage/files/payment/midtrans/config/midtrans.php', 'application/modules/payment/config/midtrans.php');

rename('storage/files/payment/midtrans/controllers/Midtrans.php', 'application/modules/payment/controllers/Midtrans.php');
rename('storage/files/payment/midtrans/controllers/admin/Midtrans.php', 'application/modules/payment/controllers/admin/Midtrans.php');

rename('storage/files/payment/midtrans/libraries/midtrans/cacert.pem', 'application/modules/payment/libraries/midtrans/cacert.pem');
rename('storage/files/payment/midtrans/libraries/Midtrans_lib.php', 'application/modules/payment/libraries/Midtrans_lib.php');

rename('storage/files/payment/midtrans/models/Midtrans_model.php', 'application/modules/payment/models/Midtrans_model.php');

rename('storage/files/payment/midtrans/views/midtrans.php', 'application/modules/payment/views/midtrans.php');
rename('storage/files/payment/midtrans/views/midtrans_instruction.php', 'application/modules/payment/views/midtrans_instruction.php');
rename('storage/files/payment/midtrans/views/admin/midtrans.php', 'application/modules/payment/views/admin/midtrans.php');
