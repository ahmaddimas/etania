<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="<?= $client_key ?>"></script>
<?= form_open($action, 'id="payment-form"') ?>
<input type="hidden" name="result_type" id="result-type" value=""></div>
<input type="hidden" name="result_data" id="result-data" value=""></div>
<?= form_close() ?>
<div>
    <button class="btn btn-primary pull-left" onclick="setSteps(3);"><i class="fa fa-chevron-left"></i> Kembali</button>
    <button class="btn btn-danger pull-right" id="pay-button"><i class="fa fa-money"></i> Proses Pembayaran</button>
</div>
<script type="text/javascript">
    $('#pay-button').click(function (event) {
        event.preventDefault();
        $(this).attr("disabled", "disabled");

        $.ajax({
            url: "<?=site_url('payment/midtrans/token')?>",
            cache: false,
            success: function (data) {
                //location = data;
                console.log('token = ' + data);
                var resultType = document.getElementById('result-type');
                var resultData = document.getElementById('result-data');

                function changeResult(type, data) {
                    $("#result-type").val(type);
                    $("#result-data").val(JSON.stringify(data));
                    //resultType.innerHTML = type;
                    //resultData.innerHTML = JSON.stringify(data);
                }

                snap.pay(data, {
                    onSuccess: function (result) {
                        changeResult('success', result);
                        console.log(result.status_message);
                        console.log(result);
                        $("#payment-form").submit();
                    },
                    onPending: function (result) {
                        changeResult('pending', result);
                        console.log(result.status_message);
                        $("#payment-form").submit();
                    },
                    onError: function (result) {
                        changeResult('error', result);
                        console.log(result.status_message);
                        $("#payment-form").submit();
                    }
                });
            }
        });
    });
</script>