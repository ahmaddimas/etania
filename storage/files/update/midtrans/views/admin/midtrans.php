<div class="page-title">
    <div>
        <h1><?= $heading_title ?></h1>
    </div>
    <div class="btn-group">
        <button id="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
        <button onclick="window.location = '<?= admin_url('system/payments') ?>';" class="btn btn-default"><i
                    class="fa fa-reply"></i> Kembali
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <?= form_open($action, 'role="form" id="form" class="form-horizontal"') ?>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab" id="default">General</a></li>
                <li><a href="#tab-2" data-toggle="tab">Status Order</a></li>
                <li><a href="#tab-3" data-toggle="tab">Setting</a></li>
            </ul>
            <div class="tab-content" style="padding-top:20px;">
                <div class="tab-pane active" id="tab-1">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Teks Menu</label>
                        <div class="col-sm-9">
                            <input type="text" name="display_name" value="<?= $display_name ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mode Production</label>
                        <div class="toggle lg col-sm-4">
                            <label style="margin-top:5px;">
                                <?php if ($production) { ?>
                                    <input type="checkbox" name="production" value="1" checked="checked">
                                <?php } else { ?>
                                    <input type="checkbox" name="production" value="1">
                                <?php } ?>
                                <span class="button-indecator"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Client Key</label>
                        <div class="col-sm-9">
                            <input type="text" name="client_key" value="<?= $client_key ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Server Key</label>
                        <div class="col-sm-9">
                            <input type="text" name="server_key" value="<?= $server_key ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Minimal Belanja</label>
                        <div class="col-sm-3">
                            <input type="text" name="total" value="<?= $total ?>" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Biaya Layanan</label>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="text" name="fees[]" value="<?= $fees[0] ?>" style="text-align:right"
                                       class="form-control"/>
                                <div class="input-group-addon">%</div>
                            </div>
                        </div>
                        <label class="col-sm-1 control-label">+</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" name="fees[]" value="<?= $fees[1] ?>" style="text-align:right"
                                       class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Note</label>
                        <div class="col-sm-9">
                            <input type="text" name="note" value="<?= $note ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Urutan</label>
                        <div class="col-sm-3">
                            <input type="text" name="sort_order" value="<?= $sort_order ?>" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Logo</label>
                        <div class=" col-sm-3">
                            <img id="image-logo" src="<?= $thumb_logo ?>" class="img-thumbnail"
                                 data-placeholder="<?= $no_image ?>">
                            <br><span><font color="red">Recomended size 400px x 200px</font></span>
                            <div class="caption" style="margin-top:10px;">
                                <button type="button" class="btn btn-default btn-block" id="upload-logo">Ubah Logo
                                </button>
                            </div>
                            <input type="hidden" id="input-logo" name="logo" value="<?= $logo ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Aktif</label>
                        <div class="toggle lg col-sm-4">
                            <label style="margin-top:5px;">
                                <?php if ($active) { ?>
                                    <input type="checkbox" name="active" value="1" checked="checked">
                                <?php } else { ?>
                                    <input type="checkbox" name="active" value="1">
                                <?php } ?>
                                <span class="button-indecator"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-2">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status Berhasil</label>
                        <div class="col-sm-4">
                            <select name="success_status_id" class="form-control">
                                <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $success_status_id) { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"
                                                selected="selected"><?= $order_status['name'] ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status Challenge</label>
                        <div class="col-sm-4">
                            <select name="challenge_status_id" class="form-control">
                                <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $challenge_status_id) { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"
                                                selected="selected"><?= $order_status['name'] ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status Pending</label>
                        <div class="col-sm-4">
                            <select name="pending_status_id" class="form-control">
                                <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $pending_status_id) { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"
                                                selected="selected"><?= $order_status['name'] ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status Ditolak</label>
                        <div class="col-sm-4">
                            <select name="deny_status_id" class="form-control">
                                <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $deny_status_id) { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"
                                                selected="selected"><?= $order_status['name'] ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status Kadaluarsa</label>
                        <div class="col-sm-4">
                            <select name="expired_status_id" class="form-control">
                                <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $expired_status_id) { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"
                                                selected="selected"><?= $order_status['name'] ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status Cancel</label>
                        <div class="col-sm-4">
                            <select name="cancel_status_id" class="form-control">
                                <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $cancel_status_id) { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"
                                                selected="selected"><?= $order_status['name'] ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $order_status['order_status_id'] ?>"><?= $order_status['name'] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-3">
                    <table style="width:100%">
                        <tr>
                            <td>
                                Payment Notification URL <br>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                https://www.domain.com/payment/midtrans/notification
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Finish Redirect URL <br>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                https://www.domain.com/payment/midtrans/confirm
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Unfinish Redirect URL <br>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                https://www.domain.com/payment/midtrans/midtrans_instruction
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Error Redirect URL <br>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                https://www.domain.com/payment/midtrans/failure
                            </td>
                        </tr>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/js/ajaxupload.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    new AjaxUpload('#upload-logo', {
        action: "<?=admin_url('system/setting/upload');?>",
        name: 'userfile',
        autoSubmit: false,
        responseType: 'json',
        onChange: function (file, extension) {
            this.submit();
        },
        onSubmit: function (file, extension) {
            $('#upload-logo').append('<span class="wait"><i class="fa fa-refresh"></i></span>');
        },
        onComplete: function (file, json) {
            if (json.success) {
                $('.wait').remove();
                $('#image-logo').attr('src', json.thumb);
                $('#input-logo').val(json.image);
            }
            if (json.error) {
                alert(json.error);
            }

            $('.loading').remove();
        }
    });

    $('#submit').bind('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=admin_url('system/payments')?>";
                }
            }
        });
    });
</script>
