<div class="body-content" style="text-align:center;">
    <div class="container">
        <div class="terms-conditions-page">
            <div class="row">
                <div class="col-md-12 terms-conditions">
                    <h2 class="heading-title">Anda belum menyelesaikan pembayaran!</h2>
                    <div>
                        <?php
                        switch ($data["payment_type"]) {
                        case "bank_transfer":
                        ?>
                        Pesanan anda dengan <?= $data['payment_method'] ?> telah diterima namun belum dilakukan
                        pembayaran.</br>
                        No. Virtual Account anda : <b><?= $data['payment_code'] ?></b></br>
                        Untuk penjelasan cara pembayaran, silakan klik di <a href="<?= $data['instruction'] ?>"
                                                                             target="_blank">sini<a></br>
                                <?php
					      break;
					      case "echannel"
					      ?>
                                Pesanan anda dengan <?=$data['payment_method']?> telah diterima namun belum dilakukan
                                pembayaran.</br>
                                Kode pembayaran anda : <b><?= $data['payment_code'] ?></b><br>
                                Kode perusahaan : <b><?= $data['company_code'] ?></b></br>
                                Untuk penjelasan cara pembayaran, silakan klik di <a href="<?= $data['instruction'] ?>"
                                                                                     target="_blank">sini<a></br>

                                        <?php
                                        break;
                                        case "cstore"
                                            ?>
                                            Pesanan anda dengan <?= $data['payment_method'] ?> telah diterima namun belum dilakukan pembayaran.</br>
                                            Kode pembayaran anda : <b><?= $data['payment_code'] ?></b></br>
                                            Silahkan datangi <?= $data['store'] ?> terdekat dan tunjukan kode pembayaran ke kasir.</br>
                                            <?php
                                            break;
                                        case "xl_tunai"
                                            ?>
                                            Pesanan anda dengan <?= $data['payment_method'] ?> telah diterima namun proses pembayaran masih dalam status pending.</br>
                                            <?= $data['instruction'] ?>
                                            <?php
                                            break;
                                        case "gopay"
                                            ?>
                                            Pesanan anda dengan <?= $data['payment_method'] ?> telah diterima namun proses pembayaran masih dalam status pending.</br>
                                            <?= $data['instruction'] ?>
                                            <?php
                                            break;
                                        case "bca_klikbca"
                                            ?>
                                            Pesanan anda dengan <?= $data['payment_method'] ?> telah diterima namun proses pembayaran masih dalam status pending.</br>
                                            <?= $data['instruction'] ?>
                                            <?php
                                            break;
                                        case "unfinished"
                                            ?>
                                            <?= $data['instruction'] ?>
                                            <?php
                                            break;
                                        }
                                        ?>
                                        <hr>
                                        <button class="btn btn-primary" onclick="window.location = '<?= site_url() ?>'">
                                            Kembali ke depan
                                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-top:30px;" class="clearfix"></div>
    </div>
</div>