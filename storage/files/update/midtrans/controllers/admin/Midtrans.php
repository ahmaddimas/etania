<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Midtrans extends Admin_Controller
{
    private $manifest = array();

    /**
     * Construction
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->config('payment/midtrans');

        $this->manifest = $this->config->item('manifest');
    }

    /**
     * Module setting
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->load->helper('form');
        $this->load->model('system/setting_model');
        $this->load->model('system/order_status_model');

        $data['order_statuses'] = $this->order_status_model->get_all();
        $data['active'] = false;
        $data['production'] = false;
        $data['display_name'] = '';
        $data['client_key'] = '';
        $data['server_key'] = '';
        $data['total'] = 0;
        $data['logo'] = "";
        $data['note'] = "";
        $data['fees'] = array(0, 0);
        $data['sort_order'] = 0;

        $settings = $this->setting_model->get_settings();

        if (isset($settings['payment_midtrans'])) {
            foreach ($settings['payment_midtrans'] as $key => $value) {
                $data[$key] = $value;
                if ($key == "fee") {
                    $fees = explode('&', $value);
                    $data['fees'] = $fees;
                }
            }
        }

        $data['logo'] = isset($data['logo']) ? $data['logo'] : '';

        $this->load->library('image');

        $data['thumb_logo'] = $this->image->resize($data['logo'] !== '' ? $data['logo'] : 'no_image.jpg', 150, 150);
        $data['no_image'] = $this->image->resize('no_image.jpg', 150, 150);

        $data['action'] = admin_url('payment/midtrans/validate');
        $data['heading_title'] = $this->manifest['name'];

        $this->load->view('admin/midtrans', $data);
    }

    /**
     * Validate configuration
     *
     * @access public
     * @return void
     */
    public function validate()
    {
        $json = array();

        $this->load->library('form_validation');

        $this->form_validation->set_rules('display_name', 'Teks Menu', 'trim|required');
        $this->form_validation->set_rules('sort_order', 'Urutan', 'trim|required|numeric');
        $this->form_validation->set_rules('total', 'Minimal belanja', 'trim|required|numeric');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->get_errors() as $field => $error) {
                $json['errors'][$field] = $error;
            }
        } else {
            $post = $this->input->post();
            $post['fee'] = $post['fees'][0] . '&' . $post['fees'][1];

            if ($this->input->post('active') == null) {
                $post['active'] = 0;
            }

            if ($this->input->post('production') == null) {
                $post['production'] = 0;
            }


            $this->setting_model->edit_setting('payment_midtrans', $post);
            $this->session->set_flashdata('success', 'Berhasil memperbarui modul pembayaran ' . $this->manifest['name']);

            $json['success'] = true;
        }

        $this->output->set_output(json_encode($json));
    }
}