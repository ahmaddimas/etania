<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['manifest'] = array(
    'name' => 'Midtrans',
    'description' => 'Metode pembayaran Midtrans SNAP',
    'version' => '1.1.0'
);