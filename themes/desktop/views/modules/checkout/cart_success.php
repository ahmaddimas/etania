<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h1><?= sprintf(lang('text_thanks'), $this->config->item('company')) ?></h1>
            <div class="alert alert-success"><?= $text ?></div>
            <div class="row">
                <div class="col-md-7">
                    <div class="media">
                        <a class="pull-left" href="<?= $href ?>"><img class="media-object" src="<?= $image ?>"
                                                                      alt="<?= $name ?>"></a>
                        <div class="media-body">
                            <h4 class="media-heading"><?= $name ?></h4>
                            <p><?= cut_text($description, 100) ?></p>
                            <p><strong><?= lang('column_price') ?> : <?= format_money($price) ?></strong></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <?php if (!$option) { ?>
                        <div class="input-group input-group-sm">
						<span class="input-group-btn">
							<a class="btn btn-default button-quantity">-</a>
						</span>
                            <input type="hidden" value="<?= $product_id ?>">
                            <input type="hidden" name="price" id="price" value="<?= $price ?>">
                            <input type="hidden" name="total" id="total" value="<?= $total ?>">
                            <input type="text" class="form-control" value="<?= $quantity ?>" style="text-align:center;"
                                   readonly="readonly">
                            <span class="input-group-btn">
							<a class="btn btn-default button-quantity">+</a>
						</span>
                        </div>
                    <?php } ?>
                    <hr style="margin:2px 0;">
                    <div id="sc" style="background-color:#efefef; padding: 10px 20px;"><?= $shopping_total ?></div>
                    <hr style="margin:2px 0;">
                </div>
            </div>
        </div>
        <div class="modal-footer" style="text-transform:uppercase;">
            <a href="<?= site_url() ?>" class="btn btn-primary" data-dismiss="modal"><i
                        class="fa fa-chevron-left"></i><?= lang('text_shopping') ?></a>
            <a href="<?= site_url('checkout/cart') ?>" class="btn btn-primary"
               id="submit"><?= lang('text_shopping_cart') ?> <i class="fa fa-chevron-right"></i></a>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.button-quantity').on('click', function () {
        var btn = $(this);
        price = $('#price').val();
        total = $('#total').val();

        var oldVal = btn.closest('.input-group').find('input[type=\'text\']').val();
        if (btn.text() == '+') {
            var newVal = parseFloat(oldVal) + 1;
            x = (total * 1) + (price * 1);
            $('#total').val(x);
        } else {
            if (oldVal > 1) {
                if (oldVal == 1) {
                    var newVal = parseFloat(oldVal) - 1;
                    x = (total * 1);
                    $('#total').val(x);
                } else {
                    var newVal = parseFloat(oldVal) - 1;
                    x = (total * 1) - (price * 1);
                    $('#total').val(x);
                }
            } else {
                var newVal = 1;
                x = (total * 1);
                $('#total').val(x);
            }
        }


        var nf = new Intl.NumberFormat();
        x = nf.format(x);

        $('#sc').html('Total Nilai Belanja ( ' + newVal + ' produk ): <h4>Rp ' + x + '</h4>');
        btn.closest('.input-group').find('input[type=\'text\']').val(newVal);
        productId = btn.closest('.input-group').find('input[type=\'hidden\']').val();
        cart.update(productId, newVal);
    });

    $('#button-checkout').on('click', function () {
        $(this).button('loading');
        window.location = "<?=site_url('checkout')?>";
    });
</script>