<h4>Metode Pembayaran</h4><span class="text-muted"><?php echo $text_payment_method; ?></span>
<hr>
<?php if ($error_warning) { ?>
    <div class="warning alert alert-warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<?= form_open($action, 'id="form-pm"') ?>
<div class="form-group">
    <?php
    $no = 1;
    $cp = count($payment_methods);
    ?>
    <?php foreach ($payment_methods

                   as $payment_method) { ?>
    <div class="radio">
        <label>
            <?php
            if ($payment_method['code'] == $code || !$code) {
                ?>
                <?php $code = $payment_method['code']; ?>
                <input type="radio" name="payment_method" onclick="pmethod(<?= $no ?>)"
                       value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>"
                       checked="checked"/>
            <?php } else { ?>
                <input type="radio" name="payment_method" onclick="pmethod(<?= $no ?>)"
                       value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>"/>
            <?php } ?>
            <?php echo $payment_method['title']; ?>
        </label>
    </div>
<?php if ($payment_method['code'] == $code || !$code) { ?>
<?php $code = $payment_method['code']; ?>
    <div style="margin-left:20px; display:block" id="<?= $no ?>">
        <?php } else { ?>
        <div style="margin-left:20px; display:none" id="<?= $no ?>">
            <?php } ?>
            <?php if ($payment_method['code'] == "midtrans") { ?>
                <table style="width:100%">
                    <tbody>
                    <tr>
                        <td style="width:50%">
                            <img style="dispaly:inline; width:150px;" src="<?php echo $payment_method['logo']; ?>"><br>
                            <font color="red"><i><?= $payment_method['note'] ?></i></font>
                            <br>
                        </td>

                    </tr>
                    </tbody>
                </table>

            <?php } else { ?>
                <img style="dispaly:inline; width:75px;" src="<?php echo $payment_method['logo']; ?>"><br>
                No Rekening : <b><?= $payment_method['rekening_number'] ?></b><br>
                Nama : <b><?= $payment_method['rekening_name'] ?></b><br>
                <b>Instruksi :</b><br>
                <?= $payment_method['instruction'] ?><br>

            <?php } ?>
        </div>
        <?php
        $no++;
        }
        ?>
    </div>
    <div class="checkbox">
        <label>Dengan memilih lanjut, maka anda telah menyetujui syarat dan ketentuan yang berlaku</label>
    </div>
    <?= form_close() ?>
    <hr>
    <div>
        <!--<button class="btn btn-primary pull-left" onclick="setSteps(2);"><i class="fa fa-chevron-left"></i> Kembali</button>-->
        <button class="btn btn-primary pull-right" id="button-pm">Lanjut <i class="fa fa-chevron-right"></i></button>
    </div>
    <?php } else { ?>
        <hr>
        <div>
            <button class="btn btn-primary pull-left" onclick="setSteps(3);"><i class="fa fa-chevron-left"></i> Reload
            </button>
        </div>
    <?php } ?>


    <script type="text/javascript">
        function pmethod(no) {
            for (i = 1; i <=<?=$cp?>; i++) {
                $('#' + i).hide();
            }
            $('#' + no).show();
        }
    </script>

