<h4>Metode Pengiriman</h4><span class="text-muted"><?php echo $text_shipping_method; ?></span>
<hr>
<?= form_open($action, 'id="form-sm" class="form-horizontal"') ?>
<div class="form-group">
    <label class="control-label col-md-4">Pilih metode pengiriman:</label>
    <div class="col-md-8">
        <?php foreach ($shipping_methods as $shipping_method) { ?>
            <?php if (!$shipping_method['error']) { ?>
                <select id="shipping_method" name="shipping_method"
                        class="form-control unicase-form-control shipping-method">
                    <?php foreach ($shipping_method['quote'] as $quote) { ?>
                        <?php if ($quote['code'] == $code) { ?>
                            <option value="<?= $quote['code'] ?>" rel="<?= $quote['text'] ?>"
                                    selected="selected"><?= $quote['title'] ?> - <?= $quote['text'] ?></option>
                        <?php } else { ?>
                            <option value="<?= $quote['code'] ?>" rel="<?= $quote['text'] ?>"><?= $quote['title'] ?>
                                - <?= $quote['text'] ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            <?php } else { ?>
                <div class="alert alert-danger"><?= $shipping_method['error'] ?></div>
            <?php } ?>
        <?php } ?>
        <button type="button" class="btn btn-primary" onclick="updateShipping()"><i class="fa fa-refresh"></i></button>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4">Catatan:</label>
    <div class="col-md-8">
        <textarea class="form-control unicase-form-control" name="comment" placeholder="Contoh: warna, ukuran, dsb"
                  rows="5"><?= $comment ?></textarea>
    </div>
</div>
</form>
<hr>
<!--<div>
	<button class="btn btn-primary pull-left" onclick="setSteps(1);"><i class="fa fa-chevron-left"></i> Kembali</button>
	<button class="btn btn-primary pull-right" id="button-sm">Lanjut <i class="fa fa-chevron-right"></i></button>
</div>-->
<script type="text/javascript">
    dgt = $('#shipping_method').val();

    if (dgt == "rajaongkir.free") {
        post();
        setSteps(3);
    }
    $(document).ready(function () {
        $('.shipping-method').on('change', function () {
            post();
        });

        $('.shipping-method').trigger('change');
    });

    function post() {
        $(this).closest('.panel').find('.shipping-cost b').html($(this).find('option:selected').attr('rel'));
        var btn = $('#button-sm');
        $.ajax({
            url: "<?=site_url('checkout/shipping_method')?>",
            type: 'post',
            data: $('#form-sm').serialize(),
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.warning, .error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#shipping-method .checkout-content').prepend('<div class="warning alert alert-warning" style="display:none; margin:15px;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                        $('.warning').fadeIn('slow');
                    }
                } else {
                    $.ajax({
                        url: "<?=site_url('checkout/overview')?>",
                        dataType: 'html',
                        beforeSend: function () {
                            $('#overview-content').html('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
                        },
                        complete: function () {
                            $('#overview-content .loading-state').remove();
                        },
                        success: function (html) {
                            $('#overview-content').html(html);
                        }
                    });
                }
            }
        });
    }
</script>