<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h1><?= sprintf(lang('text_adds'), $this->config->item('company')) ?></h1>
            <div class="alert alert-success"><?= $text ?></div>
            <div class="row">
                <div class="col-md-7">
                    <div class="media">
                        <a class="pull-left" href="<?= $href ?>"><img class="media-object" src="<?= $image ?>"
                                                                      alt="<?= $name ?>"></a>
                    </div>
                </div>
                <div class="col-md-5 product-info product-center">
                    <h4 class="media-heading"><?= $name ?></h4>
                    <p><?= cut_text($description, 100) ?></p>
                    <?= form_open(site_url('checkout/cart/add'), 'id="form-cart"') ?>
                    <div class="price">
                        <?php if ($discount) { ?>
                            <span class="price-old"><?= $price ?></span><br><span
                                    class="price-new"><?= $discount ?></span>
                        <?php } else { ?>
                            <span class="price-new"><?= $price ?></span>
                        <?php } ?>
                    </div>
                    <?php if ($discounts) { ?>
                        <div class="options">
                            <h3><?= lang('text_discount') ?></h3>
                            <table class="table table-bordered">
                                <?php foreach ($discounts as $discount) { ?>
                                    <tr>
                                        <td>&ge; <?= $discount['quantity'] ?></td>
                                        <td><?= $discount['price'] ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } ?>


                    <?php if ($options) { ?>
                    <div class="options">
                        <h2><?= lang('text_option') ?></h2>
                        <?php foreach ($options

                        as $option) { ?>
                        <?php if ($option['type'] == 'select') { ?>
                        <?php if ($option['required']) { ?>
                        <div class="form-group required">
                            <?php } else { ?>
                            <div class="form-group">
                                <?php } ?>
                                <label class="control-label" for="input-option<?= $option['product_option_id'] ?>">
                                    <?= $option['name'] ?>
                                </label>
                                <select name="option[<?= $option['product_option_id'] ?>]"
                                        id="input-option<?= $option['product_option_id'] ?>" class="form-control"
                                        required>
                                    <option value=""><?= lang('text_select') ?></option>
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <option value="<?= $option_value['product_option_value_id'] ?>"><?= $option_value['name'] ?>
                                            <?php if ($option_value['price'] > 0) { ?>
                                                (<?= $option_value['price'] ?>)
                                            <?php } ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <?php }elseif ($option['type'] == 'radio'){ ?>
                        <?php if ($option['required']) { ?>
                            <div class="form-group required">
                                <?php } else { ?>
                                <div class="form-group">
                                    <?php } ?>
                                    <label class="control-label" for="input-option<?= $option['product_option_id'] ?>">
                                        <?= $option['name'] ?>
                                    </label>
                                    <input type="radio" name="option[<?= $option['product_option_id'] ?>]"
                                           id="input-option<?= $option['product_option_id'] ?>" value="" selected="true"
                                           checked="true" style="display:none" required>
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <input type="radio" name="option[<?= $option['product_option_id'] ?>]"
                                               id="input-option<?= $option['product_option_id'] ?>"
                                               value="<?= $option_value['product_option_value_id'] ?>"
                                               required><?= $option_value['name'] ?>
                                        <?php if ($option_value['price'] > 0) { ?>
                                            (<?= $option_value['price'] ?>)
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <?php } ?>


                            <div class="input-group input-group-sm">
						<span class="input-group-btn">
							<a class="btn btn-default button-quantity">-</a>
						</span>
                                <input type="hidden" name="product_id" value="<?= $product_id ?>">
                                <input type="hidden" name="price" id="price" value="<?= $price ?>">
                                <input type="text" name="quantity" class="form-control" value="<?= $quantity ?>"
                                       style="text-align:center;" readonly="readonly">
                                <span class="input-group-btn">
						<a class="btn btn-default button-quantity">+</a>
						<button type="button" value="<?= lang('button_cart') ?>" id="button-cart"
                                rel="<?= $product_id ?>" data-loading-text="<?= lang('text_loading') ?>"
                                class="btn btn-primary"><i class="fa fa-shopping-basket"></i> <?= lang('button_cart') ?></button>
						</span>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="text-transform:uppercase;">
                    <a href="<?= site_url() ?>" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-chevron-left"></i><?= lang('text_shopping') ?></a>
                </div>

            </div>
        </div>
        <script type="text/javascript">
            $('#button-cart').on('click', function () {
                $.ajax({
                    url: $('#form-cart').attr('action'),
                    type: 'post',
                    data: $('#form-cart').serialize(),
                    dataType: 'json',
                    success: function (json) {
                        $('.form-group').removeClass('has-error');
                        $('.text-danger').remove();
                        if (json['redirect']) {
                            location = json['redirect'];
                        }
                        if (json['success']) {
                            $("#cart-modal").remove();

                            $('body').append('<div class="modal fade" id="cart-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['success'] + '</div>');
                            $('#cart-modal').modal('show');
                            $('#cart-modal').on('hidden.bs.modal', function (e) {
                                $('#cart-modal').remove();
                                $('.modal-backdrop').remove();
                            });


                            $('#cart-block .cart-content').load('checkout/cart/info #cart-content-ajax');
                            $('#cart-block .cart-count').html(json['total']);
                        } else if (json['error']['option']) {
                            for (i in json['error']['option']) {
                                $('#input-option' + i).closest('.form-group').addClass('has-error');
                                $('#input-option' + i).after('<small class="text-danger"><i>' + json['error']['option'][i] + '</i></small>');
                            }
                        }
                    }
                });
            });


            $('.button-quantity').on('click', function () {
                var btn = $(this);
                price = $('#price').val();
                total = $('#total').val();

                var oldVal = btn.closest('.input-group').find('input[type=\'text\']').val();
                if (btn.text() == '+') {
                    var newVal = parseFloat(oldVal) + 1;
                    x = (total * 1) + (price * 1);
                    $('#total').val(x);
                } else {
                    if (oldVal > 1) {
                        if (oldVal == 1) {
                            var newVal = parseFloat(oldVal) - 1;
                            x = (total * 1);
                            $('#total').val(x);
                        } else {
                            var newVal = parseFloat(oldVal) - 1;
                            x = (total * 1) - (price * 1);
                            $('#total').val(x);
                        }
                    } else {
                        var newVal = 1;
                        x = (total * 1);
                        $('#total').val(x);
                    }
                }


                var nf = new Intl.NumberFormat();
                x = nf.format(x);

                $('#sc').html('Total Nilai Belanja ( ' + newVal + ' produk ): <h4>Rp ' + x + '</h4>');
                btn.closest('.input-group').find('input[type=\'text\']').val(newVal);
                productId = btn.closest('.input-group').find('input[type=\'hidden\']').val();
                // cart.update(productId, newVal);
            });

            $('#button-checkout').on('click', function () {
                $(this).button('loading');
                window.location = "<?=site_url('checkout')?>";
            });
        </script>