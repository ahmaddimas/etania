<div class="breadcrumb full-width">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="title-page"><?= $template['title'] ?></h1>
                            <ul>
                                <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                                    <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background-content"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 center-column content-with-background" id="content">
                                <?php if ($error) { ?>
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                            &times;
                                        </button><?= $error ?></div>
                                <?php } ?>
                                <?php if ($products) { ?>
                                    <?= form_open($action, 'id="form-cart"') ?>
                                    <div class="table-responsive cart-info">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <td class="text-center"></td>
                                                <td class="text-center"><?= lang('column_image') ?></td>
                                                <td class="text-center hidden-xs"><?= lang('column_name') ?></td>
                                                <td class="text-center hidden-xs"><?= lang('column_model') ?></td>
                                                <td class="text-center"><?= lang('column_quantity') ?></td>
                                                <td class="text-right hidden-xs"><?= lang('column_price') ?></td>
                                                <td class="text-right"><?= lang('column_total') ?></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($products as $key => $product) { ?>
                                                <tr>
                                                    <td class="romove-item"><a style="cursor:pointer;"
                                                                               onclick="cart.remove('<?= $key ?>');"
                                                                               title="Keluarkan"><i
                                                                    class="fa fa-trash-o fa-2x"></i></a></td>
                                                    <td class="text-center">
                                                        <a href="<?= $product['href'] ?>"><img
                                                                    src="<?= $product['thumb'] ?>"
                                                                    alt="<?= $product['name'] ?>"
                                                                    title="<?= $product['name'] ?>"
                                                                    class="img-thumbnail"/></a>
                                                        <div class="visible-xs"><a
                                                                    href="<?= $product['href'] ?>"><?= $product['name'] ?>
                                                                <div>
                                                    </td>
                                                    <td class="text-center hidden-xs">
                                                        <a href="<?= $product['href'] ?>"><?= $product['name'] ?></a>
                                                        <?php if (!$product['stock']) { ?>
                                                            <br><span class="error">***</span>
                                                        <?php } ?>
                                                        <br/>
                                                        <?php if ($product['option']) { ?>
                                                            <?php foreach ($product['option'] as $option) { ?>
                                                                <small><?= $option['name'] ?>
                                                                    : <?= $option['option_value'] ?></small>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </td>
                                                    <td class="text-center hidden-xs"><?= $product['model'] ?></td>
                                                    <td class="text-center">
                                                        <div class="quant-input">
                                                            <div class="arrows">
                                                                <div class="arrow plus gradient"><span
                                                                            class="ir button-quantity" rel="+"><i
                                                                                class="icon fa fa-sort-asc"></i></span>
                                                                </div>
                                                                <div class="arrow minus gradient"><span
                                                                            class="ir button-quantity" rel="-"><i
                                                                                class="icon fa fa-sort-desc"></i></span>
                                                                </div>
                                                            </div>
                                                            <input type="text" value="<?= $product['quantity'] ?>"
                                                                   readonly="readonly">
                                                            <input type="hidden" value="<?= $key ?>">
                                                        </div>
                                                    </td>
                                                    <td class="text-right hidden-xs"><?= $product['price'] ?></td>
                                                    <td class="text-right"><?= $product['total'] ?></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    </form>
                                    <div class="cart-total">
                                        <table>
                                            <?php foreach ($totals as $total) { ?>
                                                <tr>
                                                    <td class="text-right"><strong><?= $total['title'] ?></strong></td>
                                                    <td class="text-right"><?= $total['text'] ?></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                    <div class="buttons">
                                        <div class="pull-left"><a href="<?= site_url() ?>"
                                                                  class="btn btn-default"><?= lang('button_continue') ?></a>
                                        </div>
                                        <div class="pull-right"><a href="<?= site_url('checkout') ?>"
                                                                   class="btn btn-primary"><?= lang('button_checkout') ?></a>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <p><?= lang('error_product') ?></p>
                                    <div class="buttons">
                                        <div class="pull-right"><a href="<?= site_url() ?>"
                                                                   class="btn btn-default"><?= lang('button_continue') ?></a>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.button-quantity').on('click', function () {
            var btn = $(this);
            var oldVal = btn.closest('.quant-input').find('input[type=\'text\']').val();
            if (btn.attr('rel') == '+') {
                var newVal = parseFloat(oldVal) + 1;
            } else {
                if (oldVal > 1) {
                    var newVal = parseFloat(oldVal) - 1;
                } else {
                    var newVal = 1;
                }
            }

            btn.closest('.quant-input').find('input[type=\'text\']').val(newVal);
            productKey = btn.closest('.quant-input').find('input[type=\'hidden\']').val();
            cart.update(productKey, newVal);
        });
    </script>