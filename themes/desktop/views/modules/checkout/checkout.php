<div class="breadcrumb full-width">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="title-page"><?= $template['title'] ?></h1>
                            <ul>
                                <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                                    <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8 center-column content-with-background" id="content">
                                <div class="panel-group checkout-steps">
                                    <div class="panel panel-default">
                                        <div class="status alert alert-success" style="display: none"></div>
                                        <div id="step-1" class="">
                                            <div class="col-md-12" id="shipping-address"></div>
                                        </div>
                                        <div id="step-2" class="">
                                            <div class="col-md-12" id="shipping-method"></div>
                                        </div>
                                        <div id="step-3" class="">
                                            <div class="col-md-12" id="payment-method"></div>
                                        </div>
                                        <div id="checkout-confirm" class="modal fade" role="dialog" tabindex="-1">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">&times;
                                                        </button>
                                                        <h3>Confirm</h3>
                                                    </div>
                                                    <div class="modal-body"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" id="column-right">
                                <div id="overview-content"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        setSteps(1);
        setSteps(2);
        setSteps(3);
        updateOverview();
    });

    function setSteps(num) {
        if (num == 1) {
            $.ajax({
                url: "<?=site_url('checkout/shipping_address')?>",
                dataType: 'html',
                beforeSend: function () {
                    $('#shipping-address').html('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
                },
                complete: function () {
                    $('#shipping-address .loading-state').remove();
                },
                success: function (html) {
                    $('#shipping-address').html(html);
                }
            });
        } else if (num == 2) {
            $.ajax({
                url: "<?=site_url('checkout/shipping_method')?>",
                dataType: 'html',
                beforeSend: function () {
                    $('#shipping-method').html('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
                },
                complete: function () {
                    $('#shipping-method .loading-state').remove();
                },
                success: function (html) {
                    $('#shipping-method').html(html);
                }
            });
        } else if (num == 3) {
            $.ajax({
                url: "<?=site_url('checkout/payment_method')?>",
                dataType: 'html',
                beforeSend: function () {
                    $('#payment-method').html('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
                },
                complete: function () {
                    $('#payment-method .loading-state').remove();
                },
                success: function (html) {
                    $('#payment-method').html(html);
                }
            });
        } else if (num == 4) {
            $.ajax({
                url: "<?=site_url('checkout/confirm')?>",
                dataType: 'html',
                beforeSend: function () {
                    $('#checkout-confirm').parent().append('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
                },
                complete: function () {
                    $('#checkout-confirm').parent().find('.loading-state').remove();
                },
                success: function (html) {
                    $('#checkout-confirm').find('.modal-body').html(html);
                    $('#checkout-confirm').modal('show');
                }
            });
        }
    }

    function updateOverview() {
        $.ajax({
            url: "<?=site_url('checkout/overview')?>",
            dataType: 'html',
            beforeSend: function () {
                $('#overview-content').html('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
            },
            complete: function () {
                $('#overview-content .loading-state').remove();
            },
            success: function (html) {
                $('#overview-content').html(html);
            }
        });
    }

    function updateShipping() {
        $.ajax({
            url: $('#form-sa').attr('action'),
            type: 'post',
            data: $('#form-sa').serialize(),
            dataType: 'json',
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                $('.alert-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#shipping-address').prepend('<div class="warning alert alert-warning" style="display:none; margin:15px;">' + json['error']['warning'] + '</div>');
                        $('.warning').fadeIn('slow');
                    }
                    for (i in json['error']) {
                        $('#shipping-address input[name=' + i + ']').closest('.form-group').addClass('has-error');
                        $('#shipping-address input[name=' + i + ']').after('<small class="text-danger">' + json['error'][i] + '</small>');
                    }
                } else {
                    setSteps(2);
                }
            }
        });
    }

    function submitMethod() {
        $.ajax({
            url: "<?=site_url('checkout/shipping_method')?>",
            type: 'post',
            data: $('#shipping-method #form-sm').serialize(),
            async: false,
            dataType: 'json',
            success: function (json) {
                $('.warning, .error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#shipping-method .checkout-content').prepend('<div class="warning alert alert-warning" style="display:none; margin:15px;">' + json['error']['warning'] + '</div>');
                        $('.warning').fadeIn('slow');
                    }
                }
            }
        });
    }

    $('#payment-method').on('click', '#button-pm', function () {
        submitMethod();
        var btn = $(this);
        $.ajax({
            url: "<?=site_url('checkout/payment_method')?>",
            type: 'post',
            data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'hidden\'], #payment-method textarea'),
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.warning, .error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#payment-method').prepend('<div class="warning alert alert-danger" style="display:none;">' + json['error']['warning'] + '</div>');
                        $('html, body').animate({scrollTop: 0}, 'slow');
                        $('.warning').fadeIn('slow');
                    }
                } else {
                    setSteps(4);
                }
            }
        });
    });
</script> 