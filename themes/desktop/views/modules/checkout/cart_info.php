<li class="dropdown" id="cart-block">
    <a href="#" class="dropdown-toggle-cart" data-toggle="dropdown"><span
                class="badge badge-primary cart-count"><?= $total ?></span><i class="fa fa-shopping-bag icon"></i></a>
    <ul class="dropdown-menu">
        <div class="cart-content">
            <?php if ($products) { ?>
                <li id="cart-content-ajax">
                    <?php foreach ($products as $product) { ?>
                        <div class="cart-item">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="image"><a href="<?= $product['href'] ?>"><img
                                                    src="<?= $product['thumb'] ?>" alt="<?= $product['name'] ?>"></a>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="price">
                                        <strong class="name"><?= $product['name'] ?></strong><br>
                                        <?php if ($product['option']) { ?>
                                            <?php foreach ($product['option'] as $option) { ?>
                                                <small><?= $option['name'] ?> : <?= $option['option_value'] ?></small>
                                                <br>
                                            <?php } ?>
                                        <?php } ?>
                                        <?= $product['quantity'] ?> &times; <?= $product['price'] ?>
                                        <span style="cursor:pointer;" class="pull-right"
                                              onclick="cart.remove('<?= $product['key'] ?>');"><i
                                                    class="fa fa-remove fa-lg"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                    <?php } ?>
                    <div class="clearfix">
                        <button class="btn btn-warning btn-block"
                                onclick="window.location = '<?= site_url('checkout/cart') ?>';"
                                style="margin-bottom:5px;"><?= lang('button_shopping_cart') ?></button>
                        <button class="btn btn-primary btn-block"
                                onclick="window.location = '<?= site_url('checkout/checkout') ?>';"><?= lang('button_checkout') ?></button>
                    </div>
                </li>
            <?php } else { ?>
                <li id="cart-content-ajax">
                    <div class="clearfix cart-total" style="text-align:center;">
                        <p><?= lang('text_empty') ?></p>
                    </div>
                </li>
            <?php } ?>
        </div>
    </ul>
</li>