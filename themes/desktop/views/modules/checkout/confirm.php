<div class="table-responsive">
    <table class="table table-bordered" width="100%">
        <thead>
        <tr>
            <th><?= lang('column_name') ?></th>
            <th class="text-right"><?= lang('column_quantity') ?></th>
            <th class="text-right"><?= lang('column_price') ?></th>
            <th class="text-right"><?= lang('column_total') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($products as $product) { ?>
            <tr>
                <td>
                    <?= $product['name'] ?>
                    <?php if ($product['option']) { ?>
                        <?php foreach ($product['option'] as $option) { ?>
                            <br>
                            <small><?= $option['name'] ?> : <?= $option['option_value'] ?></small>
                        <?php } ?>
                    <?php } ?>
                </td>
                <td class="text-right"><?= $product['quantity'] ?></td>
                <td class="text-right"><?= $this->currency->format($product['price']) ?></td>
                <td class="text-right"><?= $this->currency->format($product['total']) ?></td>
            </tr>
        <?php } ?>
        </tbody>
        <tbody>
        <?php foreach ($totals as $total) { ?>
            <tr>
                <td colspan="3"><strong><?= $total['title'] ?></strong></td>
                <td class="text-right"><strong><?= $total['text'] ?></strong></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<p><strong><?= lang('text_comment') ?>:</strong></p>
<p><?= $comment ?></p>
<hr>
<div class="row">
    <div class="col-md-12">
        <?= $payment ?>
    </div>
</div>