<style>dl dt {
        font-weight: bold;
    }

    dl dd {
        margin-bottom: 10px;
    }</style>
<div class="breadcrumb full-width">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="title-page"><?= $template['title'] ?></h1>
                            <ul>
                                <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                                    <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3" id="column-right">
                                <?= Modules::run('customer/navigation/index') ?>
                            </div>
                            <div class="col-md-9 center-column content-with-background" id="content">
                                <div id="message"></div>
                                <legend><h4><?= lang('text_item') ?></h4></legend>
                                <div class="table-responsive">
                                    <table class="table table-bordered" width="100%">
                                        <thead>
                                        <tr>
                                            <th><?= lang('column_name') ?></th>
                                            <th class="text-right"><?= lang('column_quantity') ?></th>
                                            <th class="text-right"><?= lang('column_price') ?></th>
                                            <th class="text-right"><?= lang('column_total') ?></th>
                                            <th class="text-right">Download</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($products as $product) { ?>
                                            <tr>
                                                <td>
                                                    <?= $product['name'] ?>
                                                    <?php if ($product['option']) { ?>
                                                        <p>
                                                            <?php foreach ($product['option'] as $option) { ?>
                                                                <small><i><?= $option['name'] ?>
                                                                        : <?= $option['value'] ?></i></small><br>
                                                            <?php } ?>
                                                        </p>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-right"><?= $product['quantity'] ?></td>
                                                <td class="text-right"><?= $product['price'] ?></td>
                                                <td class="text-right"><?= $product['total'] ?></td>
                                                <td class="">

                                                    <?php
                                                    if (count($product['link']) == 0) {
                                                        echo "Waiting for confirm";
                                                    } else {

                                                        $nod = 1;
                                                        foreach ($product['link'] as $link) {
                                                            ?>
                                                            <?= $nod ?>. <a target="_blank"
                                                                            href="<?= site_url($link['file']) ?>">Download</a>
                                                            <br>
                                                            <?php
                                                            $nod++;
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                        <tbody>
                                        <?php foreach ($totals as $total) { ?>
                                            <tr>
                                                <td colspan="3"><strong><?= $total['title'] ?></strong></td>
                                                <td class="text-right"><strong><?= $total['text'] ?></strong></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=site_url('customer/order/history')?>",
                "type": "POST",
                "data": function (d) {
                    d.order_id = "<?=$order_id?>";
                }
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "date_added"},
                {"orderable": false, "searchable": false, "data": "status"},
                {"orderable": false, "searchable": false, "data": "comment"},
            ],
            "sDom": '<"table-responsive" t><"pagination-results" p>',
            "order": [[0, 'desc']],
        });
    });

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }

    function confirmAccept() {
        $('#review-modal').modal('show');
    }

    function validatereview() {
        $.ajax({
            url: $('#form-review').attr('action'),
            type: 'post',
            data: $('#form-review').serialize(),
            dataType: 'json',
            success: function (json) {
                $('.error').remove();
                if (json['error']) {
                    $('#form-review').find('textarea').after('<i class="error text-danger">' + json['error']['text'] + '</i>');
                } else if (json['redirect']) {
                    window.location = json['redirect'];
                }
            }
        });
    }

    $('#track').on('click', function () {
        var btn = $(this);

        $.ajax({
            url: "<?=site_url('tool/shipping/track')?>",
            data: 'order_id=<?=$order_id?>',
            dataType: 'html',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (html) {
                $('body').append('<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">' + html + '</div>');
                $('#modal').modal('show');
                $('#modal').on('hidden.bs.modal', function (e) {
                    $('#modal').remove();
                });
            }
        });
    });
</script>
<script>
    var __slice = [].slice;

    (function ($, window) {
        var Starrr;

        Starrr = (function () {
            Starrr.prototype.defaults = {
                rating: void 0,
                numStars: 5,
                change: function (e, value) {
                }
            };

            function Starrr($el, options) {
                var i, _, _ref,
                    _this = this;

                this.options = $.extend({}, this.defaults, options);
                this.$el = $el;
                _ref = this.defaults;
                for (i in _ref) {
                    _ = _ref[i];
                    if (this.$el.data(i) != null) {
                        this.options[i] = this.$el.data(i);
                    }
                }
                this.createStars();
                this.syncRating();
                this.$el.on('mouseover.starrr', 'span', function (e) {
                    return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
                });
                this.$el.on('mouseout.starrr', function () {
                    return _this.syncRating();
                });
                this.$el.on('click.starrr', 'span', function (e) {
                    return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
                });
                this.$el.on('starrr:change', this.options.change);
            }

            Starrr.prototype.createStars = function () {
                var _i, _ref, _results;

                _results = [];
                for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                    _results.push(this.$el.append("<span class='fa fa-star-o fa-2x' style='color:gray;'></span>"));
                }
                return _results;
            };

            Starrr.prototype.setRating = function (rating) {
                if (this.options.rating === rating) {
                    rating = void 0;
                }
                this.options.rating = rating;
                this.syncRating();
                return this.$el.trigger('starrr:change', rating);
            };

            Starrr.prototype.syncRating = function (rating) {
                var i, _i, _j, _ref;

                rating || (rating = this.options.rating);
                if (rating) {
                    for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                        this.$el.find('span').eq(i).removeClass('fa-star-o').addClass('fa-star').css('color', 'gold');
                    }
                }
                if (rating && rating < 5) {
                    for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                        this.$el.find('span').eq(i).removeClass('fa-star').addClass('fa-star-o').css('color', 'gray');
                    }
                }
                if (!rating) {
                    return this.$el.find('span').removeClass('fa-star').addClass('fa-star-o').css('color', 'gray');
                }
            };

            return Starrr;

        })();
        return $.fn.extend({
            starrr: function () {
                var args, option;

                option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                return this.each(function () {
                    var data;

                    data = $(this).data('star-rating');
                    if (!data) {
                        $(this).data('star-rating', (data = new Starrr($(this), option)));
                    }
                    if (typeof option === 'string') {
                        return data[option].apply(data, args);
                    }
                });
            }
        });
    })(window.jQuery, window);

    $(function () {
        return $(".starrr").starrr();
    });

    $(document).ready(function () {
        $('#rating-rate').on('starrr:change', function (e, value) {
            $('input[name=\'rating\']').val(value);
        });
    });
</script>