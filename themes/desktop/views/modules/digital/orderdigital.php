<div class="breadcrumb full-width">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="title-page"><?= $template['title'] ?></h1>
                            <ul>
                                <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                                    <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3" id="column-right">
                                <?= Modules::run('customer/navigation/index') ?>
                            </div>
                            <div class="col-md-9 center-column content-with-background" id="content">
                                <div id="message"></div>
                                <table class="table table-hover" width="100%" id="datatable">
                                    <thead>
                                    <tr>
                                        <th><?= lang('column_order_id') ?></th>
                                        <th><?= lang('column_invoice') ?></th>
                                        <th><?= lang('column_date') ?></th>
                                        <th><?= lang('column_total') ?></th>
                                        <th><?= lang('column_status') ?></th>
                                        <th class="text-right">File</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=site_url('digital')?>",
                "type": "POST",
            },
            "columns": [
                {"data": "order_id"},
                {"data": "invoice"},
                {"data": "date_added"},
                {"data": "total"},
                {"data": "status"},
                {"orderable": false, "searchable": false, "data": "order_id"},
            ],
            "sDom": '<"table-responsive" t><"pagination-results" p>',
            "createdRow": function (row, data, index) {
                if (data.status == "Pending" || data.status == "Belum Lunas") {
                    $('td', row).eq(5).addClass('text-right').html('<a disabled class="btn btn-default">Unpaid</a>');
                } else {
                    $('td', row).eq(5).addClass('text-right').html('<a href="<?=site_url('digital/detail/\'+data.order_id+\'')?>" class="btn btn-default">Download</a>');
                }
            },
            "order": [[0, 'desc']]
        });
    });

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }
</script>