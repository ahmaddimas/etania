<div class="breadcrumb full-width">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="title-page"><?= $name ?></h1>
                            <ul>
                                <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                                    <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 center-column content-without-background" id="content">
                                <div>
                                    <div class="product-info">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row" id="quickview_product">
                                                    <script>
                                                        $(document).ready(function () {
                                                            if ($(window).width() > 992) {
                                                                $('#image').elevateZoom({
                                                                    zoomType: "inner",
                                                                    cursor: "pointer",
                                                                    zoomWindowFadeIn: 500,
                                                                    zoomWindowFadeOut: 750
                                                                });
                                                                var z_index = 0;
                                                                $(document).on('click', '.open-popup-image', function () {
                                                                    $('.popup-gallery').magnificPopup('open', z_index);
                                                                    return false;
                                                                });
                                                                $('.thumbnails a, .thumbnails-carousel a').click(function () {
                                                                    var smallImage = $(this).attr('data-image');
                                                                    var largeImage = $(this).attr('data-zoom-image');
                                                                    var ez = $('#image').data('elevateZoom');
                                                                    $('#img-1').attr('href', largeImage);
                                                                    ez.swaptheimage(smallImage, largeImage);
                                                                    z_index = $(this).index('.thumbnails a, .thumbnails-carousel a');
                                                                    return false;
                                                                });
                                                            } else {
                                                                $(document).on('click', '.open-popup-image', function () {
                                                                    $('.popup-gallery').magnificPopup('open', 0);
                                                                    return false;
                                                                });
                                                            }
                                                        });
                                                    </script>
                                                    <div class="col-sm-7 popup-gallery">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="product-image inner-cloud-zoom"><a
                                                                            href="<?= $popup ?>" title="<?= $name ?>"
                                                                            id="img-1" class="open-popup-image"><img
                                                                                src="<?= $thumb ?>" title="<?= $name ?>"
                                                                                alt="<?= $name ?>" id="image"
                                                                                data-zoom-image="<?= $popup ?>"/></a>
                                                                </div>
                                                            </div>
                                                            <?php if ($images) { ?>
                                                                <div class="col-sm-12">
                                                                    <div class="overflow-thumbnails-carousel">
                                                                        <div class="thumbnails-carousel owl-carousel">
                                                                            <?php foreach ($images as $image) { ?>
                                                                                <div class="item"><a
                                                                                            href="<?= $image['popup'] ?>"
                                                                                            class="popup-image"
                                                                                            data-image="<?= $image['thumb'] ?>"
                                                                                            data-zoom-image="<?= $image['popup'] ?>"><img
                                                                                                src="<?= $image['thumb'] ?>"
                                                                                                title="<?= $name ?>"
                                                                                                alt="<?= $name ?>"/></a>
                                                                                </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                        $(document).ready(function () {
                                                                            $(".thumbnails-carousel").owlCarousel({
                                                                                autoPlay: 6000,
                                                                                navigation: true,
                                                                                navigationText: ['', ''],
                                                                                itemsCustom: [
                                                                                    [0, 4],
                                                                                    [450, 5],
                                                                                    [550, 6],
                                                                                    [768, 4]
                                                                                ],
                                                                            });
                                                                        });
                                                                    </script>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 product-center clearfix">
                                                        <div>
                                                            <div class="review">
                                                                <div class="rating">
                                                                    <?php $star = ''; ?>
                                                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                        <?php if ($i <= $rating) { ?>
                                                                            <?php $star .= '<i class="fa fa-star active"></i>'; ?>
                                                                        <?php } else { ?>
                                                                            <?php $star .= '<i class="fa fa-star"></i>'; ?>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                    <?= $star ?>
                                                                    (<?= sprintf(lang('text_reviews'), (int)$reviews) ?>
                                                                    )
                                                                </div>
                                                            </div>
                                                            <div class="description">
                                                                <span><?= lang('text_model') ?></span>
                                                                <?= $model ?>
                                                                <br/><span><?= lang('text_stock') ?></span>
                                                                <?= $stock_status ?>
                                                            </div>
                                                            <div class="price">
                                                                <?php if ($discount) { ?>
                                                                    <span class="price-old"><?= $price ?></span><br>
                                                                    <span class="price-new"><?= $discount ?></span>
                                                                <?php } else { ?>
                                                                    <span class="price-new"><?= $price ?></span>
                                                                <?php } ?>
                                                            </div>
                                                            <?php if ($discounts) { ?>
                                                                <div class="options">
                                                                    <h3><?= lang('text_discount') ?></h3>
                                                                    <table class="table table-bordered">
                                                                        <?php foreach ($discounts as $discount) { ?>
                                                                            <tr>
                                                                                <td>
                                                                                    &ge; <?= $discount['quantity'] ?></td>
                                                                                <td><?= $discount['price'] ?></td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    </table>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <?= form_open(site_url('checkout/cart/add'), 'id="form-cart"') ?>
                                                        <div id="product">
                                                            <?php if ($options) { ?>
                                                            <div class="options">
                                                                <h2><?= lang('text_option') ?></h2>
                                                                <?php foreach ($options

                                                                as $option) { ?>
                                                                <?php if ($option['type'] == 'select') { ?>
                                                                <?php if ($option['required']) { ?>
                                                                <div class="form-group required">
                                                                    <?php } else { ?>
                                                                    <div class="form-group">
                                                                        <?php } ?>
                                                                        <label class="control-label"
                                                                               for="input-option<?= $option['product_option_id'] ?>">
                                                                            <?= $option['name'] ?>
                                                                        </label>
                                                                        <select name="option[<?= $option['product_option_id'] ?>]"
                                                                                id="input-option<?= $option['product_option_id'] ?>"
                                                                                class="form-control" required>
                                                                            <option value=""><?= lang('text_select') ?></option>
                                                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                                <option value="<?= $option_value['product_option_value_id'] ?>"><?= $option_value['name'] ?>
                                                                                    <?php if ($option_value['price'] > 0) { ?>
                                                                                        (<?= $option_value['price'] ?>)
                                                                                    <?php } ?>
                                                                                </option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    <?php }elseif ($option['type'] == 'radio'){ ?>
                                                                <?php if ($option['required']) { ?>
                                                                    <div class="form-group required">
                                                                        <?php } else { ?>
                                                                        <div class="form-group">
                                                                            <?php } ?>
                                                                            <label class="control-label"
                                                                                   for="input-option<?= $option['product_option_id'] ?>">
                                                                                <?= $option['name'] ?>
                                                                            </label>
                                                                            <input type="radio"
                                                                                   name="option[<?= $option['product_option_id'] ?>]"
                                                                                   id="input-option<?= $option['product_option_id'] ?>"
                                                                                   value="" selected="true"
                                                                                   checked="true" style="display:none"
                                                                                   required>
                                                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                                <input type="radio"
                                                                                       name="option[<?= $option['product_option_id'] ?>]"
                                                                                       id="input-option<?= $option['product_option_id'] ?>"
                                                                                       value="<?= $option_value['product_option_value_id'] ?>"
                                                                                       required><?= $option_value['name'] ?>
                                                                                <?php if ($option_value['price'] > 0) { ?>
                                                                                    (<?= $option_value['price'] ?>)
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="cart">
                                                                        <input type="hidden" name="product_id"
                                                                               value="<?= $product_id ?>"/>
                                                                        <div style="padding:30px;">
                                                                            <div class="input-group input-group-lg">
																			<span class="input-group-btn">
																				<a class="btn btn-default button-quantity">-</a>
																			</span>
                                                                                <input name="quantity" type="text"
                                                                                       class="form-control" value="1"
                                                                                       style="text-align:center;"
                                                                                       readonly="readonly">
                                                                                <span class="input-group-btn">
																				<a class="btn btn-default button-quantity">+</a>
																				<button type="button"
                                                                                        value="<?= lang('button_cart') ?>"
                                                                                        id="button-cart"
                                                                                        rel="<?= $product_id ?>"
                                                                                        data-loading-text="<?= lang('text_loading') ?>"
                                                                                        class="btn btn-primary"><i
                                                                                            class="fa fa-shopping-basket"></i> <?= lang('button_cart') ?></button>
																			</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="options">
                                                                    <?= $this->config->item('sharing_button_code') ?>
                                                                </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="tabs" class="htabs">
                                                <a href="#tab-description"
                                                   data-toggle="tab"><?= lang('tab_description') ?></a>
                                                <a href="#tab-attribute"
                                                   data-toggle="tab"><?= lang('tab_attribute') ?></a>
                                                <?php foreach ($product_tabs as $key => $product_tab) { ?>
                                                    <a href="#tab-custom-<?= $key ?>"
                                                       data-toggle="tab"><?= $product_tab['title'] ?></a>
                                                <?php } ?>
                                                <a href="#tab-review"
                                                   data-toggle="tab"><?= sprintf(lang('tab_review'), (int)$reviews) ?></a>
                                            </div>
                                            <div id="tab-description" class="tab-content">
                                                <?= $description ?>
                                            </div>
                                            <div id="tab-attribute" class="tab-content">
                                                <?php if ($attribute_groups) { ?>
                                                    <table class="table table-bordered">
                                                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                                                            <thead>
                                                            <tr>
                                                                <th colspan="2"
                                                                    style="background:#f6f6f6;"><?= $attribute_group['name'] ?></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                                <tr>
                                                                    <td><?= $attribute['name'] ?></td>
                                                                    <td><?= $attribute['text'] ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        <?php } ?>
                                                    </table>
                                                <?php } ?>
                                            </div>
                                            <?php foreach ($product_tabs as $key => $product_tab) { ?>
                                                <div id="tab-custom-<?= $key ?>"
                                                     class="tab-content"><?= $product_tab['content'] ?></div>
                                            <?php } ?>
                                            <div id="tab-review" class="tab-content"></div>
                                        </div>
                                        <script type="text/javascript">
                                            $.fn.tabs = function () {
                                                var selector = this;
                                                this.each(function () {
                                                    var obj = $(this);
                                                    $(obj.attr('href')).hide();
                                                    $(obj).click(function () {
                                                        $(selector).removeClass('selected');
                                                        $(selector).each(function (i, element) {
                                                            $($(element).attr('href')).hide();
                                                        });
                                                        $(this).addClass('selected');
                                                        $($(this).attr('href')).show();
                                                        return false;
                                                    });
                                                });
                                                $(this).show();
                                                $(this).first().click();
                                            };
                                        </script>
                                        <script type="text/javascript">
                                            $('#tabs a').tabs();
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="filter-product">
                                    <div class="filter-tabs">
                                        <div class="bg-filter-tabs">
                                            <div class="bg-filter-tabs2 clearfix">
                                                <ul id="tab01">
                                                    <li class="active"><a
                                                                href="#products-0"><?= lang('text_related') ?></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content clearfix"
                                         style="background:none;padding:20px 0;border:none;">
                                        <div class="tab-pane active" id="products-0">
                                            <a class="next-button" href="#carousel-tab-0"
                                               id="carousel-tab-0_next"><span></span></a>
                                            <a class="prev-button" href="#carousel-tab-0"
                                               id="carousel-tab-0_prev"><span></span></a>
                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    var owlObject = $(".filter-product #carousel-tab-0 .carousel-inner");

                                                    $("#carousel-tab-0_next").click(function () {
                                                        owlObject.trigger('owl.next');
                                                        return false;
                                                    });
                                                    $("#carousel-tab-0_prev").click(function () {
                                                        owlObject.trigger('owl.prev');
                                                        return false;
                                                    });

                                                    owlObject.owlCarousel({
                                                        slideSpeed: 500,
                                                        singleItem: true
                                                    });
                                                });
                                            </script>
                                            <div class="box-product">
                                                <div id="carousel-tab-0" class="carousel slide">
                                                    <div class="carousel-inner">
                                                        <?php if (count($relates) > 6) { ?>
                                                            <?php foreach (array_chunk($relates, 6) as $products) { ?>
                                                                <div class="item active">
                                                                    <div class="product-grid">
                                                                        <div class="row">
                                                                            <?php foreach ($products as $product) { ?>
                                                                                <div class="col-sm-2 col-xs-6 col-md-25 col-lg-2 col-sm-3">
                                                                                    <div class="product clearfix product-hover">
                                                                                        <div class="left">
                                                                                            <div class="image ">
                                                                                                <?php if ($product['discount_percent']) { ?>
                                                                                                    <div class="sale"><?= $product['discount_percent'] ?></div>
                                                                                                <?php } ?>
                                                                                                <?php if ($product['wholesaler']) { ?>
                                                                                                    <div class="new">
                                                                                                        GROSIR
                                                                                                    </div>
                                                                                                <?php } ?>
                                                                                                <a href="<?= $product['href'] ?>">
                                                                                                    <img src="<?= $product['thumb'] ?>"
                                                                                                         alt="<?= $product['name'] ?>"
                                                                                                         class="<?= $product['name'] ?>"/>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="rating">
                                                                                                <?php $star = ''; ?>
                                                                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                                                    <?php if ($i <= $product['rating']) { ?>
                                                                                                        <?php $star .= '<i class="fa fa-star active"></i>'; ?>
                                                                                                    <?php } else { ?>
                                                                                                        <?php $star .= '<i class="fa fa-star"></i>'; ?>
                                                                                                    <?php } ?>
                                                                                                <?php } ?>
                                                                                                <?= $star ?>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="right">
                                                                                            <div class="name"><a
                                                                                                        href="<?= $product['href'] ?>"><?= cut_text($product['name'], 48) ?></a>
                                                                                            </div>
                                                                                            <div class="price">
                                                                                                <?php if ($product['discount']) { ?>
                                                                                                    <span class="price-old"><?= $product['price'] ?></span>
                                                                                                    <br><span
                                                                                                            class="price-new"><?= $product['discount'] ?></span>
                                                                                                <?php } else { ?>
                                                                                                    <span class="price-new"><?= $product['price'] ?></span>
                                                                                                <?php } ?>
                                                                                            </div>
                                                                                            <div class="only-hover">
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        <a onclick="cart.add('<?= $product['product_id'] ?>');"
                                                                                                           data-toggle="tooltip"
                                                                                                           data-original-title="<?= lang('button_cart') ?>"><i
                                                                                                                    class="fa fa-shopping-cart"></i></a>
                                                                                                    </li>
                                                                                                    <li class="quickview">
                                                                                                        <a href="<?= $product['quickview'] ?>"
                                                                                                           data-toggle="tooltip"
                                                                                                           data-original-title="<?= lang('button_quickview') ?>"><i
                                                                                                                    class="fa fa-search"></i></a>
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <a onclick="compare.add('<?= $product['product_id'] ?>');"
                                                                                                           data-toggle="tooltip"
                                                                                                           data-original-title="<?= lang('button_compare') ?>"><i
                                                                                                                    class="fa fa-exchange"></i></a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <div class="item active">
                                                                <div class="product-grid">
                                                                    <div class="row">
                                                                        <?php foreach ($relates as $product) { ?>
                                                                            <div class="col-sm-2 col-xs-6 col-md-25 col-lg-2 col-sm-3">
                                                                                <div class="product clearfix product-hover">
                                                                                    <div class="left">
                                                                                        <div class="image ">
                                                                                            <?php if ($product['discount_percent']) { ?>
                                                                                                <div class="sale"><?= $product['discount_percent'] ?></div>
                                                                                            <?php } ?>
                                                                                            <?php if ($product['wholesaler']) { ?>
                                                                                                <div class="new">
                                                                                                    GROSIR
                                                                                                </div>
                                                                                            <?php } ?>
                                                                                            <a href="<?= $product['href'] ?>">
                                                                                                <img src="<?= $product['thumb'] ?>"
                                                                                                     alt="<?= $product['name'] ?>"
                                                                                                     class="<?= $product['name'] ?>"/>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="rating">
                                                                                            <?php $star = ''; ?>
                                                                                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                                                <?php if ($i <= $product['rating']) { ?>
                                                                                                    <?php $star .= '<i class="fa fa-star active"></i>'; ?>
                                                                                                <?php } else { ?>
                                                                                                    <?php $star .= '<i class="fa fa-star"></i>'; ?>
                                                                                                <?php } ?>
                                                                                            <?php } ?>
                                                                                            <?= $star ?>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="right">
                                                                                        <div class="name"><a
                                                                                                    href="<?= $product['href'] ?>"><?= cut_text($product['name'], 48) ?></a>
                                                                                        </div>
                                                                                        <div class="price">
                                                                                            <?php if ($product['discount']) { ?>
                                                                                                <span class="price-old"><?= $product['price'] ?></span>
                                                                                                <br><span
                                                                                                        class="price-new"><?= $product['discount'] ?></span>
                                                                                            <?php } else { ?>
                                                                                                <span class="price-new"><?= $product['price'] ?></span>
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                        <div class="only-hover">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a onclick="cart.add('<?= $product['product_id'] ?>');"
                                                                                                       data-toggle="tooltip"
                                                                                                       data-original-title="<?= lang('button_cart') ?>"><i
                                                                                                                class="fa fa-shopping-cart"></i></a>
                                                                                                </li>
                                                                                                <li class="quickview"><a
                                                                                                            href="<?= $product['quickview'] ?>"
                                                                                                            data-toggle="tooltip"
                                                                                                            data-original-title="<?= lang('button_quickview') ?>"><i
                                                                                                                class="fa fa-search"></i></a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a onclick="compare.add('<?= $product['product_id'] ?>');"
                                                                                                       data-toggle="tooltip"
                                                                                                       data-original-title="<?= lang('button_compare') ?>"><i
                                                                                                                class="fa fa-exchange"></i></a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $('#tab01 a').click(function (e) {
                                        e.preventDefault();
                                        $(this).tab('show');
                                    })
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="comment-modal-label"
             aria-hidden="true"></div>
        <script src="<?= base_url('themes/desktop/assets/js/jscroll.min.js') ?>"></script>
        <script type="text/javascript">
            $.ajax({
                url: "<?=site_url('product/review')?>",
                type: 'get',
                data: 'product_id=<?=$product_id?>',
                dataType: 'html',
                success: function (html) {
                    $('#tab-review').html('<div class="scroll">' + html + '</div>');
                    $('.scroll').jscroll({
                        autoTrigger: false,
                        loadingHtml: '<i class="fa fa-refresh fa-spin"></i> Loading...',
                        padding: 20,
                        nextSelector: 'a:last',
                        contentSelector: ''
                    });
                }
            });

            $('#button-cart').on('click', function () {
                $.ajax({
                    url: $('#form-cart').attr('action'),
                    type: 'post',
                    data: $('#form-cart').serialize(),
                    dataType: 'json',
                    success: function (json) {
                        $('.form-group').removeClass('has-error');
                        $('.text-danger').remove();
                        if (json['redirect']) {
                            location = json['redirect'];
                        }
                        if (json['success']) {
                            $('body').append('<div class="modal fade" id="cart-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['success'] + '</div>');
                            $('#cart-modal').modal('show');
                            $('#cart-modal').on('hidden.bs.modal', function (e) {
                                $('#cart-modal').remove();
                            });
                            $('#cart-block .cart-content').load('checkout/cart/info #cart-content-ajax');
                            $('#cart-block .cart-count').html(json['total']);
                        } else if (json['error']['option']) {
                            for (i in json['error']['option']) {
                                $('#input-option' + i).closest('.form-group').addClass('has-error');
                                $('#input-option' + i).after('<small class="text-danger"><i>' + json['error']['option'][i] + '</i></small>');
                            }
                        }
                    }
                });
            });

            $('.button-quantity').on('click', function () {
                var btn = $(this);
                var oldVal = $('input[name=\'quantity\']').val();
                if (btn.text() == '+') {
                    var newVal = parseFloat(oldVal) + 1;
                } else {
                    if (oldVal > 1) {
                        var newVal = parseFloat(oldVal) - 1;
                    } else {
                        var newVal = 1;
                    }
                }

                $('input[name=\'quantity\']').val(newVal);
            });
        </script>