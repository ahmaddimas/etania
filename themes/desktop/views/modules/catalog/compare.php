<div class="breadcrumb full-width">
    <div class="background-breadcrumb"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="title-page"><?= lang('heading_title') ?></h1>
                            <ul>
                                <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                                    <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background-content"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 center-column content-with-background" id="content">
                                <?php if ($products) { ?>
                                    <table class="table table-bordered compare-info">
                                        <thead>
                                        <tr>
                                            <td colspan="<?= count($products) + 1 ?>">
                                                <strong><?= lang('text_product') ?></strong></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?= lang('text_name') ?></td>
                                            <?php foreach ($products as $product) { ?>
                                                <td>
                                                    <strong><a href="<?= $products[$product['product_id']]['href'] ?>"><?= $products[$product['product_id']]['name'] ?></a></strong>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><?= lang('text_image') ?></td>
                                            <?php foreach ($products as $product) { ?>
                                                <td>
                                                    <?php if ($products[$product['product_id']]['thumb']) { ?>
                                                        <img src="<?= $products[$product['product_id']]['thumb'] ?>"
                                                             alt="<?= $products[$product['product_id']]['name'] ?>"/>
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><?= lang('text_price') ?></td>
                                            <?php foreach ($products as $product) { ?>
                                                <td>
                                                <?php if ($products[$product['product_id']]['price']) { ?>
                                                    <?php if (!$products[$product['product_id']]['special']) { ?>
                                                        <span class="price-new"><?= $products[$product['product_id']]['price'] ?></span> </td>
                                                    <?php } else { ?>
                                                        <span class="price-old"><?= $products[$product['product_id']]['price'] ?></span>
                                                        <span class="price-new"><?= $products[$product['product_id']]['special'] ?></span>
                                                    <?php } ?>
                                                <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><?= lang('text_model') ?></td>
                                            <?php foreach ($products as $product) { ?>
                                                <td><?= $products[$product['product_id']]['model'] ?></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><?= lang('text_rating') ?></td>
                                            <?php foreach ($products as $product) { ?>
                                                <td>
                                                    <div class="rating">
                                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                            <?php if ($i <= $product['rating']) { ?>
                                                                <i class="fa fa-star active"></i>
                                                            <?php } else { ?>
                                                                <i class="fa fa-star"></i>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                    <?= $product['reviews'] ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><?= lang('text_summary') ?></td>
                                            <?php foreach ($products as $product) { ?>
                                                <td><?= $product['description'] ?></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><?= lang('text_weight') ?></td>
                                            <?php foreach ($products as $product) { ?>
                                                <td><?= $product['weight'] ?></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><?= lang('text_dimension') ?></td>
                                            <?php foreach ($products as $product) { ?>
                                                <td><?= $product['dimension'] ?></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><?= lang('text_availability') ?></td>
                                            <?php foreach ($products as $product) { ?>
                                                <td><?= $product['availability'] ?></td>
                                            <?php } ?>
                                        </tr>
                                        </tbody>
                                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                                            <thead>
                                            <tr>
                                                <td colspan="<?= count($products) + 1 ?>"><?= $attribute_group['name'] ?></td>
                                            </tr>
                                            </thead>
                                            <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                                                <tbody>
                                                <tr>
                                                    <td><?= $attribute['name'] ?></td>
                                                    <?php foreach ($products as $product) { ?>
                                                        <?php if (isset($products[$product['product_id']]['attribute'][$key])) { ?>
                                                            <td><?= $products[$product['product_id']]['attribute'][$key] ?></td>
                                                        <?php } else { ?>
                                                            <td></td>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tr>
                                                </tbody>
                                            <?php } ?>
                                        <?php } ?>
                                        <tr>
                                            <td></td>
                                            <?php foreach ($products as $product) { ?>
                                                <td><input type="button" value="<?= lang('button_cart') ?>"
                                                           class="btn btn-primary btn-block"
                                                           onclick="cart.add('<?= $product['product_id'] ?>');"/> <a
                                                            href="<?= $product['remove'] ?>"
                                                            class="btn btn-danger btn-block"><?= lang('button_remove') ?></a>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    </table>
                                <?php } else { ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"></div>
                </div>
            </div>
        </div>
    </div>
</div>