<!DOCTYPE html>
<html class="quickview">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/themes/desktop/assets/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="/themes/desktop/assets/css/stylesheet.css"/>
    <link rel="stylesheet" type="text/css" href="/themes/desktop/assets/css/owl.carousel.css"/>
    <link rel="stylesheet" type="text/css" href="/themes/desktop/assets/css/font-awesome.min.css"/>
    <script type="text/javascript" src="/themes/desktop/assets/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="/themes/desktop/assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/themes/desktop/assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="/themes/desktop/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/themes/desktop/assets/js/jquery.elevateZoom-3.0.3.min.js"></script>
    <script type="text/javascript" src="/themes/desktop/assets/js/common.js"></script>
    <script type="text/javascript" src="/themes/desktop/assets/js/owl.carousel.min.js"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="/themes/desktop/assets/js/respond.min.js"></script><![endif]-->
</head>

<body>
<div id="main">
    <div class="product-info">
        <div class="row">
            <div class="col-sm-12">
                <div class="row" id="quickview_product">
                    <script>
                        $(document).ready(function () {
                            $('#ex1, .review-link').live('click', function () {
                                top.location.href = "<?=site_url()?>";
                                return false;
                            });
                            $('#image').elevateZoom({
                                zoomType: "inner",
                                cursor: "pointer",
                                zoomWindowFadeIn: 500,
                                zoomWindowFadeOut: 750
                            });
                            $('.thumbnails a, .thumbnails-carousel a').click(function () {
                                var smallImage = $(this).attr('data-image');
                                var largeImage = $(this).attr('data-zoom-image');
                                var ez = $('#image').data('elevateZoom');
                                ez.swaptheimage(smallImage, largeImage);
                                return false;
                            });
                        });
                    </script>
                    <div class="col-sm-6 popup-gallery">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="product-image inner-cloud-zoom"><a href="<?= site_url() ?>#"
                                                                               title="<?= $name ?>" id="ex1"><img
                                                src="<?= $thumb ?>" title="<?= $name ?>" alt="<?= $name ?>" id="image"
                                                data-zoom-image="<?= $popup ?>"/></a></div>
                            </div>
                            <?php if ($images) { ?>
                                <div class="col-sm-12">
                                    <div class="overflow-thumbnails-carousel">
                                        <div class="thumbnails-carousel owl-carousel">
                                            <?php foreach ($images as $image) { ?>
                                                <div class="item"><a href="<?= $image['popup'] ?>" class="popup-image"
                                                                     data-image="<?= $image['thumb'] ?>"
                                                                     data-zoom-image="<?= $image['popup'] ?>"><img
                                                                src="<?= $image['thumb'] ?>" title="<?= $name ?>"
                                                                alt="<?= $name ?>"/></a></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $(".thumbnails-carousel").owlCarousel({
                                                autoPlay: 6000,
                                                navigation: true,
                                                navigationText: ['', ''],
                                                itemsCustom: [[0, 4], [450, 5], [550, 6], [768, 4]],
                                            });
                                        });
                                    </script>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-sm-6 product-center clearfix">
                        <div>
                            <h2 class="product-name"><a href="<?= site_url() ?>#" class="review-link"><?= $name ?></a>
                            </h2>
                            <div class="review">
                                <div class="rating">
                                    <?php $star = ''; ?>
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <?php if ($i <= $rating) { ?>
                                            <?php $star .= '<i class="fa fa-star active"></i>'; ?>
                                        <?php } else { ?>
                                            <?php $star .= '<i class="fa fa-star"></i>'; ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <?= $star ?>
                                    (<?= sprintf(lang('text_reviews'), (int)$reviews) ?>)
                                </div>
                            </div>
                            <div class="description">
                                <span><?= lang('text_model') ?></span>
                                <?= $model ?>
                                <br/><span><?= lang('text_stock') ?></span>
                                <?= $stock_status ?>
                            </div>
                            <div class="price">
                                <?php if ($discount) { ?>
                                    <span class="price-old"><?= $price ?></span><br><span
                                            class="price-new"><?= $discount ?></span>
                                <?php } else { ?>
                                    <span class="price-new"><?= $price ?></span>
                                <?php } ?>
                            </div>
                            <?php if ($discounts) { ?>
                                <div class="options">
                                    <h3><?= lang('text_discount') ?></h3>
                                    <table class="table table-bordered">
                                        <?php foreach ($discounts as $discount) { ?>
                                            <tr>
                                                <td>&ge; <?= $discount['quantity'] ?></td>
                                                <td><?= $discount['price'] ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            <?php } ?>
                            <div class="options">
                                <?= $this->config->item('sharing_button_code') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#button-cart').on('click', function () {
        $.ajax({
            url: '<?=site_url('checkout/cart/add')?>',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));
                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    parent.$.notify({
                        message: json['success'],
                        target: '_blank'
                    }, {
                        selement: 'body',
                        position: null,
                        type: "info",
                        allow_dismiss: true,
                        newest_on_top: false,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 2031,
                        delay: 5000,
                        timer: 1000,
                        url_target: '_blank',
                        mouse_over: null,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        onShow: null,
                        onShown: null,
                        onClose: null,
                        onClosed: null,
                        icon_type: 'class',
                        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' + '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' + '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp;{2}</span>' + '<div class="progress" data-notify="progressbar">' + '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' + '</div>' + '<a href="{3}" target="{4}" data-notify="url"></a>' + '</div>'
                    });
                    parent.$('#cart_block #cart_content').load('<?=site_url('common/cart/info')?> #cart_content_ajax');
                    parent.$('#cart_block #total_price_ajax').load('<?=site_url('common/cart/info')?> #total_price');
                    parent.$('#cart_block .cart-count').load('<?=site_url('common/cart/info')?> #total_count_ajax');
                }
            }
        });
    });
</script>
</body>
</html>