<div class="breadcrumb full-width">
    <div class="background-breadcrumb"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <h1 id="title-page"><?= lang('heading_title') ?></h1>
                    <ul>
                        <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                            <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background-content"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-3" id="column-left">
                        <div class="box box-no-advanced">
                            <h3 class="box-heading"><?= lang('text_refine') ?></h3>
                            <div class="strip-line"></div>
                            <div class="box-content">
                                <form action="<?= current_url() ?>" id="form-filter">
                                    <input type="hidden" name="sort" value="<?= $sort ?>">
                                    <input type="hidden" name="order" value="<?= $order ?>">
                                    <?php foreach ($filter_groups as $filter_group) { ?>
                                        <div class="widget-header">
                                            <strong class="widget-title"><?= $filter_group['name'] ?></strong>
                                        </div>
                                        <div class="sidebar-widget-body m-t-10">
                                            <?php foreach ($filter_group['filters'] as $filter) { ?>
                                                <div class="checkbox">
                                                    <?php if (in_array($filter['filter_id'], $filters)) { ?>
                                                        <label><input type="checkbox" name="filter[]"
                                                                      value="<?= $filter['filter_id'] ?>"
                                                                      checked="checked"><?= $filter['name'] ?></label>
                                                    <?php } else { ?>
                                                        <label><input type="checkbox" name="filter[]"
                                                                      value="<?= $filter['filter_id'] ?>"><?= $filter['name'] ?>
                                                        </label>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </form>
                            </div>
                        </div>
                        <div class="row banners banners-with-padding-30">
                            <div class="col-sm-12"></div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12 center-column content-without-background" id="content">
                                <div>
                                    <div class="product-filter clearfix">
                                        <div class="options">
                                            <div class="product-compare"><a href="<?= site_url('product/compare') ?>"
                                                                            id="compare-total"><?= $compare ?></a></div>
                                            <div class="button-group display" data-toggle="buttons-radio">
                                                <button id="grid" rel="tooltip" title="Grid"
                                                        onclick="setDisplay('grid');"><i class="fa fa-th-large"></i>
                                                </button>
                                                <button id="list" class="active" rel="tooltip" title="List"
                                                        onclick="setDisplay('list');"><i class="fa fa-th-list"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="list-options">
                                            <div class="sort">
                                                <select class="sorter">
                                                    <?php foreach ($sorts as $sort_data) { ?>
                                                        <?php if ($sort_data['value'] == $sort . '-' . $order) { ?>
                                                            <option value="<?= $sort_data['href'] ?>"><?= $sort_data['text'] ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?= $sort_data['href'] ?>"><?= $sort_data['text'] ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div id="product-list">
                                            <?php if ($products) { ?>
                                                <div class="product-list">
                                                    <?php foreach ($products as $product) { ?>
                                                        <div>
                                                            <div class="row">
                                                                <div class="image col-sm-3">
                                                                    <?php if ($product['discount_percent']) { ?>
                                                                        <div class="sale"><?= $product['discount_percent'] ?></div>
                                                                    <?php } ?>
                                                                    <?php if ($product['free_shipping'] == 1) { ?>
                                                                        <div style="position: absolute; right: 0px; padding: 5px 20px 0 0;">
                                                                            <img style="width:80px; height:20px"
                                                                                 src="<?= site_url('storage/images/' . $free_shipping['free_shipping_logo']) ?>"
                                                                                 alt="Free Shipping"/></div>
                                                                    <?php } ?>
                                                                    <a href="<?= $product['href'] ?>"><img
                                                                                src="<?= $product['thumb'] ?>"
                                                                                alt="<?= $product['name'] ?>"
                                                                                class="img-responsive"/></a>
                                                                </div>
                                                                <div class="name-actions col-sm-4">
                                                                    <div class="name">
                                                                        <a href="<?= $product['href'] ?>"><?= cut_text($product['name'], 48) ?></a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <?php if ($product['discount']) { ?>
                                                                            <span class="price-old"><?= $product['price'] ?></span>
                                                                            <br><span
                                                                                    class="price-new"><?= $product['discount'] ?></span>
                                                                        <?php } else { ?>
                                                                            <span class="price-new"><?= $product['price'] ?></span>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <ul>
                                                                        <li>
                                                                            <a onclick="cart.add('<?= $product['product_id'] ?>');"
                                                                               data-toggle="tooltip"
                                                                               data-original-title="<?= lang('button_cart') ?>"><i
                                                                                        class="fa fa-shopping-cart"></i></a>
                                                                        </li>
                                                                        <li class="quickview"><a
                                                                                    href="<?= $product['quickview'] ?>"
                                                                                    data-toggle="tooltip"
                                                                                    data-original-title="<?= lang('button_quickview') ?>"><i
                                                                                        class="fa fa-search"></i></a>
                                                                        </li>
                                                                        <li>
                                                                            <a onclick="compare.add('<?= $product['product_id'] ?>');"
                                                                               data-toggle="tooltip"
                                                                               data-original-title="<?= lang('button_compare') ?>"><i
                                                                                        class="fa fa-exchange"></i></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="desc col-sm-5">
                                                                    <div class="rating">
                                                                        <?php $star = ''; ?>
                                                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                            <?php if ($i <= $product['rating']) { ?>
                                                                                <?php $star .= '<i class="fa fa-star active"></i>'; ?>
                                                                            <?php } else { ?>
                                                                                <?php $star .= '<i class="fa fa-star"></i>'; ?>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <?= $star ?>
                                                                    </div>
                                                                    <div class="description"><?= cut_text($product['description'], 100) ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="product-grid">
                                                    <div class="row">
                                                        <?php foreach (array_chunk($products, ceil(count($products) / 3)) as $product_list) { ?>
                                                            <?php foreach ($product_list as $product) { ?>
                                                                <div class="col-sm-3 col-xs-6">
                                                                    <div class="product clearfix product-hover">
                                                                        <div class="left">
                                                                            <div class="image">
                                                                                <?php if ($product['discount_percent']) { ?>
                                                                                    <div class="sale"><?= $product['discount_percent'] ?></div>
                                                                                <?php } ?>
                                                                                <?php if ($product['free_shipping'] == 1) { ?>
                                                                                    <div style="position: absolute; right: 0px; padding: 5px;">
                                                                                        <img style="width:80px; height:20px"
                                                                                             src="<?= site_url('storage/images/' . $free_shipping['free_shipping_logo']) ?>"
                                                                                             alt="Free Shipping"/></div>
                                                                                <?php } ?>
                                                                                <a href="<?= $product['href'] ?>">
                                                                                    <img src="<?= $product['thumb'] ?>"
                                                                                         alt="<?= $product['name'] ?>"
                                                                                         class="img-responsive"/>
                                                                                </a>
                                                                            </div>
                                                                            <div class="rating">
                                                                                <?php $star = ''; ?>
                                                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                                    <?php if ($i <= $product['rating']) { ?>
                                                                                        <?php $star .= '<i class="fa fa-star active"></i>'; ?>
                                                                                    <?php } else { ?>
                                                                                        <?php $star .= '<i class="fa fa-star"></i>'; ?>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                                <?= $star ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="right">
                                                                            <div class="name"><a
                                                                                        href="<?= $product['href'] ?>"><?= cut_text($product['name']) ?></a>
                                                                            </div>
                                                                            <div class="price">
                                                                                <?php if ($product['discount']) { ?>
                                                                                    <span class="price-old"><?= $product['price'] ?></span>
                                                                                    <br><span
                                                                                            class="price-new"><?= $product['discount'] ?></span>
                                                                                <?php } else { ?>
                                                                                    <span class="price-new"><?= $product['price'] ?></span>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="only-hover">
                                                                                <ul>
                                                                                    <li>
                                                                                        <a onclick="cart.add('<?= $product['product_id'] ?>');"
                                                                                           data-toggle="tooltip"
                                                                                           data-original-title="<?= lang('button_cart') ?>"><i
                                                                                                    class="fa fa-shopping-cart"></i></a>
                                                                                    </li>
                                                                                    <li class="quickview"><a
                                                                                                href="<?= $product['quickview'] ?>"
                                                                                                data-toggle="tooltip"
                                                                                                data-original-title="<?= lang('button_quickview') ?>"><i
                                                                                                    class="fa fa-search"></i></a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a onclick="compare.add('<?= $product['product_id'] ?>');"
                                                                                           data-toggle="tooltip"
                                                                                           data-original-title="<?= lang('button_compare') ?>"><i
                                                                                                    class="fa fa-exchange"></i></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="row pagination-results">
                                                    <?= $pagination ?>
                                                </div>
                                            <?php } else { ?>
                                                <p><?= lang('text_empty') ?></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        setDisplay();
    });

    $(document).delegate('.card .pagination li a', 'click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        if (typeof (href) !== 'undefined') {
            $('.card').load(href + ' #product-list', function (response, status, xhr) {
                setDisplay();
            });

            $('html, body').animate({scrollTop: 0}, 'slow');
            history.pushState('', 'Daftar Produk', href);
        }
    });

    $('select[class=\'sorter\']').on('change', function (e) {
        e.preventDefault();
        var href = $(this).val();
        if (typeof (href) !== 'undefined') {
            $('.card').load(href + ' #product-list', function (response, status, xhr) {
                setDisplay();
            });

            $('html, body').animate({scrollTop: 0}, 'slow');
            history.pushState('', 'Daftar Produk', href);
        }
    });

    $('input[name=\'filter[]\']').on('change', function () {
        var url = $('#form-filter').attr('action') + '?' + $('#form-filter').serialize();
        $('.card').load(url + ' #product-list', function (response, status, xhr) {
            setDisplay();
        });

        $('html, body').animate({scrollTop: 0}, 'slow');
        history.pushState('', 'Daftar Produk', url);
    });

    function setDisplay(view) {
        if (view == 'list') {
            $('.product-grid').hide();
            $('.product-list').show();
            $('.display').html('<button id="grid" rel="tooltip" title="Grid" onclick="setDisplay(\'grid\');"><i class="fa fa-th-large"></i></button> <button class="active" id="list" rel="tooltip" title="List" onclick="setDisplay(\'list\');"><i class="fa fa-th-list"></i></button>');
            localStorage.setItem('display', 'list');
        } else if (view == 'grid') {
            $('.product-grid').show();
            $('.product-list').hide();
            $('.display').html('<button class="active" id="grid" rel="tooltip" title="Grid" onclick="setDisplay(\'grid\');"><i class="fa fa-th-large"></i></button> <button id="list" rel="tooltip" title="List" onclick="setDisplay(\'list\');"><i class="fa fa-th-list"></i></button>');
            localStorage.setItem('display', 'grid');
        } else {
            if (localStorage.getItem('display') == 'list') {
                setDisplay('list');
            } else {
                setDisplay('grid');
            }
        }

        $('.quickview a').magnificPopup({
            preloader: true,
            tLoading: '',
            type: 'iframe',
            mainClass: 'quickview',
            removalDelay: 200,
            gallery: {
                enabled: true
            }
        });
    }
</script>