<div class="container">
    <div class="jumbotron">
        <h2>ERROR 401</h2>
        <p class="lead">Maaf, dengan berat hati kami tidak dapat menampilkan halaman ini. Kami sarankan Anda kembali ke
            <a href="<?= site_url() ?>">halaman depan</a></p>
    </div>
</div>
