<div class="breadcrumb full-width">
    <div class="background-breadcrumb"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <h1 id="title-page">The page you requested cannot be found!</h1>
                    <ul>
                        <li><a href="">Home</a></li>
                        <li><a href="">The page you requested cannot be found!</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background-content"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 center-column content-with-background" id="content">
                                <p>The page you requested cannot be found.</p>
                                <div class="buttons">
                                    <div class="pull-right"><a href="" class="btn btn-primary">Continue</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"></div>
                </div>
            </div>
        </div>
    </div>
</div>