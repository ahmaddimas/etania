<div class="breadcrumb full-width">
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="title-page"><?= $heading_title ?></h1>
                            <ul>
                                <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                                    <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 center-column content-with-background" id="content">
                                <p class="lead"><?= $heading_subtitle ?></p>
                                <?= form_open(site_url('custom/validate'), 'id="form"') ?>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Kategori</th>
                                        <th>Part</th>
                                        <th class="text-right">Harga</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($categories as $category) { ?>
                                        <tr>
                                            <td><?= $category['name'] ?></td>
                                            <td>
                                                <select name="parts[<?= $category['custom_category_id'] ?>]"
                                                        class="form-control">
                                                    <option value="0" rel="0"> -- Silakan Pilih --</option>
                                                    <?php foreach ($category['parts'] as $part) { ?>
                                                        <option value="<?= $part['product_id'] ?>"
                                                                rel="<?= $part['price'] ?>"><?= $part['name'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td class="text-right"><input type="text" class="subtotal text-right"
                                                                          value="0" style="border:none;"></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="2" class="text-right"><strong>Total</strong></td>
                                        <td class="text-right"><input type="text" class="grand-total text-right"
                                                                      value="0"
                                                                      style="border:none;font-weight:bold;font-size:18px;">
                                        </td>
                                    </tr>
                                </table>
                                <div class="buttons">
                                    <div class="pull-right">
                                        <button type="button" id="submit"
                                                class="btn btn-primary"><?= lang('button_cart') ?></button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= site_url('assets/js/jquery.number.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('select').on('change', function () {
            $(this).closest('tr').find('.subtotal').val($(this).find('option:selected').attr('rel'));
            calculateTotal();
        });
    });

    $('.subtotal, .grand-total').number(true);

    function calculateTotal() {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.error').remove();
                if (json['success']) {
                    var subTotals = $('.subtotal');
                    var grandTotal = 0;

                    $.each(subTotals, function (i) {
                        grandTotal += parseFloat($(subTotals[i]).val());
                    });

                    $('.grand-total').val(parseFloat(grandTotal));
                } else if (json['errors']) {
                    for (i in json['errors']) {
                        $('select[name=\'parts[' + i + ']\']').after('<small class="error text-danger">' + json['errors'][i] + '</small>');
                    }
                }
            }
        });
    }

    calculateTotal();

    $('#submit').on('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize() + '&submit=1',
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.error').remove();
                if (json['redirect']) {
                    window.location = json['redirect'];
                } else if (json['errors']) {
                    for (i in json['errors']) {
                        $('select[name=\'parts[' + i + ']\']').after('<small class="error text-danger">' + json['errors'][i] + '</small>');
                    }
                }
            }
        });
    });
</script>