<div class="main-content full-width home">
    <div class="background">
        <div class="pattern" style="background-color:#fff;border-bottom:1px solid #efefef;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <?php if ($top_banners) { ?>
                            <div class="row banners banners-with-padding-0 responsive-margin-top-30 app-banners"
                                 style="margin-top: -106px;z-index: 22;position: relative">
                                <?php foreach ($top_banners as $banner) { ?>
                                    <div class="col-sm-6">
                                        <a href="<?= $banner['link'] ?>" title="<?= $banner['link_title'] ?>"><img
                                                    src="<?= $banner['image'] ?>" alt="<?= $banner['title'] ?>"></a>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div class="advanced-grid advanced-grid-70916506" style="margin-top: 0px;margin-bottom: 0px;">
                            <div>
                                <div class="container">
                                    <div style="padding-top: 0px;padding-left: 0px;padding-bottom: 0px;padding-right: 0px;">
                                        <div class="row">
                                            <?php if (Modules::run('offers/dotd') != '') { ?>
                                            <?= Modules::run('offers/dotd') ?>
                                            <div class="col-sm-8">
                                                <?php } else { ?>
                                                <div class="col-sm-12">
                                                    <?php } ?>
                                                    <div class="box clearfix box-with-products">
                                                        <div class="box-heading"><?= lang('text_latest_product') ?></div>
                                                        <div class="strip-line"></div>
                                                        <div class="clear"></div>
                                                        <div class="box-content products"
                                                             style="background:none;padding:20px 0;border:none;">
                                                            <div class="box-product">
                                                                <div id="carousel-171952070">
                                                                    <div class="carousel-inner">
                                                                        <div class="active item">
                                                                            <div class="product-grid">
                                                                                <div class="row">
                                                                                    <?php if ($latest) { ?>
                                                                                        <?php foreach (array_chunk($latest, ceil(count($latest) / 2)) as $products) { ?>
                                                                                            <?php foreach ($products as $product) { ?>
                                                                                                <div class="col-sm-3 col-xs-6">
                                                                                                    <div class="product clearfix product-hover">
                                                                                                        <div class="left">
                                                                                                            <div class="image ">
                                                                                                                <?php if ($product['discount_percent']) { ?>
                                                                                                                    <div class="sale"><?= $product['discount_percent'] ?></div>
                                                                                                                <?php } ?>
                                                                                                                <?php if ($product['wholesaler']) { ?>
                                                                                                                    <div class="new">
                                                                                                                        GROSIR
                                                                                                                    </div>
                                                                                                                <?php } ?>
                                                                                                                <?php if ($product['free_shipping'] == 1) { ?>
                                                                                                                    <div style="position: absolute; right: 0px; padding: 5px;">
                                                                                                                        <img style="width:80px; height:20px"
                                                                                                                             src="<?= site_url('storage/images/' . $free_shipping['free_shipping_logo']) ?>"
                                                                                                                             alt="Free Shipping"/>
                                                                                                                    </div>
                                                                                                                <?php } ?>
                                                                                                                <a href="<?= $product['href'] ?>">
                                                                                                                    <img src="<?= $product['thumb'] ?>"
                                                                                                                         alt="<?= $product['name'] ?>"
                                                                                                                         class="img-responsive"/>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="rating">
                                                                                                                <?php $star = ''; ?>
                                                                                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                                                                    <?php if ($i <= $product['rating']) { ?>
                                                                                                                        <?php $star .= '<i class="fa fa-star active"></i>'; ?>
                                                                                                                    <?php } else { ?>
                                                                                                                        <?php $star .= '<i class="fa fa-star"></i>'; ?>
                                                                                                                    <?php } ?>
                                                                                                                <?php } ?>
                                                                                                                <?= $star ?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="right">
                                                                                                            <div class="name">
                                                                                                                <a href="<?= $product['href'] ?>"><?= $product['name'] ?></a>
                                                                                                            </div>
                                                                                                            <div class="price">
                                                                                                                <?php if ($product['discount']) { ?>
                                                                                                                    <span class="price-old"><?= $product['price'] ?></span>
                                                                                                                    <br>
                                                                                                                    <span class="price-new"><?= $product['discount'] ?></span>
                                                                                                                <?php } else { ?>
                                                                                                                    <span class="price-new"><?= $product['price'] ?></span>
                                                                                                                <?php } ?>
                                                                                                            </div>
                                                                                                            <div class="only-hover">
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        <a onclick="cart.add('<?= $product['product_id'] ?>');"
                                                                                                                           data-toggle="tooltip"
                                                                                                                           data-original-title="<?= lang('button_cart') ?>"><i
                                                                                                                                    class="fa fa-shopping-cart"></i></a>
                                                                                                                    </li>
                                                                                                                    <li class="quickview">
                                                                                                                        <a href="<?= $product['quickview'] ?>"
                                                                                                                           data-toggle="tooltip"
                                                                                                                           data-original-title="<?= lang('button_quickview') ?>"><i
                                                                                                                                    class="fa fa-search"></i></a>
                                                                                                                    </li>
                                                                                                                    <li>
                                                                                                                        <a onclick="compare.add('<?= $product['product_id'] ?>');"
                                                                                                                           data-toggle="tooltip"
                                                                                                                           data-original-title="<?= lang('button_compare') ?>"><i
                                                                                                                                    class="fa fa-exchange"></i></a>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            <?php } ?>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pattern" style="border-bottom:1px solid #efefef;">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <?= Modules::run('offers/special') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.quickview a').magnificPopup({
            preloader: true,
            tLoading: '',
            type: 'iframe',
            mainClass: 'quickview',
            removalDelay: 200,
            gallery: {
                enabled: true
            }
        });
    </script>