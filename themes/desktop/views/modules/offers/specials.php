<div style="background-color:#fff;">
    <div class="breadcrumb full-width">
        <div class="background-breadcrumb"></div>
    </div>
    <div class="main-content full-width inner-page">
        <div class="background-content"></div>
        <div class="background">
            <div class="shadow"></div>
            <div class="pattern">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="filter-product">
                                <div class="filter-tabs">
                                    <div class="bg-filter-tabs">
                                        <div class="bg-filter-tabs2 clearfix">
                                            <ul id="tab01">
                                                <?php foreach ($specials as $key => $special) { ?>
                                                    <li class="<?php echo $key == 0 ? 'active' : ''; ?>"><a
                                                                href="#products-<?= $key ?>"><?= $special['name'] ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content clearfix" style="background:none;padding:20px 0;border:none;">
                                    <?php foreach ($specials as $key => $special) { ?>
                                        <div class="tab-pane <?php echo $key == 0 ? 'active' : ''; ?>"
                                             id="products-<?= $key ?>">
                                            <a class="next-button" href="#carousel-tab-<?= $key ?>"
                                               id="carousel-tab-<?= $key ?>_next"><span></span></a>
                                            <a class="prev-button" href="#carousel-tab-<?= $key ?>"
                                               id="carousel-tab-<?= $key ?>_prev"><span></span></a>
                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    var owlObject = $(".filter-product #carousel-tab-<?=$key?> .carousel-inner");

                                                    $("#carousel-tab-<?=$key?>_next").click(function () {
                                                        owlObject.trigger('owl.next');
                                                        return false;
                                                    });
                                                    $("#carousel-tab-<?=$key?>_prev").click(function () {
                                                        owlObject.trigger('owl.prev');
                                                        return false;
                                                    });

                                                    owlObject.owlCarousel({
                                                        slideSpeed: 500,
                                                        singleItem: true
                                                    });
                                                });
                                            </script>
                                            <div class="box-product">
                                                <div id="carousel-tab-<?= $key ?>" class="carousel slide">
                                                    <div class="carousel-inner">
                                                        <?php if (count($special['products']) > 0) { ?>
                                                            <?php foreach (array_chunk($special['products'], 6) as $key2 => $products) { ?>
                                                                <div class="item <?php echo $key2 == 0 ? 'active' : ''; ?>">
                                                                    <div class="product-grid">
                                                                        <div class="row">
                                                                            <?php foreach ($products as $key3 => $product) { ?>
                                                                                <div class="col-sm-2 col-xs-6 col-md-25 col-lg-2 col-sm-3">
                                                                                    <div class="product clearfix product-hover">
                                                                                        <div class="left">
                                                                                            <div class="image ">
                                                                                                <?php if ($product['discount_percent']) { ?>
                                                                                                    <div class="sale"><?= $product['discount_percent'] ?></div>
                                                                                                <?php } ?>
                                                                                                <?php if ($product['wholesaler']) { ?>
                                                                                                    <div class="new">
                                                                                                        GROSIR
                                                                                                    </div>
                                                                                                <?php } ?>
                                                                                                <a href="<?= $product['href'] ?>">
                                                                                                    <img src="<?= $product['thumb'] ?>"
                                                                                                         alt="<?= $product['name'] ?>"
                                                                                                         class="<?= $product['name'] ?>"/>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="rating">
                                                                                                <?php $star = ''; ?>
                                                                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                                                    <?php if ($i <= $product['rating']) { ?>
                                                                                                        <?php $star .= '<i class="fa fa-star active"></i>'; ?>
                                                                                                    <?php } else { ?>
                                                                                                        <?php $star .= '<i class="fa fa-star"></i>'; ?>
                                                                                                    <?php } ?>
                                                                                                <?php } ?>
                                                                                                <?= $star ?>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="right">
                                                                                            <div class="name"><a
                                                                                                        href="<?= $product['href'] ?>"><?= cut_text($product['name'], 35) ?></a>
                                                                                            </div>
                                                                                            <div class="price">
                                                                                                <?php if ($product['discount']) { ?>
                                                                                                    <span class="price-old"><?= $product['price'] ?></span>
                                                                                                    <br><span
                                                                                                            class="price-new"><?= $product['discount'] ?></span>
                                                                                                <?php } else { ?>
                                                                                                    <span class="price-new"><?= $product['price'] ?></span>
                                                                                                <?php } ?>
                                                                                            </div>
                                                                                            <div class="only-hover">
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        <a onclick="cart.add('<?= $product['product_id'] ?>');"
                                                                                                           data-toggle="tooltip"
                                                                                                           data-original-title="<?= lang('button_cart') ?>"><i
                                                                                                                    class="fa fa-shopping-cart"></i></a>
                                                                                                    </li>
                                                                                                    <li class="quickview">
                                                                                                        <a href="<?= $product['quickview'] ?>"
                                                                                                           data-toggle="tooltip"
                                                                                                           data-original-title="<?= lang('button_quickview') ?>"><i
                                                                                                                    class="fa fa-search"></i></a>
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <a onclick="compare.add('<?= $product['product_id'] ?>');"
                                                                                                           data-toggle="tooltip"
                                                                                                           data-original-title="<?= lang('button_compare') ?>"><i
                                                                                                                    class="fa fa-exchange"></i></a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $('#tab01 a').click(function (e) {
                                    e.preventDefault();
                                    $(this).tab('show');
                                })
                            </script>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>