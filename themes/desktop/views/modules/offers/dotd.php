<div class="col-sm-4">
    <div class="box today-deals-app box-no-advanced">
        <div class="box-heading"><?= $title ?></div>
        <div class="strip-line"></div>
        <div class="box-content">
            <div class="clearfix" style="clear: both">
                <div class="today-deals-app-products">
                    <?php if ($status) { ?>
                        <script>
                            $(function () {
                                var austDay = new Date();
                                austDay = new Date(<?=$year?>, <?=$month?> -1, <?=$day?>, <?=$hour?>, <?=$minute?>);
                                $('#countdown-01').countdown({
                                    until: austDay
                                });
                            });
                        </script>
                        <div id="countdown-01" class="clearfix"></div>
                    <?php } ?>
                    <div class="product clearfix product-hover">
                        <div class="left">
                            <?php if ($discount_percent) { ?>
                                <div class="sale"><?= $discount_percent ?></div>
                            <?php } ?>
                            <div class="image">
                                <a href="<?= $href ?>">
                                    <img src="<?= $thumb ?>" alt="<?= $name ?>" class=""/>
                                </a>
                            </div>
                        </div>
                        <div class="right">
                            <div class="name"><a href="<?= $href ?>"><?= cut_text($name, 48) ?></a></div>
                            <div class="price">
                                <?php if ($discount) { ?>
                                    <span class="price-old"><?= $price ?></span><br><span
                                            class="price-new"><?= $discount ?></span>
                                <?php } else { ?>
                                    <span class="price-new"><?= $price ?></span>
                                <?php } ?>
                            </div>
                            <div class="only-hover">
                                <ul>
                                    <li><a onclick="cart.add('<?= $product_id ?>');"
                                           class="button-add-to-cart"><?= lang('button_cart') ?></a></li>
                                    <li class="quickview"><a href="<?= $quickview ?>" data-toggle="tooltip"
                                                             data-original-title="<?= lang('button_quickview') ?>"><i
                                                    class="fa fa-search"></i></a></li>
                                    <li><a onclick="compare.add('<?= $product_id ?>');" data-toggle="tooltip"
                                           data-original-title="<?= lang('button_compare') ?>"><i
                                                    class="fa fa-exchange"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>