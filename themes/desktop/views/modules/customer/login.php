<div class="breadcrumb full-width">
    <div class="background-breadcrumb"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <h1 id="title-page"><?= lang('heading_title') ?></h1>
                    <ul>
                        <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                            <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background-content"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 sign-in-page">
                        <div class="row">
                            <div class="col-md-12 center-column content-with-background" id="content">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5 sign-in">
                                        <?= form_open($action, 'id="form-login" class="register-form outer-top-xs"') ?>
                                        <?php if ($error) { ?>
                                            <div class="alert alert-danger">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">&times;
                                                </button><?= $error ?></div>
                                        <?php } ?>
                                        <p><?= $text_register ?></p>
                                        <div class="form-group">
                                            <input type="text" name="email" placeholder="Email" autofocus
                                                   class="form-control unicase-form-control text-input">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" placeholder="Password"
                                                   class="form-control unicase-form-control text-input">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember"
                                                               value="1"> <?= lang('entry_remember') ?>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="checkbox">
                                                    <label><a href=""><?= $text_forgotten ?></a></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary btn-block btn-lg"
                                                    id="button-login"><?= lang('button_login') ?> <i
                                                        class="fa fa-sign-in fa-lg"></i></button>
                                        </div>
                                        </form>
                                    </div>
                                    <?php if ($this->config->item('oauth_facebook_status') || $this->config->item('oauth_google_status')) { ?>
                                        <div class="col-sm-2 sep" style="height:320px;">
                                            <span class="sepText"><?= lang('text_or') ?></span>
                                        </div>
                                        <div class="col-md-5 col-sm-5 sign-in">
                                            <div class="social-sign-in">
                                                <?php if ($this->config->item('oauth_facebook_status')) { ?>
                                                    <a href="<?= user_url('facebook_login') ?>"
                                                       class="btn facebook-sign-in btn-block"><i
                                                                class="fa fa-facebook fa-lg"></i> <?= lang('button_facebook_login') ?>
                                                    </a><br>
                                                <?php } ?>
                                                <?php if ($this->config->item('oauth_google_status')) { ?>
                                                    <a href="<?= user_url('google_login') ?>"
                                                       class="btn google-sign-in btn-block"><i
                                                                class="fa fa-google fa-lg"></i> <?= lang('button_google_login') ?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"></div>
                </div>
            </div>
        </div>
    </div>
</div>