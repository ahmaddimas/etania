<div class="breadcrumb full-width">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <h1><?= lang('heading_title') ?></h1>
                    <ul>
                        <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                            <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background">
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3" id="column-right">
                                <?= Modules::run('customer/navigation/index') ?>
                            </div>
                            <div class="col-md-9 center-column content-with-background" id="content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="widget-small"><i class="icon fa fa-shopping-cart fa-3x"></i>
                                            <div class="info">
                                                <h4><?= lang('text_order') ?></h4>
                                                <p><?= sprintf(lang('text_order_total'), (int)$count_orders) ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-sm-6">
										<div class="widget-small"><i class="icon fa fa-money fa-3x"></i>
											<div class="info">
												<h4><?= lang('text_credit') ?></h4>
												<p><?= $balance ?></p>
											</div>
										</div>
									</div> -->
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><i class="fa fa-user"></i> <?= lang('text_name') ?><span
                                                class="pull-right"><?= $customer['name'] ?></span></li>
                                    <li class="list-group-item"><i
                                                class="fa fa-envelope-o"></i> <?= lang('text_email') ?><span
                                                class="pull-right"><?= $customer['email'] ?></span></li>
                                    <li class="list-group-item"><i
                                                class="fa fa-phone"></i> <?= lang('text_telephone') ?><span
                                                class="pull-right"><?= $customer['telephone'] ?></span></li>
                                    <li class="list-group-item"><i class="fa fa-calendar"></i> <?= lang('text_since') ?>
                                        <span class="pull-right"><?= format_date($customer['date_added']) ?></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>