<div class="box box-with-links box-no-advanced">
    <div class="box-content">
        <ul class="list-box">
            <li><a href="<?= site_url('customer') ?>"><?= lang('text_overview') ?></a></li>
            <li><a href="<?= site_url('customer/order') ?>"><?= lang('text_order') ?></a></li>
            <?php
            $addon_digital = $this->config->item('addon_digital');
            if (!empty($addon_digital['active'])) {
                ?>
                <li><a href="<?= site_url('digital') ?>"><?= lang('text_order_digital') ?></a></li>
            <?php } ?>
            <li><a href="<?= site_url('customer/transaction') ?>"><?= lang('text_transaction') ?></a></li>
            <li><a href="<?= site_url('customer/edit') ?>"><?= lang('text_edit') ?></a></li>
            <li><a href="<?= site_url('customer/password') ?>"><?= lang('text_password') ?></a></li>
            <li><a href="<?= site_url('customer/address') ?>"><?= lang('text_address') ?></a></li>
            <li><a href="<?= site_url('customer/logout') ?>"><?= lang('text_logout') ?></a></li>
        </ul>
    </div>
</div>

<script>
    $(document).ready(function () {
        var query = String(document.location).split('?');
        var arrPath = query[0].split('/').splice(0, 6);
        $('.list-box li a').each(function (i, val) {
            if (arrPath.join('/') === this.href) {
                $(this).closest('li').addClass('active');
            }
        });
    });
</script>