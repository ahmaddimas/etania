<div class="breadcrumb full-width">
    <div class="background-breadcrumb"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <h1 id="title-page"><?= lang('heading_title') ?></h1>
                    <ul>
                        <?php foreach ($template['breadcrumbs'] as $breadcrumb) { ?>
                            <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content full-width inner-page">
    <div class="background-content"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 sign-in-page">
                        <div class="row">
                            <div class="col-md-12 center-column content-with-background" id="content">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5 sign-in">
                                        <h3><?= $text_register ?></h3>
                                        <p><?= $text_account_already ?></p>
                                        <?= form_open($action, 'id="form-register" class="register-form outer-top-xs"') ?>
                                        <input type="hidden" name="type" value="<?= $type ?>">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control unicase-form-control"
                                                   placeholder="<?= lang('entry_name') ?>">
                                        </div>
                                        <div class="form-group">
                                            <label class="radio-inline"><input type="radio" name="gender" value="m"
                                                                               checked="checked"> <?= lang('entry_male') ?>
                                            </label>
                                            <label class="radio-inline"><input type="radio" name="gender"
                                                                               value="f"> <?= lang('entry_female') ?>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label><?= lang('entry_dob') ?></label>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <select name="dob_date" class="form-control unicase-form-control">
                                                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                                                            <option value="<?= $i ?>"><?= $i ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-5">
                                                    <select name="dob_month" class="form-control unicase-form-control">
                                                        <?php foreach ($months as $key => $value) { ?>
                                                            <option value="<?= $key ?>"><?= $value ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select name="dob_year" class="form-control unicase-form-control">
                                                        <?php for ($i = 1950; $i <= 1999; $i++) { ?>
                                                            <option value="<?= $i ?>"><?= $i ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="telephone"
                                                   placeholder="<?= lang('entry_telephone') ?>"
                                                   class="form-control unicase-form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="email" placeholder="<?= lang('entry_email') ?>"
                                                   class="form-control unicase-form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password"
                                                   placeholder="<?= lang('entry_password') ?>"
                                                   class="form-control unicase-form-control">
                                        </div>
                                        <p><?= lang('text_agree') ?></p>
                                        <div class="form-group">
                                            <button class="btn btn-block btn-primary btn-lg" id="button-register"><i
                                                        class="fa fa-user-plus"></i> <?= lang('button_register') ?>
                                            </button>
                                        </div>
                                        </form>
                                    </div>
                                    <?php if ($this->config->item('oauth_facebook_status') || $this->config->item('oauth_google_status')) { ?>
                                        <div class="col-sm-2 sep" style="height:620px;">
                                            <span class="sepText"><?= lang('text_or') ?></span>
                                        </div>
                                        <div class="col-md-5 col-sm-5 sign-in">
                                            <div class="social-sign-in">
                                                <?php if ($this->config->item('oauth_facebook_status')) { ?>
                                                    <a href="<?= user_url('facebook_login') ?>"
                                                       class="facebook-sign-in btn btn-block"><i
                                                                class="fa fa-facebook"></i> <?= lang('button_facebook_login') ?>
                                                    </a><br>
                                                <?php } ?>
                                                <?php if ($this->config->item('oauth_google_status')) { ?>
                                                    <a href="<?= user_url('google_login') ?>"
                                                       class="google-sign-in btn btn-block"><i
                                                                class="fa fa-google"></i> <?= lang('button_google_login') ?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).delegate('#button-register', 'click', function (e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            url: $('#form-register').attr('action'),
            data: $('#form-register').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.alert, .error').remove();
                $('.form-group').removeClass('has-error');
                if (json['redirect']) {
                    window.location = json['redirect'];
                } else if (json['error']) {
                    for (i in json['error']) {
                        $('input[name=\'' + i + '\']').after('<small class="help-block error"><i>' + json['error'][i] + '</i></small>');
                        $('input[name=\'' + i + '\']').parent().addClass('has-error');
                    }
                }
            }
        });
    });
</script>