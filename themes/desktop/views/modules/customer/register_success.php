<div class="breadcrumb full-width">
    <div class="background-breadcrumb"></div>
    <div class="background">
        <div class="shadow"></div>
        <div class="pattern">
            <div class="container">
                <div class="clearfix">
                    <h1 id="title-page"><?= lang('text_success') ?></h1>
                    <ul>
                        <p class="lead"><?= sprintf(lang('text_success_message'), site_url('customer/login')) ?></p>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>