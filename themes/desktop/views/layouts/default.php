<!DOCTYPE html>
<!--[if IE 7]>
<html lang="en" class="ie7 responsive"> <![endif]-->
<!--[if IE 8]>
<html lang="en" class="ie8 responsive"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 responsive"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="responsive">
<!--<![endif]-->

<head>
    <title>
        <?= $template['title'] ?>
    </title>
    <base href="<?= base_url() ?>"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= $template['metadata'] ?>

    <link href="<?= $_favicon ?>" rel="icon"/>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="/themes/desktop/assets/js/jquery/magnific/magnific-popup.css"
          media="screen"/>

    <?php Asset::css('bootstrap.css', true); ?>
    <?php Asset::css('font-awesome.min.css', true); ?>
    <?php Asset::css('animate.css', true); ?>
    <?php Asset::css('stylesheet.css', true); ?>
    <?php Asset::css('responsive.css', true); ?>
    <?php Asset::css('owl.carousel.css', true); ?>
    <?php Asset::css('filter_product.css', true); ?>
    <?php Asset::css('megamenu.css', true); ?>

    <?php Asset::js('jquery-2.1.1.min.js', true); ?>
    <?php Asset::js('jquery-migrate-1.2.1.min.js', true); ?>
    <?php Asset::js('jquery.easing.1.3.js', true); ?>
    <?php Asset::js('bootstrap.min.js', true); ?>
    <?php Asset::js('twitter-bootstrap-hover-dropdown.js', true); ?>
    <?php Asset::js('common.js', true); ?>
    <?php Asset::js('bootstrap-notify.min.js', true); ?>
    <?php Asset::js('jquery.plugin.min.js', true); ?>
    <?php Asset::js('jquery.countdown.min.js', true); ?>
    <?php Asset::js('owl.carousel.min.js', true); ?>
    <?php Asset::js('jquery-ui-1.10.4.custom.min.js', true); ?>
    <?php Asset::js('jquery/magnific/jquery.magnific-popup.min.js', true); ?>

    <?= Asset::render() ?>

    <?= $template['css'] ?>
    <?= $template['js'] ?>
    <!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="/themes/desktop/assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="common-home body-white">
<div class="standard-body">
    <div id="main">
        <header class="full-width">
            <div class="navbar navbar-default line-navbar-two navbar-fixed-top top-nav-collapse line-navbar-two hidden-sm hidden-xs"
                 role="navigation">
                <div class="container">
                    <div class="collapse navbar-collapse">
                        <div class="navbar-header">
                            <a type="button" class="navbar-toggle" data-toggle="collapse"
                               data-target=".navbar-collapse"> <i class="fa fa-search fa-lg"></i></a>
                            <a class="navbar-toggle"><i class="fa fa-shopping-basket fa-lg"></i></a>
                            <a class="navbar-toggle"><i class="fa fa-user fa-lg"></i></a>
                            <?php if (!empty($header_banners)) { ?>
                                <div class="navbar-brand" style="min-width:130px; max-height:30px;">
                                    <a style="width:100% !important" href="<?= site_url() ?>"><img src="<?= $_logo ?>"
                                                                                                   class="logo"></a>
                                </div>

                                <?php foreach ($header_banners as $banner) { ?>
                                    <div class="navbar-brand" style="min-width:130px; max-height:30px;">
                                        <a href="<?= $banner['link'] ?>" title="<?= $banner['link_title'] ?>"><img
                                                    style="padding:0px; width:130px; max-height:30px;"
                                                    src="<?= $banner['image'] ?>" alt="<?= $banner['title'] ?>"></a>
                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <a class="navbar-brand" href="<?= site_url() ?>"><img src="<?= $_logo ?>" class="logo"></a>
                            <?php } ?>
                        </div>
                        <ul class="nav navbar-nav lnt-nav-mega">
                            <li class="dropdown">
                                <button href="#" class="dropdown-toggle btn btn-default navbar-btn"
                                        data-toggle="dropdown" role="button" aria-expanded="false"><i
                                            class="fa fa-list fa-lg"></i></button>
                                <div class="dropdown-menu" role="menu">
                                    <div class="lnt-dropdown-mega-menu">
                                        <?php if ($category_menu) { ?>
                                            <ul class="lnt-category list-unstyled">
                                                <?php foreach ($category_menu as $key => $category) { ?>
                                                    <?php if ($key == 0) { ?>
                                                        <li class="active"><a href="<?= $category['href'] ?>"
                                                                              rel="#subcategory-<?= $key ?>"><?= $category['name'] ?></a>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li><a href="<?= $category['href'] ?>"
                                                               rel="#subcategory-<?= $key ?>"><?= $category['name'] ?></a>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            </ul>
                                            <div class="lnt-subcategroy-carousel-wrap">
                                                <?php foreach ($category_menu as $key => $category) { ?>
                                                    <?php $class = $key == 0 ? 'active' : ''; ?>
                                                    <div id="subcategory-<?= $key ?>" class="<?= $class ?>">
                                                        <div class="lnt-subcategory col-sm-12 col-md-12">
                                                            <?php if ($category['children']) { ?>
                                                                <?php foreach ($category['children'] as $children) { ?>
                                                                    <ul class="list-unstyled col-sm-4">
                                                                        <li class="lnt-category-name"><a
                                                                                    href="<?= $children['href'] ?>"><?= $children['name'] ?></a>
                                                                        </li>
                                                                        <?php if ($children['children']) { ?>
                                                                            <?php foreach ($children['children'] as $subchildren) { ?>
                                                                                <li><a style="font-weight:normal;"
                                                                                       href="<?= $subchildren['href'] ?>"
                                                                                       rel="<?= $subchildren['href'] ?>"><?= $subchildren['name'] ?></a>
                                                                                </li>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </ul>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?= Modules::run('checkout/cart/info') ?>
                            <?php if ($this->customer->is_logged()) { ?>
                                <li>
                                    <button type="button" onclick="window.location = '<?= site_url('customer') ?>';"
                                            class="btn btn-default navbar-btn"><?= $this->customer->name() ?></button>
                                </li>
                                <li>
                                    <button type="button"
                                            onclick="window.location = '<?= site_url('customer/logout') ?>';"
                                            class="btn btn-default navbar-btn">Logout
                                    </button>
                                </li>
                            <?php } else { ?>
                                <li>
                                    <button type="button"
                                            onclick="window.location = '<?= site_url('customer/login') ?>';"
                                            class="btn btn-default navbar-btn">Login
                                    </button>
                                </li>
                                <li>
                                    <button type="button"
                                            onclick="window.location = '<?= site_url('customer/register') ?>';"
                                            class="btn btn-default navbar-btn">Daftar
                                    </button>
                                </li>
                            <?php } ?>
                        </ul>
                        <form class="navbar-form" action="<?= site_url('search') ?>">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" autocomplete="off"
                                           placeholder="<?= lang('text_search') ?>"> <span class="input-group-btn"><button
                                                class="btn btn-primary"><i
                                                    class="fa fa-search fa-lg"></i></button></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </header>
        <?= $template['body'] ?>
        <div class="footer full-width">
            <div class="background">
                <div style="background-color:#fff;border-bottom:1px solid #efefef;">
                    <div class="container">
                        <div class="advanced-grid">
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row footer-blocks-top-app">
                                                <div class="col-sm-4">
                                                    <div class="footer-block-app">
                                                        <span class="fa fa-phone fa-3x"
                                                              style="margin-right:10px;"></span>
                                                        <div class="footer-block-content">
                                                            <h6><?= lang('footer_telephone') ?></h6>
                                                            <p><?= $this->config->item('telephone') ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div class="footer-block-app">
                                                        <span class="fa fa-envelope-o fa-3x"
                                                              style="margin-right:10px;"></span>
                                                        <div class="footer-block-content">
                                                            <h6><?= lang('footer_email') ?></h6>
                                                            <p><?= $this->config->item('email') ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 text-right">
                                                    <button onclick="window.location = '<?= site_url('contact') ?>'"
                                                            class="btn btn-primary"><?= lang('footer_contact') ?></button>
                                                </div>
                                            </div>

                                            <?php if (!empty($footer_banners)) { ?>
                                                <div class="row banners banners-with-padding-45">
                                                    <?php foreach ($footer_banners as $banner) { ?>
                                                        <div class="col-sm-6">
                                                            <a href="<?= $banner['link'] ?>"
                                                               title="<?= $banner['link_title'] ?>"><img
                                                                        src="<?= $banner['image'] ?>"
                                                                        alt="<?= $banner['title'] ?>"></a>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding-top:20px;">
                    <div class="container">
                        <div class="advanced-grid">
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4><?= $this->config->item('company') ?></h4>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <p style="text-align:justify;"><?= $this->config->item('meta_description') ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4><?= lang('footer_information') ?></h4>
                                            <div class="strip-line"></div>
                                            <div class="row" style="clear: both">
                                                <div class="col-sm-12">
                                                    <ul>
                                                        <?php foreach ($page_menu as $page) { ?>
                                                            <?php if ($page['footer_column'] == 1) { ?>
                                                                <li>
                                                                    <a href="<?= site_url('page/' . $page['slug']) ?>"><?= $page['title'] ?></a>
                                                                </li>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4><?= lang('footer_helper') ?></h4>
                                            <div class="strip-line"></div>
                                            <div class="row" style="clear: both">
                                                <div class="col-sm-12">
                                                    <ul>
                                                        <?php foreach ($page_menu as $page) { ?>
                                                            <?php if ($page['footer_column'] == 2) { ?>
                                                                <li>
                                                                    <a href="<?= site_url('page/' . $page['slug']) ?>"><?= $page['title'] ?></a>
                                                                </li>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <li>
                                                            <a href="<?= site_url('contact') ?>"><?= lang('footer_contact') ?></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <h4><?= lang('footer_account') ?></h4>
                                            <div class="strip-line"></div>
                                            <div class="row" style="clear: both">
                                                <div class="col-sm-12">
                                                    <ul>
                                                        <li>
                                                            <a href="<?= site_url('customer/login') ?>"><?= lang('footer_login') ?></a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= site_url('customer/order') ?>"><?= lang('footer_order') ?></a>
                                                        </li>
                                                        <?php if ($this->config->item('addon_custom')) { ?>
                                                            <li>
                                                                <a href="<?= site_url('custom') ?>"><?= lang('footer_custom') ?></a>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="text-center">
                                                <?= $_copyright ?>
                                                <ul class="social-icons-default" style="padding-top: 7px">
                                                    <?php if ($this->config->item('facebook_user')) { ?>
                                                        <li><a target="_blank"
                                                               href="http://www.facebook.com/<?= $this->config->item('facebook_user') ?>"
                                                               title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                                    <?php } ?>
                                                    <?php if ($this->config->item('twitter_user')) { ?>
                                                        <li><a target="_blank"
                                                               href="http://www.twitter.com/<?= $this->config->item('twitter_user') ?>"
                                                               title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                                    <?php } ?>
                                                    <?php if ($this->config->item('gplus_user')) { ?>
                                                        <li><a target="_blank"
                                                               href="https://plus.google.com<?= $this->config->item('gplus_user') ?>"
                                                               title="GooglePlus"><i class="fa fa-google"></i></a></li>
                                                    <?php } ?>
                                                    <?php if ($this->config->item('youtube_user')) { ?>
                                                        <li><a target="_blank"
                                                               href="https://www.youtube.com/channel/<?= $this->config->item('youtube_user') ?>"
                                                               title="Youtube"><i class="fa fa-youtube-play"></i></a>
                                                        </li>
                                                    <?php } ?>
                                                    <?php if ($this->config->item('instagram_user')) { ?>
                                                        <li><a target="_blank"
                                                               href="http://www.instagram.com/<?= $this->config->item('instagram_user') ?>"
                                                               title="Instagram"><i class="fa fa-instagram"></i></a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    !function () {
        "use strict";

        function a() {
            l.addClass("lnl-show").removeClass("lnl-hide"), n.addClass(i).css({
                height: o,
                overflow: "hidden"
            }), $("html, body").css("overflow", "hidden"), s.find("span").removeClass("fa-bars").addClass("fa-remove")
        }

        function e() {
            l.removeClass("lnl-show").addClass("lnl-hide"), n.removeClass(i).css({
                height: "auto",
                overflow: "auto"
            }), $("html, body").css("overflow", "auto"), s.find("span").removeClass("fa-remove").addClass("fa-bars")
        }

        function t() {
            try {
                return document.createEvent("TouchEvent"), !0
            } catch (a) {
                return !1
            }
        }

        var l = $(".line-navbar-left"), s = $(".lno-btn-toggle"), n = ($(".lno-btn-collapse"), $(".content-wrap")),
            i = n.data("effect"), o = $(window).height() - 95;
        l.addClass("lnl-hide"), s.click(function () {
            l.hasClass("lnl-hide") ? a() : e()
        }), s.click(function (a) {
            a.preventDefault(), l.hasClass("lnl-collapsed") ? (l.removeClass("lnl-collapsed"), n.removeClass("lnl-collapsed"), $(this).find(".lnl-link-icon").removeClass("fa-arrow-right").addClass("fa-arrow-left")) : (l.addClass("lnl-collapsed"), n.addClass("lnl-collapsed"), $(this).find(".lnl-link-icon").removeClass("fa-arrow-left").addClass("fa-arrow-right"))
        }), 1 == t() && $(window).swipe({
            swipeRight: function () {
                a(), $(".navbar-collapse").removeClass("in")
            }, swipeLeft: function () {
                e()
            }, threshold: 75
        }), $(window).resize(function () {
            $(window).width() >= 767 && e()
        }), $(".lnt-search-input").focusin(function () {
            $(".lnt-search-suggestion").find(".dropdown-menu").slideDown()
        }), $(".lnt-search-input").focusout(function () {
            $(".lnt-search-suggestion").find(".dropdown-menu").slideUp()
        }), $(" .lnt-search-category ").find(" .dropdown-menu ").find(" li ").click(function () {
            var a = $(this).find(" a ").text();
            $(" .selected-category-text ").text(a)
        }), $(".lnt-search-input").bind("keyup change", function () {
            var a = $(this).val(), e = $(".lnt-search-suggestion").find(".dropdown-menu > li > a");
            e.unhighlight({element: "strong", className: "important"}), a && e.highlight(a, {
                element: "strong",
                className: "important"
            })
        });
        var r = $(".lnt-category").find("li").find("a"), c = $(".lnt-category").find("li");
        r.mouseenter(function () {
            c.removeClass("active"), $(this).parent().addClass("active");
            var a = $(this).attr("rel");
            $(".lnt-subcategroy-carousel-wrap").find("> div").removeClass("active"), $(a).addClass("active")
        }), r.on("touchstart, touchend", function (a) {
            a.preventDefault(), c.removeClass("active"), $(this).parent().addClass("active");
            var e = $(this).attr("rel");
            $(".lnt-subcategroy-carousel-wrap").find("> div").removeClass("active"), $(e).addClass("active")
        }), 1 == t() && ($(window).swipe({
            swipeLeft: function () {
                $(".carousel").carousel("next")
            }, swipeRight: function () {
                $(".carousel").carousel("prev")
            }, threshold: 75
        }), $(".carousel-indicators").hide()), $(".carousel-indicators").find("li").mouseenter(function () {
            var a = $(this).data("slide-to");
            $(this).parents(".carousel").carousel(a)
        }), String.prototype.capitalize = function () {
            return this.replace(/(?:^|\s)\S/g, function (a) {
                return a.toUpperCase()
            })
        }, $(".lnt-category > li").each(function () {
            var a = $(this).find("a");
            $(".lnl-nav").append("<li><a class='collapsed' data-toggle='collapse' rel='#collapse" + a.text().capitalize().replace(/[, ]+/g, "") + "' aria-expanded='false' aria-controls='collapse" + a.text().capitalize().replace(/[, ]+/g, "") + "' data-subcategory=" + a.attr("rel").replace("#", "") + "><span class='lnl-link-text'>" + $(this).text() + "</span><span class='fa fa-angle-up lnl-btn-sub-collapse'></span></a></li>")
        }), $(".lnl-nav li").each(function () {
            var a = $(this).find("a");
            $(this).append("<ul class='lnl-sub-one collapse' id='" + a.attr("rel").replace("#", "") + "' data-subcategory='" + a.data("subcategory") + "'></ul>")
        }), $(".lnt-subcategroy-carousel-wrap > div").each(function () {
            var a = $(this).attr("id"), e = $(this).find("ul").map(function () {
                return $(this).html()
            }).get();
            $("ul[data-subcategory='" + a + "']").append(e)
        }), $(".navbar-toggle").click(function () {
            $(this).hasClass("collapsed") && e()
        }), s.click(function () {
            $(".navbar-collapse").removeClass("in")
        })
    }();
</script>
<?= $this->config->item('livechat_code') ?>
</body>
</html>