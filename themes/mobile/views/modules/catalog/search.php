<div style="margin-bottom:35px;">
    <div class="" style="margin:0 -5px;">
        <div class="row">
            <div class="col-xs-12">
                <h4 style="margin-left:-10px; border-left:5px solid #ffcc00;padding-left:8px;"><?= lang('heading_title') ?></h4>
            </div>
        </div>

        <div class="row" style="padding:10px">
            <div class="col-sm-12 col-xs-12 product-grid">
                <select class="sorter" style="width:100%">
                    <?php foreach ($sorts as $sort_data) { ?>
                        <?php if ($sort_data['value'] == $sort . '-' . $order) { ?>
                            <option value="<?= $sort_data['href'] ?>"><?= $sort_data['text'] ?></option>
                        <?php } else { ?>
                            <option value="<?= $sort_data['href'] ?>"><?= $sort_data['text'] ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
            <?php if ($products) { ?>
                <?php foreach ($products as $product) { ?>
                    <div class="col-sm-6 col-xs-6 product-grid" style="padding:0;">
                        <div class="product-thumb" style="margin:5px;">
                            <?php if ($product['wholesaler']) { ?>
                                <span class="sale">Grosir</span>
                            <?php } elseif ($product['discount_percent']) { ?>
                                <span class="sale"><?= $product['discount_percent'] ?></span>
                            <?php } ?>
                            <?php if ($product['free_shipping'] == 1) { ?>
                                <div style="position: absolute; right: 0px; padding: 15px 20px 0 0;"><img
                                            style="width:80px; height:20px"
                                            src="<?= site_url('storage/images/' . $free_shipping['free_shipping_logo']) ?>"
                                            alt="Free Shipping"/></div>
                            <?php } ?>
                            <div class="image"><a href="<?= $product['href'] ?>"><img src="<?= $product['thumb'] ?>"
                                                                                      alt="<?= $product['name'] ?>"
                                                                                      title="<?= $product['name'] ?>"
                                                                                      class="img-responsive"/></a></div>
                            <div class="rating">
                                <?php $star = ''; ?>
                                <?php for ($x = 1; $x <= 5; $x++) { ?>
                                    <?php if ($x <= $product['rating']) { ?>
                                        <?php $star .= '<span class="active"><i class="fa fa-star"></i></span>'; ?>
                                    <?php } else { ?>
                                        <?php $star .= '<span><i class="fa fa-star"></i></span>'; ?>
                                    <?php } ?>
                                <?php } ?>
                                <?= $star ?>
                            </div>
                            <div class="caption">
                                <div class="name"><?= cut_text($product['name'], 38) ?></div>
                                <div class="price">
                                    <?php if ($product['discount']) { ?>
                                        <p class="price"><span class="price-old" style="margin-left:0;"><small><?= $product['price'] ?></small></span><br><span
                                                    class="price-new"><?= $product['discount'] ?></span></p>
                                    <?php } else { ?>
                                        <p class="price"><span class="price-new"><?= $product['price'] ?></span></p>
                                    <?php } ?>
                                </div>
                                <button class="btn btn-primary btn-block"
                                        onclick="cart.add('<?= $product['product_id'] ?>');"><i
                                            class="fa fa-shopping-bag"></i> <span><?= lang('button_cart') ?></span>
                                </button>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="col-sm-12 col-xs-12 product-grid" style="align:center;">
                    <div class=" pagination-results">
                        <?= $pagination ?>
                    </div>
                </div>
            <?php } else { ?>
                <p><?= lang('text_empty') ?></p>
            <?php } ?>
        </div>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        setDisplay();
    });

    $(document).delegate('.card .pagination li a', 'click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        if (typeof (href) !== 'undefined') {
            $('.card').load(href + ' #product-list', function (response, status, xhr) {
                setDisplay();
            });

            $('html, body').animate({scrollTop: 0}, 'slow');
            history.pushState('', 'Daftar Produk', href);
        }
    });

    $('select[class=\'sorter\']').on('change', function (e) {
        e.preventDefault();
        var href = $(this).val();
        if (typeof (href) !== 'undefined') {
            $('.card').load(href + ' #product-list', function (response, status, xhr) {
                setDisplay();
            });

            $('html, body').animate({scrollTop: 0}, 'slow');
            history.pushState('', 'Daftar Produk', href);
        }
    });

    $('input[name=\'filter[]\']').on('change', function () {
        var url = $('#form-filter').attr('action') + '?' + $('#form-filter').serialize();
        $('.card').load(url + ' #product-list', function (response, status, xhr) {
            setDisplay();
        });

        $('html, body').animate({scrollTop: 0}, 'slow');
        history.pushState('', 'Daftar Produk', url);
    });

    function setDisplay(view) {
        $('.quickview a').magnificPopup({
            preloader: true,
            tLoading: '',
            type: 'iframe',
            mainClass: 'quickview',
            removalDelay: 200,
            gallery: {
                enabled: true
            }
        });
    }
</script>