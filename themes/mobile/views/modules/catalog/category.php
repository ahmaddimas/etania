<div style="margin-bottom:15px;">
    <div class="" style="margin:0 -5px;">
        <div class="row">
            <div class="col-xs-12">
                <h4 style="margin-left:-10px; border-left:5px solid #ffcc00;padding-left:8px;"><?= $name ?></h4>
            </div>
        </div>
        <div class="row">
            <?php foreach ($products as $product) { ?>
                <div class="col-sm-6 col-xs-6 product-grid" style="padding:0;">
                    <div class="product-thumb" style="margin:5px;">
                        <?php if ($product['wholesaler']) { ?>
                            <span class="sale">Grosir</span>
                        <?php } elseif ($product['discount_percent']) { ?>
                            <span class="sale"><?= $product['discount_percent'] ?></span>
                        <?php } ?>
                        <?php if ($product['free_shipping'] == 1) { ?>
                            <div style="position: absolute; right: 0px; padding: 15px 20px 0 0;"><img
                                        style="width:80px; height:20px"
                                        src="<?= site_url('storage/images/' . $free_shipping['free_shipping_logo']) ?>"
                                        alt="Free Shipping"/></div>
                        <?php } ?>
                        <div class="image"><a href="<?= $product['href'] ?>"><img src="<?= $product['thumb'] ?>"
                                                                                  alt="<?= $product['name'] ?>"
                                                                                  title="<?= $product['name'] ?>"
                                                                                  class="img-responsive"/></a></div>
                        <div class="rating">
                            <?php $star = ''; ?>
                            <?php for ($x = 1; $x <= 5; $x++) { ?>
                                <?php if ($x <= $product['rating']) { ?>
                                    <?php $star .= '<span class="active"><i class="fa fa-star"></i></span>'; ?>
                                <?php } else { ?>
                                    <?php $star .= '<span><i class="fa fa-star"></i></span>'; ?>
                                <?php } ?>
                            <?php } ?>
                            <?= $star ?>
                        </div>
                        <div class="caption">
                            <div class="name"><?= cut_text($product['name'], 38) ?></div>
                            <br>
                            <div class="price">
                                <?php if ($product['discount']) { ?>
                                    <p class="price"><span class="price-old" style="margin-left:0;"><small><?= $product['price'] ?></small></span><br><span
                                                class="price-new"><?= $product['discount'] ?></span></p>
                                <?php } else { ?>
                                    <p class="price"><span class="price-new"><?= $product['price'] ?></span></p>
                                <?php } ?>
                            </div>
                            <button class="btn btn-primary btn-block"
                                    onclick="cart.add('<?= $product['product_id'] ?>');"><i
                                        class="fa fa-shopping-bag"></i> <span><?= lang('button_cart') ?></span></button>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation" style="border-top:1px solid #eee;">
    <div class="container">
        <div class="navbar-header" style="padding:10px;">
            <button type="button" class="btn btn-default btn-block btn-lg" data-toggle="collapse"
                    data-target="#bs-collapse-filter"><?= lang('text_refine') ?> <i class="fa fa-sort fa-lg"></i>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-collapse-filter">
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <label><?= lang('text_sort') ?></label>
                    <select class="form-control sorting"
                            onchange="window.location = $(this).find('option:selected').val();">
                        <?php foreach ($sorts as $sort_data) { ?>
                            <?php if ($sort_data['value'] == $sort . '-' . $order) { ?>
                                <option value="<?= $sort_data['href'] ?>"
                                        selected="selected"><?= $sort_data['text'] ?></option>
                            <?php } else { ?>
                                <option value="<?= $sort_data['href'] ?>"><?= $sort_data['text'] ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </form>
        </div>
    </div>
</nav>
<script type="text/javascript">
    window.onscroll = function (ev) {
        if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight) {
            $('.navbar-fixed-bottom').fadeOut('slow');
        } else {
            $('.navbar-fixed-bottom').fadeIn('slow');
        }
    };
</script>