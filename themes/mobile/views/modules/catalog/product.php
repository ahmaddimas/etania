<link href="<?= base_url('assets/plugins/magnific/magnific-popup.css') ?>" rel="stylesheet">
<div class="page-title">
    <h1><?= $name ?></h1>
</div>
<div class="card full-width">
    <div id="message"></div>
    <?= form_open(site_url('checkout/cart/add'), 'id="form-cart"') ?>
    <div class="row" style="padding-bottom:22px;">
        <div class="col-sm-12">
            <div id="notification"></div>
            <div class="row">
                <div class="col-sm-6">
                    <?php if ($thumb || $images) { ?>
                        <ul class="thumbnails">
                            <?php if ($thumb) { ?>
                                <li><a class="thumbnail" href="<?php echo $popup; ?>" title=""><img
                                                src="<?php echo $thumb; ?>" title="" alt="" class="img-responsive"/></a>
                                </li>
                            <?php } ?>
                            <?php if ($images) { ?>
                                <?php foreach ($images as $image) { ?>
                                    <li class="image-additional"><a class="thumbnail"
                                                                    href="<?php echo $image['popup']; ?>" title=""> <img
                                                    src="<?php echo $image['thumb']; ?>" title="" alt=""
                                                    style="width:50px; height:auto;"/></a></li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
                <div class="col-sm-6">
                    <input type="hidden" value="<?= $product_id ?>" name="product_id">
                    <div class="product-information">
                        <p><?= lang('text_model') ?> : <?= $model ?></p>
                        <p><?= lang('text_stock') ?> : <?= $stock_status ?></p>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $this->config->item('sharing_button_code') ?>
                            </div>
                        </div>
                        <hr>
                        <span>
						<?php if ($discount) { ?>
                            <span style="color: #999;text-decoration: line-through;font-weight:none;font-size:16px;"><?= $price ?></span>
                            <br>
                            <span><?= $discount ?></span>
                        <?php } else { ?>
                            <span><?= $price ?></span>
                        <?php } ?>
					</span>

                        <?php if ($discounts) { ?>
                            <br>
                            <p><label><?= lang('text_discount') ?></label></p>
                            <table class="table table-bordered">
                                <?php foreach ($discounts as $discount) { ?>
                                    <tr>
                                        <td>&ge; <?= $discount['quantity'] ?></td>
                                        <td><?= $discount['price'] ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        <?php } ?>
                        <hr>

                        <?php if ($options) { ?>
                        <div class="options">
                            <h2><?= lang('text_option') ?></h2>
                            <?php foreach ($options

                            as $option) { ?>
                            <?php if ($option['type'] == 'select') { ?>
                            <?php if ($option['required']) { ?>
                            <div class="form-group required">
                                <?php } else { ?>
                                <div class="form-group">
                                    <?php } ?>
                                    <label class="control-label" for="input-option<?= $option['product_option_id'] ?>">
                                        <?= $option['name'] ?>
                                    </label>
                                    <select name="option[<?= $option['product_option_id'] ?>]"
                                            id="input-option<?= $option['product_option_id'] ?>" class="form-control"
                                            required>
                                        <option value=""><?= lang('text_select') ?></option>
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <option value="<?= $option_value['product_option_value_id'] ?>"><?= $option_value['name'] ?>
                                                <?php if ($option_value['price'] > 0) { ?>
                                                    (<?= $option_value['price'] ?>)
                                                <?php } ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <?php }elseif ($option['type'] == 'radio'){ ?>
                            <?php if ($option['required']) { ?>
                                <div class="form-group required">
                                    <?php } else { ?>
                                    <div class="form-group">
                                        <?php } ?>
                                        <label class="control-label"
                                               for="input-option<?= $option['product_option_id'] ?>">
                                            <?= $option['name'] ?>
                                        </label>
                                        <input type="radio" name="option[<?= $option['product_option_id'] ?>]"
                                               id="input-option<?= $option['product_option_id'] ?>" value=""
                                               selected="true" checked="true" style="display:none" required>
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <input type="radio" name="option[<?= $option['product_option_id'] ?>]"
                                                   id="input-option<?= $option['product_option_id'] ?>"
                                                   value="<?= $option_value['product_option_value_id'] ?>"
                                                   required><?= $option_value['name'] ?>
                                            <?php if ($option_value['price'] > 0) { ?>
                                                (<?= $option_value['price'] ?>)
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#details"
                                                      data-toggle="tab"><?= lang('tab_description') ?></a></li>
                                <li><a href="#attribute" data-toggle="tab"><?= lang('tab_attribute') ?></a></li>
                                <?php foreach ($product_tabs as $key => $product_tab) { ?>
                                    <li><a href="#tab-custom-<?= $key ?>"
                                           data-toggle="tab"><?= $product_tab['title'] ?></a></li>
                                <?php } ?>
                                <li><a href="#review"
                                       data-toggle="tab"><?= sprintf(lang('tab_review'), (int)$reviews) ?></a></li>
                            </ul>
                            <div class="tab-content" style="padding-top:20px;">
                                <div class="tab-pane fade active in" id="details">
                                    <?= $description ?>
                                </div>
                                <div class="tab-pane fade" id="attribute">
                                    <?php if ($attribute_groups) { ?>
                                        <table class="table table-bordered">
                                            <?php foreach ($attribute_groups as $attribute_group) { ?>
                                                <thead>
                                                <tr>
                                                    <th colspan="2"
                                                        style="background:#f6f6f6;"><?= $attribute_group['name'] ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                    <tr>
                                                        <td><?= $attribute['name'] ?></td>
                                                        <td><?= $attribute['text'] ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            <?php } ?>
                                        </table>
                                    <?php } ?>
                                </div>
                                <?php foreach ($product_tabs as $key => $product_tab) { ?>
                                    <div class="tab-pane fade"
                                         id="tab-custom-<?= $key ?>"><?= $product_tab['content'] ?></div>
                                <?php } ?>
                                <div class="tab-pane fade" id="review">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($relates) { ?>
            <div>
                <div class="container" style="margin:0 -5px;">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 style="margin-left:-10px; border-left:5px solid #ffcc00;padding-left:8px;"><?= lang('text_related') ?></h4>
                        </div>
                    </div>
                    <div class="row">
                        <?php foreach ($relates as $product) { ?>
                            <div class="col-sm-6 col-xs-6 product-grid" style="padding:0;">
                                <div class="product-thumb" style="margin:5px;">
                                    <?php if ($product['wholesaler']) { ?>
                                        <span class="sale">Grosir</span>
                                    <?php } elseif ($product['discount_percent']) { ?>
                                        <span class="sale"><?= $product['discount_percent'] ?></span>
                                    <?php } ?>
                                    <div class="image"><a href="<?= $product['href'] ?>"><img
                                                    src="<?= $product['thumb'] ?>" alt="<?= $product['name'] ?>"
                                                    title="<?= $product['name'] ?>" class="img-responsive"/></a></div>
                                    <div class="rating">
                                        <?php $star = ''; ?>
                                        <?php for ($x = 1; $x <= 5; $x++) { ?>
                                            <?php if ($x <= $product['rating']) { ?>
                                                <?php $star .= '<span class="active"><i class="fa fa-star"></i></span>'; ?>
                                            <?php } else { ?>
                                                <?php $star .= '<span><i class="fa fa-star"></i></span>'; ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <?= $star ?>
                                    </div>
                                    <div class="caption">
                                        <div class="name"><?= cut_text($product['name'], 38) ?></div>
                                        <div class="price">
                                            <?php if ($product['discount']) { ?>
                                                <p class="price"><span class="price-old" style="margin-left:0;"><small><?= $product['price'] ?></small></span><br><span
                                                            class="price-new"><?= $product['discount'] ?></span></p>
                                            <?php } else { ?>
                                                <p class="price"><span class="price-new"><?= $product['price'] ?></span>
                                                </p>
                                            <?php } ?>
                                        </div>
                                        <button class="btn btn-primary btn-block"
                                                onclick="cart.add('<?= $product['product_id'] ?>');"><i
                                                    class="fa fa-shopping-bag"></i>
                                            <span><?= lang('button_cart') ?></span></button>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
            <div class="container">
                <div class="navbar-header" style="padding:10px;">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="input-group input-group-lg">
						<span class="input-group-btn">
							<a class="btn btn-default button-quantity">-</a>
						</span>
                                <input type="text" class="form-control" name="quantity" value="1"
                                       style="text-align:center;" readonly="readonly">
                                <span class="input-group-btn">
						<a class="btn btn-default button-quantity">+</a>
						</span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <a style="line-height:26px;" id="button-cart" class="btn btn-primary btn-block btn-lg"><i
                                        class="fa fa-shopping-cart"></i> Beli</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        </form>
        <script type="text/javascript">
            window.onscroll = function (ev) {
                if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight) {
                    $('.navbar-fixed-bottom').fadeOut('slow');
                } else {
                    $('.navbar-fixed-bottom').fadeIn('slow');
                }
            };
        </script>
        <script src="<?= base_url('assets/plugins/magnific/jquery.magnific-popup.min.js') ?>"></script>
        <script src="<?= base_url('themes/mobile/assets/js/jscroll.min.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.thumbnails').magnificPopup({
                    type: 'image',
                    delegate: 'a',
                    gallery: {
                        enabled: true
                    }
                });

                $.ajax({
                    url: "<?=site_url('product/review')?>",
                    type: 'get',
                    data: 'product_id=<?=$product_id?>',
                    dataType: 'html',
                    success: function (html) {
                        $('#review').html('<div class="scroll">' + html + '</div>');
                        $('.scroll').jscroll({
                            autoTrigger: false,
                            loadingHtml: '<i class="fa fa-refresh fa-spin"></i> Loading...',
                            padding: 20,
                            nextSelector: 'a:last',
                            contentSelector: ''
                        });
                    }
                });
            });

            $('.button-quantity').on('click', function () {
                var btn = $(this);
                var oldVal = $('input[name=\'quantity\']').val();
                if (btn.text() == '+') {
                    var newVal = parseFloat(oldVal) + 1;
                } else {
                    if (oldVal > 1) {
                        var newVal = parseFloat(oldVal) - 1;
                    } else {
                        var newVal = 1;
                    }
                }

                $('input[name=\'quantity\']').val(newVal);
            });

            // $('#button-cart').on('click', function addToCart() {
            // 	$.ajax({
            // 			url: $('#form-cart').attr('action'),
            // 			type: 'post',
            // 			data: $('#form-cart').serialize(),
            // 			dataType: 'json',
            // 			success: function(json) {
            // 				$('.form-group').removeClass('has-error');
            // 				$('.text-danger').remove();
            // 				if (json['redirect']) {
            // 					location = json['redirect'];
            // 				}
            // 				if (json['success']) {
            // 					$('#cart-block').load('checkout/cart/info');
            // 				} else if (json['error']['option']) {
            // 					for (i in json['error']['option']) {
            // 						$('#input-option' + i).closest('.form-group').addClass('has-error');
            // 						$('#input-option' + i).after('<small class="text-danger"><i>' + json['error']['option'][i] + '</i></small>');
            // 					}
            // 				}
            // 				$('html, body').animate({ scrollTop: 0 }, 'slow');
            // 			}
            // 		});
            // });

            $('#button-cart').on('click', function () {
                $.ajax({
                    url: $('#form-cart').attr('action'),
                    type: 'post',
                    data: $('#form-cart').serialize(),
                    dataType: 'json',
                    success: function (json) {
                        $('.form-group').removeClass('has-error');
                        $('.text-danger').remove();
                        if (json['redirect']) {
                            location = json['redirect'];
                        }
                        if (json['success']) {
                            $('body').append('<div class="modal fade" id="cart-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['success'] + '</div>');
                            $('#cart-modal').modal('show');
                            $('#cart-modal').on('hidden.bs.modal', function (e) {
                                $('#cart-modal').remove();
                            });
                            $('#cart-block').load('checkout/cart/info');
                            $('#cart-block .cart-count').html(json['total']);
                        } else if (json['error']['option']) {
                            for (i in json['error']['option']) {
                                $('#input-option' + i).closest('.form-group').addClass('has-error');
                                $('#input-option' + i).after('<small class="text-danger"><i>' + json['error']['option'][i] + '</i></small>');
                            }
                        }
                    }
                });
            });
        </script>