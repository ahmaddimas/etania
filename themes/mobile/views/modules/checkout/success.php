<div class="card full-width">
    <h1><?= lang('text_success') ?></h1>
    <p><?= sprintf(lang('text_success_message'), $this->config->item('company')) ?></p>
    <a href="<?= site_url() ?>" class="btn btn-primary"><?= lang('button_continue') ?></a>
</div>