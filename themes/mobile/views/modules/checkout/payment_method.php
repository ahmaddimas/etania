<style>
    /* The container */
    .custom_radio {
        display: block;
        position: relative;
        padding-left: 25px !important;
        margin-bottom: 10px;
        cursor: pointer;
        font-size: 16px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .custom_radio input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px !important;
        width: 20px !important;
        background-color: #eee;
        border-radius: 50%;
        border: 2px solid #000;
    }

    /* On mouse-over, add a grey background color */
    .custom_radio:hover input ~ .checkmark {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .custom_radio input:checked ~ .checkmark {
        background-color: #000;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .custom_radio input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .custom_radio .checkmark:after {
        top: 3px;
        left: 3px;
        width: 10px;
        height: 10px;
        border-radius: 50%;
        background: white;
    }
</style>

<h4>Metode Pembayaran</h4><span class="text-muted"><?php echo $text_payment_method; ?></span>
<hr>
<?php if ($error_warning) { ?>
    <div class="warning alert alert-warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<?= form_open($action, 'id="form-pm"') ?>
<div class="form-group">
    <?php
    $no = 1;
    $cp = count($payment_methods);
    ?>
    <?php foreach ($payment_methods

                   as $payment_method) { ?>
    <div class="radio">
        <label class="custom_radio">
            <?php if ($payment_method['code'] == $code || !$code) { ?>
                <?php $code = $payment_method['code']; ?>
                <input type="radio" name="payment_method" onclick="pmethod(<?= $no ?>)"
                       value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>"
                       checked="checked"/>
            <?php } else { ?>
                <input type="radio" name="payment_method" onclick="pmethod(<?= $no ?>)"
                       value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>"/>
            <?php } ?>
            <?php echo $payment_method['title']; ?>
            <span class="checkmark"></span>
        </label>
    </div>

<?php if ($payment_method['code'] == $code || !$code) { ?>
<?php $code = $payment_method['code']; ?>
    <div style="margin-left:20px; display:block" id="<?= $no ?>">
        <?php } else { ?>
        <div style="margin-left:20px; display:none" id="<?= $no ?>">
            <?php } ?>
            <?php if ($payment_method['code'] == "midtrans") { ?>
                <table style="width:100%">
                    <tbody>
                    <tr>
                        <td style="width:50%">
                            <img style="dispaly:inline; width:120px;" src="<?php echo $payment_method['logo']; ?>"><br>
                            <font color="red"><i><?= $payment_method['note'] ?></i></font>
                            <br>
                        </td>

                    </tr>
                    </tbody>
                </table>

            <?php } else { ?>
                <img style="dispaly:inline; width:75px;" src="<?php echo $payment_method['logo']; ?>"><br>
                No Rekening : <b><?= $payment_method['rekening_number'] ?></b><br>
                Nama : <b><?= $payment_method['rekening_name'] ?></b><br>
                <b>Instruksi :</b><br>
                <?= $payment_method['instruction'] ?><br>
            <?php } ?>
        </div>
        <?php
        $no++;
        }
        ?>
    </div>
    <div class="checkbox">
        <label>Dengan memilih lanjut, maka anda telah menyetujui syarat dan ketentuan yang berlaku</label>
    </div>
    </form>
    <hr>
    <div>
        <button class="btn btn-primary pull-left" onclick="setSteps(2);"><i class="fa fa-chevron-left"></i> Kembali
        </button>
        <button class="btn btn-primary pull-right" id="button-pm">Lanjut <i class="fa fa-chevron-right"></i></button>
    </div>
    <?php } else { ?>
        <hr>
        <div>
            <button class="btn btn-primary pull-left" onclick="setSteps(2);"><i class="fa fa-chevron-left"></i> Kembali
            </button>
        </div>
    <?php } ?>

    <script type="text/javascript">
        function pmethod(no) {
            for (i = 1; i <=<?=$cp?>; i++) {
                $('#' + i).hide();
            }
            $('#' + no).show();
        }
    </script>