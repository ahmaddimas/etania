<?php if ($products) { ?>
    <div class="page-title">
        <h1><?= lang('heading_title') ?></h1><br>
    </div>
    <?php if ($success) { ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?= $success ?>
        </div>
    <?php } ?>
    <?php if ($error) { ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?= $error ?>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-8">
            <?= form_open($action, 'id="form-cart"') ?>
            <div class="panel panel-default">
                <ul class="list-group">
                    <?php foreach ($products as $key => $product) { ?>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-xs-4">
                                    <img src="<?= $product['thumb'] ?>" class="img-responsive">
                                </div>
                                <div class="col-xs-8">
                                    <a href="<?= $product['href'] ?>"><?= $product['name'] ?></a>
                                    <?php if (!$product['stock']) { ?>
                                        <br><span class="text-danger">***</span>
                                    <?php } ?>
                                    <?php if ($product['option']) { ?><br>
                                        <?php foreach ($product['option'] as $option) { ?>
                                            <small><?= $option['name'] ?> : <span><?= $option['option_value'] ?></span>
                                            </small><br>
                                        <?php } ?>
                                    <?php } ?>
                                    <p class="text-muted">@<?= $product['price'] ?></p>
                                    <div class="input-group input-group-sm">
							<span class="input-group-btn">
								<a class="btn btn-default button-quantity">-</a>
							</span>
                                        <input type="hidden" value="<?= $key ?>">
                                        <input type="text" class="form-control" value="<?= $product['quantity'] ?>"
                                               style="text-align:center;" readonly="readonly">
                                        <span class="input-group-btn">
							<a class="btn btn-default button-quantity">+</a>
							<a class="btn btn-danger" onclick="cart.remove('<?= $key ?>');"><i class="fa fa-trash"></i></a>
							</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <?= form_close() ?>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <table class="table">
                    <?php foreach ($totals as $total) { ?>
                        <tr>
                            <th class="text-right"><?= $total['title'] ?></th>
                            <th class="text-right"><?= $total['text'] ?></th>
                        </tr>
                    <?php } ?>
                    <tr>
                        <th colspan="2">
                            <button class="btn btn-primary btn-block"
                                    id="button-checkout"><?= lang('button_checkout') ?></button>
                        </th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="page-error" style="padding:60px 0;">
        <h4><?= lang('text_empty') ?></h4>
        <div style="font-size:140px;color:grey;"><i class="fa fa-shopping-basket"></i></div>
        <p><a href="<?= site_url() ?>" class="btn btn-primary"><?= lang('button_continue') ?></a></p>
    </div>
<?php } ?>

<script type="text/javascript">
    $('.button-quantity').on('click', function () {
        var btn = $(this);
        var oldVal = btn.closest('.input-group').find('input[type=\'text\']').val();
        if (btn.text() == '+') {
            var newVal = parseFloat(oldVal) + 1;
        } else {
            if (oldVal > 1) {
                var newVal = parseFloat(oldVal) - 1;
            } else {
                var newVal = 1;
            }
        }

        btn.closest('.input-group').find('input[type=\'text\']').val(newVal);
        productId = btn.closest('.input-group').find('input[type=\'hidden\']').val();
        cart.update(productId, newVal);
    });

    $('#button-checkout').on('click', function () {
        $(this).button('loading');
        window.location = "<?=site_url('checkout')?>";
    });
</script>