<div class="checkout-progress-sidebar ">
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="unicase-checkout-title">Ringkasan</h4>
            </div>
            <table class="table">
                <?php foreach ($totals as $total) { ?>
                    <tr>
                        <td class="text-right" style="padding:5px;"><?= $total['title'] ?></td>
                        <td class="text-right" style="padding:5px;"><strong><?= $total['text'] ?></strong></td>
                    </tr>
                <?php } ?>
            </table>
            <div style="border:1px dashed #ccc; background:#f5f5f5; padding:15px;">
                <dl>
                    <?php if ($shipping_address) { ?>
                        <dt>Dikirim ke:</dt>
                        <dd><?= $shipping_address ?></dd>
                    <?php } ?>
                </dl>
            </div>
        </div>
    </div>
</div>