<div class="card full-width">
    <h4><?= $template['title'] ?></h4>
    <div class="row">
        <div class="col-md-8 center-column content-with-background" id="content">
            <div class="checkout-steps">
                <div class="">
                    <div class="f1-steps">
                        <div class="f1-progress">
                            <div class="f1-progress-line" data-number-of-steps="4" style="width: 25%;"></div>
                        </div>
                        <div class="f1-step active">
                            <div class="f1-step-icon"><i class="fa fa-check"></i></div>
                            <p>Step 1</p>
                        </div>
                        <div class="f1-step">
                            <div class="f1-step-icon"><i class="fa fa-check"></i></div>
                            <p>Step 2</p>
                        </div>
                        <div class="f1-step">
                            <div class="f1-step-icon"><i class="fa fa-check"></i></div>
                            <p>Step 3</p>
                        </div>
                        <div class="f1-step">
                            <div class="f1-step-icon"><i class="fa fa-check"></i></div>
                            <p>Step 4</p>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom:20px;">
                        <div class="status alert alert-success" style="display: none"></div>
                        <div id="step-1" class="form-steps">
                            <div class="col-md-12" id="shipping-address"></div>
                        </div>
                        <div id="step-2" class="form-steps" style="display:none;">
                            <div class="col-md-12" id="shipping-method"></div>
                        </div>
                        <div id="step-3" class="form-steps" style="display:none;">
                            <div class="col-md-12" id="payment-method"></div>
                        </div>
                        <div id="step-4" class="form-steps" style="display:none;">
                            <div class="col-md-12" id="checkout-confirm"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4" id="column-right">
            <div id="overview-content"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        setSteps(1);
    });

    function setSteps(num) {
        $('html, body').animate({scrollTop: 0}, 'slow');
        if (num == 1) {
            $.ajax({
                url: "<?=site_url('checkout/shipping_address')?>",
                dataType: 'html',
                beforeSend: function () {
                    $('#shipping-address').html('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
                },
                complete: function () {
                    $('#shipping-address .loading-state').remove();
                },
                success: function (html) {
                    $('#shipping-address').html(html);
                }
            });
        } else if (num == 2) {
            $.ajax({
                url: "<?=site_url('checkout/shipping_method')?>",
                dataType: 'html',
                beforeSend: function () {
                    $('#shipping-method').html('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
                },
                complete: function () {
                    $('#shipping-method .loading-state').remove();
                },
                success: function (html) {
                    $('#shipping-method').html(html);
                }
            });
        } else if (num == 3) {
            $.ajax({
                url: "<?=site_url('checkout/payment_method')?>",
                dataType: 'html',
                beforeSend: function () {
                    $('#payment-method').html('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
                },
                complete: function () {
                    $('#payment-method .loading-state').remove();
                },
                success: function (html) {
                    $('#payment-method').html(html);
                }
            });
        } else if (num == 4) {
            $.ajax({
                url: "<?=site_url('checkout/confirm')?>",
                dataType: 'html',
                beforeSend: function () {
                    $('#checkout-confirm').html('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
                },
                complete: function () {
                    $('#checkout-confirm .loading-state').remove();
                },
                success: function (html) {
                    $('#checkout-confirm').html(html);
                }
            });
        }

        var steps = $('.f1-step');
        var formSteps = $('.form-steps');
        var steps_progress = $('.f1-progress-line');
        steps.removeClass('active');
        formSteps.hide();

        var stepElement = [];

        steps.each(function (index) {
            i = index + 1;
            stepElement[i] = $(this);
        });

        $('#step-' + num).show();

        for (i = 1; i <= num; i++) {
            if (stepElement[i]) {
                stepElement[i].addClass('active');
            }
        }

        steps_progress.css('width', num * 25 + '%');

        $.ajax({
            url: "<?=site_url('checkout/overview')?>",
            dataType: 'html',
            beforeSend: function () {
                $('#overview-content').html('<div class="text-center loading-state"><h4 class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h4></div>');
            },
            complete: function () {
                $('#overview-content .loading-state').remove();
            },
            success: function (html) {
                $('#overview-content').html(html);
            }
        });
    }

    $('#shipping-address').on('click', '#button-sa', function () {
        var btn = $(this);
        $.ajax({
            url: $('#form-sa').attr('action'),
            type: 'post',
            data: $('#form-sa').serialize(),
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                $('.alert-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#shipping-address').prepend('<div class="warning alert alert-warning" style="display:none; margin:15px;">' + json['error']['warning'] + '</div>');
                        $('.warning').fadeIn('slow');
                    }
                    for (i in json['error']) {
                        $('#shipping-address input[name=' + i + ']').closest('.form-group').addClass('has-error');
                        $('#shipping-address input[name=' + i + ']').after('<small class="text-danger">' + json['error'][i] + '</small>');
                    }
                } else {
                    setSteps(2);
                }
            }
        });
    });

    $('#shipping-method').on('click', '#button-sm', function () {
        var btn = $(this);
        $.ajax({
            url: "<?=site_url('checkout/shipping_method')?>",
            type: 'post',
            data: $('#shipping-method #form-sm').serialize(),
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.warning, .error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#shipping-method .checkout-content').prepend('<div class="warning alert alert-warning" style="display:none; margin:15px;">' + json['error']['warning'] + '</div>');
                        $('.warning').fadeIn('slow');
                    }
                } else {
                    setSteps(3);
                }
            }
        });
    });

    $('#payment-method').on('click', '#button-pm', function () {
        var btn = $(this);
        $.ajax({
            url: "<?=site_url('checkout/payment_method')?>",
            type: 'post',
            data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'hidden\'], #payment-method textarea'),
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.warning, .error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#payment-method').prepend('<div class="warning alert alert-danger" style="display:none;">' + json['error']['warning'] + '</div>');
                        $('html, body').animate({scrollTop: 0}, 'slow');
                        $('.warning').fadeIn('slow');
                    }
                } else {
                    setSteps(4);
                }
            }
        });
    });

    $('.wrapper').css('background-color', '#fff');
</script> 