<link rel="stylesheet" type="text/css" href="<?= base_url('themes/mobile/assets/css/countdown.css') ?>" media="screen"/>
<style>
    .dotd .is-countdown {
        border: 1px solid #ddd;
        background-color: #efefef;
    }

    .dotd .price .price-new {
        font-size: 22px;
    }

    .dotd .product-thumb {
        border: none;
        padding: 0;
    }
</style>
<script src="<?= base_url('themes/mobile/assets/js/jquery.plugin.min.js') ?>"></script>
<script src="<?= base_url('themes/mobile/assets/js/jquery.countdown.min.js') ?>"></script>

<div class="card">
    <h4 style="text-align:center;"><?= $title ?></h4>
    <div class="dotd">
        <div class="">
            <div id="countdown" class="clearfix"></div>
            <div class="product-thumb">
                <?php if ($discount_percent) { ?>
                    <span class="sale" style="left:15px;top:140px;"><?= $discount_percent ?></span>
                <?php } ?>
                <div class="image">
                    <a href="<?= $href ?>"><img src="<?= $thumb ?>" alt="<?= $name ?>" title="<?= $name ?>"
                                                class="img-responsive"/></a>
                </div>
                <div class="caption">
                    <div class="name"><?= cut_text($name, 48) ?></div>
                    <div class="price">
                        <?php if ($discount) { ?>
                            <p class="price"><span class="price-old" style="margin-left:0;"><small><?= $price ?></small></span>
                                <span class="price-new"><?= $discount ?></span></p>
                        <?php } else { ?>
                            <p class="price"><span class="price-new"><?= $price ?></span></p>
                        <?php } ?>
                    </div>
                    <button class="btn btn-primary btn-block" onclick="cart.add('<?= $product_id ?>');"><i
                                class="fa fa-shopping-bag"></i> <span><?= lang('button_cart') ?></span></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($status) { ?>
    <script>
        $(function () {
            var austDay = new Date();
            austDay = new Date(<?=$year?>, <?=$month?> -1, <?=$day?>, <?=$hour?>, <?=$minute?>);
            $('#countdown').countdown({
                until: austDay
            });
        });
    </script>
<?php } ?>