<div id="main-slider" class="carousel slide" data-ride="carousel" style="margin-top:20px;margin-bottom:-10px;">
    <ol class="carousel-indicators">
        <?php foreach ($banners as $key => $banner) { ?>
            <li data-target="#main-slider"
                data-slide-to="<?= $key ?>" <?php echo $key == 0 ? 'class="active"' : ''; ?>></li>
        <?php } ?>
    </ol>
    <div class="carousel-inner">
        <?php foreach ($banners as $key => $banner) { ?>
            <div class="item <?php echo $key == 0 ? 'active' : ''; ?>">
                <a href=""><img src="<?= $banner['image'] ?>" class="img-responsive"></a>
                <?php if ($banner['title']) { ?>
                    <div class="carousel-caption">
                        <p class="lead"><?= $banner['title'] ?></p>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>

<?= Modules::run('offers/dotd') ?>


<?php if ($latest) { ?>
<div id="product-slider" style="margin-bottom:15px;">
    <div>
        <div class="row">
            <div class="col-xs-9">
                <h4 style="border-left:5px solid #ffcc00;padding-left:8px;"><?= lang('text_latest_product') ?></h4>
            </div>
            <div class="col-xs-3">
                <div class="controls pull-right">
                    <a class="left fa fa-chevron-left" href="#carousel-special-latest" data-slide="prev"></a>
                    <a class="right fa fa-chevron-right" href="#carousel-special-latest" data-slide="next"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="carousel-special-latest" class="carousel slide" data-ride="carousel" style="margin:0 -5px;">
                    <div class="carousel-inner">
                        <?php foreach (array_chunk($latest, 2) as $key => $special_products) { ?>
                        <?php if ($key == 0) { ?>
                        <div class="container item active">
                            <?php } else { ?>
                            <div class="container item">
                                <?php } ?>
                                <div class="row">
                                    <?php for ($i = 0; $i <= 1; $i++) { ?>
                                        <?php if (isset($special_products[$i])) { ?>
                                            <?php $product = $special_products[$i]; ?>
                                            <div class="col-sm-6 col-xs-6 product-grid">
                                                <div class="product-thumb" style="margin:5px;">
                                                    <?php if ($product['wholesaler']) { ?>
                                                        <span class="sale">Grosir</span>
                                                    <?php } elseif ($product['discount_percent']) { ?>
                                                        <span class="sale"><?= $product['discount_percent'] ?></span>
                                                    <?php } ?>
                                                    <?php if ($product['free_shipping'] == 1) { ?>
                                                        <div style="position: absolute; right: 0px; padding: 15px 25px 0 0;">
                                                            <img style="width:80px; height:20px"
                                                                 src="<?= site_url('storage/images/' . $free_shipping['free_shipping_logo']) ?>"
                                                                 alt="Free Shipping"/></div>
                                                    <?php } ?>
                                                    <div class="image"><a href="<?= $product['href'] ?>"><img
                                                                    src="<?= $product['thumb'] ?>"
                                                                    alt="<?= $product['name'] ?>"
                                                                    title="<?= $product['name'] ?>"
                                                                    class="img-responsive"/></a></div>
                                                    <div class="rating">
                                                        <?php $star = ''; ?>
                                                        <?php for ($x = 1; $x <= 5; $x++) { ?>
                                                            <?php if ($x <= $product['rating']) { ?>
                                                                <?php $star .= '<span class="active"><i class="fa fa-star"></i></span>'; ?>
                                                            <?php } else { ?>
                                                                <?php $star .= '<span><i class="fa fa-star"></i></span>'; ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <?= $star ?>
                                                    </div>
                                                    <div class="caption">
                                                        <div class="name"><?= substr($product['name'], 0, 38) ?></div>
                                                        <br>
                                                        <div class="price">
                                                            <?php if ($product['discount']) { ?>
                                                                <p class="price"><span class="price-old"
                                                                                       style="margin-left:0;"><small><?= $product['price'] ?></small></span><br><span
                                                                            class="price-new"><?= $product['discount'] ?></span>
                                                                </p>
                                                            <?php } else { ?>
                                                                <p class="price"><span
                                                                            class="price-new"><?= $product['price'] ?></span>
                                                                </p>
                                                            <?php } ?>
                                                        </div>
                                                        <button class="btn btn-primary btn-block"
                                                                onclick="cart.add('<?= $product['product_id'] ?>');"><i
                                                                    class="fa fa-shopping-bag"></i>
                                                            <span><?= lang('button_cart') ?></span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>


<?= Modules::run('offers/special') ?>