<div class="page-title">
    <h1><?= $template['title'] ?></h1>
</div>
<div class="card full-width">
    <div id="message"></div>
    <?= form_open($action, 'id="form" class="form-horizontal"') ?>
    <div class="form-group">
        <label class="control-label col-sm-3"><?= lang('entry_name') ?></label>
        <div class="col-sm-9">
            <input type="text" class="form-control unicase-form-control" name="name" value="<?= $name ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3"><?= lang('entry_email') ?></label>
        <div class="col-sm-9">
            <input type="text" class="form-control unicase-form-control" name="email" value="<?= $email ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3"><?= lang('entry_telephone') ?></label>
        <div class="col-sm-9">
            <input type="text" class="form-control unicase-form-control" name="telephone" value="<?= $telephone ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3"><?= lang('entry_dob') ?></label>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-md-3">
                    <select name="dob_date" class="form-control unicase-form-control">
                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                            <?php if ($i == $dob_date) { ?>
                                <option value="<?= $i ?>" selected="selected"><?= $i ?></option>
                            <?php } else { ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-5">
                    <select name="dob_month" class="form-control unicase-form-control">
                        <?php foreach ($months as $key => $value) { ?>
                            <?php if ($key == $dob_month) { ?>
                                <option value="<?= $key ?>" selected="selected"><?= $value ?></option>
                            <?php } else { ?>
                                <option value="<?= $key ?>"><?= $value ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <select name="dob_year" class="form-control unicase-form-control">
                        <?php for ($i = 1950; $i <= 1999; $i++) { ?>
                            <?php if ($i == $dob_year) { ?>
                                <option value="<?= $i ?>" selected="selected"><?= $i ?></option>
                            <?php } else { ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3"></label>
        <div class="col-sm-9">
            <div class="checkbox">
                <label>
                    <?php if ($newsletter) { ?>
                        <input type="checkbox" name="newsletter" value="1" checked="checked">
                    <?php } else { ?>
                        <input type="checkbox" name="newsletter" value="1">
                    <?php } ?>
                    <?= lang('entry_newsletter') ?>
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"><?= lang('entry_image') ?></label>
        <div class="col-md-3">
            <img id="image" src="<?= $image ?>" class="img-thumbnail">
            <div class="caption" style="margin-top:10px;">
                <button type="button" class="btn btn-default" id="upload"><?= lang('button_image') ?></button>
            </div>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-9">
            <button type="button" id="submit" class="btn btn-primary pull-right"><i
                        class="fa fa-check"></i> <?= lang('button_save') ?></button>
        </div>
    </div>
    </form>
</div>
<script src="<?= base_url('assets/js/ajaxupload.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    new AjaxUpload('#upload', {
        action: "<?=site_url('customer/edit/upload?customer_id=' . $customer_id);?>",
        name: 'userfile',
        autoSubmit: false,
        responseType: 'json',
        onChange: function (file, extension) {
            this.submit();
        },
        onSubmit: function (file, extension) {
            $('#upload').append('<span class="wait"><i class="fa fa-refresh"></i></span>');
        },
        onComplete: function (file, json) {
            if (json.success) {
                $('.wait').remove();
                $('#image').attr('src', json.image);
            }
            if (json.error) {
                alert(json.error);
            }
            $('.loading').remove();
        }
    });

    $('#submit').bind('click', function () {
        var btn = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                $('.alert-danger').remove();

                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('select[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                        $('select[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            }
        });
    });
</script>