<div class="card full-width">
    <div class="row">
        <div class="col-xs-12">
            <?= form_open(site_url('customer/login'), 'id="form-login" class="login-form"') ?>
            <h3><?= lang('text_login') ?></h3>
            <p><?= $text_register ?></p>
            <hr>
            <?php if ($error) { ?>
                <div class="alert alert-danger">
                    <?= $error ?>
                </div>
            <?php } ?>
            <div class="form-group">
                <input type="text" name="email" placeholder="Email" autofocus class="form-control">
            </div>
            <div class="form-group">
                <input type="password" name="password" placeholder="Password" class="form-control">
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" value="1"> <?= lang('entry_remember') ?>
                        </label>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="checkbox">
                        <label><a href=""><?= $text_forgotten ?></a></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block btn-lg"><?= lang('button_login') ?> <i
                            class="fa fa-sign-in fa-lg"></i></button>
                <?php if ($this->config->item('oauth_facebook_status') || $this->config->item('oauth_google_status')) { ?>
                    <div class="hr-sect"><?= lang('text_or') ?></div>
                    <?php if ($this->config->item('oauth_facebook_status')) { ?>
                        <a class="btn btn-facebook btn-block" href="<?= user_url('facebook_login') ?>"><i
                                    class="fa fa-facebook fa-lg"></i> <?= lang('button_facebook_login') ?></a>
                    <?php } ?>
                    <?php if ($this->config->item('oauth_google_status')) { ?>
                        <a class="btn btn-google btn-block" href="<?= user_url('google_login') ?>"><i
                                    class="fa fa-google fa-lg"></i> <?= lang('button_google_login') ?></a>
                    <?php } ?>
                <?php } ?>
            </div>
            </form>
        </div>
    </div>
</div>