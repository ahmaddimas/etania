<li><a href="<?= site_url('customer') ?>"><span class="fa fa-circle-o"></span> <?= lang('text_overview') ?></a></li>
<li><a href="<?= site_url('customer/order') ?>"><span class="fa fa-circle-o"></span> <?= lang('text_order') ?></a></li>
<?php
$addon_digital = $this->config->item('addon_digital');
if (!empty($addon_digital['active'])) {
    ?>
    <li><a href="<?= site_url('digital') ?>"><span class="fa fa-circle-o"></span> <?= lang('text_order_digital') ?></a>
    </li>
<?php } ?>
<li><a href="<?= site_url('customer/transaction') ?>"><span
                class="fa fa-circle-o"></span> <?= lang('text_transaction') ?></a></li>
<li><a href="<?= site_url('customer/edit') ?>"><span class="fa fa-circle-o"></span> <?= lang('text_edit') ?></a></li>
<li><a href="<?= site_url('customer/password') ?>"><span class="fa fa-circle-o"></span> <?= lang('text_password') ?></a>
</li>
<li><a href="<?= site_url('customer/address') ?>"><span class="fa fa-circle-o"></span> <?= lang('text_address') ?></a>
</li>
<li><a href="<?= site_url('customer/logout') ?>"><span class="fa fa-circle-o"></span> <?= lang('text_logout') ?></a>
</li>