<div class="card full-width">
    <div class="row">
        <div class="col-xs-12">
            <h1><?= lang('text_success') ?></h1>
            <p class="lead"><?= sprintf(lang('text_success_message'), site_url('customer/login')) ?></p>
        </div>
    </div>
</div>