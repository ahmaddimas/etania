<div class="card full-width">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <?= form_open(site_url('customer/reset'), 'id="form-reset" class="login-form"') ?>
            <h3 class="login-head"><?= lang('heading_title') ?></h3>
            <p><?= lang('text_instruction') ?></p>
            <hr>
            <div class="form-group">
                <input type="text" name="telephone" placeholder="<?= lang('entry_telephone') ?>" class="form-control">
            </div>
            <div class="form-group" style="background-color:#efefef; padding: 10px;">
                <div class="row">
                    <div class="col-xs-9" id="captcha"></div>
                    <div class="col-xs-2 text-right"><a class="btn btn-default" id="reset-captcha"><i
                                    class="fa fa-refresh" title="Reload"></i></a></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="text" name="captcha" placeholder="<?= lang('text_captcha') ?>"
                               class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group btn-container">
                <button class="btn btn-primary btn-block btn-lg" id="button-reset"><?= lang('button_reset') ?> <i
                            class="fa fa-key fa-lg"></i></button>
            </div>
            <div class="form-group mt-20">
                <p class="semibold-text mb-0"><a href="<?= site_url('customer/login') ?>"><i
                                class="fa fa-angle-left fa-fw"></i> <?= $text_login ?></a></p>
            </div>
            </form>
        </div>
    </div>
</div>
<script src="<?= base_url('/assets/plugins/bootstrap-notify.min.js') ?>"></script>
<script type="text/javascript">
    $(document).delegate('#button-reset', 'click', function (e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            url: $('#form-reset').attr('action'),
            data: $('#form-reset').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                if (json['warning']) {
                    $.notify({
                        title: "Warning : ",
                        message: json['warning'],
                        icon: 'fa fa-ban'
                    }, {
                        type: "danger"
                    });
                } else if (json['redirect']) {
                    window.location = json['redirect'];
                }
            }
        });
    });

    $(document).delegate('#reset-captcha', 'click', function (e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            url: "<?=site_url('captcha')?>",
            dataType: 'html',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (html) {
                $('#captcha').html(html);
            }
        });
    });

    $('#reset-captcha').trigger('click');
</script>