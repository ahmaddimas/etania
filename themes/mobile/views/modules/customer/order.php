<div class="page-title">
    <h1><?= $template['title'] ?></h1>
</div>
<div class="card full-width">
    <div class="row">
        <div class="col-md-12">
            <div id="message"></div>
            <table class="table table-hover table-striped" width="100%" id="datatable">
                <thead>
                <tr>
                    <th><?= lang('column_order_id') ?></th>
                    <th><?= lang('column_invoice') ?></th>
                    <th><?= lang('column_date') ?></th>
                    <th><?= lang('column_total') ?></th>
                    <th><?= lang('column_status') ?></th>
                    <th class="text-right"></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=site_url('customer/order')?>",
                "type": "POST",
            },
            "columns": [
                {"data": "order_id"},
                {"data": "invoice"},
                {"data": "date_added"},
                {"data": "total"},
                {"data": "status"},
                {"orderable": false, "searchable": false, "data": "order_id"},
            ],
            "sDom": '<"table-responsive" t><"pagination-results" p>',
            "createdRow": function (row, data, index) {
                $('td', row).eq(5).addClass('text-right').html('<a href="<?=user_url('order/detail/\'+data.order_id+\'')?>" class="btn btn-default"><?=lang('button_view')?></a><a target="_blank" href="<?=user_url('order/eprint/invoice/\'+data.order_id+\'')?>" class="btn btn-default"><span class="fa fa-print"></span></a>');
            },
            "order": [[0, 'desc']]
        });
    });

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }
</script>