<div class="card full-width">
    <div class="row">
        <div class="col-xs-12">
            <?= form_open($action, 'id="form-reset" class="login-form"') ?>
            <h3><?= lang('heading_title') ?></h3>
            <p><?= lang('text_change_instruction') ?></p>
            <hr>
            <div class="form-group">
                <input type="text" name="code" placeholder="<?= lang('entry_code') ?>" autofocus class="form-control">
            </div>
            <div class="form-group">
                <input type="password" name="password" placeholder="<?= lang('entry_new_password') ?>"
                       class="form-control">
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block btn-lg" id="button-reset"><?= lang('button_reset') ?> <i
                            class="fa fa-key fa-lg"></i></button>
            </div>
            </form>
        </div>
    </div>
</div>
<script src="<?= base_url('/assets/plugins/bootstrap-notify.min.js') ?>"></script>
<script type="text/javascript">
    $(document).delegate('#button-reset', 'click', function (e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            url: $('#form-reset').attr('action'),
            data: $('#form-reset').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                if (json['warning']) {
                    $.notify({
                        title: "Warning : ",
                        message: json['warning'],
                        icon: 'fa fa-ban'
                    }, {
                        type: "danger"
                    });
                } else if (json['redirect']) {
                    window.location = json['redirect'];
                }
            }
        });
    });
</script>