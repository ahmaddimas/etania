<div class="page-title">
    <h1><?= $template['title'] ?></h1>
</div>
<div class="card full-width">
    <div class="row">
        <div class="col-md-12">
            <div id="message"></div>
            <table class="table table-hover table-striped" width="100%" id="datatable">
                <thead>
                <tr>
                    <th><?= lang('text_address') ?></th>
                    <th><?= lang('text_default') ?></th>
                    <th class="text-right"></th>
                </tr>
                </thead>
            </table>
            <hr>
            <a onclick="addressForm();" class="btn btn-primary pull-right"><i
                        class="fa fa-plus-circle"></i> <?= lang('text_create') ?></a>
        </div>
    </div>
</div>
<div id="modal"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var tableAddress = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=site_url('customer/address')?>",
                "type": "POST",
            },
            "columns": [
                {"data": "address_id"},
                {"data": "customer_id"},
                {"orderable": false, "searchable": false, "data": "address_id"},
            ],
            "sDom": '<t><"pagination-results" p>',
            "createdRow": function (row, data, index) {
                $('td', row).eq(0).html('<address><b>' + data.name + '</b><br>' + data.address + ', ' + data.subdistrict + '<br>' + data.city + ' - ' + data.province + '</address>');
                html = '<div class="btn-group">';
                html += '<a onclick="addressForm(' + data.address_id + ');" class="btn btn-default"><i class="fa fa-cog"></i> <?=lang('button_edit')?></a>';
                html += '<a onclick="delRecord(' + data.address_id + ');" class="btn btn-default"><i class="fa fa-minus-circle"></i> <?=lang('button_delete')?></a>';
                html += '</div>';
                $('td', row).eq(2).addClass('text-right').html(html);

                if (data.is_default === '1') {
                    $('td', row).eq(1).html('<i class="fa fa-check" style="color:green;"></i>');
                } else {
                    $('td', row).eq(1).html('');
                }
            },
            "order": [[0, 'asc']]
        });
    });

    $('#submit').bind('click', function () {
        var btn = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                $('.alert-danger').remove();

                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('select[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                        $('select[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    window.location = "<?=site_url('customer')?>";
                }
            }
        });
    });

    function delRecord(address_id) {
        if (confirm('<?=lang('text_confirm')?>')) {
            $.ajax({
                url: '<?=site_url('customer/address/delete')?>',
                type: 'post',
                data: 'address_id=' + address_id,
                dataType: 'json',
                success: function (json) {
                    if (json['success']) {
                        $('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                    }

                    refreshTable();
                }
            });
        }
    }

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }

    function addressForm(address_id) {
        if (typeof (address_id) == 'undefined') {
            var url = "<?=site_url('customer/address/create')?>";
        } else {
            var url = "<?=site_url('customer/address/edit')?>";
        }

        $.ajax({
            url: url,
            data: 'address_id=' + address_id,
            dataType: 'json',
            success: function (json) {
                if (json['error']) {
                    $('#message').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i> ' + json['error'] + '</div>');
                } else if (json['content']) {
                    $('#modal').html('<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['content'] + '</div>');
                    $('#form-modal').modal('show');
                    $('#form-modal').on('hidden.bs.modal', function (e) {
                        refreshTable();
                    });
                }
            }
        });
    }
</script>
