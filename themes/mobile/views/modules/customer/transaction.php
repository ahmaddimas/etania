<div class="page-title">
    <h1><?= $template['title'] ?></h1>
</div>
<div class="card full-width">
    <div class="row">
        <div class="col-md-12">
            <div id="message"></div>
            <table class="table table-hover table-striped" width="100%" id="datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?= lang('text_date') ?></th>
                    <th><?= lang('text_description') ?></th>
                    <th><?= lang('text_status') ?></th>
                    <th class="text-right"><?= lang('text_in') ?></th>
                    <th class="text-right"><?= lang('text_out') ?></th>
                    <th class="text-right"><?= lang('text_balance') ?></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=site_url('customer/transaction')?>",
                "type": "POST",
                "data": function (d) {
                    d.code = "<?=$code?>";
                }
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "customer_transaction_id"},
                {"orderable": false, "searchable": false, "data": "date_added"},
                {"orderable": false, "searchable": false, "data": "description"},
                {"orderable": false, "searchable": false, "data": "approved"},
                {"orderable": false, "searchable": false, "data": "debit"},
                {"orderable": false, "searchable": false, "data": "credit"},
                {"orderable": false, "searchable": false, "data": "balance"},
            ],
            "sDom": '<"table-responsive" t><"pagination-results" p>',
            "createdRow": function (row, data, index) {
                $('td', row).eq(4).addClass('text-right');
                $('td', row).eq(5).addClass('text-right');
                $('td', row).eq(6).addClass('text-right');

                if (data.approved == '0') {
                    $('td', row).eq(3).html('<i class="fa fa-clock-o"></i>');
                } else {
                    $('td', row).eq(3).html('<i class="fa fa-check" style="color:green;"></i>');
                }
            },
            "order": [[0, 'desc']]
        });
    });

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }
</script>