<div class="page-title">
    <h1><?= $template['title'] ?></h1>
</div>
<div class="card full-width">
    <div id="message"></div>
    <?= form_open($action, 'id="form" class="form-horizontal"') ?>
    <div class="form-group">
        <label class="control-label col-sm-3"><?= lang('entry_password') ?></label>
        <div class="col-sm-9">
            <input type="password" class="form-control" name="password" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3"><?= lang('entry_confirm') ?></label>
        <div class="col-sm-9">
            <input type="password" class="form-control" name="confirm" value="">
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-9">
            <button type="button" id="submit" class="btn btn-primary pull-right"><i
                        class="fa fa-check"></i> <?= lang('button_save') ?></button>
        </div>
    </div>
    </form>
</div>
<script type="text/javascript">
    $('#submit').bind('click', function () {
        var btn = $(this);
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (json) {
                $('.form-group').removeClass('has-error');
                $('.text-danger').remove();
                $('.alert-danger').remove();

                if (json['errors']) {
                    for (i in json['errors']) {
                        $('input[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('select[name=\'' + i + '\']').closest('.form-group').addClass('has-error');
                        $('input[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                        $('select[name=\'' + i + '\']').after('<span class="text-danger">' + json['errors'][i] + '</span>');
                    }
                } else if (json['success']) {
                    $('#message').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> ' + json['success'] + '</div>');
                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            }
        });
    });
</script>