<div class="page-title">
    <h1><?= $heading_title ?>
        <small><?= $heading_subtitle ?></small>
    </h1>
</div>
<div class="full-width">
    <?= form_open(site_url('custom/validate'), 'id="form"') ?>
    <?php foreach ($categories as $category) { ?>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label"><?= $category['name'] ?> :</label>
                    <select name="parts[<?= $category['custom_category_id'] ?>]" class="form-control">
                        <option value="0" rel="0"><?= lang('text_select') ?></option>
                        <?php foreach ($category['parts'] as $part) { ?>
                            <option value="<?= $part['product_id'] ?>"
                                    rel="<?= $part['price'] ?>"><?= $part['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="subtotal" value="0" style="border:none;">
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group">
                <label class="control-label">TOTAL :</label>
                <input type="text" class="grand-total" value="0" style="border:none;font-weight:bold;font-size:18px;">
            </div>
            <div class="form-group">
                <button type="button" id="submit" class="btn btn-primary"><?= lang('button_cart') ?></button>
            </div>
        </div>
    </div>
    </form>
</div>
<script src="<?= site_url('assets/js/jquery.number.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('select').on('change', function () {
            $(this).closest('.panel-body').find('.subtotal').val($(this).find('option:selected').attr('rel'));
            calculateTotal();
        });
    });

    $('.subtotal, .grand-total').number(true);

    function calculateTotal() {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.error').remove();
                if (json['success']) {
                    var subTotals = $('.subtotal');
                    var grandTotal = 0;

                    $.each(subTotals, function (i) {
                        grandTotal += parseFloat($(subTotals[i]).val());
                    });

                    $('.grand-total').val(parseFloat(grandTotal));
                } else if (json['errors']) {
                    for (i in json['errors']) {
                        $('select[name=\'parts[' + i + ']\']').after('<small class="error text-danger">' + json['errors'][i] + '</small>');
                    }
                }
            }
        });
    }

    calculateTotal();

    $('#submit').on('click', function () {
        $.ajax({
            url: $('#form').attr('action'),
            data: $('#form').serialize() + '&submit=1',
            type: 'post',
            dataType: 'json',
            success: function (json) {
                $('.error').remove();
                if (json['redirect']) {
                    window.location = json['redirect'];
                } else if (json['errors']) {
                    for (i in json['errors']) {
                        $('select[name=\'parts[' + i + ']\']').after('<small class="error text-danger">' + json['errors'][i] + '</small>');
                    }
                }
            }
        });
    });
</script>