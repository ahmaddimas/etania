<style>dl dt {
        font-weight: bold;
    }

    dl dd {
        margin-bottom: 10px;
    }</style>
<div class="page-title">
    <h1><?= $template['title'] ?></h1>
</div>
<div class="card full-width">
    <div id="message"></div>
    <legend><h4><?= lang('text_item') ?></h4></legend>
    <div class="table-responsive">
        <table class="table table-bordered" width="100%">
            <thead>
            <tr>
                <th><?= lang('column_name') ?></th>
                <th class="text-right"><?= lang('column_quantity') ?></th>
                <th class="text-right"><?= lang('column_price') ?></th>
                <th class="text-right"><?= lang('column_total') ?></th>
                <th class="text-right">Download></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product) { ?>
                <tr>
                    <td>
                        <?= $product['name'] ?>
                        <?php if ($product['option']) { ?>
                            <p>
                                <?php foreach ($product['option'] as $option) { ?>
                                    <small><i><?= $option['name'] ?>: <?= $option['value'] ?></i></small><br>
                                <?php } ?>
                            </p>
                        <?php } ?>
                    </td>
                    <td class="text-right"><?= $product['quantity'] ?></td>
                    <td class="text-right"><?= $product['price'] ?></td>
                    <td class="text-right"><?= $product['total'] ?></td>
                    <td class="">

                        <?php
                        if (count($product['link']) == 0) {
                            echo "Waiting for confirm";
                        } else {

                            $nod = 1;
                            foreach ($product['link'] as $link) {
                                ?>
                                <?= $nod ?>. <a target="_blank" href="<?= site_url($link['file']) ?>">Download</a><br>
                                <?php
                                $nod++;
                            }
                        }
                        ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
            <tbody>
            <?php foreach ($totals as $total) { ?>
                <tr>
                    <td colspan="3"><strong><?= $total['title'] ?></strong></td>
                    <td class="text-right"><strong><?= $total['text'] ?></strong></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div id="modal"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=site_url('customer/order/history')?>",
                "type": "POST",
                "data": function (d) {
                    d.order_id = "<?=$order_id?>";
                }
            },
            "columns": [
                {"orderable": false, "searchable": false, "data": "date_added"},
                {"orderable": false, "searchable": false, "data": "status"},
                {"orderable": false, "searchable": false, "data": "comment"},
            ],
            "sDom": '<"table-responsive" t><"pagination-results" p>',
            "order": [[0, 'desc']],
        });
    });

    function refreshTable() {
        $('#datatable').DataTable().ajax.reload();
    }

    $('#track').on('click', function () {
        var btn = $(this);

        $.ajax({
            url: "<?=site_url('tool/shipping/track')?>",
            data: 'order_id=<?=$order_id?>',
            dataType: 'html',
            beforeSend: function () {
                btn.button('loading');
            },
            complete: function () {
                btn.button('reset');
            },
            success: function (html) {
                $('body').append('<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">' + html + '</div>');
                $('#modal').modal('show');
                $('#modal').on('hidden.bs.modal', function (e) {
                    $('#modal').remove();
                });
            }
        });
    });
</script>