<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <?= $template['metadata'] ?>
    <link href="<?= $_favicon ?>" rel="icon"/>
    <title><?= $template['title'] ?></title>
    <base href="<?= site_url() ?>"/>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php Asset::css('mobile.css', true); ?>
    <?php Asset::css('font-awesome.min.css', true); ?>
    <?php Asset::js('jquery.min.js', true); ?>
    <?php Asset::js('essential-plugins.js', true); ?>
    <?php Asset::js('bootstrap.min.js', true); ?>

    <?= Asset::render() ?>

    <?= $template['css'] ?>
    <?= $template['js'] ?>
</head>
<body class="sidebar-mini fixed" style="background-color:#fff;">
<div class="wrapper">
    <div class="navbar navbar-inverse navbar-fixed-top hidden-lg hidden-md" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a type="button" class="navbar-toggle" data-toggle="collapse" data-target=".search-form">
                    <i class="fa fa-search fa-lg"></i>
                </a>
                <a data-toggle="offcanvas" class="navbar-toggle"><i class="fa fa-bars fa-lg"></i></a>
                <a href="<?= site_url('checkout/cart') ?>" class="navbar-toggle" id="cart-block">
                    <?= Modules::run('checkout/cart/info') ?>
                </a>


                <?php if (!empty($header_banners)) { ?>
                    <a class="navbar-brand" href="<?= site_url() ?>"><img src="<?= $_logo ?>" class="logo"
                                                                          style="height:28px;"></a>
                    <?php foreach ($header_banners as $banner) { ?>
                        <span style="width:100%;">
														<a href="<?= $banner['link'] ?>"
                                                           title="<?= $banner['link_title'] ?>">
														<?php if ($banner['image'] == "") { ?>
                                                            <p style="padding:0px; margin-top:20px;margin-left:40px; text-align:center; max-height:26px;">
																<b><?= $banner['title'] ?></b>
															</p>
                                                        <?php } else { ?>
                                                            <img style="padding:0px; margin-top:15px;margin-left:5px; max-height:26px;"
                                                                 src="<?= $banner['image'] ?>"
                                                                 alt="<?= $banner['title'] ?>">
                                                        <?php } ?>
														</a>
													</span>
                    <?php } ?>
                <?php } else { ?>
                    <a class="navbar-brand" href="<?= site_url() ?>"><img src="<?= $_logo ?>" class="logo"
                                                                          style="height:28px;"></a>
                <?php } ?>


            </div>
            <div class="search-form navbar-collapse collapse">
                <form class="navba" action="<?= site_url('search') ?>" style="margin:10px;">
                    <div class="form-group" style="display:inline;">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" autocomplete="off"
                                   placeholder="<?= lang('text_search') ?>">
                            <span class="input-group-btn"><button class="btn btn-primary"><i
                                            class="fa fa-search"></i></button></span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <aside class="main-sidebar hidden-print">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <?php foreach ($menu as $mn) { ?>
                    <?php if ($mn['children']) { ?>
                        <li class="treeview"><a href="<?= $mn['href'] ?>"><i
                                        class="fa <?= $mn['icon'] ?>"></i><span> <?= $mn['title'] ?></span><i
                                        class="fa fa-angle-right"></i></a>
                            <ul class="treeview-menu">
                                <?php foreach ($mn['children'] as $children) { ?>
                                    <li><a href="<?= $children['href'] ?>"><i
                                                    class="fa fa-circle-o"></i> <?= $children['title'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } else { ?>
                        <?php if ($mn['type'] == 4) { ?>
                            <li class="treeview"><a href="<?= $mn['href'] ?>"><i
                                            class="fa <?= $mn['icon'] ?>"></i><span><?= $mn['title'] ?></span><i
                                            class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu" id="category-menu">
                                    <?php foreach ($category_menu as $category) { ?>
                                        <?php if ($category['children']) { ?>
                                            <li class="treeview"><a href="<?= $category['href'] ?>"><i
                                                            class="fa <?= $category['menu_image'] ?>"></i><span> <?= $category['name'] ?></span><i
                                                            class="fa fa-angle-right"></i></a>
                                                <ul class="treeview-menu">
                                                    <?php foreach ($category['children'] as $children) { ?>
                                                        <?php if ($children['children']) { ?>
                                                            <li class="treeview"><a href="<?= $children['href'] ?>"><i
                                                                            class="fa fa-circle-o"></i><span> <?= $children['name'] ?></span><i
                                                                            class="fa fa-angle-right"></i></a>
                                                                <ul class="treeview-menu">
                                                                    <?php foreach ($children['children'] as $subchildren) { ?>
                                                                        <li><a href="<?= $subchildren['href'] ?>"><i
                                                                                        class="fa fa-circle-o"></i> <?= $subchildren['name'] ?>
                                                                            </a></li>
                                                                    <?php } ?>
                                                                    <li><a href="<?= $children['href'] ?>"><i
                                                                                    class="fa fa-circle-o"></i> <?= lang('text_all') ?> <?= $children['name'] ?>
                                                                        </a></li>
                                                                </ul>
                                                            </li>
                                                        <?php } else { ?>
                                                            <li><a href="<?= $children['href'] ?>"><i
                                                                            class="fa fa-circle-o"></i> <?= $children['name'] ?>
                                                                </a></li>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <li><a href="<?= $category['href'] ?>"><i
                                                                    class="fa fa-circle-o"></i> <?= lang('text_all') ?> <?= $category['name'] ?>
                                                        </a></li>
                                                </ul>
                                            </li>
                                        <?php } else { ?>
                                            <li><a href="<?= $category['href'] ?>"><i
                                                            class="fa <?= $category['menu_image'] ?>"></i> <?= $category['name'] ?>
                                                </a></li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } else if ($mn['type'] == 2) { ?>
                            <?php if ($this->customer->is_logged()) { ?>
                                <li class="treeview"><a href="<?= user_url('') ?>"><i class="fa fa-user"></i><span> My Account</span><i
                                                class="fa fa-angle-right"></i></a>
                                    <ul class="treeview-menu">
                                        <?= Modules::run('customer/navigation/index') ?>
                                    </ul>
                                </li>
                            <?php } else { ?>
                                <li><a href="<?= $mn['href'] ?>"><i
                                                class="fa <?= $mn['icon'] ?>"></i> <?= $mn['title'] ?></a></li>
                            <?php } ?>
                        <?php } else if ($mn['type'] == 3) { ?>
                            <?php if ($this->customer->is_logged()) { ?>

                            <?php } else { ?>
                                <li><a href="<?= $mn['href'] ?>"><i
                                                class="fa <?= $mn['icon'] ?>"></i> <?= $mn['title'] ?></a></li>
                            <?php } ?>
                        <?php } else { ?>
                            <li><a href="<?= $mn['href'] ?>"><i class="fa <?= $mn['icon'] ?>"></i> <?= $mn['title'] ?>
                                </a></li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>


            </ul>


            <div class="row" style="margin-top:25px">
                <div class="col-sm-12">
                    <div class="text-center">
                        <ul class="social-icons-default" style="padding-top: 7px">
                            <?php if ($this->config->item('facebook_user')) { ?>
                                <li><a target="_blank"
                                       href="http://www.facebook.com/<?= $this->config->item('facebook_user') ?>"
                                       title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <?php } ?>
                            <?php if ($this->config->item('twitter_user')) { ?>
                                <li><a target="_blank"
                                       href="http://www.twitter.com/<?= $this->config->item('twitter_user') ?>"
                                       title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <?php } ?>
                            <?php if ($this->config->item('gplus_user')) { ?>
                                <li><a target="_blank"
                                       href="https://plus.google.com<?= $this->config->item('gplus_user') ?>"
                                       title="GooglePlus"><i class="fa fa-google"></i></a></li>
                            <?php } ?>
                            <?php if ($this->config->item('youtube_user')) { ?>
                                <li><a target="_blank"
                                       href="https://www.youtube.com/channel/<?= $this->config->item('youtube_user') ?>"
                                       title="Youtube"><i class="fa fa-youtube-play"></i></a></li>
                            <?php } ?>
                            <?php if ($this->config->item('instagram_user')) { ?>
                                <li><a target="_blank"
                                       href="http://www.instagram.com/<?= $this->config->item('instagram_user') ?>"
                                       title="Instagram"><i class="fa fa-instagram"></i></a></li>
                            <?php } ?>
                        </ul>
                        <br>
                        <?= $_copyright ?>
                    </div>
                </div>
            </div>
        </section>
    </aside>
    <div class="content-wrapper">
        <?= $template['body'] ?>
    </div>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center" style="margin-top:10px;">
                    <p><?= $_copyright ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('themes/mobile/assets/js/mobile.js') ?>"></script>
<?= $this->config->item('livechat_code') ?>
</body>
</html>