<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E Commerce App | <?= $template['title'] ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


    <?php Asset::css('bootstrap.css', true); ?>
    <?php Asset::css('font-awesome.min.css', true); ?>
    <?php Asset::css('lte/plugins/datepicker/datepicker3.css', true); ?>
    <?php Asset::css('lte/dist/css/AdminLTE.css', true); ?>

    <?php Asset::js('jquery-2.1.1.min.js', true); ?>
    <?php Asset::js('jquery-migrate-1.2.1.min.js', true); ?>
    <?php Asset::js('jquery.easing.1.3.js', true); ?>
    <?php Asset::js('bootstrap.min.js', true); ?>
    <?php Asset::js('lte/plugins/datepicker/bootstrap-datepicker.js', true); ?>
    <?php Asset::js('lte/jquery.number.js', true); ?>

    <?= Asset::render() ?>

    <?= $template['css'] ?>
    <?= $template['js'] ?>


    <style type="text/css">
        .floatright {
            text-align: right;
        }

        #loader-wrapper {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 1000;
        }

        #loader {
            display: block;
            position: relative;
            left: 50%;
            top: 50%;
            width: 150px;
            height: 150px;
            margin: -75px 0 0 -75px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: #3498db;

            -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
            animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
        }

        #loader:before {
            content: "";
            position: absolute;
            top: 5px;
            left: 5px;
            right: 5px;
            bottom: 5px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: #e74c3c;

            -webkit-animation: spin 3s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
            animation: spin 3s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
        }

        #loader:after {
            content: "";
            position: absolute;
            top: 15px;
            left: 15px;
            right: 15px;
            bottom: 15px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: #f9c922;

            -webkit-animation: spin 1.5s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
            animation: spin 1.5s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg); /* Chrome, Opera 15+, Safari 3.1+ */
                -ms-transform: rotate(0deg); /* IE 9 */
                transform: rotate(0deg); /* Firefox 16+, IE 10+, Opera */
            }
            100% {
                -webkit-transform: rotate(360deg); /* Chrome, Opera 15+, Safari 3.1+ */
                -ms-transform: rotate(360deg); /* IE 9 */
                transform: rotate(360deg); /* Firefox 16+, IE 10+, Opera */
            }
        }

        @keyframes spin {
            0% {
                -webkit-transform: rotate(0deg); /* Chrome, Opera 15+, Safari 3.1+ */
                -ms-transform: rotate(0deg); /* IE 9 */
                transform: rotate(0deg); /* Firefox 16+, IE 10+, Opera */
            }
            100% {
                -webkit-transform: rotate(360deg); /* Chrome, Opera 15+, Safari 3.1+ */
                -ms-transform: rotate(360deg); /* IE 9 */
                transform: rotate(360deg); /* Firefox 16+, IE 10+, Opera */
            }
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition lockscreen">
<div id="loader-wrapper">
    <div id="loader"></div>
</div>
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">

    <div class="lockscreen-logo">
        <a>E Commerce</a>

    </div>

    <?= $template['body'] ?>

    <div class="lockscreen-footer text-center" style="margin-top:10%;">
        Copyright &copy; <?= date('Y') ?> <b>E Commerce App</b><br>
        All rights reserved
    </div>
</div><!-- /.center -->


<script>

    $(function () {
        $('input.price')
            .addClass('floatright')
            .number(true, 0)
            .end();
    });

    $('#Date').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    $(window).load(function () {
        $("#loader").fadeOut("slow");
        $("#loader-wrapper").fadeOut("slow");
    });

    $(window).submit(function (e) {
        $("#loader").fadeIn("slow");
        $("#loader-wrapper").fadeIn("slow");
    });
</script>
</body>
</html>
