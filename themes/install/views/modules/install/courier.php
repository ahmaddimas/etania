<div class="lockscreen-name">Courier Setting</div>
<br/>
<div align="center" style="width: 400px; align-content: center; margin: auto;">
    <form class="form-horizontal" id="form" action="<?= site_url('install/administrator') ?>" method="post">
        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">API Pengiriman Pro</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="rajaongkir_api_key" placeholder="API Key"
                   value="<?= $data['rajaongkir_api_key'] ?>" type="text" required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Provinsi Pengirim</button>
            </div><!-- /btn-group -->
            <select name="province_id" class="form-control">
                <option value="">-- Pilih --</option>
                <?php foreach ($data['provinces'] as $province) { ?>
                    <?php if ($province['province_id'] == $data['province_id']) { ?>
                        <option value="<?= $province['province_id'] ?>"
                                selected="selected"><?= $province['name'] ?></option>
                    <?php } else { ?>
                        <option value="<?= $province['province_id'] ?>"><?= $province['name'] ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Kota / Kabupaten Pengirim</button>
            </div><!-- /btn-group -->
            <select name="city_id" class="form-control"></select>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Kecamatan Pengirim</button>
            </div><!-- /btn-group -->
            <select name="subdistrict_id" class="form-control"></select>
        </div>

        <div align="center">
            <span class=""><font color="red">Kalkulasi harga dan pilihan beberapa kurir dengan API Key Pro dari <a
                            style="color:maroon !important" target="_blank" href="http://www.rajaongkir.com">RajaOngkir.com</a></font></span>
        </div>

        <button type="submit" class="btn btn-info pull-right">Next <span class="fa fa-arrow-right"></span></button>
    </form>
    <a href="<?= site_url('install/settup') ?>">
        <button class="btn btn-danger pull-left"><span class="fa fa-arrow-left"></span> Back</button>
    </a>
</div>


<script type="text/javascript">
    $('select[name=\'province_id\']').on('change', function () {
        $.ajax({
            url: "<?=site_url('location/province')?>",
            data: 'province_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'province_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                html = '<option value="">-- Pilih --</option>';

                if (json['cities'] && json['cities'] != '') {
                    for (i = 0; i < json['cities'].length; i++) {
                        html += '<option value="' + json['cities'][i]['city_id'] + '"';

                        if (json['cities'][i]['city_id'] == '<?=$data['city_id']?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['cities'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected">-- None --</option>';
                }

                $('select[name=\'city_id\']').html(html);
                $('select[name=\'city_id\']').trigger('change');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'city_id\']').on('change', function () {
        $.ajax({
            url: "<?=site_url('location/city')?>",
            data: 'city_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'city_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                html = '<option value="">-- Pilih --</option>';

                if (json['subdistricts'] && json['subdistricts'] != '') {
                    for (i = 0; i < json['subdistricts'].length; i++) {
                        html += '<option value="' + json['subdistricts'][i]['subdistrict_id'] + '"';

                        if (json['subdistricts'][i]['subdistrict_id'] == '<?=$data['subdistrict_id']?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['subdistricts'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected">-- None --</option>';
                }

                $('select[name=\'subdistrict_id\']').html(html);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'province_id\']').trigger('change');
</script>