<div class="lockscreen-name">Settup Email Pengirim</div>
<br>
<div align="center" style="width: 400px; align-content: center; margin: auto;">

    <form class="form-horizontal" action="<?= site_url('install/settup') ?>" method="post">


        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Email Pengirim</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="smtp_email" placeholder="Email Pengirim"
                   value="<?= $data['smtp_email'] ?>" type="email" required>
        </div>


        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">SMTP Host</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="smtp_host" placeholder="Host" value="<?= $data['smtp_host'] ?>"
                   type="text" required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">SMTP Port</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="smtp_port" placeholder="Port" value="<?= $data['smtp_port'] ?>"
                   type="text" required>
        </div>


        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">SMTP Username</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="smtp_user" placeholder="Username" value="<?= $data['smtp_user'] ?>"
                   type="text" required>
        </div>


        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">SMTP Password</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="smtp_pass" placeholder="Password" value="<?= $data['smtp_pass'] ?>"
                   type="text" required>
        </div>


        <span class="pull-left"><font color="red">Silahkan isi email konfigurasi sesuai dengan setting email server pada server anda</font></span>

        <button type="submit" class="btn btn-info pull-right">Next <span class="fa fa-arrow-right"></span></button>
    </form>
    <a href="<?= site_url('install/company') ?>">
        <button class="btn btn-danger pull-left"><span class="fa fa-arrow-left"></span> Back</button>
    </a>


</div>
