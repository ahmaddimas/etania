<div class="lockscreen-name">Selamat datang</div>
<br>
<br>
<br>
<br>
<div align="center" style="width: 800px; align-content: center; margin: auto;">
    <a target="_blank" href="<?= DIR_FILE . 'manual.pdf' ?>">
        <button class="btn btn-warning"><span class="fa fa-book"></span> Baca buku petunjuk</button>
    </a>
    <a href="<?= site_url('install/database') ?>">
        <button onclick="return confirm('Sudah membaca buku petunjuk?')" class="btn btn-success"><span
                    class="fa fa-download"></span> Mulai menginstall
        </button>
    </a>
</div>