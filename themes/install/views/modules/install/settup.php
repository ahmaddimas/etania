<div class="lockscreen-name">Website Setting</div>
<br/>
<div align="center" style="width: 400px; align-content: center; margin: auto;">
    <form class="form-horizontal" id="form" action="<?= site_url('install/courier') ?>" method="post">
        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Nama Website</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="site_name" placeholder="Nama Website" value="<?= $data['site_name'] ?>"
                   type="text" required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">SEO Title</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="seo_title" placeholder="Judul" value="<?= $data['seo_title'] ?>"
                   type="text" required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Meta Keyword</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="meta_keywords" placeholder="Keyword" value="<?= $data['meta_keywords'] ?>"
                   type="text" required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Meta Description</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="meta_description" placeholder="Description"
                   value="<?= $data['meta_description'] ?>" type="text" required>
        </div>


        <span class="pull-left"><font
                    color="red">Keyword dapat di isi lebih dari satu dengan pemisah tanda koma</a></font></span>

        <button type="submit" class="btn btn-info pull-right">Next <span class="fa fa-arrow-right"></span></button>
    </form>
    <a href="<?= site_url('install/email') ?>">
        <button class="btn btn-danger pull-left"><span class="fa fa-arrow-left"></span> Back</button>
    </a>
</div>
