<div class="lockscreen-name">Settup Administrator</div>
<br>
<div align="center" style="width: 400px; align-content: center; margin: auto;">

    <form class="form-horizontal" action="<?= site_url('install/finish') ?>" method="post">
        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Nama</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="name" placeholder="Nama" value="<?= $data['name'] ?>" type="text"
                   required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Username</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="username" placeholder="Username" value="<?= $data['username'] ?>"
                   type="text" required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Email</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="email" placeholder="Email" value="<?= $data['email'] ?>" type="email"
                   required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Password</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="password" placeholder="Password" value="<?= $data['password'] ?>"
                   type="text" required>
        </div>


        <span class="pull-left"><font color="red">Administrator adalah user yang dapat masuk halaman admin dan mengelola seluruh website</font></span>

        <button type="submit" class="btn btn-info pull-right">Next <span class="fa fa-arrow-right"></span></button>
    </form>
    <a href="<?= site_url('install/courier') ?>">
        <button class="btn btn-danger pull-left"><span class="fa fa-arrow-left"></span> Back</button>
    </a>


</div>
