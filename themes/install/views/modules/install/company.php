<div class="lockscreen-name">Settup Company</div>
<br>
<div align="center" style="width: 400px; align-content: center; margin: auto;">

    <form class="form-horizontal" action="<?= site_url('install/email') ?>" method="post">
        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Nama Perusahaan</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="company" placeholder="Nama Perusahaan" value="<?= $data['company'] ?>"
                   type="text" required>
        </div>


        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Alamat</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="address" placeholder="Alamat" value="<?= $data['address'] ?>" type="text"
                   required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Np. Thelephone</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="telephone" placeholder="Np. Thelephone" value="<?= $data['telephone'] ?>"
                   type="text" required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Email</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="email" placeholder="Email" value="<?= $data['email'] ?>" type="email"
                   required>
        </div>


        <span class="pull-left"><font color="red">Anda dapat melengkapi infromasi perusahaan anda setelah proses instalasi selesai</font></span>

        <button type="submit" class="btn btn-info pull-right">Next <span class="fa fa-arrow-right"></span></button>
    </form>
    <a href="<?= site_url('install/database') ?>">
        <button class="btn btn-danger pull-left"><span class="fa fa-arrow-left"></span> Back</button>
    </a>


</div>
