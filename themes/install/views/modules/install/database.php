<div class="lockscreen-name">Install Database</div>
<br>
<div align="center" style="width: 400px; align-content: center; margin: auto;">

    <form class="form-horizontal" action="<?= site_url('install/company') ?>" method="post">
        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Nama Database</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="dbname" placeholder="Nama Database" value="<?= $data['database_name'] ?>"
                   type="text" required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Database Username</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="dbusername" placeholder="Database Username"
                   value="<?= $data['database_username'] ?>" type="text" required>
        </div>

        <div class="input-group" style="margin-bottom:5px">
            <div class="input-group-btn">
                <button type="button" class="btn btn-warning">Database Password</button>
            </div><!-- /btn-group -->
            <input class="form-control" name="dbpassword" placeholder="Database Password"
                   value="<?= $data['database_password'] ?>" type="text">
        </div>


        <span class="pull-left"><font color="red">Proses instalasi database akan membutuhkan waktu sekitar 5 menit. Harap tunggu hingga proses selesai</font></span>

        <button type="submit" class="btn btn-info pull-right">Next <span class="fa fa-arrow-right"></span></button>
    </form>
    <a href="<?= site_url('install') ?>">
        <button class="btn btn-danger pull-left"><span class="fa fa-arrow-left"></span> Back</button>
    </a>


</div>
