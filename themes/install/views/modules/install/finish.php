<div class="lockscreen-name">Instalasi Selesai</div>
<br>
<div align="center" style="margin-top:3%;">

    <span class=""><font color="green">Halaman Utama : <a target="_blank" href="<?= site_url() ?>"><?= site_url() ?></a></font></span><br>
    <span class=""><font color="green">Halaman Admin : <a target="_blank"
                                                          href="<?= site_url('office') ?>"><?= site_url('office') ?></a></font></span>
    <br>
    <br>
    <br>

    <a target="_blank" href="<?= DIR_FILE . 'manual.pdf' ?>">
        <button class="btn btn-warning"><span class="fa fa-book"></span> Baca buku petunjuk</button>
    </a>
    <a href="<?= site_url('office') ?>">
        <button class="btn btn-info"><span class="fa fa-gear"></span> Buka halaman admin</button>
    </a>
    <a href="<?= site_url() ?>">
        <button class="btn btn-success"><span class="fa fa-globe"></span> Buka halaman utama</button>
    </a>
</div>
        		
