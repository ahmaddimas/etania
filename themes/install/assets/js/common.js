$(document).ready(function () {
    // Wrap selects
    $("select").wrap("<div class='select'></div>");

    // Highlight any found errors
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && responsive_design == 'yes' && $(window).width() < 768) {
        var i = 0;
        var product = [];

        $(".box-product .carousel .item").each(function () {
            $(this).find(".product-grid .row > div").each(function () {
                if (i > 1) {
                    product.push($(this).html());
                }

                i++;
            });
            for (var s = i - 3; s >= 0; s--, s--) {
                var html = "<div class='item'><div class='product-grid'><div class='row'>";
                if (product[s - 1] != undefined) {
                    html += "<div class='col-xs-6'>" + product[s - 1] + "</div>";
                } else {
                    html += "<div class='col-xs-6'>" + product[s + 1] + "</div>";
                }

                if (product[s] != undefined) {
                    html += "<div class='col-xs-6'>" + product[s] + "</div>";
                } else {
                    html += "<div class='col-xs-6'>" + product[s + 1] + "</div>";
                }
                html += "</div></div></div>";

                $(this).after(html);
            }

            product = [];
            i = 0;
        });
    }

    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });

    // tooltips on hover
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

    // Makes tooltips work on ajax generated content
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
    });
});


var storeUrl = $('base').attr('href');

function getUrlPath() {
    query = String(document.location).split('?');

    if (query[0]) {
        value = query[0].slice(storeUrl.length);
        if (value.length > 0) {
            return urlPath = value;
        } else {
            return urlPath = '';
        }
    }
}

// Cart add remove functions	
var cart = {
    'add': function (product_id, quantity) {
        $.ajax({
            url: 'checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            success: function (json) {
                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('body').append('<div class="modal fade" id="cart-modal" tabindex="-1" role="dialog" aria-hidden="true">' + json['success'] + '</div>');
                    $('#cart-modal').modal('show');
                    $('#cart-modal').on('hidden.bs.modal', function (e) {
                        $('#cart-modal').remove();
                    });

                    $('#cart-block .cart-content').load('checkout/cart/info #cart-content-ajax');
                    $('#cart-block .cart-count').html(json['total']);
                }
            }
        });
    },
    'update': function (key, quantity) {
        $.ajax({
            url: 'checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            success: function (json) {
                if (getUrlPath() == 'checkout/cart' || getUrlPath() == 'checkout') {
                    location = 'checkout/cart';
                } else {
                    $('#cart-block .cart-content').load('checkout/cart/info #cart-content-ajax');
                    $('#cart-block .cart-count').html(json['total']);
                }
            }
        });
    },
    'remove': function (key) {
        $.ajax({
            url: 'checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            success: function (json) {
                if (getUrlPath() == 'checkout/cart' || getUrlPath() == 'checkout') {
                    location = 'checkout/cart';
                } else {
                    $('#cart-block .cart-content').load('checkout/cart/info #cart-content-ajax');
                    $('#cart-block .cart-count').html(json['total']);
                }
            }
        });
    }
};

var voucher = {
    'add': function () {

    },
    'remove': function (key) {
        $.ajax({
            url: 'checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                if (getUrlPath() == 'checkout/cart' || getUrlPath() == 'checkout') {
                    location = 'checkout/cart';
                } else {
                    $('#cart-block .cart-content').load('checkout/cart/info #cart-content-ajax');
                    $('#cart-block .cart-count').html(json['total']);
                }
            }
        });
    }
};

var wishlist = {
    'add': function (product_id) {
        $.ajax({
            url: 'customer/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                if (json['success']) {
                    $.notify({
                        message: json['success'],
                        target: '_blank'
                    }, {
                        // settings
                        element: 'body',
                        position: null,
                        type: "info",
                        allow_dismiss: true,
                        newest_on_top: false,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 2031,
                        delay: 5000,
                        timer: 1000,
                        url_target: '_blank',
                        mouse_over: null,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        onShow: null,
                        onShown: null,
                        onClose: null,
                        onClosed: null,
                        icon_type: 'class',
                        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&times;</button>' +
                            '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
                            '<div class="progress" data-notify="progressbar">' +
                            '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                            '</div>' +
                            '<a href="{3}" target="{4}" data-notify="url"></a>' +
                            '</div>'
                    });
                }

                if (json['info']) {
                    $.notify({
                        message: json['info'],
                        target: '_blank'
                    }, {
                        // settings
                        element: 'body',
                        position: null,
                        type: "info",
                        allow_dismiss: true,
                        newest_on_top: false,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 2031,
                        delay: 5000,
                        timer: 1000,
                        url_target: '_blank',
                        mouse_over: null,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        onShow: null,
                        onShown: null,
                        onClose: null,
                        onClosed: null,
                        icon_type: 'class',
                        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-info" role="alert">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&times;</button>' +
                            '<span data-notify="message"><i class="fa fa-info"></i>&nbsp; {2}</span>' +
                            '<div class="progress" data-notify="progressbar">' +
                            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                            '</div>' +
                            '<a href="{3}" target="{4}" data-notify="url"></a>' +
                            '</div>'
                    });
                }

                $('#wishlist-total').html(json['total']);
            }
        });
    }
};

var compare = {
    'add': function (product_id) {
        $.ajax({
            url: 'product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                if (json['success']) {
                    $.notify({
                        message: json['success'],
                        target: '_blank'
                    }, {
                        // settings
                        element: 'body',
                        position: null,
                        type: "info",
                        allow_dismiss: true,
                        newest_on_top: false,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 2031,
                        delay: 5000,
                        timer: 1000,
                        url_target: '_blank',
                        mouse_over: null,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        onShow: null,
                        onShown: null,
                        onClose: null,
                        onClosed: null,
                        icon_type: 'class',
                        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&times;</button>' +
                            '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
                            '<div class="progress" data-notify="progressbar">' +
                            '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                            '</div>' +
                            '<a href="{3}" target="{4}" data-notify="url"></a>' +
                            '</div>'
                    });

                    $('#compare-total').html(json['total']);
                }
            }
        });
    }
};

/* Agree to Terms */
$(document).delegate('.agree', 'click', function (e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function (data) {
            html = '<div id="modal-agree" class="modal fade">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});

/* Autocomplete */
(function ($) {
    function Autocomplete(element, options) {
        this.element = element;
        this.options = options;
        this.timer = null;
        this.items = [];

        $(element).attr('autocomplete', 'off');
        $(element).on('focus', $.proxy(this.focus, this));
        $(element).on('blur', $.proxy(this.blur, this));
        $(element).on('keydown', $.proxy(this.keydown, this));

        $(element).after('<ul class="dropdown-menu"></ul>');
        $(element).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
    }

    Autocomplete.prototype = {
        focus: function () {
            this.request();
        },
        blur: function () {
            setTimeout(function (object) {
                object.hide();
            }, 200, this);
        },
        click: function (event) {
            event.preventDefault();

            value = $(event.target).parent().attr('data-value');

            if (value && this.items[value]) {
                this.options.select(this.items[value]);
            }
        },
        keydown: function (event) {
            switch (event.keyCode) {
                case 27: // escape
                    this.hide();
                    break;
                default:
                    this.request();
                    break;
            }
        },
        show: function () {
            var pos = $(this.element).position();

            $(this.element).siblings('ul.dropdown-menu').css({
                top: pos.top + $(this.element).outerHeight(),
                left: pos.left
            });

            $(this.element).siblings('ul.dropdown-menu').show();
        },
        hide: function () {
            $(this.element).siblings('ul.dropdown-menu').hide();
        },
        request: function () {
            clearTimeout(this.timer);

            this.timer = setTimeout(function (object) {
                object.options.source($(object.element).val(), $.proxy(object.response, object));
            }, 200, this);
        },
        response: function (json) {
            html = '';

            if (json.length) {
                for (i = 0; i < json.length; i++) {
                    this.items[json[i]['value']] = json[i];
                }

                for (i = 0; i < json.length; i++) {
                    if (!json[i]['category']) {
                        html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                    }
                }

                // Get all the ones with a categories
                var category = [];

                for (i = 0; i < json.length; i++) {
                    if (json[i]['category']) {
                        if (!category[json[i]['category']]) {
                            category[json[i]['category']] = [];
                            category[json[i]['category']]['name'] = json[i]['category'];
                            category[json[i]['category']]['item'] = [];
                        }

                        category[json[i]['category']]['item'].push(json[i]);
                    }
                }

                for (i in category) {
                    html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                    for (j = 0; j < category[i]['item'].length; j++) {
                        html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                    }
                }
            }

            if (html) {
                this.show();
            } else {
                this.hide();
            }

            $(this.element).siblings('ul.dropdown-menu').html(html);
        }
    };

    $.fn.autocomplete = function (option) {
        return this.each(function () {
            var data = $(this).data('autocomplete');

            if (!data) {
                data = new Autocomplete(this, option);

                $(this).data('autocomplete', data);
            }
        });
    }
})(window.jQuery);